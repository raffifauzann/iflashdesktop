﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using Microsoft.WindowsAPICodePack.Dialogs;
using System.Diagnostics;

namespace iFlashDownloader
{
    public partial class FiFlashDownloader : Form
    {
        public FiFlashDownloader()
        {
            InitializeComponent();
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            try
            {
                CommonOpenFileDialog dialog = new CommonOpenFileDialog();
                dialog.InitialDirectory = "C:\\Users";
                dialog.IsFolderPicker = true;
                if (dialog.ShowDialog() == CommonFileDialogResult.Ok)
                {
                    txbox_path.Text = dialog.FileName;
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }            
        }        
        private void btnInstall_Click(object sender, EventArgs e)
        {

            try
            {
                btnOpenFileLocation.Visible = false;
                var _proper = Properties.Settings.Default;
                lblNotif.Visible = false;
                var url = _proper.url_pama_file + "v"+txtBox_iFlash_version.Text+".zip";                
                var splitUrl = url.Split('/');
                var filename = splitUrl[splitUrl.Count() - 1];
                var filesave = txbox_path.Text + "\\" + filename;              
                if(txtBox_iFlash_version.Text == "")
                {
                    MessageBox.Show("Versi iFlash Desktop tidak boleh kosong!", "Warning");
                }
                else if (txbox_path.Text == "")
                {
                    MessageBox.Show("Save to folder tidak boleh kosong!", "Warning");
                    
                }
                else
                {
                    using (WebClient wc = new WebClient())
                    {
                        wc.DownloadProgressChanged += wc_DownloadProgressChanged;
                        wc.DownloadFileAsync(
                            // Param1 = Link of file
                            new System.Uri(url),
                            // Param2 = Path to save
                            filesave
                        );
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message,"Exception");
            }
            
            
        }
        void wc_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            progressBar.Value = e.ProgressPercentage;
            lblNotif.Visible = true;
            btnDownload.Enabled = false;
            lblNotif.Text = "Downloading..";
            if (progressBar.Value == 100)
            {
                progressBar.Value = 0;
                btnDownload.Enabled = true;
                btnOpenFileLocation.Visible = true;
                lblNotif.Visible =false;
            }
        }
        private void btnUpdate_Click(object sender, EventArgs e)
        {

        }

        private void btnOpenFileLocation_Click(object sender, EventArgs e)
        {
            Process.Start(@""+ txbox_path.Text + "");
            Application.Exit();
        }
        private void btn1_Click(object sender, EventArgs e)
        {
            txtBox_iFlash_version.Text = txtBox_iFlash_version.Text + "1";
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            txtBox_iFlash_version.Text = txtBox_iFlash_version.Text + "2";
        }

        private void btn3_Click(object sender, EventArgs e)
        {
            txtBox_iFlash_version.Text = txtBox_iFlash_version.Text + "3";
        }

        private void btn4_Click(object sender, EventArgs e)
        {
            txtBox_iFlash_version.Text = txtBox_iFlash_version.Text + "4";
        }

        private void btn5_Click(object sender, EventArgs e)
        {
            txtBox_iFlash_version.Text = txtBox_iFlash_version.Text + "5";
        }

        private void btn6_Click(object sender, EventArgs e)
        {
            txtBox_iFlash_version.Text = txtBox_iFlash_version.Text + "6";
        }

        private void btn7_Click(object sender, EventArgs e)
        {
            txtBox_iFlash_version.Text = txtBox_iFlash_version.Text + "7";
        }

        private void btn8_Click(object sender, EventArgs e)
        {
            txtBox_iFlash_version.Text = txtBox_iFlash_version.Text + "8";
        }

        private void btn9_Click(object sender, EventArgs e)
        {
            txtBox_iFlash_version.Text = txtBox_iFlash_version.Text + "9";
        }

        private void btn0_Click(object sender, EventArgs e)
        {
            txtBox_iFlash_version.Text = txtBox_iFlash_version.Text + "0";
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtBox_iFlash_version.Text = "";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
