﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyncLogsheet
{
    static class GlobalModel
    {
        private static PlanCoachProfileModel _planCoachProfileModel;
        private static tbl_logsheet_detail _tbl_logsheet_detail;
        private static LoginModel _loginModel;
        private static LcrData _lcrData;
        private static tbl_log _tbl_log;
        private static tbl_setting _tbl_setting;

        public static PlanCoachProfileModel GlobalVar
        {
            get { return _planCoachProfileModel; }
            set { _planCoachProfileModel = value; }
        }

        public static tbl_logsheet_detail logsheet_detail
        {
            get { return _tbl_logsheet_detail; }
            set { _tbl_logsheet_detail = value; }
        }

        public static LoginModel loginModel
        {
            get { return _loginModel; }
            set { _loginModel = value; }
        }

        public static LcrData lcrData
        {
            get { return _lcrData; }
            set { _lcrData = value; }
        }

        public static tbl_log logerror
        {
            get { return _tbl_log; }
            set { _tbl_log = value; }
        }

        public static tbl_setting settingWhouse
        {
            get { return _tbl_setting; }
            set { _tbl_setting = value; }
        }
    }
}
