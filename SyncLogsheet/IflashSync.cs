﻿using Flurl.Http;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyncLogsheet
{
    public class IflashSync
    {
        private string myConnectionString = string.Empty;

        public IflashSync(String sConnectionString)
        {
            this.myConnectionString = sConnectionString;
        } 

        async Task<LoginModel> getTokenAsync()
        {
            LoginModel _token = new LoginModel();
            String _url = "https://supplyservice.pamapersada.com/api/token";
            //p61121702 
            try
            {
                _token = await _url.PostJsonAsync(new { username = "p61121702", password = "pdt_ok3", app = "fuel" }).ReceiveJson<LoginModel>();
                GlobalModel.loginModel = _token;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            return _token;
        }

        public async Task onSyncAsync()
        {
            var _auth = GlobalModel.loginModel;

            String _url = "https://supplyservice.pamapersada.com/api/IssuingService/SyncRefuelingMandiri";
            await getTokenAsync();
            var postParam = getListIssuing();

            if (postParam.Count > 0)
            {
                foreach(var item in postParam)
                {
                    try
                    {

                        var _list = new List<tbl_logsheet_detail>();
                        _list.Add(item);

                        ClsIssuing data = await _url.WithHeaders(new { Authorization = _auth.token })
                            .PostJsonAsync(_list).ReceiveJson<ClsIssuing>();

                        updateTableSync(data, item);
                    }catch(Exception ex)
                    {
                        save_error_log(ex.ToString(), "IflashSync/onSyncAsync");
                    }
                }

            }
        } 

        void updateTableSync(ClsIssuing sClsIssuing, tbl_logsheet_detail sTbl_logsheet_detail)
        {
            MySqlConnection conn;
            int recs = 0;
            String iSyncs = "9";

            string query = string.Empty;
            conn = new MySqlConnection(myConnectionString);
            conn.Open();
            String failed_pids = string.Empty;
            if (sClsIssuing.status && sClsIssuing.failed_job_row_id.Count() == 0)
                iSyncs = "7";

            query = $"update tbl_t_log_sheet_detail set syncs = {iSyncs} where job_row_id ='{sTbl_logsheet_detail.job_row_id}'";
            recs = execCmd(query, conn); 

            conn.Close();
        }

        int execCmd(String sQuery, MySqlConnection sConn)
        {
            int iReturn = 0;

            var cmd = new MySqlCommand(sQuery, sConn);
            iReturn = cmd.ExecuteNonQuery();

            return iReturn;
        }

        List<tbl_logsheet_detail> getListIssuing()
        {
            List<tbl_logsheet_detail> _list = new List<tbl_logsheet_detail>();
            MySqlConnection conn;

            conn = new MySqlConnection(myConnectionString);
            conn.Open();
            string query = string.Empty; 

            query = "select * from tbl_t_log_sheet_detail where syncs in(2,9,3)";

            MySqlCommand cmd = new MySqlCommand(query, conn);
            MySqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                var cls = new tbl_logsheet_detail();

                cls.job_row_id = reader["job_row_id"].ToString();
                cls.dstrct_code = reader["dstrct_code"].ToString();
                cls.log_sheet_code = reader["log_sheet_code"].ToString();
                cls.input_type = reader["input_type"].ToString();
                cls.issued_date = Convert.ToDateTime(reader["issued_date"].ToString());
                cls.whouse_id = reader["whouse_id"].ToString();
                cls.unit_no = reader["unit_no"].ToString();
                cls.max_tank_capacity = Convert.ToInt32(reader["max_tank_capacity"].ToString());
                cls.hm_before = Convert.ToInt32(reader["hm_before"].ToString());
                cls.hm = Convert.ToInt32(reader["hm"].ToString());
                cls.flw_meter = reader["flw_meter"].ToString();
                cls.meter_faktor = Convert.ToDouble(reader["meter_faktor"].ToString());
                cls.qty_loqsheet = Convert.ToDouble(reader["qty_loqsheet"].ToString());
                cls.qty = Convert.ToDouble(reader["qty"].ToString());
                cls.shift = reader["shift"].ToString();
                cls.fuel_oil_type = reader["fuel_oil_type"].ToString();
                cls.stat_type = reader["stat_type"].ToString();
                cls.nrp_operator = reader["nrp_operator"].ToString();
                cls.nama_operator = reader["nama_operator"].ToString();
                cls.work_area = reader["work_area"].ToString();
                cls.location = reader["location"].ToString();
                cls.ref_condition = reader["ref_condition"].ToString();
                cls.ref_hour_start = GeneralFunc.RightGet(reader["ref_hour_start"].ToString(), 20);
                cls.ref_hour_stop = GeneralFunc.RightGet(reader["ref_hour_stop"].ToString(), 20);
                cls.note = reader["note"].ToString();
                cls.timezone = reader["timezone"].ToString();
                cls.flag_loading = reader["flag_loading"].ToString();
                cls.loadingerror = reader["loadingerror"].ToString();
                cls.loadingdatetime = Convert.ToDateTime(reader["loadingdatetime"].ToString());
                cls.mod_by = reader["mod_by"].ToString();
                cls.mod_date = Convert.ToDateTime(reader["mod_date"].ToString());
                cls.flow_meter_start = Convert.ToDouble(reader["flow_meter_start"].ToString());
                cls.flow_meter_end = Convert.ToDouble(reader["flow_meter_end"].ToString());
                cls.resp_code = reader["resp_code"].ToString();
                cls.resp_name = reader["resp_name"].ToString();
                cls.text_header = reader["text_header"].ToString();
                cls.text_sub_header = reader["text_sub_header"].ToString();
                cls.text_body = reader["text_body"].ToString();

                _list.Add(cls);
            }

            conn.Close();
            return _list;
        } 

        void save_error_log(string data, string actions)
        {
            try
            {
                DateTime localDate = DateTime.Now;
                var _item = new tbl_log();
                var initData = GlobalModel.GlobalVar;

                _item.pid = System.Guid.NewGuid().ToString();
                _item.actions = $"District {initData.DataEmp.DstrctCode}/{actions}";
                _item.message = $"{data}";
                _item.mod_date = localDate;
                _item.mod_by = initData.DataEmp.Nrp;

                MySqlConnection conn;

                conn = new MySqlConnection(myConnectionString);
                conn.Open();
                string query = string.Empty;
                query = "INSERT INTO tbl_log(pid,actions,message,mod_date,mod_by)"
                      + " VALUES(@pid,@actions,@message,@mod_date,@mod_by)";

                MySqlCommand cmd = new MySqlCommand(query, conn);

                cmd.Parameters.Add(new MySqlParameter("@pid", _item.pid));
                cmd.Parameters.Add(new MySqlParameter("@actions", _item.actions));
                cmd.Parameters.Add(new MySqlParameter("@message", _item.message));
                cmd.Parameters.Add(new MySqlParameter("@mod_date", _item.mod_date));
                cmd.Parameters.Add(new MySqlParameter("@mod_by", _item.mod_by));
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception)
            {
            }
        }  
    } 
   
}
