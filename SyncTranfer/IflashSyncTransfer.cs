﻿using Flurl.Http;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyncTranfer
{
    public class IflashSyncTransfer
    {
        private string myConnectionString = string.Empty;

        public IflashSyncTransfer(String sConnectionString)
        {
            this.myConnectionString = sConnectionString;
        } 

        async Task<LoginModel> getTokenAsync()
        {
            Console.WriteLine("Trying login and get token");
            LoginModel _token = new LoginModel();
            String _url = "https://supplyservice.pamapersada.com/api/token";
            //p61121702 
            try
            {
                _token = await _url.PostJsonAsync(new { username = "p61121702", password = "pdt_ok3", app = "fuel" }).ReceiveJson<LoginModel>();
                GlobalModel.loginModel = _token;
                Console.WriteLine("Get token Success");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            return _token;
        }

        public async Task onSyncAsync()
        {
            var _auth = GlobalModel.loginModel;

            String _url = "https://supplyservice.pamapersada.com/api/IssuingService/InsertTransfer";
            _auth =  await getTokenAsync();
            var postParam = getListIssuing();
            Console.WriteLine("##############################################################");
            if (postParam.Count > 0)
            {
                foreach(var item in postParam)
                {
                    try
                    {
                        Console.WriteLine("Sync to server");
                        ClsIssuing data = await _url.WithHeaders(new { Authorization = _auth.token })
                            .PostJsonAsync(item).ReceiveJson<ClsIssuing>();

                        updateTableSync(data, item);
                    }catch(Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                        save_error_log(ex.ToString(), "IflashSyncTransfer/onSyncAsync");
                    }

                    Console.WriteLine("----------------------------------");
                } 
            }

            Console.WriteLine("Finish");
        } 

        void updateTableSync(ClsIssuing sClsIssuing, WhTranfer sTbl_logsheet_detail)
        {
            Console.WriteLine("Update local flag");
            MySqlConnection conn;
            int recs = 0;
            String iSyncs = "9";

            string query = string.Empty;
            conn = new MySqlConnection(myConnectionString);
            conn.Open();
            String failed_pids = string.Empty;
            if (sClsIssuing.status)
                iSyncs = "7";

            query = $"update whtranfer set syncs = {iSyncs} where job_row_id ='{sTbl_logsheet_detail.job_row_id}'";
            recs = execCmd(query, conn); 

            conn.Close();
        }

        int execCmd(String sQuery, MySqlConnection sConn)
        {
            int iReturn = 0;

            var cmd = new MySqlCommand(sQuery, sConn);
            iReturn = cmd.ExecuteNonQuery();

            return iReturn;
        }

        List<WhTranfer> getListIssuing()
        {
            Console.WriteLine("Reading data to upload");
            var _list = new List<WhTranfer>(); 
            var conn = new MySqlConnection(myConnectionString);

            try
            {
                conn.Open();
                string query = string.Empty;

                query = "select * from whtranfer where syncs in(2,9,3)";

                MySqlCommand cmd = new MySqlCommand(query, conn);
                MySqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var whTranfer = new WhTranfer();

                    whTranfer.job_row_id = reader["job_row_id"].ToString();
                    whTranfer.xfer_code = reader["xfer_code"].ToString();
                    whTranfer.xfer_date = reader["xfer_date"].ToString();
                    whTranfer.dstrct_code = reader["dstrct_code"].ToString();
                    whTranfer.employee_id = reader["employee_id"].ToString();
                    whTranfer.issued_whouse = reader["issued_whouse"].ToString();
                    whTranfer.fmeter_serial_no = reader["fmeter_serial_no"].ToString();
                    whTranfer.issued_whouse_type = reader["issued_whouse_type"].ToString();
                    whTranfer.received_whouse = reader["received_whouse"].ToString();
                    whTranfer.received_whouse_type = reader["received_whouse_type"].ToString();
                    whTranfer.stock_code = reader["stock_code"].ToString();
                    whTranfer.qty_xfer = Convert.ToDouble(reader["qty_xfer"]);
                    whTranfer.shift = reader["shift"].ToString();
                    whTranfer.faktor_meter = Convert.ToDouble(reader["faktor_meter"].ToString());
                    whTranfer.fmeter_begin = Convert.ToDouble(reader["fmeter_begin"].ToString());
                    whTranfer.fmeter_end = Convert.ToDouble(reader["fmeter_end"].ToString());
                    whTranfer.xfer_by = reader["xfer_by"].ToString();
                    whTranfer.received_by = reader["received_by"].ToString();
                    whTranfer.mod_date = Convert.ToDateTime(reader["mod_date"].ToString());
                    whTranfer.created_date = Convert.ToDateTime(reader["created_date"].ToString());
                    whTranfer.created_by = reader["created_by"].ToString();
                    whTranfer.timezone = reader["timezone"].ToString();
                    whTranfer.is_submit = Convert.ToInt32(reader["is_submit"].ToString());
                    whTranfer.syncs = Convert.ToInt32(reader["syncs"].ToString());
                    _list.Add(whTranfer);
                }
            }
            catch(Exception ex)
            {

            } 

            conn.Close();
            return _list;
        } 

        void save_error_log(string data, string actions)
        {
            try
            {
                DateTime localDate = DateTime.Now;
                var _item = new tbl_log();
                var initData = GlobalModel.GlobalVar;

                _item.pid = System.Guid.NewGuid().ToString();
                _item.actions = $"District {initData.DataEmp.DstrctCode}/{actions}";
                _item.message = $"{data}";
                _item.mod_date = localDate;
                _item.mod_by = initData.DataEmp.Nrp;

                MySqlConnection conn;

                conn = new MySqlConnection(myConnectionString);
                conn.Open();
                string query = string.Empty;
                query = "INSERT INTO tbl_log(pid,actions,message,mod_date,mod_by)"
                      + " VALUES(@pid,@actions,@message,@mod_date,@mod_by)";

                MySqlCommand cmd = new MySqlCommand(query, conn);

                cmd.Parameters.Add(new MySqlParameter("@pid", _item.pid));
                cmd.Parameters.Add(new MySqlParameter("@actions", _item.actions));
                cmd.Parameters.Add(new MySqlParameter("@message", _item.message));
                cmd.Parameters.Add(new MySqlParameter("@mod_date", _item.mod_date));
                cmd.Parameters.Add(new MySqlParameter("@mod_by", _item.mod_by));
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception)
            {
            }
        }  
    } 
   
}
