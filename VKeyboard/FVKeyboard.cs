﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VKeyboard
{
    public partial class FVKeyboard : Form
    {
        public FVKeyboard()
        {
            InitializeComponent();
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams param = base.CreateParams;
                param.ExStyle = 0x08000000;
                return param;
            }
        }
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }       


        private void btn1_Click(object sender, EventArgs e)
        {
            SendKeys.Send("1");
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            SendKeys.Send("2");
        }

        private void btn3_Click(object sender, EventArgs e)
        {
            SendKeys.Send("3");
        }

        private void btn4_Click(object sender, EventArgs e)
        {
            SendKeys.Send("4");
        }

        private void btn5_Click(object sender, EventArgs e)
        {
            SendKeys.Send("5");
        }

        private void btn6_Click(object sender, EventArgs e)
        {
            SendKeys.Send("6");
        }

        private void btn7_Click(object sender, EventArgs e)
        {
            SendKeys.Send("7");
        }

        private void btn8_Click(object sender, EventArgs e)
        {
            SendKeys.Send("8");
        }

        private void btn9_Click(object sender, EventArgs e)
        {
            SendKeys.Send("9");
        }

        private void btn0_Click(object sender, EventArgs e)
        {
            SendKeys.Send("0");
        }

        private void btnUp_Click(object sender, EventArgs e)
        {
            SendKeys.Send("{UP}");
        }

       

        private void btnLeft_Click(object sender, EventArgs e)
        {
            SendKeys.Send("{LEFT}");
        }

        private void btnDown_Click(object sender, EventArgs e)
        {
            SendKeys.Send("{DOWN}");
        }

        private void btnRight_Click(object sender, EventArgs e)
        {
            SendKeys.Send("{RIGHT}");
        }

        private void btnDel_Click(object sender, EventArgs e)
        {
            SendKeys.Send("^a");
            SendKeys.Send("{DEL}");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SendKeys.Send("{ENTER}");
        }

        private void btnTab_Click(object sender, EventArgs e)
        {
            SendKeys.Send("{TAB}");
        }        

        private void btnSpace_Click(object sender, EventArgs e)
        {
            SendKeys.Send(" ");
        }
       
        

        private void huruf(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            if(btnCaps.Checked== true)
            {
                SendKeys.Send(btn.Text);
            }
            else
            {
                var val = btn.Text.ToLower();
                SendKeys.Send(val);
            }
            
        }

        private void btnCaps_Click(object sender, EventArgs e)
        {
            if (btnCaps.Checked == true)
            {
                btnCaps.BackColor = Color.SkyBlue;
                foreach (var ctrl in this.Controls)
                    if (ctrl.GetType() == typeof(Button))
                        if (((Button)ctrl).Text != "TAB" && ((Button)ctrl).Text != "CLR" && ((Button)ctrl).Text != "ENTER" && ((Button)ctrl).Text != "SPACE" && ((Button)ctrl).Text != "EXIT")
                            ((Button)ctrl).Text = ((Button)ctrl).Text.ToUpper();
            }
            else
            {
                btnCaps.BackColor = Color.Transparent;
                foreach (var ctrl in this.Controls)
                    if (ctrl.GetType() == typeof(Button))
                        if (((Button)ctrl).Text != "TAB" && ((Button)ctrl).Text != "CLR" && ((Button)ctrl).Text != "ENTER" && ((Button)ctrl).Text != "SPACE" && ((Button)ctrl).Text != "EXIT")
                            ((Button)ctrl).Text = ((Button)ctrl).Text.ToLower();
            }
        }

        private void btnStrip_Click(object sender, EventArgs e)
        {
            if (btnCaps.Checked == true)
            {
                SendKeys.Send("-");                
            }
            else
            {
                SendKeys.Send("_");
            }
        }

        private void FVKeyboard_Load(object sender, EventArgs e)
        {
            if(btnCaps.Checked== true)
            {
                foreach (var ctrl in this.Controls)
                    if (ctrl.GetType() == typeof(Button))
                        if (((Button)ctrl).Text != "TAB" && ((Button)ctrl).Text != "CLR" && ((Button)ctrl).Text != "ENTER" && ((Button)ctrl).Text != "SPACE" && ((Button)ctrl).Text != "EXIT")
                            ((Button)ctrl).Text = ((Button)ctrl).Text.ToUpper();
            }
            else
            {
                foreach (var ctrl in this.Controls)
                    if (ctrl.GetType() == typeof(Button))
                        if (((Button)ctrl).Text != "TAB" && ((Button)ctrl).Text != "CLR" && ((Button)ctrl).Text != "ENTER" && ((Button)ctrl).Text != "SPACE" && ((Button)ctrl).Text != "EXIT")
                            ((Button)ctrl).Text = ((Button)ctrl).Text.ToLower();
            }
        }
    }
}
