﻿namespace LCR600SDK
{
    partial class FWarehouse2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FWarehouse2));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxWarehouse = new System.Windows.Forms.ComboBox();
            this.comboBoxSerialNumber = new System.Windows.Forms.ComboBox();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.dateTimeShift1 = new System.Windows.Forms.DateTimePicker();
            this.dateTimeShift2 = new System.Windows.Forms.DateTimePicker();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnNewConfig = new System.Windows.Forms.Button();
            this.cbWarehouseName = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(60, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(256, 42);
            this.label1.TabIndex = 0;
            this.label1.Text = "Warehouse ID";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(146, 156);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(170, 42);
            this.label2.TabIndex = 1;
            this.label2.Text = "SN Code";
            // 
            // comboBoxWarehouse
            // 
            this.comboBoxWarehouse.DropDownHeight = 150;
            this.comboBoxWarehouse.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxWarehouse.FormattingEnabled = true;
            this.comboBoxWarehouse.IntegralHeight = false;
            this.comboBoxWarehouse.Location = new System.Drawing.Point(308, 12);
            this.comboBoxWarehouse.Name = "comboBoxWarehouse";
            this.comboBoxWarehouse.Size = new System.Drawing.Size(340, 50);
            this.comboBoxWarehouse.Sorted = true;
            this.comboBoxWarehouse.TabIndex = 2;
            this.comboBoxWarehouse.SelectedIndexChanged += new System.EventHandler(this.comboBoxWarehouse_SelectedIndexChanged);
            this.comboBoxWarehouse.Click += new System.EventHandler(this.comboBoxWarehouse_Click);
            // 
            // comboBoxSerialNumber
            // 
            this.comboBoxSerialNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxSerialNumber.FormattingEnabled = true;
            this.comboBoxSerialNumber.Location = new System.Drawing.Point(308, 153);
            this.comboBoxSerialNumber.Name = "comboBoxSerialNumber";
            this.comboBoxSerialNumber.Size = new System.Drawing.Size(340, 50);
            this.comboBoxSerialNumber.TabIndex = 3;
            this.comboBoxSerialNumber.SelectedIndexChanged += new System.EventHandler(this.comboBoxSerialNumber_SelectedIndexChanged);
            // 
            // btnSubmit
            // 
            this.btnSubmit.BackColor = System.Drawing.Color.Green;
            this.btnSubmit.FlatAppearance.BorderSize = 0;
            this.btnSubmit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSubmit.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSubmit.ForeColor = System.Drawing.Color.White;
            this.btnSubmit.Location = new System.Drawing.Point(151, 275);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(201, 67);
            this.btnSubmit.TabIndex = 4;
            this.btnSubmit.Text = "OK";
            this.btnSubmit.UseVisualStyleBackColor = false;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(19, 222);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(123, 42);
            this.label3.TabIndex = 5;
            this.label3.Text = "Shift 1";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(376, 222);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(123, 42);
            this.label4.TabIndex = 6;
            this.label4.Text = "Shift 2";
            // 
            // dateTimeShift1
            // 
            this.dateTimeShift1.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimeShift1.CustomFormat = "HH:mm";
            this.dateTimeShift1.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimeShift1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimeShift1.Location = new System.Drawing.Point(177, 217);
            this.dateTimeShift1.Name = "dateTimeShift1";
            this.dateTimeShift1.ShowUpDown = true;
            this.dateTimeShift1.Size = new System.Drawing.Size(175, 49);
            this.dateTimeShift1.TabIndex = 7;
            // 
            // dateTimeShift2
            // 
            this.dateTimeShift2.CustomFormat = "HH:mm";
            this.dateTimeShift2.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimeShift2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimeShift2.Location = new System.Drawing.Point(521, 217);
            this.dateTimeShift2.Name = "dateTimeShift2";
            this.dateTimeShift2.ShowUpDown = true;
            this.dateTimeShift2.Size = new System.Drawing.Size(175, 49);
            this.dateTimeShift2.TabIndex = 8;
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.Red;
            this.btnCancel.FlatAppearance.BorderSize = 0;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.ForeColor = System.Drawing.Color.White;
            this.btnCancel.Location = new System.Drawing.Point(151, 276);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(201, 64);
            this.btnCancel.TabIndex = 9;
            this.btnCancel.Text = "EXIT";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Visible = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnNewConfig
            // 
            this.btnNewConfig.BackColor = System.Drawing.Color.Blue;
            this.btnNewConfig.FlatAppearance.BorderSize = 0;
            this.btnNewConfig.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNewConfig.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold);
            this.btnNewConfig.ForeColor = System.Drawing.Color.White;
            this.btnNewConfig.Location = new System.Drawing.Point(358, 278);
            this.btnNewConfig.Name = "btnNewConfig";
            this.btnNewConfig.Size = new System.Drawing.Size(201, 64);
            this.btnNewConfig.TabIndex = 10;
            this.btnNewConfig.Text = "UPDATE";
            this.btnNewConfig.UseVisualStyleBackColor = false;
            this.btnNewConfig.Click += new System.EventHandler(this.btnNewConfig_Click);
            // 
            // cbWarehouseName
            // 
            this.cbWarehouseName.DropDownHeight = 150;
            this.cbWarehouseName.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbWarehouseName.FormattingEnabled = true;
            this.cbWarehouseName.IntegralHeight = false;
            this.cbWarehouseName.Location = new System.Drawing.Point(311, 85);
            this.cbWarehouseName.Name = "cbWarehouseName";
            this.cbWarehouseName.Size = new System.Drawing.Size(340, 50);
            this.cbWarehouseName.Sorted = true;
            this.cbWarehouseName.TabIndex = 12;
            this.cbWarehouseName.SelectedIndexChanged += new System.EventHandler(this.cbWarehouseName_SelectedIndexChanged);
            this.cbWarehouseName.Click += new System.EventHandler(this.cbWarehouseName_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(2, 85);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(319, 42);
            this.label5.TabIndex = 11;
            this.label5.Text = "Warehouse Name";
            // 
            // btnExit
            // 
            this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExit.BackColor = System.Drawing.Color.Red;
            this.btnExit.FlatAppearance.BorderSize = 0;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.btnExit.ForeColor = System.Drawing.Color.White;
            this.btnExit.Location = new System.Drawing.Point(1090, 12);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(129, 38);
            this.btnExit.TabIndex = 21;
            this.btnExit.Text = "LOG OUT";
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // FWarehouse2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightGray;
            this.ClientSize = new System.Drawing.Size(1238, 356);
            this.ControlBox = false;
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.cbWarehouseName);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.dateTimeShift2);
            this.Controls.Add(this.dateTimeShift1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.comboBoxSerialNumber);
            this.Controls.Add(this.comboBoxWarehouse);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnNewConfig);
            this.Controls.Add(this.btnCancel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FWarehouse2";
            this.Text = "FWarehouse2";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Activated += new System.EventHandler(this.FWarehouse_Activated);
            this.Load += new System.EventHandler(this.FWarehouse_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBoxWarehouse;
        private System.Windows.Forms.ComboBox comboBoxSerialNumber;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dateTimeShift1;
        private System.Windows.Forms.DateTimePicker dateTimeShift2;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnNewConfig;
        private System.Windows.Forms.ComboBox cbWarehouseName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnExit;
    }
}