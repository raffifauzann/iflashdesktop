﻿namespace LCR600SDK
{
    partial class FReceivedFTDarat
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FReceivedFTDarat));
            this.panel1 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.btnSearch = new FontAwesome.Sharp.IconButton();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblPageOf = new System.Windows.Forms.Label();
            this.btnNext = new System.Windows.Forms.Button();
            this.txtPage = new System.Windows.Forms.TextBox();
            this.btnPrev = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.listPenerimaanFtDaratClassBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.db_trainingDataSet = new LCR600SDK.db_trainingDataSet();
            this.listDatumRitasiMaintankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.listDatumRitasiMaintankTableAdapter = new LCR600SDK.db_trainingDataSetTableAdapters.ListDatumRitasiMaintankTableAdapter();
            this.listPenerimaanFtDaratClassTableAdapter = new LCR600SDK.db_trainingDataSetTableAdapters.ListPenerimaanFtDaratClassTableAdapter();
            this.Action = new System.Windows.Forms.DataGridViewButtonColumn();
            this.districtDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.transportirNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PoNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PoQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PlanStartPeriod = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PlanFinishPeriod = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.listPenerimaanFtDaratClassBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.db_trainingDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.listDatumRitasiMaintankBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(101)))), ((int)(((byte)(179)))));
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.btnSearch);
            this.panel1.Controls.Add(this.txtSearch);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(974, 87);
            this.panel1.TabIndex = 24;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(65, 6);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(141, 29);
            this.label3.TabIndex = 105;
            this.label3.Text = "PO Number";
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.BackColor = System.Drawing.Color.Red;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(865, 9);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(97, 37);
            this.button1.TabIndex = 23;
            this.button1.Text = "Close";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.BackColor = System.Drawing.Color.Orange;
            this.btnSearch.FlatAppearance.BorderSize = 0;
            this.btnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.btnSearch.IconChar = FontAwesome.Sharp.IconChar.None;
            this.btnSearch.IconColor = System.Drawing.Color.Black;
            this.btnSearch.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnSearch.IconSize = 25;
            this.btnSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSearch.Location = new System.Drawing.Point(285, 23);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(109, 36);
            this.btnSearch.TabIndex = 21;
            this.btnSearch.Text = "Cari";
            this.btnSearch.UseVisualStyleBackColor = false;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // txtSearch
            // 
            this.txtSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.txtSearch.Location = new System.Drawing.Point(12, 37);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(267, 35);
            this.txtSearch.TabIndex = 1;
            this.txtSearch.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSearch_KeyPress);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(101)))), ((int)(((byte)(179)))));
            this.panel3.Controls.Add(this.lblPageOf);
            this.panel3.Controls.Add(this.btnNext);
            this.panel3.Controls.Add(this.txtPage);
            this.panel3.Controls.Add(this.btnPrev);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 487);
            this.panel3.Margin = new System.Windows.Forms.Padding(1);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(974, 42);
            this.panel3.TabIndex = 25;
            // 
            // lblPageOf
            // 
            this.lblPageOf.AutoSize = true;
            this.lblPageOf.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.lblPageOf.ForeColor = System.Drawing.Color.White;
            this.lblPageOf.Location = new System.Drawing.Point(160, 7);
            this.lblPageOf.Name = "lblPageOf";
            this.lblPageOf.Size = new System.Drawing.Size(70, 29);
            this.lblPageOf.TabIndex = 24;
            this.lblPageOf.Text = "Page";
            // 
            // btnNext
            // 
            this.btnNext.Enabled = false;
            this.btnNext.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.btnNext.Location = new System.Drawing.Point(112, 3);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(42, 36);
            this.btnNext.TabIndex = 23;
            this.btnNext.Text = ">";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // txtPage
            // 
            this.txtPage.Enabled = false;
            this.txtPage.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.txtPage.Location = new System.Drawing.Point(60, 4);
            this.txtPage.Name = "txtPage";
            this.txtPage.Size = new System.Drawing.Size(46, 35);
            this.txtPage.TabIndex = 22;
            this.txtPage.Text = "0";
            this.txtPage.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnPrev
            // 
            this.btnPrev.Enabled = false;
            this.btnPrev.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.btnPrev.Location = new System.Drawing.Point(12, 2);
            this.btnPrev.Name = "btnPrev";
            this.btnPrev.Size = new System.Drawing.Size(42, 37);
            this.btnPrev.TabIndex = 21;
            this.btnPrev.Text = "<";
            this.btnPrev.UseVisualStyleBackColor = true;
            this.btnPrev.Click += new System.EventHandler(this.btnPrev_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dataGridView1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 87);
            this.panel2.Margin = new System.Windows.Forms.Padding(1);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(974, 400);
            this.panel2.TabIndex = 26;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridView1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(101)))), ((int)(((byte)(179)))));
            this.dataGridView1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Action,
            this.districtDataGridViewTextBoxColumn,
            this.transportirNameDataGridViewTextBoxColumn,
            this.PoNo,
            this.PoQty,
            this.PlanStartPeriod,
            this.PlanFinishPeriod});
            this.dataGridView1.DataSource = this.listPenerimaanFtDaratClassBindingSource;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.Size = new System.Drawing.Size(974, 400);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            this.dataGridView1.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridView1_CellFormatting);
            this.dataGridView1.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dataGridView1_DataBindingComplete);
            // 
            // listPenerimaanFtDaratClassBindingSource
            // 
            this.listPenerimaanFtDaratClassBindingSource.DataMember = "ListPenerimaanFtDaratClass";
            this.listPenerimaanFtDaratClassBindingSource.DataSource = this.db_trainingDataSet;
            // 
            // db_trainingDataSet
            // 
            this.db_trainingDataSet.DataSetName = "db_trainingDataSet";
            this.db_trainingDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // listDatumRitasiMaintankBindingSource
            // 
            this.listDatumRitasiMaintankBindingSource.DataMember = "ListDatumRitasiMaintank";
            this.listDatumRitasiMaintankBindingSource.DataSource = this.db_trainingDataSet;
            // 
            // listDatumRitasiMaintankTableAdapter
            // 
            this.listDatumRitasiMaintankTableAdapter.ClearBeforeFill = true;
            // 
            // listPenerimaanFtDaratClassTableAdapter
            // 
            this.listPenerimaanFtDaratClassTableAdapter.ClearBeforeFill = true;
            // 
            // Action
            // 
            this.Action.HeaderText = "Action";
            this.Action.MinimumWidth = 6;
            this.Action.Name = "Action";
            this.Action.Text = "Terima";
            this.Action.UseColumnTextForButtonValue = true;
            this.Action.Width = 85;
            // 
            // districtDataGridViewTextBoxColumn
            // 
            this.districtDataGridViewTextBoxColumn.DataPropertyName = "District";
            this.districtDataGridViewTextBoxColumn.HeaderText = "District";
            this.districtDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.districtDataGridViewTextBoxColumn.Name = "districtDataGridViewTextBoxColumn";
            this.districtDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.districtDataGridViewTextBoxColumn.Width = 92;
            // 
            // transportirNameDataGridViewTextBoxColumn
            // 
            this.transportirNameDataGridViewTextBoxColumn.DataPropertyName = "TransportirName";
            this.transportirNameDataGridViewTextBoxColumn.HeaderText = "TransportirName";
            this.transportirNameDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.transportirNameDataGridViewTextBoxColumn.Name = "transportirNameDataGridViewTextBoxColumn";
            this.transportirNameDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.transportirNameDataGridViewTextBoxColumn.Width = 202;
            // 
            // PoNo
            // 
            this.PoNo.DataPropertyName = "PoNo";
            this.PoNo.HeaderText = "PoNo";
            this.PoNo.MinimumWidth = 6;
            this.PoNo.Name = "PoNo";
            // 
            // PoQty
            // 
            this.PoQty.DataPropertyName = "PoQty";
            this.PoQty.HeaderText = "PoQty";
            this.PoQty.MinimumWidth = 6;
            this.PoQty.Name = "PoQty";
            this.PoQty.Width = 104;
            // 
            // PlanStartPeriod
            // 
            this.PlanStartPeriod.DataPropertyName = "PlanStartPeriod";
            this.PlanStartPeriod.HeaderText = "PlanStartPeriod";
            this.PlanStartPeriod.MinimumWidth = 6;
            this.PlanStartPeriod.Name = "PlanStartPeriod";
            this.PlanStartPeriod.Width = 207;
            // 
            // PlanFinishPeriod
            // 
            this.PlanFinishPeriod.DataPropertyName = "PlanFinishPeriod";
            this.PlanFinishPeriod.HeaderText = "PlanFinishPeriod";
            this.PlanFinishPeriod.MinimumWidth = 6;
            this.PlanFinishPeriod.Name = "PlanFinishPeriod";
            this.PlanFinishPeriod.Width = 223;
            // 
            // FReceivedFTDarat
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(974, 529);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel3);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(1);
            this.Name = "FReceivedFTDarat";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Received Fuel Truck Receive Darat";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FReceivedFTPort_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.listPenerimaanFtDaratClassBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.db_trainingDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.listDatumRitasiMaintankBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private db_trainingDataSet db_trainingDataSet;
        private System.Windows.Forms.BindingSource listDatumRitasiMaintankBindingSource;
        private db_trainingDataSetTableAdapters.ListDatumRitasiMaintankTableAdapter listDatumRitasiMaintankTableAdapter;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.BindingSource listPenerimaanFtDaratClassBindingSource;
        private db_trainingDataSetTableAdapters.ListPenerimaanFtDaratClassTableAdapter listPenerimaanFtDaratClassTableAdapter;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.Label lblPageOf;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.TextBox txtPage;
        private System.Windows.Forms.Button btnPrev;
        private FontAwesome.Sharp.IconButton btnSearch;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridViewButtonColumn Action;
        private System.Windows.Forms.DataGridViewTextBoxColumn districtDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn transportirNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn PoNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn PoQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn PlanStartPeriod;
        private System.Windows.Forms.DataGridViewTextBoxColumn PlanFinishPeriod;
    }
}