﻿namespace LCR600SDK
{
    partial class FormTransactionAuto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormTransactionAuto));
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblStop = new System.Windows.Forms.Label();
            this.lblConnect = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.txtTopicSubscribe = new System.Windows.Forms.TextBox();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.txtReceived = new System.Windows.Forms.TextBox();
            this.lblTextLiter = new System.Windows.Forms.Label();
            this.txtGrossQty = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtRefSTart = new System.Windows.Forms.TextBox();
            this.txtRefStop = new System.Windows.Forms.TextBox();
            this.txtGrossTotalizer = new System.Windows.Forms.TextBox();
            this.txtTotalizerStart = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.timerOpen = new System.Windows.Forms.Timer(this.components);
            this.timerQty = new System.Windows.Forms.Timer(this.components);
            this.timerClose = new System.Windows.Forms.Timer(this.components);
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.SteelBlue;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.lblStop);
            this.panel1.Controls.Add(this.lblConnect);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(984, 51);
            this.panel1.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Yellow;
            this.label1.Location = new System.Drawing.Point(12, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 25);
            this.label1.TabIndex = 3;
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label1.Visible = false;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Yellow;
            this.label2.Location = new System.Drawing.Point(18, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 25);
            this.label2.TabIndex = 2;
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label2.Visible = false;
            // 
            // lblStop
            // 
            this.lblStop.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblStop.AutoSize = true;
            this.lblStop.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStop.ForeColor = System.Drawing.Color.Yellow;
            this.lblStop.Location = new System.Drawing.Point(12, 10);
            this.lblStop.Name = "lblStop";
            this.lblStop.Size = new System.Drawing.Size(0, 25);
            this.lblStop.TabIndex = 1;
            this.lblStop.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblStop.Visible = false;
            // 
            // lblConnect
            // 
            this.lblConnect.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblConnect.AutoSize = true;
            this.lblConnect.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConnect.ForeColor = System.Drawing.Color.Yellow;
            this.lblConnect.Location = new System.Drawing.Point(12, 10);
            this.lblConnect.Name = "lblConnect";
            this.lblConnect.Size = new System.Drawing.Size(0, 25);
            this.lblConnect.TabIndex = 0;
            this.lblConnect.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblConnect.Visible = false;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.panel7);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Location = new System.Drawing.Point(884, 51);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(100, 410);
            this.panel3.TabIndex = 4;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.SteelBlue;
            this.panel7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel7.Location = new System.Drawing.Point(0, 310);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(100, 100);
            this.panel7.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel6);
            this.panel2.Controls.Add(this.txtTopicSubscribe);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 51);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(100, 410);
            this.panel2.TabIndex = 3;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.SteelBlue;
            this.panel6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel6.Location = new System.Drawing.Point(0, 310);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(100, 100);
            this.panel6.TabIndex = 0;
            // 
            // txtTopicSubscribe
            // 
            this.txtTopicSubscribe.Enabled = false;
            this.txtTopicSubscribe.Location = new System.Drawing.Point(11, 7);
            this.txtTopicSubscribe.Margin = new System.Windows.Forms.Padding(2);
            this.txtTopicSubscribe.Name = "txtTopicSubscribe";
            this.txtTopicSubscribe.Size = new System.Drawing.Size(80, 20);
            this.txtTopicSubscribe.TabIndex = 11;
            this.txtTopicSubscribe.Visible = false;
            // 
            // btnStart
            // 
            this.btnStart.BackColor = System.Drawing.Color.SteelBlue;
            this.btnStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStart.ForeColor = System.Drawing.Color.Chartreuse;
            this.btnStart.Location = new System.Drawing.Point(0, -1);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(784, 50);
            this.btnStart.TabIndex = 5;
            this.btnStart.Text = "START";
            this.btnStart.UseVisualStyleBackColor = false;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnStop
            // 
            this.btnStop.BackColor = System.Drawing.Color.SteelBlue;
            this.btnStop.Enabled = false;
            this.btnStop.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStop.ForeColor = System.Drawing.Color.Red;
            this.btnStop.Location = new System.Drawing.Point(0, 50);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(784, 50);
            this.btnStop.TabIndex = 4;
            this.btnStop.Text = "STOP";
            this.btnStop.UseVisualStyleBackColor = false;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.SteelBlue;
            this.panel4.Controls.Add(this.btnStart);
            this.panel4.Controls.Add(this.btnStop);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel4.Location = new System.Drawing.Point(100, 361);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(784, 100);
            this.panel4.TabIndex = 5;
            // 
            // txtReceived
            // 
            this.txtReceived.Location = new System.Drawing.Point(105, 79);
            this.txtReceived.Margin = new System.Windows.Forms.Padding(2);
            this.txtReceived.Name = "txtReceived";
            this.txtReceived.Size = new System.Drawing.Size(296, 20);
            this.txtReceived.TabIndex = 10;
            this.txtReceived.Visible = false;
            // 
            // lblTextLiter
            // 
            this.lblTextLiter.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lblTextLiter.AutoSize = true;
            this.lblTextLiter.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTextLiter.Location = new System.Drawing.Point(457, 329);
            this.lblTextLiter.Name = "lblTextLiter";
            this.lblTextLiter.Size = new System.Drawing.Size(59, 25);
            this.lblTextLiter.TabIndex = 12;
            this.lblTextLiter.Text = "Liters";
            // 
            // txtGrossQty
            // 
            this.txtGrossQty.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.txtGrossQty.AutoSize = true;
            this.txtGrossQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 100F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGrossQty.Location = new System.Drawing.Point(273, 132);
            this.txtGrossQty.Name = "txtGrossQty";
            this.txtGrossQty.Size = new System.Drawing.Size(0, 153);
            this.txtGrossQty.TabIndex = 13;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(107, 58);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "REF START";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(679, 57);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "REF STOP";
            // 
            // txtRefSTart
            // 
            this.txtRefSTart.Location = new System.Drawing.Point(180, 54);
            this.txtRefSTart.Name = "txtRefSTart";
            this.txtRefSTart.Size = new System.Drawing.Size(133, 20);
            this.txtRefSTart.TabIndex = 16;
            // 
            // txtRefStop
            // 
            this.txtRefStop.Location = new System.Drawing.Point(745, 54);
            this.txtRefStop.Name = "txtRefStop";
            this.txtRefStop.Size = new System.Drawing.Size(133, 20);
            this.txtRefStop.TabIndex = 17;
            // 
            // txtGrossTotalizer
            // 
            this.txtGrossTotalizer.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGrossTotalizer.Location = new System.Drawing.Point(745, 326);
            this.txtGrossTotalizer.Name = "txtGrossTotalizer";
            this.txtGrossTotalizer.Size = new System.Drawing.Size(133, 30);
            this.txtGrossTotalizer.TabIndex = 18;
            // 
            // txtTotalizerStart
            // 
            this.txtTotalizerStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalizerStart.Location = new System.Drawing.Point(106, 326);
            this.txtTotalizerStart.Name = "txtTotalizerStart";
            this.txtTotalizerStart.Size = new System.Drawing.Size(133, 30);
            this.txtTotalizerStart.TabIndex = 19;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(107, 307);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 13);
            this.label5.TabIndex = 20;
            this.label5.Text = "Totalizer Start";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(745, 306);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 13);
            this.label6.TabIndex = 21;
            this.label6.Text = "Gross Totalizer";
            // 
            // timerOpen
            // 
            this.timerOpen.Interval = 1000;
            this.timerOpen.Tick += new System.EventHandler(this.timerOpen_Tick);
            // 
            // timerQty
            // 
            this.timerQty.Interval = 1000;
            this.timerQty.Tick += new System.EventHandler(this.timerQty_Tick);
            // 
            // timerClose
            // 
            this.timerClose.Interval = 1000;
            this.timerClose.Tick += new System.EventHandler(this.timerClose_Tick);
            // 
            // FormTransactionAuto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 461);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtTotalizerStart);
            this.Controls.Add(this.txtGrossTotalizer);
            this.Controls.Add(this.txtRefStop);
            this.Controls.Add(this.txtRefSTart);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtGrossQty);
            this.Controls.Add(this.txtReceived);
            this.Controls.Add(this.lblTextLiter);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormTransactionAuto";
            this.Text = "RUNNING";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblStop;
        private System.Windows.Forms.Label lblConnect;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.TextBox txtTopicSubscribe;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox txtReceived;
        private System.Windows.Forms.Label lblTextLiter;
        private System.Windows.Forms.Label txtGrossQty;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtRefSTart;
        private System.Windows.Forms.TextBox txtRefStop;
        private System.Windows.Forms.TextBox txtGrossTotalizer;
        private System.Windows.Forms.TextBox txtTotalizerStart;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Timer timerOpen;
        private System.Windows.Forms.Timer timerQty;
        private System.Windows.Forms.Timer timerClose;
    }
}