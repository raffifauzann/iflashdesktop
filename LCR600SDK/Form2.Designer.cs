﻿namespace LCR600SDK
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtGrossQty = new System.Windows.Forms.TextBox();
            this.txtGrossTotalizer = new System.Windows.Forms.TextBox();
            this.txtTotalizerStart = new System.Windows.Forms.TextBox();
            this.timerOpen = new System.Windows.Forms.Timer(this.components);
            this.timerQty = new System.Windows.Forms.Timer(this.components);
            this.timerClose = new System.Windows.Forms.Timer(this.components);
            this.btnStop = new System.Windows.Forms.Button();
            this.lblRefStart = new System.Windows.Forms.Label();
            this.lblRefStop = new System.Windows.Forms.Label();
            this.txtRefSTart = new System.Windows.Forms.TextBox();
            this.txtRefStop = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblStop = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblRun = new System.Windows.Forms.Label();
            this.lblAlertClosed = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(873, 106);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(127, 25);
            this.label2.TabIndex = 1;
            this.label2.Text = "Totalizer End";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(10, 101);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(133, 25);
            this.label3.TabIndex = 2;
            this.label3.Text = "Totalizer Start";
            this.label3.Visible = false;
            // 
            // txtGrossQty
            // 
            this.txtGrossQty.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtGrossQty.BackColor = System.Drawing.Color.White;
            this.txtGrossQty.Enabled = false;
            this.txtGrossQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 150F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGrossQty.ForeColor = System.Drawing.Color.Black;
            this.txtGrossQty.Location = new System.Drawing.Point(0, 262);
            this.txtGrossQty.Name = "txtGrossQty";
            this.txtGrossQty.Size = new System.Drawing.Size(1008, 234);
            this.txtGrossQty.TabIndex = 7;
            this.txtGrossQty.Text = "PLEASE WAIT..";
            this.txtGrossQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtGrossTotalizer
            // 
            this.txtGrossTotalizer.Enabled = false;
            this.txtGrossTotalizer.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGrossTotalizer.Location = new System.Drawing.Point(805, 134);
            this.txtGrossTotalizer.Name = "txtGrossTotalizer";
            this.txtGrossTotalizer.Size = new System.Drawing.Size(195, 30);
            this.txtGrossTotalizer.TabIndex = 4;
            this.txtGrossTotalizer.TextChanged += new System.EventHandler(this.txtGrossTotalizer_TextChanged);
            // 
            // txtTotalizerStart
            // 
            this.txtTotalizerStart.Enabled = false;
            this.txtTotalizerStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalizerStart.Location = new System.Drawing.Point(14, 129);
            this.txtTotalizerStart.Name = "txtTotalizerStart";
            this.txtTotalizerStart.Size = new System.Drawing.Size(196, 30);
            this.txtTotalizerStart.TabIndex = 5;
            this.txtTotalizerStart.Visible = false;
            this.txtTotalizerStart.TextChanged += new System.EventHandler(this.txtTotalizerStart_TextChanged);
            // 
            // timerOpen
            // 
            this.timerOpen.Interval = 1000;
            this.timerOpen.Tick += new System.EventHandler(this.timerOpen_Tick);
            // 
            // timerQty
            // 
            this.timerQty.Interval = 1000;
            this.timerQty.Tick += new System.EventHandler(this.timerQty_Tick);
            // 
            // timerClose
            // 
            this.timerClose.Interval = 1000;
            this.timerClose.Tick += new System.EventHandler(this.timerClose_Tick);
            // 
            // btnStop
            // 
            this.btnStop.BackColor = System.Drawing.Color.Red;
            this.btnStop.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnStop.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStop.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnStop.Location = new System.Drawing.Point(0, 431);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(771, 178);
            this.btnStop.TabIndex = 6;
            this.btnStop.Text = "STOP";
            this.btnStop.UseVisualStyleBackColor = false;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // lblRefStart
            // 
            this.lblRefStart.AutoSize = true;
            this.lblRefStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRefStart.Location = new System.Drawing.Point(13, 19);
            this.lblRefStart.Name = "lblRefStart";
            this.lblRefStart.Size = new System.Drawing.Size(87, 25);
            this.lblRefStart.TabIndex = 7;
            this.lblRefStart.Text = "Ref Start";
            // 
            // lblRefStop
            // 
            this.lblRefStop.AutoSize = true;
            this.lblRefStop.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRefStop.Location = new System.Drawing.Point(913, 24);
            this.lblRefStop.Name = "lblRefStop";
            this.lblRefStop.Size = new System.Drawing.Size(87, 25);
            this.lblRefStop.TabIndex = 8;
            this.lblRefStop.Text = "Ref Stop";
            // 
            // txtRefSTart
            // 
            this.txtRefSTart.Enabled = false;
            this.txtRefSTart.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRefSTart.Location = new System.Drawing.Point(16, 46);
            this.txtRefSTart.Name = "txtRefSTart";
            this.txtRefSTart.Size = new System.Drawing.Size(194, 30);
            this.txtRefSTart.TabIndex = 9;
            // 
            // txtRefStop
            // 
            this.txtRefStop.Enabled = false;
            this.txtRefStop.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRefStop.Location = new System.Drawing.Point(805, 47);
            this.txtRefStop.Name = "txtRefStop";
            this.txtRefStop.Size = new System.Drawing.Size(195, 30);
            this.txtRefStop.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(1183, 391);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(110, 31);
            this.label1.TabIndex = 11;
            this.label1.Text = "LITERS";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 371);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(771, 60);
            this.panel1.TabIndex = 12;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Khaki;
            this.panel3.Controls.Add(this.lblStop);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Location = new System.Drawing.Point(252, 0);
            this.panel3.Margin = new System.Windows.Forms.Padding(2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(519, 60);
            this.panel3.TabIndex = 1;
            // 
            // lblStop
            // 
            this.lblStop.AutoSize = true;
            this.lblStop.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStop.ForeColor = System.Drawing.Color.Crimson;
            this.lblStop.Location = new System.Drawing.Point(432, 15);
            this.lblStop.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblStop.Name = "lblStop";
            this.lblStop.Size = new System.Drawing.Size(0, 25);
            this.lblStop.TabIndex = 0;
            this.lblStop.Visible = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Khaki;
            this.panel2.Controls.Add(this.lblRun);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(771, 60);
            this.panel2.TabIndex = 0;
            // 
            // lblRun
            // 
            this.lblRun.AutoSize = true;
            this.lblRun.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRun.ForeColor = System.Drawing.Color.Green;
            this.lblRun.Location = new System.Drawing.Point(9, 15);
            this.lblRun.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblRun.Name = "lblRun";
            this.lblRun.Size = new System.Drawing.Size(0, 25);
            this.lblRun.TabIndex = 0;
            this.lblRun.Visible = false;
            // 
            // lblAlertClosed
            // 
            this.lblAlertClosed.AutoSize = true;
            this.lblAlertClosed.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAlertClosed.ForeColor = System.Drawing.Color.Red;
            this.lblAlertClosed.Location = new System.Drawing.Point(111, 217);
            this.lblAlertClosed.Name = "lblAlertClosed";
            this.lblAlertClosed.Size = new System.Drawing.Size(781, 42);
            this.lblAlertClosed.TabIndex = 13;
            this.lblAlertClosed.Text = "TRANSACTION CLOSED.. PLEASE WAIT..";
            this.lblAlertClosed.Visible = false;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightGray;
            this.ClientSize = new System.Drawing.Size(771, 609);
            this.Controls.Add(this.lblAlertClosed);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtRefStop);
            this.Controls.Add(this.txtRefSTart);
            this.Controls.Add(this.lblRefStop);
            this.Controls.Add(this.lblRefStart);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.txtTotalizerStart);
            this.Controls.Add(this.txtGrossTotalizer);
            this.Controls.Add(this.txtGrossQty);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "IFlash Dekstop - Running";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form2_Closing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form2_Closed);
            this.Load += new System.EventHandler(this.Form2_Load);
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtGrossQty;
        private System.Windows.Forms.TextBox txtGrossTotalizer;
        private System.Windows.Forms.TextBox txtTotalizerStart;
        private System.Windows.Forms.Timer timerOpen;
        private System.Windows.Forms.Timer timerQty;
        private System.Windows.Forms.Timer timerClose;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Label lblRefStart;
        private System.Windows.Forms.Label lblRefStop;
        private System.Windows.Forms.TextBox txtRefSTart;
        private System.Windows.Forms.TextBox txtRefStop;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label lblStop;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lblRun;
        private System.Windows.Forms.Label lblAlertClosed;
    }
}