﻿namespace LCR600SDK
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtGrossQTY = new System.Windows.Forms.TextBox();
            this.txtGrossPreset = new System.Windows.Forms.TextBox();
            this.txtGrossTotalizer = new System.Windows.Forms.TextBox();
            this.button4 = new System.Windows.Forms.Button();
            this.txtFlowrate = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtValveStatus = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.lblTick = new System.Windows.Forms.Label();
            this.button6 = new System.Windows.Forms.Button();
            this.txtRefSTart = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtRefStop = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtTotalizerStart = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(174, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Get Data";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(12, 12);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 2;
            this.button2.Text = "Open";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Visible = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(93, 12);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 3;
            this.button3.Text = "Close";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Visible = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Gross QTY";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Gross Preset";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 105);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Gross Totalizer";
            // 
            // txtGrossQTY
            // 
            this.txtGrossQTY.Enabled = false;
            this.txtGrossQTY.Location = new System.Drawing.Point(125, 51);
            this.txtGrossQTY.Name = "txtGrossQTY";
            this.txtGrossQTY.Size = new System.Drawing.Size(100, 20);
            this.txtGrossQTY.TabIndex = 7;
            // 
            // txtGrossPreset
            // 
            this.txtGrossPreset.Enabled = false;
            this.txtGrossPreset.Location = new System.Drawing.Point(125, 77);
            this.txtGrossPreset.Name = "txtGrossPreset";
            this.txtGrossPreset.Size = new System.Drawing.Size(100, 20);
            this.txtGrossPreset.TabIndex = 8;
            // 
            // txtGrossTotalizer
            // 
            this.txtGrossTotalizer.Enabled = false;
            this.txtGrossTotalizer.Location = new System.Drawing.Point(125, 105);
            this.txtGrossTotalizer.Name = "txtGrossTotalizer";
            this.txtGrossTotalizer.Size = new System.Drawing.Size(100, 20);
            this.txtGrossTotalizer.TabIndex = 9;
            // 
            // button4
            // 
            this.button4.Enabled = false;
            this.button4.Location = new System.Drawing.Point(12, 288);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 10;
            this.button4.Text = "Start Valve";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // txtFlowrate
            // 
            this.txtFlowrate.Enabled = false;
            this.txtFlowrate.Location = new System.Drawing.Point(125, 156);
            this.txtFlowrate.Name = "txtFlowrate";
            this.txtFlowrate.Size = new System.Drawing.Size(100, 20);
            this.txtFlowrate.TabIndex = 12;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 156);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Flowrate";
            // 
            // txtValveStatus
            // 
            this.txtValveStatus.Enabled = false;
            this.txtValveStatus.Location = new System.Drawing.Point(125, 182);
            this.txtValveStatus.Name = "txtValveStatus";
            this.txtValveStatus.Size = new System.Drawing.Size(100, 20);
            this.txtValveStatus.TabIndex = 14;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 182);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Status";
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(93, 288);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 15;
            this.button5.Text = "Stop Valve";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // lblTick
            // 
            this.lblTick.AutoSize = true;
            this.lblTick.Location = new System.Drawing.Point(271, 293);
            this.lblTick.Name = "lblTick";
            this.lblTick.Size = new System.Drawing.Size(28, 13);
            this.lblTick.TabIndex = 16;
            this.lblTick.Text = "Tick";
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(174, 288);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 23);
            this.button6.TabIndex = 17;
            this.button6.Text = "Timer";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // txtRefSTart
            // 
            this.txtRefSTart.Enabled = false;
            this.txtRefSTart.Location = new System.Drawing.Point(125, 215);
            this.txtRefSTart.Name = "txtRefSTart";
            this.txtRefSTart.Size = new System.Drawing.Size(100, 20);
            this.txtRefSTart.TabIndex = 19;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 215);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "Ref Start";
            // 
            // txtRefStop
            // 
            this.txtRefStop.Enabled = false;
            this.txtRefStop.Location = new System.Drawing.Point(125, 241);
            this.txtRefStop.Name = "txtRefStop";
            this.txtRefStop.Size = new System.Drawing.Size(100, 20);
            this.txtRefStop.TabIndex = 21;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(13, 241);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 13);
            this.label7.TabIndex = 20;
            this.label7.Text = "Ref Stop";
            // 
            // txtTotalizerStart
            // 
            this.txtTotalizerStart.Enabled = false;
            this.txtTotalizerStart.Location = new System.Drawing.Point(125, 131);
            this.txtTotalizerStart.Name = "txtTotalizerStart";
            this.txtTotalizerStart.Size = new System.Drawing.Size(100, 20);
            this.txtTotalizerStart.TabIndex = 23;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(13, 131);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(72, 13);
            this.label8.TabIndex = 22;
            this.label8.Text = "Totalizer Start";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(333, 323);
            this.Controls.Add(this.txtTotalizerStart);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtRefStop);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtRefSTart);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.lblTick);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.txtValveStatus);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtFlowrate);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.txtGrossTotalizer);
            this.Controls.Add(this.txtGrossPreset);
            this.Controls.Add(this.txtGrossQTY);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "RUNNING";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtGrossQTY;
        private System.Windows.Forms.TextBox txtGrossPreset;
        private System.Windows.Forms.TextBox txtGrossTotalizer;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox txtFlowrate;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtValveStatus;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label lblTick;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.TextBox txtRefSTart;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtRefStop;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtTotalizerStart;
        private System.Windows.Forms.Label label8;
    }
}

