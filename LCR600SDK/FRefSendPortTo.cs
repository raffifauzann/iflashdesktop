﻿using Flurl.Http;
using LCR600SDK.DataClass;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LCR600SDK
{
    public partial class FRefSendPortTo : Form
    {
        private List<WhouseSendToPortMaintank> _dataResult;
        private string pUrl_service = string.Empty;
        public String pDistrik = string.Empty;


        public FRefSendPortTo()
        {
            InitializeComponent();
        }

        private void FRefPO_Load(object sender, EventArgs e)
        { 
            var _proper = Properties.Settings.Default;
            pUrl_service = _proper.url_service;
            loadGridAsync(); 
        }

        public async void loadGridAsync()
        {
            string URL_API = $"{pUrl_service}api/FuelService/getWhouseSendToPortMaintank?district={pDistrik}&warehousename=&is_new=1"; 
            var _auth = GlobalModel.loginModel;
            try
            {
                var res = await URL_API.WithHeaders(new { Authorization = _auth.token }).GetStringAsync();
                var _data = WhouseSendToPortMaintank.FromJson(res);

                GlobalModel.whouseSendToPortMaintanks = _data;
                _dataResult = _data;
                DataTable dt = GeneralFunc.ToDataTable(_data);
                dataGridView1.DataSource = dt; 
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            var _list = GlobalModel.whouseSendToPortMaintanks;
            _dataResult = _list.Where(d => d.Warehousename.Contains(txtSearch.Text.Trim().ToUpper())).ToList();
            DataTable dt = GeneralFunc.ToDataTable(_dataResult);
            dataGridView1.DataSource = dt;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.Columns[e.ColumnIndex].Name == "Action")
            { 
                if (Application.OpenForms["FSendFTPortForm"] != null)
                { 
                    var _from = (Application.OpenForms["FSendFTPortForm"] as FSendFTPortForm);
                    _from.txtReceiveAt.Text = _dataResult[e.RowIndex].Warehousename;
                    _from.pReceivedAtCode = _dataResult[e.RowIndex].Warehouseid;
                }
                else if (Application.OpenForms["FSendPOTForm"] != null)
                {
                    var _from = (Application.OpenForms["FSendPOTForm"] as FSendPOTForm);
                    _from.txtReceiveAt.Text = _dataResult[e.RowIndex].Warehousename;
                    _from.pReceivedAtCode = _dataResult[e.RowIndex].Warehouseid;
                }
                this.Close();
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
    }
}
