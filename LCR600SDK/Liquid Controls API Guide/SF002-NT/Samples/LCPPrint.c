/*==========================================================================*/
/* Source File: lcpprint.c                                                  */
/* Program:     LCPPrint                                                    */
/* Function:    main                                                        */
/*                                                                          */
/* Authors and Dates:                                                       */
/*      Bryan P. Haynes     08/12/97    Original                            */
/*      Bryan P. Haynes     08/20/97    Version 1.01 uses LCRM_NO_WAIT.     */
/*      Bryan P. Haynes     05/11/01    Converted to NT version.            */
/*                                                                          */
/* Function:                                                                */
/*      This file contains the program that will allow the operator to send */
/*      a string of text to the printer attached to a LCR.                  */
/*                                                                          */
/* Inputs:                                                                  */
/*      None                                                                */
/*                                                                          */
/* Outputs:                                                                 */
/*      None                                                                */
/*                                                                          */
/*--------------------------------------------------------------------------*/
/* Copyright (c) Liquid Controls, Inc. 1997, 2001                           */
/*               105 Albrecht Drive                                         */
/*               Lake Bluff, IL  60044-2242  USA                            */
/*==========================================================================*/

// Includes.

#include <stdio.h>                      // standard C I/O function prototypes
#include <ctype.h>                      // character type macros
#include <stdlib.h>                     // standard C library function prototypes
#include <string.h>                     // string function prototypes
#include <windows.h>

#include "lcpnt.h"                      // LCP function prototypes
#include "lcp02nt.h"                    // LCP02 function prototypes and definitions

// Function prototypes.

void main(int,char **);                 // lcpprint function definition
static unsigned char ValidateNodeAddress(char *);

// lcpprint entry point.

void main(argc,argv)                    // lcpprint function declaration
int argc;                               // number of command line arguements
char **argv;                            // pointer to table of arguements
{

// Local variables.

    int fail = FALSE;                   // failure flag
    unsigned char device;               // device number
    unsigned char deviceList[1];        // devices present in network
    unsigned char numDevices;           // number of devices in network
    unsigned char rc;                   // return code
    char in[128];                       // operator input buffer/text to print

// Display program name and copyright notice.

    fprintf(stderr,"LCP LCR Print Text (NT) v1.00\n");
    fprintf(stderr,"Copyright (c) 2001 LIQUID CONTROLS, INC., A Unit of IDEX Corporation.\n\n");

// Get users command line parameters.

    if (argc > 3)                       // check for too many parameters
        fail = TRUE;                    // fail the program
    else if (argc <= 1)                 // check for getting parameters via prompts
        device = LCP_BroadcastNode;     // indicate device is needed
    else if ((device=ValidateNodeAddress(argv[1])) == LCP_BroadcastNode)
        fail = TRUE;                    // fail the program
    else if (argc == 2)                 // check for only one parameter
        *in = '\0';                     // indicate text string is needed
    else
        strcpy(in,argv[2]);             // use the parameter text string

// Check for failing due to invalid command line.

    if (fail) {                         // check for invalid command line parameters
        fprintf(stderr,"\nUsage: lcpprint [Device [PrintText]]\n\n");
        fprintf(stderr,"%u <= Device <= %u\n",LCR_MIN_SLAVE_ADDR,LCR_MAX_SLAVE_ADDR);
        fprintf(stderr,"PrintText = Text that is to be printed.\n\n");
        fprintf(stderr,"Note: To print more than one word, enclose the text string in quotes.\n");
    } else {
        if (device != LCP_BroadcastNode && *in == '\0')
            fail = TRUE;                // indicate new-line is needed for esthetics

// Check for prompting for the device.

        while (device == LCP_BroadcastNode) {
            fprintf(stderr,"\nDevice [%d,%d]: ",LCR_MIN_SLAVE_ADDR,LCR_MAX_SLAVE_ADDR);
            gets(in);                   // get the device number string

            if ((device=ValidateNodeAddress(in)) == LCP_BroadcastNode)
                fprintf(stderr,"Invalid device: %s\n",in);
            else
                *in = '\0';             // clear the input string to indicate text string needed
        }

// Open LCP communications.

        if ((rc=LCP02Open(device,device,&numDevices,deviceList)) == LCP02Ra_NODEVICES)
            fprintf(stderr,"\nDevice %u is not present.\n",device);
        else if (rc != LCPR_OK)
            fprintf(stderr,"\nLCP02Open error: rc = 0x%02X\n",rc);
        else {

// Check for prompting for the text string.

            if (fail)                   // check for new-line needed
                fprintf(stderr,"\n");   // esthetics

            while (*in == '\0') {       // loop until text is entered
                fprintf(stderr,"Print Text: ");
                gets(in);               // get the text string from the operator

                if (*in == '\0')        // check for nothing entered
                    fprintf(stderr,"Enter a text string.\n\n");
            }

            strcat(in,"\n");            // append a new line character so printer will print

// Send the text to the printer.

            if ((rc=LCP02Print(device,in,strlen(in),LCRM_NO_WAIT)) == LCP02Rd_REQUESTQUEUED) {
                fprintf(stderr,"\nWaiting for response from LCR...");

                do {
                    rc = LCP02CheckRequest(device);
                } while (rc == LCP02Rd_REQUESTQUEUED);

                fprintf(stderr,"\n");   // esthetics
            }

            if (rc != LCPR_OK)
                fprintf(stderr,"\nLCP02Print error: rc = 0x%02X\n",rc);
            else
                fprintf(stderr,"\nThe text string was sent to the LCR printer.\n");

// Close LCP communications.

            if ((rc=LCP02Close()) != LCPR_OK)
                fprintf(stderr,"LCP02Close error: rc = 0x%02X\n",rc);
        }
    }

// lcpprint exit point.

}

/*==========================================================================*/
/* Source File: lcpprint.c                                                  */
/* Program:     LCPPrint                                                    */
/* Function:    ValidateNodeAddress                                         */
/*                                                                          */
/* Authors and Dates:                                                       */
/*      Bryan P. Haynes     05/11/01    Original                            */
/*                                                                          */
/* Function:                                                                */
/*      This function validates an ASCIIZ string for an LCR node address.   */
/*      If valid, the device address is returned to the caller.             */
/*                                                                          */
/* Inputs:                                                                  */
/*      in - Pointer to an ASCIIZ string that is to be validated.           */
/*                                                                          */
/* Outputs:                                                                 */
/*      ValidateNodeAddress - Node address.                                 */
/*                                                                          */
/*==========================================================================*/

// ValidateNodeAddress entry point.

unsigned char ValidateNodeAddress(char *in)
{

// Local variables.

    unsigned char device;               // device number
    size_t i;                           // loop variable
    size_t len;                         // length of input parameter

// Loop to validate the node address.

    device = LCP_BroadcastNode;         // default to invalid node address

    if ((len=strlen(in)) > 0 && len <= 3) {
        for (i=0; i < len; i++) {       // loop to check for digits
            if (!isdigit(in[i]))        // check next character
                break;                  // break if non-digit found
        }

        if (i == len && (i=atoi(in)) >= LCR_MIN_SLAVE_ADDR && i <= LCR_MAX_SLAVE_ADDR)
            device = (unsigned char)i;
    }

// ValidateNodeAddress exit point.

    return(device);                     // return the node address to the caller
}
