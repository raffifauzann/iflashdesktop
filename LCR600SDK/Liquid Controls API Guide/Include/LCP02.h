/*==========================================================================*/
/* Source File: LCP02.h                                                     */
/* Program:     Generic                                                     */
/* Function:    N/A                                                         */
/*                                                                          */
/* Authors          Dates     Revision  Change Log                          */
/*  Bryan Haynes    09/02/97            Original                            */
/*  Bryan Haynes    11/13/97            Conversion to SP200.                */
/*  Bryan Haynes    11/24/97            SP200 v1.01                         */
/*  Bryan Haynes    06/01/98            SP200 v1.02                         */
/*  Bryan Haynes    08/11/98            SP200 v1.03                         */
/*  Bryan Haynes    10/01/98            SP200 v1.04                         */
/*  Bryan Haynes    02/04/99            SP200 v1.05                         */
/*  Bryan Haynes    09/27/99            SP200 v1.07                         */
/*  Bryan Haynes    12/10/99            SP200 v1.08                         */
/*  Bryan Haynes    02/21/01            Conversion to SF002.                */
/*  Bryan Haynes    02/23/01            Converted to Windows NT.            */
/*  Bryan Haynes    03/27/01            Fixed LCRStatus structure.          */
/*  Bryan Haynes    06/25/03            Fixed external DLL import variable  */
/*                                      declarations for non-C++            */
/*                                      applications.                       */
/*  Bryan Haynes    07/02/03            Version number string added.        */
/*  Bryan Haynes    10/24/03            LCRvolume type definition added.    */
/*  Bryan Haynes    04/21/04            New defines for the Inventory       */
/*                                      preset type, Monitor Flow Rate      */
/*                                      auxiliary port 1 setting, and Epson */
/*                                      printer name changes.               */
/*  Bryan Haynes    07/08/04            LCP02AbortRequest and               */
/*                                      LCP02CheckRequest now require a     */
/*                                      device address as input.            */
/*  Bryan Haynes    07/26/04            Activate Pump & Print message.      */
/*  Bryan Haynes    10/15/04    1.07    LCRTransaction structure added.     */
/*  Bryan Haynes    10/19/04    1.08    Status and date format added to the */
/*                                      transaction structure.              */
/*  Bryan Haynes    05/17/05    1.10    Ticket functions added.             */
/*  Bryan Haynes    10/09/06    1.12    Definitions added for number of     */
/*                                      entries per list group.  Hey--it    */
/*                                      only took 9 years!!                 */
/*  Bryan Haynes    12/22/06    1.00    Ported to Visual Studio 2005.       */
/*  Bryan Haynes    11/02/09    1.13    LIBSPEC changed to LCP02_LIBSPEC.   */
/*  Shakila Xavier  12/07/10    1.14    Added APIs to get exposed global    */
/*                                      variable values.                    */
/*  Shakila Xavier  08/16/10    1.15    Exposed existing API to get comm    */
/*                                      settings for LCR.                   */
/*  Bryan Haynes    06/30/11    2.00    LCR 600 fields added.               */
/*  Bryan Haynes    09/12/11    2.00    IP support added.                   */
/*  Shakila Xavier  07/25/12    2.01    Modified IP APIs and fixed bug in   */
/*                                      reset baud rate.                    */
/*  Shakila Xavier  08/25/15    2.02    Added AvgDensity, DensityUnits      */
/*                                      and maxFlowRate fields.             */
/*  Bryan Haynes    02/26/16    2.03    Added LCRL_REQUIRED as a preset     */
/*                                      type option.                        */
/*  Bryan Haynes    08/04/17    2.03    Ported to eCos and the LCR-1000.    */
/*                                                                          */
/* Function:                                                                */
/*      This file contains the defines, global variables, structures, and   */
/*      function prototypes for the LCR RS-485 C API functions.             */
/*                                                                          */
/* Inputs:                                                                  */
/*      None                                                                */
/*                                                                          */
/* Outputs:                                                                 */
/*      None                                                                */
/*                                                                          */
/*--------------------------------------------------------------------------*/
/* Copyright (c) Liquid Controls, LLC 1997-2017                             */
/*               105 Albrecht Drive                                         */
/*               Lake Bluff, IL  60044-2242  USA                            */
/*==========================================================================*/

// Ensure the file is only included once.

#ifndef LCP02_H                                                         // check for previous inclusion
#define LCP02_H                                                         // ensure only one inclusion

// Includes.

#include <LCP.h>                                                        // include the base LCP definitions

// Miscellaneous LCR definitions.

#define LCP02_PID_LCR                   (2)                             // LCP product ID for LCR

#define LCP02_Baud57600                 (0)                             //  57.6K baud
#define LCP02_Baud19200                 (1)                             //  19.2K baud
#define LCP02_Baud9600                  (2)                             //   9.6K baud
#define LCP02_Baud4800                  (3)                             //   4.8K baud
#define LCP02_Baud2400                  (4)                             //   2.4K baud
#define LCP02_NumBauds                  (5)                             // number of supported LCP02 bauds

#define LCR_SERIAL_PORT                 (-1)                            // IP port number if serial communications is being used

#define LCR_MIN_SLAVE_ADDR              (1)                             // minimum LCR node address
#define LCR_MAX_SLAVE_ADDR              (250)                           // maximum LCR node address

#define LCR_NEW_DEVICE_ADDR             (250)                           // new LCR device address

#define LCRFeaturePOS                   (0x00000001)                    // the POS feature has been purchased
#define LCRFeatureAviation              (0x00000002)                    // the Aviation feature has been purchased

#define LCR_GET_TYPE                    (0)                             // get field type parameter subfunction code
#define LCR_GET_WIDTH                   (1)                             // get field width parameter subfunction code
#define LCR_GET_SECURITY                (2)                             // get field security byte parameter subfunction code
#define LCR_NUM_PARAM_CODES             (3)                             // number of parameter subfunction codes

// LCR text field lengths.

#define LCRTl_ActivationKey             (16)                            // length of the activation key string
#define LCRTl_BootLoader                (10)                            // boot loader revision
#define LCRTl_Date                      (8)                             // date field
#define LCRTl_DeliveryFinish            (17)                            // delivery finish date and time
#define LCRTl_DeliveryStart             (17)                            // delivery start date and time
#define LCRTl_DensityUOM                (7)                             // length of density unit of measure string
#define LCRTl_FactoryKey                (15)                            // factory key
#define LCRTl_Language                  (10)                            // language revision
#define LCRTl_LastCalibrated            (17)                            // date and time of last calibration
#define LCRTl_MeterID                   (10)                            // meter identifier
#define LCRTl_ProductCode               (5)                             // product code
#define LCRTl_ProductDescriptor         (18)                            // product descriptor
#define LCRTl_SerialID                  (10)                            // serial identifier
#define LCRTl_ShiftStart                (17)                            // date and time of beginning of shift
#define LCRTl_Software                  (10)                            // software revision
#define LCRTl_Ticket                    (10)                            // ticket revision
#define LCRTl_TicketHeaderLine          (35)                            // ticket header line
#define LCRTl_Time                      (8)                             // time field
#define LCRTl_UnitID                    (10)                            // unit identifier
#define LCRTl_UserKey                   (10)                            // user key
#define LCRTl_UserSetKey                (10)                            // key to set user key

#define LCRTl_POSActivationKey          (16)                            // length of the POS activation key string
#define LCRTl_POSCashDiscountName       (20)                            // length of the name of cash discounts
#define LCRTl_POSHeader                 (13)                            // length of header text for ticket printing
#define LCRTl_POSMiscChargeName         (8)                             // length of the name of miscellaneous charges
#define LCRTl_POSProductCode            (5)                             // length of the POS product code
#define LCRTl_POSProductName            (18)                            // length of the POS product name
#define LCRTl_POSTaxName                (20)                            // length of the name of taxes
#define LCRTl_POSVolumeDiscountName     (20)                            // length of the name of volume discounts

#define LCRTl_IOBoardUID                (24)                            // length of the UID of the I/O board
#define LCRTl_IOBoardName               (16)                            // length of the name of the I/O board

#define POSNumProducts                  (100)
#define POSNumTaxes                     (16)                            // number of taxes
#define POSNumCashDiscounts             (16)                            // number of cash discounts
#define POSNumVolumeDiscounts           (16)                            // number of volume discounts
#define POSNumMiscCharges               (16)                            // number of miscellaneous charges

#define POSNumTaxCategories             (10)                            // number of tax categories
#define POSNumCashIntervals             (3)                             // number of cash intervals
#define POSNumVolumeIntervals           (3)                             // number of volume intervals

// LCR field types.

#define LCRT_TEXT                       (0)                             // ASCIIZ string
#define LCRT_INTEGER                    (1)                             // 2-byte signed integer
#define LCRT_DATE                       (2)                             // 9-byte ASCIIZ string
#define LCRT_TIME                       (3)                             // 9-byte ASCIIZ string
#define LCRT_LONG                       (4)                             // 4-byte signed long integer
#define LCRT_VOLUME                     (5)                             // 4-byte signed long integer with implied decimal point
#define LCRT_FFLOAT                     (6)                             // free format 4-byte IEEE-754 floating point
#define LCRT_LIST600                    (10)                            // 1-byte LCRlist (LCR++/LCR 600)
#define LCRT_LIST                       (50)                            // 1-byte LCRlist
#define LCRT_UFLOAT                     (80)                            // 4-byte unsigned IEEE-754 floating point
#define LCRT_SFLOAT                     (90)                            // 4-byte signed IEEE-754 floating point
#define LCRT_LIST1000                   (100)                           // 1-byte LCRlist (LCR-1000)

// LCR list type groups.

#define LCRLg_PRODUCT_NUMBER            (0)                             // product number list
#define LCRLg_PRESETS_ALLOWED           (1)                             // presets types allowed list
#define LCRLg_PRODUCT_TYPE              (2)                             // product type list
#define LCRLg_DATE_FORMAT               (3)                             // date format list
#define LCRLg_UNIT_OF_MEASURE           (4)                             // unit of measure list
#define LCRLg_YES_NO                    (5)                             // yes/no list
#define LCRLg_AUX_UNIT_OF_MEASURE       (6)                             // auxiliary unit of measure list
#define LCRLg_PRESET_TYPE               (7)                             // preset type list
#define LCRLg_HEADER_LINE_INDEX         (8)                             // header line index list
#define LCRLg_PULSE_OUTPUT              (9)                             // pulse output list
#define LCRLg_TEMPERATURE_UNIT          (10)                            // temperature unit list
#define LCRLg_RATE_BASE                 (11)                            // rate base list
#define LCRLg_LINEARIZATION_POINT       (12)                            // linearization point list
#define LCRLg_SECURITY                  (13)                            // security locked/unlocked list
#define LCRLg_DECIMALS                  (14)                            // number of decimals list
#define LCRLg_COMP_TYPE                 (15)                            // compensation type list
#define LCRLg_DIAGNOSTIC_MESSAGE        (16)                            // diagnostic messages list
#define LCRLg_COMP_PARAMETER            (17)                            // compensation parameter list
#define LCRLg_COMP_TEMPERATURE          (18)                            // compensation temperature list
#define LCRLg_PRINTER                   (19)                            // printer type list
#define LCRLg_RESIDUAL                  (20)                            // residual processing list
#define LCRLg_FLOW_DIRECTION            (21)                            // flow direction list
#define LCRLg_AUXILIARY_PORT_1          (22)                            // auxiliary port 1 option list
#define LCRLg_AUXILIARY_PORT_2          (23)                            // auxiliary port 2 option list
#define LCRLg_LCR_RESET                 (24)                            // LCR reset option list
#define LCRLg_LINEARIZATION_STATE       (25)                            // linearization state list
#define LCRLg_PASSWORD_USAGE            (26)                            // password usage list
#define LCRLg_CUSTOMER_USAGE            (27)                            // customer number usage list

// LCR 600 list group definitions.

#define LCRLg_FS_PORT                   (50)                            // File Server port selection list
#define LCRLg_FS_BAUD                   (51)                            // File Server baud rate selection list
#define LCRLg_FS_TXENABLE               (52)                            // File Server tx enable bit selection list

#define LCRLg_DELIVERY_SCREEN           (53)                            // delivery screen list

#define LCRLg_LOCK_DISCOUNTS            (60)                            // lock discounts list
#define LCRLg_LOCK_PRICE                (61)                            // lock price list
#define LCRLg_SAVE_NEW_PRICE            (62)                            // save new price list
#define LCRLg_ALLOW_NEW_DLVY_PRICE      (63)                            // allow new delivery prices list

#define LCRLg_PRODUCT_CALIBRATION       (64)                            // product calibration list
#define LCRLg_PRODUCT_TAX_NUM           (65)                            // product tax number list
#define LCRLg_PRODUCT_CASH_DISC         (66)                            // product cash discount list
#define LCRLg_PRODUCT_VOLUME_DISC       (67)                            // product volume discount list

#define LCRLg_POS_TAX_NUM               (68)                            // POS tax number list
#define LCRLg_POS_TAX_CATEGORY          (69)                            // POS tax category list
#define LCRLg_POS_TAX_TYPE              (70)                            // POS tax type list

#define LCRLg_POS_CASH_DISC_NUM         (71)                            // POS cash discount number list
#define LCRLg_POS_CASH_DISC_INT         (72)                            // POS cash discount interval list
#define LCRLg_APPLY_CASH_DISC           (73)                            // apply cash discount list

#define LCRLg_POS_VOLUME_DISC_NUM       (74)                            // POS volume discount number list
#define LCRLg_POS_VOLUME_DISC_INT       (75)                            // POS volume discount interval list
#define LCRLg_APPLY_VOLUME_DISC         (76)                            // apply volume discount list

#define LCRLg_POS_MISC_CHARGE_NUM       (77)                            // POS miscellaneous charge number list
#define LCRLg_POS_MISC_CH_TAX_NUM       (78)                            // POS miscellaneous charge tax number list

// LCR-1000 list group definitions.

#define LCRLg_PULSER_TYPE               (0)                             // pulser type
// #define LCRLg_DELIVERY_SCREEN        (53)                            // NOTE: The delivery screen list is the same group number as used by the LCR 600 as defined above.

// Floating point fields precision.

#define LCRp_Temp                       (1)
#define LCRp_PulsesPerDistance          (6)
#define LCRp_CalibDistance              (1)
#define LCRp_Odometer                   (1)
#define LCRp_WMTemp                     (2)
#define LCRp_TempOffset                 (2)
#define LCRp_ProverQty                  (3)
#define LCRp_PulsesPerUnit              (6)
#define LCRp_AuxMult                    (3)
#define LCRp_LinearFlowRate             (2)
#define LCRp_LinearError                (3)
#define LCRp_LinearProverQty            (3)
#define LCRp_CompensationParam          (0)
#define LCRp_BaseTemp                   (1)
#define LCRp_PricePerUnit               (4)
#define LCRp_TaxPerUnit                 (4)
#define LCRp_PercentTax                 (4)
#define LCRp_TotalTaxPerUnit            (2)
#define LCRp_TotalPercentTax            (2)
#define LCRp_SupplyVoltage              (1)
#define LCRp_R100_0                     (1)
#define LCRp_R128_6                     (1)
#define LCRp_RTDSlope                   (6)
#define LCRp_RTDOffset                  (3)
#define LCRp_SubTotalCost               (2)
#define LCRp_TotalTax                   (2)
#define LCRp_TotalCost                  (2)
#define LCRp_MaxPressure                (1)
#define LCRp_ShutdownPressure           (1)

// LCR list type indexes.

// POS Tax Types.                       (LCRT_LIST600+70)

#define LCRL_POSTaxTypeUnused           (0x00000000UL)
#define LCRL_POSTaxTypePercent          (0x00000001UL)
#define LCRL_POSTaxTypePerUnit          (0x00000002UL)
#define LCRL_POSTaxTypeTaxOnTax         (0xFFFFFFFCUL)

// Delivery Screen.                     (LCRT_LIST600+20)

#define LCRL_ScreenUser                 (0)
#define LCRL_ScreenPumpAndPrint         (1)
#define LCRL_ScreenPreset               (2)
#define LCRL_ScreenAviation             (3)
#define LCRL_ScreenPOS                  (4)

#define LCR_NUM_DeliveryScreens         (5)

// Miscellaneous Charge Number.         (LCRT_LIST600+21)

#define LCRL_POSMiscCharge1             (0)
#define LCRL_POSMiscCharge2             (1)
#define LCRL_POSMiscCharge3             (2)
#define LCRL_POSMiscCharge4             (3)
#define LCRL_POSMiscCharge5             (4)
#define LCRL_POSMiscCharge6             (5)
#define LCRL_POSMiscCharge7             (6)
#define LCRL_POSMiscCharge8             (7)
#define LCRL_POSMiscCharge9             (8)
#define LCRL_POSMiscCharge10            (9)
#define LCRL_POSMiscCharge11            (10)
#define LCRL_POSMiscCharge12            (11)
#define LCRL_POSMiscCharge13            (12)
#define LCRL_POSMiscCharge14            (13)
#define LCRL_POSMiscCharge15            (14)
#define LCRL_POSMiscCharge16            (15)

#define LCR_NUM_POSMiscCharges          (16)

// Miscellaneous Charge Tax Type.       (LCRT_LIST600+22)

//      LCRL_POSTaxType1 defined below: (0)
//      LCRL_POSTaxType1 defined below: (1)
//      LCRL_POSTaxType1 defined below: (2)
//      LCRL_POSTaxType1 defined below: (3)
//      LCRL_POSTaxType1 defined below: (4)
//      LCRL_POSTaxType1 defined below: (5)
//      LCRL_POSTaxType1 defined below: (6)
//      LCRL_POSTaxType1 defined below: (7)
//      LCRL_POSTaxType1 defined below: (8)
//      LCRL_POSTaxType1 defined below: (9)
//      LCRL_POSTaxType1 defined below: (10)
//      LCRL_POSTaxType1 defined below: (11)
//      LCRL_POSTaxType1 defined below: (12)
//      LCRL_POSTaxType1 defined below: (13)
//      LCRL_POSTaxType1 defined below: (14)
//      LCRL_POSTaxType1 defined below: (15)
#define LCRL_POSTaxNone                 (16)
#define LCRL_POSTaxSameAsProduct        (17)

#define LCR_NUM_POSMiscChargeTaxTypes   (18)

// Lock Discounts.                      (LCRT_LIST600+23)
// Same as LCRT_LIST600+45

// Lock Price.                          (LCRT_LIST600+24)
// Same as LCRT_LIST600+45

// Save New Price.                      (LCRT_LIST600+25)
// Same as LCRT_LIST600+45

// Allow New Delivery Prices.           (LCRT_LIST600+26)
// Same as LCRT_LIST600+45

// Product Cash Discount.               (LCRT_LIST600+27)
// Same as LCRT_LIST600+34

// Product Volume Discount.             (LCRT_LIST600+28)
// Same as LCRT_LIST600+37

// Product Calibration.                 (LCRT_LIST600+30)
// Same as LCRT_LIST600+40

// Product Tax Type.                    (LCRT_LIST600+31)
// Same as LCRT_LIST600+32

// Tax Types.                           (LCRT_LIST600+32)

#define LCRL_POSTaxType1                (0)
#define LCRL_POSTaxType2                (1)
#define LCRL_POSTaxType3                (2)
#define LCRL_POSTaxType4                (3)
#define LCRL_POSTaxType5                (4)
#define LCRL_POSTaxType6                (5)
#define LCRL_POSTaxType7                (6)
#define LCRL_POSTaxType8                (7)
#define LCRL_POSTaxType9                (8)
#define LCRL_POSTaxType10               (9)
#define LCRL_POSTaxType11               (10)
#define LCRL_POSTaxType12               (11)
#define LCRL_POSTaxType13               (12)
#define LCRL_POSTaxType14               (13)
#define LCRL_POSTaxType15               (14)
#define LCRL_POSTaxType16               (15)

#define LCR_NUM_POSTaxTypes             (16)

// Tax Categories.                      (LCRT_LIST600+33)

#define LCRL_POSTaxCategoryA            (0)
#define LCRL_POSTaxCategoryB            (1)
#define LCRL_POSTaxCategoryC            (2)
#define LCRL_POSTaxCategoryD            (3)
#define LCRL_POSTaxCategoryE            (4)
#define LCRL_POSTaxCategoryF            (5)
#define LCRL_POSTaxCategoryG            (6)
#define LCRL_POSTaxCategoryH            (7)
#define LCRL_POSTaxCategoryI            (8)
#define LCRL_POSTaxCategoryJ            (9)

#define LCR_NUM_POSTaxCategories        (10)

// Cash Discount Numbers.               (LCRT_LIST600+34)

#define LCRL_POSCashDiscount1           (0)
#define LCRL_POSCashDiscount2           (1)
#define LCRL_POSCashDiscount3           (2)
#define LCRL_POSCashDiscount4           (3)
#define LCRL_POSCashDiscount5           (4)
#define LCRL_POSCashDiscount6           (5)
#define LCRL_POSCashDiscount7           (6)
#define LCRL_POSCashDiscount8           (7)
#define LCRL_POSCashDiscount9           (8)
#define LCRL_POSCashDiscount10          (9)
#define LCRL_POSCashDiscount11          (10)
#define LCRL_POSCashDiscount12          (11)
#define LCRL_POSCashDiscount13          (12)
#define LCRL_POSCashDiscount14          (13)
#define LCRL_POSCashDiscount15          (14)
#define LCRL_POSCashDiscount16          (15)

#define LCR_NUM_POSCashDiscounts        (16)

// Cash Discount Intervals.             (LCRT_LIST600+35)

#define LCRL_POSCashInterval1           (0)
#define LCRL_POSCashInterval2           (1)
#define LCRL_POSCashInterval3           (2)

#define LCR_NUM_POSCashIntervals        (3)

// Apply Cash Discounts.                (LCRT_LIST600+36)

#define LCRL_POSApplyBeforeTax          (0)
#define LCRL_POSApplyAfterTax           (1)

#define LCR_NUM_POSApplyDiscounts       (2)

// Volume Discount Number.              (LCRT_LIST600+37)

#define LCRL_POSVolumeDiscount1         (0)
#define LCRL_POSVolumeDiscount2         (1)
#define LCRL_POSVolumeDiscount3         (2)
#define LCRL_POSVolumeDiscount4         (3)
#define LCRL_POSVolumeDiscount5         (4)
#define LCRL_POSVolumeDiscount6         (5)
#define LCRL_POSVolumeDiscount7         (6)
#define LCRL_POSVolumeDiscount8         (7)
#define LCRL_POSVolumeDiscount9         (8)
#define LCRL_POSVolumeDiscount10        (9)
#define LCRL_POSVolumeDiscount11        (10)
#define LCRL_POSVolumeDiscount12        (11)
#define LCRL_POSVolumeDiscount13        (12)
#define LCRL_POSVolumeDiscount14        (13)
#define LCRL_POSVolumeDiscount15        (14)
#define LCRL_POSVolumeDiscount16        (15)

#define LCR_NUM_POSVolumeDiscounts      (16)

// Volume Discount Intervals.           (LCRT_LIST600+38)

#define LCRL_POSVolumeInterval1         (0)
#define LCRL_POSVolumeInterval2         (1)
#define LCRL_POSVolumeInterval3         (2)

#define LCR_NUM_POSVolumeIntervals      (3)

// Apply Volume Discounts.              (LCRT_LIST600+39)
// Same as LCRT_LIST600+36

// Product numbers.                     (LCRT_LIST+0 or LCRT_LIST600+40)

#define LCRL_PRODUCT1                   (0)
#define LCRL_PRODUCT2                   (1)
#define LCRL_PRODUCT3                   (2)
#define LCRL_PRODUCT4                   (3)

#define LCR_NUM_PRODUCTS                (4)

#define LCRL_PRODUCT5                   (4)
#define LCRL_PRODUCT6                   (5)
#define LCRL_PRODUCT7                   (6)
#define LCRL_PRODUCT8                   (7)
#define LCRL_PRODUCT9                   (8)
#define LCRL_PRODUCT10                  (9)
#define LCRL_PRODUCT11                  (10)
#define LCRL_PRODUCT12                  (11)
#define LCRL_PRODUCT13                  (12)
#define LCRL_PRODUCT14                  (13)
#define LCRL_PRODUCT15                  (14)
#define LCRL_PRODUCT16                  (15)

#define LCR_NUM_BASE_PRODUCTS           (4)
#define LCR_NUM_POS_PRODUCTS            (16)

#define LCR_NUM_LCR1000_PRODUCTS        (16)

// Preset types allowed.                (LCRT_LIST+1 or LCRT_LIST600+41)

#define LCRL_PRESETS_NONE               (0)
#define LCRL_PRESETS_GROSS              (1)
#define LCRL_PRESETS_NET                (2)
#define LCRL_PRESETS_BOTH               (3)

#define LCR_NUM_PRESETS_ALLOWED         (4)

// Product types.                       (LCRT_LIST+2 or LCRT_LIST600+42)

#define LCRL_AMMONIA                    (0)
#define LCRL_AVIATION                   (1)
#define LCRL_DISTILLATE                 (2)
#define LCRL_GASOLINE                   (3)
#define LCRL_METHANOL                   (4)
#define LCRL_LPG                        (5)
#define LCRL_LUBE_OIL                   (6)
#define LCRL_NO_PRODUCT_TYPE            (7)

#define LCR_NUM_PRODUCT_TYPES           (8)

// Date format.                         (LCRT_LIST+3 or LCRT_LIST600+43)

#define LCRL_MMDDYY                     (0)
#define LCRL_DDMMYY                     (1)

#define LCR_NUM_DATE_FORMATS            (2)

// Units of measure.                    (LCRT_LIST+4 or LCRT_LIST600+44)

#define LCRL_GALLONS                    (0)
#define LCRL_LITRES                     (1)
#define LCRL_CUBIC_M                    (2)
#define LCRL_LBS                        (3)
#define LCRL_KGS                        (4)
#define LCRL_BARRELS                    (5)
#define LCRL_OTHER                      (6)

#define LCR_NUM_UNITS_OF_MEASURE        (7)

// Yes/No fields.                       (LCRT_LIST+5 or LCRT_LIST600+45)

#define LCRL_YES                        (0)
#define LCRL_NO                         (1)

#define LCR_NUM_YES_NO                  (2)

// Auxiliary units of measure.          (LCRT_LIST+6 or LCRT_LIST600+46)
// Same as LCRT_LIST+4 or LCRT_LIST600+44.

// Preset types.                        (LCRT_LIST+7 or LCRT_LIST600+47)

#define LCRL_CLEAR                      (0)
#define LCRL_MULTIPLE                   (1)
#define LCRL_RETAIN                     (2)
#define LCRL_INVENTORY                  (3)
#define LCRL_REQUIRED                   (4)

#define LCR_NUM_PRESET_TYPES            (5)

// Header line indexes.                 (LCRT_LIST+8 or LCRT_LIST600+48)

#define LCRL_HEADER1                    (0)
#define LCRL_HEADER2                    (1)
#define LCRL_HEADER3                    (2)
#define LCRL_HEADER4                    (3)
#define LCRL_HEADER5                    (4)
#define LCRL_HEADER6                    (5)
#define LCRL_HEADER7                    (6)
#define LCRL_HEADER8                    (7)
#define LCRL_HEADER9                    (8)
#define LCRL_HEADER10                   (9)
#define LCRL_HEADER11                   (10)
#define LCRL_HEADER12                   (11)

#define LCR_NUM_HEADER_LINES            (12)

// Pulse outputs.                       (LCRT_LIST+9 or LCRT_LIST600+49)

#define LCRL_RISING                     (0)
#define LCRL_FALLING                    (1)

#define LCR_NUM_PULSE_EDGES             (2)

// Temperature units.                   (LCRT_LIST+10 or LCRT_LIST600+50)

#define LCRL_DEG_C                      (0)
#define LCRL_DEG_F                      (1)

#define LCR_NUM_TEMPERATURE_SCALES      (2)

// Rate base.                           (LCRT_LIST+11 or LCRT_LIST600+51)

#define LCRL_SECONDS                    (0)
#define LCRL_MINUTES                    (1)
#define LCRL_HOURS                      (2)

#define LCR_NUM_RATE_BASES              (3)

// Linearization points.                (LCRT_LIST+12 or LCRT_LIST600+52)

#define LCRL_LINEAR_POINT1              (0)
#define LCRL_LINEAR_POINT2              (1)
#define LCRL_LINEAR_POINT3              (2)
#define LCRL_LINEAR_POINT4              (3)
#define LCRL_LINEAR_POINT5              (4)
#define LCRL_LINEAR_POINT6              (5)
#define LCRL_LINEAR_POINT7              (6)
#define LCRL_LINEAR_POINT8              (7)
#define LCRL_LINEAR_POINT9              (8)
#define LCRL_LINEAR_POINT10             (9)

#define LCR_NUM_LINEAR_POINTS           (10)

#define LCRL_LINEAR_POINT11             (10)
#define LCRL_LINEAR_POINT12             (11)
#define LCRL_LINEAR_POINT13             (12)
#define LCRL_LINEAR_POINT14             (13)
#define LCRL_LINEAR_POINT15             (14)
#define LCRL_LINEAR_POINT16             (15)

#define LCR_NUM_IO_LINEAR_POINTS        (16)

// Security.                            (LCRT_LIST+13 or LCRT_LIST600+53)

#define LCRL_LOCKED                     (0)
#define LCRL_UNLOCKED                   (1)

#define LCR_NUM_SECURITY_STATES         (2)

// Number of decimal places.            (LCRT_LIST+14 or LCRT_LIST600+54)

#define LCRL_HUNDREDTHS                 (0)
#define LCRL_TENTHS                     (1)
#define LCRL_WHOLE                      (2)

#define LCR_NUM_DECIMALS                (3)

// Compensation types.                  (LCRT_LIST+15 or LCRT_LIST600+55)

#define LCRL_COMP_TYPE_NONE             (0)
#define LCRL_LINEAR_F                   (1)
#define LCRL_LINEAR_C                   (2)
#define LCRL_TABLE_24                   (3)
#define LCRL_TABLE_54                   (4)
#define LCRL_TABLE_6B                   (5)
#define LCRL_TABLE_54B                  (6)
#define LCRL_TABLE_54C                  (7)
#define LCRL_TABLE_54D                  (8)
#define LCRL_NH3                        (9)

#define LCR_NUM_COMP_TYPES              (10)

// Diagnostic messages.                 (LCRT_LIST+16 or LCRT_LIST600+56)

#define LCRL_ROM_CHECKSUM               (0)
#define LCRL_TEMPERATURE                (1)
#define LCRL_VCF_TYPE                   (2)
#define LCRL_VCF_PARAMETER              (3)
#define LCRL_VCF_DOMAIN                 (4)
#define LCRL_METER_CALIBRATION          (5)
#define LCRL_PULSER                     (6)
#define LCRL_PRESET_STOP                (7)
#define LCRL_NO_FLOW_STOP               (8)
#define LCRL_STOP_REQUESTED             (9)
#define LCRL_DELIVERY_END_REQUEST       (10)
#define LCRL_POWER_FAIL                 (11)
#define LCRL_PRESET                     (12)
#define LCRL_NO_TERMINAL                (13)
#define LCRL_PRINTER_NOT_READY          (14)
#define LCRL_DATA_ACCESS_ERROR          (15)
#define LCRL_DELIVERY_TICKET_PENDING    (16)
#define LCRL_SHIFT_TICKET_PENDING       (17)
#define LCRL_FLOW_ACTIVE                (18)
#define LCRL_DELIVERY_ACTIVE            (19)
#define LCRL_GROSS_PRESET_ACTIVE        (20)
#define LCRL_NET_PRESET_ACTIVE          (21)
#define LCRL_GROSS_PRESET_STOP          (22)
#define LCRL_NET_PRESET_STOP            (23)
#define LCRL_TVC_ACTIVE                 (24)
#define LCRL_S1_CLOSE                   (25)
#define LCRL_INIT_WARNING               (28)
#define LCRL_END_OF_LIST                (29)

// Compensation parameters.             (LCRT_LIST+17 or LCRT_LIST600+57)

#define LCRL_COMP_PARAM_NONE            (0)
#define LCRL_COEFFICIENT1               (1)
#define LCRL_COEFFICIENT2               (2)
#define LCRL_SG_60                      (3)
#define LCRL_DENSITY_KGPL               (4)
#define LCRL_API_GRAVITY                (5)
#define LCRL_DENSITY_KGPCUM1            (6)
#define LCRL_COEFFICIENT3               (7)
#define LCRL_DENSITY_KGPCUM2            (8)
#define LCRL_NO_COMP_PARAM              (9)

// LCR_NUM_COMP_TYPES                   Same as LCRT_LIST+15 or LCRT_LIST600+55

// Compensation temperatures.           (LCRT_LIST+18 or LCRT_LIST600+58)

#define LCRL_NO_COMP_TEMP               (0)
#define LCRL_DEG_F1                     (1)
#define LCRL_DEG_C1                     (2)
#define LCRL_DEG_F2                     (3)
#define LCRL_DEG_C2                     (4)
#define LCRL_DEG_F3                     (5)
#define LCRL_DEG_C3                     (6)
#define LCRL_DEG_C4                     (7)
#define LCRL_DEG_C5                     (8)
#define LCRL_DEG_C6                     (9)

// LCR_NUM_COMP_TYPES                   Same as LCRT_LIST+15 or LCRT_LIST600+55

// Printer.                             (LCRT_LIST+19 or LCRT_LIST600+59)

#define LCRL_EPSON200ROLL               (0)
#define LCRL_EPSONNEWFONTB              (0)

#define LCRL_EPSON290SLIP               (1)
#define LCRL_EPSONNEWFONTA              (1)

#define LCRL_EPSON295SLIP               (2)
#define LCRL_EPSONOLDFONTA              (2)

#define LCRL_EPSON300ROLL               (3)
#define LCRL_EPSONOLDFONTB              (3)

#define LCRL_OKIDATAML184T              (4)

#define LCRL_AXIOHMBLASTER              (5)
#define LCRL_BLASTER                    (5)

#define LCR_NUM_PRINTER_TYPES           (6)

// Residuals.                           (LCRT_LIST+20 or LCRT_LIST600+60)

#define LCRL_ROUND                      (0)
#define LCRL_TRUNCATE                   (1)

#define LCR_NUM_RESIDUALS               (2)

// Flow direction.                      (LCRT_LIST+21 or LCRT_LIST600+61)

#define LCRL_RIGHT                      (0)
#define LCRL_LEFT                       (1)

#define LCR_NUM_FLOW_DIRECTIONS         (2)

// Auxiliary 1 options.                 (LCRT_LIST+22 or LCRT_LIST600+62)

#define LCRL_AUX1_ON_DURING_DELIVERY    (0)
#define LCRL_AUX1_OFF                   (1)
#define LCRL_AUX1_ON                    (2)
#define LCRL_AUX1_MONITOR_FLOW_RATE     (3)

#define LCR_NUM_AUX1_COMMANDS           (4)

#define LCRL_AUX1_TOGGLE_FLOW_RATE      (4)
#define LCR600_NUM_AUX1_COMMANDS        (5)

// Auxiliary 2 options.                 (LCRT_LIST+23 or LCRT_LIST600+63)

#define LCRL_AUX2_FLOW_DIRECTION        (0)
#define LCRL_AUX2_ON_DURING_DELIVERY    (1)
#define LCRL_AUX2_OFF                   (2)
#define LCRL_AUX2_ON                    (3)

#define LCR_NUM_AUX2_COMMANDS           (4)

#define LCRL_AUX2_TOGGLE_FLOW_RATE      (4)
#define LCR600_NUM_AUX2_COMMANDS        (5)

// Reset options.                       (LCRT_LIST+24 or LCRT_LIST600+64)

#define LCRL_CLEAR_ALL                  (0)
//      LCRL_NO defined above:          (1)
#define LCRL_REBUILD                    (2)

#define LCR_NUM_RESET_COMMANDS          (3)

// Linearization state.                 (LCRT_LIST+25 or LCRT_LIST600+65)

#define LCRL_APPLIED                    (0)
#define LCRL_SETUP                      (1)

#define LCR_NUM_LINEARIZE_OPTIONS       (2)

// Password usage.                      (LCRT_LIST+26 or LCRT_LIST600+66)

#define LCRL_PASSWORD_NONE              (0)
#define LCRL_PASSWORD_DELIVERY          (1)
#define LCRL_PASSWORD_SHIFT             (2)

#define LCR_NUM_PASSWORD_OPTIONS        (3)

// Customer number usage.               (LCRT_LIST+27 or LCRT_LIST600+67)

#define LCRL_CUSTOMER_NONE              (0)
#define LCRL_CUSTOMER_DELIVERY          (1)

#define LCR_NUM_CUSTOMER_OPTIONS        (2)

//===========================
// LCR-1000 list definitions.
//===========================

// Pulser type.

#define LCRL_NO_PULSERS                 (0)
#define LCRL_ALL_SINGLE_CHANNEL         (1)
#define LCRL_ONE_DUAL_CHANNEL           (2)
#define LCRL_TWO_DUAL_CHANNEL           (3)
#define LCRL_ONE_TRIPLE_CHANNEL         (4)

#define LCR_NUM_PULSER_TYPES            (5)

// LCR A to D code word definitions.

#define LCRA_R100_0                     (0x01)
#define LCRA_R128_6                     (0x02)
#define LCRA_RTD_SLOPE                  (0x04)
#define LCRA_RTD_OFFSET                 (0x08)
#define LCRA_VOLT_12                    (0x10)
#define LCRA_VOLT_16                    (0x20)
#define LCRA_VOLT_SLOPE                 (0x40)
#define LCRA_VOLT_OFFSET                (0x80)

// LCR field number definitions.
//  NOTE:   The following naming convention for the field numbers seemed like a
//          good idea at the time. However, as with lots of good intentions,
//          things went awry when it was decided that a few of the fields needed to
//          have their access security levels changed. For backward compatibility
//          with developers who have used these definitions, I have opted to not
//          change the names to accurately reflect the protection level of each
//          field. Most are still accurate, but I apologize to those who were
//          relying on the name suffix to define the edit security level. In the
//          future, I hope to not be so short sighted.
//
//          To get the actual security level, please access the values found in
//          the FieldSecurity table.
//
//      LCRF_*_AE = Field is always editable.
//      LCRF_*_DL = Field is editable at delivery security level.
//      LCRF_*_FL = Field is editable at factory security level.
//      LCRF_*_NE = Field is never editable.
//      LCRF_*_PL = Field is editable while a delivery is paused.
//      LCRF_*_UL = Field is editable when LCR is unlocked.
//      LCRF_*_WM = Field is editable at weights and measures security level.

#define LCRF_ProductNumber_DL           (0)
#define LCRF_ProductCode_DL             (1)
#define LCRF_GrossQty_NE                (2)
#define LCRF_NetQty_NE                  (3)
#define LCRF_FlowRate_NE                (4)
#define LCRF_GrossPreset_PL             (5)
#define LCRF_NetPreset_PL               (6)
#define LCRF_Temp_NE                    (7)
#define LCRF_Residual_WM                (8)
#define LCRF_PulsesPerDistance_UL       (9)
#define LCRF_CalibDistance_UL           (10)
#define LCRF_ProductDescriptor_DL       (11)
#define LCRF_Odometer_UL                (12)
#define LCRF_ShiftGross_NE              (13)
#define LCRF_ShiftNet_NE                (14)
#define LCRF_ShiftDeliveries_NE         (15)
#define LCRF_ClearShift_DL              (16)
#define LCRF_GrossTotal_WM              (17)
#define LCRF_NetTotal_WM                (18)
#define LCRF_DateFormat_UL              (19)
#define LCRF_Date_UL                    (20)
#define LCRF_Time_UL                    (21)
#define LCRF_SaleNumber_WM              (22)
#define LCRF_TicketNumber_WM            (23)
#define LCRF_UnitID_UL                  (24)
#define LCRF_NoFlowTimer_DL             (25)
#define LCRF_S1Close_WM                 (26)
#define LCRF_PresetType_DL              (27)
#define LCRF_PulseOutputEdge_UL         (28)
#define LCRF_Header_AE                  (29)
#define LCRF_TicketHeaderLine_UL        (30)
#define LCRF_PrintGrossAndParam_WM      (31)
#define LCRF_VolCorrectedMsg_WM         (32)
#define LCRF_Temp_WM                    (33)
#define LCRF_TempOffset_WM              (34)
#define LCRF_TempScale_WM               (35)
#define LCRF_MeterID_WM                 (36)
#define LCRF_TicketRequired_WM          (37)
#define LCRF_QtyUnits_WM                (38)
#define LCRF_Decimals_WM                (39)
#define LCRF_FlowDirection_WM           (40)
#define LCRF_TimeUnit_WM                (41)
#define LCRF_CalibrationEvent_NE        (42)
#define LCRF_ConfigurationEvent_NE      (43)
#define LCRF_GrossCount_NE              (44)
#define LCRF_NetCount_NE                (45)
#define LCRF_ProverQty_WM               (46)
#define LCRF_PulsesPerUnit_WM           (47)
#define LCRF_AuxMult_WM                 (48)
#define LCRF_AuxUnit_WM                 (49)
#define LCRF_CalibrationNumber_NE       (50)
#define LCRF_LinearPoint_AE             (51)
#define LCRF_LinearFlowRate_WM          (52)
#define LCRF_LinearError_WM             (53)
#define LCRF_LinearProverQty_WM         (54)
#define LCRF_Linearize_WM               (55)
#define LCRF_Printer_WM                 (56)
#define LCRF_CompensationType_WM        (57)
#define LCRF_CompensationParam_WM       (58)
#define LCRF_BaseTemp_WM                (59)
#define LCRF_Software_NE                (60)
#define LCRF_PricePerUnit_DL            (61)
#define LCRF_TaxPerUnit_DL              (62)
#define LCRF_PercentTax_DL              (63)
#define LCRF_DiagnosticMessages_AE      (64)
#define LCRF_TotalTaxPerUnit_NE         (65)
#define LCRF_TotalPercentTax_NE         (66)
#define LCRF_ADCCode_NE                 (67)
#define LCRF_SupplyVoltage_NE           (68)
#define LCRF_PulserReversals_NE         (69)
#define LCRF_ShiftStart_NE              (70)
#define LCRF_AuxQty_NE                  (71)
#define LCRF_UserKey_DL                 (72)
#define LCRF_Security_UL                (73)
#define LCRF_FactoryKey_AE              (74)
#define LCRF_R100_0_FL                  (75)
#define LCRF_R128_6_FL                  (76)
#define LCRF_RawADC_NE                  (77)
#define LCRF_RTDSlope_FL                (78)
#define LCRF_RTDOffset_FL               (79)
#define LCRF_SerialID_FL                (80)
#define LCRF_UserSetKey_FL              (81)
#define LCRF_LCRReset_FL                (82)
#define LCRF_CompParamType_NE           (83)
#define LCRF_CompTempType_NE            (84)
#define LCRF_PresetsAllowed_DL          (85)
#define LCRF_Aux1_DL                    (86)
#define LCRF_Aux2_DL                    (87)
#define LCRF_DefaultProduct_NE          (88)
#define LCRF_DeliveryStart_NE           (89)
#define LCRF_DeliveryFinish_NE          (90)
#define LCRF_LastCalibrated_NE          (91)
#define LCRF_GrossRemaining_NE          (92)
#define LCRF_NetRemaining_NE            (93)
#define LCRF_ProductType_WM             (94)
#define LCRF_SubTotalCost_NE            (95)
#define LCRF_TotalTax_NE                (96)
#define LCRF_TotalCost_NE               (97)
#define LCRF_Ticket_NE                  (98)
#define LCRF_Language_NE                (99)

#define LCRF_PreviousGross              (100)
#define LCRF_PreviousNet                (101)
#define LCRF_ShiftPassword              (105)
#define LCRF_CustomerID                 (106)
#define LCRF_UsePassword                (107)
#define LCRF_UseCustomer                (108)
#define LCRF_MaxPressure                (109)
#define LCRF_MaxPressureFlowRate        (110)
#define LCRF_ShutdownPressure           (111)

// LCR 600 Fields.

#define LCRF_DBMNode                    (112)
#define LCRF_FSNode                     (113)
#define LCRF_FSPort                     (114)
#define LCRF_FSBaud                     (115)
#define LCRF_FSRetries                  (116)
#define LCRF_FSTxEnable                 (117)
#define LCRF_FSTimeout                  (118)

#define LCRF_PulseOutputMultiplier      (119)
#define LCRF_Aux1FlowRateToggle         (120)
#define LCRF_Aux2FlowRateToggle         (121)
#define LCRF_DeliveryScreen             (122)

#define LCRF_BootloaderRev              (123)

#define LCRF_ActivateFeatureKey         (124)
#define LCRF_FeaturesActivated          (125)

#define LCPR_AvgFlowRate                (126)
#define LCPR_CompFlowRate               (127)
#define LCRF_AvgFlowBase                (128)
#define LCRF_AvgTemp                    (129)
#define LCRF_AvgDensity                 (130)
#define LCRF_DensityUnits               (131)
#define LCRF_MaxFlowRate                (132)

#define LCR_NUM_FIELDS                  (133)

// LCR-1000 Fields.

#define LCRF_BASE_LCR1000               (144)

#define LCRF_ActiveIOBoard              (LCRF_BASE_LCR1000 +  0)
#define LCRF_IOBoardUID                 (LCRF_BASE_LCR1000 +  1)
#define LCRF_IOBoardName                (LCRF_BASE_LCR1000 +  2)
#define LCRF_PulserType                 (LCRF_BASE_LCR1000 +  3)
#define LCRF_EnableNoFlowTimer          (LCRF_BASE_LCR1000 +  4)

#define NUM_LCR1000_FIELDS              (5)

#define LCR_NUM_LCR1000_FIELDS          (LCRF_BASE_LCR1000 + NUM_LCR1000_FIELDS)

// LCR 600 POS Fields.

#define LCRF_BASE_POS_FIELD             (192)

#define LCRF_PricePreset                (LCRF_BASE_POS_FIELD +  0)
#define LCRF_POSLockDiscounts           (LCRF_BASE_POS_FIELD +  1)
#define LCRF_POSLockPrice               (LCRF_BASE_POS_FIELD +  2)
#define LCRF_POSSaveNewPrice            (LCRF_BASE_POS_FIELD +  3)
#define LCRF_POSAllowNewDeliveryPrice   (LCRF_BASE_POS_FIELD +  4)
#define LCRF_POSProductNumber           (LCRF_BASE_POS_FIELD +  5)
#define LCRF_POSProductName             (LCRF_BASE_POS_FIELD +  6)
#define LCRF_POSProductCode             (LCRF_BASE_POS_FIELD +  7)
#define LCRF_POSProductType             (LCRF_BASE_POS_FIELD +  8)
#define LCRF_POSProductCalibration      (LCRF_BASE_POS_FIELD +  9)
#define LCRF_POSProductPrice            (LCRF_BASE_POS_FIELD + 10)
#define LCRF_POSProductTax              (LCRF_BASE_POS_FIELD + 11)
#define LCRF_POSProductCashDiscount     (LCRF_BASE_POS_FIELD + 12)
#define LCRF_POSProductVolumeDiscount   (LCRF_BASE_POS_FIELD + 13)
#define LCRF_POSTaxNumber               (LCRF_BASE_POS_FIELD + 14)
#define LCRF_POSTaxName                 (LCRF_BASE_POS_FIELD + 15)
#define LCRF_POSTaxCategory             (LCRF_BASE_POS_FIELD + 16)
#define LCRF_POSTaxValue                (LCRF_BASE_POS_FIELD + 17)
#define LCRF_POSTaxType                 (LCRF_BASE_POS_FIELD + 18)
#define LCRF_POSTaxHeader               (LCRF_BASE_POS_FIELD + 19)
#define LCRF_POSCashDiscountNumber      (LCRF_BASE_POS_FIELD + 20)
#define LCRF_POSCashDiscountName        (LCRF_BASE_POS_FIELD + 21)
#define LCRF_POSCashDiscountInterval    (LCRF_BASE_POS_FIELD + 22)
#define LCRF_POSCashDiscountPercent     (LCRF_BASE_POS_FIELD + 23)
#define LCRF_POSCashDiscountPerUnit     (LCRF_BASE_POS_FIELD + 24)
#define LCRF_POSCashDiscountDays        (LCRF_BASE_POS_FIELD + 25)
#define LCRF_POSCashDiscountApply       (LCRF_BASE_POS_FIELD + 26)
#define LCRF_POSVolumeDiscountNumber    (LCRF_BASE_POS_FIELD + 27)
#define LCRF_POSVolumeDiscountName      (LCRF_BASE_POS_FIELD + 28)
#define LCRF_POSVolumeDiscountInterval  (LCRF_BASE_POS_FIELD + 29)
#define LCRF_POSVolumeDiscountPercent   (LCRF_BASE_POS_FIELD + 30)
#define LCRF_POSVolumeDiscountPerUnit   (LCRF_BASE_POS_FIELD + 31)
#define LCRF_POSVolumeDiscountAmount    (LCRF_BASE_POS_FIELD + 32)
#define LCRF_POSVolumeDiscountApply     (LCRF_BASE_POS_FIELD + 33)
#define LCRF_POSSubTotalDue             (LCRF_BASE_POS_FIELD + 34)
#define LCRF_POSPreVolumeDiscount       (LCRF_BASE_POS_FIELD + 35)
#define LCRF_POSTaxDue                  (LCRF_BASE_POS_FIELD + 36)
#define LCRF_POSPostVolumeDiscount      (LCRF_BASE_POS_FIELD + 37)
#define LCRF_POSTotalDue                (LCRF_BASE_POS_FIELD + 38)
#define LCRF_POSCashDiscount            (LCRF_BASE_POS_FIELD + 39)
#define LCRF_POSDiscountDate            (LCRF_BASE_POS_FIELD + 40)
#define LCRF_POSMiscChargeNumber        (LCRF_BASE_POS_FIELD + 41)
#define LCRF_POSMiscChargeName          (LCRF_BASE_POS_FIELD + 42)
#define LCRF_POSMiscChargeTax           (LCRF_BASE_POS_FIELD + 43)
#define LCRF_POSMiscChargePrice         (LCRF_BASE_POS_FIELD + 44)
#define LCRF_POSMiscChargeQty           (LCRF_BASE_POS_FIELD + 45)
#define LCRF_POSMiscChargeSubTotal      (LCRF_BASE_POS_FIELD + 46)
#define LCRF_POSMiscChargeTaxDue        (LCRF_BASE_POS_FIELD + 47)
#define LCRF_POSMiscChargeTotal         (LCRF_BASE_POS_FIELD + 48)

#define LCR_NUM_POS_FIELDS              (49)

#define LCR600_NUM_FIELDS               (LCRF_BASE_POS_FIELD + LCR_NUM_POS_FIELDS)

#define LCR_MAX_FIELDS                  (256)

// LCR command code definitions.

#define LCRC_RUN                        (0)                             // run command
#define LCRC_STOP                       (1)                             // stop command
#define LCRC_END_DELIVERY               (2)                             // end delivery command
#define LCRC_AUX                        (3)                             // auxiliary command
#define LCRC_SHIFT                      (4)                             // shift command
#define LCRC_CALIBRATE                  (5)                             // calibrate command
#define LCRC_PRINT                      (6)                             // print command

#define LCR_NUM_COMMANDS                (7)                             // number of LCR commands

// LCR status masks.
//      LCRSc_* = Delivery Code
//      LCRSd_* = Delivery Status
//      LCRSm_* = Machine Status
//      LCRSp_* = Printer Status

#define LCRSc_DEL_TICKET_PENDING        (0x0001)
#define LCRSc_SHIFT_TICKET_PENDING      (0x0002)
#define LCRSc_FLOW_ACTIVE               (0x0004)
#define LCRSc_DELIVERY_ACTIVE           (0x0008)
#define LCRSc_GROSS_PRESET_ACTIVE       (0x0010)
#define LCRSc_NET_PRESET_ACTIVE         (0x0020)
#define LCRSc_GROSS_PRESET_STOP         (0x0040)
#define LCRSc_NET_PRESET_STOP           (0x0080)
#define LCRSc_TVC_ACTIVE                (0x0100)
#define LCRSc_S1_CLOSE_REACHED          (0x0200)
#define LCRSc_BEGIN_DELIVERY            (0x0400)
#define LCRSc_NEW_DELIVERY_QUEUED       (0x0800)
#define LCRSc_INIT_WARNING              (0x1000)
#define LCRSc_CONFIG_EVENT              (0x2000)
#define LCRSc_CALIB_EVENT               (0x4000)

#define LCRSd_ROM_CHECKSUM              (0x0001)
#define LCRSd_TEMPERATURE               (0x0002)
#define LCRSd_VCF_TYPE                  (0x0004)
#define LCRSd_WATCHDOG                  (0x0004)
#define LCRSd_VCF_PARAMETER             (0x0008)
#define LCRSd_VCF_SETUP                 (0x0008)
#define LCRSd_VCF_DOMAIN                (0x0010)
#define LCRSd_METER_CALIBRATION         (0x0020)
#define LCRSd_PULSER_FAILURE            (0x0040)
#define LCRSd_PRESET_STOP               (0x0080)
#define LCRSd_NO_FLOW_STOP              (0x0100)
#define LCRSd_STOP_REQUEST              (0x0200)
#define LCRSd_DELIVERY_END_REQUEST      (0x0400)
#define LCRSd_POWER_FAIL                (0x0800)
#define LCRSd_PRESET_ERROR              (0x1000)
#define LCRSd_LAPPAD_ERROR              (0x2000)
#define LCRSd_PRINTER_ERROR             (0x4000)
#define LCRSd_DATA_ACCESS_ERROR         (0x8000)

#define LCRSd_FATAL_ERRORS              (0xF87F)

#define LCRSm_UNKNOWN                   (0x07)

#define LCRSm_SWITCH_BETWEEN            (0x00)
#define LCRSm_SWITCH_RUN                (0x01)
#define LCRSm_SWITCH_STOP               (0x02)
#define LCRSm_SWITCH_PRINT              (0x03)
#define LCRSm_SWITCH_SHIFT_PRINT        (0x04)
#define LCRSm_SWITCH_CALIBRATE          (0x05)
#define LCRSm_SWITCH                    (0x07)
#define LCRSm_PRINTING                  (0x08)
#define LCRSm_STATE_RUN                 (0x00)
#define LCRSm_STATE_STOP                (0x10)
#define LCRSm_STATE_END_DELIVERY        (0x20)
#define LCRSm_STATE_AUX                 (0x30)
#define LCRSm_STATE_SHIFT               (0x40)
#define LCRSm_STATE_CALIBRATE           (0x50)
#define LCRSm_STATE_WAIT_NOFLOW         (0x60)
#define LCRSm_STATE                     (0x70)
#define LCRSm_ERROR                     (0x80)

#define LCRSp_DELIVERY_TICKET           (0x01)
#define LCRSp_SHIFT_TICKET              (0x02)
#define LCRSp_DIAGNOSTIC_TICKET         (0x04)
#define LCRSp_USER_PRINT                (0x08)
#define LCRSp_OUT_OF_PAPER              (0x10)                          // reserved
#define LCRSp_OFFLINE                   (0x20)                          // reserved
#define LCRSp_ERROR_WARNING             (0x40)
#define LCRSp_BUSY                      (0x80)

// LCR return code definitions.
//      LCP02Ra_* = Return codes from C-API.
//      LCP02Rd_* = Return codes from LCR device.
//      LCP02Rf_* = Return codes from flash memory access routines.
//      LCP02Rg_* = General purpose return codes.
//      LCP02Rp_* = Return codes from printer routines.
//      LCP02Rt_* = Return codes from temperature sensing device.

#define LCP02Rd_PARAMID                 (0x20)                          // bad parameter identifier return code
#define LCP02Rd_FIELDNUM                (0x21)                          // bad field number return code
#define LCP02Rd_FIELDDATA               (0x22)                          // bad field data
#define LCP02Rd_NOTSET                  (0x23)                          // set not allowed due to mode vs status
#define LCP02Rd_COMMANDNUM              (0x24)                          // bad command number return code
#define LCP02Rd_DEVICEADDR              (0x25)                          // bad device address
#define LCP02Rd_REQUESTQUEUED           (0x26)                          // request is queued
#define LCP02Rd_NOREQUESTQUEUED         (0x27)                          // no request is queued
#define LCP02Rd_REQUESTABORTED          (0x28)                          // request has been aborted
#define LCP02Rd_PREVIOUSREQUEST         (0x29)                          // previous request being processed
#define LCP02Rd_ABORTNOTALLOWED         (0x2A)                          // abort is not allowed
#define LCP02Rd_PARAMBLOCK              (0x2B)                          // bad parameter block number return code
#define LCP02Rd_BAUDRATE                (0x2C)                          // bad baud rate

#define LCP02Rf_START                   (0x30)                          // start operation failed
#define LCP02Rf_NOACK                   (0x31)                          // a shift out operation was not acknowledged
#define LCP02Rf_CRC                     (0x32)                          // CRC error on read data
#define LCP02Rf_PARAMETERS              (0x33)                          // invalid input parameters
#define LCP02Rf_PAGEBOUNDARY            (0x34)                          // page boundary error
#define LCP02Rf_WRITEFAILED             (0x35)                          // write operation failed

#define LCP02Rt_OVERRANGE               (0x40)                          // temperature over range
#define LCP02Rt_UNDERRANGE              (0x41)                          // temperature under range
#define LCP02Rt_VCFTYPE                 (0x43)                          // VCF type error
#define LCP02Rt_PARAMLIMIT              (0x44)                          // table parameter out of limits
#define LCP02Rt_DOMAIN                  (0x45)                          // temperature domain error

#define LCP02Ra_OPEN                    (0x50)                          // NBS card initialization error
#define LCP02Ra_CLOSE                   (0x51)                          // NBS card termination error
#define LCP02Ra_CONNECT                 (0x52)                          // connection failed error
#define LCP02Ra_MSGSIZE                 (0x53)                          // message size error
#define LCP02Ra_DEVICE                  (0x54)                          // invalid device number
#define LCP02Ra_FIELDNUM                (0x55)                          // invalid field number
#define LCP02Ra_FIELDDATA               (0x56)                          // bad field data
#define LCP02Ra_COMMANDNUM              (0x57)                          // invalid command number
#define LCP02Ra_NODEVICES               (0x58)                          // no devices present in network
#define LCP02Ra_VERSION                 (0x59)                          // version mismatch
#define LCP02Ra_PARAMSTRING             (0x5A)                          // bad environment parameter string
#define LCP02Ra_NOREQUESTQUEUED         (0x5B)                          // no request has been queued
#define LCP02Ra_QUEUEDREQUEST           (0x5C)                          // a request is already queued
#define LCP02Ra_REQUESTCODE             (0x5D)                          // invalid request code queued
#define LCP02Ra_NOTOPENED               (0x5E)                          // network communications is not opened
#define LCP02Ra_ALREADYOPENED           (0x5F)                          // network communications already opened

#define LCP02Rp_PRINTERBUSY             (0x60)                          // the printer is busy
#define LCP02Rp_OVERFLOW                (0x61)                          // printer buffer overflow
#define LCP02Rp_TIMEOUT                 (0x62)                          // printer timeout

#define LCP02Rg_FIELDINIT               (0x70)                          // flash variable has not been initialized
#define LCP02Rg_FIELDRANGE              (0x71)                          // new field table data out of range
#define LCP02Rg_NOTREAD                 (0x72)                          // flash structure not yet read
#define LCP02Rg_GROSSNOTACTIVE          (0x73)                          // gross preset is not active
#define LCP02Rg_NETNOTACTIVE            (0x74)                          // net preset is not active
#define LCP02Rg_NEVERSETTABLE           (0x75)                          // field is never settable
#define LCP02Rg_FIELDNOTUSED            (0x76)                          // field is not used
#define LCP02Rg_BADSWITCH               (0x77)                          // bad switch position for requested command
#define LCP02Rg_BADSTATE                (0x78)                          // bad machine state for requested command
#define LCP02Rg_PENDINGTICKET           (0x79)                          // command ignored due to pending ticket
#define LCP02Rg_FLASHPROTECTED          (0x7A)                          // flash is protected via the switch
#define LCP02Rg_DUPLINEARPOINT          (0x7B)                          // duplicate linearization points
#define LCP02Rg_LINEARRANGE             (0x7C)                          // adjacent linear points range error
#define LCP02Rg_ENDOFLIST               (0x7D)                          // end of transactions list
#define LCP02Rg_LOSTRANSACTIONS         (0x7E)                          // transactions were lost in the LCR
#define LCP02Rg_CUSTOMERNOTSET          (0x7F)                          // customer number has not been set

// LCR communications mode bytes.

#define LCRM_NO_WAIT                    (0x00)                          // API will not wait for response
#define LCRM_WAIT                       (0x01)                          // API will wait for response
#define LCRM_ABORT                      (0x02)                          // API will abort request if no response

// LCR security levels.

#define LCRS_PAUSE_LEVEL                (0x00)                          // delivery has been paused
#define LCRS_DELIVERY_LEVEL             (0x01)                          // switch out of calibration, not unlocked, no delivery active
#define LCRS_UNLOCKED_LEVEL             (0x02)                          // switch out of calibration, unlocked, no delivery active
#define LCRS_WM_LEVEL                   (0x03)                          // switch in calibration, no delivery active
#define LCRS_FACTORY_LEVEL              (0x04)                          // user can set factory parameters
#define LCRS_ALWAYS_EDITABLE            (0x08)                          // field is always editable
#define LCRS_NEVER_EDITABLE             (0x09)                          // field is never editable

#define LCRS_DELIVERY_ACTIVE            (0x80)                          // delivery active bit

// Type definitions.

typedef void DelayFunction(void);                                       // "must" delay function prototype
typedef long LCRflowrate;                                               // LCR flow rate type
typedef long LCRvolume;                                                 // LCR volume type
typedef unsigned char LCRlist;                                          // LCR list type

// Structure definitions.

#ifndef LCR_STRUCTURES                                                  // check for previous definitions
#define LCR_STRUCTURES                                                  // ensure only one definition

#pragma pack(1)                                                         // pack on byte boundaries

struct LCRStatus {
    unsigned short DeliveryCode;                                        // delivery code word
    unsigned short DeliveryStatus;                                      // delivery status word
    unsigned char PrinterStatus;                                        // printer status byte
};

struct LCRTransaction {                                                 // transaction record
    float temperature;                                                  // temperature of product at the end of the delivery
    unsigned long customer;                                             // five-digit customer number that received fuel
    long saleNumber;                                                    // sale number
    LCRvolume gross;                                                    // gross volume delivered
    LCRvolume net;                                                      // net volume delivered
    unsigned short status;                                              // delivery status at the end of the delivery
    unsigned char compType;                                             // compensation type used during delivery
    unsigned char dateFormat;                                           // format of the date in the date/time field
    unsigned char decimals;                                             // decimal setting for the volume fields
    unsigned char product;                                              // product number delivered
    unsigned char qtyUnits;                                             // unit of measure for the volume fields
    unsigned char tempScale;                                            // temperature scale used during delivery
    char dateTime[LCRTl_DeliveryFinish+1];                              // date and time the transaction was created
};

#pragma pack()                                                          // revert to default or /Zp packing

#endif

// External reference definition.

#undef LCP02_LIBSPEC
#undef SCOPE

#if defined(eCos)                                                       // check for eCos
    #define WINAPI
#elif defined(_MSC_VER) && (_MSC_VER <= 800)                            // check for DOS
    #define WINAPI
#endif

#if defined(eCos)                                                       // check for eCos
    #define LCP02_LIBSPEC
#elif defined(_MSC_VER) && (_MSC_VER <= 800)                            // check for DOS
    #define LCP02_LIBSPEC
#elif defined(_COMPILING_LCLP02)
    #define LCP02_LIBSPEC __declspec(dllexport)
#else
    #define LCP02_LIBSPEC __declspec(dllimport)
#endif

#if defined(eCos)                                                       // check for eCos
    #define SCOPE extern
#elif defined(_MSC_VER) && (_MSC_VER <= 800)                            // check for DOS
    #define SCOPE extern
#elif defined(__cplusplus)
    #define SCOPE extern "C"
#else
    #define SCOPE extern
#endif

// External variables.

#ifndef LCLP02_EXTERNALS                                                // check for previous declaration
    #define LCLP02_EXTERNALS                                            // ensure only one declaration

    #if defined(_MSC_VER) && (_MSC_VER <= 800)                          // check for DOS
        SCOPE LCP02_LIBSPEC char *verLCxCLP02;
    #else
        SCOPE LCP02_LIBSPEC char *verLCLP02;
    #endif

    SCOPE LCP02_LIBSPEC char FieldSecurity[];
    SCOPE LCP02_LIBSPEC unsigned char FieldType[];
    SCOPE LCP02_LIBSPEC unsigned char FieldWidth[];
    SCOPE LCP02_LIBSPEC unsigned char NumFields;
#endif

// External function prototypes.

#if defined(eCos)                                                       // check for eCos

    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02ActivatePumpAndPrint(unsigned short hLCP02ix, unsigned char device, unsigned char mode);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02AreYouThere(unsigned short hLCP02ix, unsigned char device);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02Close(unsigned short hLCP02ix); 
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02DeleteTransaction(unsigned short hLCP02ix, unsigned char device, unsigned char mode);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02EStop(unsigned short hLCP02ix, unsigned char device, unsigned char mode);
    SCOPE LCP02_LIBSPEC size_t WINAPI LCP02FieldSize(unsigned short hLCP02ix, unsigned char fieldNum);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02GetDeliveryStatus(unsigned short hLCP02ix, unsigned char device, struct LCRStatus *status, unsigned char *devStatus, unsigned char mode);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02GetField(unsigned short hLCP02ix, unsigned char device, unsigned char fieldNum, void *fieldData, unsigned char *devStatus, unsigned char mode);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02GetFirstTicketLine(unsigned short hLCP02ix, unsigned char device, unsigned char *ticketLinePtr, unsigned char mode);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02GetNextTicketLine(unsigned short hLCP02ix, unsigned char device, unsigned char *ticketLinePtr, unsigned char mode);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02GetSecurityLevel(unsigned short hLCP02ix, unsigned char device, char *securityLevel, unsigned char mode);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02GetStatus(unsigned short hLCP02ix, unsigned char device, struct LCRStatus *status, unsigned char *devStatus, unsigned char mode);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02GetTransaction(unsigned short hLCP02ix, unsigned char device, struct LCRTransaction *transPtr, unsigned char mode);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02GetVersion(unsigned short hLCP02ix, unsigned char device, unsigned short *version, unsigned char mode);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02IssueCommand(unsigned short hLCP02ix, unsigned char device, unsigned char command, unsigned char mode);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02MustGetDeliveryStatus(unsigned short hLCP02ix, unsigned char device, struct LCRStatus *statusPtr, unsigned char *devStatus, DelayFunction *EntryDelay, DelayFunction *ExitDelay);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02MustGetField(unsigned short hLCP02ix, unsigned char device, unsigned char fieldNum, void *fieldData, unsigned char *devStatus, DelayFunction *EntryDelay, DelayFunction *ExitDelay);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02MustGetSecurityLevel(unsigned short hLCP02ix, unsigned char device, char *securityLevel, DelayFunction *EntryDelay, DelayFunction *ExitDelay);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02MustGetStatus(unsigned short hLCP02ix, unsigned char device, struct LCRStatus *statusPtr, unsigned char *devStatus, DelayFunction *EntryDelay, DelayFunction *ExitDelay);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02MustPrint(unsigned short hLCP02ix, unsigned char device, char *data, size_t dataLen, DelayFunction *EntryDelay, DelayFunction *ExitDelay);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02MustSetField(unsigned short hLCP02ix, unsigned char device, unsigned char fieldNum, void *fieldData, unsigned char *devStatus, DelayFunction *EntryDelay, DelayFunction *ExitDelay);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02Open(unsigned char minDevice, unsigned char maxDevice, LCPReqThread *ReqMsgProcessor, unsigned char *numDevices, unsigned char *deviceList, unsigned short *hLCP02ix);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02Print(unsigned short hLCP02ix, unsigned char device, char *text,size_t textLen, unsigned char mode);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02SetDeviceAddr(unsigned short hLCP02ix, unsigned char device, unsigned char newDevice, unsigned char mode);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02SetField(unsigned short hLCP02ix, unsigned char device, unsigned char fieldNum, void *fieldData, unsigned char *devStatus, unsigned char mode);

    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02AbortRequest(unsigned short hLCP02ix, unsigned char device);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02CheckRequest(unsigned short hLCP02ix, unsigned char device);

    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02GetNumFields(unsigned short hLCP02ix, unsigned char *numFields);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02GetFieldSecurity(unsigned short hLCP02ix, unsigned char fieldNum, char *fieldSecurity);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02GetFieldType(unsigned short hLCP02ix, unsigned char fieldNum, unsigned char *fieldType);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02GetFieldWidth(unsigned short hLCP02ix, unsigned char fieldNum, unsigned char *fieldWidth);

    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02GetLibraryRevision(unsigned short hLCP02ix, char *revision); 
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02GetPortSettings(unsigned short hLCP02ix, unsigned int *portNumber, unsigned char *baudRateIX, unsigned char *txEnable, unsigned char *retries, unsigned short *timeout, int *baudSearch);

#else

    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02ActivatePumpAndPrint(unsigned char device,unsigned char mode);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02AreYouThere(unsigned char device);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02Close(void); 
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02DeleteTransaction(unsigned char device,unsigned char mode);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02EStop(unsigned char device,unsigned char mode);
    SCOPE LCP02_LIBSPEC size_t WINAPI LCP02FieldSize(unsigned char fieldNum);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02GetDeliveryStatus(unsigned char device,struct LCRStatus *status,unsigned char *devStatus,unsigned char mode);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02GetField(unsigned char device,unsigned char fieldNum,void *fieldData,unsigned char *devStatus,unsigned char mode);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02GetFieldSecurity(char *fieldSecurity);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02GetFieldType(unsigned char *fieldType);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02GetFirstTicketLine(unsigned char device,unsigned char *ticketLinePtr,unsigned char mode);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02GetLibraryRevision(char *revision); 
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02GetNextTicketLine(unsigned char device,unsigned char *ticketLinePtr,unsigned char mode);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02GetNumFields(unsigned char *numFields);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02GetSecurityLevel(unsigned char device,char *securityLevel,unsigned char mode);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02GetStatus(unsigned char device,struct LCRStatus *status,unsigned char *devStatus,unsigned char mode);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02GetTransaction(unsigned char device,struct LCRTransaction *transPtr,unsigned char mode);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02GetVersion(unsigned char device,unsigned short *version,unsigned char mode);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02IssueCommand(unsigned char device,unsigned char command,unsigned char mode);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02MustGetDeliveryStatus(unsigned char device,struct LCRStatus *statusPtr,unsigned char *devStatus,DelayFunction *EntryDelay,DelayFunction *ExitDelay);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02MustGetField(unsigned char device,unsigned char fieldNum,void *fieldData,unsigned char *devStatus,DelayFunction *EntryDelay,DelayFunction *ExitDelay);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02MustGetSecurityLevel(unsigned char device,char *securityLevel,DelayFunction *EntryDelay,DelayFunction *ExitDelay);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02MustGetStatus(unsigned char device,struct LCRStatus *statusPtr,unsigned char *devStatus,DelayFunction *EntryDelay,DelayFunction *ExitDelay);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02MustPrint(unsigned char device,char *data,size_t dataLen,DelayFunction *EntryDelay,DelayFunction *ExitDelay);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02MustSetField(unsigned char device,unsigned char fieldNum,void *fieldData,unsigned char *devStatus,DelayFunction *EntryDelay,DelayFunction *ExitDelay);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02Open(unsigned char minDevice,unsigned char maxDevice,unsigned char *numDevices,unsigned char *deviceList);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02Print(unsigned char device,char *text,size_t textLen,unsigned char mode);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02SetDeviceAddr(unsigned char device,unsigned char newDevice,unsigned char mode);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02SetField(unsigned char device,unsigned char fieldNum,void *fieldData,unsigned char *devStatus,unsigned char mode);

#if defined(_MSC_VER) && (_MSC_VER <= 800)                              // check for DOS

    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02AbortRequest(void);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02CheckRequest(void);
	SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02GetPortSettings(char *portName,unsigned char *baudRateIndex,unsigned char *txEnable,unsigned char *maxRetries,unsigned short *timeout,int *baudSearch);

#else

    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02AbortRequest(unsigned char device);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02CheckRequest(unsigned char device);
	SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02GetPortSettings(WCHAR *portName,unsigned char *baudRateIndex,unsigned char *txEnable,unsigned char *maxRetries,unsigned short *timeout,int *baudSearch);	

    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02IPAbortRequest(unsigned char device,PLCPIP_ADDR ipAddr);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02IPActivatePumpAndPrint(unsigned char device,PLCPIP_ADDR ipAddr,unsigned char mode);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02IPAreYouThere(unsigned char device,PLCPIP_ADDR ipAddr);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02IPClose(void); 
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02IPCheckRequest(unsigned char device,PLCPIP_ADDR ipAddr);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02IPDeleteTransaction(unsigned char device,PLCPIP_ADDR ipAddr,unsigned char mode);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02IPEStop(unsigned char device,PLCPIP_ADDR ipAddr,unsigned char mode);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02IPGetDeliveryStatus(unsigned char device,PLCPIP_ADDR ipAddr,struct LCRStatus *status,unsigned char *devStatus,unsigned char mode);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02IPGetField(unsigned char device,PLCPIP_ADDR ipAddr,unsigned char fieldNum,void *fieldData,unsigned char *devStatus,unsigned char mode);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02IPGetFirstTicketLine(unsigned char device,PLCPIP_ADDR ipAddr,unsigned char *ticketLinePtr,unsigned char mode);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02IPGetNextTicketLine(unsigned char device,PLCPIP_ADDR ipAddr,unsigned char *ticketLinePtr,unsigned char mode);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02IPGetSecurityLevel(unsigned char device,PLCPIP_ADDR ipAddr,char *securityLevel,unsigned char mode);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02IPGetStatus(unsigned char device,PLCPIP_ADDR ipAddr,struct LCRStatus *status,unsigned char *devStatus,unsigned char mode);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02IPGetTransaction(unsigned char device,PLCPIP_ADDR ipAddr,struct LCRTransaction *transPtr,unsigned char mode);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02IPGetVersion(unsigned char device,PLCPIP_ADDR ipAddr,unsigned short *version,unsigned char mode);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02IPIssueCommand(unsigned char device,PLCPIP_ADDR ipAddr,unsigned char command,unsigned char mode);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02IPMustGetDeliveryStatus(unsigned char device,PLCPIP_ADDR ipAddr,struct LCRStatus *statusPtr,unsigned char *devStatus,DelayFunction *EntryDelay,DelayFunction *ExitDelay);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02IPMustGetField(unsigned char device,PLCPIP_ADDR ipAddr,unsigned char fieldNum,void *fieldData,unsigned char *devStatus,DelayFunction *EntryDelay,DelayFunction *ExitDelay);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02IPMustGetSecurityLevel(unsigned char device,PLCPIP_ADDR ipAddr,char *securityLevel,DelayFunction *EntryDelay,DelayFunction *ExitDelay);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02IPMustGetStatus(unsigned char device,PLCPIP_ADDR ipAddr,struct LCRStatus *statusPtr,unsigned char *devStatus,DelayFunction *EntryDelay,DelayFunction *ExitDelay);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02IPMustPrint(unsigned char device,PLCPIP_ADDR ipAddr,char *data,size_t dataLen,DelayFunction *EntryDelay,DelayFunction *ExitDelay);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02IPMustSetField(unsigned char device,PLCPIP_ADDR ipAddr,unsigned char fieldNum,void *fieldData,unsigned char *devStatus,DelayFunction *EntryDelay,DelayFunction *ExitDelay);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02IPOpen(unsigned char minDevice,unsigned char maxDevice);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02IPPrint(unsigned char device,PLCPIP_ADDR ipAddr,char *text,size_t textLen,unsigned char mode);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02IPSetDeviceAddr(unsigned char device,PLCPIP_ADDR ipAddr,unsigned char newDevice,unsigned char mode);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02IPSetField(unsigned char device,PLCPIP_ADDR ipAddr,unsigned char fieldNum,void *fieldData,unsigned char *devStatus,unsigned char mode);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02RegisterIPNodes(int ipPort,PLCPIP_ADDR ipAddr,unsigned char minDevice,unsigned char maxDevice);
    SCOPE LCP02_LIBSPEC unsigned char WINAPI LCP02GetRegistrySettings(_TCHAR *portName,unsigned char *baudRateIndex,unsigned char *txEnable,unsigned char *maxRetries,unsigned short *timeout,int *baudSearch,int *localIPPort,int *deviceIPPort,_TCHAR *deviceIPAddr);

#endif

#endif

#endif
