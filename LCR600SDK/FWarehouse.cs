﻿using MySql.Data.MySqlClient;
using System.Data.SQLite;
using System.IO;
using System.Reflection;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace LCR600SDK
{
    public partial class FWarehouse : Form
    {
        public string whosueSelected = "";
        private string myConnectionString = string.Empty;
        private double pMeterFaktor = 1;
        PasswordEncrypDecrypt decryptor = new PasswordEncrypDecrypt();
        WriteLogText log = new WriteLogText();
        tbl_setting WHouseSetting = new tbl_setting();
        public FWarehouse()
        {
            var _proper = Properties.Settings.Default;
            //myConnectionString = "Data Source=" + Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "SQLite", "db_iflash.db") + " ;New=False;";
            var passPhrase = "iFlashDesktop";
            var decryp = decryptor.DecryptPassword(_proper.db_pass, passPhrase);
            myConnectionString = "Data Source=" + Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "SQLite", "db_iflash.db") + " ;New=False;";

            InitializeComponent();
        }
        void getListWarehouse(string dstrctCode, SQLiteConnection conn)
        {
            conn = new SQLiteConnection(myConnectionString);
            if (conn.State != ConnectionState.Open) conn.Open();
            string query = string.Empty;
            query = "SELECT DISTINCT Warehouse FROM flowmeter where DstrctCode = '" + dstrctCode + "'";
            SQLiteCommand cmd = new SQLiteCommand(query, conn);
            SQLiteDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                comboBoxWarehouse.Items.Add(reader["Warehouse"]);
                //loadWhouse();
            }
            reader.Dispose();
            if (conn.State == ConnectionState.Open) conn.Close();
        }
        void getListWarehouseName(string dstrctCode, SQLiteConnection conn)
        {
            conn = new SQLiteConnection(myConnectionString);
            if (conn.State != ConnectionState.Open) conn.Open();
            string query = string.Empty;
            query = "SELECT DISTINCT WhouseLocation FROM flowmeter where DstrctCode = '" + dstrctCode + "'";
            SQLiteCommand cmd = new SQLiteCommand(query, conn);
            SQLiteDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                cbWarehouseName.Items.Add(reader["WhouseLocation"]);
                //loadWhouse();
            }
            reader.Dispose();
            if (conn.State == ConnectionState.Open) conn.Close();
        }
        void getLastSettingData(SQLiteConnection conn)
        {
            try
            {

                if (conn.State != ConnectionState.Open) conn.Open();
                string query = string.Empty;
                query = "SELECT * FROM tbl_setting where Whouseke = 1 ORDER BY Mod_date DESC LIMIT 1";
                SQLiteCommand cmd = new SQLiteCommand(query, conn);
                SQLiteDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    WHouseSetting.startshift1 = reader["startShift1"].ToString();
                    WHouseSetting.startshift2 = reader["startShift2"].ToString();
                    WHouseSetting.whouse = reader["Whouseid"].ToString();
                    WHouseSetting.whousename = reader["WhouseName"].ToString();
                    WHouseSetting.sncode = reader["SnCode"].ToString();

                    comboBoxWarehouse.SelectedIndex = comboBoxWarehouse.FindStringExact(WHouseSetting.whouse);
                    cbWarehouseName.SelectedIndex = cbWarehouseName.FindStringExact(WHouseSetting.whousename);
                    dateTimeShift1.Text = string.IsNullOrEmpty(reader["startShift1"].ToString()) ? "00:00" : reader["startShift1"].ToString();
                    dateTimeShift2.Text = string.IsNullOrEmpty(reader["startShift2"].ToString()) ? "00:00" : reader["startShift2"].ToString();                    
                    comboBoxSerialNumber.SelectedIndex = comboBoxSerialNumber.FindStringExact(WHouseSetting.sncode);
                    //loadWhouse();
                }
                reader.Dispose();
                if (conn.State == ConnectionState.Open) conn.Close();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
                if (conn.State == ConnectionState.Open) conn.Close();
                MessageBox.Show("FWarehouse.cs Exception: Terjadi kesalahan saat load form, mohon cek log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", "FWarehouse.cs Exception FWarehouse_Load : " + ex.Message);
                save_error_log(ex.Message, "FWarehouse_Load()");
            }


        }
        private void FWarehouse_Load(object sender, EventArgs e)
        {
            try
            {
                //CreateMyDateTimePicker();
                var _globalData = GlobalModel.GlobalVar;
                var dstrctCode = _globalData.DataEmp.DstrctCode;
                SQLiteConnection conn = new SQLiteConnection(myConnectionString);
                getListWarehouse(dstrctCode, conn);
                getListWarehouseName(dstrctCode, conn);
                getLastSettingData(conn);
                enableField(false);
                this.Text = "Warehouse Setting" + GlobalModel.app_version;
                btnSubmit.Text = "OK";

            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
                MessageBox.Show("FWarehouse.cs Exception: Terjadi kesalahan saat load form, mohon cek log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", "FWarehouse.cs Exception FWarehouse_Load : " + ex.Message);
                save_error_log(ex.Message, "FWarehouse_Load()");
            }


        }

        void loadWhouse()
        {
            SQLiteConnection conn = new SQLiteConnection(myConnectionString);
            try
            {
                string whouses = "";
                string whousesname = "";
                string sncodes = "";

                if (conn.State != ConnectionState.Open) conn.Open();
                string query = string.Empty;

                query = "SELECT * FROM tbl_setting";
                SQLiteCommand cmds = new SQLiteCommand(query, conn);
                SQLiteDataReader reader = cmds.ExecuteReader();

                while (reader.Read())
                {
                    whouses = reader["Whouseid"].ToString();
                    whousesname = reader["WhouseName"].ToString();
                    sncodes = reader["SnCode"].ToString();
                    comboBoxWarehouse.SelectedItem = whouses;
                    comboBoxSerialNumber.SelectedItem = sncodes;
                    cbWarehouseName.SelectedItem = whousesname;
                }
                reader.Dispose();
                if (conn.State == ConnectionState.Open) conn.Close();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
                if (conn.State == ConnectionState.Open) conn.Close();
                MessageBox.Show("FWarehouse.cs Exception: Terjadi kesalahan saat get data warehouse, mohon cek log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", "FWarehouse.cs Exception loadWhouse : " + ex.Message);
                save_error_log(ex.Message, "loadWhouse()");
            }

        }

        int execCmd(String sQuery, SQLiteConnection sConn)
        {
            int iReturn = 0;

            var cmd = new SQLiteCommand(sQuery, sConn);
            iReturn = cmd.ExecuteNonQuery();

            return iReturn;
        }

        public void CreateMyDateTimePicker()
        {
            dateTimeShift1.Format = DateTimePickerFormat.Custom;
            dateTimeShift1.CustomFormat = "HH:mm:ss";
            dateTimeShift2.Format = DateTimePickerFormat.Custom;
            dateTimeShift2.CustomFormat = "HH:mm:ss";
        }

        bool isValid()
        {
            bool iBlStatus = false;

            if (comboBoxWarehouse.Text == "")
            {
                MessageBox.Show("Warehouse di isi..", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (cbWarehouseName.Text == "")
            {
                MessageBox.Show("Warehouse Name di isi..", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (comboBoxSerialNumber.Text == "")
            {
                MessageBox.Show("SN Code di isi..", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (dateTimeShift1.Text == "")
            {
                MessageBox.Show("Shift 1 di isi..", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (dateTimeShift2.Text == "")
            {
                MessageBox.Show("Shift 2 di isi..", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                iBlStatus = true;
            }

            return iBlStatus;
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            SQLiteConnection conn = new SQLiteConnection(myConnectionString);
            try
            {
                var _proper = Properties.Settings.Default;
                if (isValid())
                {
                    var _item = new tbl_setting();
                    var initData = GlobalModel.GlobalVar;
                    DateTime localDate = DateTime.Now;
                    int recs = 0;

                    string selectedItemWhouse = this.comboBoxWarehouse.GetItemText(this.comboBoxWarehouse.SelectedItem);
                    string selectedItemWhouseName = this.cbWarehouseName.GetItemText(this.cbWarehouseName.SelectedItem);
                    string selectedItemSerialNumber = this.comboBoxSerialNumber.GetItemText(this.comboBoxSerialNumber.SelectedItem);


                    _item.pid = System.Guid.NewGuid().ToString();
                    _item.whouse = selectedItemWhouse.ToString();
                    _item.whousename = selectedItemWhouseName.ToString();
                    _item.sncode = selectedItemSerialNumber.ToString();
                    _item.startshift1 = dateTimeShift1.Value.ToString("HH:mm") + ":00";
                    _item.startshift2 = dateTimeShift2.Value.ToString("HH:mm") + ":00";
                    _item.meterFakor = pMeterFaktor;
                    _item.mod_date = localDate.ToString();
                    _item.mod_by = initData.DataEmp.Nrp;


                    if (conn.State != ConnectionState.Open) conn.Open();
                    string query = string.Empty;

                    SQLiteCommand cmds = new SQLiteCommand(query, conn);
                    query = "DELETE FROM tbl_setting where Whouseke = 1";
                    execCmd(query, conn);
                    query = "INSERT INTO tbl_setting(pid,whouseid,whousename,sncode,meterFakor,startshift1,startshift2,mod_date,mod_by,Whouseke)"
                        + " VALUES(@pid,@whouseid,@whousename,@sncode,@meterFakor,@startshift1,@startshift2,@mod_date,@mod_by,@Whouseke)";

                    SQLiteCommand cmd = new SQLiteCommand(query, conn);

                    cmd.Parameters.Add(new SQLiteParameter("@pid", _item.pid));
                    cmd.Parameters.Add(new SQLiteParameter("@whouseid", _item.whouse));
                    cmd.Parameters.Add(new SQLiteParameter("@whousename", _item.whousename));
                    cmd.Parameters.Add(new SQLiteParameter("@sncode", _item.sncode));
                    cmd.Parameters.Add(new SQLiteParameter("@meterFakor", _item.meterFakor));
                    cmd.Parameters.Add(new SQLiteParameter("@startshift1", _item.startshift1));
                    cmd.Parameters.Add(new SQLiteParameter("@startshift2", _item.startshift2));
                    cmd.Parameters.Add(new SQLiteParameter("@mod_date", _item.mod_date));
                    cmd.Parameters.Add(new SQLiteParameter("@mod_by", _item.mod_by));
                    cmd.Parameters.Add(new SQLiteParameter("@Whouseke", 1));


                    recs = cmd.ExecuteNonQuery();
                    if (recs > 0)
                    {
                        whosueSelected = _item.whouse.ToString();
                        MessageBox.Show("Warehouse Setting Successfull");
                    }

                    if (conn.State == ConnectionState.Open) conn.Close();


                    enableField(false);
                    this.Hide();
                    if (_proper.menu_receive_direct_from_owner == "True")
                    {

                        FWarehouse2 FWarehouse2 = new FWarehouse2();
                        FWarehouse2.ShowDialog();
                    }
                    else
                    {
                        var f = new FMain();
                        f.ShowDialog();
                    }
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
                if (conn.State == ConnectionState.Open) conn.Close();
                MessageBox.Show("FWarehouse.cs Exception: Terjadi kesalahan saat submit data, mohon cek log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", "FWarehouse.cs Exception btnSubmit_Click : " + ex.Message);
                save_error_log(ex.Message, "btnSubmit_Click()");
            }

        }

        void getWhouseLocation(string Warehouse)
        {
            SQLiteConnection conn;

            conn = new SQLiteConnection(myConnectionString);
            if (conn.State != ConnectionState.Open) conn.Open();
            string query = string.Empty;

            string selectedItem = this.comboBoxWarehouse.GetItemText(Warehouse);

            query = "SELECT DISTINCT WhouseLocation FROM flowmeter where Warehouse = '" + Warehouse + "'";
            SQLiteCommand cmds = new SQLiteCommand(query, conn);
            SQLiteDataReader readers = cmds.ExecuteReader();
            while (readers.Read())
            {
                cbWarehouseName.Items.Add(readers["WhouseLocation"]);
            }
            cbWarehouseName.SelectedIndex = 0;
            readers.Dispose();
            if (conn.State == ConnectionState.Open) conn.Close();
        }

        private void comboBoxWarehouse_SelectedIndexChanged(object sender, EventArgs e)
        {
            SQLiteConnection conn = new SQLiteConnection(myConnectionString);
            try
            {
                cbWarehouseName.Items.Clear();
                cbWarehouseName.Text = "";

                if (conn.State != ConnectionState.Open) conn.Open();
                string query = string.Empty;
                //if (WHouseSetting.whouse == null || WHouseSetting.whouse != this.comboBoxWarehouse.SelectedItem)
                //{
                //    string selectedItem = this.comboBoxWarehouse.GetItemText(this.comboBoxWarehouse.SelectedItem);
                //    WHouseSetting.whouse = selectedItem;
                //}                                    
                //string selectedItem = this.comboBoxWarehouse.GetItemText(this.comboBoxWarehouse.SelectedItem);
                string selectedItem = this.comboBoxWarehouse.SelectedItem == null ? WHouseSetting.whouse : this.comboBoxWarehouse.GetItemText(this.comboBoxWarehouse.SelectedItem);

                query = "SELECT DISTINCT WhouseLocation FROM flowmeter where Warehouse = '" + selectedItem + "'";
                SQLiteCommand cmds = new SQLiteCommand(query, conn);
                SQLiteDataReader readers = cmds.ExecuteReader();
                while (readers.Read())
                {
                    cbWarehouseName.Items.Add(readers["WhouseLocation"]);
                }
                cbWarehouseName.SelectedIndex = 0;
                readers.Dispose();
                if (conn.State == ConnectionState.Open) conn.Close();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
                if (conn.State == ConnectionState.Open) conn.Close();
                MessageBox.Show("FWarehouse.cs Exception: Terjadi kesalahan saat pilih data warehouse, mohon cek log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", "FWarehouse.cs Exception comboBoxWarehouse_SelectedIndexChanged : " + ex.Message);
                save_error_log(ex.Message, "comboBoxWarehouse_SelectedIndexChanged()");
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FWarehouse_Activated(object sender, EventArgs e)
        {
            //this.WindowState = System.Windows.Forms.FormWindowState.Normal;
            //this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
        }
        private void enableField(bool act)
        {
            comboBoxWarehouse.Enabled = act;
            comboBoxSerialNumber.Enabled = act;
            dateTimeShift1.Enabled = act;
            dateTimeShift2.Enabled = act;
            cbWarehouseName.Enabled = act;
            //btnSubmit.Visible = act;
        }
        private void btnNewConfig_Click(object sender, EventArgs e)
        {
            var f = new FAdminValidation();
            if (f.ShowDialog() == DialogResult.OK)
            {
                enableField(true);
                btnSubmit.Text = "SUBMIT";
                btnNewConfig.Hide();
            }
            //comboBoxWarehouse.Enabled = true;
            //comboBoxSerialNumber.Enabled = true;
            //dateTimeShift1.Enabled = true;
            //dateTimeShift2.Enabled = true;
            //btnSubmit.Visible = true;
            //btnNewConfig.Visible = false;
        }
        void getSNCode(string WhouseLocation)
        {
            SQLiteConnection conn;

            conn = new SQLiteConnection(myConnectionString);
            if (conn.State != ConnectionState.Open) conn.Open();
            string query = string.Empty;

            string selectedItem = this.cbWarehouseName.GetItemText(WhouseLocation);

            query = "SELECT DISTINCT SnCode FROM flowmeter where WhouseLocation = '" + selectedItem + "'";
            SQLiteCommand cmds = new SQLiteCommand(query, conn);
            SQLiteDataReader readers = cmds.ExecuteReader();
            while (readers.Read())
            {
                comboBoxSerialNumber.Items.Add(readers["SnCode"]);
            }
            comboBoxSerialNumber.SelectedIndex = 0;
            readers.Dispose();
            if (conn.State == ConnectionState.Open) conn.Close();
        }
        private void cbWarehouseName_SelectedIndexChanged(object sender, EventArgs e)
        {
            SQLiteConnection conn = new SQLiteConnection(myConnectionString);
            try
            {
                comboBoxSerialNumber.Items.Clear();
                comboBoxSerialNumber.Text = "";

                if (conn.State != ConnectionState.Open) conn.Open();
                string query = string.Empty;
                //if(WHouseSetting.whousename== null || WHouseSetting.whousename != this.cbWarehouseName.SelectedItem)
                //{
                //    string selectedItem = this.cbWarehouseName.GetItemText(this.cbWarehouseName.SelectedItem);
                //    WHouseSetting.whousename = selectedItem;
                //}
                //string selectedItem = this.cbWarehouseName.GetItemText(this.cbWarehouseName.SelectedItem);   
                string selectedItem = this.cbWarehouseName.SelectedItem == null ? WHouseSetting.whousename : this.cbWarehouseName.GetItemText(this.cbWarehouseName.SelectedItem);
                query = "SELECT DISTINCT SnCode FROM flowmeter where WhouseLocation = '" + selectedItem + "'";
                SQLiteCommand cmds = new SQLiteCommand(query, conn);
                SQLiteDataReader readers = cmds.ExecuteReader();
                while (readers.Read())
                {
                    comboBoxSerialNumber.Items.Add(readers["SnCode"]);
                }
                comboBoxSerialNumber.SelectedIndex = 0;
                readers.Dispose();
                if (conn.State == ConnectionState.Open) conn.Close();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
                if (conn.State == ConnectionState.Open) conn.Close();
                MessageBox.Show("FWarehouse.cs Exception: Terjadi kesalahan saat pilih data warehouse name, mohon cek log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", "FWarehouse.cs Exception cbWarehouseName_SelectedIndexChanged : " + ex.Message);
                save_error_log(ex.Message, "cbWarehouseName_SelectedIndexChanged()");
            }
        }

        private void comboBoxSerialNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            SQLiteConnection conn = new SQLiteConnection(myConnectionString);
            try
            {

                if (conn.State != ConnectionState.Open) conn.Open();
                string query = string.Empty;
                //if(WHouseSetting.sncode== null || WHouseSetting.sncode != this.comboBoxSerialNumber.SelectedItem)
                //{
                //    string selectedSN = this.comboBoxSerialNumber.GetItemText(this.comboBoxSerialNumber.SelectedItem);
                //    WHouseSetting.sncode = selectedSN;
                //}
                //string selectedWh = this.comboBoxWarehouse.GetItemText(this.comboBoxWarehouse.SelectedItem);
                //string selectedSN = this.comboBoxSerialNumber.GetItemText(this.comboBoxSerialNumber.SelectedItem);

                string selectedWh = this.comboBoxWarehouse.GetItemText(this.comboBoxWarehouse.SelectedItem);
                string selectedSN = this.comboBoxSerialNumber.SelectedItem == null ? WHouseSetting.sncode : this.comboBoxSerialNumber.GetItemText(this.comboBoxSerialNumber.SelectedItem);

                query = $"SELECT DISTINCT MeterFakor FROM flowmeter where Warehouse = '{selectedWh}' and SnCode = '{selectedSN}'";
                SQLiteCommand cmds = new SQLiteCommand(query, conn);
                SQLiteDataReader readers = cmds.ExecuteReader();

                while (readers.Read())
                {
                    pMeterFaktor = double.Parse(readers["MeterFakor"].ToString(), System.Globalization.CultureInfo.InvariantCulture);
                }
                readers.Dispose();
                if (conn.State == ConnectionState.Open) conn.Close();
            }
            catch (Exception ex)
            {

                //MessageBox.Show(ex.ToString());
                if (conn.State == ConnectionState.Open) conn.Close();
                MessageBox.Show("FWarehouse.cs Exception: Terjadi kesalahan saat pilih data serial number, mohon cek log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", "FWarehouse.cs Exception comboBoxSerialNumber_SelectedIndexChanged : " + ex.Message);
                save_error_log(ex.Message, "comboBoxSerialNumber_SelectedIndexChanged()");
            }
        }

        private void comboBoxWarehouse_Click(object sender, EventArgs e)
        {
            try
            {
                ProcessStartInfo psStartInfo = new ProcessStartInfo();
                //psStartInfo.FileName = @"C:\Program Files\Common Files\microsoft shared\ink\TabTip.exe";
                var dir = Path.Combine(Environment.CurrentDirectory, "VKeyboard", "VKeyboard.exe");
                psStartInfo.FileName = dir;

                Process ps = Process.Start(psStartInfo);

            }
            catch (Exception ex)
            {
                var error = ex.Message;
            }

        }
        void save_error_log(string data, string actions)
        {
            SQLiteConnection conn = new SQLiteConnection(myConnectionString);
            try
            {
                DateTime localDate = DateTime.Now;
                var _item = new tbl_log();
                var initData = GlobalModel.GlobalVar;

                _item.pid = System.Guid.NewGuid().ToString();
                _item.actions = $"District {initData.DataEmp.DstrctCode}/FWarehouse/{actions}";
                _item.message = $"{data}";
                _item.mod_date = localDate;
                _item.mod_by = initData.DataEmp.Nrp;


                if (conn.State != ConnectionState.Open) conn.Open();
                string query = string.Empty;
                query = "INSERT INTO tbl_log(pid,actions,message,mod_date,mod_by)"
                      + " VALUES(@pid,@actions,@message,@mod_date,@mod_by)";

                SQLiteCommand cmd = new SQLiteCommand(query, conn);

                cmd.Parameters.Add(new SQLiteParameter("@pid", _item.pid));
                cmd.Parameters.Add(new SQLiteParameter("@actions", _item.actions));
                cmd.Parameters.Add(new SQLiteParameter("@message", _item.message));
                cmd.Parameters.Add(new SQLiteParameter("@mod_date", _item.mod_date));
                cmd.Parameters.Add(new SQLiteParameter("@mod_by", _item.mod_by));
                cmd.ExecuteNonQuery();
                if (conn.State == ConnectionState.Open) conn.Close();
            }
            catch (Exception)
            {
                if (conn.State == ConnectionState.Open) conn.Close();
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            var result = MessageBox.Show("Sure?", "Log Out", MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                this.Hide();
                var f = new FLogin();
                f.ShowDialog();
            }
        }
    }
}
