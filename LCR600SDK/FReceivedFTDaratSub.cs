﻿using Flurl.Http;
using LCR600SDK.DataClass;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LCR600SDK
{
    public partial class FReceivedFTDaratSub : Form
    { 
        public string pPidPenerimaan;
        public string pPoNO;
        private string URL_API = string.Empty;
        private string URL_API_CALL = string.Empty;
        private string pUrl_service = string.Empty;
        public string pPage = "1";
        public string pPageSize = "500";
        public FReceivedFTDaratSub()
        {
            InitializeComponent();
        }

        private void FReceivedFTPort_Load(object sender, EventArgs e)
        {
            var _proper = Properties.Settings.Default;
            dateTimePicker1.Text = DateTime.Now.ToString("yyyy-MM-dd");
            pUrl_service = _proper.url_service;
            URL_API = $"{pUrl_service}api/FuelService/getRitasiMaintankDaratReceiveDesktop";
            String _url = $"{URL_API}?page={pPage}&pageSize={pPageSize}&activity=darat&pid_darat={pPidPenerimaan}";

            loadGridAsync(_url);
        }
        private void dataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs args)
        {
            if (args.ColumnIndex == 7)
            {
                var SendDate = args.Value.ToString();
                args.Value = DateTime.Parse(SendDate).ToString("dd-MM-yyyy");
                args.FormattingApplied = true;
            }
        }
        public async void loadGridAsync(string _url)
        { 
            var _auth = GlobalModel.loginModel; 
            try
            {
                URL_API_CALL = _url;
                var datas = await _url.WithHeaders(new { Authorization = _auth.token }).GetJsonAsync<RitasiMaintankReceiveClass>(); 

                GlobalModel.ritasiMaintankReceive = datas;
                setPaging();
                DataTable dt = GeneralFunc.ToDataTable(GlobalModel.ritasiMaintankReceive.ListData);
                dataGridView1.DataSource = dt;
                for (int i = 0; i < dataGridView1.RowCount; i++)
                {
                    DataGridViewButtonCell cell = new DataGridViewButtonCell();
                    dataGridView1[0, i] = cell;

                    if (dataGridView1[0, i] == cell)
                    {
                        cell.Value = "Terima";
                        cell.FlatStyle = FlatStyle.Flat;
                        cell.Style.BackColor = Color.SeaGreen;
                        cell.Style.ForeColor = Color.WhiteSmoke;
                        cell.Style.SelectionBackColor = Color.SeaGreen;
                    }
                    //if (dataGridView1[0, i] is DataGridViewButtonCell cell)
                    //{
                    //    cell.Value = "Terima";
                    //    cell.FlatStyle = FlatStyle.Flat;
                    //    cell.Style.BackColor = Color.SeaGreen;
                    //    cell.Style.ForeColor = Color.WhiteSmoke;
                    //    cell.Style.SelectionBackColor = Color.SeaGreen;
                    //}
                }
                Console.WriteLine("Get token Success");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            } 
        }

        void setPaging()
        {
            try
            {
                var _data = GlobalModel.penerimaanFtDarat;
                txtPage.Text = $"{_data.CurrentPage}";
                lblPageOf.Text = $"Page {_data.CurrentPage} of {_data.TotalPage}    - Total rows {_data.TotalSize}";
                btnNext.Enabled = (_data.TotalPage > _data.CurrentPage);
                btnPrev.Enabled = (_data.CurrentPage > 1);
            }
            catch (Exception)
            {
                MessageBox.Show("Tidak ada data untuk di tampilan", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.Columns[e.ColumnIndex].Name == "Action")
            {
                var f = new FReceivedFTPortForm();
                var _row = GlobalModel.ritasiMaintankReceive.ListData[e.RowIndex];
                f.pData = _row;
                f.CALL_FROM = "RECV_DARAT"; 
                f.URL_API_CALL = URL_API_CALL;
                if (_row.IsEditable == 1) 
                    f.ShowDialog(); 
                else 
                    MessageBox.Show("Data tidak bisa di edit", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information); 
            }
        }

        private void txtSearch_KeyPress(object sender, KeyPressEventArgs e)
        { 
            if (e.KeyChar == (char)Keys.Return)
            {
                String _url = $"{URL_API}?page={pPage}&pageSize={pPageSize}&activity=darat&search={txtSearch.Text}&pid_darat={pPidPenerimaan}&send_date={txtSendDate.Text}";
                loadGridAsync(_url);
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            String _url = $"{URL_API}?page={pPage}&pageSize={pPageSize}&activity=darat&search={txtSearch.Text}&pid_darat={pPidPenerimaan}&send_date={txtSendDate.Text}";
            loadGridAsync(_url);
        }

        private void btnPrev_Click(object sender, EventArgs e)
        {
            String _url = $"{URL_API}?page={Convert.ToInt32(txtPage.Text) - 1}&pageSize={pPageSize}&activity=darat&search={txtSearch.Text}&pid_darat={pPidPenerimaan}&send_date={txtSendDate.Text}";
            loadGridAsync(_url);
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            String _url = $"{URL_API}?page={Convert.ToInt32(txtPage.Text) + 1}&pageSize={pPageSize}&activity=darat&search={txtSearch.Text}&pid_darat={pPidPenerimaan}&send_date={txtSendDate.Text}";
            loadGridAsync(_url);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var _data = GlobalModel.ritasiMaintankReceive;
            button2.Text = "Waiting";
            button2.ForeColor = Color.Black;
            button2.BackColor = Color.Gold;
            if (numGoToPage.Value > _data.TotalPage)
            {
                numGoToPage.Value = decimal.Parse(_data.TotalPage.ToString());
            }
            else if (numGoToPage.Value <= 0)
            {
                numGoToPage.Value = 1;
            }

            String _url = $"{URL_API}?page={numGoToPage.Value}&pageSize={pPageSize}&search={txtSearch.Text}&pid_darat={pPidPenerimaan}&send_date={txtSendDate.Text}";
            loadGridAsync(_url);
        }
        void changeFilter()
        {
            String _url = $"{URL_API}?page={numGoToPage.Value}&pageSize={pPageSize}&search={txtSearch.Text}&pid_darat={pPidPenerimaan}&send_date={txtSendDate.Text}";
            loadGridAsync(_url);
        }
        private void dateTimePicker1_CloseUp(object sender, EventArgs e)
        {
            txtSendDate.Text = dateTimePicker1.Value.ToString("yyyy-MM-dd 00:00:00");
            changeFilter();
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {            
            txtSearch.Text = "";
            txtSendDate.Text = "";
            dataGridView1.DataSource = null;
            String _url = $"{URL_API}?page={pPage}&pageSize={pPageSize}&activity=darat&pid_darat={pPidPenerimaan}";
            loadGridAsync(_url);
        }
    }
}
