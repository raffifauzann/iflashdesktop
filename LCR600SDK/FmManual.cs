﻿using Flurl.Http;
using LCR600SDK.DataClass;
using MySql.Data.MySqlClient; using System.Data.SQLite; using System.IO; using System.Reflection;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LCR600SDK
{
    public partial class FmManual : Form
    {
        //GlobalModel GM = new GlobalModel(); 
        public double totalizer_awal;
        public double totalizer_akhir;
        public double quantity;
        public string trans_type_select=string.Empty;
        public string stat_type;
        public int preset;
        public int waitCounterMax = 30;
        int waitCounter = 0;
        int countDown = 0;
        int waitcountDown = 1;
        private bool statusOnline = false;
        public string menu_group;

        private string myConnectionString = string.Empty;
        public ListDatumRitasiMaintank ListDatumRitasiMaintank;
        public ListReceivedDirectClass ListReceivedDirectClass;
        private string pUrl_service = string.Empty;
        PasswordEncrypDecrypt decryptor = new PasswordEncrypDecrypt();
        ClsLCR lcr = new ClsLCR();
        ClsResult res = new ClsResult();
        int presetGrossVol = 0;
        int lcrState = -1;
        int stopSignal = 0;
        int lcr_whole_or_tenths = 1;

        public FmManual()
        {
            InitializeComponent();            
        }

        private void FmManual_Load(object sender, EventArgs e)
        {
            var _proper = Properties.Settings.Default;
            lcr_whole_or_tenths = int.Parse(_proper.lcr_whole_or_tenths);
            pUrl_service = _proper.url_service;
            menu_group = _proper.menu_grouping;
            statusOnline = _proper.connection == "ONLINE" ? true : false;
            //myConnectionString = "Data Source=" + Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "SQLite", "db_iflash.db") + " ;New=False;";
            var passPhrase = "iFlashDesktop";
            var decryp = decryptor.DecryptPassword(_proper.db_pass, passPhrase);
            myConnectionString = "Data Source=" + Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "SQLite", "db_iflash.db") + " ;New=False;";
            grpManual.Hide();
            var cek = GlobalModel.AutoRefueling;
            var preset_view = preset / 10;
            txtAutoPreset.Text = preset_view.ToString();
            var counterView = waitCounterMax / 10;
            countDown = counterView;
            //lblAutoValueQty.Text = "1075,6";
            //lblAutoFLowAwal1.Text = "117415391,9";
            //lblAutoFlowAkhir1.Text = "117416467,5";
            //var qty = double.Parse(lblAutoValueQty.Text) * lcr_whole_or_tenths;
            //var totalAwal = double.Parse(lblAutoFLowAwal1.Text) * lcr_whole_or_tenths;
            //var totalAkhir = double.Parse(lblAutoFlowAkhir1.Text) * lcr_whole_or_tenths;
            //txtRefStart.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            //txtRefStop.Text = DateTime.Now.AddSeconds(30.0).ToString("yyyy-MM-dd HH:mm:ss");
            //saveData();
            //saveDataTransfer();
            start_refueling();
        }
        
        private void tmrLCR_Tick(object sender, EventArgs e)
        {
            tmrLCR.Enabled = false;
            lblAutoStatus.Text = "State: " + lcrState.ToString() 
                + (lcrState == 0 ? " Failed Connect to LCR " :
                lcrState == 10 ? " Failed Get Totalizer Awal" :                
                lcrState == 20 ? " Failed Open Valve" :
                lcrState == 90 ? " Failed Set Preset Gross Volume" :
                lcrState == 30 ? " Failed Get Gross Quantity" :
                lcrState == 40 ? " Failed Close Valve" :
                lcrState == 50 ? " Failed Get Totalizer Akhir" :
                lcrState == 70 ? " Failed Disconnect LCR" : "");
            


            switch (lcrState)
            {
                case 0:
                    lblAutoFLowAwal1.Text = "";
                    lblAutoFlowAkhir1.Text = "";

                    res = lcr.ConnectLCR();
                    if (res.issuccess)
                        lcrState = 1;
                    else
                        lcrState = 0;
                    break;
                case 1:
                    res = lcr.GetGrossTotalizer();
                    if (!res.issuccess)
                        lcrState = 10;
                    else
                    {
                        lcrState = 2;
                        var totalizer_awal1 = double.Parse(res.value) * lcr_whole_or_tenths;
                        lblAutoFLowAwal1.Text = totalizer_awal1.ToString();
                        GlobalModel.AutoRefueling.receive_flow_meter_awal1 = totalizer_awal1;
                        totalizer_awal = totalizer_awal1;
                        //if (menu_group == "Transfer Issuing")
                        //if (trans_type_select== "PORT_RECEIVED")
                        //{
                        //    #region LCR = WHOLE iFlash Desktop x.x.x.x Received
                        //    var totalizer_awal1 = double.Parse(res.value) * lcr_whole_or_tenths;
                        //    lblAutoFLowAwal1.Text = totalizer_awal1.ToString();
                        //    GlobalModel.AutoRefueling.receive_flow_meter_awal1 = totalizer_awal1;
                        //    totalizer_awal = totalizer_awal1;
                        //    #endregion
                        //}
                        //else
                        //{
                        //    #region LCR = TENTHS iFlash Desktop x.x.x.x
                        //    lblAutoFLowAwal1.Text = res.value;
                        //    GlobalModel.AutoRefueling.receive_flow_meter_awal1 = double.Parse(res.value);
                        //    totalizer_awal = double.Parse(res.value);
                        //    #endregion

                        //}
                    }
                    break;
                case 10:
                    lcrState = 1;
                    break;
                case 2:
                    res = lcr.OpenValve();
                    if (res.issuccess)
                    {
                        if (presetGrossVol == 0)
                            lcrState = 3;
                        else
                            lcrState = 9;
                    }
                    else
                        lcrState = 20;
                    break;
                case 20:
                    lcrState = 2;
                    break;
                case 9:
                    res = lcr.SetPresetGrossVolume(presetGrossVol);
                    if (res.issuccess)
                        lcrState = 3;
                    else
                        lcrState = 90;
                    break;
                case 90:
                    lcrState = 9;
                    break;
                case 3:
                    if (stopSignal == 0)
                    {
                        res = lcr.GetGrossQty();
                        if (!res.issuccess)
                            lcrState = 30;
                        else
                        {
                            lcrState = 3;
                            var qty = double.Parse(res.value) * lcr_whole_or_tenths;
                            lblAutoValueQty.Text = qty.ToString();
                            GlobalModel.AutoRefueling.receive_qty1 = qty;
                            quantity = qty;
                            //if (menu_group == "Transfer Issuing")
                            //if (trans_type_select == "PORT_RECEIVED")
                            //{
                            //    #region LCR = WHOLE iFlash Desktop x.x.x.x Received
                            //    var qty = double.Parse(res.value) * lcr_whole_or_tenths;
                            //    lblAutoValueQty.Text = qty.ToString();
                            //    GlobalModel.AutoRefueling.receive_qty1 = qty;
                            //    quantity = qty;
                            //    #endregion 
                                
                            //}
                            //else
                            //{
                            //    #region LCR = TENTHS iFlash Desktop x.x.x.x
                            //    lblAutoValueQty.Text = res.value;
                            //    GlobalModel.AutoRefueling.receive_qty1 = double.Parse(res.value);
                            //    quantity = double.Parse(res.value);
                            //    #endregion
                            //}
                        }
                    }
                    else
                        lcrState = 4;
                    break;
                case 30:
                    lcrState = 3;
                    break;
                case 4:
                    res = lcr.CloseValve();
                    if (res.issuccess)
                    {
                        lcrState = 41;
                        waitCounter = 0;
                    }
                    else
                        lcrState = 40;
                    break;
                case 40:
                    lcrState = 4;
                    break;
                case 41:
                    waitCounter = waitCounter + 1;                    
                    if (waitCounter > waitCounterMax)
                    {
                        lcrState = 5;
                        lblAutoValueQty.Text = "Valve is closed.";                        
                        tmrCountDown.Stop();
                        lblCountDown.Visible = false;
                    }
                    else
                    {
                        if (!tmrCountDown.Enabled)
                        {
                            tmrCountDown.Start();
                        }                        
                        lblAutoValueQty.Text = String.Format("Valve is closing. Please Wait.");
                    }
                        

                    //lblAutoValueQty.Text = String.Format("Valve is closing. Please Wait {0}s {1}s .", waitCounterView, waitCounterView);
                    //lblAutoValue.Text = "Delay #" + waitCounter.ToString();

                    break;
                case 5:
                    res = lcr.GetGrossTotalizer();
                    if (!res.issuccess)
                        lcrState = 50;
                    else
                    {
                        lcrState = 6;
                        var totalizer_akhir1 = double.Parse(res.value) * lcr_whole_or_tenths;
                        lblAutoFlowAkhir1.Text = totalizer_akhir1.ToString();
                        GlobalModel.AutoRefueling.receive_flow_meter_akhir1 = totalizer_akhir1;
                        totalizer_akhir = totalizer_akhir1;
                        //if (menu_group == "Transfer Issuing")
                        //if (trans_type_select == "PORT_RECEIVED")
                        //{
                        //    #region LCR = WHOLE iFlash Desktop x.x.x.x Received
                        //    var totalizer_akhir1 = double.Parse(res.value) * lcr_whole_or_tenths;
                        //    lblAutoFlowAkhir1.Text = totalizer_akhir1.ToString();
                        //    GlobalModel.AutoRefueling.receive_flow_meter_akhir1 = totalizer_akhir1;
                        //    totalizer_akhir = totalizer_akhir1;
                        //    #endregion

                        //}
                        //else
                        //{                            
                        //    #region LCR = TENTHS iFlash Desktop x.x.x.x
                        //    lblAutoFlowAkhir1.Text = res.value;
                        //    GlobalModel.AutoRefueling.receive_flow_meter_akhir1 = double.Parse(res.value);
                        //    totalizer_akhir = double.Parse(res.value);
                        //    #endregion
                        //}

                    }
                    break;
                case 50:
                    lcrState = 5;
                    break;
                case 6:
                    res = lcr.GetGrossQty();
                    if (!res.issuccess)
                        lcrState = 60;
                    else
                    {
                        lcrState = 7;
                        var qty = double.Parse(res.value) * lcr_whole_or_tenths;
                        lblAutoValueQty.Text = qty.ToString();
                        GlobalModel.AutoRefueling.receive_flow_meter_awal1 = qty;
                        quantity = qty;
                        //if (menu_group == "Transfer Issuing")                        
                        //if (trans_type_select == "PORT_RECEIVED")
                        //{
                        //    #region LCR = WHOLE iFlash Desktop x.x.x.x Received
                        //    var qty = double.Parse(res.value) * lcr_whole_or_tenths;
                        //    lblAutoValueQty.Text = qty.ToString();
                        //    GlobalModel.AutoRefueling.receive_flow_meter_awal1 = qty;
                        //    quantity = qty;
                        //    #endregion

                        //}
                        //else
                        //{                            
                        //    #region LCR = TENTHS iFlash Desktop x.x.x.x
                        //    lblAutoValueQty.Text = res.value;
                        //    GlobalModel.AutoRefueling.receive_qty1 = double.Parse(res.value);
                        //    quantity = double.Parse(res.value);
                        //    #endregion
                        //}
                    }
                    break;
                case 60:
                    lcrState = 6;
                    break;
                case 7:
                    res = lcr.DisconnectLCR();
                    if (res.issuccess)
                    {
                        if (stopSignal == 0)
                        {
                            lcrState = 0;
                        }
                        else
                        {
                            lcrState = -1;
                        }
                    }
                    else
                        lcrState = 70;
                    break;
                case 70:
                    lcrState = 7;
                    break;
                default:
                    break;
            }           
            if (lcrState > -1)
                tmrLCR.Enabled = true;
            else
            {
                lblAutoStatus.Text = "Finish";
                txtAutoPreset.Text = "0";
                stopSignal = 0;
                
                
                if (String.IsNullOrEmpty(GlobalModel.AutoRefueling.trans_type_select))
                {
                    if (waitCounterMax < 350)
                    {
                        saveData();
                    }
                    else
                    {
                        saveDataTransfer();
                    }

                }
                else
                {
                    saveDataAsync();
                }
                this.DialogResult = DialogResult.OK;
                this.Hide();
            }

        }
        private void tmrCountDown_Tick(object sender, EventArgs e)
        {
            lblCountDown.Visible = true;
            lblCountDown.Text = String.Format("{0} s", countDown.ToString());
            if (countDown > 0)
            {
                countDown = countDown - 1;
            }
            
        }
        private void btnManualConnect_Click(object sender, EventArgs e)
        {
            res = lcr.ConnectLCR();
            lblManualValue.Text = "";
            if (res.issuccess)
                lblManualStatus.Text = "Connect LCR Success";
            else
                lblManualStatus.Text = "Connect LCR Failed";
        }

        private void btnManualDisconnect_Click(object sender, EventArgs e)
        {
            res = lcr.DisconnectLCR();
            lblManualValue.Text = "";
            if (res.issuccess)
                lblManualStatus.Text = "Disconnect LCR Success";
            else
                lblManualStatus.Text = "Disconnect LCR Failed";
        }

        private void btnManualOpenValve_Click(object sender, EventArgs e)
        {
            res = lcr.OpenValve();
            lblManualValue.Text = "";
            if (res.issuccess)
                lblManualStatus.Text = "Open Valve LCR Success";
            else
                lblManualStatus.Text = "Open Valve LCR Failed";
        }

        private void btnManualCloseValve_Click(object sender, EventArgs e)
        {
            res = lcr.CloseValve();
            lblManualValue.Text = "";
            if (res.issuccess)
                lblManualStatus.Text = "Close Valve LCR Success";
            else
                lblManualStatus.Text = "Close Valve LCR Failed";
        }

        private void btnManualGrossQty_Click(object sender, EventArgs e)
        {
            res = lcr.GetGrossQty();
            lblManualValue.Text = "";
            if (res.issuccess)
            {
                lblManualStatus.Text = "Get Gross Qty Success";
                lblManualValue.Text = res.value;
            }
            else
            {
                lblManualStatus.Text = "Get Gross Qty Failed";
                lblManualValue.Text = "No Value";
            }
        }

        private void btnManualTotalizer_Click(object sender, EventArgs e)
        {
            res = lcr.GetGrossTotalizer();
            lblManualValue.Text = "";
            if (res.issuccess)
            {
                lblManualStatus.Text = "Get Gross Totalizer Success";
                lblManualValue.Text = res.value;
            }
            else
            {
                lblManualStatus.Text = "Get Gross Totalizer Failed";
                lblManualValue.Text = "No Value";
            }
        }

        public void start_refueling()
        {
            presetGrossVol = preset;
            lblAutoStatus.Text = "";
            lblAutoValueQty.Text = "";
            lblAutoFLowAwal1.Text = "";
            lblAutoFlowAkhir1.Text = "";
            btnAutoStop.Enabled = true;
            lcrState = 0;
            tmrLCR.Enabled = true;
            GlobalModel.AutoRefueling.start_transaction = DateTime.Now;
            txtRefStart.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        }

        private void btnAutoStop_Click(object sender, EventArgs e)
        {
            stopSignal = 1;
            GlobalModel.AutoRefueling.stop_transaction = DateTime.Now;
            txtRefStop.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            
            btnAutoStop.Enabled = false;

        }
        void saveData()
        {
            try
            {
                if ((int)Math.Round(double.Parse(lblAutoValueQty.Text.Trim(), new System.Globalization.CultureInfo("id-ID"))) == 0)
                {
                    return;
                }

                var _item = GlobalModel.logsheet_detail;
                var _lcrData = GlobalModel.lcrData;

                _item.qty = Convert.ToDouble(lblAutoValueQty.Text.Trim());// isNumeric(txtGrossQTY.Text.Replace(".",",")) ? Convert.ToDouble(txtGrossQTY.Text) : 0;
                _item.flow_meter_end = Convert.ToDouble(lblAutoFlowAkhir1.Text.Trim()); //isNumeric(txtGrossTotalizer.Text.Replace(".", ",")) ? Convert.ToDouble(txtGrossTotalizer.Text) : 0;
                _item.flow_meter_start = Convert.ToDouble(lblAutoFLowAwal1.Text.Trim()); // isNumeric(txtTotalizerStart.Text.Replace(".", ",")) ? Convert.ToDouble(txtTotalizerStart.Text) : 0;
                _item.qty_loqsheet = Convert.ToDouble(lblAutoValueQty.Text.Trim());
                var max_tank = preset / 10;
                _item.max_tank_capacity = max_tank;
                _item.ref_hour_start = txtRefStart.Text;
                _item.ref_hour_stop = txtRefStop.Text;

                //_item.is_load_to_fosto = 99;

                SQLiteConnection conn;

                conn = new SQLiteConnection(myConnectionString);
                if (conn.State != ConnectionState.Open) conn.Open();
                string query = string.Empty;
                int recs = 0;
                query = "INSERT INTO tbl_t_log_sheet_detail(job_row_id,dstrct_code,log_sheet_code,input_type,issued_date,whouse_id,unit_no,max_tank_capacity,hm_before,hm,flw_meter,meter_faktor,qty_loqsheet,qty,shift,fuel_oil_type,stat_type,nrp_operator,nama_operator,work_area,location,ref_condition,ref_hour_start,ref_hour_stop,note,timezone,flag_loading,loadingerror,loadingdatetime,mod_by,mod_date,is_load_to_fosto,foto,flow_meter_start,flow_meter_end,resp_code,resp_name,text_header,text_sub_header,text_body,syncs,driver_nrp,driver_name,fuelman_nrp,fuelman_name)"
                      + " VALUES(@job_row_id,@dstrct_code,@log_sheet_code,@input_type,@issued_date,@whouse_id,@unit_no,@max_tank_capacity,@hm_before,@hm,@flw_meter,@meter_faktor,@qty_loqsheet,@qty,@shift,@fuel_oil_type,@stat_type,@nrp_operator,@nama_operator,@work_area,@location,@ref_condition,@ref_hour_start,@ref_hour_stop,@note,@timezone,@flag_loading,@loadingerror,@loadingdatetime,@mod_by,@mod_date,@is_load_to_fosto,@foto,@flow_meter_start,@flow_meter_end,@resp_code,@resp_name,@text_header,@text_sub_header,@text_body,@syncs,@driver_nrp,@driver_name,@fuelman_nrp,@fuelman_name)";

                SQLiteCommand cmd = new SQLiteCommand(query, conn);

                cmd.Parameters.Add(new SQLiteParameter("@job_row_id", _item.job_row_id));
                cmd.Parameters.Add(new SQLiteParameter("@dstrct_code", _item.dstrct_code));
                cmd.Parameters.Add(new SQLiteParameter("@log_sheet_code", _item.log_sheet_code));
                cmd.Parameters.Add(new SQLiteParameter("@input_type", _item.input_type));
                cmd.Parameters.Add(new SQLiteParameter("@issued_date", _item.issued_date));
                cmd.Parameters.Add(new SQLiteParameter("@whouse_id", _item.whouse_id));
                cmd.Parameters.Add(new SQLiteParameter("@unit_no", _item.unit_no));
                cmd.Parameters.Add(new SQLiteParameter("@max_tank_capacity", _item.max_tank_capacity));
                cmd.Parameters.Add(new SQLiteParameter("@hm_before", _item.hm_before));
                cmd.Parameters.Add(new SQLiteParameter("@hm", _item.hm));
                cmd.Parameters.Add(new SQLiteParameter("@flw_meter", _item.flw_meter));
                cmd.Parameters.Add(new SQLiteParameter("@meter_faktor", _item.meter_faktor));
                cmd.Parameters.Add(new SQLiteParameter("@qty_loqsheet", _item.qty_loqsheet));
                cmd.Parameters.Add(new SQLiteParameter("@qty", _item.qty));
                cmd.Parameters.Add(new SQLiteParameter("@shift", _item.shift));
                cmd.Parameters.Add(new SQLiteParameter("@fuel_oil_type", _item.fuel_oil_type));
                cmd.Parameters.Add(new SQLiteParameter("@stat_type", _item.stat_type));
                cmd.Parameters.Add(new SQLiteParameter("@nrp_operator", _item.nrp_operator));
                cmd.Parameters.Add(new SQLiteParameter("@nama_operator", _item.nama_operator));
                cmd.Parameters.Add(new SQLiteParameter("@work_area", _item.work_area));
                cmd.Parameters.Add(new SQLiteParameter("@location", _item.location));
                cmd.Parameters.Add(new SQLiteParameter("@ref_condition", _item.ref_condition));
                cmd.Parameters.Add(new SQLiteParameter("@ref_hour_start", _item.ref_hour_start));
                cmd.Parameters.Add(new SQLiteParameter("@ref_hour_stop", _item.ref_hour_stop));
                cmd.Parameters.Add(new SQLiteParameter("@note", _item.note));
                cmd.Parameters.Add(new SQLiteParameter("@timezone", _item.timezone));
                cmd.Parameters.Add(new SQLiteParameter("@flag_loading", _item.flag_loading));
                cmd.Parameters.Add(new SQLiteParameter("@loadingerror", _item.loadingerror));
                cmd.Parameters.Add(new SQLiteParameter("@loadingdatetime", _item.loadingdatetime));
                cmd.Parameters.Add(new SQLiteParameter("@mod_by", _item.mod_by));
                cmd.Parameters.Add(new SQLiteParameter("@mod_date", _item.mod_date));
                cmd.Parameters.Add(new SQLiteParameter("@is_load_to_fosto", _item.is_load_to_fosto));
                cmd.Parameters.Add(new SQLiteParameter("@foto", _item.foto));
                cmd.Parameters.Add(new SQLiteParameter("@flow_meter_start", _item.flow_meter_start));
                cmd.Parameters.Add(new SQLiteParameter("@flow_meter_end", _item.flow_meter_end));
                cmd.Parameters.Add(new SQLiteParameter("@resp_code", _item.resp_code));
                cmd.Parameters.Add(new SQLiteParameter("@resp_name", _item.resp_name));
                cmd.Parameters.Add(new SQLiteParameter("@text_header", _item.text_header));
                cmd.Parameters.Add(new SQLiteParameter("@text_sub_header", _item.text_sub_header));
                cmd.Parameters.Add(new SQLiteParameter("@text_body", _item.text_body));
                cmd.Parameters.Add(new SQLiteParameter("@syncs", _item.syncs));
                cmd.Parameters.Add(new SQLiteParameter("@driver_nrp", _item.driver_nrp));
                cmd.Parameters.Add(new SQLiteParameter("@driver_name", _item.driver_name));
                cmd.Parameters.Add(new SQLiteParameter("@fuelman_nrp", _item.fuelman_nrp));
                cmd.Parameters.Add(new SQLiteParameter("@fuelman_name", _item.fuelman_name));

                recs = cmd.ExecuteNonQuery();


                if (conn.State == ConnectionState.Open) conn.Close();
            }
            catch (Exception ex)
            {
                save_error_log(ex.ToString(), "saveData()");
                FileToExcel("refueling_ft");
                MessageBox.Show("Data Tidak Tersimpan, Silahkan tulis di logsheet manual", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        public void FileToExcel(string name)
        {
            StringBuilder sb = new StringBuilder();
            var filename = "log_transaksi_" + name + DateTime.Now.ToString("yyyyMMdd") + ".csv";
            //cek direktori jika tidak ada maka dibuat direktori baru
            if (!Directory.Exists(Path.Combine(Environment.CurrentDirectory, "log_transaksi_eror_simpan_db")))
            {
                Directory.CreateDirectory(Path.Combine(Environment.CurrentDirectory, "log_transaksi_eror_simpan_db"));
            }
            var pathExcel = Path.Combine(Environment.CurrentDirectory, "log_transaksi_eror_simpan_db", filename);
            //cek file jika tidak ada maka buat header baru
            if (!File.Exists(pathExcel))
            {
                if (name == "wh_transfer")
                {
                    sb.Append("job_row_id;xfer_code;xfer_date;dstrct_code;employee_id;issued_whouse;received_whouse;stock_code;qty_xfer;shift;fmeter_serial_no;faktor_meter;fmeter_begin;fmeter_end;created_by;xfer_by;received_by;is_submit;issued_whouse_type;received_whouse_type;mod_date;created_date;timezone;\r\n");
                }
                else
                {
                    sb.Append("job_row_id;dstrct_code;whouse_id;unit_no;max_tank_capacity;hm_before;hm;flw_meter;meter_faktor;qty_loqsheet;qty;shift;fuel_oil_type;stat_type;nrp_operator;nama_operator;work_area;location;ref_condition;ref_hour_start;ref_hour_stop;note;timezone;flag_loading;loadingerror;loadingdatetime;mod_by;mod_date;flow_meter_start;flow_meter_end;driver_nrp;driver_name;fuelman_nrp;fuelman_name\r\n");
                }
            }

            if (name == "wh_transfer")
            {
                var item = GlobalModel.whTransfer;
                string issued_date = "";
                sb.Append(item.job_row_id + ";");
                sb.Append(item.xfer_code + ";");
                sb.Append(item.xfer_date + ";");
                sb.Append(item.dstrct_code + ";");
                sb.Append(item.employee_id + ";");
                sb.Append(item.issued_whouse + ";");
                sb.Append(item.received_whouse + ";");
                sb.Append(item.stock_code + ";");
                sb.Append(item.qty_xfer + ";");
                sb.Append(item.shift + ";");
                sb.Append(item.fmeter_serial_no + ";");
                sb.Append(item.faktor_meter + ";");
                sb.Append(item.fmeter_begin + ";");
                sb.Append(item.fmeter_end + ";");
                sb.Append(item.created_by + ";");
                sb.Append(item.xfer_by + ";");
                sb.Append(item.received_by + ";");
                sb.Append(item.is_submit + ";");
                sb.Append(item.issued_whouse_type + ";");
                sb.Append(item.received_whouse_type + ";");
                sb.Append(item.mod_date.ToString("yyyy/mm/dd HH:mm:ss") + ";");
                sb.Append(item.created_date.ToString("yyyy/mm/dd HH:mm:ss") + ";");
                sb.Append(item.timezone + ";");
                sb.Append(item.syncs + ";");
                //Append new line character.
                sb.Append("\r\n");
            }
            else
            {
                var item = GlobalModel.logsheet_detail;
                string issued_date = "";
                sb.Append(item.job_row_id + ";");
                sb.Append(item.dstrct_code + ";");
                sb.Append(item.log_sheet_code + ";");
                sb.Append(item.input_type + ";");
                if (item.issued_date != null)
                {
                    issued_date = DateTime.Parse(item.issued_date.ToString()).ToString("yyyy-MM-dd");
                }
                sb.Append(issued_date + ";");
                sb.Append(item.whouse_id + ";");
                sb.Append(item.unit_no + ";");
                sb.Append(item.max_tank_capacity + ";");
                sb.Append(item.hm_before + ";");
                sb.Append(item.hm + ";");
                sb.Append(item.flw_meter + ";");
                sb.Append(item.meter_faktor + ";");
                sb.Append(item.qty_loqsheet + ";");
                sb.Append(item.qty + ";");
                sb.Append(item.shift + ";");
                sb.Append(item.fuel_oil_type + ";");
                sb.Append(item.stat_type + ";");
                sb.Append(item.nrp_operator + ";");
                sb.Append(item.nama_operator + ";");
                sb.Append(item.work_area + ";");
                sb.Append(item.location + ";");
                sb.Append(item.ref_condition + ";");
                sb.Append(item.ref_hour_start + ";");
                sb.Append(item.ref_hour_stop + ";");
                sb.Append(item.note + ";");
                sb.Append(item.timezone + ";");
                sb.Append(item.flag_loading + ";");
                sb.Append(item.loadingerror + ";");
                sb.Append(item.loadingdatetime + ";");
                sb.Append(item.mod_by + ";");
                sb.Append(item.mod_date.ToString("yyyy/mm/dd HH:mm:ss") + ";");
                sb.Append(item.flow_meter_start + ";");
                sb.Append(item.flow_meter_end + ";");
                sb.Append(item.driver_nrp + ";");
                sb.Append(item.driver_name + ";");
                sb.Append(item.fuelman_nrp + ";");
                sb.Append(item.fuelman_name + ";");
                //Append new line character.
                sb.Append("\r\n");
            }


            if (File.Exists(pathExcel))
            {
                File.AppendAllText(pathExcel, sb.ToString() + Environment.NewLine);
            }
            else
            {
                File.WriteAllText(pathExcel, sb.ToString());
            }

        }
        void saveDataTransfer()
        {
            try
            {
                var _item = GlobalModel.whTransfer;
                var _lcrData = GlobalModel.lcrData;

                _item.qty_xfer = Convert.ToDouble(lblAutoValueQty.Text.Trim());// isNumeric(txtGrossQTY.Text.Replace(".",",")) ? Convert.ToDouble(txtGrossQTY.Text) : 0;
                _item.fmeter_end = Convert.ToDouble(lblAutoFlowAkhir1.Text.Trim()); //isNumeric(txtGrossTotalizer.Text.Replace(".", ",")) ? Convert.ToDouble(txtGrossTotalizer.Text) : 0;
                _item.fmeter_begin = Convert.ToDouble(lblAutoFLowAwal1.Text.Trim()); // isNumeric(txtTotalizerStart.Text.Replace(".", ",")) ? Convert.ToDouble(txtTotalizerStart.Text) : 0;
                
                var conn = new SQLiteConnection(myConnectionString);
                if (conn.State != ConnectionState.Open) conn.Open();
                string query = string.Empty;
                int recs = 0;
                query = " INSERT INTO whtranfer(job_row_id, xfer_code, xfer_date, dstrct_code, employee_id, issued_whouse, received_whouse, stock_code, qty_xfer, shift, fmeter_serial_no, faktor_meter, fmeter_begin, fmeter_end, created_by, xfer_by, received_by, is_submit, issued_whouse_type, received_whouse_type, mod_date, created_date, timezone, syncs)"
                        + "VALUES(@job_row_id, @xfer_code, @xfer_date, @dstrct_code, @employee_id, @issued_whouse, @received_whouse, @stock_code, @qty_xfer, @shift, @fmeter_serial_no, @faktor_meter, @fmeter_begin, @fmeter_end, @created_by, @xfer_by, @received_by, @is_submit, @issued_whouse_type, @received_whouse_type, @mod_date, @created_date, @timezone, @syncs)";

                SQLiteCommand cmd = new SQLiteCommand(query, conn);

                cmd.Parameters.Add(new SQLiteParameter("@job_row_id", _item.job_row_id));
                cmd.Parameters.Add(new SQLiteParameter("@xfer_code", _item.xfer_code));
                cmd.Parameters.Add(new SQLiteParameter("@xfer_date", _item.xfer_date));
                cmd.Parameters.Add(new SQLiteParameter("@dstrct_code", _item.dstrct_code));
                cmd.Parameters.Add(new SQLiteParameter("@employee_id", _item.employee_id));
                cmd.Parameters.Add(new SQLiteParameter("@issued_whouse", _item.issued_whouse));
                cmd.Parameters.Add(new SQLiteParameter("@received_whouse", _item.received_whouse));
                cmd.Parameters.Add(new SQLiteParameter("@stock_code", _item.stock_code));
                cmd.Parameters.Add(new SQLiteParameter("@qty_xfer", _item.qty_xfer));
                cmd.Parameters.Add(new SQLiteParameter("@shift", _item.shift));
                cmd.Parameters.Add(new SQLiteParameter("@fmeter_serial_no", _item.fmeter_serial_no));
                cmd.Parameters.Add(new SQLiteParameter("@faktor_meter", _item.faktor_meter));
                cmd.Parameters.Add(new SQLiteParameter("@fmeter_begin", _item.fmeter_begin));
                cmd.Parameters.Add(new SQLiteParameter("@fmeter_end", _item.fmeter_end));
                cmd.Parameters.Add(new SQLiteParameter("@created_by", _item.created_by));
                cmd.Parameters.Add(new SQLiteParameter("@xfer_by", _item.xfer_by));
                cmd.Parameters.Add(new SQLiteParameter("@received_by", _item.received_by));
                cmd.Parameters.Add(new SQLiteParameter("@is_submit", _item.is_submit));
                cmd.Parameters.Add(new SQLiteParameter("@issued_whouse_type", _item.issued_whouse_type));
                cmd.Parameters.Add(new SQLiteParameter("@received_whouse_type", _item.received_whouse_type));
                cmd.Parameters.Add(new SQLiteParameter("@mod_date", _item.mod_date));
                cmd.Parameters.Add(new SQLiteParameter("@created_date", _item.created_date));
                cmd.Parameters.Add(new SQLiteParameter("@timezone", _item.timezone));
                cmd.Parameters.Add(new SQLiteParameter("@syncs", _item.syncs));

                recs = cmd.ExecuteNonQuery();

                if (recs > 0)
                {

                }
                if (conn.State == ConnectionState.Open) conn.Close();
            }
            catch (Exception ex)
            {
                save_error_log(ex.ToString(), "saveData()");
                FileToExcel("wh_transfer");
                MessageBox.Show("Data Tidak Tersimpan, Silahkan tulis di logsheet manual", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public async Task<string> checkConnectivity()
        {
            try
            {
                WebClient requestCek = new WebClient();
                var str = requestCek.DownloadString(pUrl_service);
                return str;
            }
            catch (WebException ex)
            {
                return "";
            }
        }

        void saveDataAsync()
        {
            if ((int)Math.Round(double.Parse(lblAutoValueQty.Text.Trim(), new System.Globalization.CultureInfo("id-ID"))) == 0)
            {
                return;
            }

            switch (trans_type_select)
            {
                case "PORT_RECEIVED":
                    savePORT_RECEIVEDAsync();
                    //var checkconnect = await checkConnectivity();
                    //if (checkconnect.Contains("Supply Service"))
                    ////if (statusOnline)
                    //{
                    //    await savePORT_RECEIVEDAsync();
                    //}
                    break;
                case "RECV_DIRECT":
                    saveRECEIVE_DIRECTAsync();
                    break;
                case "ISSUED_MANDIRI":
                    saveISSUED_MANDIRI();
                    break;
                case "PORT_SEND":
                    submitDataPortSend();
                    break;
                //checkconnect = await checkConnectivity();
                //if (checkconnect.Contains("Supply Service"))
                ////if (statusOnline)
                //{

                //}
                case "DARAT_SEND":
                    submitDataPortSend();
                    break;
                case "RECV_DIRECT_OWNER":
                    saveRECEIVE_DIRECTAsync();
                    break;
                    
            }
        }

        async void submitDataPortSend()
        {
            var _auth = GlobalModel.loginModel;
            //String _url = $"{pUrl_service}api/FuelService/sendRitasiToMaintank";
            try
            {
                ListDatumRitasiMaintank.SendQty = (int)Math.Round(double.Parse(lblAutoValueQty.Text.Trim(), new System.Globalization.CultureInfo("id-ID")));
                ListDatumRitasiMaintank.SendQtyXMf = (int)Math.Round(double.Parse(lblAutoValueQty.Text.Trim(), new System.Globalization.CultureInfo("id-ID"))) * (int)Math.Round(double.Parse(ListDatumRitasiMaintank.SendMeterFaktor.ToString().Replace('.', ','), new System.Globalization.CultureInfo("id-ID")));
                ListDatumRitasiMaintank.SendFlowMeterAwal = (int)Math.Round(double.Parse(lblAutoFLowAwal1.Text.Trim(), new System.Globalization.CultureInfo("id-ID")));
                ListDatumRitasiMaintank.SendFlowMeterAkhir = (int)Math.Round(double.Parse(lblAutoFlowAkhir1.Text.Trim(), new System.Globalization.CultureInfo("id-ID")));
                ListDatumRitasiMaintank.SendTimeStart = DateTime.Parse(txtRefStart.Text);
                ListDatumRitasiMaintank.SendTimeEnd =  DateTime.Parse(txtRefStop.Text);

                GlobalModel.ListDatumRitasiMaintank = ListDatumRitasiMaintank;
                if (Application.OpenForms["FSendFTPortForm"] != null)
                    (Application.OpenForms["FSendFTPortForm"] as FSendFTPortForm).pData = ListDatumRitasiMaintank;
                if (Application.OpenForms["FSendPOTForm"] != null)
                    (Application.OpenForms["FSendPOTForm"] as FSendPOTForm).pData = ListDatumRitasiMaintank;
                //returnValue data = await _url.WithHeaders(new { Authorization = _auth.token })
                //       .PostJsonAsync(ListDatumRitasiMaintank).ReceiveJson<returnValue>();

                //if (data.status)
                //{
                //    if (Application.OpenForms["FSendFTPortForm"] != null)
                //        (Application.OpenForms["FSendFTPortForm"] as FSendFTPortForm)._backToCallForm();
                //}
                //else
                //{
                //    MessageBox.Show(data.remarks, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //}
            }
            catch (FlurlHttpException ex)
            {
                var resp = ex.Message;
                if (ex.InnerException != null)
                {
                    resp = ex.InnerException.Message;
                }
                //var resp = await ex.GetResponseStringAsync();
                save_error_log(resp, "submitDataPortSend()");
                MessageBox.Show(resp);

            }
            catch (Exception ex)
            {
                save_error_log(ex.ToString(), "submitDataPortSend()");
                MessageBox.Show("Data Tidak Tersimpan, Silahkan tulis di logsheet manual", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        async Task savePORT_RECEIVEDAsync()
        {
            var _auth = GlobalModel.loginModel;
            var pData = ListDatumRitasiMaintank;

            //String _url = $"{pUrl_service}api/FuelService/receiveRitasiToMaintank";
            try
            {
                pData.ReceiveQty1 = (int)Math.Round(double.Parse(lblAutoValueQty.Text.Trim(), new System.Globalization.CultureInfo("id-ID")));
                pData.receiveQtyTotal = (int)Math.Round(double.Parse(lblAutoValueQty.Text.Trim(), new System.Globalization.CultureInfo("id-ID"))) * (int)Math.Round(double.Parse(pData.ReceiveMeterFaktor.ToString().Replace('.', ','), new System.Globalization.CultureInfo("id-ID"))); ;
                pData.ReceiveFlowMeterAwal1 = (int)Math.Round(double.Parse(lblAutoFLowAwal1.Text.Trim(), new System.Globalization.CultureInfo("id-ID")));
                pData.ReceiveFlowMeterAkhir1 = (int)Math.Round(double.Parse(lblAutoFlowAkhir1.Text.Trim(), new System.Globalization.CultureInfo("id-ID")));
                pData.TimeStart = DateTime.Parse(txtRefStart.Text);
                pData.TimeEnd =   DateTime.Parse(txtRefStop.Text);
                pData.SendQty = (int)Math.Round(double.Parse(lblAutoValueQty.Text.Trim(), new System.Globalization.CultureInfo("id-ID")));
                pData.SendQtyXMf = (int)Math.Round(double.Parse(lblAutoValueQty.Text.Trim(), new System.Globalization.CultureInfo("id-ID")));

                GlobalModel.ListDatumRitasiMaintank = pData;                
                if (Application.OpenForms["FReceivedFTPortForm"] != null)
                    (Application.OpenForms["FReceivedFTPortForm"] as FReceivedFTPortForm).pData = pData;
                //if (Application.OpenForms["FReceivedFTPortForm"] != null)
                //    (Application.OpenForms["FReceivedFTPortForm"] as FReceivedFTPortForm).submitData();


                //returnValue data = await _url.WithHeaders(new { Authorization = _auth.token })
                //   .PostJsonAsync(pData).ReceiveJson<returnValue>();
                //save_error_log(data.remarks, "savePORT_RECEIVEDAsync()");


                //if (!data.status)
                //{
                //    MessageBox.Show(data.remarks, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //    save_error_log(data.remarks, "savePORT_RECEIVEDAsync()");
                //    if (Application.OpenForms["FReceivedFTPortForm"] != null)
                //        (Application.OpenForms["FReceivedFTPortForm"] as FReceivedFTPortForm).pData = pData;
                //}

            }
            catch (FlurlHttpException ex)
            {
                var resp = ex.Message;
                if (ex.InnerException != null)
                {
                    resp = ex.InnerException.Message;
                }
                //var resp = await ex.GetResponseStringAsync();
                save_error_log(resp, "savePORT_RECEIVEDAsync()");
                MessageBox.Show("Data Tidak Tersimpan, Silahkan tulis di logsheet manual", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            catch (Exception ex)
            {
                save_error_log(ex.ToString(), "savePORT_RECEIVEDAsync()");
                MessageBox.Show("Data Tidak Tersimpan, Silahkan tulis di logsheet manual", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        async void saveRECEIVE_DIRECTAsync()
        {
            var _auth = GlobalModel.loginModel;
            var pData = ListReceivedDirectClass;

            //String _url = $"{pUrl_service}api/FuelService/receiveDirect";
            try
            {

                pData.ReceiveQty = (int)Math.Round(double.Parse(lblAutoValueQty.Text.Trim(), new System.Globalization.CultureInfo("id-ID")));
                pData.ReceiveQtyPama = (int)Math.Round(double.Parse(lblAutoValueQty.Text.Trim(), new System.Globalization.CultureInfo("id-ID"))) * (int)Math.Round(double.Parse(pData.ReceiveMeterFaktor.Replace('.', ','), new System.Globalization.CultureInfo("id-ID")));
                pData.TotaliserAwal = (int)Math.Round(double.Parse(lblAutoFLowAwal1.Text.Trim(), new System.Globalization.CultureInfo("id-ID")));
                pData.TotaliserAkhir = (int)Math.Round(double.Parse(lblAutoFlowAkhir1.Text.Trim(), new System.Globalization.CultureInfo("id-ID")));
                pData.start_loading = txtRefStart.Text;
                pData.end_loading = txtRefStop.Text;                                

                GlobalModel.ListReceivedDirectClass = pData;
                if (Application.OpenForms["FReceivedDirectForm"] != null)
                    (Application.OpenForms["FReceivedDirectForm"] as FReceivedDirectForm).pData = pData;
                else if (Application.OpenForms["FReceivedDirectOwnerForm"] != null)
                    (Application.OpenForms["FReceivedDirectOwnerForm"] as FReceivedDirectOwnerForm).pData = pData;

                //if (Application.OpenForms["FReceivedFTPortForm"] != null)
                //    (Application.OpenForms["FReceivedFTPortForm"] as FReceivedFTPortForm).submitData();


                //returnValue data = await _url.WithHeaders(new { Authorization = _auth.token })
                //   .PostJsonAsync(pData).ReceiveJson<returnValue>();
                //save_error_log(data.remarks, "saveRECEIVE_DIRECTAsync()");


                //if (!data.status)
                //{
                //    MessageBox.Show(data.remarks, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //    save_error_log(data.remarks, "saveRECEIVE_DIRECTAsync()");
                //    if (Application.OpenForms["FReceivedDirectForm"] != null)
                //        (Application.OpenForms["FReceivedDirectForm"] as FReceivedDirectForm).pData = pData;
                //}

            }
            catch (FlurlHttpException ex)
            {
                var resp = ex.Message;
                if (ex.InnerException != null)
                {
                    resp = ex.InnerException.Message;
                }
                //var resp = await ex.GetResponseStringAsync();
                save_error_log(resp, "saveRECEIVE_DIRECTAsync()");
                MessageBox.Show("Data Tidak Tersimpan, Silahkan tulis di logsheet manual", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            catch (Exception ex)
            {
                save_error_log(ex.ToString(), "saveRECEIVE_DIRECTAsync()");
                MessageBox.Show("Data Tidak Tersimpan, Silahkan tulis di logsheet manual", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        void saveISSUED_MANDIRI()
        {
            try
            {
                var _item = GlobalModel.logsheet_detail;
                var _lcrData = GlobalModel.lcrData;

                _item.qty = Convert.ToDouble(lblAutoValueQty.Text.Trim());// isNumeric(txtGrossQTY.Text.Replace(".",",")) ? Convert.ToDouble(txtGrossQTY.Text) : 0;
                _item.flow_meter_end = Convert.ToDouble(lblAutoFlowAkhir1.Text.Trim()); //isNumeric(txtGrossTotalizer.Text.Replace(".", ",")) ? Convert.ToDouble(txtGrossTotalizer.Text) : 0;
                _item.flow_meter_start = Convert.ToDouble(lblAutoFLowAwal1.Text.Trim()); // isNumeric(txtTotalizerStart.Text.Replace(".", ",")) ? Convert.ToDouble(txtTotalizerStart.Text) : 0;
                _item.qty_loqsheet = Convert.ToDouble(lblAutoValueQty.Text.Trim());
                var max_tank = preset / 10;
                _item.max_tank_capacity = max_tank;
                _item.ref_hour_start = txtRefStart.Text;
                _item.ref_hour_stop = txtRefStop.Text;
                _item.stat_type = stat_type;
                //_item.is_load_to_fosto = 99;

                SQLiteConnection conn;

                conn = new SQLiteConnection(myConnectionString);
                if (conn.State != ConnectionState.Open) conn.Open();
                string query = string.Empty;
                int recs = 0;
                query = "INSERT INTO tbl_t_log_sheet_detail(job_row_id,dstrct_code,log_sheet_code,input_type,issued_date,whouse_id,unit_no,max_tank_capacity,hm_before,hm,flw_meter,meter_faktor,qty_loqsheet,qty,shift,fuel_oil_type,stat_type,nrp_operator,nama_operator,work_area,location,ref_condition,ref_hour_start,ref_hour_stop,note,timezone,flag_loading,loadingerror,loadingdatetime,mod_by,mod_date,is_load_to_fosto,foto,flow_meter_start,flow_meter_end,resp_code,resp_name,text_header,text_sub_header,text_body,syncs)"
                      + " VALUES(@job_row_id,@dstrct_code,@log_sheet_code,@input_type,@issued_date,@whouse_id,@unit_no,@max_tank_capacity,@hm_before,@hm,@flw_meter,@meter_faktor,@qty_loqsheet,@qty,@shift,@fuel_oil_type,@stat_type,@nrp_operator,@nama_operator,@work_area,@location,@ref_condition,@ref_hour_start,@ref_hour_stop,@note,@timezone,@flag_loading,@loadingerror,@loadingdatetime,@mod_by,@mod_date,@is_load_to_fosto,@foto,@flow_meter_start,@flow_meter_end,@resp_code,@resp_name,@text_header,@text_sub_header,@text_body,@syncs)";

                SQLiteCommand cmd = new SQLiteCommand(query, conn);

                cmd.Parameters.Add(new SQLiteParameter("@job_row_id", _item.job_row_id));
                cmd.Parameters.Add(new SQLiteParameter("@dstrct_code", _item.dstrct_code));
                cmd.Parameters.Add(new SQLiteParameter("@log_sheet_code", _item.log_sheet_code));
                cmd.Parameters.Add(new SQLiteParameter("@input_type", _item.input_type));
                cmd.Parameters.Add(new SQLiteParameter("@issued_date", _item.issued_date));
                cmd.Parameters.Add(new SQLiteParameter("@whouse_id", _item.whouse_id));
                cmd.Parameters.Add(new SQLiteParameter("@unit_no", _item.unit_no));
                cmd.Parameters.Add(new SQLiteParameter("@max_tank_capacity", _item.max_tank_capacity));
                cmd.Parameters.Add(new SQLiteParameter("@hm_before", _item.hm_before));
                cmd.Parameters.Add(new SQLiteParameter("@hm", _item.hm));
                cmd.Parameters.Add(new SQLiteParameter("@flw_meter", _item.flw_meter));
                cmd.Parameters.Add(new SQLiteParameter("@meter_faktor", _item.meter_faktor));
                cmd.Parameters.Add(new SQLiteParameter("@qty_loqsheet", _item.qty_loqsheet));
                cmd.Parameters.Add(new SQLiteParameter("@qty", _item.qty));
                cmd.Parameters.Add(new SQLiteParameter("@shift", _item.shift));
                cmd.Parameters.Add(new SQLiteParameter("@fuel_oil_type", _item.fuel_oil_type));
                cmd.Parameters.Add(new SQLiteParameter("@stat_type", _item.stat_type));
                cmd.Parameters.Add(new SQLiteParameter("@nrp_operator", _item.nrp_operator));
                cmd.Parameters.Add(new SQLiteParameter("@nama_operator", _item.nama_operator));
                cmd.Parameters.Add(new SQLiteParameter("@work_area", _item.work_area));
                cmd.Parameters.Add(new SQLiteParameter("@location", _item.location));
                cmd.Parameters.Add(new SQLiteParameter("@ref_condition", _item.ref_condition));
                cmd.Parameters.Add(new SQLiteParameter("@ref_hour_start", _item.ref_hour_start));
                cmd.Parameters.Add(new SQLiteParameter("@ref_hour_stop", _item.ref_hour_stop));
                cmd.Parameters.Add(new SQLiteParameter("@note", _item.note));
                cmd.Parameters.Add(new SQLiteParameter("@timezone", _item.timezone));
                cmd.Parameters.Add(new SQLiteParameter("@flag_loading", _item.flag_loading));
                cmd.Parameters.Add(new SQLiteParameter("@loadingerror", _item.loadingerror));
                cmd.Parameters.Add(new SQLiteParameter("@loadingdatetime", _item.loadingdatetime));
                cmd.Parameters.Add(new SQLiteParameter("@mod_by", _item.mod_by));
                cmd.Parameters.Add(new SQLiteParameter("@mod_date", _item.mod_date));
                cmd.Parameters.Add(new SQLiteParameter("@is_load_to_fosto", _item.is_load_to_fosto));
                cmd.Parameters.Add(new SQLiteParameter("@foto", _item.foto));
                cmd.Parameters.Add(new SQLiteParameter("@flow_meter_start", _item.flow_meter_start));
                cmd.Parameters.Add(new SQLiteParameter("@flow_meter_end", _item.flow_meter_end));
                cmd.Parameters.Add(new SQLiteParameter("@resp_code", _item.resp_code));
                cmd.Parameters.Add(new SQLiteParameter("@resp_name", _item.resp_name));
                cmd.Parameters.Add(new SQLiteParameter("@text_header", _item.text_header));
                cmd.Parameters.Add(new SQLiteParameter("@text_sub_header", _item.text_sub_header));
                cmd.Parameters.Add(new SQLiteParameter("@text_body", _item.text_body));
                cmd.Parameters.Add(new SQLiteParameter("@syncs", _item.syncs));

                recs = cmd.ExecuteNonQuery();

                if (recs > 0)
                {
                    //if (Application.OpenForms["FTransaction"] != null)
                    //{
                    //    (Application.OpenForms["FTransaction"] as FTransaction).clearData();
                    //}

                    //this.Close();
                }
                if (conn.State == ConnectionState.Open) conn.Close();
            }
            catch (Exception ex)
            {
                save_error_log(ex.ToString(), "saveData()");
                FileToExcel("refueling_pitstop");
                MessageBox.Show("Data Tidak Tersimpan, Silahkan tulis di logsheet manual", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        void save_error_log(string data, string actions)
        {
            try
            {
                DateTime localDate = DateTime.Now;
                var _item = new tbl_log();
                var initData = GlobalModel.GlobalVar;

                _item.pid = System.Guid.NewGuid().ToString();
                _item.actions = $"District {initData.DataEmp.DstrctCode}/FmManual/{actions}";
                _item.message = $"{data}";
                _item.mod_date = localDate;
                _item.mod_by = initData.DataEmp.Nrp;

                SQLiteConnection conn;

                conn = new SQLiteConnection(myConnectionString);
                if (conn.State != ConnectionState.Open) conn.Open();
                string query = string.Empty;
                query = "INSERT INTO tbl_log(pid,actions,message,mod_date,mod_by)"
                      + " VALUES(@pid,@actions,@message,@mod_date,@mod_by)";

                SQLiteCommand cmd = new SQLiteCommand(query, conn);

                cmd.Parameters.Add(new SQLiteParameter("@pid", _item.pid));
                cmd.Parameters.Add(new SQLiteParameter("@actions", _item.actions));
                cmd.Parameters.Add(new SQLiteParameter("@message", _item.message));
                cmd.Parameters.Add(new SQLiteParameter("@mod_date", _item.mod_date));
                cmd.Parameters.Add(new SQLiteParameter("@mod_by", _item.mod_by));
                cmd.ExecuteNonQuery();
                if (conn.State == ConnectionState.Open) conn.Close();
            }
            catch (Exception)
            {
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int presetVal = 0;
            if (int.TryParse(txtPreset.Text, out presetVal))
            {
                res = lcr.SetPresetGrossVolume(presetVal);

                lblManualValue.Text = "";
                if (res.issuccess)
                {
                    lblManualStatus.Text = "Set Preset Gross Volume Success";
                    lblManualValue.Text = res.value;
                }
                else
                {
                    lblManualStatus.Text = "Set Preset Gross Volume Failed";
                    lblManualValue.Text = "No Value";
                }
            }
            else
            {
                lblManualStatus.Text = "Invalid Preset Gross Volume!";
                lblManualValue.Text = "";
            }
        }

        
    }
}
