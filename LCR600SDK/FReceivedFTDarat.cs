﻿using Flurl.Http;
using LCR600SDK.DataClass;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LCR600SDK
{
    public partial class FReceivedFTDarat : Form
    {
        private string URL_API = string.Empty;
        private string pUrl_service = string.Empty;

        public FReceivedFTDarat()
        {
            InitializeComponent();
        } 

        private void FReceivedFTPort_Load(object sender, EventArgs e)
        {
            var _proper = Properties.Settings.Default;            
            pUrl_service = _proper.url_service;
            URL_API = $"{pUrl_service}api/FuelService/getDeliveryDaratReceiveDesktop";
            String _url = $"{URL_API}?page=1&pageSize=50&activity=darat";

            loadGridAsync(_url);
        }
        void dataGridView1_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            dataGridView1.BorderStyle = BorderStyle.None;
            dataGridView1.AlternatingRowsDefaultCellStyle.BackColor = Color.FromArgb(238, 239, 249);
            dataGridView1.CellBorderStyle = DataGridViewCellBorderStyle.SingleHorizontal;
            dataGridView1.DefaultCellStyle.SelectionBackColor = Color.SeaGreen;
            dataGridView1.DefaultCellStyle.SelectionForeColor = Color.WhiteSmoke;
            dataGridView1.BackgroundColor = Color.FromArgb(0, 101, 179);
            dataGridView1.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridView1.EnableHeadersVisualStyles = false;
            dataGridView1.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
            dataGridView1.ColumnHeadersDefaultCellStyle.Font = new Font("MS Reference Sans Serif", 20);
            dataGridView1.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(0, 101, 179);
            dataGridView1.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;            
        }

        private void dataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs args)
        {
            if (args.ColumnIndex == 5 || args.ColumnIndex == 6)
            {
                var SendDate = args.Value.ToString();
                args.Value = DateTime.Parse(SendDate).ToString("dd-MM-yyyy");
                args.FormattingApplied = true;
            }            
        }
        public async void loadGridAsync(string _url)
        {
            var _auth = GlobalModel.loginModel;
            
            try
            {
                var datas = await _url.WithHeaders(new { Authorization = _auth.token }).GetJsonAsync<PenerimaanFtDaratClass>();
                GlobalModel.penerimaanFtDarat = datas;                
                setPaging();                
                DataTable dt = GeneralFunc.ToDataTable(GlobalModel.penerimaanFtDarat.ListData);
                dataGridView1.AutoGenerateColumns = false;
                dataGridView1.DataSource = dt;
                for (int i = 0; i < dataGridView1.RowCount; i++)
                {
                    DataGridViewButtonCell cell = new DataGridViewButtonCell();             
                    dataGridView1[0, i] = cell;

                    if (dataGridView1[0, i] == cell)
                    {
                        cell.Value = "Terima";
                        cell.FlatStyle = FlatStyle.Flat;
                        cell.Style.BackColor = Color.SeaGreen;
                        cell.Style.ForeColor = Color.WhiteSmoke;
                        cell.Style.SelectionBackColor = Color.SeaGreen;
                    }
                    //if (dataGridView1[0, i] is DataGridViewButtonCell cell)
                    //{
                    //    cell.Value = "Terima";
                    //    cell.FlatStyle = FlatStyle.Flat;
                    //    cell.Style.BackColor = Color.SeaGreen;
                    //    cell.Style.ForeColor = Color.WhiteSmoke;
                    //    cell.Style.SelectionBackColor = Color.SeaGreen;
                    //}
                }
                Console.WriteLine("Get token Success");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        void setPaging()
        { 
            try
            {
                var _data = GlobalModel.penerimaanFtDarat;
                txtPage.Text = $"{_data.CurrentPage}";
                lblPageOf.Text = $"Page {_data.CurrentPage} of {_data.TotalPage}    - Total rows {_data.TotalSize}";
                btnNext.Enabled = (_data.TotalPage > _data.CurrentPage);
                btnPrev.Enabled = (_data.CurrentPage > 1);
            }
            catch (Exception)
            {
                MessageBox.Show("Tidak ada data untuk di tampilan", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.Columns[e.ColumnIndex].Name == "Action")
            {
                var _data = GlobalModel.penerimaanFtDarat.ListData[e.RowIndex];
                var f = new FReceivedFTDaratSub();
                f.pPidPenerimaan = _data.PidDeliveryDarat;
                f.pPoNO = _data.PoNo;
                f.WindowState = FormWindowState.Minimized;
                f.WindowState = FormWindowState.Maximized;
                f.ShowDialog();
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            String _url = $"{URL_API}?page=1&pageSize=50&activity=darat&search={txtSearch.Text}";
            loadGridAsync(_url);
        }

        private void txtSearch_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            {
                String _url = $"{URL_API}?page=1&pageSize=50&activity=darat&search={txtSearch.Text}";
                loadGridAsync(_url);
            }
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            String _url = $"{URL_API}?page={Convert.ToInt32(txtPage.Text)+1}&pageSize=50&activity=darat&search={txtSearch.Text}";
            loadGridAsync(_url);
        }

        private void btnPrev_Click(object sender, EventArgs e)
        {
            String _url = $"{URL_API}?page={Convert.ToInt32(txtPage.Text)- 1}&pageSize=50&activity=darat&search={txtSearch.Text}";
            loadGridAsync(_url);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
    }
}
