﻿using Flurl.Http;
using LCR600SDK.DataClass;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LCR600SDK
{
    public partial class FRefPODirect : Form
    {
        private List<PoReceivedDirectClass> _dataResult;
        private string pUrl_service = string.Empty;

        public FRefPODirect()
        {
            InitializeComponent();
        }

        private void FRefPODirect_Load(object sender, EventArgs e)
        {
            var _proper = Properties.Settings.Default;
            pUrl_service = _proper.url_service;
            loadGridAsync(); 
        }

        public async void loadGridAsync()
        {            
            try
            {                
                var _data = GlobalModel.GlobalVar.PoReceiveDirect;                

                GlobalModel.poReceivedDirect = GlobalModel.GlobalVar.PoReceiveDirect;
                _dataResult = _data;
                DataTable dt = GeneralFunc.ToDataTable(_data);
                dataGridView1.DataSource = dt;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            var _list = GlobalModel.poReceivedDirect;
            _dataResult = _list.Where(d => d.PoNo.Contains(txtSearch.Text.Trim().ToUpper())).ToList();
            DataTable dt = GeneralFunc.ToDataTable(_dataResult);
            dataGridView1.DataSource = dt;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.Columns[e.ColumnIndex].Name == "Action")
            {
                if (Application.OpenForms["FReceivedDirectForm"] != null)
                {
                    var _from = (Application.OpenForms["FReceivedDirectForm"] as FReceivedDirectForm);
                    _from.txtPoNo.Text = _dataResult[e.RowIndex].PoNo;
                    _from.txtDistrik.Text = _dataResult[e.RowIndex].Dstrct;
                    _from.PoQty = _dataResult[e.RowIndex].PoQty;
                    _from.pPidPO = _dataResult[e.RowIndex].PidPo;
                }
                else if (Application.OpenForms["FReceivedDirectOwnerForm"] != null)
                { 
                    var _from = (Application.OpenForms["FReceivedDirectOwnerForm"] as FReceivedDirectOwnerForm);
                    _from.txtPoNo.Text = _dataResult[e.RowIndex].PoNo;
                    _from.txtDistrik.Text = _dataResult[e.RowIndex].Dstrct;
                    _from.PoQty = _dataResult[e.RowIndex].PoQty;
                    _from.pPidPO = _dataResult[e.RowIndex].PidPo;                    
                }
                this.Close();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
    }
}
