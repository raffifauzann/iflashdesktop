﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LCR600SDK
{
    public partial class FPopUpNotification : Form
    {
        private string _message = string.Empty;
        private string _title = string.Empty;
        public FPopUpNotification(string message,string title)
        {
            InitializeComponent();
            _message = message;
            _title = title;
        }

        private void FPopUpNotification_Load(object sender, EventArgs e)
        {
            this.Text = _title;
            textBox1.Text = _message.Replace("\n",Environment.NewLine);             
        }

        private void btnOke_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
    }
}
