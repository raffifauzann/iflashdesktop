﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace SDK
{
    class LCPAPI
    {
        #region Constant Declaration

        public const byte LCP_PID_Host              = 0;         // LCP product ID for any host application

        public const UInt32 LCP_MsgOverhead         = 8U;        // overhead of a LCP message
        public const UInt32 LCP_MsgOverheadLimit    = 14U;       // maximum overhead of a LCP message
        public const UInt32 LCP_MsgSizeLimit        = 523U;      // maximum size of a LCP message
        public const UInt32 LCP_DataSizeLimit       = 255U;      // maximum size of the data portion of a LCP message

        public const UInt32 LCP_BroadcastNode       = 0U;        // broadcast node address
        public const UInt32 LCP_NodeAddressLimit    = 255U;      // maximum allowed LCP node address

        public const UInt32 LCPZ_NameAndRevision    = 15U;       // maximum length of name and revision string

        #endregion Constant Declaration

        #region Enumeration Declaration

        //=======================
        // Enum definitions.
        //=======================

        // LCP message status bit definitions.

        public enum STATUS
        {
            LCP_StatusMsgID        = 0x01,      // mask of message ID in the LCP status byte
            LCP_StatusSynchronize  = 0x02,      // flag indicating message ID synchronization is needed
            LCP_StatusCheckRequest = 0x04,      // check the active request
            LCP_StatusBusy         = 0x04,      // slave is busy but will respond as soon as possible
            LCP_StatusAbortRequest = 0x08,      // abort the active request
            LCP_StatusAborted      = 0x08,      // the active request was aborted
            LCP_StatusNoRequest    = 0x10,      // no request is active
            LCP_StatusOverrun      = 0x20,      // message overrun bit in the LCP status byte
            LCP_StatusUnsupported  = 0x40,      // message ID unsupported bit in the LCP status byte
            LCP_StatusReply        = 0x80       // reply bit in the LCP status byte
        }

        // LCP baud rate definitions.

        public enum BAUD
        {
            LCP_Baud115200 = 0,                // 115.2K baud
            LCP_Baud57600  = 1,                //  57.6K baud
            LCP_Baud19200  = 2,                //  19.2K baud
            LCP_Baud9600   = 3,                //   9.6K baud
            LCP_Baud4800   = 4,                //   4.8K baud
            LCP_Baud2400   = 5,                //   2.4K baud
            LCP_NumBauds   = 6                 // nuumber of supported baud rates
        }

        // LCP transmit enable definitions.

        public enum TXENABLE
        {
            LCP_TXEnableNone     = 0,          // transmit enable signal is not used
            LCP_TXEnable_RTS     = 1,          // transmit enabled via the !RTS line
            LCP_TXEnableRTS      = 2,          // transmit enabled via the RTS line
            LCP_TXEnable_DTR     = 3,          // transmit enabled via the !DTR line
            LCP_TXEnableDTR      = 4,          // transmit enabled via the DTR line
            LCP_TXEnable_RTS_DTR = 5,          // transmit enabled via the !RTS and !DTR line
            LCP_TXEnableRTSDTR   = 6,          // transmit enabled via the RTS and DTR line
            LCP_NumTXEnables     = 7           // number of transmit enable signals supported
        }

        // LCP return code definitions.

        public enum LCPR
        {
            LCPR_OK                 =   0,  // operation was carried out successfully
            LCPR_AlreadyInstalled   = 200,  // the LCP device drivers are already installed
            LCPR_BadNameAndRevision = 201,  // invalid name and revision string
            LCPR_BadNodeAddress     = 202,  // invalid node address of host application
            LCPR_BadBaseAddress     = 203,  // invalid base communications address
            LCPR_BadIRQ             = 204,  // invalid IRQ
            LCPR_BadBaudRate        = 205,  // invalid baud rate
            LCPR_BadTimeout         = 206,  // invalid timeout
            LCPR_BadTXEnable        = 207,  // invalid transmit enable
            LCPR_NULLPointer        = 208,  // a NULL pointer was passed as an address
            LCPR_NoRequestActive    = 209,  // the slave device reported that no request was active
            LCPR_NoResponse         = 210,  // the slave device did not respond to the request
            LCPR_RequestAborted     = 211,  // the slave device aborted the last request
            LCPR_UnsupportedCommand = 212,  // the slave device reports an unsupported command
            LCPR_BufferOverflow     = 213,  // the slave device reports its receive buffer overflowed
            LCPR_Busy               = 214,  // the slave device reports that it is busy but is working on the request
            LCPR_NotInstalled       = 215,  // the LCP device drivers are not installed
            LCPR_MemoryAllocation   = 216,  // there is not enough dynamic memory available for the request
            LCPR_MissingMHz         = 217,  // the MHz environment variable is missing
            LCPR_MessageSize        = 218,  // the response message contains the incorrect number of bytes
            LCPR_WinSockErr         = 219,  // there was a problem initializing winsock
            LCPR_SocketErr          = 220,  // there was a problem initializing socket
            LCPR_BadIPAddress       = 221,  // invalid IP Addr 
            LCPR_BadIPPort          = 222   // invalid IP Port 
        };

        // LCP IP version definitions.

        public enum IPVERSION
        {
            LCP_Default = 0,                    // no IP is used
            LCP_IPv4    = 4,                    // LCP IP address is IPv4
            LCP_IPv6    = 6                     // LCP IP address is IPv6
        }

        // LCR connection status definitions.

        public enum CONNECTION
        {
            LCP_CONN_NEW     = 0x01,
            LCP_CONN_ACTIVE  = 0x02, 
            LCP_CONN_PENDING = 0x04
        }

        #endregion Enumeration Declaration

        #region Structure Declaration

        [StructLayout(LayoutKind.Explicit)]
        public struct LCPIP_ADDR
        {
            public LCPIP_ADDR(IPVERSION version)
            {
                IPv4      = 0;
                IPv61     = 0;
                IPv62     = 0;
                IPv63     = 0;
                IPv64     = 0;
                IPv65     = 0;
                IPv66     = 0;
                IPv67     = 0;
                IPv68     = 0;
                IPversion = version;
            }

            [FieldOffset(0)]
            public uint IPv4;                // IPv4 address being used
            [FieldOffset(0)]
            public ushort IPv61;             // IPv6 address being used
            [FieldOffset(2)]
            public ushort IPv62;             // IPv6 address being used
            [FieldOffset(4)]
            public ushort IPv63;             // IPv6 address being used
            [FieldOffset(6)]
            public ushort IPv64;             // IPv6 address being used
            [FieldOffset(8)]
            public ushort IPv65;             // IPv6 address being used
            [FieldOffset(10)]
            public ushort IPv66;             // IPv6 address being used
            [FieldOffset(12)]
            public ushort IPv67;             // IPv6 address being used
            [FieldOffset(14)]
            public ushort IPv68;             // IPv6 address being used
            [FieldOffset(16)]
            public IPVERSION IPversion;      // IP version being used
        }

        #endregion Structure Declaration

        #region Function Declaration

        /// <summary>
        /// LCP function defintions
        /// </summary>
        
        [DllImport("LCLCP32.dll")] public static extern byte LCPAbortRequest(byte txTo);
        [DllImport("LCLCP32.dll")] public static extern byte LCPCheckRequest(out byte txTo, IntPtr rxData, out byte txTorxDataLen);
        [DllImport("LCLCP32.dll")] public static extern byte LCPGetProductInformation(byte txTo, IntPtr PID, IntPtr name);
        [DllImport("LCLCP32.dll")] public static extern byte LCPGetRequest (out byte txTo, out byte rxFrom, IntPtr rxData, IntPtr rxDataLen);
        [DllImport("LCLCP32.dll")] public static extern void LCPGetVersion(IntPtr version);
        [DllImport("LCLCP32.dll")] public static extern void LCPGetInstallStatus(out byte status);
        [DllImport("LCLCP32.dll")] public static extern void LCPReverseBytes(IntPtr fieldData, Int16 fieldSize);
        [DllImport("LCLCP32.dll")] public static extern byte LCPSendRequest(byte txTo, IntPtr txData, byte txDataLen, IntPtr rxData, out byte rxDataLen); 
        [DllImport("LCLCP32.dll")] public static extern byte LCPSendResponse(byte txTo, IntPtr txData, byte txDataLen, byte status);

        [DllImport("LCLCP32.dll")] public static extern byte LCPInstall(byte PID, string name, byte nodeAddr, byte[] portName, byte baudRateIX, byte txEnable, ushort timeout, byte retries);
        [DllImport("LCLCP32.dll")] public static extern byte LCPUninstall();

        /// <summary>
        /// LCP IP function defintions
        /// </summary>
        /// 

        [DllImport("LCLCP32.dll")] public static extern byte LCPIPAbortRequest(byte txTo, IntPtr txIPInfo);
        [DllImport("LCLCP32.dll")] public static extern byte LCPIPCheckRequest(out byte txTo, IntPtr txIPInfo, IntPtr rxData, out byte txTorxDataLen);
        [DllImport("LCLCP32.dll")] public static extern byte LCPIPGetProductInformation(byte txTo, IntPtr txIPInfo, IntPtr PID, IntPtr name);
        [DllImport("LCLCP32.dll")] public static extern byte LCPIPGetRequest(out byte txTo, IntPtr rxFromIPInfo, out byte rxFrom, IntPtr rxData, out byte rxDataLen);
        [DllImport("LCLCP32.dll")] public static extern byte LCPIPSendRequest(byte txTo, IntPtr txIPInfo, IntPtr txData, byte txDataLen, IntPtr rxData, out byte rxDataLen);
        [DllImport("LCLCP32.dll")] public static extern byte LCPIPSendResponse(byte txTo, IntPtr txIPInfo, IntPtr txData, byte txDataLen, byte status);

        [DllImport("LCLCP32.dll")] public static extern byte LCPIPInstall(byte PID, string name, byte nodeAddr, ushort timeout, byte retries, int localIPPort);
        [DllImport("LCLCP32.dll")] public static extern byte LCPMakeIPNodeKnown(byte node, IntPtr nodeIPInfo, int nodeIPPort);
        [DllImport("LCLCP32.dll")] public static extern byte LCPIPGetConnectionStatus(byte node, IntPtr nodeIPInfo, out byte status);
        [DllImport("LCLCP32.dll")] public static extern byte LCPIPSetConnectionStatus(byte node, IntPtr nodeIPInfo, byte status);
        [DllImport("LCLCP32.dll")] public static extern byte LCPIPUninstall();


        #endregion Function Declaration
    }
}
