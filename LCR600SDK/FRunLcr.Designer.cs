﻿namespace LCR600SDK
{
    partial class FRunLcr
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtTotalizerStart = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lblAlertClosed = new System.Windows.Forms.Label();
            this.txtTotalizerGross = new System.Windows.Forms.TextBox();
            this.txtRefStart = new System.Windows.Forms.TextBox();
            this.txtRefStop = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnStop = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.txtGrossQty = new System.Windows.Forms.TextBox();
            this.timerOpen = new System.Windows.Forms.Timer(this.components);
            this.timerQty = new System.Windows.Forms.Timer(this.components);
            this.timerClose = new System.Windows.Forms.Timer(this.components);
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtTotalizerStart);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.lblAlertClosed);
            this.panel1.Controls.Add(this.txtTotalizerGross);
            this.panel1.Controls.Add(this.txtRefStart);
            this.panel1.Controls.Add(this.txtRefStop);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(871, 275);
            this.panel1.TabIndex = 0;
            // 
            // txtTotalizerStart
            // 
            this.txtTotalizerStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalizerStart.Location = new System.Drawing.Point(17, 100);
            this.txtTotalizerStart.Name = "txtTotalizerStart";
            this.txtTotalizerStart.Size = new System.Drawing.Size(187, 29);
            this.txtTotalizerStart.TabIndex = 16;
            this.txtTotalizerStart.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 72);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 25);
            this.label4.TabIndex = 15;
            this.label4.Text = "label4";
            this.label4.Visible = false;
            // 
            // lblAlertClosed
            // 
            this.lblAlertClosed.AutoSize = true;
            this.lblAlertClosed.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAlertClosed.ForeColor = System.Drawing.Color.Red;
            this.lblAlertClosed.Location = new System.Drawing.Point(133, 230);
            this.lblAlertClosed.Name = "lblAlertClosed";
            this.lblAlertClosed.Size = new System.Drawing.Size(781, 42);
            this.lblAlertClosed.TabIndex = 14;
            this.lblAlertClosed.Text = "TRANSACTION CLOSED.. PLEASE WAIT..";
            this.lblAlertClosed.Visible = false;
            // 
            // txtTotalizerGross
            // 
            this.txtTotalizerGross.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTotalizerGross.Enabled = false;
            this.txtTotalizerGross.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalizerGross.Location = new System.Drawing.Point(672, 100);
            this.txtTotalizerGross.Name = "txtTotalizerGross";
            this.txtTotalizerGross.Size = new System.Drawing.Size(187, 29);
            this.txtTotalizerGross.TabIndex = 5;
            // 
            // txtRefStart
            // 
            this.txtRefStart.Enabled = false;
            this.txtRefStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRefStart.Location = new System.Drawing.Point(17, 37);
            this.txtRefStart.Name = "txtRefStart";
            this.txtRefStart.Size = new System.Drawing.Size(187, 29);
            this.txtRefStart.TabIndex = 4;
            // 
            // txtRefStop
            // 
            this.txtRefStop.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtRefStop.Enabled = false;
            this.txtRefStop.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRefStop.Location = new System.Drawing.Point(672, 37);
            this.txtRefStop.Name = "txtRefStop";
            this.txtRefStop.Size = new System.Drawing.Size(187, 29);
            this.txtRefStop.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(701, 72);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(158, 25);
            this.label3.TabIndex = 2;
            this.label3.Text = "Totalizer Gross";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(764, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 25);
            this.label2.TabIndex = 1;
            this.label2.Text = "Ref Stop";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Ref Start";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnStop);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 182);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(871, 132);
            this.panel2.TabIndex = 1;
            // 
            // btnStop
            // 
            this.btnStop.BackColor = System.Drawing.Color.Red;
            this.btnStop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnStop.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStop.ForeColor = System.Drawing.Color.White;
            this.btnStop.Location = new System.Drawing.Point(0, 0);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(871, 132);
            this.btnStop.TabIndex = 0;
            this.btnStop.Text = "STOP";
            this.btnStop.UseVisualStyleBackColor = false;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.txtGrossQty);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 275);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(871, 0);
            this.panel3.TabIndex = 2;
            // 
            // txtGrossQty
            // 
            this.txtGrossQty.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtGrossQty.Enabled = false;
            this.txtGrossQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 140.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGrossQty.Location = new System.Drawing.Point(0, 0);
            this.txtGrossQty.Name = "txtGrossQty";
            this.txtGrossQty.Size = new System.Drawing.Size(871, 219);
            this.txtGrossQty.TabIndex = 0;
            this.txtGrossQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // timerOpen
            // 
            this.timerOpen.Interval = 1000;
            this.timerOpen.Tick += new System.EventHandler(this.timerOpen_Tick);
            // 
            // timerQty
            // 
            this.timerQty.Interval = 1000;
            this.timerQty.Tick += new System.EventHandler(this.timerQty_Tick);
            // 
            // timerClose
            // 
            this.timerClose.Interval = 1000;
            this.timerClose.Tick += new System.EventHandler(this.timerClose_Tick);
            // 
            // FRunLcr
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(871, 314);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FRunLcr";
            this.Text = "IFlash - Running";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FRunLcr_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FRunLcr_FormClosed);
            this.Load += new System.EventHandler(this.FRunLcr_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtTotalizerGross;
        private System.Windows.Forms.TextBox txtRefStart;
        private System.Windows.Forms.TextBox txtRefStop;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox txtGrossQty;
        private System.Windows.Forms.Label lblAlertClosed;
        private System.Windows.Forms.TextBox txtTotalizerStart;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Timer timerOpen;
        private System.Windows.Forms.Timer timerQty;
        private System.Windows.Forms.Timer timerClose;
    }
}