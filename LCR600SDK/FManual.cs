﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LCR600SDK
{
    public partial class FManual : Form
    {

        public string no_unit = "";

        public FManual()
        {
            InitializeComponent();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                
                string value = txtInput.Text;
                no_unit = value;
                //if (Application.OpenForms["FTransaction"] != null)
                //{
                //    (Application.OpenForms["FTransaction"] as FTransaction).txtUnitNo.Text = value;
                //}

                //if (Application.OpenForms["FRefuellingSerial"] != null)
                //{
                //    (Application.OpenForms["FRefuellingSerial"] as FRefuellingSerial).txtUnit.Text = value;
                //}
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtInput_Click(object sender, EventArgs e)
        {
            try
            {
                ProcessStartInfo psStartInfo = new ProcessStartInfo();
                //psStartInfo.FileName = @"C:\Program Files\Common Files\microsoft shared\ink\TabTip.exe";
                var dir = Path.Combine(Environment.CurrentDirectory, "VKeyboard", "VKeyboard.exe");
                psStartInfo.FileName = dir;

                Process ps = Process.Start(psStartInfo);

            }
            catch (Exception ex)
            {
                var error = ex.Message;
            } 
        }
    }
}
