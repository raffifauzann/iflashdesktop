﻿using LCR600SDK.DataClass;
using MySql.Data.MySqlClient;
using System.Data.SQLite;
using System.IO;
using System.Reflection;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;
using System.Diagnostics;
using System.Net;

namespace LCR600SDK
{
    public partial class FRefuellingSerial : Form
    { 
        private string myConnectionString = string.Empty;
        private string pWHouse = string.Empty;
        private string pSnCOde = string.Empty;
        private string pShiftStart1 = string.Empty;
        private string pShiftStart2 = string.Empty;
        private int start_mqtt = 0;
        private int start_rfid_read = 0;
        private int start_search = 0;        
        private string input_type = string.Empty;
        private string driver_nrp = string.Empty;
        private string driver_name = string.Empty;
        private string fuelman_name = string.Empty;
        private string nrp_fuelman = string.Empty;
        private string pUrl_rfid_tag = string.Empty;
        
        GetConsoleRFID cslRFID = new GetConsoleRFID();
        // MQTT init
        MqttClient mqttClient;
        private string lastUnittagid = string.Empty;
        private bool checkUnitProcessing = false;
        PasswordEncrypDecrypt decryptor = new PasswordEncrypDecrypt();
        private string unitTag = string.Empty;
        private string nozzleTag = string.Empty;
        WriteLogText log = new WriteLogText();
        public FRefuellingSerial()
        {
            var _proper = Properties.Settings.Default;
            _proper.maxTickSecond = 3;
            pUrl_rfid_tag = _proper.url_rfid_local;
            //myConnectionString = "Data Source=" + Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "SQLite", "db_iflash.db") + " ;New=False;";
            var passPhrase = "iFlashDesktop";
            var decryp = decryptor.DecryptPassword(_proper.db_pass, passPhrase);
            myConnectionString = "Data Source=" + Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "SQLite", "db_iflash.db") + " ;New=False;";
            GlobalModel.AutoRefueling = new refueling();
            InitializeComponent();
            
        }
        void startConsoleRFID()
        {
            cslRFID.startCommandConsole();           
        }
        private void FRefuellingSerial_Load(object sender, EventArgs e)
        {
            initData();
            //using_vw_rfid_db();
            start_read();
            getDriverFuelman();
            //initMqtt();
        }

        void getDriverFuelman()
        {
            SQLiteConnection conn = new SQLiteConnection(myConnectionString);

            try
            {
                if (conn.State != ConnectionState.Open) conn.Open();
                string query = string.Empty;

                query = $"SELECT * FROM tbl_user_login where login_as = 'Driver'";
                SQLiteCommand cmds = new SQLiteCommand(query, conn);
                SQLiteDataReader reader = cmds.ExecuteReader();

                while (reader.Read())
                {
                    driver_nrp = reader["nrp"].ToString();
                    driver_name = reader["name"].ToString();
                }

                query = $"SELECT * FROM tbl_user_login where login_as = 'Fuelman'";
                cmds = new SQLiteCommand(query, conn);
                reader = cmds.ExecuteReader();

                while (reader.Read())
                {
                    nrp_fuelman = reader["nrp"].ToString();
                    fuelman_name = reader["name"].ToString();
                }


                reader.Dispose();
                if (conn.State == ConnectionState.Open) conn.Close();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private static void OutputHandler(object sender, DataReceivedEventArgs e)
        {
            List<string> list = new List<string>();
            if (!e.Data.Contains("successfully") && !e.Data.Contains("Reading") && !e.Data.Contains("Exit"))
            {
                list.Add(e.Data);
            }
            var distinctList = list.Distinct();
            //GlobalModel.ListRFID.AddRange(distinctList);
            Console.WriteLine(e.Data);

        }
        #region RFID Scan mqtt
        void initMqtt()
        {
            start_mqtt = 1;
            Task.Run(() =>
            {
                try
                {
                    mqttClient = new MqttClient("127.0.0.1");
                    mqttClient.MqttMsgPublishReceived += MqttClient_MqttMsgPublishReceived;
                    mqttClient.Subscribe(new string[] { "RFID" }, new byte[] { MqttMsgBase.QOS_LEVEL_AT_MOST_ONCE });
                    mqttClient.Connect("Application1");
                    btnRFID.BackColor = Color.Green;
                }
                catch (Exception ex)
                {                    
                    btnRFID.BackColor = Color.Red;
                }                
            });            
        }

        void stopMqtt()
        {
            start_mqtt = 0;
            Task.Run(() =>
            {
                try
                {
                    mqttClient = new MqttClient("127.0.0.1");
                    mqttClient.MqttMsgPublishReceived += MqttClient_MqttMsgPublishReceived;
                    mqttClient.Unsubscribe(new string[] { "RFID" });
                    mqttClient.Connect("Application1");
                    btnRFID.BackColor = Color.Red;
                }
                catch (Exception ex)
                {
                    btnRFID.BackColor = Color.Green;
                }
            });
        }

        void usingMqtt()
        {
            if (start_mqtt == 0)
            {
                initMqtt();
            }
            else
            {
                stopMqtt();
            }
        }

        private void MqttClient_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e)
        {
            try
            {
                var message = Encoding.UTF8.GetString(e.Message);
                var result = JsonConvert.DeserializeObject<RFIDMessage>(message);
                if (result != null) validateRFID(result);

                Console.WriteLine(message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        void validateRFID(RFIDMessage rFIDMessage)
        {
            Console.WriteLine($"validateRFID: {lastUnittagid} # {rFIDMessage.unittagid}");
            

            if ((lastUnittagid != rFIDMessage.unittagid || txtUnit.Text.Trim().Equals("")) && checkUnitProcessing == false)
            {
                checkUnitProcessing = true;
                lastUnittagid = rFIDMessage.unittagid;

                var unitId = SelectUnitFromDb(rFIDMessage);
                txtUnit.Invoke((MethodInvoker)(() => txtUnit.Text = unitId));
                input_type = "RFID";
                nrp_fuelman = rFIDMessage.nozzletagid;
            }

            if (rFIDMessage.unittagid != null)
            {
                if (rFIDMessage.unittagid.Trim().Equals("")) txtUnit.Invoke((MethodInvoker)(() => txtUnit.Text = ""));
            }
            else
            {
                txtUnit.Invoke((MethodInvoker)(() => txtUnit.Text = ""));
            }
            
            //btnStart.Invoke((MethodInvoker)(() => btnStart.Enabled = (rFIDMessage.istagvalid == 1 && txtUnit.Text.Trim().Length > 0 ? true : false)));

            if (rFIDMessage.istagvalid == 1 && txtUnit.Text.Trim().Length > 0)
            {
                btnStart.Invoke((MethodInvoker)(() => btnStart.Enabled = true));                
            }
            else
            {
                btnStart.Invoke((MethodInvoker)(() => btnStart.Enabled = false));
            }


        }

        String SelectUnitFromDb(RFIDMessage rFIDMessage)
        {
            SQLiteConnection conn = new SQLiteConnection(myConnectionString);

            try
            {
                if (conn.State != ConnectionState.Open) conn.Open();
                string query = string.Empty;

                query = $"SELECT unitNo,EgiDesc,Egi FROM unit where EgiDesc like '%{rFIDMessage.unittagid}%'";
                SQLiteCommand cmds = new SQLiteCommand(query, conn);
                SQLiteDataReader reader = cmds.ExecuteReader();

                String iStrEgiDesc = string.Empty;
                String iStrUnitNo = string.Empty;
                int maxTank = 0;
                while (reader.Read())
                {
                    iStrEgiDesc = reader["EgiDesc"].ToString();
                    iStrUnitNo = reader["unitNo"].ToString();
                    maxTank = int.Parse(reader["MaxTankCapacity"].ToString());
                }

                GlobalModel.AutoRefueling.preset_gross = maxTank;
                reader.Dispose();
                if (conn.State == ConnectionState.Open) conn.Close();

                return iStrUnitNo;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return "";
            }
            finally
            {
                if (conn.State == ConnectionState.Open) conn.Close();
                checkUnitProcessing = false;
            }
        }
        #endregion
        #region rfid scan langsung db

        void start_read()
        {
            start_rfid_read = 1;
            timer_read_rfid_view.Start();
            btnRFID.BackColor = Color.Green;
            cslRFID.stopCommandConsole();
            startConsoleRFID();
        }
        
        void stop_read()
        {
            start_rfid_read = 0;
            timer_read_rfid_view.Stop();
            btnRFID.BackColor = Color.Red;
            cslRFID.stopCommandConsole();
        }

        void using_vw_rfid_db()
        {
            if (start_rfid_read == 0)
            {
                start_read();
                
                
                
            }
            else
            {
                stop_read();
                
            }
        }

        #endregion
        void clearTextBox()
        {
            txtUnit.Text = "";
            txtHm.Text = "";            
            lblQty.Visible = false;
            lblLiters.Visible = false;
            txtFuelQty.Visible = false;
            GlobalModel.AutoRefueling = new refueling();
        }

        bool IsNumeric(string s)
        {
            int output;
            return int.TryParse(s, out output);
        }

        bool isValid()
        {
            bool iBlStatus = false;
            //string nama = "-";
            //string nrp = "-";
            int hm = 0;

            if (txtUnit.Text.Trim() == "")
            {
                MessageBox.Show("Unit harus di isi..", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            //if (txtHm.Text.Trim() == "" || txtNama.Text.Trim() == "" || txtNrp.Text.Trim() == "")
            if (txtHm.Text.Trim() == "")
            {
                txtHm.Text = hm.ToString();
                //txtNama.Text = nama;
                //txtNrp.Text = nrp;
            }
            else
            {
                iBlStatus = true;
            }

            return iBlStatus;
        }

        public void idleSystem()
        {
            btnNewTrans.Visible = true;
            btnStart.Visible = false;
            lblQty.Visible = true;
            lblLiters.Visible = true;
            txtFuelQty.Visible = true;
            txtHm.Enabled = false; 
        }

        private void btnNewTrans_Click(object sender, EventArgs e)
        {
            clearTextBox();           
            btnNewTrans.Visible = false;
            btnStart.Visible = true;
            txtHm.Enabled = true;
            start_read();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            if (isValid())
            {
                //stopMqtt();
                if(start_search != 1)
                {
                    stop_read();
                }
                else
                {
                    start_search = 0;
                }
                
                loadData();

                var f = new FmManual();
                f.preset = GlobalModel.AutoRefueling.preset_gross * 10;
                f.waitCounterMax = 3 * 10;
                if (f.ShowDialog() == DialogResult.OK)
                {
                    //txtFuelQty.Text = GlobalModel.AutoRefueling.receive_qty1.ToString();
                    txtFuelQty.Text = Convert.ToString(f.quantity);
                }
                else
                {
                    txtFuelQty.Text = Convert.ToString(f.quantity);
                    txtFuelQty.Text = GlobalModel.AutoRefueling.receive_qty1.ToString();
                }
                //var f = new FRunLcr();
                //if (f.ShowDialog() == DialogResult.OK)
                //{
                //    txtFuelQty.Text = Convert.ToString(f.LitersQty);
                //}
                //else
                //{
                //    txtFuelQty.Text = Convert.ToString(f.LitersQty);
                //}

                idleSystem();
            }
        }

        void loadData()
        {
            try
            {
                var _globalData = GlobalModel.GlobalVar;
                tbl_logsheet_detail logsheet = new tbl_logsheet_detail();

                string startHour = "";
                string endHour = "";

                String _shift = String.Empty;
                //String _timezone = "WIB";
                String _timezone = "WITA";

                SQLiteConnection conn;

                conn = new SQLiteConnection(myConnectionString);
                if (conn.State != ConnectionState.Open) conn.Open();
                string query = string.Empty;

                query = "SELECT * FROM tbl_setting";
                SQLiteCommand cmds = new SQLiteCommand(query, conn);
                SQLiteDataReader reader = cmds.ExecuteReader();

                while (reader.Read())
                {
                    pShiftStart1 = reader["StartShift1"].ToString();
                    pShiftStart2 = reader["StartShift2"].ToString();
                }

                startHour = pShiftStart1;
                endHour = pShiftStart2;
                try
                {
                    startHour = startHour.Split(':')[0];
                    endHour = endHour.Split(':')[0];
                }
                catch (Exception)
                {
                }

                reader.Dispose();
                if (conn.State == ConnectionState.Open) conn.Close();

                pShiftStart1 = startHour;
                pShiftStart2 = endHour;

                if (DateTime.Now.Hour >= int.Parse(pShiftStart1) && DateTime.Now.Hour < int.Parse(pShiftStart2))
                    _shift = "1";
                else
                    _shift = "2";

                var _date = DateTime.Now;

                if (DateTime.Now.Hour <= int.Parse(pShiftStart1))
                    _date = _date.AddDays(-1);

                logsheet.job_row_id = System.Guid.NewGuid().ToString();
                logsheet.unit_no = txtUnit.Text;
                logsheet.dstrct_code = _globalData.DataEmp.DstrctCode;
                logsheet.shift = _shift;
                logsheet.log_sheet_code = $"{_globalData.DataEmp.DstrctCode}{_date.ToString("yyyyMMdd")}{_shift}{_globalData.FlowMeter[0].SnCode}LCR";
                logsheet.issued_date = _date;
                logsheet.whouse_id = txtWh.Text;
                logsheet.hm_before = 0;
                logsheet.hm = int.Parse(txtHm.Text);
                logsheet.stat_type = "HR";
                logsheet.ref_condition = "NORMAL";
                logsheet.flw_meter = _globalData.FlowMeter[0].FlowMeterNo;
                logsheet.meter_faktor = _globalData.FlowMeter[0].MeterFakor ?? 0;
                logsheet.location = _globalData.FlowMeter[0].WhouseLocation;
                logsheet.flw_meter = _globalData.FlowMeter[0].SnCode;
                logsheet.ref_hour_start = ":";
                logsheet.ref_hour_stop = ":";
                logsheet.flow_meter_start = 0;
                logsheet.flow_meter_end = 0;
                logsheet.qty_loqsheet = 0;
                logsheet.qty = 0;
                logsheet.shift = _shift;
                //logsheet.nrp_operator = txtNrp.Text;
                //logsheet.nama_operator = txtNama.Text;
                logsheet.nrp_operator = "";
                logsheet.nama_operator = "";
                logsheet.resp_code = _globalData.Gl[0].Nrp;
                logsheet.resp_name = _globalData.Gl[0].Name;
                logsheet.input_type = input_type == string.Empty ? "MANUAL" : input_type;
                logsheet.fuel_oil_type = "DF";
                logsheet.mod_date = DateTime.Now;
                //nrp login ganti jadi nrp rfid
                logsheet.mod_by = nrp_fuelman == string.Empty ? _globalData.DataEmp.Nrp : nrp_fuelman;
                logsheet.timezone = _timezone;
                logsheet.text_header = "";
                logsheet.text_sub_header = "";
                logsheet.text_body = "";
                logsheet.syncs = 2;
                logsheet.driver_nrp = driver_nrp;
                logsheet.driver_name = driver_name;
                logsheet.fuelman_name = fuelman_name;
                logsheet.fuelman_nrp = nrp_fuelman;
                GlobalModel.logsheet_detail = logsheet;
            }
            catch (Exception ex)
            {
                
                save_error_log(ex.ToString(), "loadData()");
            }
            
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            var f = new FSeacrhUnit();
            start_search = 1;            
            //using_vw_rfid_db();
            stop_read();
            btnStart.Invoke((MethodInvoker)(() => btnStart.Enabled = true));
            if (f.ShowDialog() == DialogResult.OK)
            {
                txtUnit.Text = f.no_unit;
                input_type = "MANUAL";
            }
        }

        private void FRefuellingSerial_Activated(object sender, EventArgs e)
        {
            this.WindowState = System.Windows.Forms.FormWindowState.Normal;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
        }

       

        void initData()
        {
            try
            {
                SQLiteConnection conn;

                conn = new SQLiteConnection(myConnectionString);
                if (conn.State != ConnectionState.Open) conn.Open();
                string query = string.Empty;

                query = "SELECT * FROM tbl_setting";
                SQLiteCommand cmds = new SQLiteCommand(query, conn);
                SQLiteDataReader reader = cmds.ExecuteReader();

                while (reader.Read())
                {
                    txtWh.Text = reader["Whouseid"].ToString();
                    txtSn.Text = reader["SnCode"].ToString();
                    pWHouse = reader["Whouseid"].ToString();
                    pSnCOde = reader["SnCode"].ToString();
                }
                reader.Dispose();
                if (conn.State == ConnectionState.Open) conn.Close();

                initWhSetting(conn);
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
                MessageBox.Show("Exception: Terjadi kesalahan saat innit data, mohon cek log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(ex.Message, "initData()");
            }
        }

        void initWhSetting(SQLiteConnection conn)
        {
            try
            {
                var _globalData = GlobalModel.GlobalVar;

                if (conn.State != ConnectionState.Open) conn.Open();
                string query = string.Empty;
                query = $"SELECT * FROM flowmeter where Warehouse='{pWHouse}' and  SnCode ='{pSnCOde}'";
                SQLiteCommand cmds = new SQLiteCommand(query, conn);
                SQLiteDataReader reader = cmds.ExecuteReader();

                while (reader.Read())
                {
                    _globalData.FlowMeter[0].FlowMeterNo = reader["FlowMeterNo"].ToString();
                    _globalData.FlowMeter[0].MeterFakor = int.Parse(reader["MeterFakor"].ToString());
                    _globalData.FlowMeter[0].SnCode = reader["SnCode"].ToString();
                    _globalData.FlowMeter[0].WhouseLocation = reader["WhouseLocation"].ToString();
                    _globalData.FlowMeter[0].Warehouse = reader["Warehouse"].ToString();
                }


                reader.Dispose();
                if (conn.State == ConnectionState.Open) conn.Close();
            }
            catch (Exception ex)
            {                
                //MessageBox.Show(ex.ToString());
                MessageBox.Show("Exception: Terjadi kesalahan saat innit warehouse setting, mohon cek log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(ex.Message, "initWhSetting()");
            }

        }

        void save_error_log(string data, string actions)
        {
            try
            {
                DateTime localDate = DateTime.Now;
                var _item = new tbl_log();
                var initData = GlobalModel.GlobalVar;

                _item.pid = System.Guid.NewGuid().ToString();
                _item.actions = $"District {initData.DataEmp.DstrctCode}/FRefuellingSerial/{actions}";
                _item.message = $"{data}";
                _item.mod_date = localDate;
                _item.mod_by = initData.DataEmp.Nrp;

                SQLiteConnection conn;

                conn = new SQLiteConnection(myConnectionString);
                if (conn.State != ConnectionState.Open) conn.Open();
                string query = string.Empty;
                query = "INSERT INTO tbl_log(pid,actions,message,mod_date,mod_by)"
                      + " VALUES(@pid,@actions,@message,@mod_date,@mod_by)";

                SQLiteCommand cmd = new SQLiteCommand(query, conn);

                cmd.Parameters.Add(new SQLiteParameter("@pid", _item.pid));
                cmd.Parameters.Add(new SQLiteParameter("@actions", _item.actions));
                cmd.Parameters.Add(new SQLiteParameter("@message", _item.message));
                cmd.Parameters.Add(new SQLiteParameter("@mod_date", _item.mod_date));
                cmd.Parameters.Add(new SQLiteParameter("@mod_by", _item.mod_by));
                cmd.ExecuteNonQuery();
                if (conn.State == ConnectionState.Open) conn.Close();
            }
            catch (Exception)
            {
            }
        }

        private void btnManual_Click(object sender, EventArgs e)
        {
            var f = new FManual();
            //f.ShowDialog();
            //using_vw_rfid_db();
            stop_read();
            if (f.ShowDialog() == DialogResult.OK)
            {
                txtUnit.Text = f.no_unit;
            }
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            txtHm.Text = txtHm.Text + "1";
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            txtHm.Text = txtHm.Text + "2";
        }

        private void btn3_Click(object sender, EventArgs e)
        {
            txtHm.Text = txtHm.Text + "3";
        }

        private void btn4_Click(object sender, EventArgs e)
        {
            txtHm.Text = txtHm.Text + "4";
        }

        private void btn5_Click(object sender, EventArgs e)
        {
            txtHm.Text = txtHm.Text + "5";
        }

        private void btn6_Click(object sender, EventArgs e)
        {
            txtHm.Text = txtHm.Text + "6";
        }

        private void btn7_Click(object sender, EventArgs e)
        {
            txtHm.Text = txtHm.Text + "7";
        }

        private void btn8_Click(object sender, EventArgs e)
        {
            txtHm.Text = txtHm.Text + "8";
        }

        private void btn9_Click(object sender, EventArgs e)
        {
            txtHm.Text = txtHm.Text + "9";
        }

        private void btn0_Click(object sender, EventArgs e)
        {
            txtHm.Text = txtHm.Text + "0";
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtHm.Text = "";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //using_vw_rfid_db();
            this.Close();
        }

        private void btnRFID_Click(object sender, EventArgs e)
        {
            //usingMqtt();
            using_vw_rfid_db();
            //if(mqttClient!= null)
            //{
            //    if (mqttClient.IsConnected)
            //    {
            //        stopMqtt();                    
            //    }
            //    else
            //    {
            //        initMqtt();                    
            //    }
            //}
            //else
            //{
            //    initMqtt();                
            //}

        }

        public async Task<string> getTagRFIDUrl()
        {
            try
            {
                WebClient requestCek = new WebClient();
                var str = requestCek.DownloadString(pUrl_rfid_tag);
                return str;
            }
            catch (WebException ex)
            {
                return "";
            }
        }

        private async void timer_read_rfid_view_Tick(object sender, EventArgs e)
        {
            try
            {
                string rfid_tag = await getTagRFIDUrl();
                var result = JsonConvert.DeserializeObject<url_rfid>(rfid_tag);
                if (!string.IsNullOrEmpty(result.rfid_tag))
                {
                    createLogRFID(result.rfid_tag);
                    getDataRFID();
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
                MessageBox.Show("Exception: Terjadi kesalahan saat pembacaan data RFID, mohon cek log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(ex.Message, "timer_read_rfid_view_Tick()");
            }

        }

        public class url_rfid
        {
            public string rfid_tag { get; set; }
        }
        private void createLogRFID(string rfid_tag)
        {
            using (SQLiteConnection conn = new SQLiteConnection(myConnectionString))
            {
                conn.Open();
                string query = string.Empty;
                query = "INSERT INTO taglogs(pid,tagid,district,timestamps)"
                     + " VALUES(@pid,@tagid,@district,@timestamps)";
                using (SQLiteCommand cmd = new SQLiteCommand(query, conn))
                {
                    cmd.Parameters.Add(new SQLiteParameter("@pid", Guid.NewGuid().ToString()));
                    cmd.Parameters.Add(new SQLiteParameter("@tagid", rfid_tag));
                    cmd.Parameters.Add(new SQLiteParameter("@district", GlobalModel.GlobalVar.DataEmp.DstrctCode));
                    cmd.Parameters.Add(new SQLiteParameter("@timestamps", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                    cmd.ExecuteNonQuery();
                }

            }
        }

        private void getDataRFID()
        {
            SQLiteConnection conn = new SQLiteConnection(myConnectionString);
            if (conn.State != ConnectionState.Open) conn.Open();
            var query = " SELECT * ";
            query += " FROM vw_current_tagunit ";
            SQLiteCommand cmd = new SQLiteCommand(query, conn);
            SQLiteDataReader reader = cmd.ExecuteReader();
            RFIDMessage result = new RFIDMessage();
            while (reader.Read())
            {
                result.unitno = reader["unitno"].ToString();
                result.unittagid = reader["tagid"].ToString();
                //result.nozzletagid = reader["nozzleno"].ToString();
                result.MaxTank = reader["MaxTank"].ToString();
                //result.istagvalid = int.Parse(reader["istagvalid"].ToString());
            }

            if (!string.IsNullOrEmpty(result.unitno))
            {
                GlobalModel.AutoRefueling.preset_gross = int.Parse(result.MaxTank);
                txtUnit.Invoke((MethodInvoker)(() => txtUnit.Text = result.unitno));
                input_type = "RFID";
            }
            else
            {
                txtUnit.Invoke((MethodInvoker)(() => txtUnit.Text = ""));
            }

            reader.Dispose();
            if (conn.State == ConnectionState.Open) conn.Close();
        }
        int execCmd(String sQuery, SQLiteConnection sConn)
        {
            int iReturn = 0;

            var cmd = new SQLiteCommand(sQuery, sConn);
            iReturn = cmd.ExecuteNonQuery();

            return iReturn;
        }

        private void FRefuellingSerial_FormClosing(object sender, FormClosingEventArgs e)
        {
            stop_read();
        }
    }
}
