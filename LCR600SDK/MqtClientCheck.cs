﻿using LCR600SDK.DataClass;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;

namespace LCR600SDK
{
    public partial class MqtClientCheck : Form
    {
        MqttClient mqttClient;
        int iCount = 0;
        public MqtClientCheck()
        {
            InitializeComponent();
        } 

        private void MqttClient_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e)
        {
            try
            { 
                var message = Encoding.UTF8.GetString(e.Message);
                var result = JsonConvert.DeserializeObject<RFIDMessage>(message);
                listBox1.Invoke((MethodInvoker)(() => listBox1.Items.Insert(0, $"{iCount++}. unittagid:{result.unittagid}, nozzletagid:{result.nozzletagid}, istagvalid:{result.istagvalid}"))); 
                Console.WriteLine(message);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.ToString());
            } 
        }

        private void MqtClientCheck_Load(object sender, EventArgs e)
        {
            iCount = 0;
            Task.Run(() =>
            {
                mqttClient = new MqttClient("127.0.0.1");
                mqttClient.MqttMsgPublishReceived += MqttClient_MqttMsgPublishReceived; 
                mqttClient.Subscribe(new string[] { "RFID" }, new byte[] { MqttMsgBase.QOS_LEVEL_AT_MOST_ONCE });
                mqttClient.Connect("Application1");
            });
        }

        private void MqtClientCheck_FormClosing(object sender, FormClosingEventArgs e)
        {
            closedMqttAsync();
        }

        async Task closedMqttAsync()
        {

            await Task.Run(() =>
            {
                if (mqttClient != null && mqttClient.IsConnected) mqttClient.Disconnect();
            });
        }
    }
}
