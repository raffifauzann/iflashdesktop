﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace LCR600SDK
{
    class GetConsoleRFID
    {
        Process runCMD = new Process();
        public void startCommandConsole()
        {
            try
            {
                var startInfo = new ProcessStartInfo
                {
                    WindowStyle = ProcessWindowStyle.Hidden,
                    //WindowStyle = ProcessWindowStyle.Normal,
                    FileName = "CMD.exe",
                    //RedirectStandardInput = true,
                    //RedirectStandardOutput = true,
                    //UseShellExecute = false,
                    WorkingDirectory = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "SQLite"),
                    Arguments = "/K python  rfid_serial_v2_sqlite.py"
                };

                runCMD.StartInfo = startInfo;
                runCMD.Start();                                                
            }
            catch(Exception ex)
            {

            }
            
        }        

        public void stopCommandConsole()
        {
            Array.ForEach(Process.GetProcessesByName("Python"), f=>f.Kill());
            Array.ForEach(Process.GetProcessesByName("cmd"), f => f.Kill());
        }
    }  
}
