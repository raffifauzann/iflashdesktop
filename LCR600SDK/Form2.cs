﻿using Flurl.Http;
using LCR600SDK.DataClass;
using MySql.Data.MySqlClient;
using System.Data.SQLite;
using System.IO;
using System.Reflection;
using SDK;
using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data;

namespace LCR600SDK
{
    public partial class Form2 : Form
    {
        public double LitersQty = 0.0;
        public double LitersTotalizerStart = 0.0;
        public double LitersTotalizerEnd = 0.0;
        public float qty = -1; //check
        public enum TRANS_TYPE { PORT_RECEIVED, DARAT_RECEIVED, ISSUED_MANDIRI, PORT_SEND };
        public string TRANS_TYPE_SELECT;
        private string myConnectionString = string.Empty;
        private byte device = 0;
        private int stepTimerOpen = 0;
        private int stepTimerClose = 0;
        private string pUrl_service = string.Empty;
        public ListDatumRitasiMaintank ListDatumRitasiMaintank;
        public int intMaxTick = 0;
        public int intCurrentTick = 0;
        private bool isProccessGet = false;
        PasswordEncrypDecrypt decryptor = new PasswordEncrypDecrypt();
        public Form2()
        {
            InitializeComponent();
        }

        private const int CP_DISABLE_CLOSE_BUTTON = 0x200;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ClassStyle = cp.ClassStyle | CP_DISABLE_CLOSE_BUTTON;
                return cp;
            }
        }

        private bool open()
        {
            return true;
        }

        private bool start()
        {
            return true;
        }

        private bool print()
        {
            return true;
        }

        private bool closed()
        {
            return true;
        }

        private byte doConnectLCR()
        {
            byte device = 250;
            byte numDevices;
            byte[] deviceList = new byte[device];
            IntPtr deviceListPtr = Marshal.AllocHGlobal(250);
            byte rc = LCP02API.LCP02Open(device, device, out numDevices, deviceListPtr);
            if (rc != (byte)LCPAPI.LCPR.LCPR_OK && rc != (byte)LCP02API.LCR_RETURN_CODES.LCP02Ra_ALREADYOPENED)
            {
                device = 0;
            }
            return device;
        }

        private void doOpenLCR()
        {
            try
            {
                device = doConnectLCR();
                if (device > 0)
                {
                    txtGrossQty.Text = getGrossQty(qty).Qty.ToString();
                    txtGrossTotalizer.Text = getGrossTotalizer(qty).Qty.ToString();
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        private string doStartLCR()
        {
            string strStatus = "PLEASE WAIT..";

            if (device == 0)
                return strStatus;

            byte rc = LCP02API.LCP02IssueCommand(device, (byte)LCP02API.LCR_COMMANDS.LCRC_RUN,
                                                         (byte)LCP02API.LCR_RETURN_CODES.LCRM_NO_WAIT);

            if (rc == (byte)LCPAPI.LCPR.LCPR_OK)
                strStatus = "RUN";

            //add by sup for hard reset 
            txtGrossQty.Text = "0";

            return strStatus;
        }

        private string doPrintLCR()
        {
            string strStatus = "PLEASE WAIT..";

            if (device == 0)
                return strStatus;


            byte rc = LCP02API.LCP02IssueCommand(device, (byte)LCP02API.LCR_COMMANDS.LCRC_PRINT,
                                                         (byte)LCP02API.LCR_RETURN_CODES.LCRM_NO_WAIT);

            if (rc == (byte)LCPAPI.LCPR.LCPR_OK)
                strStatus = "STOP";

            return strStatus;
        }

        private bool doCloseLCR()
        {
            byte rc = LCP02API.LCP02Close();
            if (rc != (byte)LCPAPI.LCPR.LCPR_OK)
            {
                //MessageBox.Show("LCP02Close error, rc =" + rc)
                Console.WriteLine("LCP02Close error, rc =" + rc);
                return false;
            }
            return true;
        }

        private ClsLcrRequest getGrossQty(float lastQty)
        {
            string grossQtyStr = lastQty.ToString();
            double grossQty = 0; //check
            try
            {

                byte rc;

                if (device == 0)
                {
                    return new ClsLcrRequest(lastQty, false);
                }

                byte fieldNum = (byte)LCP02API.LCR_FIELD_NUMBERS.LCRF_GrossQty_NE;
                int size = LCP02API.LCP02FieldSize(fieldNum);
                IntPtr grossQtyPtr = Marshal.AllocHGlobal((int)size);

                byte devStatus;

                rc = LCP02API.LCP02GetField(device, fieldNum, grossQtyPtr, out devStatus,
                                            (byte)LCP02API.LCR_RETURN_CODES.LCRM_NO_WAIT);

                if (rc != (byte)LCPAPI.LCPR.LCPR_OK)
                {
                    return new ClsLcrRequest(lastQty, false);
                }

                string strGrossQty = Marshal.PtrToStringAnsi(grossQtyPtr, (int)size);
                LCP02API.FloatByteArray fbaGross;
                byte[] strGrossArr = null;

                fbaGross = new LCP02API.FloatByteArray();
                strGrossArr = Encoding.GetEncoding(1252).GetBytes(strGrossQty);
                fbaGross.Byte1 = strGrossArr[0];
                fbaGross.Byte2 = strGrossArr[1];
                fbaGross.Byte3 = strGrossArr[2];
                fbaGross.Byte4 = strGrossArr[3];

                if (lastQty > 0)
                {
                    if (fbaGross.int32Value <= 0)
                    {
                        grossQty = Convert.ToDouble(lastQty.ToString());
                    }
                    else
                    {
                        strGrossQty = fbaGross.int32Value.ToString();
                        if (strGrossQty != "")
                        {
                            grossQty = Convert.ToDouble(strGrossQty);
                            grossQty = grossQty / 10;
                        }
                        else
                        {
                            grossQty = Convert.ToDouble(strGrossQty);
                        }
                    }
                }
                else
                {
                    strGrossQty = fbaGross.int32Value.ToString();
                    if (strGrossQty != "")
                    {
                        grossQty = Convert.ToDouble(strGrossQty);
                        grossQty = grossQty / 10;
                    }
                    else
                    {
                        grossQty = 0; //check
                    }
                }

                grossQtyStr = grossQty.ToString();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return new ClsLcrRequest(grossQty, true);
        }

        private ClsLcrRequest getGrossTotalizer(float lastQty)
        {
            byte rc;
            string grossTotalizerStr = lastQty.ToString();
            double grossQty = 0; //check

            if (device == 0)
            {
                return new ClsLcrRequest(lastQty, false);
            }

            byte fieldNum = (byte)LCP02API.LCR_FIELD_NUMBERS.LCRF_GrossTotal_WM;
            int size = LCP02API.LCP02FieldSize(fieldNum);
            IntPtr grossTotalizerPtr = Marshal.AllocHGlobal((int)size);

            byte devStatus;

            rc = LCP02API.LCP02GetField(device, fieldNum, grossTotalizerPtr, out devStatus,
                                        (byte)LCP02API.LCR_RETURN_CODES.LCRM_NO_WAIT);

            if (rc != (byte)LCPAPI.LCPR.LCPR_OK)
            {
                return new ClsLcrRequest(lastQty, false);
            }

            string strGrossTotalizer = Marshal.PtrToStringAnsi(grossTotalizerPtr, (int)size);
            LCP02API.FloatByteArray fbaGross;
            byte[] strGrossArr = null;

            fbaGross = new LCP02API.FloatByteArray();
            strGrossArr = Encoding.GetEncoding(1252).GetBytes(strGrossTotalizer);
            fbaGross.Byte1 = strGrossArr[0];
            fbaGross.Byte2 = strGrossArr[1];
            fbaGross.Byte3 = strGrossArr[2];
            fbaGross.Byte4 = strGrossArr[3];

            strGrossTotalizer = fbaGross.int32Value.ToString();
            if (strGrossTotalizer != "")
            {
                grossQty = Convert.ToDouble(strGrossTotalizer);
                grossQty = grossQty / 10;
            }

            grossTotalizerStr = grossQty.ToString();

            return new ClsLcrRequest(grossQty, true);
        }

        private void saveLog()
        { 
            switch (TRANS_TYPE_SELECT)
            {
                case "PORT_RECEIVED":
                    //saveLogReveived();
                    break;
                case "ISSUED_MANDIRI":
                    saveLogIssued();
                    break;
                case "PORT_SEND":
                    //saveLogTransfer();
                    break; 
            } 
        }

        void saveLogIssued()
        {
            string datetimeNow = DateTime.Now.ToString("yyyyMMdd_HHmmss");
            string fileName = @"C:\Lab\log_Qty_RefuellingMandiri_" + datetimeNow + ".txt";

            try
            {
                var _item = GlobalModel.logsheet_detail;
                var initData = GlobalModel.GlobalVar;
                DateTime dt = DateTime.Parse(DateTime.Now.ToString());

                // Check if file already exists. If yes, delete it.     
                if (File.Exists(fileName))
                {
                    File.Delete(fileName);
                }

                // Create a new file     
                using (StreamWriter sw = File.CreateText(fileName))
                {
                    sw.WriteLine("HM " + _item.hm.ToString());
                    sw.WriteLine("UnitNo " + _item.unit_no.ToString());
                    sw.WriteLine("NamaOpr " + _item.nama_operator.ToString());
                    sw.WriteLine("grossQty " + txtGrossQty.Text.ToString());
                    sw.WriteLine("grossTotalizerStart " + txtTotalizerStart.Text.ToString());
                    sw.WriteLine("grossTotalizerEnd " + txtGrossTotalizer.Text.ToString());
                    sw.WriteLine("RefStart " + txtRefSTart.Text.ToString());
                    sw.WriteLine("RefStop " + dt.ToString("HH:mm:ss"));
                    sw.WriteLine("ModDate " + DateTime.Now);
                    sw.WriteLine("ModBy " + initData.DataEmp.Nrp);
                }

                // Write file contents on console.     
                using (StreamReader sr = File.OpenText(fileName))
                {
                    string s = "";
                    while ((s = sr.ReadLine()) != null)
                    {
                        Console.WriteLine(s);
                    }
                }
            }
            catch (Exception Ex)
            {
                Console.WriteLine(Ex.ToString());
            }
        }

        void saveLogTransfer()
        {
            string datetimeNow = DateTime.Now.ToString("yyyyMMdd_HHmmss");
            string fileName = @"C:\Lab\log_Qty_RefuellingMandiri_" + datetimeNow + ".txt";

            try
            {
                var _item = GlobalModel.logsheet_detail;
                var initData = GlobalModel.GlobalVar;
                DateTime dt = DateTime.Parse(DateTime.Now.ToString());

                // Check if file already exists. If yes, delete it.     
                if (File.Exists(fileName))
                {
                    File.Delete(fileName);
                }

                // Create a new file     
                using (StreamWriter sw = File.CreateText(fileName))
                {
                    sw.WriteLine("HM " + _item.hm.ToString());
                    sw.WriteLine("UnitNo " + _item.unit_no.ToString());
                    sw.WriteLine("NamaOpr " + _item.nama_operator.ToString());
                    sw.WriteLine("grossQty " + txtGrossQty.Text.ToString());
                    sw.WriteLine("grossTotalizerStart " + txtTotalizerStart.Text.ToString());
                    sw.WriteLine("grossTotalizerEnd " + txtGrossTotalizer.Text.ToString());
                    sw.WriteLine("RefStart " + txtRefSTart.Text.ToString());
                    sw.WriteLine("RefStop " + dt.ToString("HH:mm:ss"));
                    sw.WriteLine("ModDate " + DateTime.Now);
                    sw.WriteLine("ModBy " + initData.DataEmp.Nrp);
                }

                // Write file contents on console.     
                using (StreamReader sr = File.OpenText(fileName))
                {
                    string s = "";
                    while ((s = sr.ReadLine()) != null)
                    {
                        Console.WriteLine(s);
                    }
                }
            }
            catch (Exception Ex)
            {
                Console.WriteLine(Ex.ToString());
            }
        }

        void saveLogReveived()
        {
            string datetimeNow = DateTime.Now.ToString("yyyyMMdd_HHmmss");
            string fileName = @"C:\Lab\log_Qty_RefuellingMandiri_" + datetimeNow + ".txt";

            try
            {
                var _item = GlobalModel.logsheet_detail;
                var initData = GlobalModel.GlobalVar;
                DateTime dt = DateTime.Parse(DateTime.Now.ToString());

                // Check if file already exists. If yes, delete it.     
                if (File.Exists(fileName))
                {
                    File.Delete(fileName);
                }

                // Create a new file     
                using (StreamWriter sw = File.CreateText(fileName))
                {
                    sw.WriteLine("HM " + _item.hm.ToString());
                    sw.WriteLine("UnitNo " + _item.unit_no.ToString());
                    sw.WriteLine("NamaOpr " + _item.nama_operator.ToString());
                    sw.WriteLine("grossQty " + txtGrossQty.Text.ToString());
                    sw.WriteLine("grossTotalizerStart " + txtTotalizerStart.Text.ToString());
                    sw.WriteLine("grossTotalizerEnd " + txtGrossTotalizer.Text.ToString());
                    sw.WriteLine("RefStart " + txtRefSTart.Text.ToString());
                    sw.WriteLine("RefStop " + dt.ToString("HH:mm:ss"));
                    sw.WriteLine("ModDate " + DateTime.Now);
                    sw.WriteLine("ModBy " + initData.DataEmp.Nrp);
                }

                // Write file contents on console.     
                using (StreamReader sr = File.OpenText(fileName))
                {
                    string s = "";
                    while ((s = sr.ReadLine()) != null)
                    {
                        Console.WriteLine(s);
                    }
                }
            }
            catch (Exception Ex)
            {
                Console.WriteLine(Ex.ToString());
            }
        }

        private bool getData()
        {
            try
            {
                isProccessGet = true;
                ClsLcrRequest clsLcrRequest = new ClsLcrRequest(0, false);

                clsLcrRequest = getGrossQty(float.Parse(txtGrossQty.Text));
                txtGrossQty.Text = clsLcrRequest.Qty.ToString();

                clsLcrRequest = getGrossTotalizer(float.Parse(txtGrossTotalizer.Text));
                txtGrossTotalizer.Text = clsLcrRequest.Qty.ToString();

                if (txtGrossTotalizer.Text.ToUpper() != "0" && txtTotalizerStart.Text.Trim() == "")
                {
                    txtTotalizerStart.Text = txtGrossTotalizer.Text;
                }

                if (clsLcrRequest.Status == false)
                {
                    doCloseLCR();
                    Thread.Sleep(500);
                    doOpenLCR();
                    return false;
                }

                saveLog();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                isProccessGet = false;
            }

        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            try
            {
                btnStop.Enabled = false;
                lblAlertClosed.Visible = true;
                doPrintLCR();
                doCloseLCR();
                stepTimerClose = 0;
                txtRefStop.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                timerClose.Enabled = true;
                timerOpen.Enabled = false;
                timerQty.Enabled = false;
            }
            catch (Exception)
            {

            }
        }

        void tryDataOk()
        {
            try
            {
                bool isFailed = true;
                while (isFailed)
                {
                    if (txtGrossQty.Text.Trim().ToUpper() == "WAIT..")
                    {
                        getData();
                        System.Threading.Thread.Sleep(100);
                    }
                    else
                        isFailed = false;
                }
                saveDataAsync();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

        }

        async void saveDataAsync()
        {
            if ((int)Math.Round(double.Parse(txtGrossQty.Text.Trim(), new System.Globalization.CultureInfo("id-ID")))==0){
                return;
            }

            switch (TRANS_TYPE_SELECT)
            {
                case "PORT_RECEIVED":
                    await savePORT_RECEIVEDAsync();
                    break;
                case "ISSUED_MANDIRI":
                    saveISSUED_MANDIRI();
                    break;
                case "PORT_SEND":
                    submitDataPortSend();
                    break;
            }
        }

        async void submitDataPortSend()
        {
            var _auth = GlobalModel.loginModel;
            String _url = $"{pUrl_service}api/FuelService/sendRitasiToMaintank";

            try
            {
                ListDatumRitasiMaintank.SendQty = (int)Math.Round(double.Parse(txtGrossQty.Text.Trim(), new System.Globalization.CultureInfo("id-ID")));
                ListDatumRitasiMaintank.SendQtyXMf = (int)Math.Round(double.Parse(txtGrossQty.Text.Trim(), new System.Globalization.CultureInfo("id-ID")));
                ListDatumRitasiMaintank.SendFlowMeterAwal = (int)Math.Round(double.Parse(txtTotalizerStart.Text.Trim(), new System.Globalization.CultureInfo("id-ID")));
                ListDatumRitasiMaintank.SendFlowMeterAkhir = (int)Math.Round(double.Parse(txtGrossTotalizer.Text.Trim(), new System.Globalization.CultureInfo("id-ID")));

                returnValue data = await _url.WithHeaders(new { Authorization = _auth.token })
                       .PostJsonAsync(ListDatumRitasiMaintank).ReceiveJson<returnValue>();

                if (data.status)
                {
                    if (Application.OpenForms["FSendFTPortForm"] != null)
                        (Application.OpenForms["FSendFTPortForm"] as FSendFTPortForm)._backToCallForm();
                }
                else
                {
                    MessageBox.Show(data.remarks, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        async Task savePORT_RECEIVEDAsync()
        {
            var _auth = GlobalModel.loginModel;
            var pData = ListDatumRitasiMaintank;

            String _url = $"{pUrl_service}api/FuelService/receiveRitasiToMaintank";
            try
            {
                pData.ReceiveQty1 = (int)Math.Round(double.Parse(txtGrossQty.Text.Trim(), new System.Globalization.CultureInfo("id-ID")));
                pData.receiveQtyTotal = (int)Math.Round(double.Parse(txtGrossQty.Text.Trim(), new System.Globalization.CultureInfo("id-ID")));
                pData.SendFlowMeterAwal = (int)Math.Round(double.Parse(txtTotalizerStart.Text.Trim(), new System.Globalization.CultureInfo("id-ID")));
                pData.SendFlowMeterAkhir = (int)Math.Round(double.Parse(txtGrossTotalizer.Text.Trim(), new System.Globalization.CultureInfo("id-ID")));
                pData.TimeStart = DateTime.Parse(txtRefSTart.Text);
                pData.TimeEnd = DateTime.Parse(txtRefStop.Text);
                pData.SendQty = (int)Math.Round(double.Parse(txtGrossQty.Text.Trim(), new System.Globalization.CultureInfo("id-ID")));
                pData.SendQtyXMf = (int)Math.Round(double.Parse(txtGrossQty.Text.Trim(), new System.Globalization.CultureInfo("id-ID")));

                //GlobalModel.ListDatumRitasiMaintank = pData;
                //if (Application.OpenForms["FReceivedFTPortForm"] != null)
                //    (Application.OpenForms["FReceivedFTPortForm"] as FReceivedFTPortForm).submitData(pData);


                returnValue data = await _url.WithHeaders(new { Authorization = _auth.token })
                       .PostJsonAsync(pData).ReceiveJson<returnValue>();

                if (!data.status)
                {
                    MessageBox.Show(data.remarks, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                save_error_log(ex.ToString(), "savePORT_RECEIVEDAsync()");
                MessageBox.Show(ex.ToString());
            }
        }

        void saveISSUED_MANDIRI()
        {
            try
            {
                var _item = GlobalModel.logsheet_detail;
                var _lcrData = GlobalModel.lcrData;

                _item.qty = Convert.ToDouble(txtGrossQty.Text.Trim());// isNumeric(txtGrossQTY.Text.Replace(".",",")) ? Convert.ToDouble(txtGrossQTY.Text) : 0;
                _item.flow_meter_end = Convert.ToDouble(txtGrossTotalizer.Text.Trim()); //isNumeric(txtGrossTotalizer.Text.Replace(".", ",")) ? Convert.ToDouble(txtGrossTotalizer.Text) : 0;
                _item.flow_meter_start = Convert.ToDouble(txtTotalizerStart.Text.Trim()); // isNumeric(txtTotalizerStart.Text.Replace(".", ",")) ? Convert.ToDouble(txtTotalizerStart.Text) : 0;
                _item.qty_loqsheet = Convert.ToDouble(txtGrossQty.Text.Trim());

                _item.ref_hour_start = txtRefSTart.Text;
                _item.ref_hour_stop = txtRefStop.Text;

                //_item.is_load_to_fosto = 99;

                SQLiteConnection conn;

                conn = new SQLiteConnection(myConnectionString);
                if (conn.State != ConnectionState.Open) conn.Open();
                string query = string.Empty;
                int recs = 0;
                query = "INSERT INTO tbl_t_log_sheet_detail(job_row_id,dstrct_code,log_sheet_code,input_type,issued_date,whouse_id,unit_no,max_tank_capacity,hm_before,hm,flw_meter,meter_faktor,qty_loqsheet,qty,shift,fuel_oil_type,stat_type,nrp_operator,nama_operator,work_area,location,ref_condition,ref_hour_start,ref_hour_stop,note,timezone,flag_loading,loadingerror,loadingdatetime,mod_by,mod_date,is_load_to_fosto,foto,flow_meter_start,flow_meter_end,resp_code,resp_name,text_header,text_sub_header,text_body,syncs)"
                      + " VALUES(@job_row_id,@dstrct_code,@log_sheet_code,@input_type,@issued_date,@whouse_id,@unit_no,@max_tank_capacity,@hm_before,@hm,@flw_meter,@meter_faktor,@qty_loqsheet,@qty,@shift,@fuel_oil_type,@stat_type,@nrp_operator,@nama_operator,@work_area,@location,@ref_condition,@ref_hour_start,@ref_hour_stop,@note,@timezone,@flag_loading,@loadingerror,@loadingdatetime,@mod_by,@mod_date,@is_load_to_fosto,@foto,@flow_meter_start,@flow_meter_end,@resp_code,@resp_name,@text_header,@text_sub_header,@text_body,@syncs)";

                SQLiteCommand cmd = new SQLiteCommand(query, conn);

                cmd.Parameters.Add(new SQLiteParameter("@job_row_id", _item.job_row_id));
                cmd.Parameters.Add(new SQLiteParameter("@dstrct_code", _item.dstrct_code));
                cmd.Parameters.Add(new SQLiteParameter("@log_sheet_code", _item.log_sheet_code));
                cmd.Parameters.Add(new SQLiteParameter("@input_type", _item.input_type));
                cmd.Parameters.Add(new SQLiteParameter("@issued_date", _item.issued_date));
                cmd.Parameters.Add(new SQLiteParameter("@whouse_id", _item.whouse_id));
                cmd.Parameters.Add(new SQLiteParameter("@unit_no", _item.unit_no));
                cmd.Parameters.Add(new SQLiteParameter("@max_tank_capacity", _item.max_tank_capacity));
                cmd.Parameters.Add(new SQLiteParameter("@hm_before", _item.hm_before));
                cmd.Parameters.Add(new SQLiteParameter("@hm", _item.hm));
                cmd.Parameters.Add(new SQLiteParameter("@flw_meter", _item.flw_meter));
                cmd.Parameters.Add(new SQLiteParameter("@meter_faktor", _item.meter_faktor));
                cmd.Parameters.Add(new SQLiteParameter("@qty_loqsheet", _item.qty_loqsheet));
                cmd.Parameters.Add(new SQLiteParameter("@qty", _item.qty));
                cmd.Parameters.Add(new SQLiteParameter("@shift", _item.shift));
                cmd.Parameters.Add(new SQLiteParameter("@fuel_oil_type", _item.fuel_oil_type));
                cmd.Parameters.Add(new SQLiteParameter("@stat_type", _item.stat_type));
                cmd.Parameters.Add(new SQLiteParameter("@nrp_operator", _item.nrp_operator));
                cmd.Parameters.Add(new SQLiteParameter("@nama_operator", _item.nama_operator));
                cmd.Parameters.Add(new SQLiteParameter("@work_area", _item.work_area));
                cmd.Parameters.Add(new SQLiteParameter("@location", _item.location));
                cmd.Parameters.Add(new SQLiteParameter("@ref_condition", _item.ref_condition));
                cmd.Parameters.Add(new SQLiteParameter("@ref_hour_start", _item.ref_hour_start));
                cmd.Parameters.Add(new SQLiteParameter("@ref_hour_stop", _item.ref_hour_stop));
                cmd.Parameters.Add(new SQLiteParameter("@note", _item.note));
                cmd.Parameters.Add(new SQLiteParameter("@timezone", _item.timezone));
                cmd.Parameters.Add(new SQLiteParameter("@flag_loading", _item.flag_loading));
                cmd.Parameters.Add(new SQLiteParameter("@loadingerror", _item.loadingerror));
                cmd.Parameters.Add(new SQLiteParameter("@loadingdatetime", _item.loadingdatetime));
                cmd.Parameters.Add(new SQLiteParameter("@mod_by", _item.mod_by));
                cmd.Parameters.Add(new SQLiteParameter("@mod_date", _item.mod_date));
                cmd.Parameters.Add(new SQLiteParameter("@is_load_to_fosto", _item.is_load_to_fosto));
                cmd.Parameters.Add(new SQLiteParameter("@foto", _item.foto));
                cmd.Parameters.Add(new SQLiteParameter("@flow_meter_start", _item.flow_meter_start));
                cmd.Parameters.Add(new SQLiteParameter("@flow_meter_end", _item.flow_meter_end));
                cmd.Parameters.Add(new SQLiteParameter("@resp_code", _item.resp_code));
                cmd.Parameters.Add(new SQLiteParameter("@resp_name", _item.resp_name));
                cmd.Parameters.Add(new SQLiteParameter("@text_header", _item.text_header));
                cmd.Parameters.Add(new SQLiteParameter("@text_sub_header", _item.text_sub_header));
                cmd.Parameters.Add(new SQLiteParameter("@text_body", _item.text_body));
                cmd.Parameters.Add(new SQLiteParameter("@syncs", _item.syncs));

                recs = cmd.ExecuteNonQuery();

                if (recs > 0)
                {
                    //if (Application.OpenForms["FTransaction"] != null)
                    //{
                    //    (Application.OpenForms["FTransaction"] as FTransaction).clearData();
                    //}

                    //this.Close();
                }
                if (conn.State == ConnectionState.Open) conn.Close();
            }
            catch (Exception ex)
            {
                save_error_log(ex.ToString(), "saveData()");
                MessageBox.Show(ex.ToString());
            }

        }

        void save_error_log(string data, string actions)
        {
            try
            {
                DateTime localDate = DateTime.Now;
                var _item = new tbl_log();
                var initData = GlobalModel.GlobalVar;

                _item.pid = System.Guid.NewGuid().ToString();
                _item.actions = $"District {initData.DataEmp.DstrctCode}/Form 2/{actions}";
                _item.message = $"{data}";
                _item.mod_date = localDate;
                _item.mod_by = initData.DataEmp.Nrp;

                SQLiteConnection conn;

                conn = new SQLiteConnection(myConnectionString);
                if (conn.State != ConnectionState.Open) conn.Open();
                string query = string.Empty;
                query = "INSERT INTO tbl_log(pid,actions,message,mod_date,mod_by)"
                      + " VALUES(@pid,@actions,@message,@mod_date,@mod_by)";

                SQLiteCommand cmd = new SQLiteCommand(query, conn);

                cmd.Parameters.Add(new SQLiteParameter("@pid", _item.pid));
                cmd.Parameters.Add(new SQLiteParameter("@actions", _item.actions));
                cmd.Parameters.Add(new SQLiteParameter("@message", _item.message));
                cmd.Parameters.Add(new SQLiteParameter("@mod_date", _item.mod_date));
                cmd.Parameters.Add(new SQLiteParameter("@mod_by", _item.mod_by));
                cmd.ExecuteNonQuery();
                if (conn.State == ConnectionState.Open) conn.Close();
            }
            catch (Exception)
            {
            }
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            var _proper = Properties.Settings.Default;
            pUrl_service = _proper.url_service;
            setMaxIterate();
            //myConnectionString = "Data Source=" + Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "SQLite", "db_iflash.db") + " ;New=False;";
            var passPhrase = "iFlashDesktop";
            var decryp = decryptor.DecryptPassword(_proper.db_pass, passPhrase);
            myConnectionString = "Data Source=" + Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "SQLite", "db_iflash.db") + " ;New=False;";

            txtLoad();
            txtRefSTart.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            stepTimerOpen = 0;

            doPrintLCR();
            doCloseLCR();

            timerQty.Enabled = false;
            timerClose.Enabled = false;
            timerOpen.Enabled = true;

            lblAlertClosed.Visible = false;
        }

        void setMaxIterate()
        {
            try
            {
                var _proper = Properties.Settings.Default;
                intMaxTick = _proper.maxTickSecond;
            }
            catch (Exception)
            {
                intMaxTick = 5;
            }
        } 

        void txtLoad()
        {
            txtGrossQty.Text = "";
            txtGrossTotalizer.Text = "";
            txtTotalizerStart.Text = "";
            txtRefSTart.Text = "";
            txtRefStop.Text = "";
        }

        private void timerOpen_Tick(object sender, EventArgs e)
        {
            if (stepTimerOpen == 1)
            {
                if (start())
                {
                    doStartLCR();
                    timerOpen.Enabled = false;
                    timerQty.Enabled = true;
                }
            }

            if (stepTimerOpen == 0)
            {
                if (open())
                {
                    doOpenLCR();
                    stepTimerOpen = 1;
                }
            }
        }

        private void timerQty_Tick(object sender, EventArgs e)
        {
            //if (!isProccessGet) getData();
            //timerQty.Enabled = false;
            //var threadQty = new System.Threading.Thread(WriteGetData);
            //threadQty.Start(); 
        } 

        private void WriteGetData()
        {
            while (stepTimerClose == 0)
            {
                getData();
                System.Threading.Thread.Sleep(500);
            }
        }


        private void timerClose_Tick(object sender, EventArgs e)
        {
            //if (stepTimerClose == 0)
            //{
            //    //stepTimerClose = 1; 
            //    if (getData())
            //    {
            //        txtGrossQty.Visible = true;
            //        this.LitersQty = Convert.ToDouble(txtGrossQty.Text.ToString());
            //        this.LitersTotalizerStart = Convert.ToDouble(txtTotalizerStart.Text.ToString());
            //        this.LitersTotalizerEnd = Convert.ToDouble(txtGrossTotalizer.Text.ToString());
            //        tryDataOk();
            //        this.DialogResult = DialogResult.OK; 
            //    }
            //}

            //if (stepTimerClose == 0)
            //{
            //    intCurrentTick += 1;
            //    if (intCurrentTick <= intMaxTick)
            //    {
            //        if (getData())
            //        {
            //            txtGrossQty.Visible = true;
            //            this.LitersQty = Convert.ToDouble(txtGrossQty.Text.ToString());
            //            tryDataOk();
            //            this.DialogResult = DialogResult.OK;
            //        }
            //    }
            //    else
            //    {
            //        timerClose.Enabled = false;
            //        showDialogConfirm();
            //    }

            //} 

            //if (stepTimerClose == 0)
            //{
            //    timerClose.Enabled = false;
            //    intCurrentTick += 1;
            //    if (intCurrentTick <= intMaxTick)
            //    {
            //        getData();
            //        System.Threading.Thread.Sleep(100);
            //        if (txtGrossQty.Text.Trim().ToUpper() == "WAIT.." || txtGrossQty.Text.Equals("0"))
            //        {
            //            timerClose.Enabled = true;
            //        }
            //        else
            //        {
            //            this.LitersQty = Convert.ToDouble(txtGrossQty.Text.ToString());
            //            this.LitersTotalizerStart = Convert.ToDouble(txtTotalizerStart.Text.ToString());
            //            this.LitersTotalizerEnd = Convert.ToDouble(txtGrossTotalizer.Text.ToString());
            //            saveDataAsync();
            //            this.DialogResult = DialogResult.OK;
            //        } 
            //    }
            //    else
            //    {
            //        showDialogConfirm();
            //    } 
            //} 

            if (stepTimerClose == 0)
            {
                //stepTimerClose = 1; 
                intCurrentTick += 1;
                if (intCurrentTick <= intMaxTick)
                {
                    if (getData())
                    {
                        txtGrossQty.Visible = true;
                        this.LitersQty = Convert.ToDouble(txtGrossQty.Text.ToString()); 
                        this.LitersTotalizerStart = Convert.ToDouble(txtTotalizerStart.Text.ToString());
                        this.LitersTotalizerEnd = Convert.ToDouble(txtGrossTotalizer.Text.ToString());
                        tryDataOk();
                        this.DialogResult = DialogResult.OK;
                    }
                }
                else
                {
                    timerClose.Enabled = false;
                    showDialogConfirm();
                }

            }
        }

        void showDialogConfirm()
        {
            DialogResult dialogResult = MessageBox.Show("Sistem gagal membaca data Qty terakhir", "Ulangi ?", MessageBoxButtons.RetryCancel, MessageBoxIcon.Question);

            if (dialogResult == DialogResult.Retry)
            {
                timerClose.Enabled = true;
                intCurrentTick = 0;
            }
            else
            {
                this.DialogResult = DialogResult.Abort;
            }
        }

        private void Form2_Closing(object sender, FormClosingEventArgs e)
        {
            doPrintLCR();
            doCloseLCR();

            timerOpen.Enabled = false;
            timerQty.Enabled = false;
            timerClose.Enabled = false;
        }

        private void Form2_Closed(object sender, FormClosedEventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;

            doPrintLCR();
            doCloseLCR();

            timerOpen.Enabled = false;
            timerQty.Enabled = false;
            timerClose.Enabled = false;
        }

        private void txtTotalizerStart_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtGrossTotalizer_TextChanged(object sender, EventArgs e)
        {

        }


    }
}
