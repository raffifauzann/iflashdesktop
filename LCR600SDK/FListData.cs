﻿using Flurl.Http;
using MySql.Data.MySqlClient;
using System.Data.SQLite;
using System.IO;
using System.Reflection;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace LCR600SDK
{
    public partial class FListData : Form
    {
        private string myConnectionString = string.Empty;
        private string pUrl_service = string.Empty;
        private int limit = 10;
        private int currentTotalData = 0;
        private decimal totalPage;
        private int idxCell = -1;
        private string job_rowid = "";
        private string valueHM = "";
        private string valueHMEdited = "";
        private string valueHMBefore = "";
        private bool statusOnline = false;
        PasswordEncrypDecrypt decryptor = new PasswordEncrypDecrypt();
        WriteLogText log = new WriteLogText();
        public FListData()
        {
            InitializeComponent();
        }

        private void FListData_Load(object sender, EventArgs e)
        {
            dateTimePicker1.Value = DateTime.Now;
            dateTimePicker2.Value = DateTime.Now;
            var _proper = Properties.Settings.Default;
            pUrl_service = _proper.url_service;
            statusOnline = _proper.connection == "ONLINE" ? true : false;
            dateTimePicker1.Text = DateTime.Now.ToString("yyyy-MM-dd");
            dateTimePicker2.Text = DateTime.Now.ToString("yyyy-MM-dd");
            //myConnectionString = "Data Source=" + Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "SQLite", "db_iflash.db") + " ;New=False;";
            var passPhrase = "iFlashDesktop";
            var decryp = decryptor.DecryptPassword(_proper.db_pass, passPhrase);
            myConnectionString = "Data Source=" + Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "SQLite", "db_iflash.db") + " ;New=False;";
            loadGridSearch(true, "");
            loadGrid();
            getTotalIssuing();
            

        }
        void dataGridView1_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            dataGridView1.BorderStyle = BorderStyle.None;
            dataGridView1.AlternatingRowsDefaultCellStyle.BackColor = Color.FromArgb(238, 239, 249);
            dataGridView1.CellBorderStyle = DataGridViewCellBorderStyle.SingleHorizontal;
            dataGridView1.DefaultCellStyle.SelectionBackColor = Color.SeaGreen;
            dataGridView1.DefaultCellStyle.SelectionForeColor = Color.WhiteSmoke;
            dataGridView1.BackgroundColor = Color.FromArgb(0, 101, 179);
            dataGridView1.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridView1.EnableHeadersVisualStyles = false;
            dataGridView1.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
            dataGridView1.ColumnHeadersDefaultCellStyle.Font = new Font("MS Reference Sans Serif", 20);
            dataGridView1.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(0, 101, 179);
            dataGridView1.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
            if (dataGridView1.Columns.Count > 0)
            {
                dataGridView1.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                dataGridView1.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                dataGridView1.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                dataGridView1.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                dataGridView1.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                dataGridView1.Columns[5].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                dataGridView1.Columns[6].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            }

        }
        void loadGridSearch(bool isAll, String sKey)
        {
            try
            {
                String iQuery = querySql();

                //SQLiteConnection conn = new SQLiteConnection(myConnectionString);
                //SQLiteCommand cmd = new SQLiteCommand(iQuery, conn);
                using (SQLiteConnection conn = new SQLiteConnection(myConnectionString))
                {
                    if (conn.State != ConnectionState.Open) conn.Open();
                    using (SQLiteCommand cmd = new SQLiteCommand(iQuery, conn))
                    {
                        DataTable dataTable = new DataTable();
                        SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);

                        da.Fill(dataTable);

                        dataGridView1.DataSource = dataTable;
                        dataGridView1.Columns["job_row_id"].Visible = false;

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("LoadGridSearch Exception: Terjadi kesalahan pada saat menampilkan data");
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(ex.Message, "loadGridSearch()");

            }
        }

        public void loadGrid()
        {
            using (SQLiteConnection conn = new SQLiteConnection(myConnectionString))
            {
                if (conn.State != ConnectionState.Open) conn.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(querySql(), conn))
                {
                    DataTable dataTable = new DataTable();
                    SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);

                    da.Fill(dataTable);

                    dataGridView1.DataSource = dataTable;
                    dataGridView1.Columns["job_row_id"].Visible = false;
                }
            }
        }

        void btnSyncWait()
        {
            btnSync.Enabled = false;
            btnSync.Visible = false;
            btnPleaseWait.Enabled = false;
            btnPleaseWait.Visible = true;
        }
        void btnSyncEnabled()
        {
            btnSync.Enabled = true;
            btnSync.Visible = true;
            btnPleaseWait.Visible = false;
            btnPleaseWait.Enabled = false;
        }

        private void timerSyncIssuing_Tick_1(object sender, EventArgs e)
        {
            btnSyncWait();
        }

        public async Task<string> checkConnectivity()
        {
            try
            {
                WebClient requestCek = new WebClient();
                var str = requestCek.DownloadString(pUrl_service);
                return str;
            }
            catch (WebException ex)
            {
                return "";
            }
        }
        private async void btnSync_Click(object sender, EventArgs e)
        {
            try
            {
                if (job_rowid != "" && valueHM != "")
                {
                    updateHm();
                }
                var checkconnect = await checkConnectivity();
                if (checkconnect.Contains("Supply Service"))
                //if (statusOnline)
                {
                    onSyncAsync();
                    //loadGrid();
                }
                else
                {
                    var f = new FPopUpNotification("Status saat ini OFFLINE mohon untuk sambungkan jaringan dan ganti setting connection menjadi ONLINE \r\n Setting > User Setting > Connection > Save", "Failed To Connect");
                    f.ShowDialog();
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception: Terjadi kesalahan pada saat sync, mohon periksa log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //MessageBox.Show(ex.Message.ToString());                
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(ex.Message, "onSyncAsync()");
                timerSyncIssuing.Stop();
                btnSyncEnabled();
            }
        }

        async Task onSyncAsync()
        {
            var _proper = Properties.Settings.Default;
            var _auth = GlobalModel.loginModel;

            String _url = $"{pUrl_service}api/IssuingService/SyncRefuelingMandiri";
            try
            {
                List<tbl_logsheet_detail> SendListIssuing = new List<tbl_logsheet_detail>();
                var getData = getListIssuing();
                //var total = getData.Count();
                //double howMany = double.Parse(total.ToString()) / 10;
                //var counter = howMany.ToString().Split('.');
                //if (counter.Count() == 1)
                //{
                //    counter = howMany.ToString().Split(',');
                //}

                var listFailedJobRow = new List<List<string>>();
                if (getData.Count() == 0)
                {
                    timerSyncIssuing.Stop();
                    btnSyncEnabled();
                }
                else
                {
                    btnPleaseWait.Text = string.Format("Please Wait. {0} data dari {1} data", 0, getData.Count);
                    for (int i = 0; i < getData.Count; i++)
                    {

                        SendListIssuing.Add(getData[i]);

                        //if (SendListIssuing.Count == 1 /*&& int.Parse(counter[0]) != 0*/)
                        if (SendListIssuing.Count == int.Parse(_proper.data_per_kirim) /*&& int.Parse(counter[0]) != 0*/)
                        {
                            var json = Newtonsoft.Json.JsonConvert.SerializeObject(SendListIssuing);
                            ClsIssuing data = await _url.WithHeaders(new { Authorization = _auth.token }).PostJsonAsync(SendListIssuing).ReceiveJson<ClsIssuing>();
                            if (data.status)
                            {
                                btnPleaseWait.Text = string.Format("Please Wait. {0} data dari {1} data", (i + 1), getData.Count);
                            }
                            else
                            {
                                if (data.failed_job_row_id.Count > 0 && data.failed_job_row_id != null)
                                {
                                    listFailedJobRow.Add(data.failed_job_row_id);
                                    SendListIssuing = new List<tbl_logsheet_detail>();
                                }
                            }
                            //if (data.failed_job_row_id.Count > 0)
                            //{
                            //    listFailedJobRow.Add(data.failed_job_row_id);
                            //    SendListIssuing = new List<tbl_logsheet_detail>();
                            //}
                            //else
                            //{
                            //    btnPleaseWait.Text = string.Format("Please Wait. {0} data dari {1} data", (i+1), getData.Count);
                            //}
                            save_error_log(data.remarks, "onSyncAsync()");
                            updateTableSync(SendListIssuing, data);
                            loadGrid();
                            SendListIssuing = new List<tbl_logsheet_detail>();
                        }
                        else
                        {
                            if (i == getData.Count - 1 /*&& SendListIssuing.Count == int.Parse(counter[1])*/)
                            {
                                ClsIssuing data = await _url.WithHeaders(new { Authorization = _auth.token }).PostJsonAsync(SendListIssuing).ReceiveJson<ClsIssuing>();
                                if (data.status)
                                {
                                    btnPleaseWait.Text = string.Format("Please Wait. {0} data dari {1} data", (i + 1), getData.Count);
                                }
                                else
                                {
                                    if (data.failed_job_row_id.Count > 0 && data.failed_job_row_id != null)
                                    {
                                        listFailedJobRow.Add(data.failed_job_row_id);
                                        String failed_pids = String.Join(" \r\n- ", listFailedJobRow);
                                        btnPleaseWait.Text = string.Format("Please Wait. {0} data dari {1} data", (i + 1), getData.Count);
                                    }
                                }
                                //if (data.failed_job_row_id.Count > 0)
                                //{
                                //    listFailedJobRow.Add(data.failed_job_row_id);
                                //    String failed_pids = String.Join(" \r\n- ", listFailedJobRow);
                                //    btnPleaseWait.Text = string.Format("Please Wait. {0} data dari {1} data", (i + 1), getData.Count);
                                //}
                                //else
                                //{
                                //    btnPleaseWait.Text = string.Format("Please Wait. {0} data dari {1} data", (i + 1), getData.Count);
                                //}
                                save_error_log(data.remarks, "onSyncAsync()");
                                updateTableSync(SendListIssuing, data);
                                loadGrid();
                            }
                        }
                        if (i == getData.Count - 1)
                        {
                            timerSyncIssuing.Stop();
                            btnSyncEnabled();
                        }
                    }
                }
            }
            catch (FlurlHttpException ex)
            {
                var resp = ex.Message;
                if (ex.InnerException != null)
                {
                    resp = ex.InnerException.Message;
                }
                //var resp = await ex.GetResponseStringAsync();
                MessageBox.Show("FlurHTTPException: Terjadi kesalahan pada saat sync, mohon periksa log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(resp, "onSyncAsync()");
                timerSyncIssuing.Stop();
                btnSyncEnabled();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception: Terjadi kesalahan pada saat sync, mohon periksa log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(ex.Message, "onSyncAsync()");
                timerSyncIssuing.Stop();
                btnSyncEnabled();
            }



        }
        void save_error_log(string data, string actions)
        {
            try
            {
                DateTime localDate = DateTime.Now;
                var _item = new tbl_log();
                var initData = GlobalModel.GlobalVar;

                _item.pid = System.Guid.NewGuid().ToString();
                _item.actions = $"District {initData.DataEmp.DstrctCode}/FListData/{actions}";
                _item.message = $"{data}";
                _item.mod_date = localDate;
                _item.mod_by = initData.DataEmp.Nrp;

                using (SQLiteConnection conn = new SQLiteConnection(myConnectionString))
                {
                    if (conn.State != ConnectionState.Open) conn.Open();

                    string query = string.Empty;
                    query = "INSERT INTO tbl_log(pid,actions,message,mod_date,mod_by)"
                          + " VALUES(@pid,@actions,@message,@mod_date,@mod_by)";

                    using (SQLiteCommand cmd = new SQLiteCommand(query, conn))
                    {
                        cmd.Parameters.Add(new SQLiteParameter("@pid", _item.pid));
                        cmd.Parameters.Add(new SQLiteParameter("@actions", _item.actions));
                        cmd.Parameters.Add(new SQLiteParameter("@message", _item.message));
                        cmd.Parameters.Add(new SQLiteParameter("@mod_date", _item.mod_date));
                        cmd.Parameters.Add(new SQLiteParameter("@mod_by", _item.mod_by));
                        cmd.ExecuteNonQuery();
                    }

                }                
            }
            catch (Exception)
            {
            }
        }

        void updateTableSync(List<tbl_logsheet_detail> SendListIssuing, ClsIssuing sClsIssuing)
        {
            
            int recs = 0;
            string query = string.Empty;
            using (SQLiteConnection conn = new SQLiteConnection(myConnectionString))
            {
                if (conn.State != ConnectionState.Open) conn.Open();

                String failed_pids = String.Join("','", sClsIssuing.failed_job_row_id);
                string send_pid = String.Join("','", SendListIssuing.Select(x => x.job_row_id));

                if (sClsIssuing.failed_job_row_id.Count() > 0)
                {
                    var success_pid = send_pid.Replace(failed_pids, "");
                    query = $"update tbl_t_log_sheet_detail set syncs = 9 where job_row_id in('{failed_pids}') and syncs=3 AND issued_date between '" + dateTimePicker1.Value.ToString("yyyy-MM-dd 00:00:00") + "' and '" + dateTimePicker2.Value.ToString("yyyy-MM-dd 23:59:59") + "' and shift ='" + cbShift.Text + "'";
                    recs = execCmd(query, conn);

                    query = $"update tbl_t_log_sheet_detail set syncs = 7 where job_row_id in('{success_pid}') and syncs=3 AND issued_date between '" + dateTimePicker1.Value.ToString("yyyy-MM-dd 00:00:00") + "' and '" + dateTimePicker2.Value.ToString("yyyy-MM-dd 23:59:59") + "' and shift ='" + cbShift.Text + "'";
                    recs = execCmd(query, conn);
                }
                else
                {
                    query = $"update tbl_t_log_sheet_detail set syncs = 7 where job_row_id in('{send_pid}') and syncs=3 AND issued_date between '" + dateTimePicker1.Value.ToString("yyyy-MM-dd 00:00:00") + "' and '" + dateTimePicker2.Value.ToString("yyyy-MM-dd 23:59:59") + "' and shift ='" + cbShift.Text + "'";
                    recs = execCmd(query, conn);
                }
            }            

            if (Application.OpenForms["FMain"] != null)
            {
                (Application.OpenForms["FMain"] as FMain).toolStripStatusLabelStatus.Text = "Sync finish";
            }
        }

        int execCmd(String sQuery, SQLiteConnection sConn)
        {
            try
            {
                int iReturn = 0;

                var cmd = new SQLiteCommand(sQuery, sConn);
                iReturn = cmd.ExecuteNonQuery();

                return iReturn;
            }
            catch (Exception e)
            {
                return 0;
            }

        }

        int getOffset()
        {
            int ret = 0;
            if ((int)txtPage.Value > (int)Math.Round(totalPage))
            {
                var res = (int)txtPage.Value / limit;
                ret = (int)Math.Round(double.Parse(res.ToString()));
            }            
            else
            {
                ret = (int)((int)txtPage.Value * limit)-10;
            }
            return ret;
        }

        void getTotalIssuing()
        {
            SQLiteConnection conn = new SQLiteConnection(myConnectionString);
            if (conn.State != ConnectionState.Open) conn.Open();
            try
            {
                var sql = " SELECT Count(*) as total ";
                sql += " FROM vw_logsheet ";
                sql += " WHERE issued_date between  '" + dateTimePicker1.Value.ToString("yyyy-MM-dd 00:00:00") + "' and '" + dateTimePicker2.Value.ToString("yyyy-MM-dd 23:59:59") + "' and shift ='" + cbShift.Text + "' ";
                sql += " ORDER BY case when syncs = 2 then 1 ";
                sql += " when syncs = 9 then 2 ";
                sql += " when syncs = 3 then 3 ";
                sql += " when syncs = 7 then 4 ";
                sql += " END ASC ";
                sql += " , issued_date ASC ,ref_hour_start ASC ";
                SQLiteCommand cmd = new SQLiteCommand(sql, conn);
                SQLiteDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    lblTotalData.Text = reader["total"].ToString();
                }
                if(int.Parse(lblTotalData.Text) != currentTotalData)
                {
                    currentTotalData = int.Parse(lblTotalData.Text);
                    txtPage.Value = 1;
                    lblTotalPage.Text = "1";
                }
                totalPage = (decimal.Parse(lblTotalData.Text)+9) / (decimal)limit;
                if (totalPage <= 1){
                    totalPage = 1;
                    txtPage.Value = (decimal)totalPage;
                }
                lblTotalPage.Text = "/ " + (int)Math.Round(totalPage);
                txtPage.Maximum = (int)Math.Round(totalPage);
            }
            catch (SQLiteException ex)
            {

            }
        }

        List<tbl_logsheet_detail> getListIssuing()
        {
            List<tbl_logsheet_detail> _list = new List<tbl_logsheet_detail>();                            
            string query = string.Empty;
            SQLiteConnection conn = new SQLiteConnection(myConnectionString);
            if (conn.State != ConnectionState.Open) conn.Open();
            try
            {
                query = $"update tbl_t_log_sheet_detail set syncs = 3 where syncs in(2,9) AND issued_date between '" + dateTimePicker1.Value.ToString("yyyy-MM-dd 00:00:00") + "' and '" + dateTimePicker2.Value.ToString("yyyy-MM-dd 23:59:59") + "' and shift ='" + cbShift.Text + "'";                
                var recs = execCmd(query, conn);
                loadGrid();
                timerSyncIssuing.Start();
            }
            catch (SQLiteException ex)
            {
                MessageBox.Show("SQLiteException: Terjadi kesalahan pada get list issuing, mohon periksa log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(ex.ToString(), "getListIssuing()");
            }

            //query = "select * from tbl_t_log_sheet_detail where syncs=3 AND issued_date between '" + dateTimePicker1.Value.ToString("yyyy-MM-dd 00:00:00") + "' and '" + dateTimePicker2.Value.ToString("yyyy-MM-dd 23:59:59") + "' and shift ='" + cbShift.Text + "'  ORDER BY ref_hour_start";
            query = querySqlSync();
            try
            {

                SQLiteCommand cmd = new SQLiteCommand(query, conn);
                SQLiteDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var cls = new tbl_logsheet_detail();

                    cls.job_row_id = reader["job_row_id"].ToString();
                    cls.dstrct_code = reader["dstrct_code"].ToString();
                    cls.log_sheet_code = reader["log_sheet_code"].ToString();
                    cls.input_type = reader["input_type"].ToString();
                    cls.issued_date = Convert.ToDateTime(reader["issued_date"].ToString());
                    cls.whouse_id = reader["whouse_id"].ToString();
                    cls.unit_no = reader["unit_no"].ToString();
                    cls.max_tank_capacity = Convert.ToInt32(reader["max_tank_capacity"].ToString());
                    cls.hm_before = Convert.ToInt32(string.IsNullOrEmpty(reader["hm_before"].ToString()) ? "0" : reader["hm_before"].ToString());
                    cls.hm = Convert.ToInt32(reader["hm"].ToString());
                    cls.flw_meter = reader["flw_meter"].ToString();
                    cls.meter_faktor = Convert.ToDouble(reader["meter_faktor"].ToString());
                    cls.qty_loqsheet = Convert.ToDouble(reader["qty_loqsheet"].ToString());
                    cls.qty = Convert.ToDouble(reader["qty"].ToString());
                    cls.shift = reader["shift"].ToString();
                    cls.fuel_oil_type = reader["fuel_oil_type"].ToString();
                    cls.stat_type = reader["stat_type"].ToString();
                    cls.nrp_operator = reader["nrp_operator"].ToString();
                    cls.nama_operator = reader["nama_operator"].ToString();
                    cls.driver_nrp = reader["driver_nrp"].ToString();
                    cls.driver_name = reader["driver_name"].ToString();
                    cls.fuelman_nrp = reader["fuelman_nrp"].ToString();
                    cls.fuelman_name = reader["fuelman_name"].ToString();
                    cls.work_area = reader["work_area"].ToString();
                    cls.location = reader["location"].ToString();
                    cls.ref_condition = reader["ref_condition"].ToString();
                    cls.ref_hour_start = reader["ref_hour_start"].ToString();
                    cls.ref_hour_stop = reader["ref_hour_stop"].ToString();
                    cls.note = reader["note"].ToString();
                    cls.timezone = reader["timezone"].ToString();
                    cls.flag_loading = reader["flag_loading"].ToString();
                    cls.loadingerror = reader["loadingerror"].ToString();
                    cls.loadingdatetime = Convert.ToDateTime(reader["loadingdatetime"].ToString());
                    cls.mod_by = reader["mod_by"].ToString();
                    cls.mod_date = Convert.ToDateTime(reader["mod_date"].ToString());
                    cls.flow_meter_start = Convert.ToDouble(reader["flow_meter_start"].ToString());
                    cls.flow_meter_end = Convert.ToDouble(reader["flow_meter_end"].ToString());
                    cls.resp_code = reader["resp_code"].ToString();
                    cls.resp_name = reader["resp_name"].ToString();
                    cls.text_header = reader["text_header"].ToString();
                    cls.text_sub_header = reader["text_sub_header"].ToString();
                    cls.text_body = reader["text_body"].ToString();

                    _list.Add(cls);
                }
                reader.Dispose();
                conn.Close();
            }
            catch (SQLiteException ex)
            {
                //MessageBox.Show(ex.ToString());
                conn.Close();
                MessageBox.Show("Terjadi kesalahan saat mengambil data issuing, mohon cek log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(ex.Message, "getListTransfer()");
            }
            if (conn.State == ConnectionState.Open) conn.Close();
            return _list;
        }


        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (e.ColumnIndex == 5)
                {
                    idxCell = e.RowIndex;
                    var idxCOllumn = e.ColumnIndex;
                    var columnidx = dataGridView1.Rows[idxCell].Cells["hm"].ColumnIndex;

                    string value = dataGridView1.Rows[idxCell].Cells["hm"].FormattedValue.ToString();
                    string jobrowid = dataGridView1.Rows[idxCell].Cells["job_row_id"].FormattedValue.ToString();

                    if (valueHMEdited != valueHMBefore && job_rowid != jobrowid)
                    {
                        updateHm();
                        loadGridSearch(false, dateTimePicker2.Text);
                    }
                    valueHMBefore = value;
                    job_rowid = jobrowid;
                    if (idxCOllumn == columnidx)
                    {
                        DataGridViewCell cell = dataGridView1.Rows[idxCell].Cells["hm"];
                        dataGridView1.CurrentCell = cell;
                        dataGridView1.BeginEdit(true);
                    }
                }
                else
                {
                    valueHMEdited = dataGridView1.Rows[idxCell].Cells["hm"].FormattedValue.ToString();
                    if (valueHMEdited != valueHMBefore)
                    {
                        updateHm();
                        valueHMBefore = valueHMEdited;
                        loadGridSearch(false, dateTimePicker2.Text);

                    }

                }
            }
            showKeyboard();
        }
        private void dataGridView1_KeyUp(object sender, KeyEventArgs eKey)
        {
            if (eKey.KeyCode == Keys.Enter)
            {
                if (dataGridView1.CurrentRow.Index >= 0)
                {
                    idxCell = dataGridView1.CurrentRow.Index;
                    var idxCOllumn = dataGridView1.CurrentCell.ColumnIndex;
                    var columnidx = dataGridView1.Rows[idxCell].Cells["hm"].ColumnIndex;
                    var cek = dataGridView1;
                    if (idxCell > 0)
                    {
                        valueHMEdited = dataGridView1.Rows[idxCell - 1].Cells["hm"].FormattedValue.ToString();
                        job_rowid = dataGridView1.Rows[idxCell - 1].Cells["job_row_id"].FormattedValue.ToString();

                    }
                    if (idxCOllumn == columnidx)
                    {
                        if (valueHMBefore != valueHMEdited)
                        {
                            updateHm();
                        }

                        loadGridSearch(false, dateTimePicker2.Text);
                        DataGridViewCell cell = dataGridView1.Rows[idxCell].Cells["hm"];
                        dataGridView1.CurrentCell = cell;
                        dataGridView1.BeginEdit(true);
                    }

                }
            }
        }

        private void dataGridView1_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (e.RowIndex >= 0)
            {

                idxCell = dataGridView1.CurrentRow.Index;
                var idxCOllumn = dataGridView1.CurrentCell.ColumnIndex;
                var columnidx = dataGridView1.Rows[idxCell].Cells["hm"].ColumnIndex;
                DataGridViewCell cell = dataGridView1.Rows[idxCell].Cells["hm"];
                dataGridView1.CurrentCell = cell;
                valueHMBefore = dataGridView1.Rows[idxCell].Cells["hm"].FormattedValue.ToString();
                job_rowid = dataGridView1.Rows[idxCell].Cells["job_row_id"].FormattedValue.ToString();
            }
        }
        private void dataGridView1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {

                idxCell = dataGridView1.CurrentRow.Index;
                var idxCOllumn = dataGridView1.CurrentCell.ColumnIndex;
                var columnidx = dataGridView1.Rows[idxCell].Cells["hm"].ColumnIndex;
                DataGridViewCell cell = dataGridView1.Rows[idxCell].Cells["hm"];
                dataGridView1.CurrentCell = cell;
                valueHMEdited = dataGridView1.Rows[idxCell].Cells["hm"].FormattedValue.ToString();
                job_rowid = dataGridView1.Rows[idxCell].Cells["job_row_id"].FormattedValue.ToString();
            }

        }

        void updateHm()
        {
            try
            {
                btnSyncWait();
                btnPleaseWait.Text = string.Format("Please Wait. Updating HM");
                using (SQLiteConnection conn = new SQLiteConnection(myConnectionString))
                {
                    if (conn.State != ConnectionState.Open) conn.Open();
                    using (SQLiteCommand cmd = new SQLiteCommand("UPDATE tbl_t_log_sheet_detail SET syncs = 2, hm=" + Convert.ToInt32(valueHMEdited) + " WHERE job_row_id='" + job_rowid.ToString() + "';", conn))
                    {
                        //update tbl_t_log_sheet_detail set syncs = 3 where syncs                        
                        DataTable dataTable = new DataTable();
                        SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);

                        da.Fill(dataTable);
                        dataGridView1.DataSource = dataTable;
                        btnSyncEnabled();
                    }                  
                }                
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
                MessageBox.Show("Exception: Terjadi kesalahan pada saat update HM, mohon periksa log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(ex.Message, "updateHm()");

            }

        }

        public void showKeyboard()
        {
            try
            {
                ProcessStartInfo psStartInfo = new ProcessStartInfo();
                psStartInfo.FileName = @"C:\Program Files\Common Files\microsoft shared\ink\TabTip.exe";
                //var dir = Path.Combine(Environment.CurrentDirectory, "VKeyboard", "VKeyboard.exe");
                //psStartInfo.FileName = dir;

                Process ps = Process.Start(psStartInfo);

            }
            catch (Exception ex)
            {
                var error = ex.Message;
            }
        }

        void changeFilter()
        {
            DateTime fromdate = Convert.ToDateTime(dateTimePicker1.Text);
            DateTime todate = Convert.ToDateTime(dateTimePicker2.Text);

            if (fromdate <= todate)
            {
                TimeSpan ts = todate.Subtract(fromdate);
                int days = Convert.ToInt16(ts.Days);

                try
                {
                    String iQuery = querySql();// "SELECT issued_date,ref_hour_start, unit_no, hm, whouse_id, qty, shift , sync_desc, job_row_id FROM vw_logsheet where issued_date between '" + dateTimePicker1.Value.ToString("yyyy-MM-dd 00:00:00") + "' and '" + dateTimePicker2.Value.ToString("yyyy-MM-dd 23:59:59") + "' order by issued_date,ref_hour_start";
                    SQLiteConnection conn = new SQLiteConnection(myConnectionString);
                    SQLiteCommand cmd = new SQLiteCommand(iQuery, conn);

                    if (conn.State != ConnectionState.Open) conn.Open();
                    DataTable dataTable = new DataTable();

                    dataTable.Load(cmd.ExecuteReader());
                    dataGridView1.DataSource = dataTable;
                    if (conn.State == ConnectionState.Open) conn.Close();
                    getTotalIssuing();
                }
                catch (Exception)
                {

                    throw;
                }
            }
        }

        string querySql()
        {
            //var sql = "SELECT sync_desc, job_row_id,issued_date,ref_hour_start, unit_no, hm, whouse_id, qty, shift  FROM vw_logsheet where issued_date between '" + dateTimePicker1.Value.ToString("yyyy-MM-dd 00:00:00") + "' and '" + dateTimePicker2.Value.ToString("yyyy-MM-dd 23:59:59") + "' and shift ='" + cbShift.Text + "' order by sync_desc,issued_date desc,ref_hour_start desc ";
            var sql = " SELECT ROW_NUMBER () OVER ( ORDER BY ";
            sql += " case when syncs = 2 then 1 ";
            sql += " when syncs = 9 then 2 ";
            sql += " when syncs = 3 then 3 ";
            sql += " when syncs = 7 then 4 ";
            sql += " END ASC ";
            sql += " ,issued_date ASC ";
            sql += " , ref_hour_start ASC ";            
            sql += ") Row,sync_desc,job_row_id,issued_date, unit_no, hm,qty, shift ";
            sql += " FROM vw_logsheet ";
            sql += " WHERE issued_date between  '" + dateTimePicker1.Value.ToString("yyyy-MM-dd 00:00:00") + "' and '" + dateTimePicker2.Value.ToString("yyyy-MM-dd 23:59:59") + "' and shift ='" + cbShift.Text + "' ";
            sql += " ORDER BY case when syncs = 2 then 1 ";
            sql += " when syncs = 9 then 2 ";
            sql += " when syncs = 3 then 3 ";
            sql += " when syncs = 7 then 4 ";
            sql += " END ASC ";
            sql += " , issued_date ASC ,ref_hour_start ASC ";
            sql += " limit " + limit + " offset " + getOffset();
            return sql;
        }
        string querySqlSync()
        {
            //var sql = "SELECT sync_desc, job_row_id,issued_date,ref_hour_start, unit_no, hm, whouse_id, qty, shift  FROM vw_logsheet where issued_date between '" + dateTimePicker1.Value.ToString("yyyy-MM-dd 00:00:00") + "' and '" + dateTimePicker2.Value.ToString("yyyy-MM-dd 23:59:59") + "' and shift ='" + cbShift.Text + "' order by sync_desc,issued_date desc,ref_hour_start desc ";
            var sql = " SELECT * ";
            sql += " FROM tbl_t_log_sheet_detail ";
            sql += " WHERE syncs = 3 and issued_date between  '" + dateTimePicker1.Value.ToString("yyyy-MM-dd 00:00:00") + "' and '" + dateTimePicker2.Value.ToString("yyyy-MM-dd 23:59:59") + "' and shift ='" + cbShift.Text + "' ";
            sql += " ORDER BY case when syncs = 2 then 1 ";
            sql += " when syncs = 9 then 2 ";
            sql += " when syncs = 3 then 3 ";
            sql += " when syncs = 7 then 4 ";
            sql += " END ASC ";
            sql += ", issued_date ASC ,ref_hour_start ASC ";
            sql += " limit " + limit + " offset " + getOffset();
            return sql;
        }

        private void dateTimePicker2_CloseUp(object sender, EventArgs e)
        {
            changeFilter();
        }

        private void dateTimePicker1_CloseUp(object sender, EventArgs e)
        {
            changeFilter();
        }

        private void cbShift_SelectedIndexChanged(object sender, EventArgs e)
        {
            changeFilter();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void cbStatusIssuing_SelectedIndexChanged(object sender, EventArgs e)
        {
            changeFilter();
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            try
            {
                getTotalIssuing();
                int next = (int)txtPage.Value + 1;
                if (next > (int)Math.Round(totalPage))
                {
                    next = (int)Math.Round(totalPage);
                }
                txtPage.Value = (decimal)next;                
                changeFilter();
            }
            catch (Exception ex)
            {

            }
        }

        private void btnPrev_Click(object sender, EventArgs e)
        {
            try
            {
                getTotalIssuing();
                int prev = (int)txtPage.Value - 1;
                if (prev <= 0)
                {
                    prev = 0;
                }
                txtPage.Value = (decimal)prev;
                changeFilter();
            } catch (Exception ex)
            {

            }
        }


        private void txtPage_Click(object sender, EventArgs e)
        {
            try
            {
                ProcessStartInfo psStartInfo = new ProcessStartInfo();
                var dir = Path.Combine(Environment.CurrentDirectory, "VKeyboard", "VKeyboard.exe");
                psStartInfo.FileName = dir;
                //psStartInfo.FileName = @"C:\Program Files\Common Files\microsoft shared\ink\TabTip.exe";

                Process ps = Process.Start(psStartInfo);

            }
            catch (Exception ex)
            {
                var error = ex.Message;
            }
        }
    }
}
