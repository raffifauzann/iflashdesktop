﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LCR600SDK
{
    public partial class FAboutApp : Form
    {
        public FAboutApp()
        {
            InitializeComponent();
        }
        //protected override void WndProc(ref Message m)
        //{
        //    const int WM_NCCALCSIZE = 0x0083;
        //    if (m.Msg == WM_NCCALCSIZE && m.WParam.ToInt32() == 1)
        //    {
        //        return;
        //    }
        //    base.WndProc(ref m);
        //}
        private async void FAboutApp_Load(object sender, EventArgs e)
        {
            var _proper = Properties.Settings.Default;
            //string version = Assembly.GetExecutingAssembly().GetName().Version.ToString();            
            var pUrl_service = _proper.url_service;
            if (pUrl_service == "https://supplyservice.pamapersada.com/")
                lblUrlConnection.Text = "Production";
            else
                lblUrlConnection.Text = "Development";

            string version = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            lblAppVersion.Text = "V " + version;

            this.Text = GlobalModel.app_version;                               
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
