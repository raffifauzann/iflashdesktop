﻿using Flurl.Http;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SQLite;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LCR600SDK
{
    public partial class FLogin : Form
    {
        private string myConnectionString = string.Empty;
        private string pUrl_service = string.Empty;
        private bool statusOnline = false;
        private int bordersize = 2;
        private int countMax = 0;
        private int timeoutCheckConnection = int.Parse(Properties.Settings.Default.interval_check_online);
        PasswordEncrypDecrypt decryptor = new PasswordEncrypDecrypt();
        WriteLogText log = new WriteLogText();        
        string FullfilePath, destination, backupUserConfigPath, backupUserConfig;
        LoginModel _tokenDriver;
        List<string> historyLogin;


        public FLogin()
        {
            InitializeComponent();

        }
        private async void FLogin_Load(object sender, EventArgs e)
        {
            try
            {
                SQLiteConnection conn;
                var _proper = Properties.Settings.Default;
                var passPhrase = "iFlashDesktop";                
                var decryp = decryptor.DecryptPassword(_proper.db_pass, passPhrase);
                myConnectionString = "Data Source=" + Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "SQLite", "db_iflash.db") + " ;New=False;";

                string query = string.Empty;
                //pnlLoginFuelman.Hide();
                //pnlLoginDriver.Show();
                pUrl_service = _proper.url_service;
                if (pUrl_service == "https://supplyservice.pamapersada.com/")
                    lblUrlConnection.Text = "Production";
                else
                    lblUrlConnection.Text = "Development";                


                string version = Assembly.GetExecutingAssembly().GetName().Version.ToString();
                lblAppVersion.Text = "V " + version;
                GlobalModel.app_version = this.lblAppVersion.Text + " " + this.lblUrlConnection.Text;                
                             
                if (_proper.menu_grouping == "Transfer Issuing")
                {

                    //this.Text = "V " + version;                             
                    this.Text = GlobalModel.app_version;
                }
                else
                {
                    //this.Text = "V " + version + " Received";                
                    this.Text = GlobalModel.app_version + " Received";

                }

                try
                {
                    var checkconnect = await checkConnectivity();
                    if(checkconnect.Contains("Supply Service"))
                    {
                        btnConnection.Text = "ONLINE";
                        _proper.connection = "ONLINE";
                        _proper.Save();
                        _proper.Reload();
                        btnConnection.BackColor = Color.Green;
                        btnConnection.ForeColor = Color.White;
                        statusOnline = true;
                    }
                    else
                    {
                        btnConnection.Text = "OFFLINE";
                        _proper.connection = "OFFLINE";
                        _proper.Save();
                        _proper.Reload();
                        btnConnection.BackColor = Color.Red;
                        btnConnection.ForeColor = Color.White;
                    }
                    
                }
                catch (WebException ex)
                {
                    btnConnection.Text = "OFFLINE";
                    _proper.connection = "OFFLINE";
                    _proper.Save();
                    _proper.Reload();
                    btnConnection.BackColor = Color.Red;
                    btnConnection.ForeColor = Color.White;
                    statusOnline = false;
                }

                conn = new SQLiteConnection(myConnectionString);                
                if (conn.State != ConnectionState.Open) conn.Open();               

                log.WriteToFile("logLCR600.txt", "EXECUTE create table tbl_t_receive_direct ");
                query = "CREATE TABLE IF NOT EXISTS `tbl_t_receive_direct` (";
                query += "	`pid_receive` VARCHAR(50) NOT NULL ,";
                query += "	`district` VARCHAR(50) NOT NULL ,";
                query += "	`ref_id` VARCHAR(50) NOT NULL ,";
                query += "	`po_no` VARCHAR(50) NOT NULL ,";
                query += "	`receive_at` VARCHAR(50) NOT NULL ,";
                query += "	`receive_at_code` VARCHAR(50) NOT NULL ,";
                query += "	`receive_qty` INT(10) NOT NULL,";
                query += "	`receive_sn_flow_meter1` VARCHAR(50) NOT NULL ,";
                query += "	`receive_meter_faktor` VARCHAR(50) NOT NULL ,";
                query += "	`receive_density` VARCHAR(50) NOT NULL ,";
                query += "	`receive_temperature` VARCHAR(50) NOT NULL ,";
                query += "	`receive_by` VARCHAR(50) NOT NULL ,";
                query += "	`receive_date` DATE NOT NULL,";
                query += "	`sir_no` VARCHAR(50) NOT NULL ,";
                query += "	`receive_qty_pama` INT(10) NOT NULL,";
                query += "	`totaliser_awal` INT(10) NOT NULL,";
                query += "	`totaliser_akhir` INT(10) NOT NULL,";
                query += "	`start_loading` VARCHAR(50) NOT NULL ,";
                query += "	`end_loading` VARCHAR(50) NOT NULL ,";
                query += "	`syncs` INT(10) NULL DEFAULT NULL,";
                query += "	PRIMARY KEY (`pid_receive`)";
                query += ")";
                var recs = execCmd(query, conn);
                log.WriteToFile("logLCR600.txt", "DONE create table tbl_t_receive_direct ");

                log.WriteToFile("logLCR600.txt", "EXECUTE create view vw_receive_direct ");
                query = "DROP VIEW IF EXISTS vw_receive_direct ";
                recs = execCmd(query, conn);
                query = "CREATE ";
                query += " VIEW vw_receive_direct";
                query += " AS";
                query += " SELECT *,(case when (syncs = 2) then 'waiting' when (syncs = 3) then 'proccessing' when (syncs = 7) then 'uploaded' when (syncs = 9) then 'failed upload' ELSE 'NONE' end) AS sync_desc";
                query += " FROM tbl_t_receive_direct";

                recs = execCmd(query, conn);
                log.WriteToFile("logLCR600.txt", "DONE create view vw_receive_direct ");

                log.WriteToFile("logLCR600.txt", "EXECUTE create table nozzletag_m ");
                query = " CREATE TABLE IF NOT EXISTS  `nozzletag_m` ( ";
                query += " 	`pid`	varchar(50) NOT NULL DEFAULT (uuid()), ";
                query += " 	`nozzletag`	varchar(100) DEFAULT NULL, ";
                query += " 	`tagtype`	varchar(45) DEFAULT NULL, ";
                query += " 	`district`	varchar(4) DEFAULT NULL, ";
                query += " 	`lastupdate`	varchar(45) DEFAULT NULL, ";
                query += " 	PRIMARY KEY(`pid`) ";
                query += " ); ";

                recs = execCmd(query, conn);
                log.WriteToFile("logLCR600.txt", "DONE create table nozzletag_m ");

                log.WriteToFile("logLCR600.txt", "EXECUTE create view vw_trans_tagunit ");
                query = "DROP VIEW if EXISTS `vw_trans_tagunit`";
                recs = execCmd(query, conn);
                query = " CREATE VIEW `vw_trans_tagunit` AS ";
                query += "      select `a`.`EgiDesc` AS `tagid`,`a`.`UnitNo` AS `unitno`,`a`.`MaxTankCapacity` AS `maxtank`,`b`.`timestamps` AS `timestamps`,Cast ((JulianDay('now', 'localtime') -  JulianDay(`b`.`timestamps`)) * 24 *60 * 60 As Integer) AS `diffsec` ";
                query += " 	from (`unit` `a` join `taglogs` `b` on((`a`.`EgiDesc` = `b`.`tagid`))) order by `b`.`timestamps` desc ";

                recs = execCmd(query, conn);
                log.WriteToFile("logLCR600.txt", "DONE create view vw_trans_tagunit ");

                log.WriteToFile("logLCR600.txt", "EXECUTE create view vw_trans_tagnozzle ");
                query = "DROP VIEW if EXISTS `vw_trans_tagnozzle`";
                recs = execCmd(query, conn);
                query = " CREATE VIEW `vw_trans_tagnozzle` AS ";
                query += "     select `a`.`rfid_id` AS `tagid`, ";
                query += " `a`.`Nrp` AS `nozzleno`, ";
                query += " `b`.`timestamps` AS `timestamps`, ";
                query += " Cast ((JulianDay('now', 'localtime') -  JulianDay(`b`.`timestamps`)) * 24 *60 * 60 As Integer)AS `diffsec` ";
                query += " from (`fuelman` `a` join `taglogs` `b` on ((`a`.`rfid_id` = `b`.`tagid`))) order by `b`.`timestamps` desc ";

                recs = execCmd(query, conn);
                log.WriteToFile("logLCR600.txt", "DONE create view vw_trans_tagnozzle ");              


                log.WriteToFile("logLCR600.txt", "EXECUTE create view vw_current_tagunit ");
                query = "DROP VIEW if EXISTS `vw_current_tagunit`";
                recs = execCmd(query, conn);
                query = " CREATE VIEW `vw_current_tagunit` AS ";
                query += "     select `vw_trans_tagunit`.`tagid` AS `tagid`,`vw_trans_tagunit`.`unitno` AS `unitno`,`vw_trans_tagunit`.`maxtank` AS `maxtank`,`vw_trans_tagunit`.`timestamps` AS `timestamps`,`vw_trans_tagunit`.`diffsec` AS `diffsec` from `vw_trans_tagunit` where (`vw_trans_tagunit`.`diffsec` <= 30) limit 1 ";

                recs = execCmd(query, conn);
                log.WriteToFile("logLCR600.txt", "DONE create view vw_current_tagunit ");

                log.WriteToFile("logLCR600.txt", "EXECUTE create view vw_current_tagnozzle ");
                query = "DROP VIEW if EXISTS `vw_current_tagnozzle`";
                recs = execCmd(query, conn);
                query = " CREATE VIEW `vw_current_tagnozzle` AS ";
                query += " select `vw_trans_tagnozzle`.`tagid` AS `tagid`,`vw_trans_tagnozzle`.`nozzleno` AS `nozzleno`,`vw_trans_tagnozzle`.`timestamps` AS `timestamps`,`vw_trans_tagnozzle`.`diffsec` AS `diffsec` from `vw_trans_tagnozzle` where (`vw_trans_tagnozzle`.`diffsec` <= 30) limit 1 ";

                recs = execCmd(query, conn);
                log.WriteToFile("logLCR600.txt", "DONE create view vw_current_tagnozzle ");

                log.WriteToFile("logLCR600.txt", "EXECUTE create view vw_current_nozzleunit ");
                query = "DROP VIEW if EXISTS `vw_current_nozzleunit`";
                recs = execCmd(query, conn);

                query = " CREATE VIEW `vw_current_nozzleunit` AS ";
                query += " select `a`.`tagid` AS `nozzleid`,`a`.`nozzleno` AS `nozzleno`,`b`.`tagid` AS `unittagid`,`b`.`unitno` AS `unitno`,`b`.`maxtank` AS `maxtank`,(case when ((`a`.`tagid` is not null) and (`b`.`tagid` is not null)) then 1 else 0 end) AS `istagvalid` from (`vw_current_tagnozzle` `a` join `vw_current_tagunit` `b`) ";

                recs = execCmd(query, conn);
                log.WriteToFile("logLCR600.txt", "DONE create view vw_current_nozzleunit ");


                log.WriteToFile("logLCR600.txt", "EXECUTE create view vw_current_nozzleunit ");
                query = "DROP VIEW if EXISTS `vw_current_nozzleunit`";
                recs = execCmd(query, conn);

                query = " CREATE VIEW `vw_current_nozzleunit` AS ";
                query += " select `a`.`tagid` AS `nozzleid`,`a`.`nozzleno` AS `nozzleno`,`b`.`tagid` AS `unittagid`,`b`.`unitno` AS `unitno`,`b`.`maxtank` AS `maxtank`,(case when ((`a`.`tagid` is not null) and (`b`.`tagid` is not null)) then 1 else 0 end) AS `istagvalid` from (`vw_current_tagnozzle` `a` join `vw_current_tagunit` `b`) ";

                recs = execCmd(query, conn);
                log.WriteToFile("logLCR600.txt", "DONE create view vw_current_nozzleunit ");

                log.WriteToFile("logLCR600.txt", "check tbl_setting ");
                query = "PRAGMA table_info(tbl_setting)";
                SQLiteCommand cmd = new SQLiteCommand(query, conn);
                SQLiteDataReader reader = cmd.ExecuteReader();
                var cekColumnExist = false;                
                while (reader.Read())
                {
                    if (reader["name"].ToString() == "Whouseke")
                    {
                        cekColumnExist = true;
                    }
                    

                }
                reader.Close();
                if (!cekColumnExist)
                {
                    log.WriteToFile("logLCR600.txt", "EXECUTE alter tbl_setting ");                    
                    query = "ALTER TABLE tbl_setting ADD COLUMN `Whouseke`	INTEGER DEFAULT NULL";                    
                    recs = execCmd(query, conn);
                    log.WriteToFile("logLCR600.txt", "DONE alter tbl_setting ");
                }

                log.WriteToFile("logLCR600.txt", "check tbl_user_login ");
                query = "PRAGMA table_info(tbl_user_login)";
                cmd = new SQLiteCommand(query, conn);
                reader = cmd.ExecuteReader();                
                var cekLoginAsExist = false;
                var cekPidExist = false;
                while (reader.Read())
                {
                  
                    if (reader["name"].ToString() == "login_as")
                    {
                        cekLoginAsExist = true;
                    }
                    if (reader["name"].ToString() == "pid")
                    {
                        cekPidExist = true;
                    }

                }
                reader.Close();                

                if (!cekLoginAsExist)
                {
                    log.WriteToFile("logLCR600.txt", "EXECUTE alter tbl_user_login add login_as ");
                    query = "ALTER TABLE tbl_user_login ADD COLUMN `login_as`	varchar(50) DEFAULT NULL";
                    recs = execCmd(query, conn);
                    log.WriteToFile("logLCR600.txt", "DONE alter tbl_user_login add login_as ");
                }

                if (!cekPidExist)
                {

                    log.WriteToFile("logLCR600.txt", "EXECUTE drop tbl_user_login ");
                    query = "Drop TABLE tbl_user_login ";
                    recs = execCmd(query, conn);
                    log.WriteToFile("logLCR600.txt", "DONE drop tbl_user_login ");

                    log.WriteToFile("logLCR600.txt", "EXECUTE new tbl_user_login  ");
                    query = " CREATE TABLE 'tbl_user_login' ( ";
                    query += " 	'pid'	varchar(50) NOT NULL, ";
                    query += " 	'nrp'	varchar(50) NOT NULL, ";
                    query += " 	'password'	varchar(255) NOT NULL, ";
                    query += " 	'name'	varchar(50) NOT NULL, ";
                    query += " 	'position'	varchar(50) NOT NULL, ";
                    query += " 	'district'	varchar(50) NOT NULL, ";
                    query += " 	'token'	varchar(500) NOT NULL DEFAULT '', ";
                    query += " 	'expired_date'	date NOT NULL DEFAULT '1900-01-01', ";
                    query += " 	'login_as'	varchar(50) DEFAULT NULL, ";
                    query += " 	PRIMARY KEY(\"pid\") ";
                    query += " ); ";                    
                    recs = execCmd(query, conn);
                    log.WriteToFile("logLCR600.txt", "DONE New tbl_user_login ");                    
                }

                log.WriteToFile("logLCR600.txt", "check unit ");
                query = "PRAGMA table_info(unit)";
                cmd = new SQLiteCommand(query, conn);
                reader = cmd.ExecuteReader();                
                var cekPidUnitExist = false;
                while (reader.Read())
                {                    
                    if (reader["name"].ToString() == "pid")
                    {
                        cekPidUnitExist = true;
                    }

                }
                reader.Close();               

                if (!cekPidUnitExist)
                {

                    log.WriteToFile("logLCR600.txt", "EXECUTE drop unit ");
                    query = "Drop TABLE IF EXISTS unit ";
                    recs = execCmd(query, conn);
                    log.WriteToFile("logLCR600.txt", "DONE drop unit ");

                    log.WriteToFile("logLCR600.txt", "EXECUTE new unit  ");
                    query = " CREATE TABLE 'unit' ( ";
                    query += " 	'pid'	varchar(50) NOT NULL, ";
                    query += " 	'UnitNo'	varchar(50) NOT NULL, ";
                    query += " 	'District'	varchar(50) DEFAULT NULL, ";
                    query += " 	'Egi'	varchar(50) DEFAULT NULL, ";
                    query += " 	'EgiDesc'	varchar(100) DEFAULT NULL, ";
                    query += " 	'Locations'	varchar(50) DEFAULT NULL, ";
                    query += " 	'EquipClass'	varchar(50) DEFAULT NULL, ";
                    query += " 	'Status'	varchar(50) DEFAULT NULL, ";
                    query += " 	'Starttime'	varchar(50) DEFAULT NULL, ";
                    query += " 	'Endtime'	varchar(50) DEFAULT NULL, ";
                    query += " 	'Priority'	varchar(50) DEFAULT NULL, ";
                    query += " 	'TextHeader'	varchar(50) DEFAULT NULL, ";
                    query += " 	'TextBody'	varchar(50) DEFAULT NULL, ";
                    query += " 	'MaxTankCapacity'	int DEFAULT NULL, ";
                    query += " 	'WorkArea'	varchar(50) DEFAULT NULL, ";
                    query += " 	'LatestHm'	int DEFAULT NULL, ";
                    query += " 	PRIMARY KEY(\"pid\") ";
                    query += " ) ";
                    recs = execCmd(query, conn);
                    log.WriteToFile("logLCR600.txt", "DONE New unit ");
                }

                log.WriteToFile("logLCR600.txt", "check tbl_t_log_sheet_detail ");
                query = "PRAGMA table_info(tbl_t_log_sheet_detail)";
                cmd = new SQLiteCommand(query, conn);
                reader = cmd.ExecuteReader();
                cekColumnExist = false;
                while (reader.Read())
                {
                    if (reader["name"].ToString() == "driver_nrp"
                        ||
                        reader["name"].ToString() == "driver_name"
                        ||
                        reader["name"].ToString() == "fuelman_nrp"
                        ||
                        reader["name"].ToString() == "fuelman_name")
                    {
                        cekColumnExist = true;
                    }
                    else
                    {
                        cekColumnExist = false;
                    }
                }
                reader.Close();
                if (!cekColumnExist)
                {
                    log.WriteToFile("logLCR600.txt", "EXECUTE alter tbl_t_log_sheet_detail ");
                    query = " ALTER TABLE tbl_t_log_sheet_detail ADD COLUMN `driver_nrp` varchar(50) DEFAULT NULL; ";
                    query += " ALTER TABLE tbl_t_log_sheet_detail ADD COLUMN `driver_name` varchar(50) DEFAULT NULL; ";
                    query += " ALTER TABLE tbl_t_log_sheet_detail ADD COLUMN `fuelman_nrp` varchar(50) DEFAULT NULL;";
                    query += " ALTER TABLE tbl_t_log_sheet_detail ADD COLUMN `fuelman_name` varchar(50) DEFAULT NULL";
                    recs = execCmd(query, conn);
                    log.WriteToFile("logLCR600.txt", "DONE alter tbl_t_log_sheet_detail ");
                }

                log.WriteToFile("logLCR600.txt", "EXECUTE create tbl_driver ");                
                query = " CREATE TABLE IF NOT EXISTS 'driver'( ";
                query += "                     'nrp'   varchar(50), ";
                query += "                     'driver_name'   varchar(50), ";
                query += "                     'district'  varchar(50), ";
                query += "                     'position'  varchar(50), ";
                query += "                     'status'    INTEGER, ";
                query += "                     PRIMARY KEY('nrp') ";
                query += "                 ); ";
                
                recs = execCmd(query, conn);
                log.WriteToFile("logLCR600.txt", "DONE create tbl_driver ");

                log.WriteToFile("logLCR600.txt", "EXECUTE alter vw_receive_maintank_ritasi");

                query = " DROP VIEW \"main\".\"vw_receive_maintank_ritasi\"; ";
                query += " CREATE VIEW 'vw_receive_maintank_ritasi' as ";
                query += " select  ";
                query += " [pid_ritasi_maintank]   , ";
                query += " [send_date],	 ";
                query += " [send_po_no],	 ";
                query += " 	[district]   ,	 ";
                query += " 	[receive_at]  , ";
                query += " 	[receive_at_code]  , ";
                query += " 	[receive_dipping_hole1_mt]  , ";
                query += " 	[receive_dipping_hole2_mt]  , ";
                query += " 	[receive_sn_flow_meter1]  , ";
                query += " 	[receive_meter_faktor]  , ";
                query += " 	[receive_flow_meter_awal1]  , ";
                query += " 	[receive_flow_meter_akhir1]  , ";
                query += " 	[receive_qty1]  , ";
                query += " 	[receive_qty1_x_mf]  , ";
                query += " 	[receive_sn_flow_meter2]  , ";
                query += " 	[receive_meter_faktor2]  , ";
                query += " 	[receive_flow_meter_awal2]  , ";
                query += " 	[receive_flow_meter_akhir2]  , ";
                query += " 	[receive_qty2]  , ";
                query += " 	[receive_qty2_x_mf]  , ";
                query += " 	[receive_qty_total]  , ";
                query += " 	[receive_density]  , ";
                query += " 	[receive_temperature]  , ";
                query += " 	[receive_qty_additive]  , ";
                query += " 	[receive_iscomplete]  , ";
                query += " 	[receive_driver_name]  , ";
                query += " 	[receive_no_segel_1]  , ";
                query += " 	[receive_no_segel_2]  , ";
                query += " 	[receive_no_segel_3]  , ";
                query += " 	[receive_no_segel_4]  , ";
                query += " 	[receive_no_segel_5]  , ";
                query += " 	[receive_no_segel_6]  ,	 ";
                query += " 	[doc_status]  , ";
                query += " 	[last_mod_by]  ,		 ";
                query += " 	[foto]  ,	 ";
                query += " 	[time_start]  , ";
                query += " 	[time_end]  , ";
                query += " 	`syncs` AS `syncs`, ";
                query += "         (CASE ";
                query += "             WHEN (`syncs` = 2) THEN 'waiting' ";
                query += "             WHEN (`syncs` = 3) THEN 'proccessing' ";
                query += "             WHEN (`syncs` = 7) THEN 'uploaded' ";
                query += "             WHEN (`syncs` = 9) THEN 'failed upload' ";
                query += "             ELSE 'none' ";
                query += "         END) AS `sync_desc` ";
                query += " from tbl_t_receive_maintank_ritasi ";

                recs = execCmd(query, conn);
                log.WriteToFile("logLCR600.txt", "DONE alter vw_receive_maintank_ritasi ");




                if (conn.State == ConnectionState.Open) conn.Close();
                hideResults();
                //countMax = timeoutCheckConnection/1000;
                //tmr_counting.Enabled = true;
                
                //tmr_counting.Start();
            }
            catch (Exception ex)
            {
                log.WriteToFile("logLCR600.txt", "FLogin_Load exception : " + ex.Message);
            }

        }

        void hideResults()
        {
            listHistoryLogin.Visible = false;
        }
        void showResults()
        {
            listHistoryLogin.Visible = true;
     

            listHistoryLogin.Size = new Size(328, 280);

        }

        void hideResultsDriver()
        {
            listHistoryLoginDriver.Visible = false;
        }

        void showResultsDriver()
        {
            listHistoryLoginDriver.Visible = true;
            listHistoryLoginDriver.Size = new Size(328, 280);

        }

        private void getHistoryLogin()
        {
            historyLogin = new List<string>();
            SQLiteConnection conn = new SQLiteConnection(myConnectionString);
            try
            {
                if (conn.State != ConnectionState.Open) conn.Open();
                var query = "SELECT * FROM tbl_user_login where login_as = 'Fuelman'";
                SQLiteCommand command = new SQLiteCommand(query, conn);
                SQLiteDataReader reader = command.ExecuteReader();

                List<ListItem> items = new List<ListItem>();
                while (reader.Read())
                {
                    items.Add(new ListItem(reader["nrp"].ToString(), reader["nrp"].ToString()));
                }

                listHistoryLogin.DataSource = items;
                listHistoryLogin.ValueMember = "value";
                listHistoryLogin.DisplayMember = "text";

                reader.Close();
                if (conn.State == ConnectionState.Open) conn.Close();

            }
            catch (SQLiteException ex)
            {
                if (conn.State == ConnectionState.Open) conn.Close();
                MessageBox.Show("SQLiteException: Terjadi kesalahan saat get user login local, mohon cek log LCR600");
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(ex.Message, "getUserLogin()");
                //MessageBox.Show(ex.Message);
            }
        }

        private void getHistoryLoginDriver()
        {
            historyLogin = new List<string>();
            SQLiteConnection conn = new SQLiteConnection(myConnectionString);
            try
            {
                if (conn.State != ConnectionState.Open) conn.Open();
                var query = "SELECT * FROM tbl_user_login where login_as = 'Driver'";
                SQLiteCommand command = new SQLiteCommand(query, conn);
                SQLiteDataReader reader = command.ExecuteReader();

                List<ListItem> items = new List<ListItem>();
                while (reader.Read())
                {
                    items.Add(new ListItem(reader["nrp"].ToString(), reader["nrp"].ToString()));
                }

                listHistoryLoginDriver.DataSource = items;
                listHistoryLoginDriver.ValueMember = "value";
                listHistoryLoginDriver.DisplayMember = "text";

                reader.Close();
                if (conn.State == ConnectionState.Open) conn.Close();

            }
            catch (SQLiteException ex)
            {
                if (conn.State == ConnectionState.Open) conn.Close();
                MessageBox.Show("SQLiteException: Terjadi kesalahan saat get user login local, mohon cek log LCR600");
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(ex.Message, "getHistoryLoginDriver()");
                //MessageBox.Show(ex.Message);
            }
        }

        private async void btnLogin_Click(object sender, EventArgs e)
        {
            //loginDone();            
            log.WriteToFile("logLCR600.txt", "Start Login Fuelman");
            btnLogin.Enabled = false;
            btnConnection.Enabled = false;
            log.WriteToFile("logLCR600.txt", "Check User Fuelman, Pass and Database Connection");
            if (isDbValid() && isInputValid())
            {
                loginOnLoading();
                log.WriteToFile("logLCR600.txt", "User pass Fuelman valid and database valid");
                log.WriteToFile("logLCR600.txt", "Check Online or Offline");
                var checkconnect = await checkConnectivity();
                if (checkconnect.Contains("Supply Service"))
                //if (statusOnline)
                {
                    log.WriteToFile("logLCR600.txt", "Connection Online");                    
                    loginAccountAsync();
                }
                else
                {
                    LoginLocal();
                }

            }
            btnLogin.Enabled = true;
        }

        bool isInputValid()
        {
            if (txtUSername.Text.Trim().Length == 0)
            {
                MessageBox.Show("Username harus diisi!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                btnConnection.Enabled = true;
            }

            else if (txtPassword.Text.Trim().Length == 0)
            {
                MessageBox.Show("Password harus diisi!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                btnConnection.Enabled = true;
            }
            else return true;


            return false;
        }
        bool isInputValidDriver()
        {
            if (txtUsernameDriver.Text.Trim().Length == 0)
            {
                MessageBox.Show("Username harus diisi!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                btnConnection.Enabled = true;
            }

            else if (txtPasswordDriver.Text.Trim().Length == 0)
            {
                MessageBox.Show("Password harus diisi!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                btnConnection.Enabled = true;
            }
            else return true;


            return false;
        }
        bool isDbValid()
        {
            bool iBlStatus = false;
            SQLiteConnection conn = new SQLiteConnection(myConnectionString);

            try
            {
                
                if (conn.State != ConnectionState.Open) conn.Open();
                iBlStatus = true;
                if (conn.State == ConnectionState.Open) conn.Close();
            }
            catch (SQLiteException es)
            {
                if (conn.State == ConnectionState.Open) conn.Close();
                MessageBox.Show("Terjadi kesalahan mohon cek log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", "isDbValid() Exception : " + es.Message);
                save_error_log(es.Message,"isDbValid()");
                //MessageBox.Show(es.Message.ToString());
            }
            return iBlStatus;
        }
        void save_error_log(string data, string actions)
        {
            SQLiteConnection conn = new SQLiteConnection(myConnectionString);
            try
            {
                DateTime localDate = DateTime.Now;
                var _item = new tbl_log();
                var initData = GlobalModel.GlobalVar;

                _item.pid = System.Guid.NewGuid().ToString();
                _item.actions = $"District {initData.DataEmp.DstrctCode}/FMain/{actions}";
                _item.message = $"{data}";
                _item.mod_date = localDate;
                _item.mod_by = initData.DataEmp.Nrp;

                
                if (conn.State != ConnectionState.Open) conn.Open();
                string query = string.Empty;
                query = "INSERT INTO tbl_log(pid,actions,message,mod_date,mod_by)"
                      + " VALUES(@pid,@actions,@message,@mod_date,@mod_by)";

                SQLiteCommand cmd = new SQLiteCommand(query, conn);

                cmd.Parameters.Add(new SQLiteParameter("@pid", _item.pid));
                cmd.Parameters.Add(new SQLiteParameter("@actions", _item.actions));
                cmd.Parameters.Add(new SQLiteParameter("@message", _item.message));
                cmd.Parameters.Add(new SQLiteParameter("@mod_date", _item.mod_date));
                cmd.Parameters.Add(new SQLiteParameter("@mod_by", _item.mod_by));
                cmd.ExecuteNonQuery();
                if (conn.State == ConnectionState.Open) conn.Close();
            }
            catch (Exception)
            {
                if (conn.State == ConnectionState.Open) conn.Close();
            }
        }


        void loginOnLoading()
        {
            lblMesage.Visible = true;
            btnLogin.Enabled = false;
            lblMesageDriver.Visible = true;
            btnLoginDriver.Enabled = false;
        }

        void LoginDriverDone()
        {
            lblMesage.Visible = false;
            btnLogin.Enabled = true;
            pnlLoginDriver.Hide();
            pnlLoginFuelman.Show();
        }

        void loginDone()
        {
            lblMesage.Visible = true;
            btnLogin.Enabled = true;
            this.Hide();
            var fwh = new FWarehouse();
            fwh.ShowDialog();
            //FMain fMain = new FMain();
            //fMain.Show(this);
            
        }

        async Task loginAccountAsync()
        {
            log.WriteToFile("logLCR600.txt", "TryLoginAccountAsync");
            try
            {
                lblMesage.Text = "Connecting to server..";
                log.WriteToFile("logLCR600.txt", "Connecting to server..");
                await getMasterDataAsync();
            }
            catch (FlurlHttpException ex)
            {
                log.WriteToFile("logLCR600.txt", "TryLoginAccountAsync exception");
                log.WriteToFile("logLCR600.txt", "Flurl ex content: " + ex.ToString());
                log.WriteToFile("logLCR600.txt", "Flurl ex.message content: " + ex.Message);
                log.WriteToFile("logLCR600.txt", "Flurl Inner Exeption: " + ex.InnerException.ToString());

                MessageBox.Show("Ada kendala saat login mohon periksa logLCR600.txt");
            }
            catch (Exception ex)
            {
                log.WriteToFile("logLCR600.txt", "TryLoginAccountAsync exception");
                log.WriteToFile("logLCR600.txt", "ex content: "+ex.ToString());
                log.WriteToFile("logLCR600.txt", "ex.message content: " + ex.Message);
                log.WriteToFile("logLCR600.txt", "Inner Exeption: "+ex.InnerException.ToString());
                
                MessageBox.Show("Ada kendala saat login mohon periksa logLCR600.txt");
            }
            
        }
        async Task loginAccountDriverAsync()
        {
            log.WriteToFile("logLCR600.txt", "loginAccountDriverAsync");
            try
            {
                lblMesageDriver.Text = "Validate user..";
                log.WriteToFile("logLCR600.txt", "Validate user..");
                await getDataDriverAsync();
                
            }
            catch (FlurlHttpException ex)
            {
                log.WriteToFile("logLCR600.txt", "TryloginAccountDriverAsync exception");
                log.WriteToFile("logLCR600.txt", "Flurl ex content: " + ex.ToString());
                log.WriteToFile("logLCR600.txt", "Flurl ex.message content: " + ex.Message);
                log.WriteToFile("logLCR600.txt", "Flurl Inner Exeption: " + ex.InnerException.ToString());

                MessageBox.Show("Ada kendala saat login mohon periksa logLCR600.txt");
            }
            catch (Exception ex)
            {
                log.WriteToFile("logLCR600.txt", "TryloginAccountDriverAsync exception");
                log.WriteToFile("logLCR600.txt", "ex content: " + ex.ToString());
                log.WriteToFile("logLCR600.txt", "ex.message content: " + ex.Message);
                log.WriteToFile("logLCR600.txt", "Inner Exeption: " + ex.InnerException.ToString());

                MessageBox.Show("Ada kendala saat login mohon periksa logLCR600.txt");
            }

        }

        private void LoginLocal()
        {
            lblMesage.Text = "Trying login from local..";
            getUserLogin(txtUSername.Text, txtPassword.Text);
        }
        private void LoginLocalDriver()
        {
            lblMesage.Text = "Trying login from local..";
            getUserLoginDriver(txtUsernameDriver.Text, txtPasswordDriver.Text);
        }

        

        private void getUserLogin(string pnrp, string password)
        {
            SQLiteConnection conn = new SQLiteConnection(myConnectionString);
            try
            {
                if (conn.State != ConnectionState.Open) conn.Open();
                var query = "SELECT * FROM tbl_user_login where nrp =" + $"'{pnrp.Substring(1)}' and login_as ='Fuelman'";
                SQLiteCommand command = new SQLiteCommand(query, conn);
                SQLiteDataReader reader = command.ExecuteReader();

                if (reader.Read())
                {
                    var decryptPass = reader["password"].ToString();
                    var pass = decryptor.DecryptPassword(decryptPass, password);
                    if (pass == password)
                    {
                        GlobalModel.GlobalVar = new PlanCoachProfileModel();

                        DataEmp employe = new DataEmp();
                        employe.Nrp = reader["nrp"].ToString();
                        employe.Name = reader["name"].ToString();
                        employe.Position = reader["position"].ToString();
                        employe.DstrctCode = reader["district"].ToString();

                        GlobalModel.GlobalVar.DataEmp = employe;

                        getMasterDataLocal();
                        var expired = DateTime.Parse(GlobalModel.loginModel.expired_date);
                        var dateNow = DateTime.Now;
                        if (GlobalModel.loginModel.token != null && expired < dateNow)
                        {
                            var f = new FPopUpNotification("Session login expired! mohon login kembali dengan koneksi internet", "Session Expired");
                            lblMesage.Text = "Session login Expired!";
                            f.ShowDialog();
                        }
                        else
                        {
                            reader.Close();
                            if (conn.State == ConnectionState.Open) conn.Close();
                            save_access_log();
                            loginDone();
                        }

                    }
                    else
                    {                        
                        MessageBox.Show("Gagal Login! password salah silahkan coba kembali");
                        lblMesage.Text = "Gagal Login!";
                    }                    
                }
                else
                {
                    
                    MessageBox.Show("Gagal Login! User tidak ditemukan, silahkan login menggunakan internet dahulu");
                }

                reader.Close();
                if (conn.State == ConnectionState.Open) conn.Close();

            }
            catch (SQLiteException ex)
            {
                if (conn.State == ConnectionState.Open) conn.Close();
                MessageBox.Show("SQLiteException: Terjadi kesalahan saat get user login local, mohon cek log LCR600");
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(ex.Message, "getUserLogin()");
                //MessageBox.Show(ex.Message);
            }
        }
        private void getUserLoginDriver(string pnrp, string password)
        {
            SQLiteConnection conn = new SQLiteConnection(myConnectionString);
            try
            {
                if (conn.State != ConnectionState.Open) conn.Open();
                var query = "SELECT * FROM tbl_user_login where nrp =" + $"'{pnrp.Substring(1)}' and login_as ='Driver'";
                SQLiteCommand command = new SQLiteCommand(query, conn);
                SQLiteDataReader reader = command.ExecuteReader();

                if (reader.Read())
                {
                    var decryptPass = reader["password"].ToString();
                    var pass = decryptor.DecryptPassword(decryptPass, password);
                    if (pass == password)
                    {
                        GlobalModel.GlobalVar = new PlanCoachProfileModel();

                        DataEmp employe = new DataEmp();
                        employe.Nrp = reader["nrp"].ToString();
                        employe.Name = reader["name"].ToString();
                        employe.Position = reader["position"].ToString();
                        employe.DstrctCode = reader["district"].ToString();

                        GlobalModel.GlobalVar.DataEmp = employe;

                        getMasterDataLocal();
                        var expired = DateTime.Parse(GlobalModel.loginModel.expired_date);
                        var dateNow = DateTime.Now;
                        if (GlobalModel.loginModel.token != null && expired < dateNow)
                        {
                            var f = new FPopUpNotification("Session login expired! mohon login kembali dengan koneksi internet", "Session Expired");
                            lblMesage.Text = "Session login Expired!";
                            f.ShowDialog();
                        }
                        else
                        {
                            reader.Close();
                            if (conn.State == ConnectionState.Open) conn.Close();
                            save_access_log();
                            LoginDriverDone();
                        }

                    }
                    else
                    {
                        MessageBox.Show("Gagal Login! password salah silahkan coba kembali");
                        lblMesageDriver.Text = "Gagal Login!";
                    }
                }
                else
                {

                    MessageBox.Show("Gagal Login! User tidak ditemukan, silahkan login menggunakan internet dahulu");
                    lblMesageDriver.Text = "Gagal Login!";
                }

                reader.Close();
                if (conn.State == ConnectionState.Open) conn.Close();

            }
            catch (SQLiteException ex)
            {
                if (conn.State == ConnectionState.Open) conn.Close();
                MessageBox.Show("SQLiteException: Terjadi kesalahan saat get user login local, mohon cek log LCR600");
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(ex.Message, "getUserLogin()");
                //MessageBox.Show(ex.Message);
            }
        }

        async Task getMasterDataAsync()
        {
            log.WriteToFile("logLCR600.txt", "Start get master data");
            String _url = $"{pUrl_service}api/FuelService/getMasterFuelNew?timezone={GlobalModel.timezone}";
            log.WriteToFile("logLCR600.txt", "Get Token User");
            LoginModel _token = await getTokenAsync();

            if (_token.success)
            {
                log.WriteToFile("logLCR600.txt", "Token User valid");
                try
                {
                    log.WriteToFile("logLCR600.txt", "Wait Data from API");
                    var data = await _url.WithHeaders(new { Authorization = _token.token }).GetJsonAsync<PlanCoachProfileModel>();
                    log.WriteToFile("logLCR600.txt", "Wait Data from API Finish");
                    GlobalModel.GlobalVar = data;
                    log.WriteToFile("logLCR600.txt", "Save Master Data to Local");
                    saveData(data);
                    log.WriteToFile("logLCR600.txt", "Finish Save Master Data to Local");
                    log.WriteToFile("logLCR600.txt", "Save Access Log");
                    save_access_log();
                    log.WriteToFile("logLCR600.txt", "Finish Save Access Log");
                    log.WriteToFile("logLCR600.txt", "Get Master Local");
                    getMasterDataLocal();
                    log.WriteToFile("logLCR600.txt", "Get Master Local Finish");
                    log.WriteToFile("logLCR600.txt", "Login Done");
                    loginDone();                    
                    lblMesage.Text = "Success";
                }
                catch (FlurlHttpException ex)
                {
                    //loginDone();
                    //MessageBox.Show(ex.ToString()); 
                    if(ex.InnerException != null)
                    {
                        log.WriteToFile("logLCR600.txt", "Message: "+ex.Message+" InnerException: "+ ex.InnerException);
                        save_error_log("Message: " + ex.Message + " InnerException: " + ex.InnerException, "getMasterDataAsync()");
                    }
                    else
                    {
                        log.WriteToFile("logLCR600.txt", ex.Message);
                        save_error_log(ex.Message, "getMasterDataAsync()");
                    }
                    
                    
                    MessageBox.Show("Terjadi kesalahan saat login, periksa kembali jaringan anda atau ada kesalahan pada data master");
                    LoginLocal();
                }
            }
            else
            {

                //lblMesage.Text = "Login gagal!";
                lblMesage.Text = _token.remarks;
                btnLogin.Enabled = true;
                btnConnection.Enabled = true;

            }
        }
        async Task getDataDriverAsync()
        {
            SQLiteConnection conn = new SQLiteConnection(myConnectionString);
            if (conn.State != ConnectionState.Open) conn.Open();
            log.WriteToFile("logLCR600.txt", "Start get master data");
            String _url = $"{pUrl_service}api/FuelService/getMasterFuelNew?timezone={GlobalModel.timezone}";
            log.WriteToFile("logLCR600.txt", "Get Token User Driver");
            LoginModel _token = await getTokenDriverAsync();

            if (_token.success)
            {
                log.WriteToFile("logLCR600.txt", "Token User Driver valid");
                try
                {
                    log.WriteToFile("logLCR600.txt", "Wait Data from API");
                    var data = await _url.WithHeaders(new { Authorization = _token.token }).GetJsonAsync<PlanCoachProfileModel>();
                    log.WriteToFile("logLCR600.txt", "Wait Data from API Finish");
                    GlobalModel.GlobalVar = data;
                    log.WriteToFile("logLCR600.txt", "Save Data Driver to Local");
                    if (data.DataEmp != null && GlobalModel.loginModel != null)
                    {
                        var query = "SELECT * FROM tbl_user_login where nrp =" + $"'{data.DataEmp.Nrp}' and login_as = 'Driver'";
                        SQLiteCommand command = new SQLiteCommand(query, conn);
                        SQLiteDataReader reader = command.ExecuteReader();

                        if (reader.Read())
                        {
                            
                            query = "REPLACE INTO tbl_user_login (pid,nrp,password,name,position,district,token,expired_date,login_as) VALUES" +
                            BuildQueryUserLoginOffline(reader["pid"].ToString(),data.DataEmp, GlobalModel.loginModel.token, GlobalModel.loginModel.expired_date, txtPasswordDriver.Text,"Driver");
                            reader.Close();
                            var recs = execCmd(query, conn);
                        }
                        else
                        {
                            
                            query = "INSERT INTO tbl_user_login (pid,nrp,password,name,position,district,token,expired_date,login_as) VALUES" +
                            BuildQueryUserLoginOffline(Guid.NewGuid().ToString(),data.DataEmp, GlobalModel.loginModel.token, GlobalModel.loginModel.expired_date, txtPasswordDriver.Text, "Driver");
                            reader.Close();
                            var recs = execCmd(query, conn);
                        }
                    }
                    log.WriteToFile("logLCR600.txt", "Save Data Driver to Local Finish");
                    if (conn.State == ConnectionState.Open) conn.Close();
                    LoginDriverDone();
                    lblMesageDriver.Text = "Success";
                }
                catch (FlurlHttpException ex)
                {
                    //loginDone();
                    //MessageBox.Show(ex.ToString()); 
                    if (ex.InnerException != null)
                    {
                        log.WriteToFile("logLCR600.txt", "Message: " + ex.Message + " InnerException: " + ex.InnerException);
                        save_error_log("Message: " + ex.Message + " InnerException: " + ex.InnerException, "getMasterDataAsync()");
                    }
                    else
                    {
                        log.WriteToFile("logLCR600.txt", ex.Message);
                        save_error_log(ex.Message, "getMasterDataAsync()");
                    }


                    MessageBox.Show("Terjadi kesalahan saat login, periksa kembali jaringan anda atau ada kesalahan pada data master");
                    LoginLocalDriver();
                }
                catch (Exception ex)
                {
                    //loginDone();
                    //MessageBox.Show(ex.ToString()); 
                    if (ex.InnerException != null)
                    {
                        log.WriteToFile("logLCR600.txt", "Message: " + ex.Message + " InnerException: " + ex.InnerException);
                        save_error_log("Message: " + ex.Message + " InnerException: " + ex.InnerException, "getMasterDataAsync()");
                    }
                    else
                    {
                        log.WriteToFile("logLCR600.txt", ex.Message);
                        save_error_log(ex.Message, "getMasterDataAsync()");
                    }


                    MessageBox.Show("Terjadi kesalahan saat login, periksa kembali jaringan anda atau ada kesalahan pada data master");
                    LoginLocalDriver();
                }
            }
            else
            {

                //lblMesage.Text = "Login gagal!";
                lblMesageDriver.Text = _token.remarks;
                btnLoginDriver.Enabled = true;
                btnConnection.Enabled = true;

            }
        }
        void save_access_log()
        {
            SQLiteConnection conn = new SQLiteConnection(myConnectionString);
            try
            {
                
                if (conn.State != ConnectionState.Open) conn.Open();
                string query = string.Empty;
                query = "INSERT INTO tbl_access_log (pid,login_nrp,login_token,login_date,login_active) VALUES" +
                              BuildQueryAccessLog();
                var recs = execCmd(query, conn);
                if (conn.State == ConnectionState.Open) conn.Close();
            }
            catch (SQLiteException ex)
            {
                if (conn.State == ConnectionState.Open) conn.Close();
                MessageBox.Show("Terjadi kesalahan mohon cek log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(ex.Message, "save_access_log()");
                //MessageBox.Show(ex.Message);
            }

        }
        void getMasterDataLocal()
        {
            GlobalModel.GlobalVar.FlowMeter = getListFlowMeter();
            GlobalModel.GlobalVar.Transportir = getListTransportir();
            GlobalModel.GlobalVar.Fuelman = getListFuelman();
            GlobalModel.GlobalVar.Unit = getListUnit();
            GlobalModel.GlobalVar.Gl = getListGl();
            GlobalModel.loginModel = getTokenOffline();            

        }
        List<FlowMeter> getListFlowMeter()
        {
            List<FlowMeter> _list = new List<FlowMeter>();
            SQLiteConnection conn;

            conn = new SQLiteConnection(myConnectionString);
            if (conn.State != ConnectionState.Open) conn.Open();
            string query = string.Empty;


            query = "select * from flowmeter where DstrctCode=" + "'" + GlobalModel.GlobalVar.DataEmp.DstrctCode + "'";

            SQLiteCommand cmd = new SQLiteCommand(query, conn);
            SQLiteDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                var cls = new FlowMeter();
                cls.DstrctCode = reader["DstrctCode"].ToString();
                cls.Warehouse = reader["Warehouse"].ToString();
                cls.SnCode = reader["SnCode"].ToString();
                cls.Fungsi = reader["Fungsi"].ToString();
                cls.Mnemonic = reader["Mnemonic"].ToString();
                cls.MeterFakor = int.Parse(reader["MeterFakor"].ToString());
                cls.KapMaks = int.Parse(reader["KapMaks"].ToString());
                cls.KalibartionDate = int.Parse(reader["KalibartionDate"].ToString());
                cls.KalibrationExpired = int.Parse(reader["KalibrationExpired"].ToString());
                cls.WhouseLocation = reader["WhouseLocation"].ToString();
                cls.FlowMeterLocation = reader["FlowMeterLocation"].ToString();
                cls.UpdateDate = reader["UpdateDate"].ToString();
                cls.UserNrp = reader["UserNrp"].ToString();
                cls.FlowMeterNo = reader["FlowMeterNo"].ToString();
                cls.Status = int.Parse(reader["Status"].ToString());
                cls.LcrDevice = reader["LcrDevice"].ToString();
                _list.Add(cls);
            }
            reader.Dispose();
            if (conn.State == ConnectionState.Open) conn.Close();
            return _list;
        }
        List<Transportir> getListTransportir()
        {
            List<Transportir> _list = new List<Transportir>();
            SQLiteConnection conn;

            conn = new SQLiteConnection(myConnectionString);
            if (conn.State != ConnectionState.Open) conn.Open();
            string query = string.Empty;


            query = "select * from transportir where district=" + "'" + GlobalModel.GlobalVar.DataEmp.DstrctCode + "'";

            SQLiteCommand cmd = new SQLiteCommand(query, conn);
            SQLiteDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                var cls = new Transportir();
                cls.TransportirCode = reader["transportir_code"].ToString();
                cls.TransportirName = reader["transportir_name"].ToString();
                cls.Isactive = bool.Parse(reader["isactive"].ToString());
                cls.District = reader["district"].ToString();
                _list.Add(cls);
            }
            reader.Dispose();
            if (conn.State == ConnectionState.Open) conn.Close();
            return _list;
        }
        List<Fuelman> getListFuelman()
        {
            List<Fuelman> _list = new List<Fuelman>();
            SQLiteConnection conn;

            conn = new SQLiteConnection(myConnectionString);
            if (conn.State != ConnectionState.Open) conn.Open();
            string query = string.Empty;


            query = "select * from fuelman where District=" + "'" + GlobalModel.GlobalVar.DataEmp.DstrctCode + "'";

            SQLiteCommand cmd = new SQLiteCommand(query, conn);
            SQLiteDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                var cls = new Fuelman();
                cls.Nrp = reader["Nrp"].ToString();
                cls.FuelmanName = reader["FuelmanName"].ToString();
                cls.Position = reader["Position"].ToString();
                cls.Status = long.Parse(reader["Status"].ToString());
                cls.District = reader["District"].ToString();
                cls.rfid_id = reader["rfid_id"].ToString();
                _list.Add(cls);
            }

            reader.Dispose();
            if (conn.State == ConnectionState.Open) conn.Close();
            return _list;
        }
        List<Unit> getListUnit()
        {
            List<Unit> _list = new List<Unit>();
            SQLiteConnection conn;

            conn = new SQLiteConnection(myConnectionString);
            if (conn.State != ConnectionState.Open) conn.Open();
            string query = string.Empty;


            query = "select * from unit where District=" + "'" + GlobalModel.GlobalVar.DataEmp.DstrctCode + "'";

            SQLiteCommand cmd = new SQLiteCommand(query, conn);
            SQLiteDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                var cls = new Unit();
                cls.UnitNo = reader["UnitNo"].ToString();
                cls.District = reader["District"].ToString();
                cls.Egi = reader["Egi"].ToString();
                cls.EgiDesc = reader["EgiDesc"].ToString();
                cls.Locations = reader["Locations"].ToString();
                cls.EquipClass = reader["EquipClass"].ToString();
                cls.Status = reader["Status"].ToString();
                cls.Starttime = reader["Starttime"].ToString();
                cls.Endtime = reader["Endtime"].ToString();
                cls.Priority = reader["Priority"].ToString();
                cls.TextHeader = reader["TextHeader"].ToString();
                cls.TextBody = reader["TextBody"].ToString();
                cls.MaxTankCapacity = long.Parse(reader["MaxTankCapacity"].ToString());
                cls.WorkArea = reader["WorkArea"].ToString();
                cls.LatestHm = long.Parse(reader["LatestHm"].ToString());

                _list.Add(cls);
            }

            reader.Dispose();
            if (conn.State == ConnectionState.Open) conn.Close();
            return _list;
        }
        List<DataEmp> getListGl()
        {
            List<DataEmp> _list = new List<DataEmp>();
            SQLiteConnection conn;

            conn = new SQLiteConnection(myConnectionString);
            if (conn.State != ConnectionState.Open) conn.Open();
            string query = string.Empty;


            query = "select * from gl";

            SQLiteCommand cmd = new SQLiteCommand(query, conn);
            SQLiteDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                var cls = new DataEmp();
                cls.Nrp = reader["Nrp"].ToString();
                cls.Name = reader["Name"].ToString();
                cls.Position = reader["Position"].ToString();
                cls.DstrctCode = reader["DstrctCode"].ToString();

                _list.Add(cls);
            }

            reader.Dispose();
            if (conn.State == ConnectionState.Open) conn.Close();
            return _list;
        }

        LoginModel getTokenOffline()
        {
            SQLiteConnection conn;

            conn = new SQLiteConnection(myConnectionString);
            if (conn.State != ConnectionState.Open) conn.Open();
            string query = string.Empty;

            query = "select * from tbl_user_login where nrp =" + $"'{GlobalModel.GlobalVar.DataEmp.Nrp}'";

            SQLiteCommand cmd = new SQLiteCommand(query, conn);
            SQLiteDataReader reader = cmd.ExecuteReader();
            var cls = new LoginModel();
            while (reader.Read())
            {
                cls.token = reader["token"].ToString();
                cls.expired_date = reader["expired_date"].ToString();
            }

            reader.Dispose();
            if (conn.State == ConnectionState.Open) conn.Close();
            return cls;
        }
        
        async Task<LoginModel> getTokenAsync()
        {
            LoginModel _token = new LoginModel();
            String _url = $"{pUrl_service}api/token";
            //p61121702
            //
            try
            {
                _token = await _url.PostJsonAsync(new { username = txtUSername.Text, password = txtPassword.Text, app = "fuel" }).ReceiveJson<LoginModel>();
                GlobalModel.loginModel = _token;
            }
            catch (Exception ex)
            {
                //loginDone();
                //MessageBox.Show(ex.ToString());
                
                
                if (ex.Message.Contains("Call Failed"))
                {
                    MessageBox.Show("Server sedang gangguan mohon dicoba kembali");
                }
                else
                {
                    MessageBox.Show("Jaringan Tidak Ada, Pastikan IFlash Terkoneksi Jaringan");
                }

                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(ex.Message, "getTokenAsync()");

            }

            return _token;
        }

        async Task<LoginModel> getTokenDriverAsync()
        {
            LoginModel _token = new LoginModel();
            String _url = $"{pUrl_service}api/token";
            //p61121702
            //
            try
            {
                _token = await _url.PostJsonAsync(new { username = txtUsernameDriver.Text, password = txtPasswordDriver.Text, app = "fuel" }).ReceiveJson<LoginModel>();
                GlobalModel.loginModel = _token;
            }
            catch (Exception ex)
            {
                //loginDone();
                //MessageBox.Show(ex.ToString());


                if (ex.Message.Contains("Call Failed"))
                {
                    MessageBox.Show("Server sedang gangguan mohon dicoba kembali");
                }
                else
                {
                    MessageBox.Show("Jaringan Tidak Ada, Pastikan IFlash Terkoneksi Jaringan");
                }

                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(ex.Message, "getTokenDriverAsync()");

            }

            return _token;
        }
        private void btnCancel_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        void saveData(PlanCoachProfileModel sData)
        {
            SQLiteConnection conn = new SQLiteConnection(myConnectionString);

            try
            {                
                if (conn.State != ConnectionState.Open) conn.Open();
                string query = string.Empty;
                int recs = 0;

                lblMesage.Text = "Loading data ..";
                if (sData.Fuelman.Count() > 0)
                {
                    lblMesage.Text = "Loading data fuelman..";
                    query = "DELETE FROM fuelman";
                    recs = execCmd(query, conn);

                    InsertBulkFuelman(sData.Fuelman, conn);
                    //query = "INSERT INTO fuelman (Nrp,FuelmanName,District,Position,Status,rfid_id) VALUES" +
                    //      BuildQueryFuelman(sData.Fuelman);
                    //recs = execCmd(query, conn);
                }

                if (sData.Driver.Count() > 0)
                {
                    lblMesage.Text = "Loading data Driver..";
                    query = "DELETE FROM driver";
                    recs = execCmd(query, conn);

                    InsertBulkDriver(sData.Driver, conn);
                    //query = "INSERT INTO fuelman (Nrp,FuelmanName,District,Position,Status,rfid_id) VALUES" +
                    //      BuildQueryFuelman(sData.Fuelman);
                    //recs = execCmd(query, conn);
                }

                if (sData.DataEmp != null && GlobalModel.loginModel != null)
                {
                    query = "SELECT * FROM tbl_user_login where nrp =" + $"'{sData.DataEmp.Nrp}' and login_as = 'Fuelman'";
                    SQLiteCommand command = new SQLiteCommand(query, conn);
                    SQLiteDataReader reader = command.ExecuteReader();

                    if (reader.Read())
                    {
                        
                        query = "REPLACE INTO tbl_user_login (pid,nrp,password,name,position,district,token,expired_date,login_as) VALUES" +
                        BuildQueryUserLoginOffline(reader["pid"].ToString(),sData.DataEmp, GlobalModel.loginModel.token, GlobalModel.loginModel.expired_date, txtPassword.Text,"Fuelman");
                        reader.Close();
                        recs = execCmd(query, conn);
                    }
                    else
                    {
                        
                        query = "INSERT INTO tbl_user_login (pid,nrp,password,name,position,district,token,expired_date,login_as) VALUES" +
                        BuildQueryUserLoginOffline(Guid.NewGuid().ToString(),sData.DataEmp, GlobalModel.loginModel.token, GlobalModel.loginModel.expired_date, txtPassword.Text, "Fuelman");
                        reader.Close();
                        recs = execCmd(query, conn);
                    }
                }


                if (sData.FlowMeter.Count() > 0)
                {
                    lblMesage.Text = "Loading data Flowmeter..";
                    query = "DELETE FROM Flowmeter";
                    recs = execCmd(query, conn);

                    query = "INSERT INTO Flowmeter (Pid,DstrctCode,Warehouse,SnCode,Fungsi,Mnemonic,MeterFakor,KapMaks,KalibartionDate,KalibrationExpired,WhouseLocation,FlowMeterLocation,UpdateDate,UserNrp,FlowMeterNo,Status,LcrDevice) VALUES" +
                                      BuildQueryFlowmeter(sData.FlowMeter);
                    recs = execCmd(query, conn);
                }

                if (sData.Unit.Count() > 0)
                {
                    //note EgiDesc di db local adalah rfid_id dari server
                    lblMesage.Text = "Loading data Unit..";
                    query = "DELETE FROM Unit";
                    recs = execCmd(query, conn);

                    query = "INSERT INTO Unit (pid,UnitNo,District,Egi,EgiDesc,Locations,EquipClass,Status,Starttime,Endtime,Priority,TextHeader,TextBody,MaxTankCapacity,WorkArea,LatestHm) VALUES" +
                                      BuildQueryUnit(sData.Unit);
                    recs = execCmd(query, conn);
                }

                if (sData.Gl.Count() > 0)
                {
                    lblMesage.Text = "Loading data Unit..";
                    query = "DELETE FROM GL";
                    recs = execCmd(query, conn);

                    query = "INSERT INTO GL (Nrp,Name,DstrctCode,Position) VALUES" +
                                      BuildQueryGl(sData.Gl);
                    recs = execCmd(query, conn);
                }

                if (sData.Transportir != null)
                {
                    lblMesage.Text = "Loading data Transportir..";
                    query = "DELETE FROM Transportir";
                    recs = execCmd(query, conn);

                    query = $"INSERT INTO Transportir (transportir_code,transportir_name,isactive,district) VALUES" +
                                      BuildQueryTransportir(sData.Transportir);

                    recs = execCmd(query, conn);
                }
                lblMesage.Text = "Finish Loading data ..";
                if (conn.State == ConnectionState.Open) conn.Close();
            }
            catch (SQLiteException ex)
            {
                if (conn.State == ConnectionState.Open) conn.Close();
                MessageBox.Show("Terjadi kesalahan mohon cek log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(ex.Message, "saveData()");
                //MessageBox.Show(ex.Message);
            }
        }

        int execCmd(String sQuery, SQLiteConnection sConn)
        {
            int iReturn = 0;

            var cmd = new SQLiteCommand(sQuery, sConn);
            iReturn = cmd.ExecuteNonQuery();

            return iReturn;
        }

        public string BuildQueryFuelman(IEnumerable<Fuelman> sFuelmans)
        {
            List<string> values = new List<string>();
            string query = string.Empty;
            int i = 0;
            foreach (var item in sFuelmans)
            {
                i++;
                query += $"('{item.Nrp}','{item.FuelmanName}','{item.District}','{item.Position}','{item.Status}','{item.rfid_id}')";

                if (i < sFuelmans.Count())
                    query += ",";
            }

            return query;
        }

        public string InsertBulkFuelman(List<Fuelman> sFuelmans, SQLiteConnection sConn)
        {
            List<string> values = new List<string>();
            string query = string.Empty;
            int i = 0;
            
            foreach (var _item in sFuelmans)
            {
                query = "INSERT INTO fuelman (Nrp,FuelmanName,District,Position,Status,rfid_id)";
                query += " VALUES(@Nrp,@FuelmanName,@District,@Position,@Status,@rfid_id)";                
                SQLiteCommand cmd = new SQLiteCommand(query, sConn);
                cmd.Parameters.Add(new SQLiteParameter("@Nrp", _item.Nrp));
                cmd.Parameters.Add(new SQLiteParameter("@FuelmanName", _item.FuelmanName));
                cmd.Parameters.Add(new SQLiteParameter("@District", _item.District));
                cmd.Parameters.Add(new SQLiteParameter("@Position", _item.Position));
                cmd.Parameters.Add(new SQLiteParameter("@Status", _item.Status));
                cmd.Parameters.Add(new SQLiteParameter("@rfid_id", _item.rfid_id));
                cmd.ExecuteNonQuery();
            }

            

            return query;
        }
        public string InsertBulkDriver(List<Driver> sDriver, SQLiteConnection sConn)
        {
            List<string> values = new List<string>();
            string query = string.Empty;
            int i = 0;

            foreach (var _item in sDriver)
            {
                query = "INSERT INTO driver (nrp,driver_name,district,position,status)";
                query += " VALUES(@Nrp,@FuelmanName,@District,@Position,@Status)";
                SQLiteCommand cmd = new SQLiteCommand(query, sConn);
                cmd.Parameters.Add(new SQLiteParameter("@Nrp", _item.Nrp));
                cmd.Parameters.Add(new SQLiteParameter("@FuelmanName", _item.DriverName));
                cmd.Parameters.Add(new SQLiteParameter("@District", _item.District));
                cmd.Parameters.Add(new SQLiteParameter("@Position", _item.Position));
                cmd.Parameters.Add(new SQLiteParameter("@Status", _item.Status));
                
                cmd.ExecuteNonQuery();
            }



            return query;
        }

        public string BuildQueryAccessLog()
        {
            var login = GlobalModel.loginModel;
            var user = GlobalModel.GlobalVar.DataEmp;
            List<string> values = new List<string>();
            string query = string.Empty;
            var login_date = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            int i = 0;
            var pid = Guid.NewGuid().ToString();
            query += $"('{pid}','{user.Nrp}','{login.token}','{login_date}','0')";

            return query;
        }
        public string BuildQueryUserLoginOffline(string pid,DataEmp sDataEmp, string token, string expired_date, string password,string login_as)
        {
            List<string> values = new List<string>();

            string query = string.Empty;
            var encryptedPass = decryptor.EncryptPassword(password, password);

            query += $"('{pid}','{sDataEmp.Nrp}','{encryptedPass}','{sDataEmp.Name}','{sDataEmp.Position}','{sDataEmp.DstrctCode}','{token}','{expired_date}','{login_as}')";


            return query;
        }

        public string BuildQueryFlowmeter(IEnumerable<FlowMeter> sFlowMeter)
        {
            List<string> values = new List<string>();
            string query = string.Empty;
            int i = 0;
            foreach (var item in sFlowMeter)
            {
                i++;
                query += $"('{item.Pid}','{item.DstrctCode}','{item.Warehouse}','{item.SnCode}','{item.Fungsi}','{item.Mnemonic}','1','{item.KapMaks}','{item.KalibartionDate}','{item.KalibrationExpired}','{item.WhouseLocation}','{item.FlowMeterLocation}','{item.UpdateDate}','{item.UserNrp}','{item.FlowMeterNo}','{item.Status}','{item.LcrDevice}')";

                if (i < sFlowMeter.Count())
                    query += ",";
            }

            return query;
        }

        public string BuildQueryUnit(IEnumerable<Unit> sUnit)
        {
            List<string> values = new List<string>();
            string query = string.Empty;
            int i = 0;
            foreach (var item in sUnit)
            {
                i++;
                item.pid = item.pid == null ? Guid.NewGuid().ToString() : item.pid;
                query += $"('{item.pid}','{item.UnitNo}','{item.District}','{item.Egi}','{item.rfid_id}','{item.Locations}','{item.EquipClass}','{item.Status}','{item.Starttime}','{item.Endtime}','{item.Priority}','{item.TextHeader}','{item.TextBody}','{item.MaxTankCapacity}','{item.WorkArea}','{item.LatestHm}')";

                if (i < sUnit.Count())
                    query += ",";
            }

            return query;
        }

        public string BuildQueryTransportir(IEnumerable<Transportir> sTransportir)
        {
            List<string> values = new List<string>();
            string query = string.Empty;
            int i = 0;
            foreach (var item in sTransportir)
            {
                i++;
                query += $"('{item.TransportirCode}','{item.TransportirName}','{item.Isactive}','{item.District}')";

                if (i < sTransportir.Count())
                    query += ",";
            }

            return query;
        }

        public string BuildQueryGl(IEnumerable<DataEmp> sGl)
        {
            List<string> values = new List<string>();
            string query = string.Empty;
            int i = 0;
            foreach (var item in sGl)
            {
                i++;
                query += $"('{item.Nrp}','{item.Name}','{item.DstrctCode}','{item.Position}')";

                if (i < sGl.Count())
                    query += ",";
            }

            return query;
        }

        void clearData()
        {
            txtUSername.Text = "";
            //txtPassword.Text = "";
            txtUSername.Focus();
            btnLogin.Enabled = true;
            lblMesage.Text = "";
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            clearData();
        }

        private async void loginFuelman()
        {
            //loginDone();            
            log.WriteToFile("logLCR600.txt", "Start Login");
            btnLogin.Enabled = false;
            btnConnection.Enabled = false;
            log.WriteToFile("logLCR600.txt", "Check User, Pass and Database Connection");
            if (isDbValid() && isInputValid())
            {
                loginOnLoading();
                log.WriteToFile("logLCR600.txt", "User pass valid and database valid");
                log.WriteToFile("logLCR600.txt", "Check Online or Offline");
                if (statusOnline)
                {
                    var checkconnect = await checkConnectivity();
                    if (checkconnect.Contains("Supply Service"))
                    //if (statusOnline)
                    {
                        log.WriteToFile("logLCR600.txt", "Connection Online");
                        loginAccountAsync();
                    }
                    else
                    {
                        lblMesage.Text = "Connecting to server..";
                        LoginLocal();
                    }
                }                    
                else
                {
                    LoginLocal();
                }

            }
            btnLogin.Enabled = true;
        }

        private async void loginDriver()
        {
            //loginDone();            
            log.WriteToFile("logLCR600.txt", "Start Login");
            btnLogin.Enabled = false;
            btnConnection.Enabled = false;
            log.WriteToFile("logLCR600.txt", "Check User, Pass and Database Connection");
            if (isDbValid() && isInputValidDriver())
            {
                loginOnLoading();
                log.WriteToFile("logLCR600.txt", "User pass valid and database valid");
                log.WriteToFile("logLCR600.txt", "Check Online or Offline");
                if (statusOnline)
                {
                    var checkconnect = await checkConnectivity();
                    if (checkconnect.Contains("Supply Service"))                    
                    {
                        log.WriteToFile("logLCR600.txt", "Connection Online");
                        loginAccountDriverAsync();
                    }
                    else
                    {
                        LoginLocalDriver();
                    }
                }
                else
                {
                    LoginLocalDriver();
                }

            }
            btnLogin.Enabled = true;
        }
        private void txtPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                loginFuelman();
            }
        }

        private void btnShowPass_Click(object sender, EventArgs e)
        {
            //btnShowPass.Text = btnShowPass.Text == "Show" ? "Hide" : "Show";
            if (iconBtnHide.Visible || iconButton1.Visible)
            {
                txtPassword.UseSystemPasswordChar = false;
                txtPasswordDriver.UseSystemPasswordChar = false;
                iconBtnHide.Visible = false;                
                iconBtnShow.Visible = true;

                iconButton1.Visible = false;
                iconButton2.Visible = true;
            }
            else
            {
                txtPassword.UseSystemPasswordChar = true;
                txtPasswordDriver.UseSystemPasswordChar = true;
                iconBtnHide.Visible = true;
                iconBtnShow.Visible = false;
                iconButton1.Visible = true;
                iconButton2.Visible = false;

            }
            //txtPassword.PasswordChar = txtPassword.PasswordChar == '*' ? '\0' : '*';
        }
        public async Task<string> checkConnectivity()
        {
            try
            {
                WebClient requestCek = new WebClient();
                var str = requestCek.DownloadString(pUrl_service);
                return str;
                //var request = (HttpWebRequest)WebRequest.Create(pUrl_service);
                //request.Timeout = timeoutCheckConnection;
                //return request.GetResponse();
            }
            catch (WebException ex)
            {
                return "";
            }
        }

        private async void btnOnlineOffline_ClickAsync(object sender, EventArgs e)
        {
            var _proper = Properties.Settings.Default;
            if (btnConnection.Text == "ONLINE")
            {
                btnConnection.Text = "OFFLINE";
                _proper.connection = "OFFLINE";
                _proper.Save();
                _proper.Reload();
                btnConnection.BackColor = Color.Red;
                btnConnection.ForeColor = Color.White;
                statusOnline = false;
            }
            else
            {
                btnConnection.Text = "CHECKING";
                btnConnection.BackColor = Color.Yellow;
                btnConnection.ForeColor = Color.Black;
                try
                {
                    var checkconnect = await checkConnectivity();
                    if(checkconnect.Contains("Supply Service"))
                    {
                        btnConnection.Text = "ONLINE";
                        _proper.connection = "ONLINE";
                        _proper.Save();
                        _proper.Reload();
                        btnConnection.BackColor = Color.Green;
                        btnConnection.ForeColor = Color.White;
                        statusOnline = true;
                    }
                    else
                    {
                        btnConnection.Text = "OFFLINE";
                        _proper.connection = "OFFLINE";
                        _proper.Save();
                        _proper.Reload();
                        btnConnection.BackColor = Color.Red;
                        btnConnection.ForeColor = Color.White;
                        statusOnline = true;
                    }
                    
                }
                catch (WebException ex)
                {                    
                    statusOnline = false;
                }

            }
            //countMax = timeoutCheckConnection / 1000;            
            //tmr_counting.Enabled = false;
            //tmr_counting.Enabled = true;
        }

        private void btnMinimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnMaximize_Click(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Normal)
            {
                this.WindowState = FormWindowState.Maximized;
            }
            else
            {
                this.WindowState = FormWindowState.Normal;
            }

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private async void tmr_counting_Tick(object sender, EventArgs e)
        {
            //lblCheckConnection.Visible = true;
            if (countMax == 0)
            {
                countMax = timeoutCheckConnection / 1000;
            }

            if (countMax == timeoutCheckConnection / 1000)
            {
                var _proper = Properties.Settings.Default;                
                btnConnection.Text = "CHECKING";
                btnConnection.BackColor = Color.Yellow;
                btnConnection.ForeColor = Color.Black;
                try
                {
                    var checkconnect = await checkConnectivity();
                    if (checkconnect.Contains("Supply Service"))
                    {
                        btnConnection.Text = "ONLINE";
                        _proper.connection = "ONLINE";
                        _proper.Save();
                        _proper.Reload();
                        btnConnection.BackColor = Color.Green;
                        btnConnection.ForeColor = Color.White;
                        statusOnline = true;
                        //tmr_counting.Enabled = true;
                    }
                    else
                    {
                        btnConnection.Text = "OFFLINE";
                        _proper.connection = "OFFLINE";
                        _proper.Save();
                        _proper.Reload();
                        btnConnection.BackColor = Color.Red;
                        btnConnection.ForeColor = Color.White;
                        statusOnline = false;
                        //tmr_counting.Enabled = true;
                    }
                    
                }
                catch (WebException ex)
                {
                    btnConnection.Text = "OFFLINE";
                    _proper.connection = "OFFLINE";
                    _proper.Save();
                    _proper.Reload();
                    btnConnection.BackColor = Color.Red;
                    btnConnection.ForeColor = Color.White;
                    statusOnline = false;
                    //tmr_counting.Enabled = true;
                }
            }

            lblCheckConnection.Text = string.Format("Checking in : {0}s", countMax);
            countMax--;
        }

        private void FLogin_FormClosing(object sender, FormClosingEventArgs e)
        {
            Array.ForEach(Process.GetProcessesByName("LCR600SDK"), f => f.Kill());
        }

        private void txtUSername_TextChanged(object sender, EventArgs e)
        {
            if (txtUSername.Text.Length == 0)
            {
                getHistoryLogin();
                hideResults();
            }
            else
            {
                getHistoryLogin();
                if (!string.IsNullOrEmpty(txtUSername.Text))
                {                    
                    var itemsFound = listHistoryLogin.Items.Cast<ListItem>().Where(f => f.text.Contains(txtUSername.Text.Substring(1))).ToList();
                    if (itemsFound.Count > 0)
                    {
                        listHistoryLogin.DataSource = itemsFound;
                        listHistoryLogin.ValueMember = "value";
                        listHistoryLogin.DisplayMember = "text";
                        showResults();
                    }
                    else
                    {
                        hideResults();
                    }   
                }               
            }
        }

        private void txtUsername_Leave(object sender, EventArgs e)
        {
            hideResults();
        }

        private void listHistoryLogin_Click(object sender, EventArgs e)
        {
            txtUSername.Text = "p"+ ((ListItem)listHistoryLogin.SelectedItem).text;            
            hideResults();
        }

        

        private async void btnLoginDriver_Click(object sender, EventArgs e)
        {
            try
            {
                loginDriver();

                btnLoginDriver.Enabled = true;
                btnConnection.Enabled = true;
            }
            catch (Exception ex)
            {

            }
            
        }

        private void txtUsernameDriver_TextChanged(object sender, EventArgs e)
        {
            if (txtUsernameDriver.Text.Length == 0)
            {
                getHistoryLoginDriver();
                hideResultsDriver();
            }
            else
            {
                getHistoryLoginDriver();
                if (!string.IsNullOrEmpty(txtUsernameDriver.Text))
                {
                    var itemsFound = listHistoryLoginDriver.Items.Cast<ListItem>().Where(f => f.text.Contains(txtUsernameDriver.Text.Substring(1))).ToList();
                    if (itemsFound.Count > 0)
                    {
                        listHistoryLoginDriver.DataSource = itemsFound;
                        listHistoryLoginDriver.ValueMember = "value";
                        listHistoryLoginDriver.DisplayMember = "text";
                        showResultsDriver();
                    }
                    else
                    {
                        hideResultsDriver();
                    }
                }
            }
        }

        private void txtUsernameDriver_Click(object sender, EventArgs e)
        {
            try
            {                
                ProcessStartInfo psStartInfo = new ProcessStartInfo();
                var dir = Path.Combine(Environment.CurrentDirectory, "VKeyboard", "VKeyboard.exe");
                psStartInfo.FileName = dir;
                //psStartInfo.FileName = @"C:\Program Files\Common Files\microsoft shared\ink\TabTip.exe";
                

                Process ps = Process.Start(psStartInfo);

            }
            catch (Exception ex)
            {
                var error = ex.Message;
            }
        }

        private void listHistoryLoginDriver_Click(object sender, EventArgs e)
        {
            txtUsernameDriver.Text = "p" + ((ListItem)listHistoryLoginDriver.SelectedItem).text;
            hideResultsDriver();
        }

        private void txtPasswordDriver_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                loginDriver();
            }
        }

        private void txtUSername_Click(object sender, EventArgs e)
        {
            try
            {
                ProcessStartInfo psStartInfo = new ProcessStartInfo();
                //psStartInfo.FileName = @"C:\Program Files\Common Files\microsoft shared\ink\TabTip.exe";
                var dir = Path.Combine(Environment.CurrentDirectory, "VKeyboard", "VKeyboard.exe");
                psStartInfo.FileName = dir;

                Process ps = Process.Start(psStartInfo);

            }
            catch (Exception ex)
            {
                var error = ex.Message;
            }

            //SetFocus to your TextBox here

        }

        private void txtPassword_Click(object sender, EventArgs e)
        {
            try
            {
                ProcessStartInfo psStartInfo = new ProcessStartInfo();
                //psStartInfo.FileName = @"C:\Program Files\Common Files\microsoft shared\ink\TabTip.exe";
                var dir = Path.Combine(Environment.CurrentDirectory, "VKeyboard", "VKeyboard.exe");
                psStartInfo.FileName = dir;

                Process ps = Process.Start(psStartInfo);

            }
            catch (Exception ex)
            {
                var error = ex.Message;
            }
        }

        private void FLogin_Shown(object sender, EventArgs e)
        {

            try
            {
                log.WriteToFile("logLCR600.txt", "Prepare backup user.config file to origin");
                FullfilePath = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.PerUserRoamingAndLocal).FilePath;
                destination = Directory.GetParent(FullfilePath).ToString();
                backupUserConfigPath = Path.Combine(Environment.CurrentDirectory, "backupUserConfig");
                if (Directory.Exists(backupUserConfigPath))
                {
                    backupUserConfig = Path.Combine(Environment.CurrentDirectory, "backupUserConfig", Path.GetFileName(FullfilePath));
                    if (File.Exists(backupUserConfig))
                    {
                        var fileMap = new ExeConfigurationFileMap { ExeConfigFilename = backupUserConfig };
                        try
                        {
                            log.WriteToFile("logLCR600.txt", "Check file user.config last saved : ");
                            var configuration = ConfigurationManager.OpenMappedExeConfiguration(fileMap, ConfigurationUserLevel.None);
                        }
                        catch (Exception ex)
                        {
                            log.WriteToFile("logLCR600.txt", "Exception error user.config last saved : " + ex);
                            log.WriteToFile("logLCR600.txt", "Deleting user.config last saved");
                            File.Delete(backupUserConfig);
                            log.WriteToFile("logLCR600.txt", "using default user.config by system");
                        }

                        log.WriteToFile("logLCR600.txt", "using user.config from last saved");

                        if (File.Exists(FullfilePath))
                        {
                            File.Delete(FullfilePath);
                        }

                        log.WriteToFile("logLCR600.txt", "copy user.config from last saved to origin directoy");
                        try
                        {
                            if (Directory.Exists(destination))
                            {
                                File.Copy(backupUserConfig, FullfilePath);
                            }
                            else
                            {
                                log.WriteToFile("logLCR600.txt", "Directory origin not found!");
                            }
                        }
                        catch (Exception ex)
                        {
                            log.WriteToFile("logLCR600.txt", "Exception copy backup user.config to origin : " + ex);
                        }

                    }
                    else
                    {
                        log.WriteToFile("logLCR600.txt", "File not found!, using default user.config");
                    }

                    log.WriteToFile("logLCR600.txt", "Run Application LCR600SDK");
                }
                else
                {
                    log.WriteToFile("logLCR600.txt", "Directory not found!, using default user.config");
                }
            }
            catch (Exception ex)
            {
                log.WriteToFile("logLCR600.txt", "Exception prepare backup user.config file to origin : " + ex);
                log.WriteToFile("logLCR600.txt", "using default user.config by system");
            }

        }
    }
}
