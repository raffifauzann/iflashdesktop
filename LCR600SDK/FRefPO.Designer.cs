﻿
namespace LCR600SDK
{
    partial class FRefPO
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FRefPO));
            this.panel1 = new System.Windows.Forms.Panel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.pidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pidPoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.refTblDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.districtDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.poNoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.poQtyDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.receiveAtDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.transportirCodeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.transportirNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Action = new System.Windows.Forms.DataGridViewButtonColumn();
            this.poRitasiMaintankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.db_trainingDataSet = new LCR600SDK.db_trainingDataSet();
            this.btnSearch = new System.Windows.Forms.Button();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.poRitasiMaintankTableAdapter = new LCR600SDK.db_trainingDataSetTableAdapters.PoRitasiMaintankTableAdapter();
            this.button1 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.poRitasiMaintankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.db_trainingDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.dataGridView1);
            this.panel1.Location = new System.Drawing.Point(4, 43);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(655, 281);
            this.panel1.TabIndex = 0;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.pidDataGridViewTextBoxColumn,
            this.pidPoDataGridViewTextBoxColumn,
            this.refTblDataGridViewTextBoxColumn,
            this.districtDataGridViewTextBoxColumn,
            this.poNoDataGridViewTextBoxColumn,
            this.poQtyDataGridViewTextBoxColumn,
            this.receiveAtDataGridViewTextBoxColumn,
            this.transportirCodeDataGridViewTextBoxColumn,
            this.transportirNameDataGridViewTextBoxColumn,
            this.Action});
            this.dataGridView1.DataSource = this.poRitasiMaintankBindingSource;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridView1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(653, 279);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // pidDataGridViewTextBoxColumn
            // 
            this.pidDataGridViewTextBoxColumn.DataPropertyName = "Pid";
            this.pidDataGridViewTextBoxColumn.HeaderText = "Pid";
            this.pidDataGridViewTextBoxColumn.Name = "pidDataGridViewTextBoxColumn";
            this.pidDataGridViewTextBoxColumn.Visible = false;
            // 
            // pidPoDataGridViewTextBoxColumn
            // 
            this.pidPoDataGridViewTextBoxColumn.DataPropertyName = "PidPo";
            this.pidPoDataGridViewTextBoxColumn.HeaderText = "PidPo";
            this.pidPoDataGridViewTextBoxColumn.Name = "pidPoDataGridViewTextBoxColumn";
            this.pidPoDataGridViewTextBoxColumn.Visible = false;
            // 
            // refTblDataGridViewTextBoxColumn
            // 
            this.refTblDataGridViewTextBoxColumn.DataPropertyName = "RefTbl";
            this.refTblDataGridViewTextBoxColumn.HeaderText = "RefTbl";
            this.refTblDataGridViewTextBoxColumn.Name = "refTblDataGridViewTextBoxColumn";
            this.refTblDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.refTblDataGridViewTextBoxColumn.Visible = false;
            // 
            // districtDataGridViewTextBoxColumn
            // 
            this.districtDataGridViewTextBoxColumn.DataPropertyName = "District";
            this.districtDataGridViewTextBoxColumn.HeaderText = "District";
            this.districtDataGridViewTextBoxColumn.Name = "districtDataGridViewTextBoxColumn";
            this.districtDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // poNoDataGridViewTextBoxColumn
            // 
            this.poNoDataGridViewTextBoxColumn.DataPropertyName = "PoNo";
            this.poNoDataGridViewTextBoxColumn.HeaderText = "PoNo";
            this.poNoDataGridViewTextBoxColumn.Name = "poNoDataGridViewTextBoxColumn";
            this.poNoDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // poQtyDataGridViewTextBoxColumn
            // 
            this.poQtyDataGridViewTextBoxColumn.DataPropertyName = "PoQty";
            this.poQtyDataGridViewTextBoxColumn.HeaderText = "PoQty";
            this.poQtyDataGridViewTextBoxColumn.Name = "poQtyDataGridViewTextBoxColumn";
            this.poQtyDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // receiveAtDataGridViewTextBoxColumn
            // 
            this.receiveAtDataGridViewTextBoxColumn.DataPropertyName = "ReceiveAt";
            this.receiveAtDataGridViewTextBoxColumn.HeaderText = "ReceiveAt";
            this.receiveAtDataGridViewTextBoxColumn.Name = "receiveAtDataGridViewTextBoxColumn";
            this.receiveAtDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // transportirCodeDataGridViewTextBoxColumn
            // 
            this.transportirCodeDataGridViewTextBoxColumn.DataPropertyName = "TransportirCode";
            this.transportirCodeDataGridViewTextBoxColumn.HeaderText = "TransportirCode";
            this.transportirCodeDataGridViewTextBoxColumn.Name = "transportirCodeDataGridViewTextBoxColumn";
            this.transportirCodeDataGridViewTextBoxColumn.Visible = false;
            // 
            // transportirNameDataGridViewTextBoxColumn
            // 
            this.transportirNameDataGridViewTextBoxColumn.DataPropertyName = "TransportirName";
            this.transportirNameDataGridViewTextBoxColumn.HeaderText = "TransportirName";
            this.transportirNameDataGridViewTextBoxColumn.Name = "transportirNameDataGridViewTextBoxColumn";
            this.transportirNameDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Action
            // 
            this.Action.DataPropertyName = "Pilih";
            this.Action.HeaderText = "Action";
            this.Action.Name = "Action";
            this.Action.Text = "Pilih";
            this.Action.UseColumnTextForButtonValue = true;
            // 
            // poRitasiMaintankBindingSource
            // 
            this.poRitasiMaintankBindingSource.DataMember = "PoRitasiMaintank";
            this.poRitasiMaintankBindingSource.DataSource = this.db_trainingDataSet;
            // 
            // db_trainingDataSet
            // 
            this.db_trainingDataSet.DataSetName = "db_trainingDataSet";
            this.db_trainingDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.btnSearch.Location = new System.Drawing.Point(309, 2);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(97, 35);
            this.btnSearch.TabIndex = 22;
            this.btnSearch.Text = "Cari";
            this.btnSearch.UseVisualStyleBackColor = true;
            // 
            // txtSearch
            // 
            this.txtSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.txtSearch.Location = new System.Drawing.Point(5, 5);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(298, 35);
            this.txtSearch.TabIndex = 21;
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            // 
            // poRitasiMaintankTableAdapter
            // 
            this.poRitasiMaintankTableAdapter.ClearBeforeFill = true;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.BackColor = System.Drawing.Color.Red;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(562, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(97, 36);
            this.button1.TabIndex = 23;
            this.button1.Text = "Close";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // FRefPO
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(671, 347);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.txtSearch);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FRefPO";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Daftar PO";
            this.Load += new System.EventHandler(this.FRefPO_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.poRitasiMaintankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.db_trainingDataSet)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.DataGridView dataGridView1;
        private db_trainingDataSet db_trainingDataSet;
        private System.Windows.Forms.BindingSource poRitasiMaintankBindingSource;
        private db_trainingDataSetTableAdapters.PoRitasiMaintankTableAdapter poRitasiMaintankTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn pidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn pidPoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn refTblDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn districtDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn poNoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn poQtyDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn receiveAtDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn transportirCodeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn transportirNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewButtonColumn Action;
        private System.Windows.Forms.Button button1;
    }
}