﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LCR600SDK
{
    public class WriteLogText
    {
        public void WriteToFile(string name, string message)
        {
            string directory = Path.Combine(Environment.CurrentDirectory, "log_LCR600SDK");
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
            string filename = String.Format("{0:yyyy-MM-dd}__{1}", DateTime.Now, name);
            string path = Path.Combine(directory, filename);
            using (StreamWriter sw = File.AppendText(path))
            {
                string write = String.Format("{0:HH:mm:ss} : {1}", DateTime.Now, message);
                sw.WriteLine(write);
            }
        }
    }
}
