﻿namespace LCR600SDK
{
    partial class FTransProgress
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FTransProgress));
            this.btnStop = new System.Windows.Forms.Button();
            this.Litre = new System.Windows.Forms.Label();
            this.lblLitr = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.lblFLowrate = new System.Windows.Forms.Label();
            this.lblTotalizer = new System.Windows.Forms.Label();
            this.lblValve = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnStop
            // 
            this.btnStop.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStop.Location = new System.Drawing.Point(135, 161);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(75, 23);
            this.btnStop.TabIndex = 20;
            this.btnStop.Text = "STOP";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // Litre
            // 
            this.Litre.AutoSize = true;
            this.Litre.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Litre.Location = new System.Drawing.Point(61, 50);
            this.Litre.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.Litre.Name = "Litre";
            this.Litre.Size = new System.Drawing.Size(40, 20);
            this.Litre.TabIndex = 21;
            this.Litre.Text = "Litre";
            // 
            // lblLitr
            // 
            this.lblLitr.AutoSize = true;
            this.lblLitr.Font = new System.Drawing.Font("Microsoft Sans Serif", 50F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLitr.Location = new System.Drawing.Point(145, 31);
            this.lblLitr.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.lblLitr.Name = "lblLitr";
            this.lblLitr.Size = new System.Drawing.Size(69, 76);
            this.lblLitr.TabIndex = 22;
            this.lblLitr.Text = "0";
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // lblFLowrate
            // 
            this.lblFLowrate.AutoSize = true;
            this.lblFLowrate.Location = new System.Drawing.Point(12, 107);
            this.lblFLowrate.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.lblFLowrate.Name = "lblFLowrate";
            this.lblFLowrate.Size = new System.Drawing.Size(51, 13);
            this.lblFLowrate.TabIndex = 23;
            this.lblFLowrate.Text = "FLowrate";
            // 
            // lblTotalizer
            // 
            this.lblTotalizer.AutoSize = true;
            this.lblTotalizer.Location = new System.Drawing.Point(12, 124);
            this.lblTotalizer.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.lblTotalizer.Name = "lblTotalizer";
            this.lblTotalizer.Size = new System.Drawing.Size(47, 13);
            this.lblTotalizer.TabIndex = 24;
            this.lblTotalizer.Text = "Totalizer";
            // 
            // lblValve
            // 
            this.lblValve.AutoSize = true;
            this.lblValve.Location = new System.Drawing.Point(12, 140);
            this.lblValve.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.lblValve.Name = "lblValve";
            this.lblValve.Size = new System.Drawing.Size(34, 13);
            this.lblValve.TabIndex = 25;
            this.lblValve.Text = "Valve";
            // 
            // FTransProgress
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(336, 226);
            this.Controls.Add(this.lblValve);
            this.Controls.Add(this.lblTotalizer);
            this.Controls.Add(this.lblFLowrate);
            this.Controls.Add(this.lblLitr);
            this.Controls.Add(this.Litre);
            this.Controls.Add(this.btnStop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FTransProgress";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FTransProgress";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FTransProgress_FormClosing);
            this.Load += new System.EventHandler(this.FTransProgress_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Label Litre;
        private System.Windows.Forms.Label lblLitr;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label lblFLowrate;
        private System.Windows.Forms.Label lblTotalizer;
        private System.Windows.Forms.Label lblValve;
    }
}