﻿using System.Windows.Forms;

namespace LCR600SDK
{
    partial class FReceivedFTPort
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FReceivedFTPort));
            this.panel1 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.btnRefresh = new FontAwesome.Sharp.IconButton();
            this.btnSearch = new FontAwesome.Sharp.IconButton();
            this.button1 = new System.Windows.Forms.Button();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.numGoToPage = new System.Windows.Forms.NumericUpDown();
            this.button2 = new System.Windows.Forms.Button();
            this.lblPageOf = new System.Windows.Forms.Label();
            this.btnNext = new System.Windows.Forms.Button();
            this.txtPage = new System.Windows.Forms.TextBox();
            this.btnPrev = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Action = new System.Windows.Forms.DataGridViewButtonColumn();
            this.sendFtNoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sendDoNoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pidRitasiMaintankDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.transportirNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sendPoNoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sendQtyDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sendDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sendFromDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sendToDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sendDriverNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.districtDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DocStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.listDatumRitasiMaintankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.db_trainingDataSet = new LCR600SDK.db_trainingDataSet();
            this.listDatumRitasiMaintankTableAdapter = new LCR600SDK.db_trainingDataSetTableAdapters.ListDatumRitasiMaintankTableAdapter();
            this.tmrRefresh = new System.Windows.Forms.Timer(this.components);
            this.txtDoNumber = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numGoToPage)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.listDatumRitasiMaintankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.db_trainingDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(101)))), ((int)(((byte)(179)))));
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.txtDoNumber);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.btnRefresh);
            this.panel1.Controls.Add(this.btnSearch);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.txtSearch);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(974, 87);
            this.panel1.TabIndex = 24;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(46, 14);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(141, 29);
            this.label3.TabIndex = 100;
            this.label3.Text = "PO Number";
            // 
            // btnRefresh
            // 
            this.btnRefresh.BackColor = System.Drawing.Color.DodgerBlue;
            this.btnRefresh.FlatAppearance.BorderSize = 0;
            this.btnRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRefresh.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.btnRefresh.ForeColor = System.Drawing.Color.White;
            this.btnRefresh.IconChar = FontAwesome.Sharp.IconChar.None;
            this.btnRefresh.IconColor = System.Drawing.Color.Black;
            this.btnRefresh.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnRefresh.IconSize = 25;
            this.btnRefresh.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRefresh.Location = new System.Drawing.Point(731, 28);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(115, 36);
            this.btnRefresh.TabIndex = 25;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = false;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.BackColor = System.Drawing.Color.Orange;
            this.btnSearch.FlatAppearance.BorderSize = 0;
            this.btnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.btnSearch.IconChar = FontAwesome.Sharp.IconChar.None;
            this.btnSearch.IconColor = System.Drawing.Color.Black;
            this.btnSearch.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnSearch.IconSize = 25;
            this.btnSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSearch.Location = new System.Drawing.Point(610, 28);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(115, 36);
            this.btnSearch.TabIndex = 24;
            this.btnSearch.Text = "Cari";
            this.btnSearch.UseVisualStyleBackColor = false;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.BackColor = System.Drawing.Color.Red;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(867, 7);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(97, 35);
            this.button1.TabIndex = 23;
            this.button1.Text = "Close";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtSearch
            // 
            this.txtSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.txtSearch.Location = new System.Drawing.Point(12, 46);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(210, 35);
            this.txtSearch.TabIndex = 21;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(101)))), ((int)(((byte)(179)))));
            this.panel3.Controls.Add(this.numGoToPage);
            this.panel3.Controls.Add(this.button2);
            this.panel3.Controls.Add(this.lblPageOf);
            this.panel3.Controls.Add(this.btnNext);
            this.panel3.Controls.Add(this.txtPage);
            this.panel3.Controls.Add(this.btnPrev);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 403);
            this.panel3.Margin = new System.Windows.Forms.Padding(1);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(974, 42);
            this.panel3.TabIndex = 25;
            // 
            // numGoToPage
            // 
            this.numGoToPage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.numGoToPage.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.numGoToPage.Location = new System.Drawing.Point(758, 5);
            this.numGoToPage.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numGoToPage.Name = "numGoToPage";
            this.numGoToPage.Size = new System.Drawing.Size(71, 35);
            this.numGoToPage.TabIndex = 34;
            this.numGoToPage.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.BackColor = System.Drawing.Color.MidnightBlue;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(835, 5);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(136, 35);
            this.button2.TabIndex = 33;
            this.button2.Text = "Go To Page";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // lblPageOf
            // 
            this.lblPageOf.AutoSize = true;
            this.lblPageOf.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.lblPageOf.ForeColor = System.Drawing.Color.White;
            this.lblPageOf.Location = new System.Drawing.Point(167, 8);
            this.lblPageOf.Name = "lblPageOf";
            this.lblPageOf.Size = new System.Drawing.Size(70, 29);
            this.lblPageOf.TabIndex = 28;
            this.lblPageOf.Text = "Page";
            // 
            // btnNext
            // 
            this.btnNext.Enabled = false;
            this.btnNext.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.btnNext.Location = new System.Drawing.Point(119, 4);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(42, 34);
            this.btnNext.TabIndex = 27;
            this.btnNext.Text = ">";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // txtPage
            // 
            this.txtPage.Enabled = false;
            this.txtPage.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.txtPage.Location = new System.Drawing.Point(67, 4);
            this.txtPage.Name = "txtPage";
            this.txtPage.Size = new System.Drawing.Size(46, 35);
            this.txtPage.TabIndex = 26;
            this.txtPage.Text = "0";
            this.txtPage.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnPrev
            // 
            this.btnPrev.Enabled = false;
            this.btnPrev.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.btnPrev.Location = new System.Drawing.Point(19, 4);
            this.btnPrev.Name = "btnPrev";
            this.btnPrev.Size = new System.Drawing.Size(42, 34);
            this.btnPrev.TabIndex = 25;
            this.btnPrev.Text = "<";
            this.btnPrev.UseVisualStyleBackColor = true;
            this.btnPrev.Click += new System.EventHandler(this.btnPrev_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dataGridView1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 87);
            this.panel2.Margin = new System.Windows.Forms.Padding(1);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(974, 316);
            this.panel2.TabIndex = 26;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridView1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(101)))), ((int)(((byte)(179)))));
            this.dataGridView1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Action,
            this.sendFtNoDataGridViewTextBoxColumn,
            this.sendDoNoDataGridViewTextBoxColumn,
            this.pidRitasiMaintankDataGridViewTextBoxColumn,
            this.transportirNameDataGridViewTextBoxColumn,
            this.sendPoNoDataGridViewTextBoxColumn,
            this.sendQtyDataGridViewTextBoxColumn,
            this.sendDateDataGridViewTextBoxColumn,
            this.sendFromDataGridViewTextBoxColumn,
            this.sendToDataGridViewTextBoxColumn,
            this.sendDriverNameDataGridViewTextBoxColumn,
            this.districtDataGridViewTextBoxColumn,
            this.DocStatus});
            this.dataGridView1.DataSource = this.listDatumRitasiMaintankBindingSource;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.Size = new System.Drawing.Size(974, 316);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            this.dataGridView1.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridView1_CellFormatting);
            this.dataGridView1.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dataGridView1_DataBindingComplete);
            // 
            // Action
            // 
            this.Action.DefaultCellStyle = dataGridViewCellStyle2;
            this.Action.HeaderText = "Action";
            this.Action.MinimumWidth = 6;
            this.Action.Name = "Action";
            this.Action.Width = 85;
            // 
            // sendFtNoDataGridViewTextBoxColumn
            // 
            this.sendFtNoDataGridViewTextBoxColumn.DataPropertyName = "SendFtNo";
            this.sendFtNoDataGridViewTextBoxColumn.HeaderText = "SendFtNo";
            this.sendFtNoDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.sendFtNoDataGridViewTextBoxColumn.Name = "sendFtNoDataGridViewTextBoxColumn";
            this.sendFtNoDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.sendFtNoDataGridViewTextBoxColumn.Width = 129;
            // 
            // sendDoNoDataGridViewTextBoxColumn
            // 
            this.sendDoNoDataGridViewTextBoxColumn.DataPropertyName = "SendDoNo";
            this.sendDoNoDataGridViewTextBoxColumn.HeaderText = "SendDoNo";
            this.sendDoNoDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.sendDoNoDataGridViewTextBoxColumn.Name = "sendDoNoDataGridViewTextBoxColumn";
            this.sendDoNoDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.sendDoNoDataGridViewTextBoxColumn.Width = 139;
            // 
            // pidRitasiMaintankDataGridViewTextBoxColumn
            // 
            this.pidRitasiMaintankDataGridViewTextBoxColumn.DataPropertyName = "PidRitasiMaintank";
            this.pidRitasiMaintankDataGridViewTextBoxColumn.HeaderText = "PidRitasiMaintank";
            this.pidRitasiMaintankDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.pidRitasiMaintankDataGridViewTextBoxColumn.Name = "pidRitasiMaintankDataGridViewTextBoxColumn";
            this.pidRitasiMaintankDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.pidRitasiMaintankDataGridViewTextBoxColumn.Visible = false;
            this.pidRitasiMaintankDataGridViewTextBoxColumn.Width = 211;
            // 
            // transportirNameDataGridViewTextBoxColumn
            // 
            this.transportirNameDataGridViewTextBoxColumn.DataPropertyName = "TransportirName";
            this.transportirNameDataGridViewTextBoxColumn.HeaderText = "TransportirName";
            this.transportirNameDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.transportirNameDataGridViewTextBoxColumn.Name = "transportirNameDataGridViewTextBoxColumn";
            this.transportirNameDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.transportirNameDataGridViewTextBoxColumn.Width = 202;
            // 
            // sendPoNoDataGridViewTextBoxColumn
            // 
            this.sendPoNoDataGridViewTextBoxColumn.DataPropertyName = "SendPoNo";
            this.sendPoNoDataGridViewTextBoxColumn.HeaderText = "SendPoNo";
            this.sendPoNoDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.sendPoNoDataGridViewTextBoxColumn.Name = "sendPoNoDataGridViewTextBoxColumn";
            this.sendPoNoDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.sendPoNoDataGridViewTextBoxColumn.Width = 138;
            // 
            // sendQtyDataGridViewTextBoxColumn
            // 
            this.sendQtyDataGridViewTextBoxColumn.DataPropertyName = "SendQty";
            this.sendQtyDataGridViewTextBoxColumn.HeaderText = "SendQty";
            this.sendQtyDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.sendQtyDataGridViewTextBoxColumn.Name = "sendQtyDataGridViewTextBoxColumn";
            this.sendQtyDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.sendQtyDataGridViewTextBoxColumn.Width = 112;
            // 
            // sendDateDataGridViewTextBoxColumn
            // 
            this.sendDateDataGridViewTextBoxColumn.DataPropertyName = "SendDate";
            this.sendDateDataGridViewTextBoxColumn.HeaderText = "SendDate";
            this.sendDateDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.sendDateDataGridViewTextBoxColumn.Name = "sendDateDataGridViewTextBoxColumn";
            this.sendDateDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.sendDateDataGridViewTextBoxColumn.Width = 145;
            // 
            // sendFromDataGridViewTextBoxColumn
            // 
            this.sendFromDataGridViewTextBoxColumn.DataPropertyName = "SendFrom";
            this.sendFromDataGridViewTextBoxColumn.HeaderText = "SendFrom";
            this.sendFromDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.sendFromDataGridViewTextBoxColumn.Name = "sendFromDataGridViewTextBoxColumn";
            this.sendFromDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.sendFromDataGridViewTextBoxColumn.Width = 133;
            // 
            // sendToDataGridViewTextBoxColumn
            // 
            this.sendToDataGridViewTextBoxColumn.DataPropertyName = "SendTo";
            this.sendToDataGridViewTextBoxColumn.HeaderText = "SendTo";
            this.sendToDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.sendToDataGridViewTextBoxColumn.Name = "sendToDataGridViewTextBoxColumn";
            this.sendToDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.sendToDataGridViewTextBoxColumn.Width = 106;
            // 
            // sendDriverNameDataGridViewTextBoxColumn
            // 
            this.sendDriverNameDataGridViewTextBoxColumn.DataPropertyName = "SendDriverName";
            this.sendDriverNameDataGridViewTextBoxColumn.HeaderText = "SendDriverName";
            this.sendDriverNameDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.sendDriverNameDataGridViewTextBoxColumn.Name = "sendDriverNameDataGridViewTextBoxColumn";
            this.sendDriverNameDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.sendDriverNameDataGridViewTextBoxColumn.Width = 205;
            // 
            // districtDataGridViewTextBoxColumn
            // 
            this.districtDataGridViewTextBoxColumn.DataPropertyName = "District";
            this.districtDataGridViewTextBoxColumn.HeaderText = "District";
            this.districtDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.districtDataGridViewTextBoxColumn.Name = "districtDataGridViewTextBoxColumn";
            this.districtDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.districtDataGridViewTextBoxColumn.Width = 92;
            // 
            // DocStatus
            // 
            this.DocStatus.DataPropertyName = "DocDescription";
            this.DocStatus.HeaderText = "Doc Status";
            this.DocStatus.MinimumWidth = 6;
            this.DocStatus.Name = "DocStatus";
            this.DocStatus.Width = 153;
            // 
            // listDatumRitasiMaintankBindingSource
            // 
            this.listDatumRitasiMaintankBindingSource.DataMember = "ListDatumRitasiMaintank";
            this.listDatumRitasiMaintankBindingSource.DataSource = this.db_trainingDataSet;
            this.listDatumRitasiMaintankBindingSource.CurrentChanged += new System.EventHandler(this.listDatumRitasiMaintankBindingSource_CurrentChanged);
            // 
            // db_trainingDataSet
            // 
            this.db_trainingDataSet.DataSetName = "db_trainingDataSet";
            this.db_trainingDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // listDatumRitasiMaintankTableAdapter
            // 
            this.listDatumRitasiMaintankTableAdapter.ClearBeforeFill = true;
            // 
            // tmrRefresh
            // 
            this.tmrRefresh.Interval = 1800000;
            this.tmrRefresh.Tick += new System.EventHandler(this.tmrRefresh_Tick);
            // 
            // txtDoNumber
            // 
            this.txtDoNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.txtDoNumber.Location = new System.Drawing.Point(240, 46);
            this.txtDoNumber.Name = "txtDoNumber";
            this.txtDoNumber.Size = new System.Drawing.Size(341, 35);
            this.txtDoNumber.TabIndex = 101;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(338, 14);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(142, 29);
            this.label1.TabIndex = 102;
            this.label1.Text = "DO Number";
            // 
            // FReceivedFTPort
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(974, 445);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel3);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(1);
            this.Name = "FReceivedFTPort";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Received Fuel Truck Receive";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FReceivedFTPort_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numGoToPage)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.listDatumRitasiMaintankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.db_trainingDataSet)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private db_trainingDataSet db_trainingDataSet;
        private System.Windows.Forms.BindingSource listDatumRitasiMaintankBindingSource;
        private db_trainingDataSetTableAdapters.ListDatumRitasiMaintankTableAdapter listDatumRitasiMaintankTableAdapter;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label lblPageOf;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.TextBox txtPage;
        private System.Windows.Forms.Button btnPrev;
        private System.Windows.Forms.DataGridViewButtonColumn Action;
        private System.Windows.Forms.DataGridViewTextBoxColumn sendFtNoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sendDoNoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn pidRitasiMaintankDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn transportirNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sendPoNoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sendQtyDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sendDateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sendFromDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sendToDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sendDriverNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn districtDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn DocStatus;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtSearch;
        private FontAwesome.Sharp.IconButton btnSearch;
        private FontAwesome.Sharp.IconButton btnRefresh;
        private Timer tmrRefresh;
        private Label label3;
        private NumericUpDown numGoToPage;
        private Button button2;
        private Label label1;
        private TextBox txtDoNumber;
    }
}