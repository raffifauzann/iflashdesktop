﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LCR600SDK
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            WriteLogText log = new WriteLogText();
            try
            {
                if (PriorProcess() != null)
                {

                    MessageBox.Show("Aplikasi sudah terbuka! Mohon di tutup dahulu.");
                    return;
                }
                else
                {
                    log.WriteToFile("logLCR600.txt", "Checking user.config at AppData\\Local\\LCR600SDK");
                    var FullfilePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "LCR600SDK\\");
                    var directories = Directory.GetDirectories(FullfilePath);
                    if (!Directory.Exists(FullfilePath))
                    {
                        Directory.CreateDirectory(FullfilePath);
                    }
                    log.WriteToFile("logLCR600.txt", "user.config count : " + directories.Count());
                    log.WriteToFile("logLCR600.txt", "Deleting...");
                    foreach (var item in directories)
                    {
                        Directory.Delete(item, true);
                    }
                    log.WriteToFile("logLCR600.txt", "Delete Complete!");
                    Application.EnableVisualStyles();
                    Application.SetCompatibleTextRenderingDefault(false);
                    Application.Run(new FLogin());
                }
            }
            catch(Exception ex)
            {                
                log.WriteToFile("logLCR600.txt", "program.cs Exception : "+ex);                
            }            
        }
        static Process PriorProcess()       
        {
            Process curr = Process.GetCurrentProcess();
            Process[] procs = Process.GetProcessesByName(curr.ProcessName);
            foreach (Process p in procs)
            {
                if ((p.Id != curr.Id) &&
                    (p.MainModule.FileName == curr.MainModule.FileName))
                    return p;
            }
            return null;
        }
    }
}
