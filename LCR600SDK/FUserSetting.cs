﻿using MySql.Data.MySqlClient; using System.Data.SQLite; using System.IO; using System.Reflection;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LCR600SDK
{
    public partial class FUserSetting : Form
    {
        private string pUrl_service = string.Empty;
        public string toolConnection = Properties.Settings.Default.connection;
        public string auto_sync = Properties.Settings.Default.auto_sync_on_off;        
        public string whosueSelected = "";
        private string myConnectionString = string.Empty;
        private double pMeterFaktor = 1;



        public string menu_refueling_pitstop = Properties.Settings.Default.menu_refueling_pitstop;
        public string menu_refueling_ft = Properties.Settings.Default.menu_refueling_ft;
        public string menu_warehouse_transfer = Properties.Settings.Default.menu_warehouse_transfer;
        public string menu_warehouse_transfer_pengirim = Properties.Settings.Default.menu_warehouse_transfer_pengirim;
        public string menu_received_ft_port = Properties.Settings.Default.menu_received_ft_port;
        public string menu_received_ft_darat = Properties.Settings.Default.menu_received_ft_darat;
        public string menu_pengiriman_ft_port_pama = Properties.Settings.Default.menu_pengiriman_ft_port_pama;
        public string menu_list_data_issued = Properties.Settings.Default.menu_list_data_issued;
        public string menu_list_data_transfer = Properties.Settings.Default.menu_list_data_transfer;
        public string menu_receive_direct = Properties.Settings.Default.menu_receive_direct;
        public string menu_list_receive_direct = Properties.Settings.Default.menu_list_receive_direct;
        public string menu_list_data_pengiriman_ft_port_pama = Properties.Settings.Default.menu_list_data_pengiriman_ft_port_pama;
        public string menu_list_data_received_ft_port = Properties.Settings.Default.menu_list_data_received_ft_port;
        public string menu_list_data_received_ft_darat = Properties.Settings.Default.menu_list_data_received_ft_darat;
        public string menu_receive_direct_from_owner = Properties.Settings.Default.menu_receive_direct_from_owner;
        public string menu_pengiriman_pot = Properties.Settings.Default.menu_pengiriman_pot;
        public string menu_list_data_receive_direct_owner = Properties.Settings.Default.menu_list_data_receive_direct_owner;
        public string menu_list_data_pengiriman_pot = Properties.Settings.Default.menu_list_data_pengiriman_pot;
        public string interval_auto_sync = Properties.Settings.Default.interval_auto_sync;
        public string interval_check_online = Properties.Settings.Default.interval_check_online;

        Properties.Settings _proper = Properties.Settings.Default;
        PasswordEncrypDecrypt decryptor = new PasswordEncrypDecrypt();
        tbl_setting WHouseSetting1 = new tbl_setting();
        tbl_setting WHouseSetting2 = new tbl_setting();
        WriteLogText log = new WriteLogText();
        public FUserSetting()
        {
            _proper = Properties.Settings.Default;
            //myConnectionString = "Data Source=" + Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "SQLite", "db_iflash.db") + " ;New=False;";
            var passPhrase = "iFlashDesktop";
            var decryp = decryptor.DecryptPassword(_proper.db_pass, passPhrase);
            myConnectionString = "Data Source=" + Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "SQLite", "db_iflash.db") + " ;New=False;";
            InitializeComponent();
        }
        private async void FUserSetting_Load(object sender, EventArgs e)
        {
            try
            {
                #region warehouse 1
                var _globalData = GlobalModel.GlobalVar;
                var dstrctCode = _globalData.DataEmp.DstrctCode;
                SQLiteConnection conn = new SQLiteConnection(myConnectionString);
                getListWarehouse1(dstrctCode, conn);
                getListWarehouseName1(dstrctCode, conn);
                getLastSettingData1(conn);                
                comboBoxWarehouse2.Enabled = false;
                cbWarehouseName2.Enabled = false;
                comboBoxSerialNumberwh2.Enabled = false;
                dateTimeShift12.Enabled = false;
                dateTimeShift22.Enabled = false;
                btnSubmit2.Enabled = false;
                btnNewConfig2.Enabled = false;
                #endregion
                #region warehouse 2
                if (_proper.menu_receive_direct_from_owner == "True")
                {
                    comboBoxWarehouse2.Enabled = true;
                    cbWarehouseName2.Enabled = true;
                    comboBoxSerialNumberwh2.Enabled = true;
                    dateTimeShift12.Enabled = true;
                    dateTimeShift22.Enabled = true;
                    btnSubmit2.Enabled = true;
                    btnNewConfig2.Enabled = true;
                    log.WriteToFile("logLCR600.txt", "Get List Warehouse 2");
                    getListWarehouse2(dstrctCode, conn);
                    log.WriteToFile("logLCR600.txt", "Get List Warehouse Name 2");
                    getListWarehouseName2(dstrctCode, conn);
                    log.WriteToFile("logLCR600.txt", "Get Last Setting Whouse ke 2");
                    getLastSettingData2(conn);                    

                }
                #endregion
                #region user setting

                if (_proper.device_monitor == "10")
                {
                    pMenu10Inch.Show();
                    pMenu7Inch.Hide();                    
                }
                else if (_proper.device_monitor == "7")
                {
                    pMenu7Inch.Show();
                    pMenu10Inch.Hide();
                    this.AutoScroll = true;
                }
                pUrl_service = _proper.url_service;
                numDataPerKirimSync.Value = int.Parse(_proper.data_per_kirim);
                cbDfStatusType.SelectedIndex = cbDfStatusType.FindStringExact(_proper.status_type_default);
                numLCRWholeTenth.Value = int.Parse(_proper.lcr_whole_or_tenths);
                numCheckOnlineLogin.Value = int.Parse(interval_check_online) / 1000;
                numIntervalAutoSync.Value = int.Parse(interval_auto_sync) / 60000;
                //checkbox menu access
                if (_proper.menu_refueling_pitstop == "True")
                {
                    var idx = clb_menu_access.FindString("Refueling Pitstop");
                    clb_menu_access.SetItemChecked(idx, true);
                    btnMenu1.BackColor = Color.Green;
                    btn7InchMenu4.BackColor = Color.Green;                    
                }
                else
                {
                    btnMenu1.BackColor = Color.Red;
                    btn7InchMenu4.BackColor = Color.Red;
                    var idx = clb_menu_access.FindString("Refueling Pitstop");
                    clb_menu_access.SetItemChecked(idx, false);
                }

                if (_proper.menu_refueling_ft == "True")
                {
                    var idx = clb_menu_access.FindString("Refueling FT");
                    clb_menu_access.SetItemChecked(idx, true);
                    btnMenu2.BackColor = Color.Green;                    
                    btn7InchMenu2.BackColor = Color.Green;
                }
                else
                {
                    btnMenu2.BackColor = Color.Red;
                    btn7InchMenu2.BackColor = Color.Red;
                    var idx = clb_menu_access.FindString("Refueling FT");
                    clb_menu_access.SetItemChecked(idx, false);
                }

                if (_proper.menu_warehouse_transfer == "True")
                {
                    var idx = clb_menu_access.FindString("Warehouse Transfer");
                    clb_menu_access.SetItemChecked(idx, true);
                    btnMenu3.BackColor = Color.Green;
                    btn7InchMenu3.BackColor = Color.Green;
                }
                else
                {
                    btnMenu3.BackColor = Color.Red;
                    btn7InchMenu3.BackColor = Color.Red;
                    var idx = clb_menu_access.FindString("Warehouse Transfer");
                    clb_menu_access.SetItemChecked(idx, false);
                }

                if (_proper.menu_warehouse_transfer_pengirim == "True")
                {
                    var idx = clb_menu_access.FindString("Warehouse Transfer Pengirim");
                    clb_menu_access.SetItemChecked(idx, true);
                    btnMenu4.BackColor = Color.Green;
                }
                else
                {
                    btnMenu4.BackColor = Color.Red;
                    var idx = clb_menu_access.FindString("Warehouse Transfer Pengirim");
                    clb_menu_access.SetItemChecked(idx, false);
                }

                if (_proper.menu_received_ft_port == "True")
                {
                    var idx = clb_menu_access.FindString("Received FT Port");
                    clb_menu_access.SetItemChecked(idx, true);
                    btnMenu5.BackColor = Color.Green;
                }
                else
                {
                    btnMenu5.BackColor = Color.Red;
                    var idx = clb_menu_access.FindString("Received FT Port");
                    clb_menu_access.SetItemChecked(idx, false);
                }

                if (_proper.menu_received_ft_darat == "True")
                {
                    var idx = clb_menu_access.FindString("Received FT Darat");
                    clb_menu_access.SetItemChecked(idx, true);
                    btnMenu6.BackColor = Color.Green;
                }
                else
                {
                    btnMenu6.BackColor = Color.Red;
                    var idx = clb_menu_access.FindString("Received FT Darat");
                    clb_menu_access.SetItemChecked(idx, false);
                }

                if (_proper.menu_pengiriman_ft_port_pama == "True")
                {
                    var idx = clb_menu_access.FindString("Pengiriman FT Port PAMA");
                    clb_menu_access.SetItemChecked(idx, true);
                    btnMenu7.BackColor = Color.Green;
                }
                else
                {
                    btnMenu7.BackColor = Color.Red;
                    var idx = clb_menu_access.FindString("Pengiriman FT Port PAMA");
                    clb_menu_access.SetItemChecked(idx, false);
                }

                if (_proper.menu_list_data_issued == "True")
                {
                    var idx = clb_menu_access.FindString("List Data Issued");
                    clb_menu_access.SetItemChecked(idx, true);
                    btnMenu8.BackColor = Color.Green;
                    btn7InchMenu8.BackColor = Color.Green;                    
                }
                else
                {
                    btnMenu8.BackColor = Color.Red;
                    btn7InchMenu8.BackColor = Color.Red;
                    var idx = clb_menu_access.FindString("List Data Issued");
                    clb_menu_access.SetItemChecked(idx, false);
                }

                if (_proper.menu_list_data_transfer == "True")
                {
                    var idx = clb_menu_access.FindString("List Data Transfer");
                    clb_menu_access.SetItemChecked(idx, true);
                    btnMenu9.BackColor = Color.Green;
                    btn7InchMenu9.BackColor = Color.Green;
                }
                else
                {
                    btnMenu9.BackColor = Color.Red;
                    btn7InchMenu9.BackColor = Color.Red;
                    var idx = clb_menu_access.FindString("List Data Transfer");
                    clb_menu_access.SetItemChecked(idx, false);
                }

                if (_proper.menu_receive_direct == "True")
                {
                    var idx = clb_menu_access.FindString("Received Direct");
                    clb_menu_access.SetItemChecked(idx, true);
                    btnMenu10.BackColor = Color.Green;                    
                }
                else
                {
                    btnMenu10.BackColor = Color.Red;                    
                    var idx = clb_menu_access.FindString("Received Direct");
                    clb_menu_access.SetItemChecked(idx, false);
                }

                if (_proper.menu_list_receive_direct == "True")
                {
                    var idx = clb_menu_access.FindString("List Data Receive Direct");
                    clb_menu_access.SetItemChecked(idx, true);
                    btnMenu11.BackColor = Color.Green;
                }
                else
                {
                    btnMenu11.BackColor = Color.Red;
                    var idx = clb_menu_access.FindString("List Data Receive Direct");
                    clb_menu_access.SetItemChecked(idx, false);
                }


                if (_proper.menu_list_data_pengiriman_ft_port_pama == "True")
                {
                    var idx = clb_menu_access.FindString("List Data Pengiriman FT Port PAMA");
                    clb_menu_access.SetItemChecked(idx, true);
                    btnMenu12.BackColor = Color.Green;
                }
                else
                {
                    btnMenu12.BackColor = Color.Red;
                    var idx = clb_menu_access.FindString("List Data Pengiriman FT Port PAMA");
                    clb_menu_access.SetItemChecked(idx, false);
                }
                if (_proper.menu_list_data_received_ft_port == "True")
                {
                    var idx = clb_menu_access.FindString("List Data Received FT Port");
                    clb_menu_access.SetItemChecked(idx, true);
                    btnMenu13.BackColor = Color.Green;
                }
                else
                {
                    btnMenu13.BackColor = Color.Red;
                    var idx = clb_menu_access.FindString("List Data Received FT Port");
                    clb_menu_access.SetItemChecked(idx, false);
                }
                if (_proper.menu_list_data_received_ft_darat == "True")
                {
                    var idx = clb_menu_access.FindString("List Data Received FT Darat");
                    clb_menu_access.SetItemChecked(idx, true);
                    btnMenu14.BackColor = Color.Green;
                }
                else
                {
                    btnMenu14.BackColor = Color.Red;
                    var idx = clb_menu_access.FindString("List Data Received FT Darat");
                    clb_menu_access.SetItemChecked(idx, false);
                }
                if (_proper.menu_receive_direct_from_owner == "True")
                {
                    var idx = clb_menu_access.FindString("Receive Direct Owner");
                    clb_menu_access.SetItemChecked(idx, true);
                    btnMenu15.BackColor = Color.Green;
                }
                else
                {
                    btnMenu15.BackColor = Color.Red;
                    var idx = clb_menu_access.FindString("Receive Direct Owner");
                    clb_menu_access.SetItemChecked(idx, false);
                }

                if (_proper.menu_pengiriman_pot == "True")
                {
                    var idx = clb_menu_access.FindString("Pengiriman POT");
                    clb_menu_access.SetItemChecked(idx, true);
                    btnMenu16.BackColor = Color.Green;
                }
                else
                {
                    btnMenu16.BackColor = Color.Red;
                    var idx = clb_menu_access.FindString("Pengiriman POT");
                    clb_menu_access.SetItemChecked(idx, false);
                }

                if (_proper.menu_list_data_receive_direct_owner == "True")
                {
                    var idx = clb_menu_access.FindString("List Data Receive Direct Owner");
                    clb_menu_access.SetItemChecked(idx, true);
                    btnMenu17.BackColor = Color.Green;
                }
                else
                {
                    btnMenu17.BackColor = Color.Red;
                    var idx = clb_menu_access.FindString("List Data Receive Direct Owner");
                    clb_menu_access.SetItemChecked(idx, false);
                }

                if (_proper.menu_list_data_pengiriman_pot == "True")
                {
                    var idx = clb_menu_access.FindString("List Data Pengiriman POT");
                    clb_menu_access.SetItemChecked(idx, true);
                    btnMenu18.BackColor = Color.Green;
                }
                else
                {
                    btnMenu18.BackColor = Color.Red;
                    var idx = clb_menu_access.FindString("List Data Pengiriman POT");
                    clb_menu_access.SetItemChecked(idx, false);
                }

                var checkconnect = await checkConnectivity();
                if (checkconnect.Contains("Supply Service"))
                //if (_proper.connection == "ONLINE")
                {
                    
                    btnConnection.Text = _proper.connection;
                    btnConnection.BackColor = Color.Green;
                    btnConnection.ForeColor = Color.White;
                }
                else
                {
                    if (_proper.connection == "ONLINE") {
                        btnConnection.Text = "OFFLINE";
                    }
                    else
                    {
                        btnConnection.Text = _proper.connection;
                    }                    
                    btnConnection.BackColor = Color.Red;
                    btnConnection.ForeColor = Color.White;
                }
                if (_proper.auto_sync_on_off == "ON")
                {
                    btnAutoSyncOnOff.Text = _proper.auto_sync_on_off;
                    btnAutoSyncOnOff.BackColor = Color.Green;
                    btnAutoSyncOnOff.ForeColor = Color.White;
                }
                else
                {
                    btnAutoSyncOnOff.Text = _proper.auto_sync_on_off;
                    btnAutoSyncOnOff.BackColor = Color.Red;
                    btnAutoSyncOnOff.ForeColor = Color.White;
                }

                #endregion
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
                MessageBox.Show("FUserSetting.cs Exception: Terjadi kesalahan saat load form, mohon cek log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", "FUserSetting.cs Exception FUserSetting_Load : " + ex.Message);
                save_error_log(ex.Message, "FUserSetting_Load()");
            }



        }

        void save_error_log(string data, string actions)
        {
            try
            {
                DateTime localDate = DateTime.Now;
                var _item = new tbl_log();
                var initData = GlobalModel.GlobalVar;

                _item.pid = System.Guid.NewGuid().ToString();
                _item.actions = $"District {initData.DataEmp.DstrctCode}/FUserSetting/{actions}";
                _item.message = $"{data}";
                _item.mod_date = localDate;
                _item.mod_by = initData.DataEmp.Nrp;

                SQLiteConnection conn;

                conn = new SQLiteConnection(myConnectionString);
                if (conn.State != ConnectionState.Open) conn.Open();
                string query = string.Empty;
                query = "INSERT INTO tbl_log(pid,actions,message,mod_date,mod_by)"
                      + " VALUES(@pid,@actions,@message,@mod_date,@mod_by)";

                SQLiteCommand cmd = new SQLiteCommand(query, conn);

                cmd.Parameters.Add(new SQLiteParameter("@pid", _item.pid));
                cmd.Parameters.Add(new SQLiteParameter("@actions", _item.actions));
                cmd.Parameters.Add(new SQLiteParameter("@message", _item.message));
                cmd.Parameters.Add(new SQLiteParameter("@mod_date", _item.mod_date));
                cmd.Parameters.Add(new SQLiteParameter("@mod_by", _item.mod_by));
                cmd.ExecuteNonQuery();
                if (conn.State == ConnectionState.Open) conn.Close();
            }
            catch (Exception)
            {
            }
        }


        #region user setting
        private void btnSaveUserSetting_Click(object sender, EventArgs e)
        {
            try
            {
                var interval_auto_sync = (int.Parse(numIntervalAutoSync.Value.ToString()) * 60000);
                Properties.Settings.Default.status_type_default = cbDfStatusType.Text;
                Properties.Settings.Default.auto_sync_on_off = btnAutoSyncOnOff.Text;
                Properties.Settings.Default.data_per_kirim = numDataPerKirimSync.Value.ToString();
                Properties.Settings.Default.menu_refueling_pitstop = clb_menu_access.CheckedItems.IndexOf("Refueling Pitstop") != -1 ? "True" : "False";
                Properties.Settings.Default.menu_refueling_ft = clb_menu_access.CheckedItems.IndexOf("Refueling FT") != -1 ? "True" : "False";
                Properties.Settings.Default.menu_warehouse_transfer = clb_menu_access.CheckedItems.IndexOf("Warehouse Transfer") != -1 ? "True" : "False";
                Properties.Settings.Default.menu_warehouse_transfer_pengirim = clb_menu_access.CheckedItems.IndexOf("Warehouse Transfer Pengirim") != -1 ? "True" : "False";
                Properties.Settings.Default.menu_received_ft_port = clb_menu_access.CheckedItems.IndexOf("Received FT Port") != -1 ? "True" : "False";
                Properties.Settings.Default.menu_received_ft_darat = clb_menu_access.CheckedItems.IndexOf("Received FT Darat") != -1 ? "True" : "False";
                Properties.Settings.Default.menu_pengiriman_ft_port_pama = clb_menu_access.CheckedItems.IndexOf("Pengiriman FT Port PAMA") != -1 ? "True" : "False";
                Properties.Settings.Default.menu_list_data_issued = clb_menu_access.CheckedItems.IndexOf("List Data Issued") != -1 ? "True" : "False";
                Properties.Settings.Default.menu_list_data_transfer = clb_menu_access.CheckedItems.IndexOf("List Data Transfer") != -1 ? "True" : "False";
                Properties.Settings.Default.menu_receive_direct = clb_menu_access.CheckedItems.IndexOf("Received Direct") != -1 ? "True" : "False";
                Properties.Settings.Default.menu_list_receive_direct = clb_menu_access.CheckedItems.IndexOf("List Data Receive Direct") != -1 ? "True" : "False";
                Properties.Settings.Default.menu_list_data_pengiriman_ft_port_pama = clb_menu_access.CheckedItems.IndexOf("List Data Pengiriman FT Port PAMA") != -1 ? "True" : "False";
                Properties.Settings.Default.menu_list_data_received_ft_port = clb_menu_access.CheckedItems.IndexOf("List Data Received FT Port") != -1 ? "True" : "False";
                Properties.Settings.Default.menu_list_data_received_ft_darat = clb_menu_access.CheckedItems.IndexOf("List Data Received FT Darat") != -1 ? "True" : "False";
                Properties.Settings.Default.menu_receive_direct_from_owner = clb_menu_access.CheckedItems.IndexOf("Receive Direct Owner") != -1 ? "True" : "False";
                Properties.Settings.Default.menu_pengiriman_pot = clb_menu_access.CheckedItems.IndexOf("Pengiriman POT") != -1 ? "True" : "False";
                Properties.Settings.Default.interval_auto_sync = interval_auto_sync.ToString();
                var cek = clb_menu_access.CheckedItems.IndexOf("List Data Receive Direct Owner") != -1 ? "True" : "False";
                Properties.Settings.Default.menu_list_data_receive_direct_owner = clb_menu_access.CheckedItems.IndexOf("List Data Receive Direct Owner") != -1 ? "True" : "False";
                var cek2 = clb_menu_access.CheckedItems.IndexOf("List Data Pengiriman POT") != -1 ? "True" : "False";
                Properties.Settings.Default.menu_list_data_pengiriman_pot = clb_menu_access.CheckedItems.IndexOf("List Data Pengiriman POT") != -1 ? "True" : "False";
                

                Properties.Settings.Default.lcr_whole_or_tenths = numLCRWholeTenth.Value.ToString();
                Properties.Settings.Default.Save();
                Properties.Settings.Default.Reload();

                string copyUseConfig = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.PerUserRoamingAndLocal).FilePath;
                string destinatonFolder = Path.Combine(Environment.CurrentDirectory, "backupUserConfig");
                var fullpath = Path.Combine(destinatonFolder, Path.GetFileName(copyUseConfig));

                if (!Directory.Exists(destinatonFolder))
                {
                    Directory.CreateDirectory(destinatonFolder);
                }
                if(File.Exists(fullpath))
                {
                    File.Delete(fullpath);
                }

                File.Copy(copyUseConfig, fullpath);

                auto_sync = btnAutoSyncOnOff.Text;
                toolConnection = btnConnection.Text;
                menu_refueling_pitstop = clb_menu_access.CheckedItems.IndexOf("Refueling Pitstop") != -1 ? "True" : "False";
                menu_refueling_ft = clb_menu_access.CheckedItems.IndexOf("Refueling FT") != -1 ? "True" : "False";
                menu_warehouse_transfer = clb_menu_access.CheckedItems.IndexOf("Warehouse Transfer") != -1 ? "True" : "False";
                menu_warehouse_transfer_pengirim = clb_menu_access.CheckedItems.IndexOf("Warehouse Transfer Pengirim") != -1 ? "True" : "False";
                menu_received_ft_port = clb_menu_access.CheckedItems.IndexOf("Received FT Port") != -1 ? "True" : "False";
                menu_received_ft_darat = clb_menu_access.CheckedItems.IndexOf("Received FT Darat") != -1 ? "True" : "False";
                menu_pengiriman_ft_port_pama = clb_menu_access.CheckedItems.IndexOf("Pengiriman FT Port PAMA") != -1 ? "True" : "False";
                menu_list_data_issued = clb_menu_access.CheckedItems.IndexOf("List Data Issued") != -1 ? "True" : "False";
                menu_list_data_transfer = clb_menu_access.CheckedItems.IndexOf("List Data Transfer") != -1 ? "True" : "False";
                menu_receive_direct = clb_menu_access.CheckedItems.IndexOf("Received Direct") != -1 ? "True" : "False";
                menu_list_receive_direct = clb_menu_access.CheckedItems.IndexOf("List Data Receive Direct") != -1 ? "True" : "False";
                menu_list_data_pengiriman_ft_port_pama = clb_menu_access.CheckedItems.IndexOf("List Data Pengiriman FT Port PAMA") != -1 ? "True" : "False";
                menu_list_data_received_ft_port = clb_menu_access.CheckedItems.IndexOf("List Data Received FT Port") != -1 ? "True" : "False";
                menu_list_data_received_ft_darat = clb_menu_access.CheckedItems.IndexOf("List Data Received FT Darat") != -1 ? "True" : "False";
                menu_receive_direct_from_owner = clb_menu_access.CheckedItems.IndexOf("Receive Direct Owner") != -1 ? "True" : "False";
                menu_pengiriman_pot = clb_menu_access.CheckedItems.IndexOf("Pengiriman POT") != -1 ? "True" : "False";
                menu_list_data_receive_direct_owner = clb_menu_access.CheckedItems.IndexOf("List Data Receive Direct Owner") != -1 ? "True" : "False";
                menu_list_data_pengiriman_pot = clb_menu_access.CheckedItems.IndexOf("List Data Pengiriman POT") != -1 ? "True" : "False";
                interval_auto_sync = interval_auto_sync;
                MessageBox.Show("Berhasil Disimpan!", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
                MessageBox.Show("FUserSetting.cs Exception: Terjadi kesalahan saat simpan data, mohon cek log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", "FUserSetting.cs Exception btnSaveUserSetting_Click : " + ex.Message);
                save_error_log(ex.Message, "btnSaveUserSetting_Click()");
            }


        }
        public async Task<string> checkConnectivity()
        {
            try
            {
                WebClient requestCek = new WebClient();
                var str = requestCek.DownloadString(pUrl_service);
                return str;
            }
            catch (WebException ex)
            {
                return "";
            }
        }
        private async void btnConnection_Click(object sender, EventArgs e)
        {
            if (btnConnection.Text == "ONLINE")
            {
                btnConnection.Text = "OFFLINE";                
                btnConnection.BackColor = Color.Red;
                btnConnection.ForeColor = Color.White;
            }
            else
            {
                btnConnection.Text = "CHECKING";
                btnConnection.BackColor = Color.Yellow;
                btnConnection.ForeColor = Color.Black;
                try
                {
                    var checkconnect = await checkConnectivity();
                    if (checkconnect.Contains("Supply Service"))
                    {
                        btnConnection.Text = "ONLINE";
                        btnConnection.BackColor = Color.Green;
                        btnConnection.ForeColor = Color.White;
                    }
                    else
                    {
                        btnConnection.Text = "OFFLINE";
                        btnConnection.BackColor = Color.Red;
                        btnConnection.ForeColor = Color.White;
                    }
                    
                }
                catch (WebException ex)
                {
                    MessageBox.Show("FUserSetting.cs Exception: Terjadi kesalahan saat cek koneksi, mohon cek log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    btnConnection.Text = "OFFLINE";                    
                    btnConnection.BackColor = Color.Red;
                    btnConnection.ForeColor = Color.White;                    
                    log.WriteToFile("logLCR600.txt", "FUserSetting.cs Exception btnConnection_Click : " + ex);
                    save_error_log(ex.Message, "btnConnection_Click()");
                }
            }
        }

        private void btnAutoSyncOnOff_Click(object sender, EventArgs e)
        {
            if (btnAutoSyncOnOff.Text == "ON")
            {
                btnAutoSyncOnOff.Text = "OFF";
                btnAutoSyncOnOff.BackColor = Color.Red;
                btnAutoSyncOnOff.ForeColor = Color.White;
            }
            else
            {
                btnAutoSyncOnOff.Text = "ON";
                btnAutoSyncOnOff.BackColor = Color.Green;
                btnAutoSyncOnOff.ForeColor = Color.White;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Hide();
        }
        #endregion
        #region warehouse
        void getListWarehouse1(string dstrctCode, SQLiteConnection conn)
        {
            conn = new SQLiteConnection(myConnectionString);
            if (conn.State != ConnectionState.Open) conn.Open();
            string query = string.Empty;
            query = "SELECT DISTINCT Warehouse FROM flowmeter where DstrctCode = '" + dstrctCode + "'";
            SQLiteCommand cmd = new SQLiteCommand(query, conn);
            SQLiteDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                comboBoxWarehouse.Items.Add(reader["Warehouse"]);
                //loadWhouse();
            }
            reader.Dispose();
            if (conn.State == ConnectionState.Open) conn.Close();
        }
        void getListWarehouseName1(string dstrctCode, SQLiteConnection conn)
        {
            conn = new SQLiteConnection(myConnectionString);
            if (conn.State != ConnectionState.Open) conn.Open();
            string query = string.Empty;
            query = "SELECT DISTINCT WhouseLocation FROM flowmeter where DstrctCode = '" + dstrctCode + "'";
            SQLiteCommand cmd = new SQLiteCommand(query, conn);
            SQLiteDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                cbWarehouseName.Items.Add(reader["WhouseLocation"]);
                //loadWhouse();
            }
            reader.Dispose();
            if (conn.State == ConnectionState.Open) conn.Close();
        }
        void getLastSettingData1(SQLiteConnection conn)
        {


            if (conn.State != ConnectionState.Open) conn.Open();
            string query = string.Empty;
            query = "SELECT * FROM tbl_setting where Whouseke = 1 ORDER BY Mod_date";
            SQLiteCommand cmd = new SQLiteCommand(query, conn);
            SQLiteDataReader reader = cmd.ExecuteReader();
            
            while (reader.Read())
            {                
                WHouseSetting1.startshift1 = reader["startShift1"].ToString();
                WHouseSetting1.startshift2 = reader["startShift2"].ToString();
                WHouseSetting1.whouse = reader["Whouseid"].ToString();
                WHouseSetting1.whousename = reader["WhouseName"].ToString();
                WHouseSetting1.sncode = reader["SnCode"].ToString();
                
                comboBoxWarehouse.SelectedIndex = comboBoxWarehouse.FindStringExact(WHouseSetting1.whouse);
                cbWarehouseName.SelectedIndex = cbWarehouseName.FindStringExact(WHouseSetting1.whousename);
                dateTimeShift1.Text = string.IsNullOrEmpty(reader["startShift1"].ToString())? "00:00": reader["startShift1"].ToString();
                dateTimeShift2.Text = string.IsNullOrEmpty(reader["startShift2"].ToString()) ? "00:00" : reader["startShift2"].ToString();
                comboBoxSerialNumber.SelectedIndex = comboBoxSerialNumber.FindStringExact(WHouseSetting1.sncode);                
                
                //loadWhouse();
            }
            reader.Dispose();
            if (conn.State == ConnectionState.Open) conn.Close();
        }

        void getListWarehouse2(string dstrctCode, SQLiteConnection conn)
        {
            conn = new SQLiteConnection(myConnectionString);
            if (conn.State != ConnectionState.Open) conn.Open();
            string query = string.Empty;
            query = "SELECT DISTINCT Warehouse FROM flowmeter where DstrctCode = '" + dstrctCode + "'";
            SQLiteCommand cmd = new SQLiteCommand(query, conn);
            SQLiteDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                comboBoxWarehouse2.Items.Add(reader["Warehouse"]);
                //loadWhouse();
            }
            
            log.WriteToFile("logLCR600.txt", "Get List Warehouse Name 2 total => "+ comboBoxWarehouse2.Items.Count);
            reader.Dispose();
            if (conn.State == ConnectionState.Open) conn.Close();
        }
        void getListWarehouseName2(string dstrctCode, SQLiteConnection conn)
        {
            conn = new SQLiteConnection(myConnectionString);
            if (conn.State != ConnectionState.Open) conn.Open();
            string query = string.Empty;
            query = "SELECT DISTINCT WhouseLocation FROM flowmeter where DstrctCode = '" + dstrctCode + "'";
            SQLiteCommand cmd = new SQLiteCommand(query, conn);
            SQLiteDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                cbWarehouseName2.Items.Add(reader["WhouseLocation"]);
                //loadWhouse();
            }
            log.WriteToFile("logLCR600.txt", "Get List Warehouse Name 2 total =>"+ cbWarehouseName2.Items.Count);
            reader.Dispose();
            if (conn.State == ConnectionState.Open) conn.Close();
        }
        void getLastSettingData2(SQLiteConnection conn)
        {


            if (conn.State != ConnectionState.Open) conn.Open();
            string query = string.Empty;
            query = "SELECT * FROM tbl_setting where Whouseke = 2 ORDER BY Mod_date";
            SQLiteCommand cmd = new SQLiteCommand(query, conn);
            SQLiteDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                WHouseSetting2.startshift1 = reader["startShift1"].ToString();
                WHouseSetting2.startshift2 = reader["startShift2"].ToString();
                WHouseSetting2.whouse = reader["Whouseid"].ToString();
                WHouseSetting2.whousename = reader["WhouseName"].ToString();
                WHouseSetting2.sncode = reader["SnCode"].ToString();
                              
                comboBoxWarehouse2.SelectedIndex = comboBoxWarehouse2.FindStringExact(reader["Whouseid"].ToString());
                log.WriteToFile("logLCR600.txt", "Get Warehouse  2 =>" + comboBoxWarehouse2.Text);
                cbWarehouseName2.SelectedIndex = cbWarehouseName2.FindStringExact(reader["WhouseName"].ToString());
                log.WriteToFile("logLCR600.txt", "Get Warehouse Name 2 =>" + cbWarehouseName2.Text);
                dateTimeShift12.Text = string.IsNullOrEmpty(reader["startShift1"].ToString()) ? "00:00" : reader["startShift1"].ToString();
                dateTimeShift22.Text = string.IsNullOrEmpty(reader["startShift2"].ToString()) ? "00:00" : reader["startShift2"].ToString();
                comboBoxSerialNumberwh2.SelectedIndex = comboBoxSerialNumberwh2.FindStringExact(reader["SnCode"].ToString());
                log.WriteToFile("logLCR600.txt", "Get SN Code 2 =>" + comboBoxSerialNumberwh2.Text);
                //loadWhouse();
            }
            reader.Dispose();
            if (conn.State == ConnectionState.Open) conn.Close();
        }
        void loadWhouse()
        {
            try
            {
                string whouses = "";
                string whousesname = "";
                string sncodes = "";
                SQLiteConnection conn;

                conn = new SQLiteConnection(myConnectionString);
                if (conn.State != ConnectionState.Open) conn.Open();
                string query = string.Empty;

                query = "SELECT * FROM tbl_setting";
                SQLiteCommand cmds = new SQLiteCommand(query, conn);
                SQLiteDataReader reader = cmds.ExecuteReader();

                while (reader.Read())
                {
                    whouses = reader["Whouseid"].ToString();
                    whousesname = reader["WhouseName"].ToString();
                    sncodes = reader["SnCode"].ToString();
                    comboBoxWarehouse.SelectedItem = whouses;
                    comboBoxSerialNumber.SelectedItem = sncodes;
                    cbWarehouseName.SelectedItem = whousesname;
                }
                reader.Dispose();
                if (conn.State == ConnectionState.Open) conn.Close();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
                MessageBox.Show("FUserSetting.cs Exception: Terjadi kesalahan saat get data warehouse, mohon cek log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", "FUserSetting.cs Exception loadWhouse : " + ex);
                save_error_log(ex.Message, "loadWhouse()");
            }

        }
        private void btnNewConfig_Click(object sender, EventArgs e)
        {
            comboBoxWarehouse.Enabled = true;
            comboBoxSerialNumber.Enabled = true;
            dateTimeShift1.Enabled = true;
            dateTimeShift2.Enabled = true;
            btnSubmit.Visible = true;
            btnNewConfig.Visible = false;
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (isValid())
                {
                    var _item = new tbl_setting();
                    var initData = GlobalModel.GlobalVar;
                    DateTime localDate = DateTime.Now;
                    int recs = 0;

                    string selectedItemWhouse = this.comboBoxWarehouse.GetItemText(this.comboBoxWarehouse.SelectedItem);
                    string selectedItemWhouseName = this.cbWarehouseName.GetItemText(this.cbWarehouseName.SelectedItem);
                    string selectedItemSerialNumber = this.comboBoxSerialNumber.GetItemText(this.comboBoxSerialNumber.SelectedItem);


                    _item.pid = System.Guid.NewGuid().ToString();
                    _item.whouse = selectedItemWhouse.ToString();
                    _item.whousename = selectedItemWhouseName.ToString();
                    _item.sncode = selectedItemSerialNumber.ToString();
                    _item.startshift1 = dateTimeShift1.Value.ToString("HH:mm") + ":00";
                    _item.startshift2 = dateTimeShift2.Value.ToString("HH:mm") + ":00";
                    _item.meterFakor = pMeterFaktor;
                    _item.mod_date = localDate.ToString();
                    _item.mod_by = initData.DataEmp.Nrp;

                    SQLiteConnection conn;

                    conn = new SQLiteConnection(myConnectionString);
                    if (conn.State != ConnectionState.Open) conn.Open();
                    string query = string.Empty;

                    SQLiteCommand cmds = new SQLiteCommand(query, conn);
                    query = "DELETE FROM tbl_setting where Whouseke = 1";
                    execCmd(query, conn);
                    query = "INSERT INTO tbl_setting(pid,whouseid,whousename,sncode,meterFakor,startshift1,startshift2,mod_date,mod_by,Whouseke)"
                        + " VALUES(@pid,@whouseid,@whousename,@sncode,@meterFakor,@startshift1,@startshift2,@mod_date,@mod_by,@Whouseke)";

                    SQLiteCommand cmd = new SQLiteCommand(query, conn);

                    cmd.Parameters.Add(new SQLiteParameter("@pid", _item.pid));
                    cmd.Parameters.Add(new SQLiteParameter("@whouseid", _item.whouse));
                    cmd.Parameters.Add(new SQLiteParameter("@whousename", _item.whousename));
                    cmd.Parameters.Add(new SQLiteParameter("@sncode", _item.sncode));
                    cmd.Parameters.Add(new SQLiteParameter("@meterFakor", _item.meterFakor));
                    cmd.Parameters.Add(new SQLiteParameter("@startshift1", _item.startshift1));
                    cmd.Parameters.Add(new SQLiteParameter("@startshift2", _item.startshift2));
                    cmd.Parameters.Add(new SQLiteParameter("@mod_date", _item.mod_date));
                    cmd.Parameters.Add(new SQLiteParameter("@mod_by", _item.mod_by));
                    cmd.Parameters.Add(new SQLiteParameter("@Whouseke", 1));
                    

                    recs = cmd.ExecuteNonQuery();
                    if (recs > 0)
                    {
                        whosueSelected = _item.whouse.ToString();
                        MessageBox.Show("Warehouse Setting Successfull", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                    if (conn.State == ConnectionState.Open) conn.Close();

                    comboBoxWarehouse.Enabled = false;
                    cbWarehouseName.Enabled = false;
                    comboBoxSerialNumber.Enabled = false;
                    dateTimeShift1.Enabled = false;
                    dateTimeShift2.Enabled = false;
                    btnSubmit.Visible = false;
                    btnNewConfig.Visible = true;
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
                MessageBox.Show("FUserSetting.cs Exception: Terjadi kesalahan saat simpan setting, mohon cek log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", "FUserSetting.cs Exception btnSubmit_Click : " + ex.Message);
                save_error_log(ex.Message, "btnSubmit_Click()");
            }
        }
        bool isValid()
        {
            bool iBlStatus = false;

            if (comboBoxWarehouse.Text == "")
            {
                MessageBox.Show("Warehouse di isi..", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (cbWarehouseName.Text == "")
            {
                MessageBox.Show("Warehouse Name di isi..", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (comboBoxSerialNumber.Text == "")
            {
                MessageBox.Show("SN Code di isi..", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (dateTimeShift1.Text == "")
            {
                MessageBox.Show("Shift 1 di isi..", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (dateTimeShift2.Text == "")
            {
                MessageBox.Show("Shift 2 di isi..", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                iBlStatus = true;
            }

            return iBlStatus;
        }
        bool isValid2()
        {
            bool iBlStatus = false;

            if (comboBoxWarehouse2.Text == "")
            {
                MessageBox.Show("Warehouse di isi..", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (cbWarehouseName2.Text == "")
            {
                MessageBox.Show("Warehouse Name di isi..", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (comboBoxSerialNumberwh2.Text == "")
            {
                MessageBox.Show("SN Code di isi..", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (dateTimeShift12.Text == "")
            {
                MessageBox.Show("Shift 1 di isi..", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (dateTimeShift22.Text == "")
            {
                MessageBox.Show("Shift 2 di isi..", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                iBlStatus = true;
            }

            return iBlStatus;
        }
        int execCmd(String sQuery, SQLiteConnection sConn)
        {
            int iReturn = 0;

            var cmd = new SQLiteCommand(sQuery, sConn);
            iReturn = cmd.ExecuteNonQuery();

            return iReturn;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void comboBoxWarehouse_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                cbWarehouseName.Items.Clear();
                cbWarehouseName.Text = "";
                SQLiteConnection conn;

                conn = new SQLiteConnection(myConnectionString);
                if (conn.State != ConnectionState.Open) conn.Open();
                string query = string.Empty;                

                query = "SELECT DISTINCT WhouseLocation FROM flowmeter where Warehouse = '" + WHouseSetting1.whouse + "'";
                SQLiteCommand cmds = new SQLiteCommand(query, conn);
                SQLiteDataReader readers = cmds.ExecuteReader();
                while (readers.Read())
                {
                    cbWarehouseName.Items.Add(readers["WhouseLocation"]);
                }
                cbWarehouseName.SelectedIndex = 0;
                if (conn.State == ConnectionState.Open) conn.Close();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
                MessageBox.Show("FUserSetting.cs Exception: Terjadi kesalahan saat pilih warehouse, mohon cek log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", "FUserSetting.cs Exception comboBoxWarehouse_SelectedIndexChanged : " + ex.Message);
                save_error_log(ex.Message, "comboBoxWarehouse_SelectedIndexChanged()");
            }
        }

        private void cbWarehouseName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                comboBoxSerialNumber.Items.Clear();
                comboBoxSerialNumber.Text = "";
                SQLiteConnection conn;

                conn = new SQLiteConnection(myConnectionString);
                if (conn.State != ConnectionState.Open) conn.Open();
                string query = string.Empty;

                //string selectedItem = this.comboBoxSerialNumberwh2.GetItemText(this.comboBoxSerialNumber.SelectedItem);

                query = "SELECT DISTINCT SnCode FROM flowmeter where WhouseLocation = '" + WHouseSetting1.whousename + "'";
                SQLiteCommand cmds = new SQLiteCommand(query, conn);
                SQLiteDataReader readers = cmds.ExecuteReader();
                while (readers.Read())
                {
                    comboBoxSerialNumber.Items.Add(readers["SnCode"]);
                }
                comboBoxSerialNumber.SelectedIndex = 0;
                if (conn.State == ConnectionState.Open) conn.Close();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
                MessageBox.Show("FUserSetting.cs Exception: Terjadi kesalahan saat pilih warehouse name, mohon cek log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", "FUserSetting.cs Exception cbWarehouseName_SelectedIndexChanged : " + ex.Message);
                save_error_log(ex.Message, "cbWarehouseName_SelectedIndexChanged()");
            }
        }

        private void comboBoxSerialNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SQLiteConnection conn;

                conn = new SQLiteConnection(myConnectionString);
                if (conn.State != ConnectionState.Open) conn.Open();
                string query = string.Empty;

                //string selectedWh = this.comboBoxWarehouse.GetItemText(this.comboBoxWarehouse.SelectedItem);
                //string selectedSN = this.comboBoxSerialNumber.GetItemText(this.comboBoxSerialNumber.SelectedItem);

                query = $"SELECT DISTINCT MeterFakor FROM flowmeter where Warehouse = '{WHouseSetting1.whouse}' and SnCode = '{WHouseSetting1.sncode}'";
                SQLiteCommand cmds = new SQLiteCommand(query, conn);
                SQLiteDataReader readers = cmds.ExecuteReader();

                while (readers.Read())
                {
                    pMeterFaktor = double.Parse(readers["MeterFakor"].ToString(), System.Globalization.CultureInfo.InvariantCulture);
                }

                if (conn.State == ConnectionState.Open) conn.Close();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
                MessageBox.Show("FUserSetting.cs Exception: Terjadi kesalahan saat pilih serial number, mohon cek log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", "FUserSetting.cs Exception comboBoxSerialNumber_SelectedIndexChanged : " + ex.Message);
                save_error_log(ex.Message, "comboBoxSerialNumber_SelectedIndexChanged()");
            }
        }

        
        #endregion
        private void tabControl1_DrawItem(object sender, DrawItemEventArgs e)
        {
            TabPage page = tabControl1.TabPages[e.Index];
            e.Graphics.FillRectangle(new SolidBrush(page.BackColor), e.Bounds);

            Rectangle paddedBounds = e.Bounds;
            int yOffset = (e.State == DrawItemState.Selected) ? -2 : 1;
            paddedBounds.Offset(1, yOffset);
            TextRenderer.DrawText(e.Graphics, page.Text, e.Font, paddedBounds, page.ForeColor);
        }

        private void tabControl1_Selected(object sender, TabControlEventArgs e)
        {
            if (tabControl1.SelectedTab.Text == "User Setting")
            {
                var f = new FAdminValidation();
                f.ShowDialog();
                if (f.DialogResult != DialogResult.OK)
                {
                    tabControl1.SelectedTab = tabControl1.TabPages["tpConnection"];
                }
            }
            else if (tabControl1.SelectedTab.Text == "WHouse 1")
            {
                var f = new FAdminValidation();
                f.ShowDialog();
                if (f.DialogResult != DialogResult.OK)
                {
                    tabControl1.SelectedTab = tabControl1.TabPages["tpConnection"];
                }
            }
            else if (tabControl1.SelectedTab.Text == "WHouse 2")
            {
                var f = new FAdminValidation();
                f.ShowDialog();
                if (f.DialogResult != DialogResult.OK)
                {
                    tabControl1.SelectedTab = tabControl1.TabPages["tpConnection"];
                }
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Hide();
        }

        private void btnSaveConnection_Click(object sender, EventArgs e)
        {
            try
            {
                var numCheck = numCheckOnlineLogin.Value * 1000;
                Properties.Settings.Default.connection = btnConnection.Text;
                Properties.Settings.Default.interval_check_online = numCheck.ToString();
                Properties.Settings.Default.Save();
                Properties.Settings.Default.Reload();
                toolConnection = btnConnection.Text;
                MessageBox.Show("Berhasil Disimpan!", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
                MessageBox.Show("FUserSetting.cs Exception: Terjadi kesalahan saat simpan koneksi, mohon cek log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", "FUserSetting.cs Exception btnSaveConnection_Click : " + ex.Message);
                save_error_log(ex.Message, "btnSaveConnection_Click()");
            }
        }

        private void btnMenu1_Click(object sender, EventArgs e)
        {
            if (clb_menu_access.GetItemCheckState(0) == CheckState.Checked)
            {
                clb_menu_access.SetItemChecked(0, false);
                btnMenu1.BackColor = Color.Red;
                btn7InchMenu4.BackColor = Color.Red;
            }
            else
            {
                btnMenu1.BackColor = Color.Green;
                btn7InchMenu4.BackColor = Color.Green;
                clb_menu_access.SetItemChecked(0, true);
            }
        }

        private void btnMenu2_Click(object sender, EventArgs e)
        {
            if (clb_menu_access.GetItemCheckState(1) == CheckState.Checked)
            {
                clb_menu_access.SetItemChecked(1, false);
                btnMenu2.BackColor = Color.Red;
                btn7InchMenu2.BackColor = Color.Red;

            }
            else
            {
                btnMenu2.BackColor = Color.Green;
                btn7InchMenu2.BackColor = Color.Green;
                clb_menu_access.SetItemChecked(1, true);
            }
        }

        private void btnMenu3_Click(object sender, EventArgs e)
        {
            if (clb_menu_access.GetItemCheckState(2) == CheckState.Checked)
            {
                clb_menu_access.SetItemChecked(2, false);
                btnMenu3.BackColor = Color.Red;
                btn7InchMenu3.BackColor = Color.Red;
            }
            else
            {
                btnMenu3.BackColor = Color.Green;
                btn7InchMenu3.BackColor = Color.Green;
                clb_menu_access.SetItemChecked(2, true);
            }
        }

        private void btnMenu4_Click(object sender, EventArgs e)
        {
            if (clb_menu_access.GetItemCheckState(3) == CheckState.Checked)
            {
                clb_menu_access.SetItemChecked(3, false);
                btnMenu4.BackColor = Color.Red;
            }
            else
            {
                btnMenu4.BackColor = Color.Green;
                clb_menu_access.SetItemChecked(3, true);
            }
        }

        private void btnMenu5_Click(object sender, EventArgs e)
        {
            if (clb_menu_access.GetItemCheckState(4) == CheckState.Checked)
            {
                clb_menu_access.SetItemChecked(4, false);
                btnMenu5.BackColor = Color.Red;
            }
            else
            {
                btnMenu5.BackColor = Color.Green;
                clb_menu_access.SetItemChecked(4, true);
            }
        }

        private void btnMenu6_Click(object sender, EventArgs e)
        {
            if (clb_menu_access.GetItemCheckState(5) == CheckState.Checked)
            {
                clb_menu_access.SetItemChecked(5, false);
                btnMenu6.BackColor = Color.Red;
            }
            else
            {
                btnMenu6.BackColor = Color.Green;
                clb_menu_access.SetItemChecked(5, true);
            }
        }

        private void btnMenu7_Click(object sender, EventArgs e)
        {
            if (clb_menu_access.GetItemCheckState(6) == CheckState.Checked)
            {
                clb_menu_access.SetItemChecked(6, false);
                btnMenu7.BackColor = Color.Red;
            }
            else
            {
                btnMenu7.BackColor = Color.Green;
                clb_menu_access.SetItemChecked(6, true);
            }
        }

        private void btnMenu8_Click(object sender, EventArgs e)
        {
            if (clb_menu_access.GetItemCheckState(7) == CheckState.Checked)
            {
                clb_menu_access.SetItemChecked(7, false);
                btnMenu8.BackColor = Color.Red;
                btn7InchMenu8.BackColor = Color.Red;
            }
            else
            {
                btnMenu8.BackColor = Color.Green;
                btn7InchMenu8.BackColor = Color.Green;
                clb_menu_access.SetItemChecked(7, true);
            }
        }

        private void btnMenu9_Click(object sender, EventArgs e)
        {
            if (clb_menu_access.GetItemCheckState(8) == CheckState.Checked)
            {
                clb_menu_access.SetItemChecked(8, false);
                btnMenu9.BackColor = Color.Red;
                btn7InchMenu9.BackColor = Color.Red;
            }
            else
            {
                btnMenu9.BackColor = Color.Green;
                btn7InchMenu9.BackColor = Color.Green;
                clb_menu_access.SetItemChecked(8, true);
            }
        }

        private void comboBoxWarehouse_Click(object sender, EventArgs e)
        {
            try
            {
                ProcessStartInfo psStartInfo = new ProcessStartInfo();
                //psStartInfo.FileName = @"C:\Program Files\Common Files\microsoft shared\ink\TabTip.exe";
                var dir = Path.Combine(Environment.CurrentDirectory, "VKeyboard", "VKeyboard.exe");
                psStartInfo.FileName = dir;

                Process ps = Process.Start(psStartInfo);

            }
            catch (Exception ex)
            {
                var error = ex.Message;
            }
        }

        

        private void btnMenu10_Click(object sender, EventArgs e)
        {
            if (clb_menu_access.GetItemCheckState(9) == CheckState.Checked)
            {
                clb_menu_access.SetItemChecked(9, false);
                btnMenu10.BackColor = Color.Red;                
            }
            else
            {
                btnMenu10.BackColor = Color.Green;
                clb_menu_access.SetItemChecked(9, true);
            }
        }

        private void btnMenu11_Click(object sender, EventArgs e)
        {
            if (clb_menu_access.GetItemCheckState(10) == CheckState.Checked)
            {
                clb_menu_access.SetItemChecked(10, false);
                btnMenu11.BackColor = Color.Red;
            }
            else
            {
                btnMenu11.BackColor = Color.Green;
                clb_menu_access.SetItemChecked(10, true);
            }
        }

        private void btnMenu12_Click(object sender, EventArgs e)
        {
            if (clb_menu_access.GetItemCheckState(11) == CheckState.Checked)
            {
                clb_menu_access.SetItemChecked(11, false);
                btnMenu12.BackColor = Color.Red;
            }
            else
            {
                btnMenu12.BackColor = Color.Green;
                clb_menu_access.SetItemChecked(11, true);
            }
        }

        private void btnMenu13_Click(object sender, EventArgs e)
        {
            if (clb_menu_access.GetItemCheckState(12) == CheckState.Checked)
            {
                clb_menu_access.SetItemChecked(12, false);
                btnMenu13.BackColor = Color.Red;
            }
            else
            {
                btnMenu13.BackColor = Color.Green;
                clb_menu_access.SetItemChecked(12, true);
            }
        }

        private void btnMenu14_Click(object sender, EventArgs e)
        {
            if (clb_menu_access.GetItemCheckState(13) == CheckState.Checked)
            {
                clb_menu_access.SetItemChecked(13, false);
                btnMenu14.BackColor = Color.Red;
            }
            else
            {
                btnMenu14.BackColor = Color.Green;
                clb_menu_access.SetItemChecked(13, true);
            }
        }

        private void btnSubmit2_Click(object sender, EventArgs e)
        {
            try
            {
                if (isValid())
                {
                    var _item = new tbl_setting();
                    var initData = GlobalModel.GlobalVar;
                    DateTime localDate = DateTime.Now;
                    int recs = 0;

                    string selectedItemWhouse = this.comboBoxWarehouse2.GetItemText(this.comboBoxWarehouse2.SelectedItem);
                    string selectedItemWhouseName = this.cbWarehouseName2.GetItemText(this.cbWarehouseName2.SelectedItem);
                    string selectedItemSerialNumber = this.comboBoxSerialNumberwh2.GetItemText(this.comboBoxSerialNumberwh2.SelectedItem);


                    _item.pid = System.Guid.NewGuid().ToString();
                    _item.whouse = selectedItemWhouse.ToString();
                    _item.whousename = selectedItemWhouseName.ToString();
                    _item.sncode = selectedItemSerialNumber.ToString();
                    _item.startshift1 = dateTimeShift1.Value.ToString("HH:mm") + ":00";
                    _item.startshift2 = dateTimeShift2.Value.ToString("HH:mm") + ":00";
                    _item.meterFakor = pMeterFaktor;
                    _item.mod_date = localDate.ToString();
                    _item.mod_by = initData.DataEmp.Nrp;

                    SQLiteConnection conn;

                    conn = new SQLiteConnection(myConnectionString);
                    if (conn.State != ConnectionState.Open) conn.Open();
                    string query = string.Empty;

                    SQLiteCommand cmds = new SQLiteCommand(query, conn);
                    query = "DELETE FROM tbl_setting";
                    execCmd(query, conn);
                    query = "INSERT INTO tbl_setting(pid,whouseid,whousename,sncode,meterFakor,startshift1,startshift2,mod_date,mod_by)"
                        + " VALUES(@pid,@whouseid,@whousename,@sncode,@meterFakor,@startshift1,@startshift2,@mod_date,@mod_by)";

                    SQLiteCommand cmd = new SQLiteCommand(query, conn);

                    cmd.Parameters.Add(new SQLiteParameter("@pid", _item.pid));
                    cmd.Parameters.Add(new SQLiteParameter("@whouseid", _item.whouse));
                    cmd.Parameters.Add(new SQLiteParameter("@whousename", _item.whousename));
                    cmd.Parameters.Add(new SQLiteParameter("@sncode", _item.sncode));
                    cmd.Parameters.Add(new SQLiteParameter("@meterFakor", _item.meterFakor));
                    cmd.Parameters.Add(new SQLiteParameter("@startshift1", _item.startshift1));
                    cmd.Parameters.Add(new SQLiteParameter("@startshift2", _item.startshift2));
                    cmd.Parameters.Add(new SQLiteParameter("@mod_date", _item.mod_date));
                    cmd.Parameters.Add(new SQLiteParameter("@mod_by", _item.mod_by));

                    recs = cmd.ExecuteNonQuery();
                    if (recs > 0)
                    {
                        whosueSelected = _item.whouse.ToString();
                        MessageBox.Show("Warehouse Setting Successfull", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                    if (conn.State == ConnectionState.Open) conn.Close();

                    comboBoxWarehouse.Enabled = false;
                    cbWarehouseName.Enabled = false;
                    comboBoxSerialNumber.Enabled = false;
                    dateTimeShift1.Enabled = false;
                    dateTimeShift2.Enabled = false;
                    btnSubmit.Visible = false;
                    btnNewConfig.Visible = true;
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
                MessageBox.Show("FUserSetting.cs Exception: Terjadi kesalahan saat simpan setting, mohon cek log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", "FUserSetting.cs Exception btnSubmit_Click : " + ex.Message);
                save_error_log(ex.Message, "btnSubmit_Click()");
            }
        }

        private void btnSubmit2_Click_1(object sender, EventArgs e)
        {
            try
            {
                if (isValid2())
                {
                    var _item = new tbl_setting();
                    var initData = GlobalModel.GlobalVar;
                    DateTime localDate = DateTime.Now;
                    int recs = 0;

                    string selectedItemWhouse = this.comboBoxWarehouse2.GetItemText(this.comboBoxWarehouse2.SelectedItem);
                    string selectedItemWhouseName = this.cbWarehouseName2.GetItemText(this.cbWarehouseName2.SelectedItem);
                    string selectedItemSerialNumber = this.comboBoxSerialNumberwh2.GetItemText(this.comboBoxSerialNumberwh2.SelectedItem);


                    _item.pid = System.Guid.NewGuid().ToString();
                    _item.whouse = selectedItemWhouse.ToString();
                    _item.whousename = selectedItemWhouseName.ToString();
                    _item.sncode = selectedItemSerialNumber.ToString();
                    _item.startshift1 = dateTimeShift1.Value.ToString("HH:mm") + ":00";
                    _item.startshift2 = dateTimeShift2.Value.ToString("HH:mm") + ":00";
                    _item.meterFakor = pMeterFaktor;
                    _item.mod_date = localDate.ToString();
                    _item.mod_by = initData.DataEmp.Nrp;

                    SQLiteConnection conn;

                    conn = new SQLiteConnection(myConnectionString);
                    if (conn.State != ConnectionState.Open) conn.Open();
                    string query = string.Empty;

                    SQLiteCommand cmds = new SQLiteCommand(query, conn);
                    query = "DELETE FROM tbl_setting where Whouseke = 2";
                    execCmd(query, conn);
                    query = "INSERT INTO tbl_setting(pid,whouseid,whousename,sncode,meterFakor,startshift1,startshift2,mod_date,mod_by,Whouseke)"
                        + " VALUES(@pid,@whouseid,@whousename,@sncode,@meterFakor,@startshift1,@startshift2,@mod_date,@mod_by,@Whouseke)";

                    SQLiteCommand cmd = new SQLiteCommand(query, conn);

                    cmd.Parameters.Add(new SQLiteParameter("@pid", _item.pid));
                    cmd.Parameters.Add(new SQLiteParameter("@whouseid", _item.whouse));
                    cmd.Parameters.Add(new SQLiteParameter("@whousename", _item.whousename));
                    cmd.Parameters.Add(new SQLiteParameter("@sncode", _item.sncode));
                    cmd.Parameters.Add(new SQLiteParameter("@meterFakor", _item.meterFakor));
                    cmd.Parameters.Add(new SQLiteParameter("@startshift1", _item.startshift1));
                    cmd.Parameters.Add(new SQLiteParameter("@startshift2", _item.startshift2));
                    cmd.Parameters.Add(new SQLiteParameter("@mod_date", _item.mod_date));
                    cmd.Parameters.Add(new SQLiteParameter("@mod_by", _item.mod_by));
                    cmd.Parameters.Add(new SQLiteParameter("@Whouseke", 2));
                    
                    recs = cmd.ExecuteNonQuery();
                    if (recs > 0)
                    {
                        whosueSelected = _item.whouse.ToString();
                        MessageBox.Show("Warehouse Setting 2 Successfull", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                    if (conn.State == ConnectionState.Open) conn.Close();

                    comboBoxWarehouse2.Enabled = false;
                    cbWarehouseName2.Enabled = false;
                    comboBoxSerialNumberwh2.Enabled = false;
                    dateTimeShift12.Enabled = false;
                    dateTimeShift22.Enabled = false;
                    btnSubmit2.Visible = false;
                    btnNewConfig2.Visible = true;
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
                MessageBox.Show("FUserSetting.cs Exception: Terjadi kesalahan saat simpan setting 2, mohon cek log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", "FUserSetting.cs Exception btnSubmit2_Click : " + ex.Message);
                save_error_log(ex.Message, "btnSubmit2_Click()");
            }
        }

        private void btnNewConfig2_Click(object sender, EventArgs e)
        {
            comboBoxWarehouse2.Enabled = true;
            comboBoxSerialNumberwh2.Enabled = true;
            dateTimeShift12.Enabled = true;
            dateTimeShift22.Enabled = true;
            btnSubmit2.Visible = true;
            btnNewConfig2.Visible = false;
        }

        private void btnMenu15_Click(object sender, EventArgs e)
        {
            if (clb_menu_access.GetItemCheckState(14) == CheckState.Checked)
            {
                clb_menu_access.SetItemChecked(14, false);
                btnMenu15.BackColor = Color.Red;
            }
            else
            {
                btnMenu15.BackColor = Color.Green;
                clb_menu_access.SetItemChecked(14, true);
            }
        }

        private void btnMenu16_Click(object sender, EventArgs e)
        {
            if (clb_menu_access.GetItemCheckState(15) == CheckState.Checked)
            {
                clb_menu_access.SetItemChecked(15, false);
                btnMenu16.BackColor = Color.Red;
            }
            else
            {
                btnMenu16.BackColor = Color.Green;
                clb_menu_access.SetItemChecked(15, true);
            }
        }

        private void btnMenu17_Click(object sender, EventArgs e)
        {
            if (clb_menu_access.GetItemCheckState(16) == CheckState.Checked)
            {
                clb_menu_access.SetItemChecked(16, false);
                btnMenu17.BackColor = Color.Red;
            }
            else
            {
                btnMenu17.BackColor = Color.Green;
                clb_menu_access.SetItemChecked(16, true);
            }
        }

        private void btnMenu18_Click(object sender, EventArgs e)
        {
            if (clb_menu_access.GetItemCheckState(17) == CheckState.Checked)
            {
                clb_menu_access.SetItemChecked(17, false);
                btnMenu18.BackColor = Color.Red;
            }
            else
            {
                btnMenu18.BackColor = Color.Green;
                clb_menu_access.SetItemChecked(17, true);
            }
        }

        private void comboBoxWarehouse2_Click(object sender, EventArgs e)
        {
            try
            {
                ProcessStartInfo psStartInfo = new ProcessStartInfo();
                //psStartInfo.FileName = @"C:\Program Files\Common Files\microsoft shared\ink\TabTip.exe";
                var dir = Path.Combine(Environment.CurrentDirectory, "VKeyboard", "VKeyboard.exe");
                psStartInfo.FileName = dir;

                Process ps = Process.Start(psStartInfo);

            }
            catch (Exception ex)
            {
                var error = ex.Message;
            }
        }

        private void cbWarehouseName2_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                comboBoxSerialNumberwh2.Items.Clear();
                comboBoxSerialNumberwh2.Text = "";
                SQLiteConnection conn;

                conn = new SQLiteConnection(myConnectionString);
                if (conn.State != ConnectionState.Open) conn.Open();
                string query = string.Empty;

                //string selectedItem = this.comboBoxSerialNumberwh2.GetItemText(this.comboBoxSerialNumber.SelectedItem);

                query = "SELECT DISTINCT SnCode FROM flowmeter where WhouseLocation = '" + WHouseSetting2.whousename + "'";
                SQLiteCommand cmds = new SQLiteCommand(query, conn);
                SQLiteDataReader readers = cmds.ExecuteReader();
                while (readers.Read())
                {
                    comboBoxSerialNumberwh2.Items.Add(readers["SnCode"]);
                }
                comboBoxSerialNumberwh2.SelectedIndex = 0;
                if (conn.State == ConnectionState.Open) conn.Close();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
                MessageBox.Show("FUserSetting.cs Exception: Terjadi kesalahan saat pilih warehouse name setting 2, mohon cek log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", "FUserSetting.cs Exception cbWarehouseName2_SelectedIndexChanged : " + ex.Message);
                save_error_log(ex.Message, "cbWarehouseName2_SelectedIndexChanged()");
            }
        }

        private void comboBoxSerialNumber2_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SQLiteConnection conn;

                conn = new SQLiteConnection(myConnectionString);
                if (conn.State != ConnectionState.Open) conn.Open();
                string query = string.Empty;

                //string selectedWh = this.comboBoxWarehouse.GetItemText(this.comboBoxWarehouse.SelectedItem);
                //string selectedSN = this.comboBoxSerialNumber.GetItemText(this.comboBoxSerialNumber.SelectedItem);

                query = $"SELECT DISTINCT MeterFakor FROM flowmeter where Warehouse = '{WHouseSetting2.whouse}' and SnCode = '{WHouseSetting2.sncode}'";
                SQLiteCommand cmds = new SQLiteCommand(query, conn);
                SQLiteDataReader readers = cmds.ExecuteReader();

                while (readers.Read())
                {
                    pMeterFaktor = double.Parse(readers["MeterFakor"].ToString(), System.Globalization.CultureInfo.InvariantCulture);
                }

                if (conn.State == ConnectionState.Open) conn.Close();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
                MessageBox.Show("FUserSetting.cs Exception: Terjadi kesalahan saat pilih serial number setting 2, mohon cek log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", "FUserSetting.cs Exception comboBoxSerialNumber2_SelectedIndexChanged : " + ex.Message);
                save_error_log(ex.Message, "comboBoxSerialNumber2_SelectedIndexChanged()");
            }
        }

        private void cbWarehouseName_Click(object sender, EventArgs e)
        {
            try
            {
                ProcessStartInfo psStartInfo = new ProcessStartInfo();
                //psStartInfo.FileName = @"C:\Program Files\Common Files\microsoft shared\ink\TabTip.exe";
                var dir = Path.Combine(Environment.CurrentDirectory, "VKeyboard", "VKeyboard.exe");
                psStartInfo.FileName = dir;

                Process ps = Process.Start(psStartInfo);

            }
            catch (Exception ex)
            {
                var error = ex.Message;
            }
        }

        private void cbWarehouseName2_Click(object sender, EventArgs e)
        {
            try
            {
                ProcessStartInfo psStartInfo = new ProcessStartInfo();
                //psStartInfo.FileName = @"C:\Program Files\Common Files\microsoft shared\ink\TabTip.exe";
                var dir = Path.Combine(Environment.CurrentDirectory, "VKeyboard", "VKeyboard.exe");
                psStartInfo.FileName = dir;

                Process ps = Process.Start(psStartInfo);

            }
            catch (Exception ex)
            {
                var error = ex.Message;
            }
        }

        private void comboBoxWarehouse2_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                cbWarehouseName2.Items.Clear();
                cbWarehouseName2.Text = "";
                SQLiteConnection conn;

                conn = new SQLiteConnection(myConnectionString);
                if (conn.State != ConnectionState.Open) conn.Open();
                string query = string.Empty;

                query = "SELECT DISTINCT WhouseLocation FROM flowmeter where Warehouse = '" + WHouseSetting2.whouse + "'";
                SQLiteCommand cmds = new SQLiteCommand(query, conn);
                SQLiteDataReader readers = cmds.ExecuteReader();
                while (readers.Read())
                {
                    cbWarehouseName2.Items.Add(readers["WhouseLocation"]);
                }
                cbWarehouseName2.SelectedIndex = 0;
                if (conn.State == ConnectionState.Open) conn.Close();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
                MessageBox.Show("FUserSetting.cs Exception: Terjadi kesalahan saat pilih warehouse setting 2, mohon cek log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", "FUserSetting.cs Exception comboBoxWarehouse2_SelectedIndexChanged : " + ex.Message);
                save_error_log(ex.Message, "comboBoxWarehouse2_SelectedIndexChanged()");
            }
        }
    }
}
