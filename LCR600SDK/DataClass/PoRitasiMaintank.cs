﻿using System;
using System.Collections.Generic;

using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace LCR600SDK.DataClass
{
    public partial class PoRitasiMaintank
    {
        [JsonProperty("pid", NullValueHandling = NullValueHandling.Ignore)]
        public string Pid { get; set; }

        [JsonProperty("pid_po", NullValueHandling = NullValueHandling.Ignore)]
        public string PidPo { get; set; }

        [JsonProperty("ref_tbl", NullValueHandling = NullValueHandling.Ignore)]
        public string RefTbl { get; set; }

        [JsonProperty("district", NullValueHandling = NullValueHandling.Ignore)]
        public string District { get; set; }

        [JsonProperty("po_no", NullValueHandling = NullValueHandling.Ignore)]
        public string PoNo { get; set; }

        [JsonProperty("po_qty", NullValueHandling = NullValueHandling.Ignore)]
        public long? PoQty { get; set; }

        [JsonProperty("receive_at", NullValueHandling = NullValueHandling.Ignore)]
        public string ReceiveAt { get; set; }

        [JsonProperty("transportir_code", NullValueHandling = NullValueHandling.Ignore)]
        public string TransportirCode { get; set; }

        [JsonProperty("transportir_name", NullValueHandling = NullValueHandling.Ignore)]
        public string TransportirName { get; set; }
    }

    public partial class PoRitasiMaintank
    {
        public static List<PoRitasiMaintank> FromJson(string json) => JsonConvert.DeserializeObject<List<PoRitasiMaintank>>(json, Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this List<PoRitasiMaintank> self) => JsonConvert.SerializeObject(self, Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}

