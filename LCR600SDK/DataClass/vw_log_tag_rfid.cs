﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LCR600SDK.DataClass
{
    class vw_log_tag_rfid
    {
        public DateTime? timestamps { get; set; }
        public string unit_tag { get; set; }
        public string unitno_rfid { get; set; }
        public string nozzleunittag_m { get; set; }
        public string UnitNo_db_local { get; set; }        
    }
}
