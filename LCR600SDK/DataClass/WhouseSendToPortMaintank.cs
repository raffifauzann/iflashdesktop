﻿using System;
using System.Collections.Generic;

using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace LCR600SDK.DataClass
{ 
    public partial class WhouseSendToPortMaintank
    {
        [JsonProperty("pid", NullValueHandling = NullValueHandling.Ignore)]
        public string Pid { get; set; }

        [JsonProperty("warehouseid", NullValueHandling = NullValueHandling.Ignore)]
        public string Warehouseid { get; set; }

        [JsonProperty("warehousename", NullValueHandling = NullValueHandling.Ignore)]
        public string Warehousename { get; set; }

        [JsonProperty("warehousetype", NullValueHandling = NullValueHandling.Ignore)]
        public string Warehousetype { get; set; }

        [JsonProperty("district", NullValueHandling = NullValueHandling.Ignore)]
        public string District { get; set; }

        [JsonProperty("fungsi", NullValueHandling = NullValueHandling.Ignore)]
        public string Fungsi { get; set; }

        [JsonProperty("availablestatus", NullValueHandling = NullValueHandling.Ignore)]
        public long? Availablestatus { get; set; }

        [JsonProperty("issuedhd", NullValueHandling = NullValueHandling.Ignore)]
        public long? Issuedhd { get; set; }

        [JsonProperty("istransfer", NullValueHandling = NullValueHandling.Ignore)]
        public long? Istransfer { get; set; }

        [JsonProperty("isreceiving", NullValueHandling = NullValueHandling.Ignore)]
        public long? Isreceiving { get; set; }

        [JsonProperty("issupport", NullValueHandling = NullValueHandling.Ignore)]
        public long? Issupport { get; set; }

        [JsonProperty("isexavator", NullValueHandling = NullValueHandling.Ignore)]
        public long? Isexavator { get; set; }

        [JsonProperty("lineactive", NullValueHandling = NullValueHandling.Ignore)]
        public long? Lineactive { get; set; }

        [JsonProperty("modifieddate", NullValueHandling = NullValueHandling.Ignore)]
        public string Modifieddate { get; set; }

        [JsonProperty("modifiedby", NullValueHandling = NullValueHandling.Ignore)]
        public string Modifiedby { get; set; }

        [JsonProperty("modifiedbydesc", NullValueHandling = NullValueHandling.Ignore)]
        public string Modifiedbydesc { get; set; }

        [JsonProperty("issubmited", NullValueHandling = NullValueHandling.Ignore)]
        public long? Issubmited { get; set; }

        [JsonProperty("docstatus", NullValueHandling = NullValueHandling.Ignore)]
        public long? Docstatus { get; set; }

        [JsonProperty("kategori_whouse", NullValueHandling = NullValueHandling.Ignore)]
        public string KategoriWhouse { get; set; }

        [JsonProperty("kategori_whouse_to", NullValueHandling = NullValueHandling.Ignore)]
        public string KategoriWhouseTo { get; set; }

        [JsonProperty("is_issued", NullValueHandling = NullValueHandling.Ignore)]
        public string IsIssued { get; set; }
    }

    public partial class WhouseSendToPortMaintank
    {
        public static List<WhouseSendToPortMaintank> FromJson(string json) => JsonConvert.DeserializeObject<List<WhouseSendToPortMaintank>>(json, Converter.Settings);
    } 
}

