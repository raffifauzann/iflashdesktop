﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LCR600SDK.DataClass
{
    class RFIDMessage
    {
        public string prodid { get; set; }
        public string devname { get; set; }
        public string localip { get; set; }
        public string softapip { get; set; }
        public string mac { get; set; }
        public string nozzletagid { get; set; }
        public string unittagid { get; set; }
        public string unitno { get; set; }
        public string MaxTank { get; set; }
        public int istagvalid { get; set; }
    }
}
