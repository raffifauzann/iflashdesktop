﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters; 

namespace LCR600SDK.DataClass
{
    public partial class ReceivedDirectClass
    {
        public bool? Status { get; set; }

        [JsonProperty("remarks", NullValueHandling = NullValueHandling.Ignore)]
        public string Remarks { get; set; }

        [JsonProperty("listData", NullValueHandling = NullValueHandling.Ignore)]
        public List<ListReceivedDirectClass> ListData { get; set; }

        [JsonProperty("totalPage", NullValueHandling = NullValueHandling.Ignore)]
        public long? TotalPage { get; set; }

        [JsonProperty("currentPage", NullValueHandling = NullValueHandling.Ignore)]
        public long? CurrentPage { get; set; }

        [JsonProperty("pageSize", NullValueHandling = NullValueHandling.Ignore)]
        public long? PageSize { get; set; }

        [JsonProperty("totalSize", NullValueHandling = NullValueHandling.Ignore)]
        public long? TotalSize { get; set; }
    }

    public partial class ListReceivedDirectClass
    {
        [JsonProperty("pid_receive", NullValueHandling = NullValueHandling.Ignore)]
        public string PidReceive { get; set; }

        [JsonProperty("ref_tbl", NullValueHandling = NullValueHandling.Ignore)]
        public string RefTbl { get; set; }

        [JsonProperty("ref_id", NullValueHandling = NullValueHandling.Ignore)]
        public string RefId { get; set; }

        [JsonProperty("district", NullValueHandling = NullValueHandling.Ignore)]
        public string District { get; set; }

        [JsonProperty("po_no", NullValueHandling = NullValueHandling.Ignore)]
        public string PoNo { get; set; }

        [JsonProperty("receive_from", NullValueHandling = NullValueHandling.Ignore)]
        public string ReceiveForm { get; set; }

        [JsonProperty("receive_at", NullValueHandling = NullValueHandling.Ignore)]
        public string ReceiveAt { get; set; }

        [JsonProperty("receive_at_code", NullValueHandling = NullValueHandling.Ignore)]
        public string ReceiveAtCode { get; set; }

        [JsonProperty("receive_qty", NullValueHandling = NullValueHandling.Ignore)]
        public int? ReceiveQty { get; set; }

        [JsonProperty("receive_sn_flow_meter1", NullValueHandling = NullValueHandling.Ignore)]
        public string ReceiveSnFlowMeter1 { get; set; }

        [JsonProperty("receive_meter_faktor")]
        public string ReceiveMeterFaktor { get; set; }

        [JsonProperty("receive_density")]
        public string ReceiveDensity { get; set; }

        [JsonProperty("receive_temperature")]
        public string ReceiveTemperature { get; set; }

        [JsonProperty("receive_by")] 
        public string ReceiveBy { get; set; }

        [JsonProperty("receive_date")]
        public DateTime? ReceiveDate { get; set; }

        [JsonProperty("foto", NullValueHandling = NullValueHandling.Ignore)]
        public string Foto { get; set; }

        [JsonProperty("is_valid", NullValueHandling = NullValueHandling.Ignore)]
        public int? IsValid { get; set; }

        [JsonProperty("is_complete_po", NullValueHandling = NullValueHandling.Ignore)]
        public int? IsCOmpletePo { get; set; }

        [JsonProperty("last_mod_by", NullValueHandling = NullValueHandling.Ignore)]
        public string LastModBy { get; set; }

        [JsonProperty("last_mod_date", NullValueHandling = NullValueHandling.Ignore)]
        public DateTime? LastModDate { get; set; }

        [JsonProperty("oc_flag_loading_mse1r1", NullValueHandling = NullValueHandling.Ignore)]
        public int? OcFlaghLoadingMse1r1 { get; set; }

        [JsonProperty("oc_loading_remarks_mse1r1", NullValueHandling = NullValueHandling.Ignore)]
        public int? OcLoadingRemarksMse1r1 { get; set; }

        [JsonProperty("oc_loading_datetime_mse1r1", NullValueHandling = NullValueHandling.Ignore)]
        public DateTime? OcLoadingDateTimeMse1r1 { get; set; }

        [JsonProperty("sir_no", NullValueHandling = NullValueHandling.Ignore)]
        public string SirNo { get; set; }

        [JsonProperty("receive_qty_pama", NullValueHandling = NullValueHandling.Ignore)]
        public int? ReceiveQtyPama { get; set; }

        [JsonProperty("receive_date_exit", NullValueHandling = NullValueHandling.Ignore)]
        public DateTime? ReceiveDateExit { get; set; }

        [JsonProperty("totaliser_awal", NullValueHandling = NullValueHandling.Ignore)]
        public int? TotaliserAwal { get; set; }

        [JsonProperty("totaliser_akhir", NullValueHandling = NullValueHandling.Ignore)]
        public int? TotaliserAkhir { get; set; }
        [JsonProperty("start_loading", NullValueHandling = NullValueHandling.Ignore)]
        public string start_loading { get; set; }
        [JsonProperty("end_loading", NullValueHandling = NullValueHandling.Ignore)]
        public string end_loading { get; set; }





    }
}
