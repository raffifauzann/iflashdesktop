﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters; 

namespace LCR600SDK.DataClass
{
    public partial class RitasiMaintankReceiveClass
    {
        public bool? Status { get; set; }

        [JsonProperty("remarks", NullValueHandling = NullValueHandling.Ignore)]
        public string Remarks { get; set; }

        [JsonProperty("listData", NullValueHandling = NullValueHandling.Ignore)]
        public List<ListDatumRitasiMaintank> ListData { get; set; }

        [JsonProperty("totalPage", NullValueHandling = NullValueHandling.Ignore)]
        public long? TotalPage { get; set; }

        [JsonProperty("currentPage", NullValueHandling = NullValueHandling.Ignore)]
        public long? CurrentPage { get; set; }

        [JsonProperty("pageSize", NullValueHandling = NullValueHandling.Ignore)]
        public long? PageSize { get; set; }

        [JsonProperty("totalSize", NullValueHandling = NullValueHandling.Ignore)]
        public long? TotalSize { get; set; }
    }

    public partial class ListDatumRitasiMaintank
    {
        [JsonProperty("pid_ritasi_maintank", NullValueHandling = NullValueHandling.Ignore)]
        public string PidRitasiMaintank { get; set; }

        [JsonProperty("ref_tbl", NullValueHandling = NullValueHandling.Ignore)]
        public string RefTbl { get; set; }

        [JsonProperty("ref_id", NullValueHandling = NullValueHandling.Ignore)]
        public string RefId { get; set; }

        [JsonProperty("district", NullValueHandling = NullValueHandling.Ignore)]
        public string District { get; set; }

        [JsonProperty("plan_start_period", NullValueHandling = NullValueHandling.Ignore)]
        public string PlanStartPeriod { get; set; }

        [JsonProperty("plan_finish_period", NullValueHandling = NullValueHandling.Ignore)]
        public string PlanFinishPeriod { get; set; }

        [JsonProperty("send_type", NullValueHandling = NullValueHandling.Ignore)]
        public string SendType { get; set; }

        [JsonProperty("send_po_no", NullValueHandling = NullValueHandling.Ignore)]
        public string SendPoNo { get; set; }

        [JsonProperty("send_do_no", NullValueHandling = NullValueHandling.Ignore)]
        public string SendDoNo { get; set; }

        [JsonProperty("send_qty", NullValueHandling = NullValueHandling.Ignore)]
        public int? SendQty { get; set; }

        [JsonProperty("send_qty_x_mf", NullValueHandling = NullValueHandling.Ignore)]
        public int? SendQtyXMf { get; set; }

        [JsonProperty("transportir_code", NullValueHandling = NullValueHandling.Ignore)]
        public string TransportirCode { get; set; }

        [JsonProperty("transportir_name", NullValueHandling = NullValueHandling.Ignore)]
        public string TransportirName { get; set; }

        [JsonProperty("send_ft_no", NullValueHandling = NullValueHandling.Ignore)] 
        public string SendFtNo { get; set; }

        [JsonProperty("send_driver_name", NullValueHandling = NullValueHandling.Ignore)]
        public string SendDriverName { get; set; }

        [JsonProperty("send_date", NullValueHandling = NullValueHandling.Ignore)]
        public DateTime? SendDate { get; set; }

        [JsonProperty("send_bbn", NullValueHandling = NullValueHandling.Ignore)]
        public int? SendBbn { get; set; }

        [JsonProperty("send_from", NullValueHandling = NullValueHandling.Ignore)]
        public string SendFrom { get; set; }
        [JsonProperty("send_from_code", NullValueHandling = NullValueHandling.Ignore)]
        public string SendFromCode { get; set; }

        [JsonProperty("send_to", NullValueHandling = NullValueHandling.Ignore)]
        public string SendTo { get; set; }
        [JsonProperty("send_to_code", NullValueHandling = NullValueHandling.Ignore)]
        public string SendToCode { get; set; }

        [JsonProperty("send_dipping_hole1_port", NullValueHandling = NullValueHandling.Ignore)]
        public double? SendDippingHole1Port { get; set; }

        [JsonProperty("send_dipping_hole2_port", NullValueHandling = NullValueHandling.Ignore)]
        public double? SendDippingHole2Port { get; set; }

        [JsonProperty("send_iscomplete", NullValueHandling = NullValueHandling.Ignore)]
        public int? SendIscomplete { get; set; }

        [JsonProperty("send_sn_flow_meter", NullValueHandling = NullValueHandling.Ignore)]
        public string SendsnFlowMeter { get; set; }

        [JsonProperty("send_meter_faktor", NullValueHandling = NullValueHandling.Ignore)]
        public double? SendMeterFaktor { get; set; }

        [JsonProperty("send_flow_meter_awal", NullValueHandling = NullValueHandling.Ignore)]
        public int? SendFlowMeterAwal { get; set; }

        [JsonProperty("send_flow_meter_akhir", NullValueHandling = NullValueHandling.Ignore)]
        public int? SendFlowMeterAkhir { get; set; }

        [JsonProperty("send_no_segel_1", NullValueHandling = NullValueHandling.Ignore)]
        public string SendNoSegel1 { get; set; }

        [JsonProperty("send_no_segel_2", NullValueHandling = NullValueHandling.Ignore)]
        public string SendNoSegel2 { get; set; }

        [JsonProperty("send_no_segel_3", NullValueHandling = NullValueHandling.Ignore)]
        public string SendNoSegel3 { get; set; }

        [JsonProperty("send_no_segel_4", NullValueHandling = NullValueHandling.Ignore)]
        public string SendNoSegel4 { get; set; }

        [JsonProperty("send_no_segel_5", NullValueHandling = NullValueHandling.Ignore)]
        public string SendNoSegel5 { get; set; }

        [JsonProperty("send_no_segel_6", NullValueHandling = NullValueHandling.Ignore)]
        public string SendNoSegel6 { get; set; }

        [JsonProperty("send_no_segel_7", NullValueHandling = NullValueHandling.Ignore)]
        public string SendNoSegel7 { get; set; }

        [JsonProperty("send_no_segel_8", NullValueHandling = NullValueHandling.Ignore)]
        public string SendNoSegel8 { get; set; }

        [JsonProperty("send_no_segel_9", NullValueHandling = NullValueHandling.Ignore)]
        public string SendNoSegel9 { get; set; }

        [JsonProperty("send_no_segel_10", NullValueHandling = NullValueHandling.Ignore)]
        public string SendNoSegel10 { get; set; }

        [JsonProperty("receive_at", NullValueHandling = NullValueHandling.Ignore)]
        public string ReceiveAt { get; set; }
        [JsonProperty("receive_at_code", NullValueHandling = NullValueHandling.Ignore)]
        public string ReceiveAtCode { get; set; }

        [JsonProperty("receive_dipping_hole1_mt", NullValueHandling = NullValueHandling.Ignore)]
        public string ReceiveDippingHole1Mt { get; set; }

        [JsonProperty("receive_dipping_hole2_mt", NullValueHandling = NullValueHandling.Ignore)]
        public string ReceiveDippingHole2Mt { get; set; }

        [JsonProperty("receive_sn_flow_meter1", NullValueHandling = NullValueHandling.Ignore)]
        public string ReceiveSnFlowMeter1 { get; set; }

        [JsonProperty("receive_meter_faktor", NullValueHandling = NullValueHandling.Ignore)]
        public double? ReceiveMeterFaktor { get; set; }

        [JsonProperty("receive_flow_meter_awal1", NullValueHandling = NullValueHandling.Ignore)]
        public int? ReceiveFlowMeterAwal1 { get; set; }

        [JsonProperty("receive_flow_meter_akhir1")]
        public int? ReceiveFlowMeterAkhir1 { get; set; }

        [JsonProperty("receive_qty1")]
        public int? ReceiveQty1 { get; set; }
        [JsonProperty("receive_meter_faktor2", NullValueHandling = NullValueHandling.Ignore)]
        public double? ReceiveMeterFaktor2 { get; set; }

        [JsonProperty("receive_sn_flow_meter2")]
        public string ReceiveSnFlowMeter2 { get; set; }

        [JsonProperty("receive_flow_meter_awal2")]
        public int? ReceiveFlowMeterAwal2 { get; set; }

        [JsonProperty("receive_flow_meter_akhir2")]
        public int? ReceiveFlowMeterAkhir2 { get; set; }

        [JsonProperty("receive_qty2")]
        public int? ReceiveQty2 { get; set; }

        [JsonProperty("receive_density")]
        public string ReceiveDensity { get; set; }

        [JsonProperty("receive_temperature")]
        public string ReceiveTemperature { get; set; }

        [JsonProperty("receive_qty_additive")]
        public string ReceiveQtyAdditive { get; set; }

        [JsonProperty("receive_iscomplete")]
        public string ReceiveIscomplete { get; set; }

        [JsonProperty("receive_driver_name")]
        public string ReceiveDriverName { get; set; }

        [JsonProperty("receive_no_segel_1")]
        public string ReceiveNoSegel1 { get; set; }

        [JsonProperty("receive_no_segel_2")]
        public string ReceiveNoSegel2 { get; set; }

        [JsonProperty("receive_no_segel_3")]
        public string ReceiveNoSegel3 { get; set; }

        [JsonProperty("receive_no_segel_4")]
        public string ReceiveNoSegel4 { get; set; }

        [JsonProperty("receive_no_segel_5")]
        public string ReceiveNoSegel5 { get; set; }

        [JsonProperty("receive_no_segel_6")]
        public string ReceiveNoSegel6 { get; set; }

        [JsonProperty("receive_no_segel_7")]
        public string ReceiveNoSegel7 { get; set; }

        [JsonProperty("receive_no_segel_8")]
        public string ReceiveNoSegel8 { get; set; }

        [JsonProperty("receive_no_segel_9")]
        public string ReceiveNoSegel9 { get; set; }

        [JsonProperty("receive_no_segel_10")]
        public string ReceiveNoSegel10 { get; set; }

        [JsonProperty("doc_status", NullValueHandling = NullValueHandling.Ignore)]
        public string DocStatus { get; set; }

        [JsonProperty("doc_description", NullValueHandling = NullValueHandling.Ignore)]
        public string DocDescription { get; set; }

        [JsonProperty("last_mod_by", NullValueHandling = NullValueHandling.Ignore)]
        public string LastModBy { get; set; }

        [JsonProperty("last_mod_date", NullValueHandling = NullValueHandling.Ignore)]
        public DateTime? LastModDate { get; set; }

        [JsonProperty("text_header", NullValueHandling = NullValueHandling.Ignore)]
        public string TextHeader { get; set; }

        [JsonProperty("text_sub_header", NullValueHandling = NullValueHandling.Ignore)]
        public string TextSubHeader { get; set; }

        [JsonProperty("text_body", NullValueHandling = NullValueHandling.Ignore)]
        public string TextBody { get; set; }

        [JsonProperty("is_editable", NullValueHandling = NullValueHandling.Ignore)]
        public int? IsEditable { get; set; }

        [JsonProperty("foto", NullValueHandling = NullValueHandling.Ignore)]
        public string Foto { get; set; }

        [JsonProperty("time_start")]
        public DateTime? TimeStart { get; set; }

        [JsonProperty("time_end")]
        public DateTime? TimeEnd { get; set; }

        [JsonProperty("send_time_start")]
        public DateTime? SendTimeStart { get; set; }

        [JsonProperty("send_time_end")]
        public DateTime? SendTimeEnd { get; set; }

        [JsonProperty("receive_qty1_x_mf", NullValueHandling = NullValueHandling.Ignore)]
        public int? receiveQty1xMf { get; set; }
        [JsonProperty("receive_qty2_x_mf", NullValueHandling = NullValueHandling.Ignore)]
        public int? receiveQty2xMf { get; set; }

        [JsonProperty("receive_qty_total", NullValueHandling = NullValueHandling.Ignore)]
        public int? receiveQtyTotal { get; set; }
    }
}
