﻿using LCR600SDK.DataClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LCR600SDK
{
    static class GlobalModel
    {
        private static PlanCoachProfileModel _planCoachProfileModel;
        private static tbl_logsheet_detail _tbl_logsheet_detail;
        private static LoginModel _loginModel;
        private static LcrData _lcrData;
        private static tbl_log _tbl_log;
        private static tbl_setting _tbl_setting;
        private static WhTranfer _WhTranfer;
        private static refueling _AutoRefueling;
        private static RitasiMaintankReceiveClass ritasiMaintankReceiveClass;
        private static ReceivedDirectClass receivedDirectClass;
        private static PenerimaanFtDaratClass penerimaanFtDaratClass;
        private static List<PoRitasiMaintank> _poRitasiMaintank;
        private static PoSendPOTDaratClass _poSendPOTDarat;
        private static List<transportir_truck> _transportir_truck;        
        private static List<WhouseSendFromPortMaintank> _whouseSendFromPortMaintank;
        private static List<WhouseSendToPortMaintank> _whouseSendToPortMaintanks;  
        private static ListDatumRitasiMaintank _listDatumRitasiMaintank;
        private static ListReceivedDirectClass _listReceivedDirectClass;
        private static List<PoReceivedDirectClass> _poReceivedDirectClass;
        
        private static string _app_version;

        public static ListDatumRitasiMaintank ListDatumRitasiMaintank
        {
            get { return _listDatumRitasiMaintank; }
            set { _listDatumRitasiMaintank = value; }
        }

        public static ListReceivedDirectClass ListReceivedDirectClass
        {
            get { return _listReceivedDirectClass; }
            set { _listReceivedDirectClass = value; }
        }

        public static String timezone
        {
            get {
                TimeZoneInfo localZone = TimeZoneInfo.Local;
                var _timezone = localZone.DisplayName.Substring(4, 6);
                var _zone = "WIB";

                if (_timezone == "+08:00")
                    _zone = "WITA";

                return _zone;
            } 
        }
        public static string app_version
        {
            get { return _app_version; }
            set { _app_version = value;  }
        }
        public static List<WhouseSendFromPortMaintank> whouseSendFromPortMaintank
        {
            get { return _whouseSendFromPortMaintank; }
            set { _whouseSendFromPortMaintank = value; }
        }

        public static List<WhouseSendToPortMaintank> whouseSendToPortMaintanks
        {
            get { return _whouseSendToPortMaintanks; }
            set { _whouseSendToPortMaintanks = value; }
        }

        public static List<PoRitasiMaintank> poRitasiMaintank
        {
            get { return _poRitasiMaintank; }
            set { _poRitasiMaintank = value; }
        }
        public static PoSendPOTDaratClass poSendPOTDarat
        {
            get { return _poSendPOTDarat; }
            set { _poSendPOTDarat = value; }
        }
        
        public static List<transportir_truck> transportir_truck
        {
            get { return _transportir_truck; }
            set { _transportir_truck = value; }
        }

        
        public static List<PoReceivedDirectClass> poReceivedDirect
        {
            get { return _poReceivedDirectClass; }
            set { _poReceivedDirectClass = value; }
        }

        

        public static RitasiMaintankReceiveClass ritasiMaintankReceive
        {
            get { return ritasiMaintankReceiveClass; }
            set { ritasiMaintankReceiveClass = value; }
        }

        public static ReceivedDirectClass receivedDirect
        {
            get { return receivedDirectClass; }
            set { receivedDirectClass = value; }
        }

        public static PenerimaanFtDaratClass penerimaanFtDarat
        {
            get { return penerimaanFtDaratClass; }
            set { penerimaanFtDaratClass = value; }
        }

        public static PlanCoachProfileModel GlobalVar
        {
            get { return _planCoachProfileModel; }
            set { _planCoachProfileModel = value; }
        }

        public static tbl_logsheet_detail logsheet_detail
        {
            get { return _tbl_logsheet_detail; }
            set { _tbl_logsheet_detail = value; }
        }

        public static LoginModel loginModel
        {
            get { return _loginModel; }
            set { _loginModel = value; }
        }

        public static LcrData lcrData
        {
            get { return _lcrData; }
            set { _lcrData = value; }
        }

        public static tbl_log logerror
        {
            get { return _tbl_log; }
            set { _tbl_log = value; }
        }

        public static tbl_setting settingWhouse
        {
            get { return _tbl_setting; }
            set { _tbl_setting = value; }
        }

        public static WhTranfer whTransfer
        {
            get { return _WhTranfer; }
            set { _WhTranfer = value; }
        }

        public static refueling AutoRefueling
        {
            get { return _AutoRefueling; }
            set { _AutoRefueling = value; }
        }
    }
}
