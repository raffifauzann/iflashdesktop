﻿using Flurl.Http;
using LCR600SDK.DataClass;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LCR600SDK
{
    public partial class FRefPOSendPOT : Form
    {
        private PoSendPOTDaratClass _dataResult;
        private string pUrl_service = string.Empty;
        public string activity = string.Empty;

        public FRefPOSendPOT()
        {
            InitializeComponent();
        }

        private void FRefPOSendPOT_Load(object sender, EventArgs e)
        {
            var _proper = Properties.Settings.Default;
            pUrl_service = _proper.url_service;
            loadGridAsync(); 
        }

        public async void loadGridAsync()
        {
            string URL_API = $"{pUrl_service}api/FuelService/getDeliveryDaratSend?page=1&pageSize=100000&search="; 
            var _auth = GlobalModel.loginModel;
            try
            {
                var _data = await URL_API.WithHeaders(new { Authorization = _auth.token }).GetJsonAsync<PoSendPOTDaratClass>();
                //var res = await URL_API.WithHeaders(new { Authorization = _auth.token }).GetStringAsync();
                //var _data = PoSendPOTDarat.FromJson(res);

                GlobalModel.poSendPOTDarat = _data;                
                DataTable dt = GeneralFunc.ToDataTable(GlobalModel.poSendPOTDarat.ListData);
                dataGridView1.DataSource = dt; 
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        void setPaging()
        {
            try
            {
                var _data = GlobalModel.ritasiMaintankReceive;
                //txtPage.Text = $"{_data.CurrentPage}";
                //lblPageOf.Text = $"Page {_data.CurrentPage} of {_data.TotalPage}    - Total rows {_data.TotalSize}";
                //btnNext.Enabled = (_data.TotalPage > _data.CurrentPage);
                //btnPrev.Enabled = (_data.CurrentPage > 1);
            }
            catch (Exception)
            {
                MessageBox.Show("Tidak ada data untuk di tampilan", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private async void txtSearch_TextChanged(object sender, EventArgs e)
        {
            //var _list = GlobalModel.poSendPOTDarat.ListData;
            //_dataResult.ListData = _list.Where(d => d.PoNo.Contains(txtSearch.Text.Trim().ToUpper())).ToList();
            //DataTable dt = GeneralFunc.ToDataTable(_dataResult.ListData);
            //dataGridView1.DataSource = dt;
            string URL_API = $"{pUrl_service}api/FuelService/getDeliveryDaratSend?page=1&pageSize=100000&search={txtSearch.Text.Trim().ToUpper()}";
            var _auth = GlobalModel.loginModel;
            try
            {
                var _data = await URL_API.WithHeaders(new { Authorization = _auth.token }).GetJsonAsync<PoSendPOTDaratClass>();
                //var res = await URL_API.WithHeaders(new { Authorization = _auth.token }).GetStringAsync();
                //var _data = PoSendPOTDarat.FromJson(res);

                GlobalModel.poSendPOTDarat = _data;
                DataTable dt = GeneralFunc.ToDataTable(GlobalModel.poSendPOTDarat.ListData);
                dataGridView1.DataSource = dt;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.Columns[e.ColumnIndex].Name == "Action")
            { 
                if (Application.OpenForms["FSendPOTForm"] != null)
                { 
                    var _from = (Application.OpenForms["FSendPOTForm"] as FSendPOTForm);
                    _from.txtPoNo.Text = GlobalModel.poSendPOTDarat.ListData[e.RowIndex].PoNo;
                    _from.txtDistrik.Text = GlobalModel.poSendPOTDarat.ListData[e.RowIndex].District;
                    _from.pTblRef = "tbl_t_fuel_delivery_darat";
                    _from.pPidPO = GlobalModel.poSendPOTDarat.ListData[e.RowIndex].Pid;
                    _from.pReceivedAt = "";
                }
                this.Close();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
        }      
    }
}
