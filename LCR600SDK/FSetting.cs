﻿using MySql.Data.MySqlClient; using System.Data.SQLite; using System.IO; using System.Reflection;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LCR600SDK
{
    public partial class FSetting : Form
    {
        private string myConnectionString = string.Empty;
        PasswordEncrypDecrypt decryptor = new PasswordEncrypDecrypt();
        public FSetting()
        {
            InitializeComponent();
        }

        private void FSetting_Load(object sender, EventArgs e)
        {
            var _proper = Properties.Settings.Default;
            //myConnectionString = $"server={_proper.db_server};uid={_proper.db_username};pwd={decryp};database={_proper.db_dbase}";
            var passPhrase = "iFlashDesktop";
            var decryp = decryptor.DecryptPassword(_proper.db_pass, passPhrase);
            myConnectionString = $"server={_proper.db_server};uid={_proper.db_username};pwd={decryp};database={_proper.db_dbase}";
        }


        void loadGrid()
        {
            String iQuery = "SELECT * FROM flowmeter order by unitNo"; 
            MySqlConnection conn = new MySqlConnection(myConnectionString);
            MySqlCommand cmd = new MySqlCommand(iQuery, conn);

            if (conn.State != ConnectionState.Open) conn.Open();

            DataTable dataTable = new DataTable();
            MySqlDataAdapter da = new MySqlDataAdapter(cmd);

            da.Fill(dataTable);

            //dataGridView1.DataSource = dataTable;

            if (conn.State == ConnectionState.Open)
                if (conn.State == ConnectionState.Open) conn.Close();
        } 

        static DataTable ConvertListToDataTable(List<string[]> list)
        {
            // New table.
            DataTable table = new DataTable();

            // Get max columns.
            int columns = 0;
            foreach (var array in list)
            {
                if (array.Length > columns)
                {
                    columns = array.Length;
                }
            }

            // Add columns.
            for (int i = 0; i < columns; i++)
            {
                table.Columns.Add();
            }

            // Add rows.
            foreach (var array in list)
            {
                table.Rows.Add(array);
            }

            return table;
        }
    }
}
