﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LCR600SDK
{
    class ClsResult
    {
        public byte rc { get; set; }
        public bool issuccess { get; set;}
        public string value { get; set; }
    }
}
