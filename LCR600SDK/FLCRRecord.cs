﻿using SDK;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LCR600SDK
{
    public partial class FLCRRecord : Form
    {
        public double LitersQty = 0.0;
        public float qty = -1;
        private string myConnectionString = string.Empty;
        private byte device = 0;
        private int stepTimerOpen = 0;
        private int stepTimerClose = 0;
        PasswordEncrypDecrypt decryptor = new PasswordEncrypDecrypt();

        public FLCRRecord()
        {
            InitializeComponent();
        }

        private void FLCRRecord_Load(object sender, EventArgs e)
        {
            var _proper = Properties.Settings.Default;
            //myConnectionString = $"server={_proper.db_server};uid={_proper.db_username};pwd={decryp};database={_proper.db_dbase}";
            var passPhrase = "iFlashDesktop";
            var decryp = decryptor.DecryptPassword(_proper.db_pass, passPhrase);
            myConnectionString = $"server={_proper.db_server};uid={_proper.db_username};pwd={decryp};database={_proper.db_dbase}";
            txtLoad();
            txtRefStart.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            stepTimerOpen = 0;
             
            doPrintLCR();
            doCloseLCR();

            timerQty.Enabled = false;
            timerClose.Enabled = false;
            timerOpen.Enabled = true;

            lblAlertClosed.Visible = false;
        }

        private bool open()
        {
            return true;
        }

        private bool start()
        {
            return true;
        }

        private bool print()
        {
            return true;
        }

        private bool closed()
        {
            return true;
        }

        void txtLoad()
        {
            txtGrossQty.Text = "";
            txtTotalizerGross.Text = "";
            txtTotalizerStart.Text = "";
            txtRefStart.Text = "";
            txtRefStop.Text = "";
        }

        private string doStartLCR()
        {
            string strStatus = "PLEASE WAIT..";

            if (device == 0)
                return strStatus;

            byte rc = LCP02API.LCP02IssueCommand(device, (byte)LCP02API.LCR_COMMANDS.LCRC_RUN,
                                                         (byte)LCP02API.LCR_RETURN_CODES.LCRM_NO_WAIT);

            if (rc == (byte)LCPAPI.LCPR.LCPR_OK)
                strStatus = "RUN";

            return strStatus;
        }

        private string doPrintLCR()
        {
            string strStatus = "PLEASE WAIT..";

            if (device == 0)
                return strStatus;


            byte rc = LCP02API.LCP02IssueCommand(device, (byte)LCP02API.LCR_COMMANDS.LCRC_PRINT,
                                                         (byte)LCP02API.LCR_RETURN_CODES.LCRM_NO_WAIT);

            if (rc == (byte)LCPAPI.LCPR.LCPR_OK)
                strStatus = "STOP";

            return strStatus;
        }

        private bool doCloseLCR()
        {
            byte rc = LCP02API.LCP02Close();
            if (rc != (byte)LCPAPI.LCPR.LCPR_OK)
            {
                //MessageBox.Show("LCP02Close error, rc =" + rc)
                Console.WriteLine("LCP02Close error, rc =" + rc);
                return false;
            }
            return true;
        }

        private void timerOpen_Tick(object sender, EventArgs e)
        {
            if (stepTimerOpen == 1)
            {
                if (start())
                {
                    doStartLCR();
                    timerOpen.Enabled = false;
                    timerQty.Enabled = true;
                }
            }

            if (stepTimerOpen == 0)
            {
                if (open())
                {
                    doOpenLCR();
                    stepTimerOpen = 1;
                }
            }
        }

        private void timerQty_Tick(object sender, EventArgs e)
        {
            getData();
        }

        private bool getData()
        {
            ClsLcrRequest clsLcrRequest = new ClsLcrRequest(0, false);
            try
            { 

                clsLcrRequest = getGrossQty(float.Parse(txtGrossQty.Text));
                txtGrossQty.Text = clsLcrRequest.Qty.ToString();

                clsLcrRequest = getGrossTotalizer(float.Parse(txtTotalizerGross.Text));
                txtTotalizerGross.Text = clsLcrRequest.Qty.ToString();

                if (txtTotalizerGross.Text.ToUpper() != "0" && txtTotalizerStart.Text.Trim() == "")
                {
                    txtTotalizerStart.Text = txtTotalizerGross.Text;
                }

                if (clsLcrRequest.Status == false)
                {
                    doCloseLCR();
                    Thread.Sleep(500);
                    doOpenLCR();
                    return false;
                } 

                saveLog();

                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        private void saveLog()
        {
            string datetimeNow = DateTime.Now.ToString("yyyyMMdd_HHmmss");
            string fileName = @"C:\Lab\log_Qty_RefuellingTruck_" + datetimeNow + ".txt";

            try
            {
                var _item = GlobalModel.logsheet_detail;
                var initData = GlobalModel.GlobalVar;
                DateTime dt = DateTime.Parse(DateTime.Now.ToString());

                // Check if file already exists. If yes, delete it.     
                if (File.Exists(fileName))
                {
                    File.Delete(fileName);
                }

                // Create a new file     
                using (StreamWriter sw = File.CreateText(fileName))
                {
                    //sw.WriteLine("HM " + _item.hm.ToString());
                    //sw.WriteLine("UnitNo " + _item.unit_no.ToString());
                    //sw.WriteLine("NamaOpr " + _item.nama_operator.ToString());
                    sw.WriteLine("grossQty " + txtGrossQty.Text.ToString());
                    sw.WriteLine("grossTotalizerStart " + txtTotalizerStart.Text.ToString());
                    sw.WriteLine("grossTotalizerEnd " + txtTotalizerGross.Text.ToString());
                    sw.WriteLine("RefStart " + txtRefStart.Text.ToString());
                    sw.WriteLine("RefStop " + dt.ToString("HH:mm:ss"));
                    sw.WriteLine("ModDate " + DateTime.Now);
                    sw.WriteLine("ModBy " + initData.DataEmp.Nrp);
                }

                // Write file contents on console.     
                using (StreamReader sr = File.OpenText(fileName))
                {
                    string s = "";
                    while ((s = sr.ReadLine()) != null)
                    {
                        Console.WriteLine(s);
                    }
                }
            }
            catch (Exception Ex)
            {
                Console.WriteLine(Ex.ToString());
            }
        }

        private ClsLcrRequest getGrossTotalizer(float lastQty)
        {
            byte rc;
            string grossTotalizerStr = lastQty.ToString();
            double grossQty = 0;

            if (device == 0)
            {
                return new ClsLcrRequest(lastQty, false);
            }

            byte fieldNum = (byte)LCP02API.LCR_FIELD_NUMBERS.LCRF_GrossTotal_WM;
            int size = LCP02API.LCP02FieldSize(fieldNum);
            IntPtr grossTotalizerPtr = Marshal.AllocHGlobal((int)size);

            byte devStatus;

            rc = LCP02API.LCP02GetField(device, fieldNum, grossTotalizerPtr, out devStatus,
                                        (byte)LCP02API.LCR_RETURN_CODES.LCRM_NO_WAIT);

            if (rc != (byte)LCPAPI.LCPR.LCPR_OK)
            {
                return new ClsLcrRequest(lastQty, false);
            }

            string strGrossTotalizer = Marshal.PtrToStringAnsi(grossTotalizerPtr, (int)size);
            LCP02API.FloatByteArray fbaGross;
            byte[] strGrossArr = null;

            fbaGross = new LCP02API.FloatByteArray();
            strGrossArr = Encoding.GetEncoding(1252).GetBytes(strGrossTotalizer);
            fbaGross.Byte1 = strGrossArr[0];
            fbaGross.Byte2 = strGrossArr[1];
            fbaGross.Byte3 = strGrossArr[2];
            fbaGross.Byte4 = strGrossArr[3];


            strGrossTotalizer = fbaGross.int32Value.ToString();
            if (strGrossTotalizer != "")
            {
                grossQty = Convert.ToDouble(strGrossTotalizer);
                grossQty = grossQty / 10;
            }

            grossTotalizerStr = grossQty.ToString();

            return new ClsLcrRequest(grossQty, true);
        }

        private ClsLcrRequest getGrossQty(float lastQty)
        {
            string grossQtyStr = lastQty.ToString();
            double grossQty = 0;
            try
            {

                byte rc;

                if (device == 0)
                {
                    return new ClsLcrRequest(lastQty, false);
                }

                byte fieldNum = (byte)LCP02API.LCR_FIELD_NUMBERS.LCRF_GrossQty_NE;
                int size = LCP02API.LCP02FieldSize(fieldNum);
                IntPtr grossQtyPtr = Marshal.AllocHGlobal((int)size);

                byte devStatus;

                rc = LCP02API.LCP02GetField(device, fieldNum, grossQtyPtr, out devStatus,
                                            (byte)LCP02API.LCR_RETURN_CODES.LCRM_NO_WAIT);

                if (rc != (byte)LCPAPI.LCPR.LCPR_OK)
                {
                    return new ClsLcrRequest(lastQty, false);
                }

                string strGrossQty = Marshal.PtrToStringAnsi(grossQtyPtr, (int)size);
                LCP02API.FloatByteArray fbaGross;
                byte[] strGrossArr = null;

                fbaGross = new LCP02API.FloatByteArray();
                strGrossArr = Encoding.GetEncoding(1252).GetBytes(strGrossQty);
                fbaGross.Byte1 = strGrossArr[0];
                fbaGross.Byte2 = strGrossArr[1];
                fbaGross.Byte3 = strGrossArr[2];
                fbaGross.Byte4 = strGrossArr[3];

                if (lastQty > 0)
                {
                    if (fbaGross.int32Value <= 0)
                    {
                        grossQty = Convert.ToDouble(lastQty.ToString());
                    }
                    else
                    {
                        strGrossQty = fbaGross.int32Value.ToString();
                        if (strGrossQty != "")
                        {
                            grossQty = Convert.ToDouble(strGrossQty);
                            grossQty = grossQty / 10;
                        }
                        else
                        {
                            grossQty = Convert.ToDouble(strGrossQty);
                        }
                    }
                }
                else
                {
                    strGrossQty = fbaGross.int32Value.ToString();
                    if (strGrossQty != "")
                    {
                        grossQty = Convert.ToDouble(strGrossQty);
                        grossQty = grossQty / 10;
                    }
                    else
                    {
                        grossQty = 0;
                    }
                }

                grossQtyStr = grossQty.ToString();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return new ClsLcrRequest(grossQty, true);
        }

        private void doOpenLCR()
        {
            try
            {
                device = doConnectLCR();
                if (device > 0)
                {
                    txtGrossQty.Text = getGrossQty(qty).Qty.ToString();
                    txtTotalizerGross.Text = getGrossTotalizer(qty).Qty.ToString();
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        private byte doConnectLCR()
        {
            byte device = 250;
            byte numDevices;
            byte[] deviceList = new byte[device];
            IntPtr deviceListPtr = Marshal.AllocHGlobal(250);
            byte rc = LCP02API.LCP02Open(device, device, out numDevices, deviceListPtr);
            if (rc != (byte)LCPAPI.LCPR.LCPR_OK && rc != (byte)LCP02API.LCR_RETURN_CODES.LCP02Ra_ALREADYOPENED)
            {
                device = 0;
            }
            return device;
        }

        private void FLCRRecord_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                //btnStop.Enabled = false;
                lblAlertClosed.Visible = true;
                doPrintLCR();
                doCloseLCR();
                stepTimerClose = 0;
                txtRefStop.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                timerClose.Enabled = true;
                timerOpen.Enabled = false;
                timerQty.Enabled = false;
            }
            catch (Exception)
            {

            }
        }

        private void timerClose_Tick(object sender, EventArgs e)
        {
            if (stepTimerClose == 0)
            {
                if (getData())
                {
                    txtGrossQty.Visible = true;
                    this.LitersQty = Convert.ToDouble(txtGrossQty.Text.ToString());
                    tryDataOk();
                    this.DialogResult = DialogResult.OK;
                }
            }
        }

        void tryDataOk()
        {
            try
            {

                bool isFailed = true;
                while (isFailed)
                {
                    if (txtGrossQty.Text.Trim().ToUpper() == "WAIT..")
                    {
                        getData();
                        Thread.Sleep(100);
                    }
                    else
                        isFailed = false;
                }
                //saveData();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

        }
    }
}
