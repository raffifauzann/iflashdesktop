﻿namespace LCR600SDK
{
    partial class FSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FSetting));
            this.label1 = new System.Windows.Forms.Label();
            this.cbFlowMeter = new System.Windows.Forms.ComboBox();
            this.btnPilih = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 26);
            this.label1.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Flow Meter";
            // 
            // cbFlowMeter
            // 
            this.cbFlowMeter.FormattingEnabled = true;
            this.cbFlowMeter.Location = new System.Drawing.Point(97, 24);
            this.cbFlowMeter.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
            this.cbFlowMeter.Name = "cbFlowMeter";
            this.cbFlowMeter.Size = new System.Drawing.Size(186, 21);
            this.cbFlowMeter.TabIndex = 1;
            // 
            // btnPilih
            // 
            this.btnPilih.Location = new System.Drawing.Point(201, 149);
            this.btnPilih.Name = "btnPilih";
            this.btnPilih.Size = new System.Drawing.Size(75, 23);
            this.btnPilih.TabIndex = 20;
            this.btnPilih.Text = "Simpan";
            this.btnPilih.UseVisualStyleBackColor = true;
            // 
            // FSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(300, 189);
            this.Controls.Add(this.btnPilih);
            this.Controls.Add(this.cbFlowMeter);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FSetting";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Setting";
            this.Load += new System.EventHandler(this.FSetting_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbFlowMeter;
        private System.Windows.Forms.Button btnPilih;
    }
}