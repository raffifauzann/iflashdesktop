﻿using LCR600SDK.DataClass;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LCR600SDK
{
    public partial class FTools : Form
    {
        public FTools()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            GetPropertyValues(new WhouseSendToPortMaintank(), "WhouseSendToPortMaintank");
        }

        private static void GetPropertyValues(Object obj, String sTableName)
        {
            Type t = obj.GetType();
            Console.WriteLine("Type is: {0}", t.Name);
            PropertyInfo[] props = t.GetProperties();
            Console.WriteLine("Properties (N = {0}):",
                              props.Length);
            Console.WriteLine($" CREATE TABLE {sTableName} (");
            foreach (var prop in props)
            {
                var _type = string.Empty; 
                switch (prop.PropertyType.Name)
                {
                    case "String":
                        _type = "Varchar(50)";
                        break;
                    case "Double":
                        _type = "Float";
                        break;
                    case "Int64":
                        _type = "INT";
                        break;
                    default:
                        _type = prop.PropertyType.Name;
                        break;
                }

                Console.WriteLine("   {0} {1},", prop.Name,
                                  _type);
            }

            Console.WriteLine($")");
        }
    }
}
