﻿using Flurl.Http;
using LCR600SDK.DataClass;
using MySql.Data.MySqlClient;
using System.Data.SQLite;
using System.IO;
using System.Reflection;
using System.IO;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Text;
using System.Data;
using System.Diagnostics;

namespace LCR600SDK
{
    public partial class FSendPOTForm : Form
    {
        public ListDatumRitasiMaintank pData;
        private List<transportir_truck> _dataResult;
        private string myConnectionString = string.Empty;
        public string whosueSelected = string.Empty;
        private string pShiftStart1 = string.Empty;
        private string pShiftStart2 = string.Empty;
        private string pWHouse = string.Empty;
        private string pWhouseName = string.Empty;
        private string pSnCOde = string.Empty;
        public string URL_API_CALL = string.Empty;
        public string CALL_FROM = string.Empty;
        SQLiteConnection conn;
        private string pUrl_service = string.Empty;
        public string pTblRef = string.Empty;
        public string pPidPO = string.Empty;
        public string pReceivedAt = string.Empty;
        public string pReceivedAtCode = string.Empty;
        public string pSendFromId = string.Empty;
        public string pSendFromName = string.Empty;
        public string meterFaktor = string.Empty;
        PasswordEncrypDecrypt decryptor = new PasswordEncrypDecrypt();
        WriteLogText log = new WriteLogText();
        public FSendPOTForm()
        {
            var _proper = Properties.Settings.Default;
            var passPhrase = "iFlashDesktop";
            var decryp = decryptor.DecryptPassword(_proper.db_pass, passPhrase);
            myConnectionString = "Data Source=" + Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "SQLite", "db_iflash.db") + " ;New=False;";
            this.Text = "Pengiriman POT Form" + GlobalModel.app_version;
            //myConnectionString = "Data Source=" + Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "SQLite", "db_iflash.db") + " ;New=False;";
            GlobalModel.AutoRefueling = new refueling();
            conn = new SQLiteConnection(myConnectionString);
            InitializeComponent();
        }

        public async void getTransportirTruck()
        {
            string URL_API = $"{pUrl_service}api/FuelService/GetTransportirTruck";
            var _auth = GlobalModel.loginModel;
            try
            {
                var _data = await URL_API.WithHeaders(new { Authorization = _auth.token }).GetJsonAsync<transportir_truck>();
                //var res = await URL_API.WithHeaders(new { Authorization = _auth.token }).GetStringAsync<transportir_truck>();
                //var _data = transportir_truck.FromJson(res);

                GlobalModel.transportir_truck = _data.ListData;                
                foreach (var item in GlobalModel.transportir_truck)
                {
                    if (item.DSTRCT_CODE == GlobalModel.GlobalVar.DataEmp.DstrctCode && item.STATUS == "true")
                    {
                        comboBoxTransportirTruck.Items.Add(item.FT_NUMBER);
                    }                    
                }
                
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void FSendPOTForm_Load(object sender, EventArgs e)
        {
            var _proper = Properties.Settings.Default;
            pUrl_service = _proper.url_service;
            pData = new ListDatumRitasiMaintank();
            initDataMaster();
            getTransportirTruck();
            initData();

            var _globalData = GlobalModel.GlobalVar;
            var dstrctCode = _globalData.DataEmp.DstrctCode;

            SQLiteConnection conn;
            conn = new SQLiteConnection(myConnectionString);
            if (conn.State != ConnectionState.Open) conn.Open();
            string query = string.Empty;
            query = "SELECT transportir_name FROM transportir where district = '" + dstrctCode + "'";
            SQLiteCommand cmd = new SQLiteCommand(query, conn);
            SQLiteDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                comboBoxTransportir.Items.Add(reader["transportir_name"]);
                loadTransportir();
            }
            if (txtSendQty.Text == "0")
            {
                btnSubmit.Enabled = false;
            }
            reader.Dispose();
            if (conn.State == ConnectionState.Open) conn.Close();
        }

        void loadTransportir()
        {
            try
            {
                string transportirName = "";
                SQLiteConnection conn;

                conn = new SQLiteConnection(myConnectionString);
                if (conn.State != ConnectionState.Open) conn.Open();
                string query = string.Empty;

                query = "SELECT * FROM transportir";
                SQLiteCommand cmds = new SQLiteCommand(query, conn);
                SQLiteDataReader reader = cmds.ExecuteReader();

                while (reader.Read())
                {
                    transportirName = reader["transportir_name"].ToString();
                    comboBoxTransportir.SelectedItem = transportirName;
                }
                reader.Dispose();
                if (conn.State == ConnectionState.Open) conn.Close();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
                MessageBox.Show("Exception: Terjadi kesalahan saat get data transportir, mohon cek log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(ex.Message, "loadTransportir()");
            }

        }

        void save_error_log(string data, string actions)
        {
            try
            {
                DateTime localDate = DateTime.Now;
                var _item = new tbl_log();
                var initData = GlobalModel.GlobalVar;

                _item.pid = System.Guid.NewGuid().ToString();
                _item.actions = $"District {initData.DataEmp.DstrctCode}/FSendFTPortForm/{actions}";
                _item.message = $"{data}";
                _item.mod_date = localDate;
                _item.mod_by = initData.DataEmp.Nrp;

                SQLiteConnection conn;

                conn = new SQLiteConnection(myConnectionString);
                if (conn.State != ConnectionState.Open) conn.Open();
                string query = string.Empty;
                query = "INSERT INTO tbl_log(pid,actions,message,mod_date,mod_by)"
                      + " VALUES(@pid,@actions,@message,@mod_date,@mod_by)";

                SQLiteCommand cmd = new SQLiteCommand(query, conn);

                cmd.Parameters.Add(new SQLiteParameter("@pid", _item.pid));
                cmd.Parameters.Add(new SQLiteParameter("@actions", _item.actions));
                cmd.Parameters.Add(new SQLiteParameter("@message", _item.message));
                cmd.Parameters.Add(new SQLiteParameter("@mod_date", _item.mod_date));
                cmd.Parameters.Add(new SQLiteParameter("@mod_by", _item.mod_by));
                cmd.ExecuteNonQuery();
                if (conn.State == ConnectionState.Open) conn.Close();
            }
            catch (Exception)
            {
            }
        }

        void initData()
        {
            txtPoNo.Text = "";
            txtDistrik.Text = "";
            //txtTranportir.Text = "";
            txtDo.Text = "";
            txtFt.Text = "";
            txtSendDriverName.Text = "";
            txtReceiveDriverName.Text = "";
            txtSendDate.Text = "";
            txtSendQty.Text = "0";            
            txtReceiveAt.Text = "";
            txtSendfmAwal.Text = "0";
            txtSendfmAkhir.Text = "0";
            txtSegel1.Text = "";
            txtSegel2.Text = "";
            txtSegel3.Text = "";
            txtSegel4.Text = "";
            txtSegel5.Text = "";
            txtSegel6.Text = "";
            txtSegel7.Text = "";
            txtSegel8.Text = "";
            txtSegel9.Text = "";
            txtSegel10.Text = "";
            txtSendDate.Text = DateTime.Now.ToString();
        }

        void initDataMaster()
        {
            try
            {
                if (conn.State != ConnectionState.Open) conn.Open();
                string query = string.Empty;

                query = "SELECT * FROM tbl_setting";
                SQLiteCommand cmds = new SQLiteCommand(query, conn);
                SQLiteDataReader reader = cmds.ExecuteReader();

                while (reader.Read())
                {
                    pWHouse = reader["Whouseid"].ToString();
                    pWhouseName = reader["WhouseName"].ToString();
                    pSnCOde = reader["SnCode"].ToString();
                    txtSendFrom.Text = pWhouseName;
                    txtSnFlowMeter.Text = reader["SnCode"].ToString();
                    meterFaktor = reader["MeterFakor"].ToString();
                }
                reader.Dispose();
                if (conn.State == ConnectionState.Open) conn.Close();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
                MessageBox.Show("Exception: Terjadi kesalahan saat initialisasi data, mohon cek log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(ex.Message, "initData()");
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnTerima_Click(object sender, EventArgs e)
        {
            kirimAsync();
        }

        async Task kirimAsync()
        {
            var _confirm = MessageBox.Show("Anda yakin data sudah benar?", "Confirm submit!!", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (_confirm == DialogResult.Yes && txtSendQty.Text == "0")
            {
                if (isValid())
                {
                    setData();
                    //btnSubmit.Enabled = true;
                    var f = new FmManual();
                    f.preset = GlobalModel.AutoRefueling.preset_gross * 10;

                    f.trans_type_select = "PORT_SEND";
                    GlobalModel.AutoRefueling.trans_type_select = f.trans_type_select;
                    f.waitCounterMax = 3 * 10;
                    f.ListDatumRitasiMaintank = pData;
                    if (f.ShowDialog() == DialogResult.OK)
                    {
                        txtSendQty.Text = Convert.ToString(f.quantity);
                        txtSendfmAwal.Text = Convert.ToString(f.totalizer_awal);
                        txtSendfmAkhir.Text = Convert.ToString(f.totalizer_akhir);
                        btnSubmit.Enabled = true;
                    }
                    else
                    {
                        txtSendQty.Text = Convert.ToString(f.quantity);
                        txtSendfmAwal.Text = Convert.ToString(f.totalizer_awal);
                        txtSendfmAkhir.Text = Convert.ToString(f.totalizer_akhir);
                    }

                    //var f = new Form2();
                    //f.TRANS_TYPE_SELECT = "PORT_SEND";
                    //f.ListDatumRitasiMaintank = pData;

                    ////f.ShowDialog();
                    //if (f.ShowDialog() == DialogResult.OK)
                    //{
                    //    txtSendQty.Text = Convert.ToString(f.LitersQty);
                    //    txtSendfmAwal.Text = Convert.ToString(f.LitersTotalizerStart);
                    //    txtSendfmAkhir.Text = Convert.ToString(f.LitersTotalizerEnd);
                    //    _backToCallForm();
                    //}
                    //else
                    //{
                    //    txtSendQty.Text = Convert.ToString(f.LitersQty);
                    //    txtSendfmAwal.Text = Convert.ToString(f.LitersTotalizerStart);
                    //    txtSendfmAkhir.Text = Convert.ToString(f.LitersTotalizerEnd);
                    //}
                    //submitData();
                }
            }
            else if (_confirm == DialogResult.Yes && txtSendQty.Text != "0")
            {
                setData();
                var _auth = GlobalModel.loginModel;
                String _url = $"{pUrl_service}api/FuelService/sendRitasiToMaintank";
                returnValue data = await _url.WithHeaders(new { Authorization = _auth.token })
                       .PostJsonAsync(pData).ReceiveJson<returnValue>();

                if (data.status)
                    _backToCallForm();
                else
                    MessageBox.Show(data.remarks, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        void setData()
        {
            var _globalData = GlobalModel.GlobalVar;
            int isComplete = chkConfirm.Checked ? 1 : 0;
            string selectedItemTransportir = this.comboBoxTransportir.GetItemText(this.comboBoxTransportir.SelectedItem);
            String docStatus = "51";
            pData.PidRitasiMaintank = System.Guid.NewGuid().ToString();
            pData.TransportirCode = "";            
            pData.TransportirName = selectedItemTransportir.ToString();
            pData.SendPoNo = txtPoNo.Text;
            pData.District = txtDistrik.Text;
            pData.SendDoNo = txtDo.Text;
            pData.SendFtNo = txtFt.Text;
            pData.SendDriverName = txtSendDriverName.Text;
            pData.SendDate = DateTime.Now;
            pData.SendQty = (int)Math.Round(double.Parse(txtSendQty.Text.Trim(), new System.Globalization.CultureInfo("id-ID")));
            pData.SendQtyXMf = (int)Math.Round(double.Parse(txtSendQty.Text.Trim(), new System.Globalization.CultureInfo("id-ID")));
            pData.SendFrom = pReceivedAt;
            pData.SendFromCode = pWHouse;
            pData.SendTo = txtReceiveAt.Text;
            pData.SendToCode = pReceivedAtCode;            
            pData.SendBbn = 0;
            pData.SendDippingHole1Port = double.Parse(txtReceiveDippingHole1Mt.Text);
            pData.SendDippingHole2Port = double.Parse(txtReceiveDippingHole2Mt.Text);
            pData.LastModBy = _globalData.DataEmp.Nrp;            
            pData.RefTbl = pTblRef;
            pData.DocStatus = docStatus;
            pData.RefId = pPidPO;
            pData.SendType = "port";
            pData.SendIscomplete = isComplete;
            pData.SendsnFlowMeter = txtSnFlowMeter.Text;
            pData.SendMeterFaktor = Math.Round(double.Parse(meterFaktor.Replace(',', '.').Trim(), new System.Globalization.CultureInfo("id-ID")));
            pData.SendFlowMeterAwal = (int)Math.Round(double.Parse(txtSendfmAwal.Text.Trim(), new System.Globalization.CultureInfo("id-ID")));// int.Parse(txtSendfmAwal.Text);
            pData.SendFlowMeterAkhir = (int)Math.Round(double.Parse(txtSendfmAkhir.Text.Trim(), new System.Globalization.CultureInfo("id-ID")));// int.Parse(txtSendfmAkhir.Text);
            pData.SendNoSegel1 = txtSegel1.Text.Trim();
            pData.SendNoSegel2 = txtSegel2.Text.Trim();
            pData.SendNoSegel3 = txtSegel3.Text.Trim();
            pData.SendNoSegel4 = txtSegel4.Text.Trim();
            pData.SendNoSegel5 = txtSegel5.Text.Trim();
            pData.SendNoSegel6 = txtSegel6.Text.Trim();
            pData.SendNoSegel7 = txtSegel7.Text.Trim();
            pData.SendNoSegel8 = txtSegel8.Text.Trim();
            pData.SendNoSegel9 = txtSegel9.Text.Trim();
            pData.SendNoSegel10 = txtSegel10.Text.Trim();
            pData.LastModBy = _globalData.Gl[0].Nrp;
            pData.Foto = string.Empty;

            //test
            txtSendQty.Text = "10";
            txtSendfmAwal.Text = "0";
            txtSendfmAkhir.Text = "10";
            pData.SendQty = 10;            
            pData.SendQtyXMf = pData.SendQty * (int)pData.SendMeterFaktor;
            pData.SendFlowMeterAwal = 0;
            pData.SendFlowMeterAkhir = 10;
            pData.SendTimeStart = DateTime.Now;
            pData.SendTimeEnd = DateTime.Now;
        }

        bool isValid()
        {
            return true;
        }

        //async void submitData()
        //{
        //    var _auth = GlobalModel.loginModel;
        //    String _url = $"{pUrl_service}api/FuelService/sendRitasiToMaintank";

        //    try
        //    {
        //        returnValue data = await _url.WithHeaders(new { Authorization = _auth.token })
        //               .PostJsonAsync(pData).ReceiveJson<returnValue>();

        //        if (data.status)
        //        {
        //            _backToCallForm();
        //        }
        //        else
        //            MessageBox.Show(data.remarks, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

        //    }
        //    catch (Exception ex)
        //    {
        //        //MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        MessageBox.Show("Exception: Terjadi kesalahan saat submit data, mohon cek log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        log.WriteToFile("logLCR600.txt", ex.Message);
        //        save_error_log(ex.Message, "submitData()");
        //    }
        //}

        public void _backToCallForm()
        {
            switch (CALL_FROM)
            {
                case "RECV_DARAT":
                    if (Application.OpenForms["FReceivedFTDaratSub"] != null)
                        (Application.OpenForms["FReceivedFTDaratSub"] as FReceivedFTDaratSub).loadGridAsync(URL_API_CALL);
                    break;
                case "RECV_PORT":
                    if (Application.OpenForms["FReceivedFTPort"] != null)
                        (Application.OpenForms["FReceivedFTPort"] as FReceivedFTPort).loadGridAsync(URL_API_CALL);
                    break;
                case "SEND_PORT":
                    if (Application.OpenForms["FSendFTPort"] != null)
                        (Application.OpenForms["FSendFTPort"] as FSendFTPort).loadGridAsync(URL_API_CALL);
                    break;
                case "SEND_DARAT":
                    if (Application.OpenForms["FSendPOT"] != null)
                        (Application.OpenForms["FSendPOT"] as FSendPOT).loadGridAsync(URL_API_CALL);
                    break;
                    
            }

            this.Close();
        }

        public void cbReceiveAt_SelectedIndexChanged(String _itemSelect)
        {
            try
            {
                var _globalData = GlobalModel.GlobalVar;

                SQLiteConnection conn;
                conn = new SQLiteConnection(myConnectionString);
                if (conn.State != ConnectionState.Open) conn.Open();
                string query = string.Empty;
                query = $"SELECT * FROM flowmeter where DstrctCode = '{ _globalData.DataEmp.DstrctCode}' and Warehouse ='{_itemSelect}' order by SnCode";
                SQLiteCommand cmd = new SQLiteCommand(query, conn);
                SQLiteDataReader reader = cmd.ExecuteReader();

                List<ListItem> items = new List<ListItem>();
                items.Add(new ListItem("", "[Pilih]"));
                while (reader.Read())
                {
                    items.Add(new ListItem(reader["SnCode"].ToString(), reader["SnCode"].ToString()));
                }

                cbSnCode.DataSource = items;
                cbSnCode.ValueMember = "value";
                cbSnCode.DisplayMember = "text";
                reader.Dispose();
                if (conn.State == ConnectionState.Open) conn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void btnSendFrom_Click(object sender, EventArgs e)
        {
            if (txtDistrik.Text.Trim().Length == 0)
            {
                MessageBox.Show("PO belum di pilih", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                FRefSendPortFrom f = new FRefSendPortFrom();
                f.pPO_NO = txtPoNo.Text;
                f.ShowDialog();
            }
        }

        private void btnSendTo_Click(object sender, EventArgs e)
        {
            if (txtDistrik.Text.Trim().Length == 0)
            {
                MessageBox.Show("PO belum di pilih", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                FRefSendPortTo f = new FRefSendPortTo();
                f.pDistrik = txtDistrik.Text;
                f.ShowDialog();
            }
        }

        private void btnSearchPo_Click(object sender, EventArgs e)
        {
            FRefPOSendPOT f = new FRefPOSendPOT();            
            f.ShowDialog();
        }

        private void btnProcess_Click(object sender, EventArgs e)
        {
            setData();
            btnSubmit.Enabled = true;
            //var f = new FmManual();
            //f.preset = GlobalModel.AutoRefueling.preset_gross * 10;

            //f.trans_type_select = "DARAT_SEND";
            //GlobalModel.AutoRefueling.trans_type_select = f.trans_type_select;
            //f.waitCounterMax = 3 * 10;
            //f.ListDatumRitasiMaintank = pData;
            //if (f.ShowDialog() == DialogResult.OK)
            //{
            //    txtSendQty.Text = Convert.ToString(f.quantity);
            //    txtSendfmAwal.Text = Convert.ToString(f.totalizer_awal);
            //    txtSendfmAkhir.Text = Convert.ToString(f.totalizer_akhir);
            //    btnSubmit.Enabled = true;
            //}
            //else
            //{
            //    txtSendQty.Text = Convert.ToString(f.quantity);
            //    txtSendfmAwal.Text = Convert.ToString(f.totalizer_awal);
            //    txtSendfmAkhir.Text = Convert.ToString(f.totalizer_akhir);
            //}
        }
        async void submitData()
        {
            var _auth = GlobalModel.loginModel;
            String _url = $"{pUrl_service}api/FuelService/sendRitasiToMaintankDesktop";

            try
            {
                returnValue data = await _url.WithHeaders(new { Authorization = _auth.token })
                       .PostJsonAsync(pData).ReceiveJson<returnValue>();
                if (data.status)
                {
                    MessageBox.Show("Data Submitted", MessageBoxButtons.OK.ToString());
                    submitDataToLocalSuccess();
                    _backToCallForm();
                    this.Close();
                }                
                else
                {
                    MessageBox.Show(data.remarks, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    log.WriteToFile("logLCR600.txt", data.remarks);
                    save_error_log(data.remarks, "submitData");
                    MessageBox.Show("Menyimpan data transaksi ke local", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    var res = submitDataToLocal();
                    if (res == 1)
                    {
                        var f = new FPopUpNotification("Berhasil menyimpan data pada lokal!", "Simpan Data Receive Direct");
                        f.ShowDialog();
                        this.Close();
                    }
                }                    
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception: Terjadi kesalahan saat submit data mohon dicek logLCR600.txt", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", ex.ToString());
                save_error_log(ex.ToString(), "submitData()");
                MessageBox.Show("Menyimpan data transaksi ke local", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                var res = submitDataToLocal();
                if (res == 1)
                {
                    var f = new FPopUpNotification("Berhasil menyimpan data pada lokal!", "Simpan Data Receive Direct");
                    f.ShowDialog();

                    this.Close();
                }
            }
        }
        int submitDataToLocalSuccess()
        {
            try
            {
                var _item = pData;
                SQLiteConnection conn;

                conn = new SQLiteConnection(myConnectionString);
                if (conn.State != ConnectionState.Open) conn.Open();
                string query = string.Empty;
                int recs = 0;
                query = "INSERT INTO tbl_t_send_pot(pid_ritasi_maintank,ref_tbl,ref_id,district,send_type,send_po_no,send_do_no,send_qty,transportir_code,transportir_name,send_ft_no,send_driver_name,send_date,send_bbn,send_from,send_from_code,send_to,send_to_code,send_dipping_hole1_port,send_dipping_hole2_port,send_iscomplete,send_sn_flow_meter,send_meter_faktor,send_flow_meter_awal,send_flow_meter_akhir,send_qty_x_mf,send_no_segel_1,send_no_segel_2,send_no_segel_3,send_no_segel_4,send_no_segel_5,send_no_segel_6,doc_status,last_mod_by,foto,send_time_start,send_time_end	,syncs)"
                      + " VALUES(@pid_ritasi_maintank,@ref_tbl,@ref_id,@district,@send_type,@send_po_no,@send_do_no,@send_qty,@transportir_code,@transportir_name,@send_ft_no,@send_driver_name,@send_date,@send_bbn,@send_from,@send_from_code,@send_to,@send_to_code,@send_dipping_hole1_port,@send_dipping_hole2_port,@send_iscomplete,@send_sn_flow_meter,@send_meter_faktor,@send_flow_meter_awal,@send_flow_meter_akhir,@send_qty_x_mf,@send_no_segel_1,@send_no_segel_2,@send_no_segel_3,@send_no_segel_4,@send_no_segel_5,@send_no_segel_6,@doc_status,@last_mod_by,@foto,@send_time_start,@send_time_end,@syncs)";

                SQLiteCommand cmd = new SQLiteCommand(query, conn);


                cmd.Parameters.Add(new SQLiteParameter("@pid_ritasi_maintank", _item.PidRitasiMaintank));
                cmd.Parameters.Add(new SQLiteParameter("@transportir_code", _item.TransportirCode));
                cmd.Parameters.Add(new SQLiteParameter("@transportir_name", _item.TransportirName));
                cmd.Parameters.Add(new SQLiteParameter("@send_po_no", _item.SendPoNo));
                cmd.Parameters.Add(new SQLiteParameter("@district", _item.District));
                cmd.Parameters.Add(new SQLiteParameter("@send_do_no", _item.SendDoNo));
                cmd.Parameters.Add(new SQLiteParameter("@send_ft_no", _item.SendFtNo));
                cmd.Parameters.Add(new SQLiteParameter("@send_driver_name", _item.SendDriverName));
                cmd.Parameters.Add(new SQLiteParameter("@send_date", _item.SendDate));
                cmd.Parameters.Add(new SQLiteParameter("@send_qty", _item.SendQty));
                cmd.Parameters.Add(new SQLiteParameter("@send_from", _item.SendFrom));
                cmd.Parameters.Add(new SQLiteParameter("@send_from_code", _item.SendFromCode));
                cmd.Parameters.Add(new SQLiteParameter("@send_to", _item.SendTo));
                cmd.Parameters.Add(new SQLiteParameter("@send_to_code", _item.SendToCode));
                cmd.Parameters.Add(new SQLiteParameter("@send_bbn", _item.SendBbn));
                cmd.Parameters.Add(new SQLiteParameter("@send_dipping_hole1_port", _item.SendDippingHole1Port));
                cmd.Parameters.Add(new SQLiteParameter("@send_dipping_hole2_port", _item.SendDippingHole2Port));
                cmd.Parameters.Add(new SQLiteParameter("@last_mod_by", _item.LastModBy));
                cmd.Parameters.Add(new SQLiteParameter("@ref_tbl", _item.RefTbl));
                cmd.Parameters.Add(new SQLiteParameter("@doc_status", _item.DocStatus));
                cmd.Parameters.Add(new SQLiteParameter("@ref_id", _item.RefId));
                cmd.Parameters.Add(new SQLiteParameter("@send_type", _item.SendType));
                cmd.Parameters.Add(new SQLiteParameter("@send_iscomplete", '1'));
                cmd.Parameters.Add(new SQLiteParameter("@send_sn_flow_meter", _item.SendsnFlowMeter));
                cmd.Parameters.Add(new SQLiteParameter("@send_meter_faktor", _item.SendMeterFaktor));
                cmd.Parameters.Add(new SQLiteParameter("@send_flow_meter_awal", _item.SendFlowMeterAwal));
                cmd.Parameters.Add(new SQLiteParameter("@send_flow_meter_akhir", _item.SendFlowMeterAkhir));
                cmd.Parameters.Add(new SQLiteParameter("@send_qty_x_mf", _item.SendQtyXMf));
                cmd.Parameters.Add(new SQLiteParameter("@send_no_segel_1", _item.SendNoSegel1));
                cmd.Parameters.Add(new SQLiteParameter("@send_no_segel_2", _item.SendNoSegel2));
                cmd.Parameters.Add(new SQLiteParameter("@send_no_segel_3", _item.SendNoSegel3));
                cmd.Parameters.Add(new SQLiteParameter("@send_no_segel_4", _item.SendNoSegel4));
                cmd.Parameters.Add(new SQLiteParameter("@send_no_segel_5", _item.SendNoSegel5));
                cmd.Parameters.Add(new SQLiteParameter("@send_no_segel_6", _item.SendNoSegel6));
                cmd.Parameters.Add(new SQLiteParameter("@last_mod_by	", _item.LastModBy));
                cmd.Parameters.Add(new SQLiteParameter("@foto", _item.Foto));
                cmd.Parameters.Add(new SQLiteParameter("@send_time_start", _item.SendTimeStart));
                cmd.Parameters.Add(new SQLiteParameter("@send_time_end", _item.SendTimeEnd));
                cmd.Parameters.Add(new SQLiteParameter("@syncs", 7));

                recs = cmd.ExecuteNonQuery();

                if (conn.State == ConnectionState.Open) conn.Close();
                return recs;
            }
            catch (Exception ex)
            {
                save_error_log(ex.ToString(), "submitDataToLocal()");
                FileToExcel("pengiriman_ft_port_pama");
                MessageBox.Show("Data Tidak Tersimpan, Silahkan tulis di logsheet manual", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return -1;
            }


        }
        int submitDataToLocal()
        {
            try
            {
                var _item = pData;
                SQLiteConnection conn;

                conn = new SQLiteConnection(myConnectionString);
                if (conn.State != ConnectionState.Open) conn.Open();
                string query = string.Empty;
                int recs = 0;
                query = "INSERT INTO tbl_t_send_pot(pid_ritasi_maintank,ref_tbl,ref_id,district,send_type,send_po_no,send_do_no,send_qty,transportir_code,transportir_name,send_ft_no,send_driver_name,send_date,send_bbn,send_from,send_from_code,send_to,send_to_code,send_dipping_hole1_port,send_dipping_hole2_port,send_iscomplete,send_sn_flow_meter,send_meter_faktor,send_flow_meter_awal,send_flow_meter_akhir,send_qty_x_mf,send_no_segel_1,send_no_segel_2,send_no_segel_3,send_no_segel_4,send_no_segel_5,send_no_segel_6,doc_status,last_mod_by,foto,send_time_start,send_time_end	,syncs)"
                      + " VALUES(@pid_ritasi_maintank,@ref_tbl,@ref_id,@district,@send_type,@send_po_no,@send_do_no,@send_qty,@transportir_code,@transportir_name,@send_ft_no,@send_driver_name,@send_date,@send_bbn,@send_from,@send_from_code,@send_to,@send_to_code,@send_dipping_hole1_port,@send_dipping_hole2_port,@send_iscomplete,@send_sn_flow_meter,@send_meter_faktor,@send_flow_meter_awal,@send_flow_meter_akhir,@send_qty_x_mf,@send_no_segel_1,@send_no_segel_2,@send_no_segel_3,@send_no_segel_4,@send_no_segel_5,@send_no_segel_6,@doc_status,@last_mod_by,@foto,@send_time_start,@send_time_end,@syncs)";

                SQLiteCommand cmd = new SQLiteCommand(query, conn);


                cmd.Parameters.Add(new SQLiteParameter("@pid_ritasi_maintank", _item.PidRitasiMaintank));
                cmd.Parameters.Add(new SQLiteParameter("@transportir_code", _item.TransportirCode));
                cmd.Parameters.Add(new SQLiteParameter("@transportir_name", _item.TransportirName));
                cmd.Parameters.Add(new SQLiteParameter("@send_po_no", _item.SendPoNo));
                cmd.Parameters.Add(new SQLiteParameter("@district", _item.District));
                cmd.Parameters.Add(new SQLiteParameter("@send_do_no", _item.SendDoNo));
                cmd.Parameters.Add(new SQLiteParameter("@send_ft_no", _item.SendFtNo));
                cmd.Parameters.Add(new SQLiteParameter("@send_driver_name", _item.SendDriverName));
                cmd.Parameters.Add(new SQLiteParameter("@send_date", _item.SendDate));
                cmd.Parameters.Add(new SQLiteParameter("@send_qty", _item.SendQty));                
                cmd.Parameters.Add(new SQLiteParameter("@send_from", _item.SendFrom));
                cmd.Parameters.Add(new SQLiteParameter("@send_from_code", _item.SendFromCode));
                cmd.Parameters.Add(new SQLiteParameter("@send_to", _item.SendTo));
                cmd.Parameters.Add(new SQLiteParameter("@send_to_code", _item.SendToCode));                
                cmd.Parameters.Add(new SQLiteParameter("@send_bbn", _item.SendBbn));
                cmd.Parameters.Add(new SQLiteParameter("@send_dipping_hole1_port", _item.SendDippingHole1Port));
                cmd.Parameters.Add(new SQLiteParameter("@send_dipping_hole2_port", _item.SendDippingHole2Port));
                cmd.Parameters.Add(new SQLiteParameter("@last_mod_by", _item.LastModBy));
                cmd.Parameters.Add(new SQLiteParameter("@ref_tbl", _item.RefTbl));
                cmd.Parameters.Add(new SQLiteParameter("@doc_status", _item.DocStatus));
                cmd.Parameters.Add(new SQLiteParameter("@ref_id", _item.RefId));
                cmd.Parameters.Add(new SQLiteParameter("@send_type", _item.SendType));
                cmd.Parameters.Add(new SQLiteParameter("@send_iscomplete", _item.SendIscomplete));
                cmd.Parameters.Add(new SQLiteParameter("@send_sn_flow_meter", _item.SendsnFlowMeter));
                cmd.Parameters.Add(new SQLiteParameter("@send_meter_faktor", _item.SendMeterFaktor));
                cmd.Parameters.Add(new SQLiteParameter("@send_flow_meter_awal", _item.SendFlowMeterAwal));
                cmd.Parameters.Add(new SQLiteParameter("@send_flow_meter_akhir", _item.SendFlowMeterAkhir));
                cmd.Parameters.Add(new SQLiteParameter("@send_qty_x_mf", _item.SendQtyXMf));
                cmd.Parameters.Add(new SQLiteParameter("@send_no_segel_1", _item.SendNoSegel1));
                cmd.Parameters.Add(new SQLiteParameter("@send_no_segel_2", _item.SendNoSegel2));
                cmd.Parameters.Add(new SQLiteParameter("@send_no_segel_3", _item.SendNoSegel3));
                cmd.Parameters.Add(new SQLiteParameter("@send_no_segel_4", _item.SendNoSegel4));
                cmd.Parameters.Add(new SQLiteParameter("@send_no_segel_5", _item.SendNoSegel5));
                cmd.Parameters.Add(new SQLiteParameter("@send_no_segel_6", _item.SendNoSegel6));
                cmd.Parameters.Add(new SQLiteParameter("@last_mod_by	", _item.LastModBy));
                cmd.Parameters.Add(new SQLiteParameter("@foto", _item.Foto));
                cmd.Parameters.Add(new SQLiteParameter("@send_time_start", _item.SendTimeStart));
                cmd.Parameters.Add(new SQLiteParameter("@send_time_end", _item.SendTimeEnd));
                cmd.Parameters.Add(new SQLiteParameter("@syncs", 2));

                recs = cmd.ExecuteNonQuery();                

                if (conn.State == ConnectionState.Open) conn.Close();
                return recs;
            }
            catch (Exception ex)
            {
                save_error_log(ex.ToString(), "submitDataToLocal()");
                FileToExcel("pengiriman_ft_port_pama");
                MessageBox.Show("Data Tidak Tersimpan, Silahkan tulis di logsheet manual", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return -1;
            }


        }
        public void FileToExcel(string name)
        {
            StringBuilder sb = new StringBuilder();
            var filename = "log_transaksi_" + name + DateTime.Now.ToString("yyyyMMdd") + ".csv";
            //cek direktori jika tidak ada maka dibuat direktori baru
            if (!Directory.Exists(Path.Combine(Environment.CurrentDirectory, "log_transaksi_eror_simpan_db")))
            {
                Directory.CreateDirectory(Path.Combine(Environment.CurrentDirectory, "log_transaksi_eror_simpan_db"));
            }
            var pathExcel = Path.Combine(Environment.CurrentDirectory, "log_transaksi_eror_simpan_db", filename);
            //cek file jika tidak ada maka buat header baru
            if (!File.Exists(pathExcel))
            {
                sb.Append("pid_ritasi_maintank;ref_tbl;ref_id;district;send_type;send_po_no;send_do_no;send_qty;transportir_code;transportir_name;send_ft_no;send_driver_name;send_date;send_bbn;send_from;send_from_code;send_to;send_to_code;send_dipping_hole1_port;send_dipping_hole2_port;send_iscomplete;send_sn_flow_meter;send_meter_faktor;send_flow_meter_awal;send_flow_meter_akhir;send_qty_x_mf;send_no_segel_1;send_no_segel_2;send_no_segel_3;send_no_segel_4;send_no_segel_5;send_no_segel_6;doc_status;last_mod_by;foto;send_time_start;send_time_end;\r\n");
            }
            var item = pData;
            sb.Append(item.PidRitasiMaintank + ";");
            sb.Append(item.RefTbl + ";");
            sb.Append(item.RefId + ";");
            sb.Append(item.District + ";");
            sb.Append(item.SendType + ";");
            sb.Append(item.SendPoNo + ";");
            sb.Append(item.SendDoNo + ";");
            sb.Append(item.SendQty + ";");
            sb.Append(item.TransportirCode + ";");
            sb.Append(item.TransportirName + ";");                                    
            sb.Append(item.SendFtNo + ";");
            sb.Append(item.SendDriverName + ";");
            sb.Append(item.SendDate + ";");
            sb.Append(item.SendBbn + ";");            
            sb.Append(item.SendFrom + ";");
            sb.Append(item.SendFromCode + ";");
            sb.Append(item.SendTo + ";");
            sb.Append(item.SendToCode + ";");
            sb.Append(item.SendDippingHole1Port + ";");
            sb.Append(item.SendDippingHole2Port + ";");
            sb.Append(item.SendIscomplete + ";");
            sb.Append(item.SendsnFlowMeter + ";");
            sb.Append(item.SendMeterFaktor + ";");
            sb.Append(item.SendFlowMeterAwal + ";");
            sb.Append(item.SendFlowMeterAkhir + ";");
            sb.Append(item.SendQtyXMf + ";");
            sb.Append(item.SendNoSegel1 + ";");
            sb.Append(item.SendNoSegel2 + ";");
            sb.Append(item.SendNoSegel3 + ";");
            sb.Append(item.SendNoSegel4 + ";");
            sb.Append(item.SendNoSegel5 + ";");
            sb.Append(item.SendNoSegel6 + ";");                        
            sb.Append(item.DocStatus + ";");                                                
            sb.Append(item.LastModBy + ";");
            sb.Append(item.Foto + ";");
            sb.Append(item.SendTimeStart + ";");
            sb.Append(item.SendTimeEnd + ";");
            sb.Append("\r\n");



            if (File.Exists(pathExcel))
            {
                File.AppendAllText(pathExcel, sb.ToString() + Environment.NewLine);
            }
            else
            {
                File.WriteAllText(pathExcel, sb.ToString());
            }

        }

        public async Task<string> checkConnectivity()
        {
            try
            {
                WebClient requestCek = new WebClient();
                var str = requestCek.DownloadString(pUrl_service);
                return str;
            }
            catch (WebException ex)
            {
                return "";
            }
        }
        private async void btnSubmit_Click(object sender, EventArgs e)
        {
            var _confirm = MessageBox.Show("Anda yakin data sudah benar?", "Confirm submit!!", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (_confirm == DialogResult.Yes)
            {
                var checkconnect = await checkConnectivity();
                if (checkconnect.Contains("Supply Service"))
                {
                    submitData();
                    
                }
                else
                {
                    var res = submitDataToLocal();
                    if (res == 1)
                    {
                        var f = new FPopUpNotification("Berhasil menyimpan data pada lokal!", "Simpan Data Pengiriman FT Port Pama");
                        f.ShowDialog();
                        this.Close();
                    }
                }
            }
        }

        private void comboBoxTransportirTruck_Click(object sender, EventArgs e)
        {
            try
            {
                ProcessStartInfo psStartInfo = new ProcessStartInfo();
                psStartInfo.FileName = @"C:\Program Files\Common Files\microsoft shared\ink\TabTip.exe";
                //var dir = Path.Combine(Environment.CurrentDirectory, "VKeyboard", "VKeyboard.exe");
                //psStartInfo.FileName = dir;

                Process ps = Process.Start(psStartInfo);

            }
            catch (Exception ex)
            {
                var error = ex.Message;
            }

        }

        private void comboBoxTransportirTruck_SelectedIndexChanged(object sender, EventArgs e)
        {

        }       
    }
}
