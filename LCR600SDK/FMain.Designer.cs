﻿namespace LCR600SDK
{
    partial class FMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FMain));
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabelUsername = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelDistrik = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblAutoSyncStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.settingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.toolConnection = new System.Windows.Forms.Button();
            this.panelForm = new System.Windows.Forms.Panel();
            this.btnListDataLocal = new System.Windows.Forms.Button();
            this.btnPengirimanReceive = new System.Windows.Forms.Button();
            this.btnTransaksi = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pnlListData = new System.Windows.Forms.Panel();
            this.btnListDataPengirimanPOT = new System.Windows.Forms.Button();
            this.btnListDataReceiveDirectOwner = new System.Windows.Forms.Button();
            this.btnListDataReceivedFTPort = new System.Windows.Forms.Button();
            this.btnListDataReceiveFTDarat = new System.Windows.Forms.Button();
            this.btnListDataPengirimanFTPortPama = new System.Windows.Forms.Button();
            this.btnListDataReceiveDirect = new System.Windows.Forms.Button();
            this.btnListDataTransfer = new System.Windows.Forms.Button();
            this.btnListDataIssued = new System.Windows.Forms.Button();
            this.pnlTransaksi = new System.Windows.Forms.Panel();
            this.btnRefFT = new System.Windows.Forms.Button();
            this.btnRefPitstop = new System.Windows.Forms.Button();
            this.btnWhTransfer = new System.Windows.Forms.Button();
            this.btnWhPengirim = new System.Windows.Forms.Button();
            this.pnlPengirimanReceive = new System.Windows.Forms.Panel();
            this.btnPengirimanPOT = new System.Windows.Forms.Button();
            this.btnReceiveDirectOwner = new System.Windows.Forms.Button();
            this.btnReceivedDirect = new System.Windows.Forms.Button();
            this.btnPengirimanFtPortPama = new System.Windows.Forms.Button();
            this.btnReceivedFtDarat = new System.Windows.Forms.Button();
            this.btnReceivedFtPort = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.p7Inch = new System.Windows.Forms.Panel();
            this.btnRefPS7Inch = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.btnRefFT7Inch = new System.Windows.Forms.Button();
            this.btnListDataTransfer7Inch = new System.Windows.Forms.Button();
            this.btnListDataIssued7Inch = new System.Windows.Forms.Button();
            this.btnWhTransfer7Inch = new System.Windows.Forms.Button();
            this.statusStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.panelForm.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.pnlListData.SuspendLayout();
            this.pnlTransaksi.SuspendLayout();
            this.pnlPengirimanReceive.SuspendLayout();
            this.p7Inch.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Font = new System.Drawing.Font("Segoe UI", 18F);
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(40, 40);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabelUsername,
            this.toolStripStatusLabelDistrik,
            this.toolStripStatusLabelStatus,
            this.lblAutoSyncStatus});
            this.statusStrip1.Location = new System.Drawing.Point(0, 665);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(0, 0, 5, 0);
            this.statusStrip1.Size = new System.Drawing.Size(1117, 37);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabelUsername
            // 
            this.toolStripStatusLabelUsername.Name = "toolStripStatusLabelUsername";
            this.toolStripStatusLabelUsername.Size = new System.Drawing.Size(122, 32);
            this.toolStripStatusLabelUsername.Text = "Username";
            // 
            // toolStripStatusLabelDistrik
            // 
            this.toolStripStatusLabelDistrik.Name = "toolStripStatusLabelDistrik";
            this.toolStripStatusLabelDistrik.Size = new System.Drawing.Size(82, 32);
            this.toolStripStatusLabelDistrik.Text = "Distrik";
            // 
            // toolStripStatusLabelStatus
            // 
            this.toolStripStatusLabelStatus.Name = "toolStripStatusLabelStatus";
            this.toolStripStatusLabelStatus.Size = new System.Drawing.Size(79, 32);
            this.toolStripStatusLabelStatus.Text = "Status";
            // 
            // lblAutoSyncStatus
            // 
            this.lblAutoSyncStatus.Name = "lblAutoSyncStatus";
            this.lblAutoSyncStatus.Size = new System.Drawing.Size(171, 32);
            this.lblAutoSyncStatus.Text = "Auto Sync OFF";
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.Control;
            this.menuStrip1.Font = new System.Drawing.Font("Segoe UI", 18F);
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(40, 40);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.settingToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(2, 1, 0, 1);
            this.menuStrip1.Size = new System.Drawing.Size(1117, 38);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // settingToolStripMenuItem
            // 
            this.settingToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.settingToolStripMenuItem.Name = "settingToolStripMenuItem";
            this.settingToolStripMenuItem.Size = new System.Drawing.Size(103, 36);
            this.settingToolStripMenuItem.Text = "Setting";
            this.settingToolStripMenuItem.Click += new System.EventHandler(this.settingToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator1,
            this.toolStripMenuItem1,
            this.aboutToolStripMenuItem,
            this.logToolStripMenuItem,
            this.toolStripSeparator2,
            this.exitToolStripMenuItem});
            this.helpToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(77, 36);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(179, 6);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.BackColor = System.Drawing.SystemColors.Control;
            this.toolStripMenuItem1.ForeColor = System.Drawing.Color.Black;
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(182, 36);
            this.toolStripMenuItem1.Text = "Panduan";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(182, 36);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // logToolStripMenuItem
            // 
            this.logToolStripMenuItem.Name = "logToolStripMenuItem";
            this.logToolStripMenuItem.Size = new System.Drawing.Size(182, 36);
            this.logToolStripMenuItem.Text = "LogApps";
            this.logToolStripMenuItem.Click += new System.EventHandler(this.logToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(179, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.exitToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(182, 36);
            this.exitToolStripMenuItem.Text = "Log Out";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 900000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // toolConnection
            // 
            this.toolConnection.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.toolConnection.BackColor = System.Drawing.Color.Red;
            this.toolConnection.FlatAppearance.BorderSize = 0;
            this.toolConnection.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.toolConnection.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.toolConnection.ForeColor = System.Drawing.Color.White;
            this.toolConnection.Location = new System.Drawing.Point(981, 664);
            this.toolConnection.Name = "toolConnection";
            this.toolConnection.Size = new System.Drawing.Size(136, 38);
            this.toolConnection.TabIndex = 17;
            this.toolConnection.Text = "OFFLINE";
            this.toolConnection.UseVisualStyleBackColor = false;
            // 
            // panelForm
            // 
            this.panelForm.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(101)))), ((int)(((byte)(179)))));
            this.panelForm.Controls.Add(this.btnListDataLocal);
            this.panelForm.Controls.Add(this.btnPengirimanReceive);
            this.panelForm.Controls.Add(this.btnTransaksi);
            this.panelForm.Controls.Add(this.panel1);
            this.panelForm.Controls.Add(this.pnlTransaksi);
            this.panelForm.Controls.Add(this.pnlPengirimanReceive);
            this.panelForm.Controls.Add(this.pnlListData);
            this.panelForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelForm.Location = new System.Drawing.Point(0, 0);
            this.panelForm.Name = "panelForm";
            this.panelForm.Size = new System.Drawing.Size(1117, 702);
            this.panelForm.TabIndex = 18;
            // 
            // btnListDataLocal
            // 
            this.btnListDataLocal.BackColor = System.Drawing.Color.LawnGreen;
            this.btnListDataLocal.FlatAppearance.BorderSize = 0;
            this.btnListDataLocal.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btnListDataLocal.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnListDataLocal.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnListDataLocal.ForeColor = System.Drawing.Color.Black;
            this.btnListDataLocal.Location = new System.Drawing.Point(25, 334);
            this.btnListDataLocal.Margin = new System.Windows.Forms.Padding(2);
            this.btnListDataLocal.Name = "btnListDataLocal";
            this.btnListDataLocal.Size = new System.Drawing.Size(157, 97);
            this.btnListDataLocal.TabIndex = 36;
            this.btnListDataLocal.Text = "List Data Local";
            this.btnListDataLocal.UseVisualStyleBackColor = false;
            this.btnListDataLocal.Click += new System.EventHandler(this.btnListDataLocal_Click);
            // 
            // btnPengirimanReceive
            // 
            this.btnPengirimanReceive.BackColor = System.Drawing.Color.SkyBlue;
            this.btnPengirimanReceive.FlatAppearance.BorderSize = 0;
            this.btnPengirimanReceive.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPengirimanReceive.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPengirimanReceive.Location = new System.Drawing.Point(25, 206);
            this.btnPengirimanReceive.Margin = new System.Windows.Forms.Padding(2);
            this.btnPengirimanReceive.Name = "btnPengirimanReceive";
            this.btnPengirimanReceive.Size = new System.Drawing.Size(157, 97);
            this.btnPengirimanReceive.TabIndex = 35;
            this.btnPengirimanReceive.Text = "Pengiriman dan Receive";
            this.btnPengirimanReceive.UseVisualStyleBackColor = false;
            this.btnPengirimanReceive.Click += new System.EventHandler(this.btnPengirimanReceive_Click);
            // 
            // btnTransaksi
            // 
            this.btnTransaksi.BackColor = System.Drawing.Color.Orange;
            this.btnTransaksi.FlatAppearance.BorderSize = 0;
            this.btnTransaksi.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTransaksi.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTransaksi.ForeColor = System.Drawing.Color.Black;
            this.btnTransaksi.Location = new System.Drawing.Point(25, 82);
            this.btnTransaksi.Margin = new System.Windows.Forms.Padding(2);
            this.btnTransaksi.Name = "btnTransaksi";
            this.btnTransaksi.Size = new System.Drawing.Size(157, 97);
            this.btnTransaksi.TabIndex = 34;
            this.btnTransaksi.Text = "Transaksi";
            this.btnTransaksi.UseVisualStyleBackColor = false;
            this.btnTransaksi.Click += new System.EventHandler(this.btnTransaksi_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Location = new System.Drawing.Point(860, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(254, 280);
            this.panel1.TabIndex = 28;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(-148, -143);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(562, 536);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 27;
            this.pictureBox1.TabStop = false;
            // 
            // pnlListData
            // 
            this.pnlListData.Controls.Add(this.btnListDataPengirimanPOT);
            this.pnlListData.Controls.Add(this.btnListDataReceiveDirectOwner);
            this.pnlListData.Controls.Add(this.btnListDataReceivedFTPort);
            this.pnlListData.Controls.Add(this.btnListDataReceiveFTDarat);
            this.pnlListData.Controls.Add(this.btnListDataPengirimanFTPortPama);
            this.pnlListData.Controls.Add(this.btnListDataReceiveDirect);
            this.pnlListData.Controls.Add(this.btnListDataTransfer);
            this.pnlListData.Controls.Add(this.btnListDataIssued);
            this.pnlListData.Location = new System.Drawing.Point(203, 51);
            this.pnlListData.Name = "pnlListData";
            this.pnlListData.Size = new System.Drawing.Size(665, 611);
            this.pnlListData.TabIndex = 38;
            // 
            // btnListDataPengirimanPOT
            // 
            this.btnListDataPengirimanPOT.BackColor = System.Drawing.Color.LawnGreen;
            this.btnListDataPengirimanPOT.FlatAppearance.BorderSize = 0;
            this.btnListDataPengirimanPOT.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btnListDataPengirimanPOT.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnListDataPengirimanPOT.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnListDataPengirimanPOT.ForeColor = System.Drawing.Color.Black;
            this.btnListDataPengirimanPOT.Location = new System.Drawing.Point(492, 110);
            this.btnListDataPengirimanPOT.Margin = new System.Windows.Forms.Padding(2);
            this.btnListDataPengirimanPOT.Name = "btnListDataPengirimanPOT";
            this.btnListDataPengirimanPOT.Size = new System.Drawing.Size(157, 97);
            this.btnListDataPengirimanPOT.TabIndex = 41;
            this.btnListDataPengirimanPOT.Text = "List Data Pengiriman POT";
            this.btnListDataPengirimanPOT.UseVisualStyleBackColor = false;
            this.btnListDataPengirimanPOT.Click += new System.EventHandler(this.btnListDataPengirimanPOT_Click);
            // 
            // btnListDataReceiveDirectOwner
            // 
            this.btnListDataReceiveDirectOwner.BackColor = System.Drawing.Color.LawnGreen;
            this.btnListDataReceiveDirectOwner.FlatAppearance.BorderSize = 0;
            this.btnListDataReceiveDirectOwner.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btnListDataReceiveDirectOwner.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnListDataReceiveDirectOwner.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnListDataReceiveDirectOwner.ForeColor = System.Drawing.Color.Black;
            this.btnListDataReceiveDirectOwner.Location = new System.Drawing.Point(331, 109);
            this.btnListDataReceiveDirectOwner.Margin = new System.Windows.Forms.Padding(2);
            this.btnListDataReceiveDirectOwner.Name = "btnListDataReceiveDirectOwner";
            this.btnListDataReceiveDirectOwner.Size = new System.Drawing.Size(157, 97);
            this.btnListDataReceiveDirectOwner.TabIndex = 40;
            this.btnListDataReceiveDirectOwner.Text = "List Data Receive Direct Owner";
            this.btnListDataReceiveDirectOwner.UseVisualStyleBackColor = false;
            this.btnListDataReceiveDirectOwner.Click += new System.EventHandler(this.btnListDataReceiveDirectOwner_Click);
            // 
            // btnListDataReceivedFTPort
            // 
            this.btnListDataReceivedFTPort.BackColor = System.Drawing.Color.LawnGreen;
            this.btnListDataReceivedFTPort.FlatAppearance.BorderSize = 0;
            this.btnListDataReceivedFTPort.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btnListDataReceivedFTPort.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnListDataReceivedFTPort.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnListDataReceivedFTPort.ForeColor = System.Drawing.Color.Black;
            this.btnListDataReceivedFTPort.Location = new System.Drawing.Point(9, 8);
            this.btnListDataReceivedFTPort.Margin = new System.Windows.Forms.Padding(2);
            this.btnListDataReceivedFTPort.Name = "btnListDataReceivedFTPort";
            this.btnListDataReceivedFTPort.Size = new System.Drawing.Size(157, 97);
            this.btnListDataReceivedFTPort.TabIndex = 39;
            this.btnListDataReceivedFTPort.Text = "List Data Received FT Port";
            this.btnListDataReceivedFTPort.UseVisualStyleBackColor = false;
            this.btnListDataReceivedFTPort.Click += new System.EventHandler(this.btnListDataReceivedFTPort_Click);
            // 
            // btnListDataReceiveFTDarat
            // 
            this.btnListDataReceiveFTDarat.BackColor = System.Drawing.Color.LawnGreen;
            this.btnListDataReceiveFTDarat.FlatAppearance.BorderSize = 0;
            this.btnListDataReceiveFTDarat.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btnListDataReceiveFTDarat.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnListDataReceiveFTDarat.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnListDataReceiveFTDarat.ForeColor = System.Drawing.Color.Black;
            this.btnListDataReceiveFTDarat.Location = new System.Drawing.Point(170, 8);
            this.btnListDataReceiveFTDarat.Margin = new System.Windows.Forms.Padding(2);
            this.btnListDataReceiveFTDarat.Name = "btnListDataReceiveFTDarat";
            this.btnListDataReceiveFTDarat.Size = new System.Drawing.Size(157, 97);
            this.btnListDataReceiveFTDarat.TabIndex = 38;
            this.btnListDataReceiveFTDarat.Text = "List Data Received FT Darat";
            this.btnListDataReceiveFTDarat.UseVisualStyleBackColor = false;
            this.btnListDataReceiveFTDarat.Click += new System.EventHandler(this.btnListDataReceiveFTDarat_Click);
            // 
            // btnListDataPengirimanFTPortPama
            // 
            this.btnListDataPengirimanFTPortPama.BackColor = System.Drawing.Color.LawnGreen;
            this.btnListDataPengirimanFTPortPama.FlatAppearance.BorderSize = 0;
            this.btnListDataPengirimanFTPortPama.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btnListDataPengirimanFTPortPama.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnListDataPengirimanFTPortPama.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnListDataPengirimanFTPortPama.ForeColor = System.Drawing.Color.Black;
            this.btnListDataPengirimanFTPortPama.Location = new System.Drawing.Point(331, 8);
            this.btnListDataPengirimanFTPortPama.Margin = new System.Windows.Forms.Padding(2);
            this.btnListDataPengirimanFTPortPama.Name = "btnListDataPengirimanFTPortPama";
            this.btnListDataPengirimanFTPortPama.Size = new System.Drawing.Size(157, 97);
            this.btnListDataPengirimanFTPortPama.TabIndex = 37;
            this.btnListDataPengirimanFTPortPama.Text = "List Data Pengiriman FT Port PAMA";
            this.btnListDataPengirimanFTPortPama.UseVisualStyleBackColor = false;
            this.btnListDataPengirimanFTPortPama.Click += new System.EventHandler(this.btnListDataPengirimanFTPortPama_Click);
            // 
            // btnListDataReceiveDirect
            // 
            this.btnListDataReceiveDirect.BackColor = System.Drawing.Color.LawnGreen;
            this.btnListDataReceiveDirect.FlatAppearance.BorderSize = 0;
            this.btnListDataReceiveDirect.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btnListDataReceiveDirect.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnListDataReceiveDirect.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnListDataReceiveDirect.ForeColor = System.Drawing.Color.Black;
            this.btnListDataReceiveDirect.Location = new System.Drawing.Point(492, 8);
            this.btnListDataReceiveDirect.Margin = new System.Windows.Forms.Padding(2);
            this.btnListDataReceiveDirect.Name = "btnListDataReceiveDirect";
            this.btnListDataReceiveDirect.Size = new System.Drawing.Size(157, 97);
            this.btnListDataReceiveDirect.TabIndex = 36;
            this.btnListDataReceiveDirect.Text = "List Data Receive Direct";
            this.btnListDataReceiveDirect.UseVisualStyleBackColor = false;
            this.btnListDataReceiveDirect.Click += new System.EventHandler(this.btnListDataReceiveDirect_Click);
            // 
            // btnListDataTransfer
            // 
            this.btnListDataTransfer.BackColor = System.Drawing.Color.LawnGreen;
            this.btnListDataTransfer.FlatAppearance.BorderSize = 0;
            this.btnListDataTransfer.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btnListDataTransfer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnListDataTransfer.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnListDataTransfer.ForeColor = System.Drawing.Color.Black;
            this.btnListDataTransfer.Location = new System.Drawing.Point(170, 109);
            this.btnListDataTransfer.Margin = new System.Windows.Forms.Padding(2);
            this.btnListDataTransfer.Name = "btnListDataTransfer";
            this.btnListDataTransfer.Size = new System.Drawing.Size(157, 97);
            this.btnListDataTransfer.TabIndex = 35;
            this.btnListDataTransfer.Text = "List Data Transfer";
            this.btnListDataTransfer.UseVisualStyleBackColor = false;
            this.btnListDataTransfer.Click += new System.EventHandler(this.btnListDataTransfer_Click);
            // 
            // btnListDataIssued
            // 
            this.btnListDataIssued.BackColor = System.Drawing.Color.LawnGreen;
            this.btnListDataIssued.FlatAppearance.BorderSize = 0;
            this.btnListDataIssued.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btnListDataIssued.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnListDataIssued.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnListDataIssued.ForeColor = System.Drawing.Color.Black;
            this.btnListDataIssued.Location = new System.Drawing.Point(9, 109);
            this.btnListDataIssued.Margin = new System.Windows.Forms.Padding(2);
            this.btnListDataIssued.Name = "btnListDataIssued";
            this.btnListDataIssued.Size = new System.Drawing.Size(157, 97);
            this.btnListDataIssued.TabIndex = 34;
            this.btnListDataIssued.Text = "List Data Issued";
            this.btnListDataIssued.UseVisualStyleBackColor = false;
            this.btnListDataIssued.Click += new System.EventHandler(this.btnListDataIssued_Click);
            // 
            // pnlTransaksi
            // 
            this.pnlTransaksi.Controls.Add(this.btnRefFT);
            this.pnlTransaksi.Controls.Add(this.btnRefPitstop);
            this.pnlTransaksi.Controls.Add(this.btnWhTransfer);
            this.pnlTransaksi.Controls.Add(this.btnWhPengirim);
            this.pnlTransaksi.Location = new System.Drawing.Point(203, 51);
            this.pnlTransaksi.Name = "pnlTransaksi";
            this.pnlTransaksi.Size = new System.Drawing.Size(665, 611);
            this.pnlTransaksi.TabIndex = 37;
            // 
            // btnRefFT
            // 
            this.btnRefFT.BackColor = System.Drawing.Color.Orange;
            this.btnRefFT.FlatAppearance.BorderSize = 0;
            this.btnRefFT.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRefFT.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRefFT.ForeColor = System.Drawing.Color.Black;
            this.btnRefFT.Location = new System.Drawing.Point(173, 11);
            this.btnRefFT.Margin = new System.Windows.Forms.Padding(2);
            this.btnRefFT.Name = "btnRefFT";
            this.btnRefFT.Size = new System.Drawing.Size(157, 97);
            this.btnRefFT.TabIndex = 9;
            this.btnRefFT.Text = "Refueling FT";
            this.btnRefFT.UseVisualStyleBackColor = false;
            this.btnRefFT.Click += new System.EventHandler(this.btnRefFT_Click);
            // 
            // btnRefPitstop
            // 
            this.btnRefPitstop.BackColor = System.Drawing.Color.Orange;
            this.btnRefPitstop.FlatAppearance.BorderSize = 0;
            this.btnRefPitstop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRefPitstop.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRefPitstop.ForeColor = System.Drawing.Color.Black;
            this.btnRefPitstop.Location = new System.Drawing.Point(12, 11);
            this.btnRefPitstop.Margin = new System.Windows.Forms.Padding(2);
            this.btnRefPitstop.Name = "btnRefPitstop";
            this.btnRefPitstop.Size = new System.Drawing.Size(157, 97);
            this.btnRefPitstop.TabIndex = 8;
            this.btnRefPitstop.Text = "Refueling Pitstop";
            this.btnRefPitstop.UseVisualStyleBackColor = false;
            this.btnRefPitstop.Click += new System.EventHandler(this.btnRefPitstop_Click);
            // 
            // btnWhTransfer
            // 
            this.btnWhTransfer.BackColor = System.Drawing.Color.Orange;
            this.btnWhTransfer.FlatAppearance.BorderSize = 0;
            this.btnWhTransfer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnWhTransfer.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnWhTransfer.ForeColor = System.Drawing.Color.Black;
            this.btnWhTransfer.Location = new System.Drawing.Point(334, 11);
            this.btnWhTransfer.Margin = new System.Windows.Forms.Padding(2);
            this.btnWhTransfer.Name = "btnWhTransfer";
            this.btnWhTransfer.Size = new System.Drawing.Size(157, 97);
            this.btnWhTransfer.TabIndex = 10;
            this.btnWhTransfer.Text = "Warehouse Transfer";
            this.btnWhTransfer.UseVisualStyleBackColor = false;
            this.btnWhTransfer.Click += new System.EventHandler(this.btnWhTransfer_Click);
            // 
            // btnWhPengirim
            // 
            this.btnWhPengirim.BackColor = System.Drawing.Color.Orange;
            this.btnWhPengirim.FlatAppearance.BorderSize = 0;
            this.btnWhPengirim.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnWhPengirim.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnWhPengirim.ForeColor = System.Drawing.Color.Black;
            this.btnWhPengirim.Location = new System.Drawing.Point(495, 11);
            this.btnWhPengirim.Margin = new System.Windows.Forms.Padding(2);
            this.btnWhPengirim.Name = "btnWhPengirim";
            this.btnWhPengirim.Size = new System.Drawing.Size(157, 97);
            this.btnWhPengirim.TabIndex = 11;
            this.btnWhPengirim.Text = "Warehouse Transfer Pengirim";
            this.btnWhPengirim.UseVisualStyleBackColor = false;
            this.btnWhPengirim.Click += new System.EventHandler(this.btnWhPengirim_Click);
            // 
            // pnlPengirimanReceive
            // 
            this.pnlPengirimanReceive.Controls.Add(this.btnPengirimanPOT);
            this.pnlPengirimanReceive.Controls.Add(this.btnReceiveDirectOwner);
            this.pnlPengirimanReceive.Controls.Add(this.btnReceivedDirect);
            this.pnlPengirimanReceive.Controls.Add(this.btnPengirimanFtPortPama);
            this.pnlPengirimanReceive.Controls.Add(this.btnReceivedFtDarat);
            this.pnlPengirimanReceive.Controls.Add(this.btnReceivedFtPort);
            this.pnlPengirimanReceive.Location = new System.Drawing.Point(203, 51);
            this.pnlPengirimanReceive.Name = "pnlPengirimanReceive";
            this.pnlPengirimanReceive.Size = new System.Drawing.Size(665, 611);
            this.pnlPengirimanReceive.TabIndex = 38;
            // 
            // btnPengirimanPOT
            // 
            this.btnPengirimanPOT.BackColor = System.Drawing.Color.SkyBlue;
            this.btnPengirimanPOT.FlatAppearance.BorderSize = 0;
            this.btnPengirimanPOT.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPengirimanPOT.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPengirimanPOT.Location = new System.Drawing.Point(165, 104);
            this.btnPengirimanPOT.Margin = new System.Windows.Forms.Padding(2);
            this.btnPengirimanPOT.Name = "btnPengirimanPOT";
            this.btnPengirimanPOT.Size = new System.Drawing.Size(157, 97);
            this.btnPengirimanPOT.TabIndex = 35;
            this.btnPengirimanPOT.Text = "Pengiriman POT";
            this.btnPengirimanPOT.UseVisualStyleBackColor = false;
            this.btnPengirimanPOT.Click += new System.EventHandler(this.btnPengirimanPOT_Click);
            // 
            // btnReceiveDirectOwner
            // 
            this.btnReceiveDirectOwner.BackColor = System.Drawing.Color.SkyBlue;
            this.btnReceiveDirectOwner.FlatAppearance.BorderSize = 0;
            this.btnReceiveDirectOwner.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReceiveDirectOwner.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReceiveDirectOwner.Location = new System.Drawing.Point(4, 104);
            this.btnReceiveDirectOwner.Margin = new System.Windows.Forms.Padding(2);
            this.btnReceiveDirectOwner.Name = "btnReceiveDirectOwner";
            this.btnReceiveDirectOwner.Size = new System.Drawing.Size(157, 97);
            this.btnReceiveDirectOwner.TabIndex = 34;
            this.btnReceiveDirectOwner.Text = "Received Direct Owner";
            this.btnReceiveDirectOwner.UseVisualStyleBackColor = false;
            this.btnReceiveDirectOwner.Click += new System.EventHandler(this.btnReceiveDirectOwner_Click);
            // 
            // btnReceivedDirect
            // 
            this.btnReceivedDirect.BackColor = System.Drawing.Color.SkyBlue;
            this.btnReceivedDirect.FlatAppearance.BorderSize = 0;
            this.btnReceivedDirect.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReceivedDirect.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReceivedDirect.Location = new System.Drawing.Point(487, 3);
            this.btnReceivedDirect.Margin = new System.Windows.Forms.Padding(2);
            this.btnReceivedDirect.Name = "btnReceivedDirect";
            this.btnReceivedDirect.Size = new System.Drawing.Size(157, 97);
            this.btnReceivedDirect.TabIndex = 33;
            this.btnReceivedDirect.Text = "Received Direct (VHS)";
            this.btnReceivedDirect.UseVisualStyleBackColor = false;
            this.btnReceivedDirect.Click += new System.EventHandler(this.btnReceivedDirect_Click);
            // 
            // btnPengirimanFtPortPama
            // 
            this.btnPengirimanFtPortPama.BackColor = System.Drawing.Color.SkyBlue;
            this.btnPengirimanFtPortPama.FlatAppearance.BorderSize = 0;
            this.btnPengirimanFtPortPama.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPengirimanFtPortPama.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPengirimanFtPortPama.Location = new System.Drawing.Point(326, 3);
            this.btnPengirimanFtPortPama.Margin = new System.Windows.Forms.Padding(2);
            this.btnPengirimanFtPortPama.Name = "btnPengirimanFtPortPama";
            this.btnPengirimanFtPortPama.Size = new System.Drawing.Size(157, 97);
            this.btnPengirimanFtPortPama.TabIndex = 32;
            this.btnPengirimanFtPortPama.Text = "Pengiriman FT Port PAMA";
            this.btnPengirimanFtPortPama.UseVisualStyleBackColor = false;
            this.btnPengirimanFtPortPama.Click += new System.EventHandler(this.btnPengirimanFtPortPama_Click);
            // 
            // btnReceivedFtDarat
            // 
            this.btnReceivedFtDarat.BackColor = System.Drawing.Color.SkyBlue;
            this.btnReceivedFtDarat.FlatAppearance.BorderSize = 0;
            this.btnReceivedFtDarat.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReceivedFtDarat.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReceivedFtDarat.Location = new System.Drawing.Point(165, 3);
            this.btnReceivedFtDarat.Margin = new System.Windows.Forms.Padding(2);
            this.btnReceivedFtDarat.Name = "btnReceivedFtDarat";
            this.btnReceivedFtDarat.Size = new System.Drawing.Size(157, 97);
            this.btnReceivedFtDarat.TabIndex = 31;
            this.btnReceivedFtDarat.Text = "Received FT Darat";
            this.btnReceivedFtDarat.UseVisualStyleBackColor = false;
            this.btnReceivedFtDarat.Click += new System.EventHandler(this.btnReceivedFtDarat_Click);
            // 
            // btnReceivedFtPort
            // 
            this.btnReceivedFtPort.BackColor = System.Drawing.Color.SkyBlue;
            this.btnReceivedFtPort.FlatAppearance.BorderSize = 0;
            this.btnReceivedFtPort.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReceivedFtPort.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReceivedFtPort.Location = new System.Drawing.Point(4, 3);
            this.btnReceivedFtPort.Margin = new System.Windows.Forms.Padding(2);
            this.btnReceivedFtPort.Name = "btnReceivedFtPort";
            this.btnReceivedFtPort.Size = new System.Drawing.Size(157, 97);
            this.btnReceivedFtPort.TabIndex = 30;
            this.btnReceivedFtPort.Text = "Received FT Port";
            this.btnReceivedFtPort.UseVisualStyleBackColor = false;
            this.btnReceivedFtPort.Click += new System.EventHandler(this.btnReceivedFtPort_Click);
            // 
            // btnExit
            // 
            this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExit.BackColor = System.Drawing.Color.Red;
            this.btnExit.FlatAppearance.BorderSize = 0;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.btnExit.ForeColor = System.Drawing.Color.White;
            this.btnExit.Location = new System.Drawing.Point(981, 0);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(136, 38);
            this.btnExit.TabIndex = 20;
            this.btnExit.Text = "LOG OUT";
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // p7Inch
            // 
            this.p7Inch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(101)))), ((int)(((byte)(179)))));
            this.p7Inch.Controls.Add(this.btnRefPS7Inch);
            this.p7Inch.Controls.Add(this.panel3);
            this.p7Inch.Controls.Add(this.btnRefFT7Inch);
            this.p7Inch.Controls.Add(this.btnListDataTransfer7Inch);
            this.p7Inch.Controls.Add(this.btnListDataIssued7Inch);
            this.p7Inch.Controls.Add(this.btnWhTransfer7Inch);
            this.p7Inch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.p7Inch.Location = new System.Drawing.Point(0, 0);
            this.p7Inch.Name = "p7Inch";
            this.p7Inch.Size = new System.Drawing.Size(1117, 702);
            this.p7Inch.TabIndex = 29;
            // 
            // btnRefPS7Inch
            // 
            this.btnRefPS7Inch.BackColor = System.Drawing.Color.Orange;
            this.btnRefPS7Inch.FlatAppearance.BorderSize = 0;
            this.btnRefPS7Inch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRefPS7Inch.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRefPS7Inch.ForeColor = System.Drawing.Color.Black;
            this.btnRefPS7Inch.Location = new System.Drawing.Point(25, 62);
            this.btnRefPS7Inch.Margin = new System.Windows.Forms.Padding(2);
            this.btnRefPS7Inch.Name = "btnRefPS7Inch";
            this.btnRefPS7Inch.Size = new System.Drawing.Size(157, 97);
            this.btnRefPS7Inch.TabIndex = 29;
            this.btnRefPS7Inch.Text = "Refueling Pitstop";
            this.btnRefPS7Inch.UseVisualStyleBackColor = false;
            this.btnRefPS7Inch.Click += new System.EventHandler(this.btnRefPS7Inch_Click);
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.panel3.Controls.Add(this.pictureBox2);
            this.panel3.Location = new System.Drawing.Point(818, -5);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(293, 280);
            this.panel3.TabIndex = 28;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(-148, -151);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(579, 569);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 27;
            this.pictureBox2.TabStop = false;
            // 
            // btnRefFT7Inch
            // 
            this.btnRefFT7Inch.BackColor = System.Drawing.Color.Orange;
            this.btnRefFT7Inch.FlatAppearance.BorderSize = 0;
            this.btnRefFT7Inch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRefFT7Inch.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRefFT7Inch.ForeColor = System.Drawing.Color.Black;
            this.btnRefFT7Inch.Location = new System.Drawing.Point(186, 62);
            this.btnRefFT7Inch.Margin = new System.Windows.Forms.Padding(2);
            this.btnRefFT7Inch.Name = "btnRefFT7Inch";
            this.btnRefFT7Inch.Size = new System.Drawing.Size(157, 97);
            this.btnRefFT7Inch.TabIndex = 5;
            this.btnRefFT7Inch.Text = "Refueling FT";
            this.btnRefFT7Inch.UseVisualStyleBackColor = false;
            this.btnRefFT7Inch.Click += new System.EventHandler(this.btnRefFT_Click);
            // 
            // btnListDataTransfer7Inch
            // 
            this.btnListDataTransfer7Inch.BackColor = System.Drawing.Color.LawnGreen;
            this.btnListDataTransfer7Inch.FlatAppearance.BorderSize = 0;
            this.btnListDataTransfer7Inch.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btnListDataTransfer7Inch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnListDataTransfer7Inch.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnListDataTransfer7Inch.ForeColor = System.Drawing.Color.Black;
            this.btnListDataTransfer7Inch.Location = new System.Drawing.Point(186, 163);
            this.btnListDataTransfer7Inch.Margin = new System.Windows.Forms.Padding(2);
            this.btnListDataTransfer7Inch.Name = "btnListDataTransfer7Inch";
            this.btnListDataTransfer7Inch.Size = new System.Drawing.Size(157, 97);
            this.btnListDataTransfer7Inch.TabIndex = 12;
            this.btnListDataTransfer7Inch.Text = "List Data Transfer";
            this.btnListDataTransfer7Inch.UseVisualStyleBackColor = false;
            this.btnListDataTransfer7Inch.Click += new System.EventHandler(this.btnListDataTransfer_Click);
            // 
            // btnListDataIssued7Inch
            // 
            this.btnListDataIssued7Inch.BackColor = System.Drawing.Color.LawnGreen;
            this.btnListDataIssued7Inch.FlatAppearance.BorderSize = 0;
            this.btnListDataIssued7Inch.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btnListDataIssued7Inch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnListDataIssued7Inch.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnListDataIssued7Inch.ForeColor = System.Drawing.Color.Black;
            this.btnListDataIssued7Inch.Location = new System.Drawing.Point(25, 163);
            this.btnListDataIssued7Inch.Margin = new System.Windows.Forms.Padding(2);
            this.btnListDataIssued7Inch.Name = "btnListDataIssued7Inch";
            this.btnListDataIssued7Inch.Size = new System.Drawing.Size(157, 97);
            this.btnListDataIssued7Inch.TabIndex = 11;
            this.btnListDataIssued7Inch.Text = "List Data Issued";
            this.btnListDataIssued7Inch.UseVisualStyleBackColor = false;
            this.btnListDataIssued7Inch.Click += new System.EventHandler(this.btnListDataIssued_Click);
            // 
            // btnWhTransfer7Inch
            // 
            this.btnWhTransfer7Inch.BackColor = System.Drawing.Color.Orange;
            this.btnWhTransfer7Inch.FlatAppearance.BorderSize = 0;
            this.btnWhTransfer7Inch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnWhTransfer7Inch.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnWhTransfer7Inch.ForeColor = System.Drawing.Color.Black;
            this.btnWhTransfer7Inch.Location = new System.Drawing.Point(347, 62);
            this.btnWhTransfer7Inch.Margin = new System.Windows.Forms.Padding(2);
            this.btnWhTransfer7Inch.Name = "btnWhTransfer7Inch";
            this.btnWhTransfer7Inch.Size = new System.Drawing.Size(157, 97);
            this.btnWhTransfer7Inch.TabIndex = 6;
            this.btnWhTransfer7Inch.Text = "Warehouse Transfer";
            this.btnWhTransfer7Inch.UseVisualStyleBackColor = false;
            this.btnWhTransfer7Inch.Click += new System.EventHandler(this.btnWhTransfer_Click);
            // 
            // FMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(1117, 702);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.toolConnection);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.panelForm);
            this.Controls.Add(this.p7Inch);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MinimizeBox = false;
            this.Name = "FMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "IFlash Dekstop - Menu";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Activated += new System.EventHandler(this.FMain_Activated);
            this.Load += new System.EventHandler(this.FMain_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panelForm.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.pnlListData.ResumeLayout(false);
            this.pnlTransaksi.ResumeLayout(false);
            this.pnlPengirimanReceive.ResumeLayout(false);
            this.p7Inch.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem settingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelUsername;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelDistrik;
        public System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelStatus;
        private System.Windows.Forms.Timer timer1;
        
        private System.Windows.Forms.Button toolConnection;
        private System.Windows.Forms.Panel panelForm;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel lblAutoSyncStatus;
        private System.Windows.Forms.Panel p7Inch;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button btnRefFT7Inch;
        private System.Windows.Forms.Button btnListDataTransfer7Inch;
        private System.Windows.Forms.Button btnListDataIssued7Inch;
        private System.Windows.Forms.Button btnWhTransfer7Inch;
        private System.Windows.Forms.ToolStripMenuItem logToolStripMenuItem;
        private System.Windows.Forms.Button btnListDataLocal;
        private System.Windows.Forms.Button btnPengirimanReceive;
        private System.Windows.Forms.Button btnTransaksi;
        private System.Windows.Forms.Panel pnlTransaksi;
        private System.Windows.Forms.Button btnRefFT;
        private System.Windows.Forms.Button btnRefPitstop;
        private System.Windows.Forms.Button btnWhTransfer;
        private System.Windows.Forms.Button btnWhPengirim;
        private System.Windows.Forms.Panel pnlPengirimanReceive;
        private System.Windows.Forms.Button btnReceivedDirect;
        private System.Windows.Forms.Button btnPengirimanFtPortPama;
        private System.Windows.Forms.Button btnReceivedFtDarat;
        private System.Windows.Forms.Button btnReceivedFtPort;
        private System.Windows.Forms.Panel pnlListData;
        private System.Windows.Forms.Button btnListDataReceivedFTPort;
        private System.Windows.Forms.Button btnListDataReceiveFTDarat;
        private System.Windows.Forms.Button btnListDataPengirimanFTPortPama;
        private System.Windows.Forms.Button btnListDataReceiveDirect;
        private System.Windows.Forms.Button btnListDataTransfer;
        private System.Windows.Forms.Button btnListDataIssued;
        private System.Windows.Forms.Button btnPengirimanPOT;
        private System.Windows.Forms.Button btnReceiveDirectOwner;
        private System.Windows.Forms.Button btnListDataPengirimanPOT;
        private System.Windows.Forms.Button btnListDataReceiveDirectOwner;
        private System.Windows.Forms.Button btnRefPS7Inch;
    }
}