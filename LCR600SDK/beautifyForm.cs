﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Design;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LCR600SDK
{
    public class CustomButton : Button
    {
        //Fields
        private int borderSize = 0;
        private int borderRadius = 40;
        private Color borderColor = Color.PaleVioletRed;
        [Category("Custom Button Advanced")]
        public int BorderSize { get { return borderSize; } set { borderSize = value; this.Invalidate(); } }
        [Category("Custom Button Advanced")]
        public int BorderRadius
        {
            get
            {
                return borderRadius;
            }
            set
            {
                if (value <= this.Height)
                    borderRadius = value;
                else borderRadius = this.Height;
                this.Invalidate();
            }
        }
        [Category("Custom Button Advanced")]
        public Color BorderColor { get { return borderColor; } set { borderColor = value; this.Invalidate(); } }
        [Category("Custom Button Advanced")]
        public Color BackgroundColor { get { return this.BackColor; } set { this.BackColor = value; } }
        [Category("Custom Button Advanced")]
        public Color TextColor { get { return this.ForeColor; } set { this.ForeColor = value; } }

        //Constructor
        public CustomButton()
        {
            this.FlatStyle = FlatStyle.Flat;
            this.FlatAppearance.BorderSize = 0;
            this.Size = new Size(150, 40);
            this.BackColor = Color.MediumSlateBlue;
            this.ForeColor = Color.White;
            this.Resize += new EventHandler(Button_Resize);
        }



        //Methods
        private GraphicsPath GetFigrePath(RectangleF rect, float radius)
        {
            GraphicsPath path = new GraphicsPath();
            path.StartFigure();
            //upper left corner
            path.AddArc(rect.X, rect.Y, radius, radius, 180, 90);
            //upper right corner
            path.AddArc(rect.Width - radius, rect.Y, radius, radius, 270, 90);
            //lower right corner
            path.AddArc(rect.Width - radius, rect.Height - radius, radius, radius, 0, 90);
            //lower left corner
            path.AddArc(rect.X, rect.Height - radius, radius, radius, 90, 90);
            path.CloseFigure();

            return path;

        }
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;

            RectangleF rectSurface = new RectangleF(0, 0, this.Width, this.Height);
            RectangleF rectBorder = new RectangleF(1, 1, this.Width - 0.8F, this.Height - 1);

            if (borderRadius > 2) // Rounded button
            {
                using (GraphicsPath pathSurface = GetFigrePath(rectSurface, borderRadius))
                using (GraphicsPath pathBorder = GetFigrePath(rectBorder, borderRadius - 1F))
                using (Pen penSurface = new Pen(this.Parent.BackColor, 2))
                using (Pen penBorder = new Pen(borderColor, borderSize))
                {
                    penBorder.Alignment = PenAlignment.Inset;
                    //Button Surface
                    this.Region = new System.Drawing.Region(pathSurface);
                    //Draw surface border for HD result
                    e.Graphics.DrawPath(penSurface, pathSurface);

                    //Button border
                    if (borderSize > 1)
                        //Draw Control border
                        e.Graphics.DrawPath(penBorder, pathBorder);
                }
            }
            else //normal button
            {
                this.Region = new System.Drawing.Region(rectSurface);
                if (borderSize >= 1)
                {
                    using (Pen penBorder = new Pen(borderColor, borderSize))
                    {
                        penBorder.Alignment = PenAlignment.Inset;
                        e.Graphics.DrawRectangle(penBorder, 0, 0, this.Width - 1, this.Height - 1);
                    }
                }
            }

        }

        protected override void OnHandleCreated(EventArgs e)
        {
            base.OnHandleCreated(e);
            this.Parent.BackColorChanged += new EventHandler(Container_backColorChanged);

        }
        private void Container_backColorChanged(object sender, EventArgs e)
        {
            if (this.DesignMode)
                this.Invalidate();
        }
        private void Button_Resize(object sender, EventArgs e)
        {
            if (borderRadius > this.Height)
                borderRadius = this.Height;
        }
    }
    public class MyDateTimePicker : DateTimePicker
    {
        private const int SWP_NOMOVE = 0x0002;
        private const int DTM_First = 0x1000;
        private const int DTM_GETMONTHCAL = DTM_First + 8;
        private const int MCM_GETMINREQRECT = DTM_First + 9;

        [DllImport("uxtheme.dll")]
        private static extern int SetWindowTheme(IntPtr hWnd, string appName, string idList);
        [DllImport("user32.dll")]
        static extern IntPtr SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImport("user32.dll")]
        static extern IntPtr SendMessage(IntPtr hWnd, int Msg, int wParam, ref RECT lParam);
        [DllImport("user32.dll")]
        static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter,
        int X, int Y, int cx, int cy, int uFlags);
        [DllImport("User32.dll")]
        private static extern IntPtr GetParent(IntPtr hWnd);
        [StructLayout(LayoutKind.Sequential)]
        private struct RECT { public int L, T, R, B; }
        protected override void OnDropDown(EventArgs eventargs)
        {
            var hwndCalendar = SendMessage(this.Handle, DTM_GETMONTHCAL, 0, 0);
            SetWindowTheme(hwndCalendar, string.Empty, string.Empty);
            var r = new RECT();
            SendMessage(hwndCalendar, MCM_GETMINREQRECT, 0, ref r);
            var hwndDropDown = GetParent(hwndCalendar);
            SetWindowPos(hwndDropDown, IntPtr.Zero, 0, 0,
                r.R - r.L + 6, r.B - r.T + 6, SWP_NOMOVE);
            base.OnDropDown(eventargs);
        }
    }


    [DefaultEvent("OnSelectedIndexChanged")]
    public class CustomComboBox : UserControl
    {

        private Color backColor = Color.WhiteSmoke;
        private Color iconColor = Color.MediumSlateBlue;
        private Color listBackColor = Color.FromArgb(230, 228, 245);
        private Color listTextColor = Color.DimGray;
        private Color borderColor = Color.MediumSlateBlue;
        private int borderSize = 1;
        private CharacterCasing chars = CharacterCasing.Upper;


        private ComboBox cmbList;
        private Label lblText;
        private Button btnIcon;
        #region appearence
        public new Color BackColor
        {
            get
            {
                return backColor;
            }
            set
            {
                backColor = value;
                lblText.BackColor = backColor;
                btnIcon.BackColor = backColor;
            }
        }
        public Color IconColor
        {
            get
            {
                return iconColor;
            }

            set
            {
                iconColor = value;
                btnIcon.Invalidate();
            }
        }
        public Color ListBackColor
        {
            get
            {
                return listBackColor;
            }
            set
            {
                listBackColor = value;
                cmbList.BackColor = listBackColor;
            }
        }
        public Color ListTextColor
        {
            get
            {
                return listTextColor;
            }
            set
            {
                listTextColor = value;
                cmbList.ForeColor = listTextColor;
            }
        }
        public Color BorderColor
        {
            get
            {
                return borderColor;
            }
            set
            {
                borderColor = value;
                base.BackColor = borderColor;
            }
        }
        public int BorderSize
        {
            get
            {
                return borderSize;
            }
            set
            {
                borderSize = value;
                this.Padding = new Padding(borderSize);
                AdjustComboBoxDimension();
            }
        }

        public override Color ForeColor
        {
            get
            {
                return base.ForeColor;
            }
            set
            {
                base.ForeColor = value;
                lblText.ForeColor = value;
            }
        }

        public override Font Font
        {
            get
            {
                return base.Font;
            }

            set
            {
                base.Font = value;
                lblText.Font = value;
                cmbList.Font = value;
            }
                
        }
         
        public string Texts
        {
            get { return lblText.Text; }
            set { lblText.Text = value; }
        }
        #endregion

        #region data
        //
        // Summary:
        //     Gets or sets the path of the property to use as the actual value for the items
        //     in the System.Windows.Forms.ListControl.
        //
        // Returns:
        //     A System.String representing a single property name of the System.Windows.Forms.ListControl.DataSource
        //     property value, or a hierarchy of period-delimited property names that resolves
        //     to a property name of the final data-bound object. The default is an empty string
        //     ("").
        //
        // Exceptions:
        //   T:System.ArgumentException:
        //     The specified property path cannot be resolved through the object specified by
        //     the System.Windows.Forms.ListControl.DataSource property.
        [DefaultValue("")]
        [Editor("System.Windows.Forms.Design.DataMemberFieldEditor, System.Design, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(UITypeEditor))]        
        public string ValueMember { get { return cmbList.ValueMember; } set { cmbList.ValueMember = value; } }
        //
        // Summary:
        //     Gets or sets the property to display for this System.Windows.Forms.ListControl.
        //
        // Returns:
        //     A System.String specifying the name of an object property that is contained in
        //     the collection specified by the System.Windows.Forms.ListControl.DataSource property.
        //     The default is an empty string ("").
        [DefaultValue("")]
        [Editor("System.Windows.Forms.Design.DataMemberFieldEditor, System.Design, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(UITypeEditor))]
        
        [TypeConverter("System.Windows.Forms.Design.DataMemberFieldConverter, System.Design, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")]
        public string DisplayMember { get { return cmbList.DisplayMember; } set { cmbList.DisplayMember = value; } }
        //
        // Summary:
        //     Gets an object representing the collection of the items contained in this System.Windows.Forms.ComboBox.
        //
        // Returns:
        //     A System.Windows.Forms.ComboBox.ObjectCollection representing the items in the
        //     System.Windows.Forms.ComboBox.
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [Editor("System.Windows.Forms.Design.ListControlStringCollectionEditor, System.Design, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(UITypeEditor))]
        [Localizable(true)]
        [MergableProperty(false)]
        
        public ComboBox.ObjectCollection Items {
            get
            {
                return cmbList.Items;
            }
        }

        //
        // Summary:
        //     Gets or sets the data source for this System.Windows.Forms.ComboBox.
        //
        // Returns:
        //     An object that implements the System.Collections.IList interface or an System.Array.
        //     The default is null.
        [AttributeProvider(typeof(IListSource))]
        [DefaultValue(null)]
        [RefreshProperties(RefreshProperties.Repaint)]

        public object DataSource {
            get
            {
                return cmbList.DataSource;
            }
            set
            {
                cmbList.DataSource = value;
            }

        }
        //
        // Summary:
        //     Gets or sets currently selected item in the System.Windows.Forms.ComboBox.
        //
        // Returns:
        //     The object that is the currently selected item or null if there is no currently
        //     selected item.
        [Bindable(true)]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]

        public object SelectedItem {
            get { return cmbList.SelectedItem; }
            set { cmbList.SelectedItem = value; }
        }
        //
        // Summary:
        //     Gets or sets the index specifying the currently selected item.
        //
        // Returns:
        //     A zero-based index of the currently selected item. A value of negative one (-1)
        //     is returned if no item is selected.
        //
        // Exceptions:
        //   T:System.ArgumentOutOfRangeException:
        //     The specified index is less than or equal to -2. -or- The specified index is
        //     greater than or equal to the number of items in the combo box.
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]

        public int SelectedIndex {
            get { return cmbList.SelectedIndex; }
            set { cmbList.SelectedIndex = value; }
        }

        //
        // Summary:
        //     Gets or sets a custom System.Collections.Specialized.StringCollection to use
        //     when the System.Windows.Forms.ComboBox.AutoCompleteSource property is set to
        //     CustomSource.
        //
        // Returns:
        //     A System.Collections.Specialized.StringCollection to use with System.Windows.Forms.ComboBox.AutoCompleteSource.
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [Editor("System.Windows.Forms.Design.ListControlStringCollectionEditor, System.Design, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(UITypeEditor))]
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Localizable(true)]

        public AutoCompleteStringCollection AutoCompleteCustomSource {
            get { return cmbList.AutoCompleteCustomSource; }
            set { cmbList.AutoCompleteCustomSource = value; }
        }
        //
        // Summary:
        //     Gets or sets a value specifying the source of complete strings used for automatic
        //     completion.
        //
        // Returns:
        //     One of the values of System.Windows.Forms.AutoCompleteSource. The options are
        //     AllSystemSources, AllUrl, FileSystem, HistoryList, RecentlyUsedList, CustomSource,
        //     and None. The default is None.
        //
        // Exceptions:
        //   T:System.ComponentModel.InvalidEnumArgumentException:
        //     The specified value is not one of the values of System.Windows.Forms.AutoCompleteSource.
        [Browsable(true)]
        [DefaultValue(AutoCompleteSource.None)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        
        public AutoCompleteSource AutoCompleteSource {
            get { return cmbList.AutoCompleteSource; }
            set { cmbList.AutoCompleteSource = value; } 
        }
        //
        // Summary:
        //     Gets or sets an option that controls how automatic completion works for the System.Windows.Forms.ComboBox.
        //
        // Returns:
        //     One of the values of System.Windows.Forms.AutoCompleteMode. The values are System.Windows.Forms.AutoCompleteMode.Append,
        //     System.Windows.Forms.AutoCompleteMode.None, System.Windows.Forms.AutoCompleteMode.Suggest,
        //     and System.Windows.Forms.AutoCompleteMode.SuggestAppend. The default is System.Windows.Forms.AutoCompleteMode.None.
        //
        // Exceptions:
        //   T:System.ComponentModel.InvalidEnumArgumentException:
        //     The specified value is not one of the values of System.Windows.Forms.AutoCompleteMode.
        [Browsable(true)]
        [DefaultValue(AutoCompleteMode.None)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        
        public AutoCompleteMode AutoCompleteMode {
            get { return cmbList.AutoCompleteMode; }
            set { cmbList.AutoCompleteMode = value; }
        }
        #endregion
        public ComboBoxStyle DropDownStyle
        {
            get
            {
                return cmbList.DropDownStyle;
            }
            set
            {
                if (cmbList.DropDownStyle != ComboBoxStyle.Simple)
                    cmbList.DropDownStyle = value;
            }
        }

        public CharacterCasing CharCasing { get { return chars; } set { chars = value; } }

        public event EventHandler OnSelectedIndexChanged;

        public CustomComboBox()
        {
            cmbList = new ComboBox();
            lblText = new Label();
            btnIcon = new Button();

            cmbList.BackColor = listBackColor;
            cmbList.Font = new Font(this.Font.Name, 10f);
            cmbList.ForeColor = listTextColor;
            cmbList.SelectedIndexChanged += new EventHandler((ComboBox_SelectedIndexChanged));
            cmbList.TextChanged += new EventHandler(ComboBox_TextChanged);

            btnIcon.Dock = DockStyle.Right;
            btnIcon.FlatStyle = FlatStyle.Flat;
            btnIcon.FlatAppearance.BorderSize = 0;
            btnIcon.BackColor = backColor;
            btnIcon.Size = new Size(30, 30);
            btnIcon.Cursor = Cursors.Hand;
            btnIcon.Click += new EventHandler(Icon_Click);
            btnIcon.Paint += new PaintEventHandler(Icon_Paint);

            lblText.Dock = DockStyle.Fill;
            lblText.AutoSize = false;
            lblText.BackColor = backColor;
            lblText.TextAlign = ContentAlignment.MiddleLeft;
            lblText.Padding = new Padding(8, 0, 0, 0);
            lblText.Font = new Font(this.Font.Name, 10f);
            lblText.Click += new EventHandler(Surface_Click);
            lblText.MouseEnter += new EventHandler(Surface_MouseEnter);
            lblText.MouseLeave += new EventHandler(Surface_MouseLeave);

            this.Controls.Add(lblText);
            this.Controls.Add(btnIcon);
            this.Controls.Add(cmbList);
            this.MinimumSize = new Size(200, 30);
            this.Size = new Size(200, 30);
            this.ForeColor = Color.DimGray;
            this.Padding = new Padding(borderSize);
            base.BackColor = backColor;
            this.ResumeLayout();
            AdjustComboBoxDimension();
        }

        private void Surface_MouseLeave(object sender, EventArgs e)
        {
            this.OnMouseLeave(e);
        }

        private void Surface_MouseEnter(object sender, EventArgs e)
        {
            this.OnMouseEnter(e);
        }

        private void AdjustComboBoxDimension()
        {
            cmbList.Width = lblText.Width;
            cmbList.Location = new Point()
            {
                X = this.Width - this.Padding.Right - cmbList.Width,
                Y = lblText.Bottom - cmbList.Height
            };

        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            AdjustComboBoxDimension();
        }
        private void Surface_Click(object sender, EventArgs e)
        {
            this.OnClick(e);
            cmbList.Select();
            if (cmbList.DropDownStyle == ComboBoxStyle.DropDown)
            {
                cmbList.DroppedDown = true;
            }
        }

        private void Icon_Paint(object sender, PaintEventArgs e)
        {
            int iconWidth = 14;
            int iconHeigth = 6;
            var rectIcon = new Rectangle((btnIcon.Width - iconWidth) / 2,
                (btnIcon.Height - iconHeigth) / 2, iconWidth, iconHeigth);
            Graphics graph = e.Graphics;

            using (GraphicsPath path = new GraphicsPath())
            using (Pen pen = new Pen(iconColor, 2))
            {
                graph.SmoothingMode = SmoothingMode.AntiAlias;
                path.AddLine(rectIcon.X, rectIcon.Y, rectIcon.X + (iconWidth / 2), rectIcon.Bottom);
                path.AddLine(rectIcon.X + (iconWidth / 2), rectIcon.Bottom, rectIcon.Right, rectIcon.Y);
                graph.DrawPath(pen, path);
            }
        }

        private void Icon_Click(object sender, EventArgs e)
        {
            cmbList.Select();
            cmbList.DroppedDown = true;
        }

        private void ComboBox_TextChanged(object sender, EventArgs e)
        {
            lblText.Text = cmbList.Text;
        }

        private void ComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (OnSelectedIndexChanged != null)
            {
                OnSelectedIndexChanged.Invoke(sender, e);
            }
            lblText.Text = cmbList.Text;

        }
    }
}
