﻿namespace LCR600SDK
{
    partial class FAdminValidation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FAdminValidation));
            this.txtUSername = new System.Windows.Forms.TextBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblNotif = new System.Windows.Forms.Label();
            this.btnCancel = new LCR600SDK.CustomButton();
            this.btnLogin = new LCR600SDK.CustomButton();
            this.iconBtnHide = new FontAwesome.Sharp.IconButton();
            this.iconBtnShow = new FontAwesome.Sharp.IconButton();
            this.SuspendLayout();
            // 
            // txtUSername
            // 
            this.txtUSername.Enabled = false;
            this.txtUSername.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.txtUSername.Location = new System.Drawing.Point(74, 136);
            this.txtUSername.Name = "txtUSername";
            this.txtUSername.Size = new System.Drawing.Size(306, 35);
            this.txtUSername.TabIndex = 0;
            this.txtUSername.Visible = false;
            // 
            // txtPassword
            // 
            this.txtPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.txtPassword.Location = new System.Drawing.Point(74, 136);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(306, 35);
            this.txtPassword.TabIndex = 1;
            this.txtPassword.UseSystemPasswordChar = true;
            this.txtPassword.Click += new System.EventHandler(this.txtPassword_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Enabled = false;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.label1.Location = new System.Drawing.Point(183, 104);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 29);
            this.label1.TabIndex = 2;
            this.label1.Text = "PNRP";
            this.label1.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.label2.Location = new System.Drawing.Point(165, 104);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 29);
            this.label2.TabIndex = 3;
            this.label2.Text = "Password";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(93, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(291, 55);
            this.label3.TabIndex = 37;
            this.label3.Text = "Admin Login";
            // 
            // lblNotif
            // 
            this.lblNotif.AutoSize = true;
            this.lblNotif.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.lblNotif.ForeColor = System.Drawing.Color.Red;
            this.lblNotif.Location = new System.Drawing.Point(69, 174);
            this.lblNotif.Name = "lblNotif";
            this.lblNotif.Size = new System.Drawing.Size(89, 29);
            this.lblNotif.TabIndex = 38;
            this.lblNotif.Text = "lblNotif";
            this.lblNotif.Visible = false;
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.Red;
            this.btnCancel.BackgroundColor = System.Drawing.Color.Red;
            this.btnCancel.BorderColor = System.Drawing.Color.Empty;
            this.btnCancel.BorderRadius = 40;
            this.btnCancel.BorderSize = 0;
            this.btnCancel.FlatAppearance.BorderSize = 0;
            this.btnCancel.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnCancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Brown;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F);
            this.btnCancel.ForeColor = System.Drawing.Color.White;
            this.btnCancel.Location = new System.Drawing.Point(230, 207);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(150, 51);
            this.btnCancel.TabIndex = 36;
            this.btnCancel.Text = "CANCEL";
            this.btnCancel.TextColor = System.Drawing.Color.White;
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnLogin
            // 
            this.btnLogin.BackColor = System.Drawing.Color.Green;
            this.btnLogin.BackgroundColor = System.Drawing.Color.Green;
            this.btnLogin.BorderColor = System.Drawing.Color.Empty;
            this.btnLogin.BorderRadius = 40;
            this.btnLogin.BorderSize = 0;
            this.btnLogin.FlatAppearance.BorderSize = 0;
            this.btnLogin.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green;
            this.btnLogin.FlatAppearance.MouseOverBackColor = System.Drawing.Color.SeaGreen;
            this.btnLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F);
            this.btnLogin.ForeColor = System.Drawing.Color.White;
            this.btnLogin.Location = new System.Drawing.Point(74, 207);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(150, 51);
            this.btnLogin.TabIndex = 34;
            this.btnLogin.Text = "LOGIN";
            this.btnLogin.TextColor = System.Drawing.Color.White;
            this.btnLogin.UseVisualStyleBackColor = false;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // iconBtnHide
            // 
            this.iconBtnHide.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.iconBtnHide.BackColor = System.Drawing.Color.Transparent;
            this.iconBtnHide.FlatAppearance.BorderSize = 0;
            this.iconBtnHide.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.iconBtnHide.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.iconBtnHide.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconBtnHide.ForeColor = System.Drawing.SystemColors.ControlText;
            this.iconBtnHide.IconChar = FontAwesome.Sharp.IconChar.Lock;
            this.iconBtnHide.IconColor = System.Drawing.Color.Black;
            this.iconBtnHide.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.iconBtnHide.IconSize = 25;
            this.iconBtnHide.Location = new System.Drawing.Point(384, 132);
            this.iconBtnHide.Name = "iconBtnHide";
            this.iconBtnHide.Size = new System.Drawing.Size(51, 44);
            this.iconBtnHide.TabIndex = 40;
            this.iconBtnHide.UseVisualStyleBackColor = false;
            this.iconBtnHide.Click += new System.EventHandler(this.btnShowPass_Click);
            // 
            // iconBtnShow
            // 
            this.iconBtnShow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.iconBtnShow.BackColor = System.Drawing.Color.Transparent;
            this.iconBtnShow.FlatAppearance.BorderSize = 0;
            this.iconBtnShow.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.iconBtnShow.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.iconBtnShow.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconBtnShow.ForeColor = System.Drawing.SystemColors.ControlText;
            this.iconBtnShow.IconChar = FontAwesome.Sharp.IconChar.LockOpen;
            this.iconBtnShow.IconColor = System.Drawing.Color.Black;
            this.iconBtnShow.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.iconBtnShow.IconSize = 25;
            this.iconBtnShow.Location = new System.Drawing.Point(384, 132);
            this.iconBtnShow.Name = "iconBtnShow";
            this.iconBtnShow.Size = new System.Drawing.Size(51, 44);
            this.iconBtnShow.TabIndex = 39;
            this.iconBtnShow.UseVisualStyleBackColor = false;
            this.iconBtnShow.Click += new System.EventHandler(this.btnShowPass_Click);
            // 
            // FAdminValidation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(473, 270);
            this.ControlBox = false;
            this.Controls.Add(this.iconBtnHide);
            this.Controls.Add(this.iconBtnShow);
            this.Controls.Add(this.lblNotif);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnLogin);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.txtUSername);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FAdminValidation";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Need Admin";
            this.Load += new System.EventHandler(this.FAdminValidation_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtUSername;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private CustomButton btnLogin;
        private System.Windows.Forms.Label label3;
        private CustomButton btnCancel;
        private System.Windows.Forms.Label lblNotif;
        private FontAwesome.Sharp.IconButton iconBtnHide;
        private FontAwesome.Sharp.IconButton iconBtnShow;
    }
}