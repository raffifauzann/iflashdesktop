﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LCR600SDK
{
    class WhTranfer
    {
        public String job_row_id { get; set; }
        public String xfer_code { get; set; }
        public String xfer_date { get; set; }
        public String dstrct_code { get; set; }
        public String employee_id { get; set; }
        public String issued_whouse { get; set; }
        public String received_whouse { get; set; }
        public String stock_code { get; set; }
        public double qty_xfer { get; set; }
        public String shift { get; set; }
        public String fmeter_serial_no { get; set; }
        public double faktor_meter { get; set; }
        public double fmeter_begin { get; set; }
        public double fmeter_end { get; set; }
        public String created_by { get; set; }
        public String xfer_by { get; set; }
        public String received_by { get; set; }
        public int is_submit { get; set; }
        public String issued_whouse_type { get; set; }
        public String received_whouse_type { get; set; }
        public DateTime mod_date { get; set; }
        public DateTime created_date { get; set; }
        public String timezone { get; set; }
        public int syncs { get; set; }
    }
}
