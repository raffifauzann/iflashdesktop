﻿using Flurl.Http;
using MySql.Data.MySqlClient;
using System.Data.SQLite;
using System.IO;
using System.Reflection;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.NetworkInformation;
using System.Data;

namespace LCR600SDK
{
    public partial class FMain : Form
    {
        private string myConnectionString = string.Empty;
        private string pUrl_service = string.Empty;
        private bool statusOnline = false;
        private bool auto_sync_on_off = false;
        private string appExit = string.Empty;
        private string version = string.Empty;
        PasswordEncrypDecrypt decryptor = new PasswordEncrypDecrypt();
        WriteLogText log = new WriteLogText();
        public FMain()
        {
            InitializeComponent();
        }

        private async void FMain_Load(object sender, EventArgs e)
        {         
            var _proper = Properties.Settings.Default;
            pUrl_service = _proper.url_service;
            timer1.Interval = int.Parse(_proper.interval_auto_sync);
            if (_proper.device_monitor == "10")
            {
                panelForm.Show();
                pnlTransaksi.Hide();
                pnlPengirimanReceive.Hide();
                pnlListData.Hide();
                p7Inch.Hide();
                this.Size = new Size(952, 741);
            }
            else if (_proper.device_monitor == "7")
            {
                p7Inch.Show();
                panelForm.Hide();
                this.MaximizeBox = false;
                this.Size = new Size(652, 541);
            }

            var passPhrase = "iFlashDesktop";
            var decryp = decryptor.DecryptPassword(_proper.db_pass, passPhrase);
            myConnectionString = "Data Source=" + Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "SQLite", "db_iflash.db") + " ;New=False;";
            //version = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            this.Text = "iFLash Desktop " + GlobalModel.app_version;
            if (_proper.menu_refueling_pitstop == "True")
            {
                btnRefPitstop.Show();
                btnRefPS7Inch.Show();
            }
            else
            {
                btnRefPitstop.Hide();
                btnRefPS7Inch.Hide();

            }

            if (_proper.menu_refueling_ft == "True")
            {
                btnRefFT.Show();
                btnRefFT7Inch.Show();
            }
            else
            {
                btnRefFT.Hide();
                btnRefFT7Inch.Hide();
            }

            if (_proper.menu_warehouse_transfer == "True")
            {
                btnWhTransfer.Show();
                btnWhTransfer7Inch.Show();
            }
            else
            {
                btnWhTransfer.Hide();
                btnWhTransfer7Inch.Hide();
            }

            if (_proper.menu_warehouse_transfer_pengirim == "True")
            {
                btnWhPengirim.Show();
            }
            else
            {
                btnWhPengirim.Hide();
            }

            if (_proper.menu_received_ft_port == "True")
            {
                btnReceivedFtPort.Show();
                this.Text = "iFLash Desktop " + GlobalModel.app_version + " Received";
            }
            else
            {
                btnReceivedFtPort.Hide();
                this.Text = "iFLash Desktop " + GlobalModel.app_version;
            }

            if (_proper.menu_received_ft_darat == "True")
            {
                btnReceivedFtDarat.Show();
            }
            else
            {
                btnReceivedFtDarat.Hide();
            }

            if (_proper.menu_pengiriman_ft_port_pama == "True")
            {
                btnPengirimanFtPortPama.Show();
            }
            else
            {
                btnPengirimanFtPortPama.Hide();
            }

            if (_proper.menu_list_data_issued == "True")
            {
                btnListDataIssued.Show();
                btnListDataIssued7Inch.Show();
            }
            else
            {
                btnListDataIssued.Hide();
                btnListDataIssued7Inch.Hide();
            }

            if (_proper.menu_list_data_transfer == "True")
            {
                btnListDataTransfer.Show();
                btnListDataTransfer7Inch.Show();
            }
            else
            {
                btnListDataTransfer.Hide();
                btnListDataTransfer7Inch.Hide();
            }

            if (_proper.menu_receive_direct == "True")
            {
                btnReceivedDirect.Show();                
            }
            else
            {
                btnReceivedDirect.Hide();                
            }

            if (_proper.menu_list_receive_direct == "True")
            {
                btnListDataReceiveDirect.Show();
            }
            else
            {
                btnListDataReceiveDirect.Hide();
            }

            if (_proper.menu_list_data_pengiriman_ft_port_pama == "True")
            {
                btnListDataPengirimanFTPortPama.Show();
            }
            else
            {
                btnListDataPengirimanFTPortPama.Hide();
            }

            if (_proper.menu_list_data_received_ft_darat == "True")
            {
                btnListDataReceiveFTDarat.Show();
            }
            else
            {
                btnListDataReceiveFTDarat.Hide();
            }

            if (_proper.menu_list_data_received_ft_port == "True")
            {
                btnListDataReceivedFTPort.Show();
            }
            else
            {
                btnListDataReceivedFTPort.Hide();
            }

            if (_proper.menu_receive_direct_from_owner == "True")
            {
                btnReceiveDirectOwner.Show();
            }
            else
            {
                btnReceiveDirectOwner.Hide();
            }

            if (_proper.menu_pengiriman_pot == "True")
            {
                btnPengirimanPOT.Show();
            }
            else
            {
                btnPengirimanPOT.Hide();
            }

            if (_proper.menu_list_data_receive_direct_owner == "True")
            {
                btnListDataReceiveDirectOwner.Show();
            }
            else
            {
                btnListDataReceiveDirectOwner.Hide();
            }

            if (_proper.menu_list_data_pengiriman_pot == "True")
            {
                btnListDataPengirimanPOT.Show();
            }
            else
            {
                btnListDataPengirimanPOT.Hide();
            }

            var checkconnect = await checkConnectivity();
            if (checkconnect.Contains("Supply Service"))
            //if (_proper.connection == "ONLINE")
            {
                toolConnection.Text = "ONLINE";
                toolConnection.BackColor = Color.Green;
                toolConnection.ForeColor = Color.White;
                statusOnline = true;
            }
            else
            {
                toolConnection.Text = "OFFLINE";
                toolConnection.BackColor = Color.Red;
                toolConnection.ForeColor = Color.White;
                statusOnline = false;
            }

            auto_sync_on_off = _proper.auto_sync_on_off == "ON" ? true : false;
            lblAutoSyncStatus.Text = "Auto Sync " + _proper.auto_sync_on_off;
            
            loadInitData();
            log.WriteToFile("logLCR600.txt", "Load Form  Main Start System Checking...");
            var f = new FSystemChecking();
            f.ShowDialog();
            
        }
        private void checkSetingWarehouse()
        {
            SQLiteConnection conn;

            conn = new SQLiteConnection(myConnectionString);
            if (conn.State != ConnectionState.Open) conn.Open();
            string query = string.Empty;

            query = "SELECT * FROM tbl_setting";
            SQLiteCommand cmds = new SQLiteCommand(query, conn);
            SQLiteDataReader reader = cmds.ExecuteReader();
            var pid = string.Empty;
            var whouseid =string.Empty;
            var whousename = string.Empty;
            var sncode = string.Empty;
            var meterfaktor = string.Empty;
            var shift1 = string.Empty;
            var shift2 = string.Empty;
            
            while (reader.Read())
            {
                pid = reader["Pid"].ToString();
                whouseid = reader["WhouseId"].ToString();
                whousename = reader["WhouseName"].ToString();
                sncode = reader["SnCode"].ToString();
                meterfaktor = reader["MeterFaktor"].ToString();
                shift1 = reader["startShift1"].ToString();
                shift2 = reader["startShift2"].ToString();                
            
            }
            if (string.IsNullOrEmpty(pid))
            {
                var f = new FPopUpNotification("Setting Warehouse Kosong, Mohon diisi dahulu","Setting Warehouse Kosong!");
                f.ShowDialog();
            }
            else if (string.IsNullOrEmpty(whouseid))
            {
                var f = new FPopUpNotification("Setting Warehouse Id Kosong, Mohon diisi dahulu", "Setting Warehouse Kosong!");
                f.ShowDialog();
            }
            else if (string.IsNullOrEmpty(whousename))
            {
                var f = new FPopUpNotification("Setting Warehouse Name Kosong, Mohon diisi dahulu", "Setting Warehouse Kosong!");
                f.ShowDialog();
            }
            else if (string.IsNullOrEmpty(sncode))
            {
                var f = new FPopUpNotification("Setting Warehouse SN Code Kosong, Mohon diisi dahulu", "Setting Warehouse Kosong!");
                f.ShowDialog();
            }
            else if (string.IsNullOrEmpty(shift1))
            {
                var f = new FPopUpNotification("Setting Warehouse Shift 1 Kosong, Mohon diisi dahulu", "Setting Warehouse Kosong!");
                f.ShowDialog();
            }
            else if (string.IsNullOrEmpty(shift2))
            {
                var f = new FPopUpNotification("Setting Warehouse Shift 2 Kosong, Mohon diisi dahulu", "Setting Warehouse Kosong!");
                f.ShowDialog();
            }
            reader.Dispose();
            if (conn.State == ConnectionState.Open) conn.Close();

        }
        void loadInitData()
        {
            var initData = GlobalModel.GlobalVar;
            toolStripStatusLabelUsername.Text = initData.DataEmp.Nrp;
            toolStripStatusLabelDistrik.Text = initData.DataEmp.DstrctCode;
        }

        private void refuellingMandiriToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var f = new FTransaction();
            f.ShowDialog();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var result = MessageBox.Show("Sure?", "Log Out", MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                this.Close();
                var f = new FLogin();
                f.ShowDialog();
            }
        }

        private void listDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var f = new FListData();
            f.MdiParent = this;
            f.Show();
        }

        private string getTokenFromLocal()
        {
            SQLiteConnection conn;
            conn = new SQLiteConnection(myConnectionString);
            if (conn.State != ConnectionState.Open) conn.Open();
            var token = "";
            var expired_date = "";
            var username = toolStripStatusLabelUsername.Text;
            var query = "SELECT * FROM tbl_user_login where nrp=" + username;
            SQLiteCommand cmd = new SQLiteCommand(query, conn);
            SQLiteDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                token = reader["token"].ToString();
                expired_date = reader["expired_date"].ToString();
            }

            reader.Dispose();
            if (conn.State == ConnectionState.Open) conn.Close();

            return token + "|" + expired_date;
        }
        private async void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                if (auto_sync_on_off)
                {
                    var checkconnect = await checkConnectivity();
                    if (checkconnect.Contains("Supply Service"))
                    {
                        onSyncAsync();
                        onSyncTransferAsync();
                        onSyncAsyncReceiveDirect();
                        onSyncPengirimanFTPortPamaAsync();
                        onSyncReceiveFTDaratAsync();
                        onSyncReceiveFTPortAsync();


                    }
                    else if (checkconnect.Contains("Supply Service"))
                    {
                        toolConnection.Text = "Online";
                        toolConnection.BackColor = Color.Green;
                    }
                    else
                    {
                        toolConnection.Text = "Offline";
                        toolConnection.BackColor = Color.Red;
                    }
                }                

            }
            catch (Exception ex)
            {
                log.WriteToFile("logLCR600.txt", "timer1_Tick() exception: " + ex.Message);
                save_error_log(ex.ToString(), "timer1_Tick()");
            }

        }


        

        public async Task<string> checkConnectivity()
        {
            try
            {
                WebClient requestCek = new WebClient();
                var str = requestCek.DownloadString(pUrl_service);
                return str;
                //var request = (HttpWebRequest)WebRequest.Create(pUrl_service);
                //request.Timeout = timeoutCheckConnection;
                //return request.GetResponse();
            }
            catch (WebException ex)
            {
                return "";
            }
        }
       

        void save_error_log(string data, string actions)
        {
            try
            {
                DateTime localDate = DateTime.Now;
                var _item = new tbl_log();
                var initData = GlobalModel.GlobalVar;

                _item.pid = System.Guid.NewGuid().ToString();
                _item.actions = $"District {initData.DataEmp.DstrctCode}/FMain/{actions}";
                _item.message = $"{data}";
                _item.mod_date = localDate;
                _item.mod_by = initData.DataEmp.Nrp;

                SQLiteConnection conn;

                conn = new SQLiteConnection(myConnectionString);
                if (conn.State != ConnectionState.Open) conn.Open();
                string query = string.Empty;
                query = "INSERT INTO tbl_log(pid,actions,message,mod_date,mod_by)"
                      + " VALUES(@pid,@actions,@message,@mod_date,@mod_by)";

                SQLiteCommand cmd = new SQLiteCommand(query, conn);

                cmd.Parameters.Add(new SQLiteParameter("@pid", _item.pid));
                cmd.Parameters.Add(new SQLiteParameter("@actions", _item.actions));
                cmd.Parameters.Add(new SQLiteParameter("@message", _item.message));
                cmd.Parameters.Add(new SQLiteParameter("@mod_date", _item.mod_date));
                cmd.Parameters.Add(new SQLiteParameter("@mod_by", _item.mod_by));
                cmd.ExecuteNonQuery();
                if (conn.State == ConnectionState.Open) conn.Close();
            }
            catch (Exception)
            {
            }
        }

        async Task onSyncTransferAsync()
        {
            try
            {
                var _proper = Properties.Settings.Default;
                var _auth = GlobalModel.loginModel;

                String _url = $"{pUrl_service}api/IssuingService/SyncTransfer";
                List<whtranfer> SendListTranfer = new List<whtranfer>();
                var getData = getListTransfer();
                for (int i = 0; i < getData.Count; i++)
                {

                    SendListTranfer.Add(getData[i]);

                    //if (SendListIssuing.Count == 10 /*&& int.Parse(counter[0]) != 0*/)
                    if (SendListTranfer.Count == int.Parse(_proper.data_per_kirim) /*&& int.Parse(counter[0]) != 0*/)
                    {
                        ClsIssuingTransfer data = await _url.WithHeaders(new { Authorization = _auth.token }).PostJsonAsync(SendListTranfer).ReceiveJson<ClsIssuingTransfer>();

                        save_error_log(data.remarks, "onSyncTransferAsync()");
                        updateTableTransferSync(data);
                        if (Application.OpenForms["FListDataTransfer"] != null)
                            (Application.OpenForms["FListDataTransfer"] as FListDataTransfer).loadGrid();
                        SendListTranfer = new List<whtranfer>();
                    }
                    else
                    {
                        if (i == getData.Count - 1 /*&& SendListIssuing.Count == int.Parse(counter[1])*/)
                        {
                            ClsIssuingTransfer data = await _url.WithHeaders(new { Authorization = _auth.token }).PostJsonAsync(getListTransfer()).ReceiveJson<ClsIssuingTransfer>();
                            save_error_log(data.remarks, "onSyncTransferAsync()");
                            updateTableTransferSync(data);
                            if (Application.OpenForms["FListDataTransfer"] != null)
                                (Application.OpenForms["FListDataTransfer"] as FListDataTransfer).loadGrid();

                        }
                    }
                }
                //ClsIssuingTransfer data = await _url.WithHeaders(new { Authorization = _auth.token }).PostJsonAsync(getListTransfer()).ReceiveJson<ClsIssuingTransfer>();

                //save_error_log(data.remarks, "onSyncTransferAsync()");
                //updateTableTransferSync(data);
                //if (Application.OpenForms["FListDataTransfer"] != null)
                //    (Application.OpenForms["FListDataTransfer"] as FListDataTransfer).loadGrid();
            }
            catch (FlurlHttpException ex)
            {
                var resp = ex.Message;
                if (ex.InnerException != null)
                {
                    resp = ex.InnerException.Message;
                }
                //var resp = await ex.GetResponseStringAsync();                
                log.WriteToFile("logLCR600.txt", ex.Message);                
                save_error_log(resp, "onSyncTransferAsync()");
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);

                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(ex.Message, "onSyncTransferAsync()");
            }
        }
        async Task onSyncAsync()
        {
            var _auth = GlobalModel.loginModel;
            var _proper = Properties.Settings.Default;
            String _url = $"{pUrl_service}api/IssuingService/SyncRefuelingMandiri";
            try
            {
                List<tbl_logsheet_detail> SendListIssuing = new List<tbl_logsheet_detail>();
                var getData = getListIssuing();             

                for (int i = 0; i < getData.Count; i++)
                {

                    SendListIssuing.Add(getData[i]);

                    //if (SendListIssuing.Count == 10 /*&& int.Parse(counter[0]) != 0*/)
                    if (SendListIssuing.Count == int.Parse(_proper.data_per_kirim) /*&& int.Parse(counter[0]) != 0*/)
                    {
                        ClsIssuing data = await _url.WithHeaders(new { Authorization = _auth.token }).PostJsonAsync(SendListIssuing).ReceiveJson<ClsIssuing>();

                        updateTableSync(SendListIssuing, data);
                        log.WriteToFile("logLCR600.txt", data.remarks);
                        save_error_log(data.remarks, "onSyncAsync()");
                        if (Application.OpenForms["FListData"] != null)
                            (Application.OpenForms["FListData"] as FListData).loadGrid();
                        SendListIssuing = new List<tbl_logsheet_detail>();
                    }
                    else
                    {
                        if (i == getData.Count - 1 /*&& SendListIssuing.Count == int.Parse(counter[1])*/)
                        {
                            ClsIssuing data = await _url.WithHeaders(new { Authorization = _auth.token }).PostJsonAsync(SendListIssuing).ReceiveJson<ClsIssuing>();
                            updateTableSync(SendListIssuing, data);
                            log.WriteToFile("logLCR600.txt", data.remarks);
                            save_error_log(data.remarks, "onSyncAsync()");
                            if (Application.OpenForms["FListData"] != null)
                                (Application.OpenForms["FListData"] as FListData).loadGrid();

                        }
                    }
                }

            }
            catch (FlurlHttpException ex)
            {
                var resp = ex.Message;
                if (ex.InnerException != null)
                {
                    resp = ex.InnerException.Message;
                }
                //var resp = await ex.GetResponseStringAsync();
                
                log.WriteToFile("logLCR600.txt", ex.Message);                
                save_error_log(resp, "onSyncAsync()");
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);

                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(ex.Message, "onSyncAsync()");
            }



        }
        async Task onSyncAsyncReceiveDirect()
        {

            var _proper = Properties.Settings.Default;
            var _auth = GlobalModel.loginModel;

            String _url = $"{pUrl_service}api/FuelService/SyncReceiveDirectDesktop";
            try
            {
                List<tbl_t_receive_direct> SendListIssuing = new List<tbl_t_receive_direct>();
                var getData = getListReceiveDirect();
                for (int i = 0; i < getData.Count; i++)
                {

                    SendListIssuing.Add(getData[i]);

                    //if (SendListIssuing.Count == 10 /*&& int.Parse(counter[0]) != 0*/)
                    if (SendListIssuing.Count == int.Parse(_proper.data_per_kirim) /*&& int.Parse(counter[0]) != 0*/)
                    {
                        ClsReceiveDirect data = await _url.WithHeaders(new { Authorization = _auth.token }).PostJsonAsync(SendListIssuing).ReceiveJson<ClsReceiveDirect>();

                        updateTableReceiveDirectSync(SendListIssuing, data);
                        log.WriteToFile("logLCR600.txt", data.remarks);
                        save_error_log(data.remarks, "onSyncAsyncReceiveDirect()");
                        if (Application.OpenForms["FListDataReceiveDirect"] != null)
                            (Application.OpenForms["FListDataReceiveDirect"] as FListDataReceiveDirect).loadGrid();
                        SendListIssuing = new List<tbl_t_receive_direct>();
                    }
                    else
                    {
                        if (i == getData.Count - 1 /*&& SendListIssuing.Count == int.Parse(counter[1])*/)
                        {
                            ClsReceiveDirect data = await _url.WithHeaders(new { Authorization = _auth.token }).PostJsonAsync(SendListIssuing).ReceiveJson<ClsReceiveDirect>();
                            updateTableReceiveDirectSync(SendListIssuing, data);
                            log.WriteToFile("logLCR600.txt", data.remarks);
                            save_error_log(data.remarks, "onSyncAsyncReceiveDirect()");
                            if (Application.OpenForms["FListDataReceiveDirect"] != null)
                                (Application.OpenForms["FListDataReceiveDirect"] as FListDataReceiveDirect).loadGrid();

                        }
                    }
                }                
            }
            catch (FlurlHttpException ex)
            {
                var resp = ex.Message;
                if (ex.InnerException != null)
                {
                    resp = ex.InnerException.Message;
                }
                //var resp = await ex.GetResponseStringAsync();
                
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(resp, "onSyncAsyncReceiveDirect()");                
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
                
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(ex.Message, "onSyncAsyncReceiveDirect()");                
            }



        }
        async Task onSyncPengirimanFTPortPamaAsync()
        {
            var _proper = Properties.Settings.Default;
            var _auth = GlobalModel.loginModel;

            String _url = $"{pUrl_service}api/FuelService/SyncSendRitasiToMaintankDesktop";
            try
            {
                List<tbl_t_send_maintank_ritasi> SendListIssuing = new List<tbl_t_send_maintank_ritasi>();
                var getData = getListPengirimanFTPortPama();


                var listFailedPidReceive = new List<List<string>>();
                if (getData.Count() == 0)
                {

                }
                else
                {

                    for (int i = 0; i < getData.Count; i++)
                    {

                        SendListIssuing.Add(getData[i]);

                        //if (SendListIssuing.Count == 1 /*&& int.Parse(counter[0]) != 0*/)
                        if (SendListIssuing.Count == int.Parse(_proper.data_per_kirim) /*&& int.Parse(counter[0]) != 0*/)
                        {
                            var json = Newtonsoft.Json.JsonConvert.SerializeObject(SendListIssuing);
                            ClsRitasiToMaintank data = await _url.WithHeaders(new { Authorization = _auth.token }).PostJsonAsync(SendListIssuing).ReceiveJson<ClsRitasiToMaintank>();                            
                            updateTablePengirimanFTPortPamaSync(SendListIssuing, data);
                            if (Application.OpenForms["FListDataPengirimanFTPortPama"] != null)
                                (Application.OpenForms["FListDataPengirimanFTPortPama"] as FListDataPengirimanFTPortPama).loadGrid();
                            log.WriteToFile("logLCR600.txt", data.remarks);
                            save_error_log(data.remarks, "onSyncPengirimanFTPortPamaAsync()");
                            SendListIssuing = new List<tbl_t_send_maintank_ritasi>();
                        }
                        else
                        {
                            if (i == getData.Count - 1 /*&& SendListIssuing.Count == int.Parse(counter[1])*/)
                            {
                                ClsRitasiToMaintank data = await _url.WithHeaders(new { Authorization = _auth.token }).PostJsonAsync(SendListIssuing).ReceiveJson<ClsRitasiToMaintank>();
                                
                                updateTablePengirimanFTPortPamaSync(SendListIssuing, data);
                                if (Application.OpenForms["FListDataPengirimanFTPortPama"] != null)
                                    (Application.OpenForms["FListDataPengirimanFTPortPama"] as FListDataPengirimanFTPortPama).loadGrid();                                
                                log.WriteToFile("logLCR600.txt", data.remarks);
                                save_error_log(data.remarks, "onSyncPengirimanFTPortPamaAsync()");


                            }
                        }

                    }
                }
            }
            catch (FlurlHttpException ex)
            {
                var resp = ex.Message;
                if (ex.InnerException != null)
                {
                    resp = ex.InnerException.Message;
                }
                //var resp = await ex.GetResponseStringAsync();
                
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(resp, "onSyncPengirimanFTPortPamaAsync()");

            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
                
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(ex.Message, "onSyncPengirimanFTPortPamaAsync()");

            }



        }
        async Task onSyncReceiveFTDaratAsync()
        {
            var _proper = Properties.Settings.Default;
            var _auth = GlobalModel.loginModel;

            String _url = $"{pUrl_service}api/FuelService/SyncReceiveRitasiToMaintankDesktop";
            try
            {
                List<tbl_t_receive_maintank_ritasi> SendListIssuing = new List<tbl_t_receive_maintank_ritasi>();
                var getData = getListReceiveFTDarat();


                var listFailedPidReceive = new List<List<string>>();
                if (getData.Count() == 0)
                {
                    
                }
                else
                {
                    
                    for (int i = 0; i < getData.Count; i++)
                    {

                        SendListIssuing.Add(getData[i]);

                        //if (SendListIssuing.Count == 1 /*&& int.Parse(counter[0]) != 0*/)
                        if (SendListIssuing.Count == int.Parse(_proper.data_per_kirim) /*&& int.Parse(counter[0]) != 0*/)
                        {
                            var json = Newtonsoft.Json.JsonConvert.SerializeObject(SendListIssuing);
                            ClsRitasiToMaintank data = await _url.WithHeaders(new { Authorization = _auth.token }).PostJsonAsync(SendListIssuing).ReceiveJson<ClsRitasiToMaintank>();                            
                            updateTableReceiveFTDaratSync(SendListIssuing, data);
                            if (Application.OpenForms["FListDataReceiveFTDarat"] != null)
                                (Application.OpenForms["FListDataReceiveFTDarat"] as FListDataReceiveFTDarat).loadGrid();
                            log.WriteToFile("logLCR600.txt", data.remarks);
                            save_error_log(data.remarks, "onSyncReceiveFTDaratAsync()");
                            SendListIssuing = new List<tbl_t_receive_maintank_ritasi>();
                        }
                        else
                        {
                            if (i == getData.Count - 1 /*&& SendListIssuing.Count == int.Parse(counter[1])*/)
                            {
                                ClsRitasiToMaintank data = await _url.WithHeaders(new { Authorization = _auth.token }).PostJsonAsync(SendListIssuing).ReceiveJson<ClsRitasiToMaintank>();                                
                                updateTableReceiveFTDaratSync(SendListIssuing, data);
                                if (Application.OpenForms["FListDataReceiveFTDarat"] != null)
                                    (Application.OpenForms["FListDataReceiveFTDarat"] as FListDataReceiveFTDarat).loadGrid();
                                log.WriteToFile("logLCR600.txt", data.remarks);
                                save_error_log(data.remarks, "onSyncReceiveFTDaratAsync()");

                            }
                        }
                        
                    }
                }
            }
            catch (FlurlHttpException ex)
            {
                var resp = ex.Message;
                if (ex.InnerException != null)
                {
                    resp = ex.InnerException.Message;
                }
                //var resp = await ex.GetResponseStringAsync();
                
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(resp, "onSyncAsync()");                
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
                
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(ex.Message, "onSyncAsync()");                
            }



        }
        async Task onSyncReceiveFTPortAsync()
        {
            var _proper = Properties.Settings.Default;
            var _auth = GlobalModel.loginModel;

            String _url = $"{pUrl_service}api/FuelService/SyncReceiveRitasiToMaintankDesktop";
            try
            {
                List<tbl_t_receive_maintank_ritasi> SendListIssuing = new List<tbl_t_receive_maintank_ritasi>();
                var getData = getListReceiveFTPort();


                var listFailedPidReceive = new List<List<string>>();
                if (getData.Count() == 0)
                {                    
                }
                else
                {                    
                    for (int i = 0; i < getData.Count; i++)
                    {

                        SendListIssuing.Add(getData[i]);

                        //if (SendListIssuing.Count == 1 /*&& int.Parse(counter[0]) != 0*/)
                        if (SendListIssuing.Count == int.Parse(_proper.data_per_kirim) /*&& int.Parse(counter[0]) != 0*/)
                        {
                            var json = Newtonsoft.Json.JsonConvert.SerializeObject(SendListIssuing);
                            ClsRitasiToMaintank data = await _url.WithHeaders(new { Authorization = _auth.token }).PostJsonAsync(SendListIssuing).ReceiveJson<ClsRitasiToMaintank>();                                                       
                            updateTableReceiveFTPortSync(SendListIssuing, data);
                            log.WriteToFile("logLCR600.txt", "Data Remarks: " + data.remarks + " List Remarks: " + String.Join("','", data.list_remarks));
                            save_error_log("Data Remarks: " + data.remarks + " List Remarks: " + String.Join("','", data.list_remarks), "onSyncReceiveFTPortAsync()");
                            SendListIssuing = new List<tbl_t_receive_maintank_ritasi>();
                        }
                        else
                        {
                            if (i == getData.Count - 1 /*&& SendListIssuing.Count == int.Parse(counter[1])*/)
                            {
                                ClsRitasiToMaintank data = await _url.WithHeaders(new { Authorization = _auth.token }).PostJsonAsync(SendListIssuing).ReceiveJson<ClsRitasiToMaintank>();

                                updateTableReceiveFTPortSync(SendListIssuing, data);
                                log.WriteToFile("logLCR600.txt", "Data Remarks: " + data.remarks + " List Remarks: " + String.Join("','", data.list_remarks));
                                save_error_log("Data Remarks: " + data.remarks + " List Remarks: " + String.Join("','", data.list_remarks), "onSyncReceiveFTPortAsync()");

                            }
                        }
                     
                    }
                }
            }
            catch (FlurlHttpException ex)
            {
                var resp = ex.Message;
                if (ex.InnerException != null)
                {
                    resp = ex.InnerException.Message;
                }
                //var resp = await ex.GetResponseStringAsync();                
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(resp, "onSyncReceiveFTPortAsync()");                
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);                
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(ex.Message, "onSyncReceiveFTPortAsync()");                
            }



        }
        async Task onSyncReceiveDirectOwnerAsync()
        {
            var _proper = Properties.Settings.Default;
            var _auth = GlobalModel.loginModel;

            String _url = $"{pUrl_service}api/FuelService/SyncReceiveDirectDesktop";
            try
            {
                List<tbl_t_receive_direct> SendListIssuing = new List<tbl_t_receive_direct>();
                var getData = getListReceiveDirectOwner();


                var listFailedPidReceive = new List<List<string>>();
                if (getData.Count() == 0)
                {
                    //timerSyncIssuing.Stop();
                    //btnSyncEnabled();
                }
                else
                {
                    //btnPleaseWait.Text = string.Format("Please Wait. {0} data dari {1} data", 0, getData.Count);
                    for (int i = 0; i < getData.Count; i++)
                    {

                        SendListIssuing.Add(getData[i]);

                        //if (SendListIssuing.Count == 1 /*&& int.Parse(counter[0]) != 0*/)
                        if (SendListIssuing.Count == int.Parse(_proper.data_per_kirim) /*&& int.Parse(counter[0]) != 0*/)
                        {
                            var json = Newtonsoft.Json.JsonConvert.SerializeObject(SendListIssuing);
                            ClsReceiveDirect data = await _url.WithHeaders(new { Authorization = _auth.token }).PostJsonAsync(SendListIssuing).ReceiveJson<ClsReceiveDirect>();                                                        
                            updateTableReceiveDirectOwnerSync(SendListIssuing, data);
                            log.WriteToFile("logLCR600.txt", data.remarks);
                            save_error_log(data.remarks, "onSyncAsyncReceiveDirectOwner()");
                            if (Application.OpenForms["FListDataReceiveDirectOwner"] != null)
                                (Application.OpenForms["FListDataReceiveDirectOwner"] as FListDataReceiveDirectOwner).loadGrid();
                            SendListIssuing = new List<tbl_t_receive_direct>();
                        }
                        else
                        {
                            if (i == getData.Count - 1 /*&& SendListIssuing.Count == int.Parse(counter[1])*/)
                            {
                                ClsReceiveDirect data = await _url.WithHeaders(new { Authorization = _auth.token }).PostJsonAsync(SendListIssuing).ReceiveJson<ClsReceiveDirect>();
                                updateTableReceiveDirectOwnerSync(SendListIssuing, data);
                                log.WriteToFile("logLCR600.txt", data.remarks);
                                save_error_log(data.remarks, "onSyncAsyncReceiveDirectOwner()");
                                if (Application.OpenForms["FListDataReceiveDirectOwner"] != null)
                                    (Application.OpenForms["FListDataReceiveDirectOwner"] as FListDataReceiveDirectOwner).loadGrid();                                
                            }
                        }                        
                    }
                }
            }
            catch (FlurlHttpException ex)
            {
                var resp = ex.Message;
                if (ex.InnerException != null)
                {
                    resp = ex.InnerException.Message;
                }
                //var resp = await ex.GetResponseStringAsync();
                MessageBox.Show("Terjadi kesalahan saat sync, periksa log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(resp, "onSyncAsync()");                
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
                MessageBox.Show("Exception: Terjadi kesalahan pada saat sync, mohon periksa log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(ex.Message, "onSyncAsync()");                
            }



        }
        async Task onSyncPengirimanPOTAsync()
        {
            var _proper = Properties.Settings.Default;
            var _auth = GlobalModel.loginModel;

            String _url = $"{pUrl_service}api/FuelService/SyncSendRitasiToMaintankDesktop";
            try
            {
                List<tbl_t_send_maintank_ritasi> SendListIssuing = new List<tbl_t_send_maintank_ritasi>();
                var getData = getListPengirimanPOT();


                var listFailedPidReceive = new List<List<string>>();
                if (getData.Count() == 0)
                {
                    //timerSyncIssuing.Stop();
                    //btnSyncEnabled();
                }
                else
                {
                    
                    for (int i = 0; i < getData.Count; i++)
                    {

                        SendListIssuing.Add(getData[i]);

                        //if (SendListIssuing.Count == 1 /*&& int.Parse(counter[0]) != 0*/)
                        if (SendListIssuing.Count == int.Parse(_proper.data_per_kirim) /*&& int.Parse(counter[0]) != 0*/)
                        {
                            var json = Newtonsoft.Json.JsonConvert.SerializeObject(SendListIssuing);
                            ClsRitasiToMaintank data = await _url.WithHeaders(new { Authorization = _auth.token }).PostJsonAsync(SendListIssuing).ReceiveJson<ClsRitasiToMaintank>();                           
                            
                            updateTablePengirimanPOTSync(SendListIssuing, data);
                            log.WriteToFile("logLCR600.txt", data.remarks);
                            save_error_log(data.remarks, "onSyncPengirimanPOTAsync()");
                            if (Application.OpenForms["FListDataPengirimanPOT"] != null)
                                (Application.OpenForms["FListDataPengirimanPOT"] as FListDataPengirimanPOT).loadGrid();
                            SendListIssuing = new List<tbl_t_send_maintank_ritasi>();
                        }
                        else
                        {
                            if (i == getData.Count - 1 /*&& SendListIssuing.Count == int.Parse(counter[1])*/)
                            {
                                ClsRitasiToMaintank data = await _url.WithHeaders(new { Authorization = _auth.token }).PostJsonAsync(SendListIssuing).ReceiveJson<ClsRitasiToMaintank>();

                                updateTablePengirimanPOTSync(SendListIssuing, data);
                                log.WriteToFile("logLCR600.txt", data.remarks);
                                save_error_log(data.remarks, "onSyncPengirimanPOTAsync()");
                                if (Application.OpenForms["FListDataPengirimanPOT"] != null)
                                    (Application.OpenForms["FListDataPengirimanPOT"] as FListDataPengirimanPOT).loadGrid();
                            }
                        }                      
                    }
                }
            }
            catch (FlurlHttpException ex)
            {
                var resp = ex.Message;
                if (ex.InnerException != null)
                {
                    resp = ex.InnerException.Message;
                }
                //var resp = await ex.GetResponseStringAsync();
                MessageBox.Show("Terjadi kesalahan saat sync, periksa log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(resp, "onSyncAsync()");                
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
                MessageBox.Show("Exception: Terjadi kesalahan pada saat sync, mohon periksa log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(ex.Message, "onSyncAsync()");                
            }



        }

        void updateTableSync(List<tbl_logsheet_detail> SendListIssuing, ClsIssuing sClsIssuing)
        {
            SQLiteConnection conn;
            int recs = 0;
            string query = string.Empty;
            conn = new SQLiteConnection(myConnectionString);
            if (conn.State != ConnectionState.Open) conn.Open();

            String failed_pids = String.Join("','", sClsIssuing.failed_job_row_id);
            string send_pid = String.Join("','", SendListIssuing.Select(x => x.job_row_id));

            if (sClsIssuing.failed_job_row_id.Count() > 0)
            {
                var success_pid = send_pid.Replace(failed_pids, "");
                query = $"update tbl_t_log_sheet_detail set syncs = 9 where job_row_id in('{failed_pids}') and syncs=3 ";
                recs = execCmd(query, conn);

                query = $"update tbl_t_log_sheet_detail set syncs = 7 where job_row_id in('{success_pid}') and syncs=3 ";
                recs = execCmd(query, conn);
            }
            else
            {
                query = $"update tbl_t_log_sheet_detail set syncs = 7 where job_row_id in('{send_pid}') and syncs=3 ";
                recs = execCmd(query, conn);
            }
            if (conn.State == ConnectionState.Open) conn.Close();

            if (Application.OpenForms["FMain"] != null)
            {
                (Application.OpenForms["FMain"] as FMain).toolStripStatusLabelStatus.Text = "Sync finish";
            }
        }
        void updateTableSync(ClsIssuing sClsIssuing)
        {
            SQLiteConnection conn;
            int recs = 0;
            string query = string.Empty;
            conn = new SQLiteConnection(myConnectionString);
            if (conn.State != ConnectionState.Open) conn.Open();

            String failed_pids = String.Join(",", sClsIssuing.failed_job_row_id);            
            if (sClsIssuing.failed_job_row_id.Count() > 0)
            {
                query = $"update tbl_t_log_sheet_detail set syncs = 9 where job_row_id in({failed_pids}) and syncs=3";
                recs = execCmd(query, conn);

                query = $"update tbl_t_log_sheet_detail set syncs = 7 where job_row_id not in({failed_pids}) and syncs=3";
                recs = execCmd(query, conn);
            }
            else
            {
                query = $"update tbl_t_log_sheet_detail set syncs = 7 where syncs=3";
                recs = execCmd(query, conn);
            }
            if (conn.State == ConnectionState.Open) conn.Close();

            if (Application.OpenForms["FMain"] != null)
            {
                (Application.OpenForms["FMain"] as FMain).toolStripStatusLabelStatus.Text = "Sync finish";
            }
        }
        void updateTableTransferSync(ClsIssuingTransfer sClsIssuing)
        {
            SQLiteConnection conn;
            int recs = 0;
            string query = string.Empty;
            conn = new SQLiteConnection(myConnectionString);
            if (conn.State != ConnectionState.Open) conn.Open();


            if (sClsIssuing.status)
            {
                query = $"update whtranfer set syncs = 7 where syncs=3";
                recs = execCmd(query, conn);
            }

            //String failed_pids = String.Join(",", sClsIssuing.failed_job_row_id);

            //if (sClsIssuing.failed_job_row_id.Count() > 0)
            //{
            //    query = $"update whtransfer set syncs = 9 where job_row_id in({failed_pids}) and syncs=3";
            //    recs = execCmd(query, conn);

            //    query = $"update whtranfer set syncs = 7 where job_row_id not in({failed_pids}) and syncs=3";
            //    recs = execCmd(query, conn);
            //}
            //else
            //{
            //    query = $"update whtranfer set syncs = 7 where syncs=3";
            //    recs = execCmd(query, conn);
            //}
            if (conn.State == ConnectionState.Open) conn.Close();

            if (Application.OpenForms["FMain"] != null)
            {
                (Application.OpenForms["FMain"] as FMain).toolStripStatusLabelStatus.Text = "Sync finish";
            }
        }
        void updateTableReceiveDirectSync(List<tbl_t_receive_direct> SendListIssuing, ClsReceiveDirect sClsIssuing)
        {
            SQLiteConnection conn;
            int recs = 0;
            string query = string.Empty;
            conn = new SQLiteConnection(myConnectionString);
            if (conn.State != ConnectionState.Open) conn.Open();

            String failed_pids = String.Join("','", sClsIssuing.failed_pid);
            string send_pid = String.Join("','", SendListIssuing.Select(x => x.pid_receive));

            if (sClsIssuing.failed_pid.Count() > 0)
            {
                var success_pid = send_pid.Replace(failed_pids, "");
                query = $"update tbl_t_receive_direct set syncs = 9 where pid_receive in('{failed_pids}') ";
                recs = execCmd(query, conn);

                query = $"update tbl_t_receive_direct set syncs = 7 where pid_receive in('{success_pid}') ";
                recs = execCmd(query, conn);
            }
            else
            {
                query = $"update tbl_t_receive_direct set syncs = 7 where pid_receive in('{send_pid}') ";
                recs = execCmd(query, conn);
            }
            if (conn.State == ConnectionState.Open) conn.Close();

            if (Application.OpenForms["FMain"] != null)
            {
                (Application.OpenForms["FMain"] as FMain).toolStripStatusLabelStatus.Text = "Sync finish";
            }
        }
        void updateTablePengirimanFTPortPamaSync(List<tbl_t_send_maintank_ritasi> SendListIssuing, ClsRitasiToMaintank sClsRitasiToMaintank)
        {
            SQLiteConnection conn;
            int recs = 0;
            string query = string.Empty;
            conn = new SQLiteConnection(myConnectionString);
            if (conn.State != ConnectionState.Open) conn.Open();


            string send_pid = String.Join("','", SendListIssuing.Select(x => x.pid_ritasi_maintank));

            if (sClsRitasiToMaintank.failed_pid != null && sClsRitasiToMaintank.failed_pid.Count() > 0)
            {
                String failed_pids = String.Join("','", sClsRitasiToMaintank.failed_pid);
                var success_pid = send_pid.Replace(failed_pids, "");
                query = $"update tbl_t_send_maintank_ritasi set syncs = 9 where pid_ritasi_maintank in('{failed_pids}') and syncs=3 ";
                recs = execCmd(query, conn);

                query = $"update tbl_t_send_maintank_ritasi set syncs = 7, send_iscomplete = 1 where pid_ritasi_maintank in('{success_pid}') and syncs=3 ";
                recs = execCmd(query, conn);
            }
            else
            {
                query = $"update tbl_t_send_maintank_ritasi set syncs = 7, send_iscomplete = 1 where pid_ritasi_maintank in('{send_pid}') and syncs=3 ";
                recs = execCmd(query, conn);
            }
            if (conn.State == ConnectionState.Open) conn.Close();

            if (Application.OpenForms["FMain"] != null)
            {
                (Application.OpenForms["FMain"] as FMain).toolStripStatusLabelStatus.Text = "Sync finish";
            }
        }
        void updateTableReceiveFTDaratSync(List<tbl_t_receive_maintank_ritasi> SendListIssuing, ClsRitasiToMaintank sClsRitasiToMaintank)
        {
            SQLiteConnection conn;
            int recs = 0;
            string query = string.Empty;
            conn = new SQLiteConnection(myConnectionString);
            if (conn.State != ConnectionState.Open) conn.Open();

            string send_pid = String.Join("','", SendListIssuing.Select(x => x.pid_ritasi_maintank));

            if (sClsRitasiToMaintank.failed_pid != null)
            {
                String failed_pids = String.Join("','", sClsRitasiToMaintank.failed_pid);
                var success_pid = send_pid.Replace(failed_pids, "");
                query = $"update tbl_t_receive_maintank_ritasi set syncs = 9 where pid_ritasi_maintank in('{failed_pids}') and syncs=3 ";
                query += " and doc_status in ('D20') ";
                recs = execCmd(query, conn);

                query = $"update tbl_t_receive_maintank_ritasi set syncs = 7, set receive_iscomplete = 1 set doc_status = 70 where pid_ritasi_maintank in('{success_pid}') and syncs=3 ";
                query += " and doc_status in ('D20') ";
                recs = execCmd(query, conn);
            }
            else
            {
                query = $"update tbl_t_receive_maintank_ritasi set syncs = 7, set receive_iscomplete = 1 set doc_status = 70 where pid_ritasi_maintank in('{send_pid}') and syncs=3 ";
                query += " and doc_status in ('D20') ";
                recs = execCmd(query, conn);
            }
            if (conn.State == ConnectionState.Open) conn.Close();

            if (Application.OpenForms["FMain"] != null)
            {
                (Application.OpenForms["FMain"] as FMain).toolStripStatusLabelStatus.Text = "Sync finish";
            }
        }
        void updateTableReceiveFTPortSync(List<tbl_t_receive_maintank_ritasi> SendListIssuing, ClsRitasiToMaintank sClsRitasiToMaintank)
        {
            SQLiteConnection conn;
            int recs = 0;
            string query = string.Empty;
            conn = new SQLiteConnection(myConnectionString);
            if (conn.State != ConnectionState.Open) conn.Open();


            string send_pid = String.Join("','", SendListIssuing.Select(x => x.pid_ritasi_maintank));

            if (sClsRitasiToMaintank.failed_pid != null)
            {
                String failed_pids = String.Join("','", sClsRitasiToMaintank.failed_pid);
                var success_pid = send_pid.Replace(failed_pids, "");
                query = $"update tbl_t_receive_maintank_ritasi set syncs = 9 where pid_ritasi_maintank in('{failed_pids}') ";
                query += " and doc_status IN('51')  ";
                recs = execCmd(query, conn);

                query = $"update tbl_t_receive_maintank_ritasi set syncs = 7,set receive_iscomplete = 1 set doc_status = 80 where pid_ritasi_maintank in('{success_pid}') and syncs=3 and doc_status IN('51')";                
                recs = execCmd(query, conn);
            }
            else
            {
                query = $"update tbl_t_receive_maintank_ritasi set syncs = 7,set receive_iscomplete = 1 set doc_status = 80 where pid_ritasi_maintank in('{send_pid}') and syncs=3 and doc_status IN('51')";
                
                recs = execCmd(query, conn);
            }
            if (conn.State == ConnectionState.Open) conn.Close();

            if (Application.OpenForms["FMain"] != null)
            {
                (Application.OpenForms["FMain"] as FMain).toolStripStatusLabelStatus.Text = "Sync finish";
            }
        }
        void updateTableReceiveDirectOwnerSync(List<tbl_t_receive_direct> SendListIssuing, ClsReceiveDirect sClsIssuing)
        {
            SQLiteConnection conn;
            int recs = 0;
            string query = string.Empty;
            conn = new SQLiteConnection(myConnectionString);
            if (conn.State != ConnectionState.Open) conn.Open();

            String failed_pids = String.Join("','", sClsIssuing.failed_pid);
            string send_pid = String.Join("','", SendListIssuing.Select(x => x.pid_receive));

            if (sClsIssuing.failed_pid.Count() > 0)
            {
                var success_pid = send_pid.Replace(failed_pids, "");
                query = $"update tbl_t_receive_direct_owner set syncs = 9 where pid_receive in('{failed_pids}') ";
                recs = execCmd(query, conn);

                query = $"update tbl_t_receive_direct_owner set syncs = 7 where pid_receive in('{success_pid}') ";
                recs = execCmd(query, conn);
            }
            else
            {
                query = $"update tbl_t_receive_direct set syncs = 7 where pid_receive in('{send_pid}') ";
                recs = execCmd(query, conn);
            }
            if (conn.State == ConnectionState.Open) conn.Close();

            if (Application.OpenForms["FMain"] != null)
            {
                (Application.OpenForms["FMain"] as FMain).toolStripStatusLabelStatus.Text = "Sync finish";
            }
        }
        void updateTablePengirimanPOTSync(List<tbl_t_send_maintank_ritasi> SendListIssuing, ClsRitasiToMaintank sClsRitasiToMaintank)
        {
            SQLiteConnection conn;
            int recs = 0;
            string query = string.Empty;
            conn = new SQLiteConnection(myConnectionString);
            if (conn.State != ConnectionState.Open) conn.Open();


            string send_pid = String.Join("','", SendListIssuing.Select(x => x.pid_ritasi_maintank));

            if (sClsRitasiToMaintank.failed_pid != null && sClsRitasiToMaintank.failed_pid.Count() > 0)
            {
                String failed_pids = String.Join("','", sClsRitasiToMaintank.failed_pid);
                var success_pid = send_pid.Replace(failed_pids, "");
                query = $"update tbl_t_send_maintank_ritasi set syncs = 9 where pid_ritasi_maintank in('{failed_pids}') and syncs=3 ";
                recs = execCmd(query, conn);

                query = $"update tbl_t_send_maintank_ritasi set syncs = 7, send_iscomplete = 1 where pid_ritasi_maintank in('{success_pid}') and syncs=3 ";
                recs = execCmd(query, conn);
            }
            else
            {
                query = $"update tbl_t_send_maintank_ritasi set syncs = 7, send_iscomplete = 1 where pid_ritasi_maintank in('{send_pid}') and syncs=3 ";
                recs = execCmd(query, conn);
            }
            if (conn.State == ConnectionState.Open) conn.Close();

            if (Application.OpenForms["FMain"] != null)
            {
                (Application.OpenForms["FMain"] as FMain).toolStripStatusLabelStatus.Text = "Sync finish";
            }
        }

        int execCmd(String sQuery, SQLiteConnection sConn)
        {
            int iReturn = 0;

            var cmd = new SQLiteCommand(sQuery, sConn);
            iReturn = cmd.ExecuteNonQuery();

            return iReturn;
        }

        string querySqlSyncTranfer()
        {
            var sql = " SELECT * ";
            sql += " FROM whtranfer  ";
            sql += " order BY case when syncs = 2 then 1 ";
            sql += " when syncs = 9 then 2 ";
            sql += " when syncs = 3 then 3  ";
            sql += " when syncs = 7 then 4 ";
            sql += " END asc ";
            sql += " , created_date asc ";
            return sql;
        }
        string querySqlSync()
        {
            //var sql = "SELECT sync_desc, job_row_id,issued_date,ref_hour_start, unit_no, hm, whouse_id, qty, shift  FROM vw_logsheet where issued_date between '" + dateTimePicker1.Value.ToString("yyyy-MM-dd 00:00:00") + "' and '" + dateTimePicker2.Value.ToString("yyyy-MM-dd 23:59:59") + "' and shift ='" + cbShift.Text + "' order by sync_desc,issued_date desc,ref_hour_start desc ";
            var sql = " SELECT * ";
            sql += " FROM tbl_t_log_sheet_detail ";
            sql += " WHERE syncs = 3 ";
            sql += " ORDER BY case when syncs = 2 then 1 ";
            sql += " when syncs = 9 then 2 ";
            sql += " when syncs = 3 then 3 ";
            sql += " when syncs = 7 then 4 ";
            sql += " END ASC ";
            sql += ", issued_date ASC ,ref_hour_start ASC ";
            return sql;
        }
        string querySqlReceiveDirectSync()
        {
            //var sql = "SELECT sync_desc, job_row_id,issued_date,ref_hour_start, unit_no, hm, whouse_id, qty, shift  FROM vw_logsheet where issued_date between '" + dateTimePicker1.Value.ToString("yyyy-MM-dd 00:00:00") + "' and '" + dateTimePicker2.Value.ToString("yyyy-MM-dd 23:59:59") + "' and shift ='" + cbShift.Text + "' order by sync_desc,issued_date desc,ref_hour_start desc ";
            var sql = " SELECT * ";
            sql += " FROM tbl_t_receive_direct ";
            sql += " WHERE syncs = 3 ";
            sql += " ORDER BY case when syncs = 2 then 1 ";
            sql += " when syncs = 9 then 2 ";
            sql += " when syncs = 3 then 3 ";
            sql += " when syncs = 7 then 4 ";
            sql += " END ASC ";
            sql += ", receive_date ASC ,start_loading ASC ";
            return sql;
        }
        string querySqlPengirimanFTPortPamaSync()
        {
            var sql = " SELECT * ";
            sql += " FROM tbl_t_send_maintank_ritasi ";
            sql += " WHERE syncs = 3 ";
            sql += " ORDER BY case when syncs = 2 then 1 ";
            sql += " when syncs = 9 then 2 ";
            sql += " when syncs = 3 then 3 ";
            sql += " when syncs = 7 then 4 ";
            sql += " END ASC ";
            sql += ", send_date ASC ,send_time_start ASC ";
            return sql;
        }
        string querySqlReceiveFTDaratSync()
        {
            var sql = " SELECT * ";
            sql += " FROM tbl_t_receive_maintank_ritasi ";
            sql += " WHERE syncs = 3 ";
            sql += " and doc_status IN('D20')";            
            sql += " ORDER BY case when syncs = 2 then 1 ";
            sql += " when syncs = 9 then 2 ";
            sql += " when syncs = 3 then 3 ";
            sql += " when syncs = 7 then 4 ";
            sql += " END ASC ";
            sql += ", send_date ASC ,time_start ASC ";
            return sql;
        }
        string querySqlReceveFTPortSync()
        {
            var sql = " SELECT * ";
            sql += " FROM tbl_t_receive_maintank_ritasi ";
            sql += " WHERE syncs = 3 ";
            sql += " and doc_status IN('51')  ";
            
            sql += " ORDER BY case when syncs = 2 then 1 ";
            sql += " when syncs = 9 then 2 ";
            sql += " when syncs = 3 then 3 ";
            sql += " when syncs = 7 then 4 ";
            sql += " END ASC ";
            sql += ", send_date ASC ,time_start ASC ";
            return sql;
        }
        string querySqlReceiveDirectOwnerSync()
        {
            var sql = " SELECT * ";
            sql += " FROM tbl_t_receive_direct_owner ";
            sql += " WHERE syncs = 3 ";           
            sql += " ORDER BY case when syncs = 2 then 1 ";
            sql += " when syncs = 9 then 2 ";
            sql += " when syncs = 3 then 3 ";
            sql += " when syncs = 7 then 4 ";
            sql += " END ASC ";
            sql += ", receive_date ASC ,start_loading ASC ";
            return sql;
        }
        string querySqlPengirimanPOTSync()
        {
            var sql = " SELECT * ";
            sql += " FROM tbl_t_send_pot ";
            sql += " WHERE syncs = 3 ";           
            sql += " ORDER BY case when syncs = 2 then 1 ";
            sql += " when syncs = 9 then 2 ";
            sql += " when syncs = 3 then 3 ";
            sql += " when syncs = 7 then 4 ";
            sql += " END ASC ";
            sql += ", send_date ASC ,send_time_start ASC ";
            return sql;
        }

        List<tbl_logsheet_detail> getListIssuing()
        {
            List<tbl_logsheet_detail> _list = new List<tbl_logsheet_detail>();
            SQLiteConnection conn = new SQLiteConnection(myConnectionString);

            try
            {
                
                if (conn.State != ConnectionState.Open) conn.Open();
                string query = string.Empty;

                try
                {

                    query = $"update tbl_t_log_sheet_detail set syncs = 3 where syncs in(2,9)";
                    var recs = execCmd(query, conn);
                }
                catch (SQLiteException ex)
                {
                    MessageBox.Show("Terjadi kesalahan saat mengambil data issuing, mohon cek log LCR600");
                    log.WriteToFile("logLCR600.txt", ex.Message);
                    save_error_log(ex.Message, "getListIssuing()");
                    //MessageBox.Show(ex.ToString());
                }

                //query = "select * from tbl_t_log_sheet_detail where syncs=3 ORDER BY issued_date,ref_hour_start asc";
                query = querySqlSync();

                SQLiteCommand cmd = new SQLiteCommand(query, conn);
                SQLiteDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var cls = new tbl_logsheet_detail();

                    cls.job_row_id = reader["job_row_id"].ToString();
                    cls.dstrct_code = reader["dstrct_code"].ToString();
                    cls.log_sheet_code = reader["log_sheet_code"].ToString();
                    cls.input_type = reader["input_type"].ToString();
                    cls.issued_date = Convert.ToDateTime(reader["issued_date"].ToString());
                    cls.whouse_id = reader["whouse_id"].ToString();
                    cls.unit_no = reader["unit_no"].ToString();
                    cls.max_tank_capacity = Convert.ToInt32(reader["max_tank_capacity"].ToString());
                    cls.hm_before = Convert.ToInt32(string.IsNullOrEmpty(reader["hm_before"].ToString()) ? "0" : reader["hm_before"].ToString());
                    cls.hm = Convert.ToInt32(reader["hm"].ToString());
                    cls.flw_meter = reader["flw_meter"].ToString();
                    cls.meter_faktor = Convert.ToDouble(reader["meter_faktor"].ToString());
                    cls.qty_loqsheet = Convert.ToDouble(reader["qty_loqsheet"].ToString());
                    cls.qty = Convert.ToDouble(reader["qty"].ToString());
                    cls.shift = reader["shift"].ToString();
                    cls.fuel_oil_type = reader["fuel_oil_type"].ToString();
                    cls.stat_type = reader["stat_type"].ToString();
                    cls.nrp_operator = reader["nrp_operator"].ToString();
                    cls.nama_operator = reader["nama_operator"].ToString();
                    cls.work_area = reader["work_area"].ToString();
                    cls.location = reader["location"].ToString();
                    cls.ref_condition = reader["ref_condition"].ToString();
                    cls.ref_hour_start = reader["ref_hour_start"].ToString();
                    cls.ref_hour_stop = reader["ref_hour_stop"].ToString();
                    cls.note = reader["note"].ToString();
                    cls.timezone = reader["timezone"].ToString();
                    cls.flag_loading = reader["flag_loading"].ToString();
                    cls.loadingerror = reader["loadingerror"].ToString();
                    cls.loadingdatetime = Convert.ToDateTime(reader["loadingdatetime"].ToString());
                    cls.mod_by = reader["mod_by"].ToString();
                    cls.mod_date = Convert.ToDateTime(reader["mod_date"].ToString());
                    cls.flow_meter_start = Convert.ToDouble(reader["flow_meter_start"].ToString());
                    cls.flow_meter_end = Convert.ToDouble(reader["flow_meter_end"].ToString());
                    cls.resp_code = reader["resp_code"].ToString();
                    cls.resp_name = reader["resp_name"].ToString();
                    cls.text_header = reader["text_header"].ToString();
                    cls.text_sub_header = reader["text_sub_header"].ToString();
                    cls.text_body = reader["text_body"].ToString();

                    _list.Add(cls);
                }

                reader.Dispose();
                if (conn.State == ConnectionState.Open) conn.Close();
                return _list;
            }
            catch (SQLiteException ex)
            {
                if (conn.State == ConnectionState.Open) conn.Close();
                MessageBox.Show("Terjadi kesalahan saat mengambil data issuing, mohon cek log LCR600");
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(ex.Message, "getListTransfer()");
                //MessageBox.Show(ex.Message);
                return null;
            }
        }
        List<whtranfer> getListTransfer()
        {
            SQLiteConnection conn = new SQLiteConnection(myConnectionString);
            try
            {
                List<whtranfer> _list = new List<whtranfer>();
                
                if (conn.State != ConnectionState.Open) conn.Open();
                string query = string.Empty;

                try
                {
                    query = $"update whtranfer set syncs = 3 where syncs in(2,9)";
                    var recs = execCmd(query, conn);
                }
                catch (SQLiteException ex)
                {
                    MessageBox.Show("Terjadi kesalahan saat mengambil data Transfer, mohon cek log LCR600");
                    log.WriteToFile("logLCR600.txt", ex.Message);
                    save_error_log(ex.Message, "getListTransfer()");
                    //MessageBox.Show(ex.ToString());
                }

                //query = "select * from whtranfer where syncs = 3 ORDER BY created_date asc";
                query = querySqlSyncTranfer();

                SQLiteCommand cmd = new SQLiteCommand(query, conn);
                SQLiteDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var cls = new whtranfer();

                    cls.job_row_id = reader["job_row_id"].ToString();
                    cls.xfer_code = reader["xfer_code"].ToString();
                    cls.xfer_date = reader["xfer_date"].ToString();
                    cls.dstrct_code = reader["dstrct_code"].ToString();
                    cls.employee_id = reader["employee_id"].ToString();
                    cls.issued_whouse = reader["issued_whouse"].ToString();
                    cls.received_whouse = reader["received_whouse"].ToString();
                    cls.stock_code = reader["stock_code"].ToString();
                    cls.qty_xfer = float.Parse(reader["qty_xfer"].ToString());
                    cls.shift = reader["shift"].ToString();
                    cls.fmeter_serial_no = reader["fmeter_serial_no"].ToString();
                    cls.faktor_meter = float.Parse(reader["faktor_meter"].ToString());
                    cls.fmeter_begin = float.Parse(reader["fmeter_begin"].ToString());
                    cls.fmeter_end = float.Parse(reader["fmeter_end"].ToString());
                    cls.created_by = reader["created_by"].ToString();
                    cls.xfer_by = reader["xfer_by"].ToString();
                    cls.received_by = reader["received_by"].ToString();
                    cls.issued_whouse_type = reader["issued_whouse_type"].ToString();
                    cls.received_whouse_type = reader["received_whouse_type"].ToString();
                    cls.mod_date = Convert.ToDateTime(reader["mod_date"].ToString());
                    cls.created_date = Convert.ToDateTime(reader["created_date"].ToString());
                    cls.timezone = reader["timezone"].ToString();
                    cls.syncs = int.Parse(reader["syncs"].ToString());

                    _list.Add(cls);
                }

                reader.Dispose();
                if (conn.State == ConnectionState.Open) conn.Close();
                return _list;

            }
            catch (SQLiteException ex)
            {
                if (conn.State == ConnectionState.Open) conn.Close();
                MessageBox.Show("Terjadi kesalahan saat mengambil data Transfer, mohon cek log LCR600");
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(ex.Message, "getListTransfer()");
                //MessageBox.Show(ex.Message);
                return null;
            }

        }
        List<tbl_t_receive_direct> getListReceiveDirect()
        {
            List<tbl_t_receive_direct> _list = new List<tbl_t_receive_direct>();
            SQLiteConnection conn;

            conn = new SQLiteConnection(myConnectionString);
            if (conn.State != ConnectionState.Open) conn.Open();
            string query = string.Empty;

            try
            {

                query = $"update tbl_t_receive_direct set syncs = 3 where syncs in(2,9)";
                var recs = execCmd(query, conn);                
            }
            catch (SQLiteException ex)
            {
                //MessageBox.Show(ex.ToString());
                MessageBox.Show("Exception: Terjadi kesalahan pada saat get data receive direct local, mohon periksa log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(ex.Message, "getListReceiveDirect()");
            }

            //query = "select * from tbl_t_log_sheet_detail where syncs=3 AND issued_date between '" + dateTimePicker1.Value.ToString("yyyy-MM-dd 00:00:00") + "' and '" + dateTimePicker2.Value.ToString("yyyy-MM-dd 23:59:59") + "' and shift ='" + cbShift.Text + "'  ORDER BY ref_hour_start";
            query = querySqlReceiveDirectSync();
            try
            {

                SQLiteCommand cmd = new SQLiteCommand(query, conn);
                SQLiteDataReader reader = cmd.ExecuteReader();



                while (reader.Read())
                {
                    var cls = new tbl_t_receive_direct();

                    cls.pid_receive = reader["pid_receive"].ToString();
                    cls.ref_id = reader["ref_id"].ToString();
                    cls.district = reader["district"].ToString();
                    cls.po_no = reader["po_no"].ToString();
                    cls.receive_at = reader["receive_at"].ToString();
                    cls.receive_at_code = reader["receive_at_code"].ToString();
                    cls.receive_qty = int.Parse(reader["receive_qty"].ToString());
                    cls.receive_sn_flow_meter1 = reader["receive_sn_flow_meter1"].ToString();
                    cls.receive_meter_faktor = reader["receive_meter_faktor"].ToString();
                    cls.receive_density = reader["receive_density"].ToString();
                    cls.receive_temperature = reader["receive_temperature"].ToString();
                    cls.receive_by = reader["receive_by"].ToString();
                    cls.receive_date = Convert.ToDateTime(reader["receive_date"].ToString());
                    cls.sir_no = reader["sir_no"].ToString();
                    cls.receive_qty_pama = int.Parse(reader["receive_qty_pama"].ToString());
                    cls.totaliser_awal = int.Parse(reader["totaliser_awal"].ToString());
                    cls.totaliser_akhir = int.Parse(reader["totaliser_akhir"].ToString());
                    cls.start_loading = reader["start_loading"].ToString();
                    cls.end_loading = reader["end_loading"].ToString();



                    _list.Add(cls);
                }
                reader.Dispose();
                if (conn.State == ConnectionState.Open) conn.Close();
            }
            catch (SQLiteException ex)
            {
                if (conn.State == ConnectionState.Open) conn.Close();
                //MessageBox.Show(ex.ToString());
                MessageBox.Show("Exception: Terjadi kesalahan pada saat get data receive direct local, mohon periksa log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(ex.Message, "getListReceiveDirect()");
            }
            if (conn.State == ConnectionState.Open) conn.Close();
            return _list;
        }
        List<tbl_t_send_maintank_ritasi> getListPengirimanFTPortPama()
        {
            List<tbl_t_send_maintank_ritasi> _list = new List<tbl_t_send_maintank_ritasi>();
            SQLiteConnection conn;

            conn = new SQLiteConnection(myConnectionString);
            if (conn.State != ConnectionState.Open) conn.Open();
            string query = string.Empty;

            try
            {

                query = $"update tbl_t_send_maintank_ritasi set syncs = 3 where syncs in(2,9) ";
                var recs = execCmd(query, conn);
            }
            catch (SQLiteException ex)
            {
                //MessageBox.Show(ex.ToString());
                MessageBox.Show("Exception: Terjadi kesalahan pada saat get data receive direct local, mohon periksa log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(ex.Message, "getListPengirimanFTPortPama()");
            }

            //query = "select * from tbl_t_log_sheet_detail where syncs=3 AND issued_date between '" + dateTimePicker1.Value.ToString("yyyy-MM-dd 00:00:00") + "' and '" + dateTimePicker2.Value.ToString("yyyy-MM-dd 23:59:59") + "' and shift ='" + cbShift.Text + "'  ORDER BY ref_hour_start";
            query = querySqlPengirimanFTPortPamaSync();
            try
            {

                SQLiteCommand cmd = new SQLiteCommand(query, conn);
                SQLiteDataReader reader = cmd.ExecuteReader();



                while (reader.Read())
                {
                    var cls = new tbl_t_send_maintank_ritasi();

                    cls.pid_ritasi_maintank = reader["pid_ritasi_maintank"].ToString();
                    cls.ref_id = reader["ref_id"].ToString();
                    cls.ref_tbl = reader["ref_tbl"].ToString();
                    cls.district = reader["district"].ToString();
                    cls.send_type = reader["send_type"].ToString();
                    cls.send_po_no = reader["send_po_no"].ToString();
                    cls.send_do_no = reader["send_do_no"].ToString();
                    cls.send_qty = reader["send_qty"].ToString();
                    cls.transportir_code = reader["transportir_code"].ToString();
                    cls.transportir_name = reader["transportir_name"].ToString();
                    cls.send_ft_no = reader["send_ft_no"].ToString();
                    cls.send_driver_name = reader["send_driver_name"].ToString();
                    cls.send_date = DateTime.Parse(reader["send_date"].ToString());
                    cls.send_bbn = int.Parse(reader["send_bbn"].ToString());
                    cls.send_from = reader["send_from"].ToString();
                    cls.send_from_code = reader["send_from_code"].ToString();
                    cls.send_to = reader["send_to"].ToString();
                    cls.send_to_code = reader["send_to_code"].ToString();
                    cls.send_dipping_hole1_port = double.Parse(reader["send_dipping_hole1_port"].ToString());
                    cls.send_dipping_hole2_port = double.Parse(reader["send_dipping_hole2_port"].ToString());
                    cls.send_iscomplete = int.Parse(reader["send_iscomplete"].ToString());
                    cls.send_sn_flow_meter = reader["send_sn_flow_meter"].ToString();
                    cls.send_meter_faktor = double.Parse(reader["send_meter_faktor"].ToString());
                    cls.send_flow_meter_awal = int.Parse(reader["send_flow_meter_awal"].ToString());
                    cls.send_flow_meter_akhir = int.Parse(reader["send_flow_meter_akhir"].ToString());
                    cls.send_qty_x_mf = int.Parse(reader["send_qty_x_mf"].ToString());
                    cls.send_no_segel_1 = reader["send_no_segel_1"].ToString();
                    cls.send_no_segel_2 = reader["send_no_segel_2"].ToString();
                    cls.send_no_segel_3 = reader["send_no_segel_3"].ToString();
                    cls.send_no_segel_4 = reader["send_no_segel_4"].ToString();
                    cls.send_no_segel_5 = reader["send_no_segel_5"].ToString();
                    cls.send_no_segel_6 = reader["send_no_segel_6"].ToString();
                    cls.doc_status = reader["doc_status"].ToString();
                    cls.last_mod_by = reader["last_mod_by"].ToString();
                    cls.foto = reader["foto"].ToString();
                    cls.send_time_start = DateTime.Parse(reader["send_time_start"].ToString());
                    cls.send_time_end = DateTime.Parse(reader["send_time_end"].ToString());




                    _list.Add(cls);
                }
                reader.Dispose();
                if (conn.State == ConnectionState.Open) conn.Close();
            }
            catch (SQLiteException ex)
            {
                if (conn.State == ConnectionState.Open) conn.Close();
                //MessageBox.Show(ex.ToString());
                MessageBox.Show("Exception: Terjadi kesalahan pada saat get data receive direct local, mohon periksa log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(ex.Message, "getListPengirimanFTPortPama()");
            }
            if (conn.State == ConnectionState.Open) conn.Close();
            return _list;
        }
        List<tbl_t_receive_maintank_ritasi> getListReceiveFTDarat()
        {
            List<tbl_t_receive_maintank_ritasi> _list = new List<tbl_t_receive_maintank_ritasi>();
            SQLiteConnection conn;

            conn = new SQLiteConnection(myConnectionString);
            if (conn.State != ConnectionState.Open) conn.Open();
            string query = string.Empty;

            try
            {

                query = $"update tbl_t_receive_maintank_ritasi set syncs = 3 where syncs in(2,9) ";
                query += " and doc_status IN('D20')";
               
                var recs = execCmd(query, conn);                
            }
            catch (SQLiteException ex)
            {
                //MessageBox.Show(ex.ToString());
                MessageBox.Show("Exception: Terjadi kesalahan pada saat get data receive maintank ritasi Darat local, mohon periksa log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(ex.Message, "getListReceiveFTDarat()");
            }

            //query = "select * from tbl_t_log_sheet_detail where syncs=3 AND issued_date between '" + dateTimePicker1.Value.ToString("yyyy-MM-dd 00:00:00") + "' and '" + dateTimePicker2.Value.ToString("yyyy-MM-dd 23:59:59") + "' and shift ='" + cbShift.Text + "'  ORDER BY ref_hour_start";
            query = querySqlReceiveFTDaratSync();
            try
            {

                SQLiteCommand cmd = new SQLiteCommand(query, conn);
                SQLiteDataReader reader = cmd.ExecuteReader();



                while (reader.Read())
                {
                    var cls = new tbl_t_receive_maintank_ritasi();

                    cls.pid_ritasi_maintank = reader["pid_ritasi_maintank"].ToString();
                    cls.send_date = DateTime.Parse(reader["send_date"].ToString());
                    cls.send_po_no = reader["send_po_no"].ToString();
                    cls.district = reader["district"].ToString();
                    cls.receive_at = reader["receive_at"].ToString();
                    cls.receive_at_code = reader["receive_at_code"].ToString();
                    cls.receive_density = double.Parse(reader["receive_density"].ToString());
                    cls.receive_temperature = int.Parse(reader["receive_temperature"].ToString());


                    cls.receive_sn_flow_meter1 = reader["receive_sn_flow_meter1"].ToString();
                    cls.receive_meter_faktor = double.Parse(reader["receive_meter_faktor"].ToString());
                    cls.receive_qty1 = int.Parse(reader["receive_qty1"].ToString());
                    cls.receive_qty1_x_mf = int.Parse(reader["receive_qty1_x_mf"].ToString());
                    cls.receive_flow_meter_awal1 = int.Parse(reader["receive_flow_meter_awal1"].ToString());
                    cls.receive_flow_meter_akhir1 = int.Parse(reader["receive_flow_meter_akhir1"].ToString());


                    cls.receive_sn_flow_meter2 = reader["receive_sn_flow_meter2"].ToString();
                    cls.receive_meter_faktor2 = double.Parse(reader["receive_meter_faktor2"].ToString());
                    cls.receive_qty2 = int.Parse(reader["receive_qty2"].ToString());
                    cls.receive_qty2_x_mf = int.Parse(reader["receive_qty2_x_mf"].ToString());
                    cls.receive_flow_meter_awal2 = int.Parse(reader["receive_flow_meter_awal2"].ToString());
                    cls.receive_flow_meter_akhir2 = int.Parse(reader["receive_flow_meter_akhir2"].ToString());


                    cls.receive_qty_additive = int.Parse(reader["receive_qty_additive"].ToString());
                    //cls.receive_iscomplete = int.Parse(reader["receive_iscomplete"].ToString());
                    cls.receive_driver_name = reader["receive_driver_name"].ToString();
                    cls.receive_no_segel_1 = reader["receive_no_segel_1"].ToString();
                    cls.receive_no_segel_2 = reader["receive_no_segel_2"].ToString();
                    cls.receive_no_segel_3 = reader["receive_no_segel_3"].ToString();
                    cls.receive_no_segel_4 = reader["receive_no_segel_4"].ToString();
                    cls.receive_no_segel_5 = reader["receive_no_segel_5"].ToString();
                    cls.receive_no_segel_6 = reader["receive_no_segel_6"].ToString();
                    cls.doc_status = reader["doc_status"].ToString();
                    cls.last_mod_by = reader["last_mod_by"].ToString();
                    cls.foto = reader["foto"].ToString();

                    cls.time_start = DateTime.Parse(reader["time_start"].ToString());
                    cls.time_end = DateTime.Parse(reader["time_end"].ToString());




                    _list.Add(cls);
                }
                reader.Dispose();
                if (conn.State == ConnectionState.Open) conn.Close();
            }
            catch (SQLiteException ex)
            {
                if (conn.State == ConnectionState.Open) conn.Close();
                //MessageBox.Show(ex.ToString());
                MessageBox.Show("Exception: Terjadi kesalahan pada saat get data receive maintank ritasi Darat local, mohon periksa log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(ex.Message, "getListReceiveFTDarat()");
            }
            if (conn.State == ConnectionState.Open) conn.Close();
            return _list;
        }
        List<tbl_t_receive_maintank_ritasi> getListReceiveFTPort()
        {
            List<tbl_t_receive_maintank_ritasi> _list = new List<tbl_t_receive_maintank_ritasi>();
            SQLiteConnection conn;

            conn = new SQLiteConnection(myConnectionString);
            if (conn.State != ConnectionState.Open) conn.Open();
            string query = string.Empty;

            try
            {

                query = $"update tbl_t_receive_maintank_ritasi set syncs = 3 where syncs in(2,9) ";
                query += " and doc_status IN('51') ";
                
                var recs = execCmd(query, conn);
                
            }
            catch (SQLiteException ex)
            {
                //MessageBox.Show(ex.ToString());
                MessageBox.Show("Exception: Terjadi kesalahan pada saat get data receive maintank ritasi Port local, mohon periksa log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(ex.Message, "getListReceiveFTPort()");
            }

            //query = "select * from tbl_t_log_sheet_detail where syncs=3 AND issued_date between '" + dateTimePicker1.Value.ToString("yyyy-MM-dd 00:00:00") + "' and '" + dateTimePicker2.Value.ToString("yyyy-MM-dd 23:59:59") + "' and shift ='" + cbShift.Text + "'  ORDER BY ref_hour_start";
            query = querySqlReceveFTPortSync();
            try
            {

                SQLiteCommand cmd = new SQLiteCommand(query, conn);
                SQLiteDataReader reader = cmd.ExecuteReader();



                while (reader.Read())
                {
                    var cls = new tbl_t_receive_maintank_ritasi();

                    cls.pid_ritasi_maintank = reader["pid_ritasi_maintank"].ToString();
                    cls.send_date = DateTime.Parse(reader["send_date"].ToString());
                    cls.send_po_no = reader["send_po_no"].ToString();
                    cls.district = reader["district"].ToString();
                    cls.receive_at = reader["receive_at"].ToString();
                    cls.receive_at_code = reader["receive_at_code"].ToString();
                    cls.receive_density = double.Parse(reader["receive_density"].ToString());
                    cls.receive_temperature = int.Parse(reader["receive_temperature"].ToString());


                    cls.receive_sn_flow_meter1 = reader["receive_sn_flow_meter1"].ToString();
                    cls.receive_meter_faktor = double.Parse(reader["receive_meter_faktor"].ToString());
                    cls.receive_qty1 = int.Parse(reader["receive_qty1"].ToString());
                    cls.receive_qty1_x_mf = int.Parse(reader["receive_qty1_x_mf"].ToString());
                    cls.receive_flow_meter_awal1 = int.Parse(reader["receive_flow_meter_awal1"].ToString());
                    cls.receive_flow_meter_akhir1 = int.Parse(reader["receive_flow_meter_akhir1"].ToString());


                    cls.receive_sn_flow_meter2 = reader["receive_sn_flow_meter2"].ToString();
                    cls.receive_meter_faktor2 = double.Parse(reader["receive_meter_faktor2"].ToString());
                    cls.receive_qty2 = int.Parse(reader["receive_qty2"].ToString());
                    cls.receive_qty2_x_mf = int.Parse(reader["receive_qty2_x_mf"].ToString());
                    cls.receive_flow_meter_awal2 = int.Parse(reader["receive_flow_meter_awal2"].ToString());
                    cls.receive_flow_meter_akhir2 = int.Parse(reader["receive_flow_meter_akhir2"].ToString());


                    cls.receive_qty_additive = int.Parse(reader["receive_qty_additive"].ToString());
                    //cls.receive_iscomplete = int.Parse(reader["receive_iscomplete"].ToString());
                    cls.receive_driver_name = reader["receive_driver_name"].ToString();
                    cls.receive_no_segel_1 = reader["receive_no_segel_1"].ToString();
                    cls.receive_no_segel_2 = reader["receive_no_segel_2"].ToString();
                    cls.receive_no_segel_3 = reader["receive_no_segel_3"].ToString();
                    cls.receive_no_segel_4 = reader["receive_no_segel_4"].ToString();
                    cls.receive_no_segel_5 = reader["receive_no_segel_5"].ToString();
                    cls.receive_no_segel_6 = reader["receive_no_segel_6"].ToString();
                    cls.doc_status = reader["doc_status"].ToString();
                    cls.last_mod_by = reader["last_mod_by"].ToString();
                    cls.foto = reader["foto"].ToString();

                    cls.time_start = DateTime.Parse(reader["time_start"].ToString());
                    cls.time_end = DateTime.Parse(reader["time_end"].ToString());



                    _list.Add(cls);
                }
                reader.Dispose();
                if (conn.State == ConnectionState.Open) conn.Close();
            }
            catch (SQLiteException ex)
            {
                if (conn.State == ConnectionState.Open) conn.Close();
                //MessageBox.Show(ex.ToString());
                MessageBox.Show("Exception: Terjadi kesalahan pada saat get data receive maintank ritasi Port local, mohon periksa log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(ex.Message, "getListReceiveFTDarat()");
            }
            if (conn.State == ConnectionState.Open) conn.Close();
            return _list;
        }
        List<tbl_t_receive_direct> getListReceiveDirectOwner()
        {
            List<tbl_t_receive_direct> _list = new List<tbl_t_receive_direct>();
            SQLiteConnection conn;

            conn = new SQLiteConnection(myConnectionString);
            if (conn.State != ConnectionState.Open) conn.Open();
            string query = string.Empty;

            try
            {

                query = $"update tbl_t_receive_direct_owner set syncs = 3 where syncs in(2,9)";
                var recs = execCmd(query, conn);                                
            }
            catch (SQLiteException ex)
            {
                //MessageBox.Show(ex.ToString());
                MessageBox.Show("Exception: Terjadi kesalahan pada saat get data receive direct owner local, mohon periksa log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(ex.Message, "getListReceiveDirect()");
            }

            //query = "select * from tbl_t_log_sheet_detail where syncs=3 AND issued_date between '" + dateTimePicker1.Value.ToString("yyyy-MM-dd 00:00:00") + "' and '" + dateTimePicker2.Value.ToString("yyyy-MM-dd 23:59:59") + "' and shift ='" + cbShift.Text + "'  ORDER BY ref_hour_start";
            query = querySqlSync();
            try
            {

                SQLiteCommand cmd = new SQLiteCommand(query, conn);
                SQLiteDataReader reader = cmd.ExecuteReader();



                while (reader.Read())
                {
                    var cls = new tbl_t_receive_direct();

                    cls.pid_receive = reader["pid_receive"].ToString();
                    cls.ref_id = reader["ref_id"].ToString();
                    cls.district = reader["district"].ToString();
                    cls.po_no = reader["po_no"].ToString();
                    cls.receive_at = reader["receive_at"].ToString();
                    cls.receive_at_code = reader["receive_at_code"].ToString();
                    cls.receive_qty = int.Parse(reader["receive_qty"].ToString());
                    cls.receive_sn_flow_meter1 = reader["receive_sn_flow_meter1"].ToString();
                    cls.receive_meter_faktor = reader["receive_meter_faktor"].ToString();
                    cls.receive_density = reader["receive_density"].ToString();
                    cls.receive_temperature = reader["receive_temperature"].ToString();
                    cls.receive_by = reader["receive_by"].ToString();
                    cls.receive_date = Convert.ToDateTime(reader["receive_date"].ToString());
                    cls.sir_no = reader["sir_no"].ToString();
                    cls.receive_qty_pama = int.Parse(reader["receive_qty_pama"].ToString());
                    cls.totaliser_awal = int.Parse(reader["totaliser_awal"].ToString());
                    cls.totaliser_akhir = int.Parse(reader["totaliser_akhir"].ToString());
                    cls.start_loading = reader["start_loading"].ToString();
                    cls.end_loading = reader["end_loading"].ToString();



                    _list.Add(cls);
                }
                reader.Dispose();
                conn.Close();
            }
            catch (SQLiteException ex)
            {
                conn.Close();
                //MessageBox.Show(ex.ToString());
                MessageBox.Show("Exception: Terjadi kesalahan pada saat get data receive direct local, mohon periksa log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(ex.Message, "getListReceiveDirect()");
            }
            if (conn.State == ConnectionState.Open) conn.Close();
            return _list;
        }
        List<tbl_t_send_maintank_ritasi> getListPengirimanPOT()
        {
            List<tbl_t_send_maintank_ritasi> _list = new List<tbl_t_send_maintank_ritasi>();
            SQLiteConnection conn;

            conn = new SQLiteConnection(myConnectionString);
            if (conn.State != ConnectionState.Open) conn.Open();
            string query = string.Empty;

            try
            {

                query = $"update tbl_t_send_pot set syncs = 3 where syncs in(2,9) ";                
                var recs = execCmd(query, conn);
                
            }
            catch (SQLiteException ex)
            {
                //MessageBox.Show(ex.ToString());
                MessageBox.Show("Exception: Terjadi kesalahan pada saat get data Pengiriman POT local, mohon periksa log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(ex.Message, "getListReceiveDirect()");
            }

            //query = "select * from tbl_t_log_sheet_detail where syncs=3 AND issued_date between '" + dateTimePicker1.Value.ToString("yyyy-MM-dd 00:00:00") + "' and '" + dateTimePicker2.Value.ToString("yyyy-MM-dd 23:59:59") + "' and shift ='" + cbShift.Text + "'  ORDER BY ref_hour_start";
            query = querySqlSync();
            try
            {

                SQLiteCommand cmd = new SQLiteCommand(query, conn);
                SQLiteDataReader reader = cmd.ExecuteReader();



                while (reader.Read())
                {
                    var cls = new tbl_t_send_maintank_ritasi();

                    cls.pid_ritasi_maintank = reader["pid_ritasi_maintank"].ToString();
                    cls.ref_id = reader["ref_id"].ToString();
                    cls.ref_tbl = reader["ref_tbl"].ToString();
                    cls.district = reader["district"].ToString();
                    cls.send_type = reader["send_type"].ToString();
                    cls.send_po_no = reader["send_po_no"].ToString();
                    cls.send_do_no = reader["send_do_no"].ToString();
                    cls.send_qty = reader["send_qty"].ToString();
                    cls.transportir_code = reader["transportir_code"].ToString();
                    cls.transportir_name = reader["transportir_name"].ToString();
                    cls.send_ft_no = reader["send_ft_no"].ToString();
                    cls.send_driver_name = reader["send_driver_name"].ToString();
                    cls.send_date = DateTime.Parse(reader["send_date"].ToString());
                    cls.send_bbn = int.Parse(reader["send_bbn"].ToString());
                    cls.send_from = reader["send_from"].ToString();
                    cls.send_from_code = reader["send_from_code"].ToString();
                    cls.send_to = reader["send_to"].ToString();
                    cls.send_to_code = reader["send_to_code"].ToString();
                    cls.send_dipping_hole1_port = double.Parse(reader["send_dipping_hole1_port"].ToString());
                    cls.send_dipping_hole2_port = double.Parse(reader["send_dipping_hole2_port"].ToString());
                    cls.send_iscomplete = int.Parse(reader["send_iscomplete"].ToString());
                    cls.send_sn_flow_meter = reader["send_sn_flow_meter"].ToString();
                    cls.send_meter_faktor = double.Parse(reader["send_meter_faktor"].ToString());
                    cls.send_flow_meter_awal = int.Parse(reader["send_flow_meter_awal"].ToString());
                    cls.send_flow_meter_akhir = int.Parse(reader["send_flow_meter_akhir"].ToString());
                    cls.send_qty_x_mf = int.Parse(reader["send_qty_x_mf"].ToString());
                    cls.send_no_segel_1 = reader["send_no_segel_1"].ToString();
                    cls.send_no_segel_2 = reader["send_no_segel_2"].ToString();
                    cls.send_no_segel_3 = reader["send_no_segel_3"].ToString();
                    cls.send_no_segel_4 = reader["send_no_segel_4"].ToString();
                    cls.send_no_segel_5 = reader["send_no_segel_5"].ToString();
                    cls.send_no_segel_6 = reader["send_no_segel_6"].ToString();
                    cls.doc_status = reader["doc_status"].ToString();
                    cls.last_mod_by = reader["last_mod_by"].ToString();
                    cls.foto = reader["foto"].ToString();
                    cls.send_time_start = DateTime.Parse(reader["send_time_start"].ToString());
                    cls.send_time_end = DateTime.Parse(reader["send_time_end"].ToString());




                    _list.Add(cls);
                }

                reader.Dispose();
                conn.Close();
            }
            catch (SQLiteException ex)
            {
                //MessageBox.Show(ex.ToString());
                conn.Close();
                MessageBox.Show("Exception: Terjadi kesalahan pada saat get data receive direct local, mohon periksa log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(ex.Message, "getListReceiveDirect()");
            }
            if (conn.State == ConnectionState.Open) conn.Close();
            return _list;
        }



        private void warehouseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var f = new FWarehouse();
            f.Text += " " + GlobalModel.app_version;
            f.ShowDialog();
        }

        private void FMain_Activated(object sender, EventArgs e)
        {
            this.WindowState = System.Windows.Forms.FormWindowState.Normal;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
        }

        private void refuellingSerialToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var f = new FRefuellingSerial();
            f.Text += " " + GlobalModel.app_version;
            f.ShowDialog();
        }

        private void warehouseTransferToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var f = new FWhouseTransfer();
            f.Text += " " + GlobalModel.app_version;
            f.ShowDialog();
        }

        private void listDataTransferToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var f = new FListDataTransfer();
            f.Text += " " + GlobalModel.app_version;
            f.MdiParent = this;
            f.Show();
        }

        private void receivedFTPortToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var f = new FReceivedFTPort();
            f.Text += " " + GlobalModel.app_version;
            f.MdiParent = this;
            f.WindowState = FormWindowState.Maximized;
            f.WindowState = FormWindowState.Maximized;
            f.Show();
        }

        private void receivedFTDaratToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var f = new FReceivedFTDarat();
            f.Text += " " + GlobalModel.app_version;
            f.MdiParent = this;
            f.WindowState = FormWindowState.Maximized;
            f.WindowState = FormWindowState.Maximized;
            f.Show();
        }

        private void pengirimanFTPortPamaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var f = new FSendFTPort();
            f.Text += " " + GlobalModel.app_version;
            f.MdiParent = this;
            f.WindowState = FormWindowState.Maximized;
            f.WindowState = FormWindowState.Maximized;
            f.Show();
        }

        private void warehouseTransferPengirimToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var f = new FWHouseTransferPengirim();
            f.Text += " " + GlobalModel.app_version;
            f.ShowDialog();
        }

        private void btnRefPitstop_Click(object sender, EventArgs e)
        {
            var f = new FTransaction();
            f.Text += " " + GlobalModel.app_version;
            f.ShowDialog();
        }

        private void btnRefFT_Click(object sender, EventArgs e)
        {
            var _proper = Properties.Settings.Default;
            if (_proper.device_monitor == "10")
            {
                var f = new FRefuellingSerial();
                f.Text += " " + GlobalModel.app_version;
                f.ShowDialog();
            } else if (_proper.device_monitor == "7")
            {
                var f = new FRefuellingSerial7inch();
                f.Text += " " + GlobalModel.app_version;
                f.ShowDialog();
            }
            
        }

        private void btnWhTransfer_Click(object sender, EventArgs e)
        {
         

            var _proper = Properties.Settings.Default;
            if (_proper.device_monitor == "10")
            {
                var f = new FWhouseTransfer();
                f.Text += " " + GlobalModel.app_version;
                f.ShowDialog();
            }
            else if (_proper.device_monitor == "7")
            {
                var f = new FWhouseTransfer7Inch();
                f.Text += " " + GlobalModel.app_version;
                f.ShowDialog();
            }
        }

        private void btnWhPengirim_Click(object sender, EventArgs e)
        {
            var f = new FWHouseTransferPengirim();
            f.Text += " " + GlobalModel.app_version;
            f.ShowDialog();
        }
       

        private async void btnReceivedFtPort_Click(object sender, EventArgs e)
        {
            var f = new FReceivedFTPort();
            f.Text += " " + GlobalModel.app_version;
            //f.MdiParent = this;
            //f.WindowState = FormWindowState.Maximized;
            //f.WindowState = FormWindowState.Maximized;
            f.ShowDialog();
            //var checkconnect = await checkConnectivity();
            //if (checkconnect.Contains("Supply Service"))
            ////if (statusOnline)
            //{
            //    var f = new FReceivedFTPort();
            //    f.Text += " " + GlobalModel.app_version;
            //    //f.MdiParent = this;
            //    //f.WindowState = FormWindowState.Maximized;
            //    //f.WindowState = FormWindowState.Maximized;
            //    f.ShowDialog();
            //}
            //else
            //{
            //    var f = new FPopUpNotification("Status saat ini OFFLINE mohon untuk sambungkan jaringan dan ganti setting connection menjadi ONLINE \r\n Setting > User Setting > Connection > Save", "Failed To Connect");
            //    f.ShowDialog();
            //}

        }
        //public async Task<string> checkConnectivity()
        //{
        //    try
        //    {
        //        WebClient requestCek = new WebClient();
        //        var str = requestCek.DownloadString(pUrl_service);
        //        return str;
        //    }
        //    catch (WebException ex)
        //    {
        //        return "";
        //    }
        //}
        private void btnReceivedFtDarat_Click(object sender, EventArgs e)
        {
            var f = new FReceivedFTDarat();
            f.Text += " " + GlobalModel.app_version;
            //f.MdiParent = this;
            //f.WindowState = FormWindowState.Maximized;
            //f.WindowState = FormWindowState.Maximized;
            f.ShowDialog();
            //if (statusOnline)
            //{
            //    var f = new FReceivedFTDarat();
            //    f.Text += " " + GlobalModel.app_version;
            //    //f.MdiParent = this;
            //    //f.WindowState = FormWindowState.Maximized;
            //    //f.WindowState = FormWindowState.Maximized;
            //    f.ShowDialog();
            //}
            //else
            //{
            //    var f = new FPopUpNotification("Status saat ini OFFLINE mohon untuk sambungkan jaringan dan ganti setting connection menjadi ONLINE \r\n Setting > User Setting > Connection > Save", "Failed To Connect");
            //    f.ShowDialog();
            //}
        }

        private void btnPengirimanFtPortPama_Click(object sender, EventArgs e)
        {
            var f = new FSendFTPort();
            f.Text += " " + GlobalModel.app_version;
            //f.MdiParent = this;
            //f.WindowState = FormWindowState.Maximized;
            //f.WindowState = FormWindowState.Maximized;
            f.ShowDialog();
            //var checkconnect = await checkConnectivity();
            //if (checkconnect.Contains("Supply Service"))
            ////if (statusOnline)
            //{
            //    var f = new FSendFTPort();
            //    f.Text += " " + GlobalModel.app_version;
            //    //f.MdiParent = this;
            //    //f.WindowState = FormWindowState.Maximized;
            //    //f.WindowState = FormWindowState.Maximized;
            //    f.ShowDialog();
            //}
            //else
            //{
            //    var f = new FPopUpNotification("Status saat ini OFFLINE mohon untuk sambungkan jaringan dan ganti setting connection menjadi ONLINE \r\n Setting > User Setting > Connection > Save", "Failed To Connect");
            //    f.ShowDialog();
            //}
        }

        private void btnListDataIssued_Click(object sender, EventArgs e)
        {            
            var _proper = Properties.Settings.Default;
            if (_proper.device_monitor == "10")
            {
                var f = new FListData();
                f.Text += " " + GlobalModel.app_version;
                //f.MdiParent = this;
                f.ShowDialog();
            }
            else if (_proper.device_monitor == "7")
            {
                var f = new FListData7Inch();
                f.Text += " " + GlobalModel.app_version;
                //f.MdiParent = this;
                f.ShowDialog();
            }
        }

        private void btnListDataTransfer_Click(object sender, EventArgs e)
        {
            var _proper = Properties.Settings.Default;
            if (_proper.device_monitor == "10")
            {
                var f = new FListDataTransfer();
                f.Text += " " + GlobalModel.app_version;
                //f.MdiParent = this;
                f.ShowDialog();            
            } else if (_proper.device_monitor == "7")
            {
                var f = new FListDataTransfer7Inch();
                f.Text += " " + GlobalModel.app_version;
                //f.MdiParent = this;
                f.ShowDialog();
            }
                
        }

        private async void statusTypeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var f = new FUserSetting();
            f.Text += " " + GlobalModel.app_version;
            if (f.ShowDialog() == DialogResult.OK)
            {

                toolConnection.Text = f.toolConnection;
                var checkconnect = await checkConnectivity();
                if (checkconnect.Contains("Supply Service"))
                //if (f.toolConnection == "ONLINE")
                {
                    toolConnection.BackColor = Color.Green;
                    toolConnection.ForeColor = Color.White;
                    statusOnline = true;
                }
                else
                {
                    toolConnection.BackColor = Color.Red;
                    toolConnection.ForeColor = Color.White;
                    statusOnline = false;
                }

                auto_sync_on_off = f.auto_sync == "ON" ? true : false;
                lblAutoSyncStatus.Text = "Auto Sync " + f.auto_sync;

                //string version = Assembly.GetExecutingAssembly().GetName().Version.ToString();
                //if (f.mg_transfer_issuing == true && f.mg_receiving == false)
                //{
                //    btnRefPitstop.Show();
                //    btnRefFT.Show();
                //    btnWhTransfer.Show();
                //    btnWhPengirim.Show();
                //    btnListDataIssued.Show();
                //    btnListDataTransfer.Show();
                //    btnReceivedFtPort.Hide();
                //    btnReceivedFtDarat.Hide();
                //    btnPengirimanFtPortPama.Hide();
                //    //this.Text = "iFLash Desktop " + version;
                //    this.Text = "iFLash Desktop " + GlobalModel.app_version;
                //}
                //else if (f.mg_receiving == true && f.mg_transfer_issuing == false)
                //{
                //    btnRefPitstop.Hide();
                //    btnRefFT.Hide();
                //    btnWhTransfer.Hide();
                //    btnWhPengirim.Hide();
                //    btnListDataIssued.Hide();
                //    btnListDataTransfer.Hide();
                //    btnReceivedFtPort.Show();
                //    btnReceivedFtDarat.Show();
                //    btnPengirimanFtPortPama.Show();
                //    //this.Text = "iFLash Desktop " + version + " Received";
                //    this.Text = "iFLash Desktop " + GlobalModel.app_version + " Received";
                //}
                //else
                //{
                //    btnRefPitstop.Show();
                //    btnRefFT.Show();
                //    btnWhTransfer.Show();
                //    btnWhPengirim.Show();
                //    btnListDataIssued.Show();
                //    btnListDataTransfer.Show();
                //    btnReceivedFtPort.Show();
                //    btnReceivedFtDarat.Show();
                //    btnPengirimanFtPortPama.Show();
                //    //this.Text = "iFLash Desktop " + version;
                //    this.Text = "iFLash Desktop " + GlobalModel.app_version;
                //}
               

                statusStrip1.Update();

            }
        }


        private void FMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (appExit == "Yes")
            {
                var f = new FLogin();
                f.ShowDialog();
            }
            else
            {
                var result = MessageBox.Show("Sure?", "Exit", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    appExit = result.ToString();
                    var f = new FLogin();
                    f.ShowDialog();
                }
                else
                {
                    e.Cancel = true;
                }
            }

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            var result = MessageBox.Show("Sure?", "Log Out", MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                this.Hide();
                var f = new FLogin();
                f.ShowDialog();
            }         
        }

        private async void settingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var f = new FUserSetting();
            f.Text += " " + GlobalModel.app_version;
            if (f.ShowDialog() == DialogResult.OK)
            {
                //toolConnection.Text = f.toolConnection;
                var checkconnect = await checkConnectivity();
                if (checkconnect.Contains("Supply Service"))
                //if (f.toolConnection == "ONLINE")
                {
                    toolConnection.BackColor = Color.Green;
                    toolConnection.ForeColor = Color.White;
                    statusOnline = true;
                }
                else
                {
                    toolConnection.BackColor = Color.Red;
                    toolConnection.ForeColor = Color.White;
                    statusOnline = false;
                }

                auto_sync_on_off = f.auto_sync == "ON" ? true : false;
                lblAutoSyncStatus.Text = "Auto Sync " + f.auto_sync;
                if (f.menu_refueling_pitstop == "True")
                {
                    btnRefPitstop.Show();
                    btnRefPS7Inch.Show();
                }
                else
                {
                    btnRefPitstop.Hide();
                    btnRefPS7Inch.Hide();

                }

                if (f.menu_refueling_ft == "True")
                {
                    btnRefFT.Show();
                    btnRefFT7Inch.Show();
                }
                else
                {
                    btnRefFT.Hide();
                    btnRefFT7Inch.Hide();
                }

                if (f.menu_warehouse_transfer == "True")
                {
                    btnWhTransfer.Show();
                    btnWhTransfer7Inch.Show();
                }
                else
                {
                    btnWhTransfer.Hide();
                    btnWhTransfer7Inch.Hide();
                }

                if (f.menu_warehouse_transfer_pengirim == "True")
                {
                    btnWhPengirim.Show();
                }
                else
                {
                    btnWhPengirim.Hide();
                }

                if (f.menu_received_ft_port == "True")
                {
                    btnReceivedFtPort.Show();
                    this.Text = "iFLash Desktop " + GlobalModel.app_version + " Received";
                }
                else
                {
                    btnReceivedFtPort.Hide();
                    this.Text = "iFLash Desktop " + GlobalModel.app_version;
                }

                if (f.menu_received_ft_darat == "True")
                {
                    btnReceivedFtDarat.Show();
                }
                else
                {
                    btnReceivedFtDarat.Hide();
                }

                if (f.menu_pengiriman_ft_port_pama == "True")
                {
                    btnPengirimanFtPortPama.Show();
                }
                else
                {
                    btnPengirimanFtPortPama.Hide();
                }

                if (f.menu_list_data_issued == "True")
                {
                    btnListDataIssued.Show();
                    btnListDataIssued7Inch.Show();
                }
                else
                {
                    btnListDataIssued.Hide();
                    btnListDataIssued7Inch.Hide();
                }

                if (f.menu_list_data_transfer == "True")
                {
                    btnListDataTransfer.Show();
                    btnListDataTransfer7Inch.Show();
                }
                else
                {
                    btnListDataTransfer.Hide();
                    btnListDataTransfer7Inch.Hide();
                }

                if (f.menu_receive_direct == "True")
                {
                    btnReceivedDirect.Show();                    
                }
                else
                {
                    btnReceivedDirect.Hide();                    
                }

                if (f.menu_list_receive_direct == "True")
                {
                    btnListDataReceiveDirect.Show();
                }
                else
                {
                    btnListDataReceiveDirect.Hide();
                }

                if (f.menu_list_data_pengiriman_ft_port_pama == "True")
                {
                    btnListDataPengirimanFTPortPama.Show();
                }
                else
                {
                    btnListDataPengirimanFTPortPama.Hide();
                }

                if (f.menu_list_data_received_ft_port == "True")
                {
                    btnListDataReceivedFTPort.Show();
                }
                else
                {
                    btnListDataReceivedFTPort.Hide();
                }

                if (f.menu_list_data_received_ft_darat == "True")
                {
                    btnListDataReceiveFTDarat.Show();
                }
                else
                {
                    btnListDataReceiveFTDarat.Hide();
                }

                if (f.menu_receive_direct_from_owner == "True")
                {
                    btnReceiveDirectOwner.Show();
                }
                else
                {
                    btnReceiveDirectOwner.Hide();
                }

                if (f.menu_pengiriman_pot == "True")
                {
                    btnPengirimanPOT.Show();
                }
                else
                {
                    btnPengirimanPOT.Hide();
                }

                if (f.menu_list_data_receive_direct_owner == "True")
                {
                    btnListDataReceiveDirectOwner.Show();
                }
                else
                {
                    btnListDataReceiveDirectOwner.Hide();
                }

                if (f.menu_list_data_pengiriman_pot == "True")
                {
                    btnListDataPengirimanPOT.Show();
                }
                else
                {
                    btnListDataPengirimanPOT.Hide();
                }

                timer1.Interval = int.Parse(f.interval_auto_sync);
                statusStrip1.Update();

            }
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var f = new FAboutApp();

            f.ShowDialog();
        }

        private void logToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var f = new FLog();
            f.ShowDialog();
        }

      

        private async void btnReceivedDirect_Click(object sender, EventArgs e)
        {
            var f = new FReceivedDirect();
            f.Text += " " + GlobalModel.app_version;            
            f.ShowDialog();
            //var checkconnect = await checkConnectivity();
            //if (checkconnect.Contains("Supply Service"))
            ////if (statusOnline)
            //{
            //    var f = new FReceivedDirect();
            //    f.Text += " " + GlobalModel.app_version;
            //    //f.MdiParent = this;
            //    //f.WindowState = FormWindowState.Maximized;
            //    //f.WindowState = FormWindowState.Maximized;
            //    f.ShowDialog();
            //}
            //else
            //{
            //    var f = new FPopUpNotification("Status saat ini OFFLINE mohon untuk sambungkan jaringan dan ganti setting connection menjadi ONLINE \r\n Setting > User Setting > Connection > Save", "Failed To Connect");
            //    f.ShowDialog();
            //}
        }
        
        private void btnListDataReceiveDirect_Click(object sender, EventArgs e)
        {
            var f = new FListDataReceiveDirect();
            f.Text += " " + GlobalModel.app_version;
            //f.MdiParent = this;
            f.ShowDialog();
        }

        private void btnListDataPengirimanFTPortPama_Click(object sender, EventArgs e)
        {
            var f = new FListDataPengirimanFTPortPama();
            f.Text += " " + GlobalModel.app_version;            
            f.ShowDialog();
        }

        private void btnListDataReceiveFTDarat_Click(object sender, EventArgs e)
        {
            var f = new FListDataReceiveFTDarat();
            f.Text += " " + GlobalModel.app_version;            
            f.ShowDialog();
        }

        private void btnListDataReceivedFTPort_Click(object sender, EventArgs e)
        {
            var f = new FlistDataReceiveFTPort();
            f.Text += " " + GlobalModel.app_version;            
            f.ShowDialog();
        }

        void mainButtonRefresh()
        {
            btnTransaksi.BackColor = Color.Orange;
            btnTransaksi.FlatAppearance.BorderSize = 0;            
            

            btnPengirimanReceive.BackColor = Color.SkyBlue;            
            btnPengirimanReceive.FlatAppearance.BorderSize = 0;

            btnListDataLocal.BackColor = Color.LawnGreen;            
            btnListDataLocal.FlatAppearance.BorderSize = 0;


        }

        void mainButtonSelect()
        {

        }

        private void btnTransaksi_Click(object sender, EventArgs e)
        {
            if (pnlTransaksi.Visible)
            {
                pnlTransaksi.Hide();
                mainButtonRefresh();
            }
            else
            {
                mainButtonRefresh();
                btnTransaksi.BackColor = Color.DarkGoldenrod;
                btnTransaksi.FlatAppearance.BorderColor = Color.Red;
                btnTransaksi.FlatAppearance.BorderSize = 5;
                pnlTransaksi.Show();
                pnlPengirimanReceive.Hide();
                pnlListData.Hide();
            }
        }

        private void btnPengirimanReceive_Click(object sender, EventArgs e)
        {
            if (pnlPengirimanReceive.Visible)
            {
                pnlPengirimanReceive.Hide();
                mainButtonRefresh();
            }
            else
            {
                mainButtonRefresh();
                btnPengirimanReceive.BackColor = Color.CadetBlue;
                btnPengirimanReceive.FlatAppearance.BorderColor = Color.Red;
                btnPengirimanReceive.FlatAppearance.BorderSize = 5;
                pnlTransaksi.Hide();
                pnlPengirimanReceive.Show();
                pnlListData.Hide();
            }
        }

        private void btnListDataLocal_Click(object sender, EventArgs e)
        {
            if (pnlListData.Visible)
            {
                pnlListData.Hide();
                mainButtonRefresh();
            }
            else
            {
                mainButtonRefresh();
                btnListDataLocal.BackColor = Color.Lime;
                btnListDataLocal.FlatAppearance.BorderColor = Color.Red;
                btnListDataLocal.FlatAppearance.BorderSize = 5;
                pnlTransaksi.Hide();
                pnlPengirimanReceive.Hide();
                pnlListData.Show();
            }
        }

        private void btnReceiveDirectOwner_Click(object sender, EventArgs e)
        {
            var f = new FReceivedDirectOwner();
            f.Text += " " + GlobalModel.app_version;
            f.ShowDialog();
        }

        private void btnPengirimanPOT_Click(object sender, EventArgs e)
        {
            var f = new FSendPOT();
            f.Text += " " + GlobalModel.app_version;
            f.ShowDialog();
        }

        private void btnListDataReceiveDirectOwner_Click(object sender, EventArgs e)
        {
            var f = new FListDataReceiveDirectOwner();
            f.Text += " " + GlobalModel.app_version;
            f.ShowDialog();
        }

        private void btnListDataPengirimanPOT_Click(object sender, EventArgs e)
        {
            var f = new FListDataPengirimanPOT();
            f.Text += " " + GlobalModel.app_version;
            f.ShowDialog();
        }

        private void btnRefPS7Inch_Click(object sender, EventArgs e)
        {
            var f = new FTransaction7Inch();
            f.Text += " " + GlobalModel.app_version;
            f.ShowDialog();
        }
    }
}
