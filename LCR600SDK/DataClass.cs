﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LCR600SDK.DataClass;
using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace LCR600SDK
{

    public class LcrData
    {
        public string GrossQTY { get; set; }
        public string GrossTotalizer { get; set; }
        public string GrossPreset { get; set; }
        public string Flowrate { get; set; }
        public string ValveStatus { get; set; }
    }

    public class ClsIssuing
    {
        public bool status { get; set; }
        public string remarks { get; set; }
        public List<String> failed_job_row_id { get; set; }
        public List<tbl_logsheet_detail> log_sheet_detail { get; set; }
    }
    public class ClsIssuingTransfer
    {
        public bool status { get; set; }
        public string remarks { get; set; }
    }

    public class ClsReceiveDirect
    {
        public bool status { get; set; }
        public string remarks { get; set; }
        public List<String> failed_pid { get; set; }
        public List<ListReceivedDirectClass> direct { get; set; }
    }

    public class ClsRitasiToMaintank
    {
        public bool status { get; set; }
        public string remarks { get; set; }
        public List<String> failed_pid { get; set; }
        public List<String> list_remarks { get; set; }
    }

    public class LoginModel
    {
        public string token { get; set; }
        public int expired { get; set; }
        public bool show_captcha { get; set; }
        public string remarks { get; set; }
        public string expired_date { get; set; }
        public bool success { get; set; }
        public string pid_user { get; set; }
        public string district { get; set; }
    }

    public partial class PlanCoachProfileModel
    {
        [JsonProperty("status", NullValueHandling = NullValueHandling.Ignore)]
        public bool? Status { get; set; }

        [JsonProperty("data_emp", NullValueHandling = NullValueHandling.Ignore)]
        public DataEmp DataEmp { get; set; }

        [JsonProperty("menu", NullValueHandling = NullValueHandling.Ignore)]
        public List<Menu> Menu { get; set; }

        [JsonProperty("transportir", NullValueHandling = NullValueHandling.Ignore)]
        public List<Transportir> Transportir { get; set; }

        [JsonProperty("whouse", NullValueHandling = NullValueHandling.Ignore)]
        public List<Whouse> Whouse { get; set; }

        [JsonProperty("unit", NullValueHandling = NullValueHandling.Ignore)]
        public List<Unit> Unit { get; set; }

        [JsonProperty("flow_meter", NullValueHandling = NullValueHandling.Ignore)]
        public List<FlowMeter> FlowMeter { get; set; }

        [JsonProperty("fuelman", NullValueHandling = NullValueHandling.Ignore)]
        public List<Fuelman> Fuelman { get; set; }

        [JsonProperty("driver", NullValueHandling = NullValueHandling.Ignore)]
        public List<Driver> Driver { get; set; }

        [JsonProperty("region", NullValueHandling = NullValueHandling.Ignore)]
        public List<Region> Region { get; set; }

        [JsonProperty("gl", NullValueHandling = NullValueHandling.Ignore)]
        public List<DataEmp> Gl { get; set; }

        [JsonProperty("data_issuing", NullValueHandling = NullValueHandling.Ignore)]
        public List<object> DataIssuing { get; set; }

        [JsonProperty("lokasi_filter", NullValueHandling = NullValueHandling.Ignore)]
        public List<LokasiFilter> LokasiFilter { get; set; }

        [JsonProperty("periode", NullValueHandling = NullValueHandling.Ignore)]
        public List<long> Periode { get; set; }

        [JsonProperty("doc_status", NullValueHandling = NullValueHandling.Ignore)]
        public List<DocStatus> DocStatus { get; set; }

        [JsonProperty("doc_status_approval", NullValueHandling = NullValueHandling.Ignore)]
        public List<DocStatus> DocStatusApproval { get; set; }

        [JsonProperty("master_lokasi", NullValueHandling = NullValueHandling.Ignore)]
        public List<MasterLokasi> MasterLokasi { get; set; }

        [JsonProperty("jenis_fluida", NullValueHandling = NullValueHandling.Ignore)]
        public List<string> JenisFluida { get; set; }

        [JsonProperty("tanki", NullValueHandling = NullValueHandling.Ignore)]
        public List<object> Tanki { get; set; }

        [JsonProperty("po_no_direct", NullValueHandling = NullValueHandling.Ignore)]
        public List<PoReceivedDirectClass> PoReceiveDirect { get; set; }

    }

    public partial class PoReceivedDirectClass
    {
        [JsonProperty("pid_po", NullValueHandling = NullValueHandling.Ignore)]
        public string PidPo { get; set; }

        [JsonProperty("po_no", NullValueHandling = NullValueHandling.Ignore)]
        public string PoNo { get; set; }

        [JsonProperty("district", NullValueHandling = NullValueHandling.Ignore)]
        public string Dstrct { get; set; }

        [JsonProperty("po_qty", NullValueHandling = NullValueHandling.Ignore)]
        public int PoQty { get; set; }

        [JsonProperty("whouse", NullValueHandling = NullValueHandling.Ignore)]
        public string WareHouse { get; set; }
    }


    public partial class DataEmp
    {
        [JsonProperty("nrp", NullValueHandling = NullValueHandling.Ignore)]
        public string Nrp { get; set; }

        [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }

        [JsonProperty("dstrct_code", NullValueHandling = NullValueHandling.Ignore)]
        public string DstrctCode { get; set; }

        [JsonProperty("position", NullValueHandling = NullValueHandling.Ignore)]
        public string Position { get; set; }
    }

    public partial class DocStatus
    {
        [JsonProperty("status", NullValueHandling = NullValueHandling.Ignore)]
        public long? Status { get; set; }

        [JsonProperty("status_desc", NullValueHandling = NullValueHandling.Ignore)]
        public string StatusDesc { get; set; }
    }

    public partial class Driver
    {
        [JsonProperty("pid", NullValueHandling = NullValueHandling.Ignore)]
        public string Pid { get; set; }

        [JsonProperty("nrp", NullValueHandling = NullValueHandling.Ignore)]
        public string Nrp { get; set; }

        [JsonProperty("driver_name", NullValueHandling = NullValueHandling.Ignore)]
        public string DriverName { get; set; }

        [JsonProperty("district", NullValueHandling = NullValueHandling.Ignore)]
        public string District { get; set; }

        [JsonProperty("position", NullValueHandling = NullValueHandling.Ignore)]
        public string Position { get; set; }

        [JsonProperty("status", NullValueHandling = NullValueHandling.Ignore)]
        public long? Status { get; set; }

        [JsonProperty("available_status")]
        public object AvailableStatus { get; set; }

        [JsonProperty("shifts")]
        public object Shifts { get; set; }

        [JsonProperty("start_shift")]
        public object StartShift { get; set; }

        [JsonProperty("end_shift")]
        public object EndShift { get; set; }

        [JsonProperty("created_date")]
        public object CreatedDate { get; set; }

        [JsonProperty("created_by")]
        public object CreatedBy { get; set; }

        [JsonProperty("created_by_desc")]
        public object CreatedByDesc { get; set; }

        [JsonProperty("modified_date")]
        public object ModifiedDate { get; set; }

        [JsonProperty("modified_by")]
        public object ModifiedBy { get; set; }

        [JsonProperty("modified_by_desc")]
        public object ModifiedByDesc { get; set; }
    }

    public partial class FlowMeter
    {
        [JsonProperty("pid", NullValueHandling = NullValueHandling.Ignore)]
        public string Pid { get; set; }

        [JsonProperty("dstrct_code", NullValueHandling = NullValueHandling.Ignore)]
        public string DstrctCode { get; set; }

        [JsonProperty("warehouse", NullValueHandling = NullValueHandling.Ignore)]
        public string Warehouse { get; set; }

        [JsonProperty("sn_code", NullValueHandling = NullValueHandling.Ignore)]
        public string SnCode { get; set; }

        [JsonProperty("fungsi", NullValueHandling = NullValueHandling.Ignore)]
        public string Fungsi { get; set; }

        [JsonProperty("mnemonic", NullValueHandling = NullValueHandling.Ignore)]
        public string Mnemonic { get; set; }

        [JsonProperty("meter_fakor", NullValueHandling = NullValueHandling.Ignore)]
        public double? MeterFakor { get; set; }

        [JsonProperty("kap_maks", NullValueHandling = NullValueHandling.Ignore)]
        public long? KapMaks { get; set; }

        [JsonProperty("kalibartion_date", NullValueHandling = NullValueHandling.Ignore)]
        public long? KalibartionDate { get; set; }

        [JsonProperty("kalibration_expired", NullValueHandling = NullValueHandling.Ignore)]
        public long? KalibrationExpired { get; set; }

        [JsonProperty("whouse_location", NullValueHandling = NullValueHandling.Ignore)]
        public string WhouseLocation { get; set; }

        [JsonProperty("flow_meter_location", NullValueHandling = NullValueHandling.Ignore)]
        public string FlowMeterLocation { get; set; }

        [JsonProperty("update_date", NullValueHandling = NullValueHandling.Ignore)]
        public string UpdateDate { get; set; }

        [JsonProperty("user_nrp", NullValueHandling = NullValueHandling.Ignore)]
        public string UserNrp { get; set; }

        [JsonProperty("flow_meter_no", NullValueHandling = NullValueHandling.Ignore)]
        public string FlowMeterNo { get; set; }

        [JsonProperty("status", NullValueHandling = NullValueHandling.Ignore)]
        public int? Status { get; set; }

        [JsonProperty("lcr_device")]
        public string LcrDevice { get; set; }
    }

    public partial class Fuelman
    {
        [JsonProperty("nrp", NullValueHandling = NullValueHandling.Ignore)]
        public string Nrp { get; set; }

        [JsonProperty("fuelman_name", NullValueHandling = NullValueHandling.Ignore)]
        public string FuelmanName { get; set; }

        [JsonProperty("district", NullValueHandling = NullValueHandling.Ignore)]
        public string District { get; set; }

        [JsonProperty("position", NullValueHandling = NullValueHandling.Ignore)]
        public string Position { get; set; }

        [JsonProperty("status", NullValueHandling = NullValueHandling.Ignore)]
        public long? Status { get; set; }
        [JsonProperty("rfid_id", NullValueHandling = NullValueHandling.Ignore)]
        public string rfid_id { get; set; }
    }

    public partial class LokasiFilter
    {
        [JsonProperty("pid_location", NullValueHandling = NullValueHandling.Ignore)]
        public Guid? PidLocation { get; set; }

        [JsonProperty("dstrct_code", NullValueHandling = NullValueHandling.Ignore)]
        public string DstrctCode { get; set; }

        [JsonProperty("cn_unit", NullValueHandling = NullValueHandling.Ignore)]
        public string CnUnit { get; set; }

        [JsonProperty("whouse", NullValueHandling = NullValueHandling.Ignore)]
        public string Whouse { get; set; }

        [JsonProperty("sn_flowmeter", NullValueHandling = NullValueHandling.Ignore)]
        public string SnFlowmeter { get; set; }

        [JsonProperty("filtrasi_area", NullValueHandling = NullValueHandling.Ignore)]
        public string FiltrasiArea { get; set; }

        [JsonProperty("pid_filter", NullValueHandling = NullValueHandling.Ignore)]
        public Guid? PidFilter { get; set; }

        [JsonProperty("housing_filter_type", NullValueHandling = NullValueHandling.Ignore)]
        public string HousingFilterType { get; set; }

        [JsonProperty("housing_filer_merk", NullValueHandling = NullValueHandling.Ignore)]
        public string HousingFilerMerk { get; set; }

        [JsonProperty("stock_code_filter", NullValueHandling = NullValueHandling.Ignore)]
        public string StockCodeFilter { get; set; }

        [JsonProperty("micron_rating", NullValueHandling = NullValueHandling.Ignore)]
        public long? MicronRating { get; set; }

        [JsonProperty("pn_filter_element", NullValueHandling = NullValueHandling.Ignore)]
        public string PnFilterElement { get; set; }

        [JsonProperty("merk_filter_element", NullValueHandling = NullValueHandling.Ignore)]
        public string MerkFilterElement { get; set; }

        [JsonProperty("create_date", NullValueHandling = NullValueHandling.Ignore)]
        public string CreateDate { get; set; }

        [JsonProperty("create_by", NullValueHandling = NullValueHandling.Ignore)]
        public string CreateBy { get; set; }

        [JsonProperty("mod_date")]
        public string ModDate { get; set; }

        [JsonProperty("mod_by")]
        public string ModBy { get; set; }
    }

    public partial class MasterLokasi
    {
        [JsonProperty("pid", NullValueHandling = NullValueHandling.Ignore)]
        public Guid? Pid { get; set; }

        [JsonProperty("whouse", NullValueHandling = NullValueHandling.Ignore)]
        public string Whouse { get; set; }

        [JsonProperty("stock_code", NullValueHandling = NullValueHandling.Ignore)]
        public string StockCode { get; set; }

        [JsonProperty("oil_type", NullValueHandling = NullValueHandling.Ignore)]
        public string OilType { get; set; }

        [JsonProperty("location", NullValueHandling = NullValueHandling.Ignore)]
        public string Location { get; set; }

        [JsonProperty("description", NullValueHandling = NullValueHandling.Ignore)]
        public string Description { get; set; }

        [JsonProperty("district", NullValueHandling = NullValueHandling.Ignore)]
        public string District { get; set; }

        [JsonProperty("status", NullValueHandling = NullValueHandling.Ignore)]
        public bool? Status { get; set; }
    }

    public partial class Menu
    {
        [JsonProperty("pid", NullValueHandling = NullValueHandling.Ignore)]
        public Guid? Pid { get; set; }

        [JsonProperty("nrp", NullValueHandling = NullValueHandling.Ignore)]
        public string Nrp { get; set; }

        [JsonProperty("appl_id", NullValueHandling = NullValueHandling.Ignore)]
        public string ApplId { get; set; }

        [JsonProperty("appl_name", NullValueHandling = NullValueHandling.Ignore)]
        public string ApplName { get; set; }

        [JsonProperty("appl_group", NullValueHandling = NullValueHandling.Ignore)]
        public string ApplGroup { get; set; }

        [JsonProperty("class_name", NullValueHandling = NullValueHandling.Ignore)]
        public string ClassName { get; set; }

        [JsonProperty("icon", NullValueHandling = NullValueHandling.Ignore)]
        public Uri Icon { get; set; }

        [JsonProperty("urls", NullValueHandling = NullValueHandling.Ignore)]
        public string Urls { get; set; }

        [JsonProperty("urutan", NullValueHandling = NullValueHandling.Ignore)]
        public long? Urutan { get; set; }
    }

    public partial class Region
    {
        [JsonProperty("pid", NullValueHandling = NullValueHandling.Ignore)]
        public string Pid { get; set; }

        [JsonProperty("region_id", NullValueHandling = NullValueHandling.Ignore)]
        public string RegionId { get; set; }

        [JsonProperty("region_name", NullValueHandling = NullValueHandling.Ignore)]
        public string RegionName { get; set; }

        [JsonProperty("district", NullValueHandling = NullValueHandling.Ignore)]
        public string District { get; set; }
    }

    public partial class Transportir
    {
        [JsonProperty("transportir_code", NullValueHandling = NullValueHandling.Ignore)]
        public string TransportirCode { get; set; }

        [JsonProperty("transportir_name", NullValueHandling = NullValueHandling.Ignore)]
        public string TransportirName { get; set; }

        [JsonProperty("isactive", NullValueHandling = NullValueHandling.Ignore)]
        public bool? Isactive { get; set; }

        [JsonProperty("district", NullValueHandling = NullValueHandling.Ignore)]
        public string District { get; set; }
    }

    public partial class Unit
    {
        [JsonProperty("pid", NullValueHandling = NullValueHandling.Ignore)]
        public string pid { get; set; } 

        [JsonProperty("unit_no", NullValueHandling = NullValueHandling.Ignore)]
        public string UnitNo { get; set; }

        [JsonProperty("district", NullValueHandling = NullValueHandling.Ignore)]
        public string District { get; set; }

        [JsonProperty("egi", NullValueHandling = NullValueHandling.Ignore)]
        public string Egi { get; set; }

        [JsonProperty("egi_desc", NullValueHandling = NullValueHandling.Ignore)]
        public string EgiDesc { get; set; }

        [JsonProperty("rfid_id", NullValueHandling = NullValueHandling.Ignore)]
        public string rfid_id { get; set; }

        [JsonProperty("locations", NullValueHandling = NullValueHandling.Ignore)]
        public string Locations { get; set; }

        [JsonProperty("equip_class", NullValueHandling = NullValueHandling.Ignore)]
        public string EquipClass { get; set; }

        [JsonProperty("status", NullValueHandling = NullValueHandling.Ignore)]
        public string Status { get; set; }

        [JsonProperty("starttime", NullValueHandling = NullValueHandling.Ignore)]
        public string Starttime { get; set; }

        [JsonProperty("endtime", NullValueHandling = NullValueHandling.Ignore)]
        public string Endtime { get; set; }

        [JsonProperty("priority", NullValueHandling = NullValueHandling.Ignore)]
        public string Priority { get; set; }

        [JsonProperty("text_header", NullValueHandling = NullValueHandling.Ignore)]
        public string TextHeader { get; set; }

        [JsonProperty("text_body", NullValueHandling = NullValueHandling.Ignore)]
        public string TextBody { get; set; }

        [JsonProperty("MAX_TANK_CAPACITY", NullValueHandling = NullValueHandling.Ignore)]
        public long? MaxTankCapacity { get; set; }

        [JsonProperty("WORK_AREA", NullValueHandling = NullValueHandling.Ignore)]
        public string WorkArea { get; set; }

        [JsonProperty("LATEST_HM", NullValueHandling = NullValueHandling.Ignore)]
        public long? LatestHm { get; set; }
    }

    public partial class Whouse
    {
        [JsonProperty("pid", NullValueHandling = NullValueHandling.Ignore)]
        public string Pid { get; set; }

        [JsonProperty("warehouseid", NullValueHandling = NullValueHandling.Ignore)]
        public string Warehouseid { get; set; }

        [JsonProperty("warehousename", NullValueHandling = NullValueHandling.Ignore)]
        public string Warehousename { get; set; }

        [JsonProperty("warehousetype", NullValueHandling = NullValueHandling.Ignore)]
        public string Warehousetype { get; set; }

        [JsonProperty("district", NullValueHandling = NullValueHandling.Ignore)]
        public string District { get; set; }

        [JsonProperty("fungsi", NullValueHandling = NullValueHandling.Ignore)]
        public string Fungsi { get; set; }

        [JsonProperty("availablestatus", NullValueHandling = NullValueHandling.Ignore)]
        public long? Availablestatus { get; set; }

        [JsonProperty("issuedhd", NullValueHandling = NullValueHandling.Ignore)]
        public long? Issuedhd { get; set; }

        [JsonProperty("istransfer", NullValueHandling = NullValueHandling.Ignore)]
        public long? Istransfer { get; set; }

        [JsonProperty("isreceiving", NullValueHandling = NullValueHandling.Ignore)]
        public long? Isreceiving { get; set; }

        [JsonProperty("issupport", NullValueHandling = NullValueHandling.Ignore)]
        public long? Issupport { get; set; }

        [JsonProperty("isexavator", NullValueHandling = NullValueHandling.Ignore)]
        public long? Isexavator { get; set; }

        [JsonProperty("lineactive", NullValueHandling = NullValueHandling.Ignore)]
        public long? Lineactive { get; set; }

        [JsonProperty("modifieddate", NullValueHandling = NullValueHandling.Ignore)]
        public string Modifieddate { get; set; }

        [JsonProperty("modifiedby", NullValueHandling = NullValueHandling.Ignore)]
        public string Modifiedby { get; set; }

        [JsonProperty("modifiedbydesc", NullValueHandling = NullValueHandling.Ignore)]
        public string Modifiedbydesc { get; set; }

        [JsonProperty("issubmited", NullValueHandling = NullValueHandling.Ignore)]
        public long? Issubmited { get; set; }

        [JsonProperty("docstatus", NullValueHandling = NullValueHandling.Ignore)]
        public long? Docstatus { get; set; }

        [JsonProperty("kategori_whouse")]
        public string KategoriWhouse { get; set; }

        [JsonProperty("kategori_whouse_to")]
        public string KategoriWhouseTo { get; set; }

        [JsonProperty("is_issued")]
        public string IsIssued { get; set; }
    }

    public partial class transportir_truck
    {
        public bool? Status { get; set; }       

        [JsonProperty("data", NullValueHandling = NullValueHandling.Ignore)]
        public List<transportir_truck> ListData { get; set; }
        
    }

    public partial class transportir_truck
    {
      public string PID {get;set;} 
      public string DSTRCT_CODE {get;set;}   
      public string TRANSPORTIR_KODE {get;set;}   
      public string FT_NUMBER {get;set;}   
      public string KOMISIONING_DATE {get;set;}   
      public string NO_POLISI {get;set;}   
      public string KAPASITAS_ANGKUT {get;set;}   
      public string STATUS {get;set;}   
    }
   
    public class tbl_logsheet_detail
    {
        public String job_row_id { get; set; }
        public String dstrct_code { get; set; }
        public String log_sheet_code { get; set; }
        public String input_type { get; set; }
        public DateTime issued_date { get; set; }
        public String whouse_id { get; set; }
        public String unit_no { get; set; }
        public Double? max_tank_capacity { get; set; }
        public int? hm_before { get; set; }
        public int? hm { get; set; }
        public String flw_meter { get; set; }
        public Double? meter_faktor { get; set; }
        public Double? qty_loqsheet { get; set; }
        public Double? qty { get; set; }
        public String shift { get; set; }
        public String fuel_oil_type { get; set; }
        public String stat_type { get; set; }
        public String nrp_operator { get; set; }
        public String nama_operator { get; set; }
        public String driver_nrp { get; set; }
        public String driver_name { get; set; }
        public String fuelman_nrp { get; set; }
        public String fuelman_name { get; set; }
        public String work_area { get; set; }
        public String location { get; set; }
        public String ref_condition { get; set; }
        public String ref_hour_start { get; set; }
        public String ref_hour_stop { get; set; }
        public String note { get; set; }
        public String timezone { get; set; }
        public String flag_loading { get; set; }
        public String loadingerror { get; set; }
        public DateTime loadingdatetime { get; set; }
        public String mod_by { get; set; }
        public DateTime mod_date { get; set; }
        public int is_load_to_fosto { get; set; }
        public String foto { get; set; }
        public Double? flow_meter_start { get; set; }
        public Double? flow_meter_end { get; set; }
        public String resp_code { get; set; }
        public String resp_name { get; set; }
        public String text_header { get; set; }
        public String text_sub_header { get; set; }
        public String text_body { get; set; }
        public int syncs { get; set; }
    }

    public class whtranfer
    {
        public String job_row_id { get; set; }
        public String xfer_code { get; set; }
        public String xfer_date { get; set; }
        public String dstrct_code { get; set; }
        public String employee_id { get; set; }
        public String issued_whouse { get; set; }
        public String received_whouse { get; set; }
        public String stock_code { get; set; }
        public Double? qty_xfer { get; set; }
        public String shift { get; set; }
        public String fmeter_serial_no { get; set; }
        public Double? faktor_meter { get; set; }
        public Double? fmeter_begin { get; set; }
        public Double? fmeter_end { get; set; }
        public String created_by { get; set; }
        public String xfer_by { get; set; }
        public String received_by { get; set; }
        public int is_submit { get; set; }
        public String issued_whouse_type { get; set; }
        public String received_whouse_type { get; set; }
        public DateTime mod_date { get; set; }
        public DateTime created_date { get; set; }
        public String timezone { get; set; }
        public int syncs { get; set; }

    }
    public class tbl_log
    {
        public String pid { get; set; }
        public String actions { get; set; }
        public String message { get; set; }
        public DateTime mod_date { get; set; }
        public String mod_by { get; set; }
    }

    public class tbl_setting
    {
        public String pid { get; set; }
        public String whouse { get; set; }
        public String whousename { get; set; }
        public String driver_nrp { get; set; }
        public String driver_name { get; set; }
        public String fuelman_nrp { get; set; }
        public String fuelman_name { get; set; }
        public String sncode { get; set; }
        public String startshift1 { get; set; }
        public String startshift2 { get; set; }
        public Double meterFakor { get; set; }
        public String mod_date { get; set; }
        public String mod_by { get; set; }
    }

    public class returnValue
    {
        public bool status { get; set; }
        public string remarks { get; set; }
    }

    public class refueling
    {
        public String stat_type { get; set; }
        public String trans_type_select { get; set; }
        public int preset_gross { get; set; }
        public double receive_qty1 { get; set; }
        public double receive_flow_meter_awal1 { get; set; }
        public double receive_flow_meter_akhir1 { get; set; }
        public DateTime start_transaction { get; set; }
        public DateTime stop_transaction { get; set; }

    }

    public partial class tbl_t_receive_direct
    {

        public string pid_receive { get; set; }
        public string ref_id { get; set; }
        public string district { get; set; }
        public string po_no { get; set; }        
        public string receive_at { get; set; }
        public string receive_at_code { get; set; }
        public int? receive_qty { get; set; }
        public string receive_sn_flow_meter1 { get; set; }
        public string receive_meter_faktor { get; set; }
        public string receive_density { get; set; }
        public string receive_temperature { get; set; }
        public string receive_by { get; set; }
        public DateTime? receive_date { get; set; }                        
        public string sir_no { get; set; }
        public int? receive_qty_pama { get; set; }
        public DateTime? receive_date_exit { get; set; }
        public int? totaliser_awal { get; set; }
        public int? totaliser_akhir { get; set; }
        public string start_loading { get; set; }
        public string end_loading { get; set; }





    }

    public partial class tbl_t_send_maintank_ritasi
    {
        public string pid_ritasi_maintank { get; set; }
        public string ref_tbl { get; set; }
        public string ref_id { get; set; }
        public string district { get; set; }
        public string send_type { get; set; }
        public string send_po_no { get; set; }
        public string send_do_no { get; set; }
        public string send_qty { get; set; }
        public string transportir_code { get; set; }
        public string transportir_name { get; set; }
        public string send_ft_no { get; set; }
        public string send_driver_name { get; set; }
        public DateTime? send_date { get; set; }
        public int? send_bbn { get; set; }
        public string send_from { get; set; }
        public string send_from_code { get; set; }
        public string send_to { get; set; }
        public string send_to_code { get; set; }
        public double? send_dipping_hole1_port { get; set; }
        public double? send_dipping_hole2_port { get; set; }
        public int? send_iscomplete { get; set; }
        public string send_sn_flow_meter { get; set; }
        public double? send_meter_faktor { get; set; }
        public int? send_flow_meter_awal { get; set; }
        public int? send_flow_meter_akhir { get; set; }
        public int? send_qty_x_mf { get; set; }
        public string send_no_segel_1 { get; set; }
        public string send_no_segel_2 { get; set; }
        public string send_no_segel_3 { get; set; }
        public string send_no_segel_4 { get; set; }
        public string send_no_segel_5 { get; set; }
        public string send_no_segel_6 { get; set; }
        public string doc_status { get; set; }
        public string last_mod_by { get; set; }        
        public string foto { get; set; }
        public DateTime? send_time_start { get; set; }
        public DateTime? send_time_end { get; set; }
    }

    public partial class tbl_t_receive_maintank_ritasi
    {
        public string pid_ritasi_maintank { get; set; }
        public DateTime? send_date { get; set; }
        public string send_po_no { get; set; }
        public string district { get; set; }
        public string receive_at { get; set; }
        public string receive_at_code { get; set; }
        public Double? receive_dipping_hole1_mt { get; set; }
        public Double? receive_dipping_hole2_mt { get; set; }
        public string receive_sn_flow_meter1 { get; set; }
        public Double? receive_meter_faktor { get; set; }
        public int? receive_flow_meter_awal1 { get; set; }
        public int? receive_flow_meter_akhir1 { get; set; }
        public int? receive_qty1 { get; set; }
        public int? receive_qty1_x_mf { get; set; }
        public string receive_sn_flow_meter2 { get; set; }
        public Double? receive_meter_faktor2 { get; set; }
        public int? receive_flow_meter_awal2 { get; set; }
        public int? receive_flow_meter_akhir2 { get; set; }
        public int? receive_qty2 { get; set; }
        public int? receive_qty2_x_mf { get; set; }
        public int? receive_qty_total { get; set; }
        public Double? receive_density { get; set; }
        public int? receive_temperature { get; set; }
        public int? receive_qty_additive { get; set; }
        public int? receive_iscomplete { get; set; }
        public string receive_driver_name { get; set; }
        public string receive_no_segel_1 { get; set; }
        public string receive_no_segel_2 { get; set; }
        public string receive_no_segel_3 { get; set; }
        public string receive_no_segel_4 { get; set; }
        public string receive_no_segel_5 { get; set; }
        public string receive_no_segel_6 { get; set; }
        public string doc_status { get; set; }
        public string last_mod_by { get; set; }
        public string foto { get; set; }
        public DateTime? time_start { get; set; }
        public DateTime? time_end { get; set; }
    }
}
