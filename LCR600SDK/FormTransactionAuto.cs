﻿using MySql.Data.MySqlClient; using System.Data.SQLite; using System.IO; using System.Reflection;
using SDK;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;

namespace LCR600SDK
{
    public partial class FormTransactionAuto : Form
    {
        private string myConnectionString = string.Empty;
        private byte device = 0;
        private int stepTimerOpen = 0;
        private int stepTimerClose = 0;

        MqttClient client;
        string clientId;

        public FormTransactionAuto()
        {
            InitializeComponent();

            string BrokerAddress = "127.0.0.1";

            client = new MqttClient(BrokerAddress);

            // register a callback-function (we have to implement, see below) which is called by the library when a message was received
            client.MqttMsgPublishReceived += client_MqttMsgPublishReceived;

            // use a unique id as client id, each time we start the application
            clientId = Guid.NewGuid().ToString();

            client.Connect(clientId);
        }

        // this code runs when a message was received
        void client_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e)
        {
            try
            {
                string ReceivedMessage = Encoding.UTF8.GetString(e.Message);
                txtReceived.Invoke((MethodInvoker)(() => txtReceived.Text = ReceivedMessage));

                panel4.Invoke((MethodInvoker)(() => panel4.Enabled = !txtReceived.Text.Contains("disconnected")));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private bool open()
        {
            return true;
        }

        private bool start()
        {
            return true;
        }

        private bool print()
        {
            return true;
        }

        private bool closed()
        {
            return true;
        }

        #region LCR CORE
        private byte doConnectLCR()
        {
            byte device = 250;
            byte numDevices;
            byte[] deviceList = new byte[device];
            IntPtr deviceListPtr = Marshal.AllocHGlobal(250);
            byte rc = LCP02API.LCP02Open(device, device, out numDevices, deviceListPtr);
            if (rc != (byte)LCPAPI.LCPR.LCPR_OK && rc != (byte)LCP02API.LCR_RETURN_CODES.LCP02Ra_ALREADYOPENED)
            {
                device = 0;
            }
            return device;
        }

        private void doOpenLCR()
        {
            try
            {
                device = doConnectLCR();
            }
            catch (Exception)
            {

                throw;
            }
        }

        private string doStartLCR()
        {
            string strStatus = "PLEASE WAIT..";

            if (device == 0)
                return strStatus;

            byte rc = LCP02API.LCP02IssueCommand(device, (byte)LCP02API.LCR_COMMANDS.LCRC_RUN,
                                                         (byte)LCP02API.LCR_RETURN_CODES.LCRM_NO_WAIT);

            if (rc == (byte)LCPAPI.LCPR.LCPR_OK)
                strStatus = "RUN";

            return strStatus;
        }

        private string doPrintLCR()
        {
            string strStatus = "PLEASE WAIT..";

            if (device == 0)
                return strStatus;


            byte rc = LCP02API.LCP02IssueCommand(device, (byte)LCP02API.LCR_COMMANDS.LCRC_PRINT,
                                                         (byte)LCP02API.LCR_RETURN_CODES.LCRM_NO_WAIT);

            if (rc == (byte)LCPAPI.LCPR.LCPR_OK)
                strStatus = "STOP";

            return strStatus;
        }

        private bool doCloseLCR()
        {
            byte rc = LCP02API.LCP02Close();
            if (rc != (byte)LCPAPI.LCPR.LCPR_OK)
            {
                //MessageBox.Show("LCP02Close error, rc =" + rc)
                Console.WriteLine("LCP02Close error, rc =" + rc);
                return false;
            }
            return true;
        }
        #endregion

        private string getGrossQty()
        {
            string grossQtyStr = "PLEASE WAIT..";
            try
            {

                byte rc;

                if (device == 0)
                {
                    return grossQtyStr;
                }

                byte fieldNum = (byte)LCP02API.LCR_FIELD_NUMBERS.LCRF_GrossQty_NE;
                int size = LCP02API.LCP02FieldSize(fieldNum);
                IntPtr grossQtyPtr = Marshal.AllocHGlobal((int)size);

                byte devStatus;

                rc = LCP02API.LCP02GetField(device, fieldNum, grossQtyPtr, out devStatus,
                                            (byte)LCP02API.LCR_RETURN_CODES.LCRM_NO_WAIT);

                if (rc != (byte)LCPAPI.LCPR.LCPR_OK)
                {
                    return grossQtyStr;
                }

                string strGrossQty = Marshal.PtrToStringAnsi(grossQtyPtr, (int)size);
                LCP02API.FloatByteArray fbaGross;
                byte[] strGrossArr = null;

                fbaGross = new LCP02API.FloatByteArray();
                strGrossArr = Encoding.GetEncoding(1252).GetBytes(strGrossQty);
                fbaGross.Byte1 = strGrossArr[0];
                fbaGross.Byte2 = strGrossArr[1];
                fbaGross.Byte3 = strGrossArr[2];
                fbaGross.Byte4 = strGrossArr[3];

                double grossQty = 0;
                strGrossQty = fbaGross.int32Value.ToString();
                if (strGrossQty != "")
                {
                    grossQty = Convert.ToDouble(strGrossQty);
                    grossQty = grossQty / 10;
                }

                grossQtyStr = grossQty.ToString();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return grossQtyStr;
        }

        private string getGrossTotalizer()
        {
            byte rc;
            string grossTotalizerStr = "PLEASE WAIT..";

            if (device == 0)
            {
                return grossTotalizerStr;
            }

            byte fieldNum = (byte)LCP02API.LCR_FIELD_NUMBERS.LCRF_GrossTotal_WM;
            int size = LCP02API.LCP02FieldSize(fieldNum);
            IntPtr grossTotalizerPtr = Marshal.AllocHGlobal((int)size);

            byte devStatus;

            rc = LCP02API.LCP02GetField(device, fieldNum, grossTotalizerPtr, out devStatus,
                                        (byte)LCP02API.LCR_RETURN_CODES.LCRM_NO_WAIT);

            if (rc != (byte)LCPAPI.LCPR.LCPR_OK)
            {
                return grossTotalizerStr;
            }

            string strGrossTotalizer = Marshal.PtrToStringAnsi(grossTotalizerPtr, (int)size);
            LCP02API.FloatByteArray fbaGross;
            byte[] strGrossArr = null;

            fbaGross = new LCP02API.FloatByteArray();
            strGrossArr = Encoding.GetEncoding(1252).GetBytes(strGrossTotalizer);
            fbaGross.Byte1 = strGrossArr[0];
            fbaGross.Byte2 = strGrossArr[1];
            fbaGross.Byte3 = strGrossArr[2];
            fbaGross.Byte4 = strGrossArr[3];

            double grossQty = 0;
            strGrossTotalizer = fbaGross.int32Value.ToString();
            if (strGrossTotalizer != "")
            {
                grossQty = Convert.ToDouble(strGrossTotalizer);
                grossQty = grossQty / 10;
            }

            grossTotalizerStr = grossQty.ToString();

            return grossTotalizerStr;
        }

        private bool getData()
        {
            txtGrossQty.Text = getGrossQty();
            txtGrossTotalizer.Text = getGrossTotalizer();

            if (txtGrossTotalizer.Text.ToUpper() != "PLEASE WAIT.." && txtTotalizerStart.Text.Trim() == "")
                txtTotalizerStart.Text = txtGrossTotalizer.Text;

            string allText = txtGrossQty.Text + txtGrossTotalizer.Text;
            if (allText.Contains("PLEASE WAIT.."))
            {
                doCloseLCR();
                Thread.Sleep(500);
                doOpenLCR();
                return false;
            }
            return true;
        }

        private void stopTransaction()
        {
            txtRefStop.Text = DateTime.Now.ToLongTimeString();
            stepTimerClose = 0;

            lblStop.Text = "STOPING..";

            timerOpen.Enabled = false;
            timerQty.Enabled = false;
            timerClose.Enabled = true;
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            try
            {
                txtRefSTart.Text = DateTime.Now.ToLongTimeString();
                stepTimerOpen = 0;

                lblConnect.Text = "CONNECTING..";

                timerQty.Enabled = false;
                timerClose.Enabled = false;
                timerOpen.Enabled = true;

                saveData();
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            stopTransaction();
        }

        private void timerOpen_Tick(object sender, EventArgs e)
        {
            if (stepTimerOpen == 2)
            {
                if (start())
                {
                    doStartLCR();
                    timerOpen.Enabled = false;
                    timerQty.Enabled = true;
                    btnStop.Enabled = true;
                    lblConnect.Text = "";
                }
            }

            if (stepTimerOpen == 1)
            {
                if (open())
                {
                    btnStart.Enabled = false;
                    doOpenLCR();
                    stepTimerOpen = 2;
                }
            }
            if (stepTimerOpen == 0)
            {
                if (getData())
                {
                    txtTotalizerStart.Text = getGrossTotalizer();
                    stepTimerOpen = 1;
                }
            }
        }

        private void timerQty_Tick(object sender, EventArgs e)
        {
            getData();
            txtGrossQty.Visible = !txtGrossQty.Visible;
            if (txtReceived.Text.Contains("disconnected"))
            {
                stopTransaction();
            }
        }

        private void timerClose_Tick(object sender, EventArgs e)
        {
            if (stepTimerClose == 2)
            {
                if (closed())
                {
                    doCloseLCR();
                    timerOpen.Enabled = false;
                    timerQty.Enabled = false;
                    btnStart.Enabled = true;
                    lblStop.Text = "";
                }
            }

            if (stepTimerClose == 1)
            {
                if (print())
                {
                    btnStop.Enabled = false;
                    doPrintLCR();
                    stepTimerClose = 2;
                }
            }

            if (stepTimerClose == 0)
            {
                if (getData())
                {
                    txtGrossQty.Visible = true;
                    stepTimerClose = 1;
                }
            }
        }

        void saveData()
        {
            var _item = GlobalModel.logsheet_detail;
            var _lcrData = GlobalModel.lcrData;

            _item.qty = Convert.ToDouble(txtGrossQty.Text.Trim());// isNumeric(txtGrossQTY.Text.Replace(".",",")) ? Convert.ToDouble(txtGrossQTY.Text) : 0;
            _item.flow_meter_end = Convert.ToDouble(txtGrossTotalizer.Text.Trim()); //isNumeric(txtGrossTotalizer.Text.Replace(".", ",")) ? Convert.ToDouble(txtGrossTotalizer.Text) : 0;
            _item.flow_meter_start = Convert.ToDouble(txtTotalizerStart.Text.Trim()); // isNumeric(txtTotalizerStart.Text.Replace(".", ",")) ? Convert.ToDouble(txtTotalizerStart.Text) : 0;
            _item.ref_hour_start = txtRefSTart.Text;
            _item.ref_hour_stop = txtRefStop.Text;

            SQLiteConnection conn;

            conn = new SQLiteConnection(myConnectionString);
            if (conn.State != ConnectionState.Open) conn.Open();
            string query = string.Empty;
            int recs = 0;
            query = "INSERT INTO tbl_t_log_sheet_detail(job_row_id,dstrct_code,log_sheet_code,input_type,issued_date,whouse_id,unit_no,max_tank_capacity,hm_before,hm,flw_meter,meter_faktor,qty_loqsheet,qty,shift,fuel_oil_type,stat_type,nrp_operator,nama_operator,work_area,location,ref_condition,ref_hour_start,ref_hour_stop,note,timezone,flag_loading,loadingerror,loadingdatetime,mod_by,mod_date,is_load_to_fosto,foto,flow_meter_start,flow_meter_end,resp_code,resp_name,text_header,text_sub_header,text_body,syncs)"
                  + " VALUES(@job_row_id,@dstrct_code,@log_sheet_code,@input_type,@issued_date,@whouse_id,@unit_no,@max_tank_capacity,@hm_before,@hm,@flw_meter,@meter_faktor,@qty_loqsheet,@qty,@shift,@fuel_oil_type,@stat_type,@nrp_operator,@nama_operator,@work_area,@location,@ref_condition,@ref_hour_start,@ref_hour_stop,@note,@timezone,@flag_loading,@loadingerror,@loadingdatetime,@mod_by,@mod_date,@is_load_to_fosto,@foto,@flow_meter_start,@flow_meter_end,@resp_code,@resp_name,@text_header,@text_sub_header,@text_body,@syncs)";

            SQLiteCommand cmd = new SQLiteCommand(query, conn);

            cmd.Parameters.Add(new SQLiteParameter("@job_row_id", _item.job_row_id));
            cmd.Parameters.Add(new SQLiteParameter("@dstrct_code", _item.dstrct_code));
            cmd.Parameters.Add(new SQLiteParameter("@log_sheet_code", _item.log_sheet_code));
            cmd.Parameters.Add(new SQLiteParameter("@input_type", _item.input_type));
            cmd.Parameters.Add(new SQLiteParameter("@issued_date", _item.issued_date));
            cmd.Parameters.Add(new SQLiteParameter("@whouse_id", _item.whouse_id));
            cmd.Parameters.Add(new SQLiteParameter("@unit_no", _item.unit_no));
            cmd.Parameters.Add(new SQLiteParameter("@max_tank_capacity", _item.max_tank_capacity));
            cmd.Parameters.Add(new SQLiteParameter("@hm_before", _item.hm_before));
            cmd.Parameters.Add(new SQLiteParameter("@hm", _item.hm));
            cmd.Parameters.Add(new SQLiteParameter("@flw_meter", _item.flw_meter));
            cmd.Parameters.Add(new SQLiteParameter("@meter_faktor", _item.meter_faktor));
            cmd.Parameters.Add(new SQLiteParameter("@qty_loqsheet", _item.qty_loqsheet));
            cmd.Parameters.Add(new SQLiteParameter("@qty", _item.qty));
            cmd.Parameters.Add(new SQLiteParameter("@shift", _item.shift));
            cmd.Parameters.Add(new SQLiteParameter("@fuel_oil_type", _item.fuel_oil_type));
            cmd.Parameters.Add(new SQLiteParameter("@stat_type", _item.stat_type));
            cmd.Parameters.Add(new SQLiteParameter("@nrp_operator", _item.nrp_operator));
            cmd.Parameters.Add(new SQLiteParameter("@nama_operator", _item.nama_operator));
            cmd.Parameters.Add(new SQLiteParameter("@work_area", _item.work_area));
            cmd.Parameters.Add(new SQLiteParameter("@location", _item.location));
            cmd.Parameters.Add(new SQLiteParameter("@ref_condition", _item.ref_condition));
            cmd.Parameters.Add(new SQLiteParameter("@ref_hour_start", _item.ref_hour_start));
            cmd.Parameters.Add(new SQLiteParameter("@ref_hour_stop", _item.ref_hour_stop));
            cmd.Parameters.Add(new SQLiteParameter("@note", _item.note));
            cmd.Parameters.Add(new SQLiteParameter("@timezone", _item.timezone));
            cmd.Parameters.Add(new SQLiteParameter("@flag_loading", _item.flag_loading));
            cmd.Parameters.Add(new SQLiteParameter("@loadingerror", _item.loadingerror));
            cmd.Parameters.Add(new SQLiteParameter("@loadingdatetime", _item.loadingdatetime));
            cmd.Parameters.Add(new SQLiteParameter("@mod_by", _item.mod_by));
            cmd.Parameters.Add(new SQLiteParameter("@mod_date", _item.mod_date));
            cmd.Parameters.Add(new SQLiteParameter("@is_load_to_fosto", _item.is_load_to_fosto));
            cmd.Parameters.Add(new SQLiteParameter("@foto", _item.foto));
            cmd.Parameters.Add(new SQLiteParameter("@flow_meter_start", _item.flow_meter_start));
            cmd.Parameters.Add(new SQLiteParameter("@flow_meter_end", _item.flow_meter_end));
            cmd.Parameters.Add(new SQLiteParameter("@resp_code", _item.resp_code));
            cmd.Parameters.Add(new SQLiteParameter("@resp_name", _item.resp_name));
            cmd.Parameters.Add(new SQLiteParameter("@text_header", _item.text_header));
            cmd.Parameters.Add(new SQLiteParameter("@text_sub_header", _item.text_sub_header));
            cmd.Parameters.Add(new SQLiteParameter("@text_body", _item.text_body));
            cmd.Parameters.Add(new SQLiteParameter("@syncs", _item.syncs));

            recs = cmd.ExecuteNonQuery();

            if (recs > 0)
            {
                if (Application.OpenForms["FTransaction"] != null)
                {
                    (Application.OpenForms["FTransaction"] as FTransaction).clearData();
                }
                doPrintLCR();
                doCloseLCR();
                this.Close();
            }
            if (conn.State == ConnectionState.Open) conn.Close();
        }
    }
}
