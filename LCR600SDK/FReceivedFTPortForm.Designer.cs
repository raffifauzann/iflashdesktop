﻿namespace LCR600SDK
{
    partial class FReceivedFTPortForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FReceivedFTPortForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtSnFlowMeter = new System.Windows.Forms.TextBox();
            this.chkConfirm = new System.Windows.Forms.CheckBox();
            this.txtReceiveQtyAdditive = new System.Windows.Forms.TextBox();
            this.txtReceiveTemperature = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.txtReceiveDensity = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtReceiveQty1 = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtReceiveFlowMeterAkhir1 = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtReceiveFlowMeterAwal1 = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.txtReceiveDippingHole2Mt = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.txtReceiveDippingHole1Mt = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.txtSendFrom = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtSendQty = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtSendDate = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtReceiveDriverName = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtSendDriverName = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtFt = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtDo = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtTranportir = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtDistrik = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPoNo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.ckSegel10 = new System.Windows.Forms.CheckBox();
            this.ckSegel9 = new System.Windows.Forms.CheckBox();
            this.ckSegel8 = new System.Windows.Forms.CheckBox();
            this.ckSegel7 = new System.Windows.Forms.CheckBox();
            this.ckSegel6 = new System.Windows.Forms.CheckBox();
            this.ckSegel5 = new System.Windows.Forms.CheckBox();
            this.ckSegel4 = new System.Windows.Forms.CheckBox();
            this.ckSegel3 = new System.Windows.Forms.CheckBox();
            this.ckSegel2 = new System.Windows.Forms.CheckBox();
            this.ckSegel1 = new System.Windows.Forms.CheckBox();
            this.txtSegel10 = new System.Windows.Forms.TextBox();
            this.txtSegel9 = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.txtSegel8 = new System.Windows.Forms.TextBox();
            this.txtSegel7 = new System.Windows.Forms.TextBox();
            this.txtSegel6 = new System.Windows.Forms.TextBox();
            this.txtSegel5 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.txtSegel4 = new System.Windows.Forms.TextBox();
            this.txtSegel3 = new System.Windows.Forms.TextBox();
            this.txtSegel2 = new System.Windows.Forms.TextBox();
            this.txtSegel1 = new System.Windows.Forms.TextBox();
            this.txtReceiveAt = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.txtSendfmAkhir = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtSendfmAwal = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnTerima = new System.Windows.Forms.Button();
            this.panelHide = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panelHide.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Location = new System.Drawing.Point(12, 13);
            this.panel1.Margin = new System.Windows.Forms.Padding(1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(952, 500);
            this.panel1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.txtSnFlowMeter);
            this.panel2.Controls.Add(this.chkConfirm);
            this.panel2.Controls.Add(this.txtReceiveQtyAdditive);
            this.panel2.Controls.Add(this.txtReceiveTemperature);
            this.panel2.Controls.Add(this.label16);
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.txtReceiveDensity);
            this.panel2.Controls.Add(this.label18);
            this.panel2.Controls.Add(this.txtReceiveQty1);
            this.panel2.Controls.Add(this.label23);
            this.panel2.Controls.Add(this.txtReceiveFlowMeterAkhir1);
            this.panel2.Controls.Add(this.label24);
            this.panel2.Controls.Add(this.txtReceiveFlowMeterAwal1);
            this.panel2.Controls.Add(this.label25);
            this.panel2.Controls.Add(this.label26);
            this.panel2.Controls.Add(this.txtReceiveDippingHole2Mt);
            this.panel2.Controls.Add(this.label27);
            this.panel2.Controls.Add(this.txtReceiveDippingHole1Mt);
            this.panel2.Controls.Add(this.label28);
            this.panel2.Controls.Add(this.txtSendFrom);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.txtSendQty);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.txtSendDate);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.txtReceiveDriverName);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.txtSendDriverName);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.txtFt);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.txtDo);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.txtTranportir);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.txtDistrik);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.txtPoNo);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Location = new System.Drawing.Point(1, 1);
            this.panel2.Margin = new System.Windows.Forms.Padding(1);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(946, 491);
            this.panel2.TabIndex = 0;
            // 
            // txtSnFlowMeter
            // 
            this.txtSnFlowMeter.Enabled = false;
            this.txtSnFlowMeter.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.txtSnFlowMeter.Location = new System.Drawing.Point(200, 242);
            this.txtSnFlowMeter.Margin = new System.Windows.Forms.Padding(1);
            this.txtSnFlowMeter.Name = "txtSnFlowMeter";
            this.txtSnFlowMeter.Size = new System.Drawing.Size(212, 35);
            this.txtSnFlowMeter.TabIndex = 86;
            this.txtSnFlowMeter.Text = "0";
            // 
            // chkConfirm
            // 
            this.chkConfirm.AutoSize = true;
            this.chkConfirm.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.chkConfirm.Location = new System.Drawing.Point(3, 431);
            this.chkConfirm.Name = "chkConfirm";
            this.chkConfirm.Size = new System.Drawing.Size(423, 33);
            this.chkConfirm.TabIndex = 55;
            this.chkConfirm.Text = "Ritasi pengiriman terakhir PO / kapal";
            this.chkConfirm.UseVisualStyleBackColor = true;
            // 
            // txtReceiveQtyAdditive
            // 
            this.txtReceiveQtyAdditive.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.txtReceiveQtyAdditive.Location = new System.Drawing.Point(707, 399);
            this.txtReceiveQtyAdditive.Margin = new System.Windows.Forms.Padding(1);
            this.txtReceiveQtyAdditive.Name = "txtReceiveQtyAdditive";
            this.txtReceiveQtyAdditive.Size = new System.Drawing.Size(224, 35);
            this.txtReceiveQtyAdditive.TabIndex = 54;
            this.txtReceiveQtyAdditive.Text = "0";
            // 
            // txtReceiveTemperature
            // 
            this.txtReceiveTemperature.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.txtReceiveTemperature.Location = new System.Drawing.Point(707, 357);
            this.txtReceiveTemperature.Margin = new System.Windows.Forms.Padding(1);
            this.txtReceiveTemperature.Name = "txtReceiveTemperature";
            this.txtReceiveTemperature.Size = new System.Drawing.Size(224, 35);
            this.txtReceiveTemperature.TabIndex = 52;
            this.txtReceiveTemperature.Text = "0";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.label16.Location = new System.Drawing.Point(478, 399);
            this.label16.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(210, 29);
            this.label16.TabIndex = 51;
            this.label16.Text = "Qty Additive (Liter)";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.label17.Location = new System.Drawing.Point(433, 357);
            this.label17.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(255, 29);
            this.label17.TabIndex = 50;
            this.label17.Text = "Temperature (Celcius)";
            // 
            // txtReceiveDensity
            // 
            this.txtReceiveDensity.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.txtReceiveDensity.Location = new System.Drawing.Point(707, 318);
            this.txtReceiveDensity.Margin = new System.Windows.Forms.Padding(1);
            this.txtReceiveDensity.Name = "txtReceiveDensity";
            this.txtReceiveDensity.Size = new System.Drawing.Size(224, 35);
            this.txtReceiveDensity.TabIndex = 49;
            this.txtReceiveDensity.Text = "0";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.label18.Location = new System.Drawing.Point(467, 316);
            this.label18.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(216, 29);
            this.label18.TabIndex = 48;
            this.label18.Text = "Density (0.8..... g/L)";
            // 
            // txtReceiveQty1
            // 
            this.txtReceiveQty1.Enabled = false;
            this.txtReceiveQty1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.txtReceiveQty1.Location = new System.Drawing.Point(224, 391);
            this.txtReceiveQty1.Margin = new System.Windows.Forms.Padding(1);
            this.txtReceiveQty1.Name = "txtReceiveQty1";
            this.txtReceiveQty1.Size = new System.Drawing.Size(188, 35);
            this.txtReceiveQty1.TabIndex = 39;
            this.txtReceiveQty1.Text = "0";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.label23.Location = new System.Drawing.Point(11, 399);
            this.label23.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(176, 29);
            this.label23.TabIndex = 38;
            this.label23.Text = "Qty Received 1";
            // 
            // txtReceiveFlowMeterAkhir1
            // 
            this.txtReceiveFlowMeterAkhir1.Enabled = false;
            this.txtReceiveFlowMeterAkhir1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.txtReceiveFlowMeterAkhir1.Location = new System.Drawing.Point(224, 354);
            this.txtReceiveFlowMeterAkhir1.Margin = new System.Windows.Forms.Padding(1);
            this.txtReceiveFlowMeterAkhir1.Name = "txtReceiveFlowMeterAkhir1";
            this.txtReceiveFlowMeterAkhir1.Size = new System.Drawing.Size(188, 35);
            this.txtReceiveFlowMeterAkhir1.TabIndex = 37;
            this.txtReceiveFlowMeterAkhir1.Text = "0";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.label24.Location = new System.Drawing.Point(11, 357);
            this.label24.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(213, 29);
            this.label24.TabIndex = 36;
            this.label24.Text = "Flow Meter Akhir 1";
            // 
            // txtReceiveFlowMeterAwal1
            // 
            this.txtReceiveFlowMeterAwal1.Enabled = false;
            this.txtReceiveFlowMeterAwal1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.txtReceiveFlowMeterAwal1.Location = new System.Drawing.Point(224, 316);
            this.txtReceiveFlowMeterAwal1.Margin = new System.Windows.Forms.Padding(1);
            this.txtReceiveFlowMeterAwal1.Name = "txtReceiveFlowMeterAwal1";
            this.txtReceiveFlowMeterAwal1.Size = new System.Drawing.Size(188, 35);
            this.txtReceiveFlowMeterAwal1.TabIndex = 35;
            this.txtReceiveFlowMeterAwal1.Text = "0";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.label25.Location = new System.Drawing.Point(11, 317);
            this.label25.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(211, 29);
            this.label25.TabIndex = 34;
            this.label25.Text = "Flow Meter Awal 1";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.label26.Location = new System.Drawing.Point(11, 245);
            this.label26.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(193, 29);
            this.label26.TabIndex = 32;
            this.label26.Text = "SN Flow Meter 1";
            // 
            // txtReceiveDippingHole2Mt
            // 
            this.txtReceiveDippingHole2Mt.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.txtReceiveDippingHole2Mt.Location = new System.Drawing.Point(707, 171);
            this.txtReceiveDippingHole2Mt.Margin = new System.Windows.Forms.Padding(1);
            this.txtReceiveDippingHole2Mt.Name = "txtReceiveDippingHole2Mt";
            this.txtReceiveDippingHole2Mt.Size = new System.Drawing.Size(224, 35);
            this.txtReceiveDippingHole2Mt.TabIndex = 31;
            this.txtReceiveDippingHole2Mt.Text = "0";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.label27.Location = new System.Drawing.Point(421, 164);
            this.label27.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(277, 29);
            this.label27.TabIndex = 30;
            this.label27.Text = "Dipping Hole 2 MT (mm)";
            // 
            // txtReceiveDippingHole1Mt
            // 
            this.txtReceiveDippingHole1Mt.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.txtReceiveDippingHole1Mt.Location = new System.Drawing.Point(707, 211);
            this.txtReceiveDippingHole1Mt.Margin = new System.Windows.Forms.Padding(1);
            this.txtReceiveDippingHole1Mt.Name = "txtReceiveDippingHole1Mt";
            this.txtReceiveDippingHole1Mt.Size = new System.Drawing.Size(224, 35);
            this.txtReceiveDippingHole1Mt.TabIndex = 29;
            this.txtReceiveDippingHole1Mt.Text = "0";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.label28.Location = new System.Drawing.Point(421, 204);
            this.label28.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(277, 29);
            this.label28.TabIndex = 28;
            this.label28.Text = "Dipping Hole 1 MT (mm)";
            // 
            // txtSendFrom
            // 
            this.txtSendFrom.Enabled = false;
            this.txtSendFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.txtSendFrom.Location = new System.Drawing.Point(200, 201);
            this.txtSendFrom.Margin = new System.Windows.Forms.Padding(1);
            this.txtSendFrom.Name = "txtSendFrom";
            this.txtSendFrom.Size = new System.Drawing.Size(212, 35);
            this.txtSendFrom.TabIndex = 19;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.label6.Location = new System.Drawing.Point(11, 205);
            this.label6.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(138, 29);
            this.label6.TabIndex = 18;
            this.label6.Text = "Dikirim Dari";
            // 
            // txtSendQty
            // 
            this.txtSendQty.Enabled = false;
            this.txtSendQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.txtSendQty.Location = new System.Drawing.Point(200, 164);
            this.txtSendQty.Margin = new System.Windows.Forms.Padding(1);
            this.txtSendQty.Name = "txtSendQty";
            this.txtSendQty.Size = new System.Drawing.Size(212, 35);
            this.txtSendQty.TabIndex = 17;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.label7.Location = new System.Drawing.Point(11, 164);
            this.label7.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 29);
            this.label7.TabIndex = 16;
            this.label7.Text = "Qty";
            // 
            // txtSendDate
            // 
            this.txtSendDate.Enabled = false;
            this.txtSendDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.txtSendDate.Location = new System.Drawing.Point(200, 125);
            this.txtSendDate.Margin = new System.Windows.Forms.Padding(1);
            this.txtSendDate.Name = "txtSendDate";
            this.txtSendDate.Size = new System.Drawing.Size(212, 35);
            this.txtSendDate.TabIndex = 15;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.label8.Location = new System.Drawing.Point(11, 129);
            this.label8.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(184, 29);
            this.label8.TabIndex = 14;
            this.label8.Text = "Berangkat Pada";
            // 
            // txtReceiveDriverName
            // 
            this.txtReceiveDriverName.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.txtReceiveDriverName.Location = new System.Drawing.Point(707, 133);
            this.txtReceiveDriverName.Margin = new System.Windows.Forms.Padding(1);
            this.txtReceiveDriverName.Name = "txtReceiveDriverName";
            this.txtReceiveDriverName.Size = new System.Drawing.Size(224, 35);
            this.txtReceiveDriverName.TabIndex = 13;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.label9.Location = new System.Drawing.Point(421, 126);
            this.label9.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(283, 29);
            this.label9.TabIndex = 12;
            this.label9.Text = "Nama Driver Penerimaan";
            // 
            // txtSendDriverName
            // 
            this.txtSendDriverName.Enabled = false;
            this.txtSendDriverName.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.txtSendDriverName.Location = new System.Drawing.Point(707, 95);
            this.txtSendDriverName.Margin = new System.Windows.Forms.Padding(1);
            this.txtSendDriverName.Name = "txtSendDriverName";
            this.txtSendDriverName.Size = new System.Drawing.Size(224, 35);
            this.txtSendDriverName.TabIndex = 11;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.label10.Location = new System.Drawing.Point(421, 90);
            this.label10.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(276, 29);
            this.label10.TabIndex = 10;
            this.label10.Text = "Nama Driver Pengiriman";
            // 
            // txtFt
            // 
            this.txtFt.Enabled = false;
            this.txtFt.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.txtFt.Location = new System.Drawing.Point(707, 55);
            this.txtFt.Margin = new System.Windows.Forms.Padding(1);
            this.txtFt.Name = "txtFt";
            this.txtFt.Size = new System.Drawing.Size(224, 35);
            this.txtFt.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.label5.Location = new System.Drawing.Point(421, 52);
            this.label5.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(208, 29);
            this.label5.TabIndex = 8;
            this.label5.Text = "Nomer Fuel Truck";
            // 
            // txtDo
            // 
            this.txtDo.Enabled = false;
            this.txtDo.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.txtDo.Location = new System.Drawing.Point(707, 16);
            this.txtDo.Margin = new System.Windows.Forms.Padding(1);
            this.txtDo.Name = "txtDo";
            this.txtDo.Size = new System.Drawing.Size(224, 35);
            this.txtDo.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.label4.Location = new System.Drawing.Point(421, 16);
            this.label4.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(212, 29);
            this.label4.TabIndex = 6;
            this.label4.Text = "Nomer Surat Jalan";
            // 
            // txtTranportir
            // 
            this.txtTranportir.Enabled = false;
            this.txtTranportir.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.txtTranportir.Location = new System.Drawing.Point(200, 87);
            this.txtTranportir.Margin = new System.Windows.Forms.Padding(1);
            this.txtTranportir.Name = "txtTranportir";
            this.txtTranportir.Size = new System.Drawing.Size(212, 35);
            this.txtTranportir.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.label3.Location = new System.Drawing.Point(11, 95);
            this.label3.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(131, 29);
            this.label3.TabIndex = 4;
            this.label3.Text = "Transportir";
            // 
            // txtDistrik
            // 
            this.txtDistrik.Enabled = false;
            this.txtDistrik.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.txtDistrik.Location = new System.Drawing.Point(200, 50);
            this.txtDistrik.Margin = new System.Windows.Forms.Padding(1);
            this.txtDistrik.Name = "txtDistrik";
            this.txtDistrik.Size = new System.Drawing.Size(212, 35);
            this.txtDistrik.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.label2.Location = new System.Drawing.Point(11, 55);
            this.label2.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 29);
            this.label2.TabIndex = 2;
            this.label2.Text = "Distrik";
            // 
            // txtPoNo
            // 
            this.txtPoNo.Enabled = false;
            this.txtPoNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.txtPoNo.Location = new System.Drawing.Point(200, 13);
            this.txtPoNo.Margin = new System.Windows.Forms.Padding(1);
            this.txtPoNo.Name = "txtPoNo";
            this.txtPoNo.Size = new System.Drawing.Size(212, 35);
            this.txtPoNo.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.label1.Location = new System.Drawing.Point(11, 15);
            this.label1.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(128, 29);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nomer PO";
            // 
            // btnSubmit
            // 
            this.btnSubmit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSubmit.BackColor = System.Drawing.Color.Green;
            this.btnSubmit.FlatAppearance.BorderSize = 0;
            this.btnSubmit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSubmit.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.btnSubmit.ForeColor = System.Drawing.Color.White;
            this.btnSubmit.Location = new System.Drawing.Point(856, 522);
            this.btnSubmit.Margin = new System.Windows.Forms.Padding(2);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(112, 45);
            this.btnSubmit.TabIndex = 3;
            this.btnSubmit.Text = "SUBMIT";
            this.btnSubmit.UseVisualStyleBackColor = false;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // ckSegel10
            // 
            this.ckSegel10.AutoSize = true;
            this.ckSegel10.Location = new System.Drawing.Point(311, 247);
            this.ckSegel10.Name = "ckSegel10";
            this.ckSegel10.Size = new System.Drawing.Size(15, 14);
            this.ckSegel10.TabIndex = 85;
            this.ckSegel10.UseVisualStyleBackColor = true;
            // 
            // ckSegel9
            // 
            this.ckSegel9.AutoSize = true;
            this.ckSegel9.Location = new System.Drawing.Point(311, 222);
            this.ckSegel9.Name = "ckSegel9";
            this.ckSegel9.Size = new System.Drawing.Size(15, 14);
            this.ckSegel9.TabIndex = 84;
            this.ckSegel9.UseVisualStyleBackColor = true;
            // 
            // ckSegel8
            // 
            this.ckSegel8.AutoSize = true;
            this.ckSegel8.Location = new System.Drawing.Point(311, 194);
            this.ckSegel8.Name = "ckSegel8";
            this.ckSegel8.Size = new System.Drawing.Size(15, 14);
            this.ckSegel8.TabIndex = 83;
            this.ckSegel8.UseVisualStyleBackColor = true;
            // 
            // ckSegel7
            // 
            this.ckSegel7.AutoSize = true;
            this.ckSegel7.Location = new System.Drawing.Point(311, 169);
            this.ckSegel7.Name = "ckSegel7";
            this.ckSegel7.Size = new System.Drawing.Size(15, 14);
            this.ckSegel7.TabIndex = 82;
            this.ckSegel7.UseVisualStyleBackColor = true;
            // 
            // ckSegel6
            // 
            this.ckSegel6.AutoSize = true;
            this.ckSegel6.Location = new System.Drawing.Point(311, 144);
            this.ckSegel6.Name = "ckSegel6";
            this.ckSegel6.Size = new System.Drawing.Size(15, 14);
            this.ckSegel6.TabIndex = 81;
            this.ckSegel6.UseVisualStyleBackColor = true;
            // 
            // ckSegel5
            // 
            this.ckSegel5.AutoSize = true;
            this.ckSegel5.Location = new System.Drawing.Point(311, 119);
            this.ckSegel5.Name = "ckSegel5";
            this.ckSegel5.Size = new System.Drawing.Size(15, 14);
            this.ckSegel5.TabIndex = 80;
            this.ckSegel5.UseVisualStyleBackColor = true;
            // 
            // ckSegel4
            // 
            this.ckSegel4.AutoSize = true;
            this.ckSegel4.Location = new System.Drawing.Point(311, 93);
            this.ckSegel4.Name = "ckSegel4";
            this.ckSegel4.Size = new System.Drawing.Size(15, 14);
            this.ckSegel4.TabIndex = 79;
            this.ckSegel4.UseVisualStyleBackColor = true;
            // 
            // ckSegel3
            // 
            this.ckSegel3.AutoSize = true;
            this.ckSegel3.Location = new System.Drawing.Point(311, 69);
            this.ckSegel3.Name = "ckSegel3";
            this.ckSegel3.Size = new System.Drawing.Size(15, 14);
            this.ckSegel3.TabIndex = 78;
            this.ckSegel3.UseVisualStyleBackColor = true;
            // 
            // ckSegel2
            // 
            this.ckSegel2.AutoSize = true;
            this.ckSegel2.Location = new System.Drawing.Point(311, 42);
            this.ckSegel2.Name = "ckSegel2";
            this.ckSegel2.Size = new System.Drawing.Size(15, 14);
            this.ckSegel2.TabIndex = 77;
            this.ckSegel2.UseVisualStyleBackColor = true;
            // 
            // ckSegel1
            // 
            this.ckSegel1.AutoSize = true;
            this.ckSegel1.Location = new System.Drawing.Point(311, 16);
            this.ckSegel1.Name = "ckSegel1";
            this.ckSegel1.Size = new System.Drawing.Size(15, 14);
            this.ckSegel1.TabIndex = 76;
            this.ckSegel1.UseVisualStyleBackColor = true;
            // 
            // txtSegel10
            // 
            this.txtSegel10.Enabled = false;
            this.txtSegel10.Location = new System.Drawing.Point(143, 244);
            this.txtSegel10.Margin = new System.Windows.Forms.Padding(1);
            this.txtSegel10.Name = "txtSegel10";
            this.txtSegel10.Size = new System.Drawing.Size(154, 20);
            this.txtSegel10.TabIndex = 75;
            // 
            // txtSegel9
            // 
            this.txtSegel9.Enabled = false;
            this.txtSegel9.Location = new System.Drawing.Point(143, 220);
            this.txtSegel9.Margin = new System.Windows.Forms.Padding(1);
            this.txtSegel9.Name = "txtSegel9";
            this.txtSegel9.Size = new System.Drawing.Size(154, 20);
            this.txtSegel9.TabIndex = 74;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(11, 247);
            this.label32.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(49, 13);
            this.label32.TabIndex = 73;
            this.label32.Text = "Segel 10";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(11, 222);
            this.label33.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(43, 13);
            this.label33.TabIndex = 72;
            this.label33.Text = "Segel 9";
            // 
            // txtSegel8
            // 
            this.txtSegel8.Enabled = false;
            this.txtSegel8.Location = new System.Drawing.Point(143, 192);
            this.txtSegel8.Margin = new System.Windows.Forms.Padding(1);
            this.txtSegel8.Name = "txtSegel8";
            this.txtSegel8.Size = new System.Drawing.Size(154, 20);
            this.txtSegel8.TabIndex = 71;
            // 
            // txtSegel7
            // 
            this.txtSegel7.Enabled = false;
            this.txtSegel7.Location = new System.Drawing.Point(143, 168);
            this.txtSegel7.Margin = new System.Windows.Forms.Padding(1);
            this.txtSegel7.Name = "txtSegel7";
            this.txtSegel7.Size = new System.Drawing.Size(154, 20);
            this.txtSegel7.TabIndex = 70;
            // 
            // txtSegel6
            // 
            this.txtSegel6.Enabled = false;
            this.txtSegel6.Location = new System.Drawing.Point(143, 141);
            this.txtSegel6.Margin = new System.Windows.Forms.Padding(1);
            this.txtSegel6.Name = "txtSegel6";
            this.txtSegel6.Size = new System.Drawing.Size(154, 20);
            this.txtSegel6.TabIndex = 69;
            // 
            // txtSegel5
            // 
            this.txtSegel5.Enabled = false;
            this.txtSegel5.Location = new System.Drawing.Point(143, 116);
            this.txtSegel5.Margin = new System.Windows.Forms.Padding(1);
            this.txtSegel5.Name = "txtSegel5";
            this.txtSegel5.Size = new System.Drawing.Size(154, 20);
            this.txtSegel5.TabIndex = 68;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(11, 195);
            this.label15.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(43, 13);
            this.label15.TabIndex = 67;
            this.label15.Text = "Segel 8";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(11, 170);
            this.label29.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(43, 13);
            this.label29.TabIndex = 66;
            this.label29.Text = "Segel 7";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(11, 141);
            this.label30.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(43, 13);
            this.label30.TabIndex = 65;
            this.label30.Text = "Segel 6";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(11, 116);
            this.label31.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(43, 13);
            this.label31.TabIndex = 64;
            this.label31.Text = "Segel 5";
            // 
            // txtSegel4
            // 
            this.txtSegel4.Enabled = false;
            this.txtSegel4.Location = new System.Drawing.Point(143, 90);
            this.txtSegel4.Margin = new System.Windows.Forms.Padding(1);
            this.txtSegel4.Name = "txtSegel4";
            this.txtSegel4.Size = new System.Drawing.Size(154, 20);
            this.txtSegel4.TabIndex = 63;
            // 
            // txtSegel3
            // 
            this.txtSegel3.Enabled = false;
            this.txtSegel3.Location = new System.Drawing.Point(143, 66);
            this.txtSegel3.Margin = new System.Windows.Forms.Padding(1);
            this.txtSegel3.Name = "txtSegel3";
            this.txtSegel3.Size = new System.Drawing.Size(154, 20);
            this.txtSegel3.TabIndex = 62;
            // 
            // txtSegel2
            // 
            this.txtSegel2.Enabled = false;
            this.txtSegel2.Location = new System.Drawing.Point(143, 39);
            this.txtSegel2.Margin = new System.Windows.Forms.Padding(1);
            this.txtSegel2.Name = "txtSegel2";
            this.txtSegel2.Size = new System.Drawing.Size(154, 20);
            this.txtSegel2.TabIndex = 61;
            // 
            // txtSegel1
            // 
            this.txtSegel1.Enabled = false;
            this.txtSegel1.Location = new System.Drawing.Point(143, 14);
            this.txtSegel1.Margin = new System.Windows.Forms.Padding(1);
            this.txtSegel1.Name = "txtSegel1";
            this.txtSegel1.Size = new System.Drawing.Size(154, 20);
            this.txtSegel1.TabIndex = 60;
            // 
            // txtReceiveAt
            // 
            this.txtReceiveAt.Enabled = false;
            this.txtReceiveAt.Location = new System.Drawing.Point(143, 269);
            this.txtReceiveAt.Margin = new System.Windows.Forms.Padding(1);
            this.txtReceiveAt.Name = "txtReceiveAt";
            this.txtReceiveAt.Size = new System.Drawing.Size(154, 20);
            this.txtReceiveAt.TabIndex = 59;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(11, 93);
            this.label19.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(43, 13);
            this.label19.TabIndex = 46;
            this.label19.Text = "Segel 4";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(11, 68);
            this.label20.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(43, 13);
            this.label20.TabIndex = 44;
            this.label20.Text = "Segel 3";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(11, 39);
            this.label21.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(43, 13);
            this.label21.TabIndex = 42;
            this.label21.Text = "Segel 2";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(11, 14);
            this.label22.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(43, 13);
            this.label22.TabIndex = 40;
            this.label22.Text = "Segel 1";
            // 
            // txtSendfmAkhir
            // 
            this.txtSendfmAkhir.Enabled = false;
            this.txtSendfmAkhir.Location = new System.Drawing.Point(143, 346);
            this.txtSendfmAkhir.Margin = new System.Windows.Forms.Padding(1);
            this.txtSendfmAkhir.Name = "txtSendfmAkhir";
            this.txtSendfmAkhir.Size = new System.Drawing.Size(154, 20);
            this.txtSendfmAkhir.TabIndex = 27;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(11, 347);
            this.label12.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(86, 13);
            this.label12.TabIndex = 26;
            this.label12.Text = "Flow Meter Akhir";
            // 
            // txtSendfmAwal
            // 
            this.txtSendfmAwal.Enabled = false;
            this.txtSendfmAwal.Location = new System.Drawing.Point(143, 320);
            this.txtSendfmAwal.Margin = new System.Windows.Forms.Padding(1);
            this.txtSendfmAwal.Name = "txtSendfmAwal";
            this.txtSendfmAwal.Size = new System.Drawing.Size(154, 20);
            this.txtSendfmAwal.TabIndex = 25;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(11, 322);
            this.label13.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(85, 13);
            this.label13.TabIndex = 24;
            this.label13.Text = "Flow Meter Awal";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(11, 297);
            this.label14.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(80, 13);
            this.label14.TabIndex = 22;
            this.label14.Text = "SN Flow Meter ";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(11, 269);
            this.label11.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(58, 13);
            this.label11.TabIndex = 20;
            this.label11.Text = "Diterima Di";
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.BackColor = System.Drawing.Color.Red;
            this.btnCancel.FlatAppearance.BorderSize = 0;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.btnCancel.ForeColor = System.Drawing.Color.White;
            this.btnCancel.Location = new System.Drawing.Point(584, 523);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(1);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(124, 45);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "CANCEL";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnTerima
            // 
            this.btnTerima.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTerima.BackColor = System.Drawing.Color.Orange;
            this.btnTerima.FlatAppearance.BorderSize = 0;
            this.btnTerima.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTerima.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.btnTerima.ForeColor = System.Drawing.Color.White;
            this.btnTerima.Location = new System.Drawing.Point(710, 523);
            this.btnTerima.Margin = new System.Windows.Forms.Padding(1);
            this.btnTerima.Name = "btnTerima";
            this.btnTerima.Size = new System.Drawing.Size(143, 45);
            this.btnTerima.TabIndex = 1;
            this.btnTerima.Text = "PROCESS";
            this.btnTerima.UseVisualStyleBackColor = false;
            this.btnTerima.Click += new System.EventHandler(this.btnTerima_Click);
            // 
            // panelHide
            // 
            this.panelHide.Controls.Add(this.label22);
            this.panelHide.Controls.Add(this.txtReceiveAt);
            this.panelHide.Controls.Add(this.label21);
            this.panelHide.Controls.Add(this.ckSegel10);
            this.panelHide.Controls.Add(this.label20);
            this.panelHide.Controls.Add(this.ckSegel9);
            this.panelHide.Controls.Add(this.label19);
            this.panelHide.Controls.Add(this.ckSegel8);
            this.panelHide.Controls.Add(this.txtSegel1);
            this.panelHide.Controls.Add(this.ckSegel7);
            this.panelHide.Controls.Add(this.txtSegel2);
            this.panelHide.Controls.Add(this.ckSegel6);
            this.panelHide.Controls.Add(this.txtSegel3);
            this.panelHide.Controls.Add(this.ckSegel5);
            this.panelHide.Controls.Add(this.txtSegel4);
            this.panelHide.Controls.Add(this.ckSegel4);
            this.panelHide.Controls.Add(this.label31);
            this.panelHide.Controls.Add(this.ckSegel3);
            this.panelHide.Controls.Add(this.label30);
            this.panelHide.Controls.Add(this.ckSegel2);
            this.panelHide.Controls.Add(this.label29);
            this.panelHide.Controls.Add(this.txtSendfmAkhir);
            this.panelHide.Controls.Add(this.ckSegel1);
            this.panelHide.Controls.Add(this.label12);
            this.panelHide.Controls.Add(this.label15);
            this.panelHide.Controls.Add(this.txtSendfmAwal);
            this.panelHide.Controls.Add(this.txtSegel10);
            this.panelHide.Controls.Add(this.label13);
            this.panelHide.Controls.Add(this.txtSegel5);
            this.panelHide.Controls.Add(this.label14);
            this.panelHide.Controls.Add(this.txtSegel9);
            this.panelHide.Controls.Add(this.label11);
            this.panelHide.Controls.Add(this.txtSegel6);
            this.panelHide.Controls.Add(this.label32);
            this.panelHide.Controls.Add(this.txtSegel7);
            this.panelHide.Controls.Add(this.label33);
            this.panelHide.Controls.Add(this.txtSegel8);
            this.panelHide.Location = new System.Drawing.Point(968, 30);
            this.panelHide.Name = "panelHide";
            this.panelHide.Size = new System.Drawing.Size(393, 444);
            this.panelHide.TabIndex = 87;
            // 
            // FReceivedFTPortForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(975, 578);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.panelHide);
            this.Controls.Add(this.btnTerima);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FReceivedFTPortForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form Received FT";
            this.Load += new System.EventHandler(this.FReceivedFTPortForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panelHide.ResumeLayout(false);
            this.panelHide.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnTerima;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox txtSegel10;
        private System.Windows.Forms.TextBox txtSegel9;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox txtSegel8;
        private System.Windows.Forms.TextBox txtSegel7;
        private System.Windows.Forms.TextBox txtSegel6;
        private System.Windows.Forms.TextBox txtSegel5;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox txtSegel4;
        private System.Windows.Forms.TextBox txtSegel3;
        private System.Windows.Forms.TextBox txtSegel2;
        private System.Windows.Forms.TextBox txtSegel1;
        private System.Windows.Forms.TextBox txtReceiveAt;
        private System.Windows.Forms.CheckBox chkConfirm;
        private System.Windows.Forms.TextBox txtReceiveQtyAdditive;
        private System.Windows.Forms.TextBox txtReceiveTemperature;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtReceiveDensity;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtReceiveQty1;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtReceiveFlowMeterAkhir1;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtReceiveFlowMeterAwal1;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtReceiveDippingHole2Mt;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtReceiveDippingHole1Mt;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox txtSendfmAkhir;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtSendfmAwal;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtSendFrom;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtSendQty;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtSendDate;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtReceiveDriverName;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtSendDriverName;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtFt;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtDo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtTranportir;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtDistrik;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtPoNo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox ckSegel10;
        private System.Windows.Forms.CheckBox ckSegel9;
        private System.Windows.Forms.CheckBox ckSegel8;
        private System.Windows.Forms.CheckBox ckSegel7;
        private System.Windows.Forms.CheckBox ckSegel6;
        private System.Windows.Forms.CheckBox ckSegel5;
        private System.Windows.Forms.CheckBox ckSegel4;
        private System.Windows.Forms.CheckBox ckSegel3;
        private System.Windows.Forms.CheckBox ckSegel2;
        private System.Windows.Forms.CheckBox ckSegel1;
        private System.Windows.Forms.TextBox txtSnFlowMeter;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.Panel panelHide;
    }
}