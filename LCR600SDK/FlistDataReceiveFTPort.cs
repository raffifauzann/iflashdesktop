﻿using Flurl.Http;
using MySql.Data.MySqlClient; using System.Data.SQLite; using System.IO; using System.Reflection;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LCR600SDK
{
    public partial class FlistDataReceiveFTPort : Form
    {
        private string myConnectionString = string.Empty;
        private string pUrl_service = string.Empty;
        private int idxCell = -1;
        private string job_rowid = "";
        private string valueHM = "";
        private string valueHMEdited = "";
        private string valueHMBefore = "";
        private bool statusOnline = false;
        PasswordEncrypDecrypt decryptor = new PasswordEncrypDecrypt();
        WriteLogText log = new WriteLogText();
        public FlistDataReceiveFTPort()
        {
            InitializeComponent();
        }      

        private void FlistDataReceiveFTPort_Load(object sender, EventArgs e)
        {
            dateTimePicker1.Value = DateTime.Now;
            dateTimePicker2.Value = DateTime.Now;
            var _proper = Properties.Settings.Default;
            pUrl_service = _proper.url_service;
            statusOnline = _proper.connection == "ONLINE" ? true : false;
            dateTimePicker1.Text = DateTime.Now.ToString("yyyy-MM-dd");
            dateTimePicker2.Text = DateTime.Now.ToString("yyyy-MM-dd");
            //myConnectionString = "Data Source=" + Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "SQLite", "db_iflash.db") + " ;New=False;";
            var passPhrase = "iFlashDesktop";
            var decryp = decryptor.DecryptPassword(_proper.db_pass, passPhrase);
            myConnectionString = "Data Source=" + Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "SQLite", "db_iflash.db") + " ;New=False;";
            loadGridSearch(true, "");
            loadGrid();

        }
        void dataGridView1_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            dataGridView1.BorderStyle = BorderStyle.None;
            dataGridView1.AlternatingRowsDefaultCellStyle.BackColor = Color.FromArgb(238, 239, 249);
            dataGridView1.CellBorderStyle = DataGridViewCellBorderStyle.SingleHorizontal;
            dataGridView1.DefaultCellStyle.SelectionBackColor = Color.SeaGreen;
            dataGridView1.DefaultCellStyle.SelectionForeColor = Color.WhiteSmoke;
            dataGridView1.BackgroundColor = Color.FromArgb(0, 101, 179);
            dataGridView1.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridView1.EnableHeadersVisualStyles = false;
            dataGridView1.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
            dataGridView1.ColumnHeadersDefaultCellStyle.Font = new Font("MS Reference Sans Serif", 20);
            dataGridView1.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(0, 101, 179);
            dataGridView1.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
            if (dataGridView1.Columns.Count > 0)
            {
                dataGridView1.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                dataGridView1.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                dataGridView1.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                dataGridView1.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                dataGridView1.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                dataGridView1.Columns[5].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                dataGridView1.Columns[6].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            }
            
        }
        void loadGridSearch(bool isAll, String sKey)
        {
            try
            {
                String iQuery = querySql();

                SQLiteConnection conn = new SQLiteConnection(myConnectionString);
                SQLiteCommand cmd = new SQLiteCommand(iQuery, conn);

                if (conn.State != ConnectionState.Open) conn.Open();

                DataTable dataTable = new DataTable();
                SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);

                da.Fill(dataTable);

                dataGridView1.DataSource = dataTable;
                dataGridView1.Columns["pid_ritasi_maintank"].Visible = false;
                if (conn.State == ConnectionState.Open)
                    if (conn.State == ConnectionState.Open) conn.Close();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
                MessageBox.Show("SQLiteException: Terjadi kesalahan pada saat search data, mohon periksa log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(ex.Message, "loadGridSearch()");
            }
        }

        public void loadGrid()
        {
            SQLiteConnection conn = new SQLiteConnection(myConnectionString);
            SQLiteCommand cmd = new SQLiteCommand(querySql(), conn);

            if (conn.State != ConnectionState.Open) conn.Open();

            DataTable dataTable = new DataTable();
            SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);

            da.Fill(dataTable);

            dataGridView1.DataSource = dataTable;
            dataGridView1.Columns["pid_ritasi_maintank"].Visible = false;
            if (conn.State == ConnectionState.Open)
                if (conn.State == ConnectionState.Open) conn.Close();
        }

        void btnSyncWait()
        {
            btnSync.Enabled = false;
            btnSync.Visible = false;
            btnPleaseWait.Enabled = false;
            btnPleaseWait.Visible = true;
        }
        void btnSyncEnabled()
        {
            btnSync.Enabled = true;
            btnSync.Visible = true;
            btnPleaseWait.Visible = false;
            btnPleaseWait.Enabled = false;
        }

        private void timerSyncIssuing_Tick_1(object sender, EventArgs e)
        {
            btnSyncWait();
        }
        public async Task<string> checkConnectivity()
        {
            try
            {
                WebClient requestCek = new WebClient();
                var str = requestCek.DownloadString(pUrl_service);
                return str;
            }
            catch (WebException ex)
            {
                return "";
            }
        }
        private async void btnSync_Click(object sender, EventArgs e)
        {
            try
            {
                var checkconnect = await checkConnectivity();
                if (checkconnect.Contains("Supply Service"))
                //if (statusOnline)
                {
                    onSyncAsync();                    
                }
                else
                {
                    var f = new FPopUpNotification("Status saat ini OFFLINE mohon untuk sambungkan jaringan dan ganti setting connection menjadi ONLINE \r\n Setting > User Setting > Connection > Save", "Failed To Connect");
                    f.ShowDialog();
                }

            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message.ToString());
                MessageBox.Show("Exception: Terjadi kesalahan pada saat sync, mohon periksa log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", ex.Message);                
                save_error_log(ex.Message, "btnSync_Click()");
                timerSyncIssuing.Stop();
                btnSyncEnabled();
            }
        }

        async Task onSyncAsync()
        {
            var _proper = Properties.Settings.Default;
            var _auth = GlobalModel.loginModel;

            String _url = $"{pUrl_service}api/FuelService/SyncReceiveRitasiToMaintankDesktop";            
            try
            {
                List<tbl_t_receive_maintank_ritasi> SendListIssuing = new List<tbl_t_receive_maintank_ritasi>();
                var getData = getListReceiveFTPort();
              

                var listFailedPidReceive = new List<List<string>>();
                if (getData.Count() == 0)
                {
                    timerSyncIssuing.Stop();
                    btnSyncEnabled();
                }
                else
                {                    
                    btnPleaseWait.Text = string.Format("Please Wait. {0} data dari {1} data", 0, getData.Count);
                    for (int i = 0; i < getData.Count; i++)
                    {

                        SendListIssuing.Add(getData[i]);
                        
                        //if (SendListIssuing.Count == 1 /*&& int.Parse(counter[0]) != 0*/)
                        if (SendListIssuing.Count == int.Parse(_proper.data_per_kirim) /*&& int.Parse(counter[0]) != 0*/)
                        {
                            var json = Newtonsoft.Json.JsonConvert.SerializeObject(SendListIssuing);
                            ClsRitasiToMaintank data = await _url.WithHeaders(new { Authorization = _auth.token }).PostJsonAsync(SendListIssuing).ReceiveJson<ClsRitasiToMaintank>();
                            if (data.status)
                            {
                                btnPleaseWait.Text = string.Format("Please Wait. {0} data dari {1} data", (i + 1), getData.Count);
                            }
                            else
                            {
                                if(data.failed_pid != null && data.failed_pid.Count > 0)
                                {
                                    listFailedPidReceive.Add(data.failed_pid);
                                    SendListIssuing = new List<tbl_t_receive_maintank_ritasi>();
                                }
                            }
                                                  
                            log.WriteToFile("logLCR600.txt", "Data Remarks: " + data.remarks + " List Remarks: " + String.Join("','", data.list_remarks));
                            save_error_log("Data Remarks: " + data.remarks + " List Remarks: " + String.Join("','", data.list_remarks), "onSyncAsync()");
                            updateTableSync(SendListIssuing, data);
                            loadGrid();
                            SendListIssuing = new List<tbl_t_receive_maintank_ritasi>();
                        }
                        else 
                        {
                            if (i == getData.Count - 1 /*&& SendListIssuing.Count == int.Parse(counter[1])*/)
                            {
                                ClsRitasiToMaintank data = await _url.WithHeaders(new { Authorization = _auth.token }).PostJsonAsync(SendListIssuing).ReceiveJson<ClsRitasiToMaintank>();
                                if (data.status)
                                {
                                    btnPleaseWait.Text = string.Format("Please Wait. {0} data dari {1} data", (i + 1), getData.Count);
                                }
                                else
                                {
                                    if (data.failed_pid!=null && data.failed_pid.Count > 0)
                                    {
                                        listFailedPidReceive.Add(data.failed_pid);
                                        String failed_pids = String.Join(" \r\n- ", listFailedPidReceive);
                                        btnPleaseWait.Text = string.Format("Please Wait. {0} data dari {1} data", (i + 1), getData.Count);
                                    }
                                }
                               
                                log.WriteToFile("logLCR600.txt", "Data Remarks: " +data.remarks +" List Remarks: " + String.Join("','", data.list_remarks));
                                save_error_log("Data Remarks: " + data.remarks + " List Remarks: " + String.Join("','", data.list_remarks), "onSyncAsync()");
                                updateTableSync(SendListIssuing, data);
                                loadGrid();
                            }                              
                        }
                        if (i == getData.Count - 1)
                        {
                            timerSyncIssuing.Stop();
                            btnSyncEnabled();
                        }
                    }
                }
            }
            catch (FlurlHttpException ex)
            {
                var resp = ex.Message;
                if (ex.InnerException != null)
                {
                    resp = ex.InnerException.Message;
                }
                //var resp = await ex.GetResponseStringAsync();
                MessageBox.Show("Terjadi kesalahan saat sync, periksa log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                save_error_log(resp, "onSyncAsync()");
                timerSyncIssuing.Stop();
                btnSyncEnabled();
            }
            catch (Exception ex)
            {                
                //MessageBox.Show(ex.Message);
                MessageBox.Show("Exception: Terjadi kesalahan pada saat sync, mohon periksa log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", ex.Message);                
                save_error_log(ex.Message, "onSyncAsync()");
                timerSyncIssuing.Stop();
                btnSyncEnabled();
            }



        }
        void save_error_log(string data, string actions)
        {
            try
            {
                DateTime localDate = DateTime.Now;
                var _item = new tbl_log();
                var initData = GlobalModel.GlobalVar;

                _item.pid = System.Guid.NewGuid().ToString();
                _item.actions = $"District {initData.DataEmp.DstrctCode}/FListDataReceiveFTPort/{actions}";
                _item.message = $"{data}";
                _item.mod_date = localDate;
                _item.mod_by = initData.DataEmp.Nrp;

                SQLiteConnection conn;

                conn = new SQLiteConnection(myConnectionString);
                if (conn.State != ConnectionState.Open) conn.Open();
                string query = string.Empty;
                query = "INSERT INTO tbl_log(pid,actions,message,mod_date,mod_by)"
                      + " VALUES(@pid,@actions,@message,@mod_date,@mod_by)";

                SQLiteCommand cmd = new SQLiteCommand(query, conn);

                cmd.Parameters.Add(new SQLiteParameter("@pid", _item.pid));
                cmd.Parameters.Add(new SQLiteParameter("@actions", _item.actions));
                cmd.Parameters.Add(new SQLiteParameter("@message", _item.message));
                cmd.Parameters.Add(new SQLiteParameter("@mod_date", _item.mod_date));
                cmd.Parameters.Add(new SQLiteParameter("@mod_by", _item.mod_by));
                cmd.ExecuteNonQuery();
                if (conn.State == ConnectionState.Open) conn.Close();
            }
            catch (Exception)
            {
            }
        }

        void updateTableSync(List<tbl_t_receive_maintank_ritasi> SendListIssuing, ClsRitasiToMaintank sClsRitasiToMaintank)
        {
            SQLiteConnection conn;
            int recs = 0;
            string query = string.Empty;
            conn = new SQLiteConnection(myConnectionString);
            if (conn.State != ConnectionState.Open) conn.Open();

            
            string send_pid = String.Join("','", SendListIssuing.Select(x => x.pid_ritasi_maintank));

            if (sClsRitasiToMaintank.failed_pid != null)
            {
                String failed_pids = String.Join("','", sClsRitasiToMaintank.failed_pid);
                var success_pid = send_pid.Replace(failed_pids, "");
                query = $"update tbl_t_receive_maintank_ritasi set syncs = 9 where pid_ritasi_maintank in('{failed_pids}') and syncs=3 AND send_date between '" + dateTimePicker1.Value.ToString("yyyy-MM-dd 00:00:00") + "' and '" + dateTimePicker2.Value.ToString("yyyy-MM-dd 23:59:59") + "'";
                query += " and doc_status IN('51')  ";
                recs = execCmd(query, conn);

                query = $"update tbl_t_receive_maintank_ritasi set syncs = 7,set receive_iscomplete = 1 ";
                query += " set doc_status = 80 ";
                query+=" where pid_ritasi_maintank in('{success_pid}') and syncs=3 AND send_date between '" + dateTimePicker1.Value.ToString("yyyy-MM-dd 00:00:00") + "' and '" + dateTimePicker2.Value.ToString("yyyy-MM-dd 23:59:59") + "''";
                query += " and doc_status IN('51')  ";
                recs = execCmd(query, conn);
            }
            else
            {
                query = $"update tbl_t_receive_maintank_ritasi set syncs = 7,set receive_iscomplete = 1 ";
                query += " set doc_status = 80 ";
                query += " where pid_ritasi_maintank in('{success_pid}') and syncs=3 AND send_date between '" + dateTimePicker1.Value.ToString("yyyy-MM-dd 00:00:00") + "' and '" + dateTimePicker2.Value.ToString("yyyy-MM-dd 23:59:59") + "''";
                query += " and doc_status IN('51')  ";
                recs = execCmd(query, conn);
            }
            if (conn.State == ConnectionState.Open) conn.Close();

            if (Application.OpenForms["FMain"] != null)
            {
                (Application.OpenForms["FMain"] as FMain).toolStripStatusLabelStatus.Text = "Sync finish";
            }
        }

        int execCmd(String sQuery, SQLiteConnection sConn)
        {
            try
            {
                int iReturn = 0;

                var cmd = new SQLiteCommand(sQuery, sConn);
                iReturn = cmd.ExecuteNonQuery();

                return iReturn;
            }
            catch (Exception e)
            {
                return 0;
            }

        }

        List<tbl_t_receive_maintank_ritasi> getListReceiveFTPort()
        {
            List<tbl_t_receive_maintank_ritasi> _list = new List<tbl_t_receive_maintank_ritasi>();
            SQLiteConnection conn;

            conn = new SQLiteConnection(myConnectionString);
            if (conn.State != ConnectionState.Open) conn.Open();
            string query = string.Empty;

            try
            {

                query = $"update tbl_t_receive_maintank_ritasi set syncs = 3 where syncs in(2,9) AND send_date between '" + dateTimePicker1.Value.ToString("yyyy-MM-dd 00:00:00") + "' and '" + dateTimePicker2.Value.ToString("yyyy-MM-dd 23:59:59") + "'";
                query += " and doc_status IN('51') ";
                if (!string.IsNullOrEmpty(txtSearch.Text))
                {
                    query += " and send_po_no like '%" + txtSearch.Text + "%' ";
                }
                var recs = execCmd(query, conn);
                loadGrid();
                timerSyncIssuing.Start();
            }
            catch (SQLiteException ex)
            {
                //MessageBox.Show(ex.ToString());
                MessageBox.Show("Exception: Terjadi kesalahan pada saat get data receive maintank ritasi Port local, mohon periksa log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(ex.Message, "getListReceiveFTPort()");
            }

            //query = "select * from tbl_t_log_sheet_detail where syncs=3 AND issued_date between '" + dateTimePicker1.Value.ToString("yyyy-MM-dd 00:00:00") + "' and '" + dateTimePicker2.Value.ToString("yyyy-MM-dd 23:59:59") + "' and shift ='" + cbShift.Text + "'  ORDER BY ref_hour_start";
            query = querySqlSync();
            try
            {

                SQLiteCommand cmd = new SQLiteCommand(query, conn);
                SQLiteDataReader reader = cmd.ExecuteReader();



                while (reader.Read())
                {
                    var cls = new tbl_t_receive_maintank_ritasi();

                    cls.pid_ritasi_maintank = reader["pid_ritasi_maintank"].ToString();
                    cls.send_date = DateTime.Parse(reader["send_date"].ToString());
                    cls.send_po_no = reader["send_po_no"].ToString();
                    cls.district = reader["district"].ToString();                    
                    cls.receive_at = reader["receive_at"].ToString();
                    cls.receive_at_code = reader["receive_at_code"].ToString();                                       
                    cls.receive_density = double.Parse(reader["receive_density"].ToString());
                    cls.receive_temperature = int.Parse(reader["receive_temperature"].ToString());


                    cls.receive_sn_flow_meter1 = reader["receive_sn_flow_meter1"].ToString();
                    cls.receive_meter_faktor = double.Parse(reader["receive_meter_faktor"].ToString());
                    cls.receive_qty1 = int.Parse(reader["receive_qty1"].ToString());
                    cls.receive_qty1_x_mf = int.Parse(reader["receive_qty1_x_mf"].ToString());
                    cls.receive_flow_meter_awal1 = int.Parse(reader["receive_flow_meter_awal1"].ToString());
                    cls.receive_flow_meter_akhir1 = int.Parse(reader["receive_flow_meter_akhir1"].ToString());


                    cls.receive_sn_flow_meter2 = reader["receive_sn_flow_meter2"].ToString();
                    cls.receive_meter_faktor2 = double.Parse(reader["receive_meter_faktor2"].ToString());
                    cls.receive_qty2 = int.Parse(reader["receive_qty2"].ToString());
                    cls.receive_qty2_x_mf = int.Parse(reader["receive_qty2_x_mf"].ToString());
                    cls.receive_flow_meter_awal2 = int.Parse(reader["receive_flow_meter_awal2"].ToString());
                    cls.receive_flow_meter_akhir2 = int.Parse(reader["receive_flow_meter_akhir2"].ToString());


                    cls.receive_qty_additive = int.Parse(reader["receive_qty_additive"].ToString());
                    //cls.receive_iscomplete = int.Parse(reader["receive_iscomplete"].ToString());
                    cls.receive_driver_name = reader["receive_driver_name"].ToString();
                    cls.receive_no_segel_1 = reader["receive_no_segel_1"].ToString();
                    cls.receive_no_segel_2 = reader["receive_no_segel_2"].ToString();
                    cls.receive_no_segel_3 = reader["receive_no_segel_3"].ToString();
                    cls.receive_no_segel_4 = reader["receive_no_segel_4"].ToString();
                    cls.receive_no_segel_5 = reader["receive_no_segel_5"].ToString();
                    cls.receive_no_segel_6 = reader["receive_no_segel_6"].ToString();
                    cls.doc_status = reader["doc_status"].ToString();
                    cls.last_mod_by = reader["last_mod_by"].ToString();
                    cls.foto = reader["foto"].ToString();
                    
                    cls.time_start = DateTime.Parse(reader["time_start"].ToString());
                    cls.time_end = DateTime.Parse(reader["time_end"].ToString());



                    _list.Add(cls);
                }
                reader.Dispose();
                conn.Close();
            }
            catch (SQLiteException ex)
            {
                conn.Close();
                //MessageBox.Show(ex.ToString());
                MessageBox.Show("Exception: Terjadi kesalahan pada saat get data receive maintank ritasi Port local, mohon periksa log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(ex.Message, "getListReceiveFTDarat()");
            }
            if (conn.State == ConnectionState.Open) conn.Close();
            return _list;
        }
      
     
        
       


        void changeFilter()
        {
            DateTime fromdate = Convert.ToDateTime(dateTimePicker1.Text);
            DateTime todate = Convert.ToDateTime(dateTimePicker2.Text);

            if (fromdate <= todate)
            {
                TimeSpan ts = todate.Subtract(fromdate);
                int days = Convert.ToInt16(ts.Days);

                try
                {
                    String iQuery = querySql();// "SELECT issued_date,ref_hour_start, unit_no, hm, whouse_id, qty, shift , sync_desc, job_row_id FROM vw_logsheet where issued_date between '" + dateTimePicker1.Value.ToString("yyyy-MM-dd 00:00:00") + "' and '" + dateTimePicker2.Value.ToString("yyyy-MM-dd 23:59:59") + "' order by issued_date,ref_hour_start";
                    SQLiteConnection conn = new SQLiteConnection(myConnectionString);
                    SQLiteCommand cmd = new SQLiteCommand(iQuery, conn);

                    if (conn.State != ConnectionState.Open) conn.Open();
                    DataTable dataTable = new DataTable();

                    dataTable.Load(cmd.ExecuteReader());
                    dataGridView1.DataSource = dataTable;                    
                    if (conn.State == ConnectionState.Open) conn.Close();
                }
                catch (Exception)
                {

                    throw;
                }
            }
        }

        string querySql()
        {
            var sql = " SELECT sync_desc,pid_ritasi_maintank,send_date,send_po_no, receive_at,time_start, receive_qty1,receive_qty1_x_mf,receive_sn_flow_meter1,receive_meter_faktor,receive_flow_meter_awal1,receive_flow_meter_akhir1 ";
            sql += " FROM vw_receive_maintank_ritasi ";
            sql += " WHERE send_date between  '" + dateTimePicker1.Value.ToString("yyyy-MM-dd 00:00:00") + "' and '" + dateTimePicker2.Value.ToString("yyyy-MM-dd 23:59:59") +"'";
            sql += " and doc_status IN('51') ";   
            if (!string.IsNullOrEmpty(txtSearch.Text))
            {
                sql += " and send_po_no like '%" + txtSearch.Text + "%' ";
            }
            sql += " ORDER BY case when syncs = 2 then 1 ";
            sql += " when syncs = 9 then 2 ";
            sql += " when syncs = 3 then 3 ";
            sql += " when syncs = 7 then 4 ";
            sql += " END ASC ";
            sql += ", send_date ASC ,time_start ASC ";
            return sql;
        }
        string querySqlSync()
        {            
            var sql = " SELECT * ";
            sql += " FROM tbl_t_receive_maintank_ritasi ";
            sql += " WHERE syncs = 3 and send_date between  '" + dateTimePicker1.Value.ToString("yyyy-MM-dd 00:00:00") + "' and '" + dateTimePicker2.Value.ToString("yyyy-MM-dd 23:59:59") +  "' ";
            sql += " and doc_status IN('51')  ";
            if (!string.IsNullOrEmpty(txtSearch.Text))
            {
                sql += " and send_po_no like '%" + txtSearch.Text + "%' ";
            }
            sql += " ORDER BY case when syncs = 2 then 1 ";
            sql += " when syncs = 9 then 2 ";
            sql += " when syncs = 3 then 3 ";
            sql += " when syncs = 7 then 4 ";
            sql += " END ASC ";
            sql += ", send_date ASC ,time_start ASC ";
            return sql;
        }

        private void dateTimePicker2_CloseUp(object sender, EventArgs e)
        {
            changeFilter();
        }

        private void dateTimePicker1_CloseUp(object sender, EventArgs e)
        {
            changeFilter();
        }

        private void cbShift_SelectedIndexChanged(object sender, EventArgs e)
        {
            changeFilter();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void cbStatusIssuing_SelectedIndexChanged(object sender, EventArgs e)
        {
            changeFilter();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            changeFilter();
        }
    }
}
