﻿namespace LCR600SDK
{
    partial class FReceivedDirectOwnerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FReceivedDirectOwnerForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnSubmitReceive = new System.Windows.Forms.Button();
            this.btnLoad = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.comboBoxWarehouse = new System.Windows.Forms.ComboBox();
            this.tbQtyLoading = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtSnFlowMeter = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtSelesaiLoading = new System.Windows.Forms.TextBox();
            this.txtMulaiLoading = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtNomorSuratJalan = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtTemperatur = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtDensity = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtReceiveAtCode = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.chkConfirm = new System.Windows.Forms.CheckBox();
            this.btnSearchPo = new System.Windows.Forms.Button();
            this.txtReceivedfmAkhir = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtReceivedfmAwal = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtReceivedQty = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtDistrik = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPoNo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtWarehouse = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.comboBoxWarehouse);
            this.panel1.Controls.Add(this.btnSubmitReceive);
            this.panel1.Controls.Add(this.btnLoad);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Location = new System.Drawing.Point(4, 6);
            this.panel1.Margin = new System.Windows.Forms.Padding(0, 1, 0, 1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(804, 476);
            this.panel1.TabIndex = 0;
            // 
            // btnSubmitReceive
            // 
            this.btnSubmitReceive.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSubmitReceive.BackColor = System.Drawing.Color.Green;
            this.btnSubmitReceive.FlatAppearance.BorderSize = 0;
            this.btnSubmitReceive.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSubmitReceive.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.btnSubmitReceive.ForeColor = System.Drawing.Color.White;
            this.btnSubmitReceive.Location = new System.Drawing.Point(650, 291);
            this.btnSubmitReceive.Margin = new System.Windows.Forms.Padding(1);
            this.btnSubmitReceive.Name = "btnSubmitReceive";
            this.btnSubmitReceive.Size = new System.Drawing.Size(124, 45);
            this.btnSubmitReceive.TabIndex = 6;
            this.btnSubmitReceive.Text = "SUBMIT";
            this.btnSubmitReceive.UseVisualStyleBackColor = false;
            this.btnSubmitReceive.Click += new System.EventHandler(this.btnSubmitReceive_Click);
            // 
            // btnLoad
            // 
            this.btnLoad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLoad.BackColor = System.Drawing.Color.Orange;
            this.btnLoad.FlatAppearance.BorderSize = 0;
            this.btnLoad.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLoad.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.btnLoad.ForeColor = System.Drawing.Color.White;
            this.btnLoad.Location = new System.Drawing.Point(535, 291);
            this.btnLoad.Margin = new System.Windows.Forms.Padding(2);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(112, 45);
            this.btnLoad.TabIndex = 5;
            this.btnLoad.Text = "LOAD";
            this.btnLoad.UseVisualStyleBackColor = false;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.BackColor = System.Drawing.Color.Red;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(408, 291);
            this.button1.Margin = new System.Windows.Forms.Padding(1);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(124, 45);
            this.button1.TabIndex = 4;
            this.button1.Text = "CANCEL";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.txtWarehouse);
            this.panel2.Controls.Add(this.tbQtyLoading);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.txtSnFlowMeter);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.txtSelesaiLoading);
            this.panel2.Controls.Add(this.txtMulaiLoading);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.txtNomorSuratJalan);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.txtTemperatur);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.txtDensity);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.txtReceiveAtCode);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.chkConfirm);
            this.panel2.Controls.Add(this.btnSearchPo);
            this.panel2.Controls.Add(this.txtReceivedfmAkhir);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.txtReceivedfmAwal);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.txtReceivedQty);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.txtDistrik);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.txtPoNo);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Location = new System.Drawing.Point(16, 16);
            this.panel2.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(758, 271);
            this.panel2.TabIndex = 0;
            // 
            // comboBoxWarehouse
            // 
            this.comboBoxWarehouse.DropDownHeight = 150;
            this.comboBoxWarehouse.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.comboBoxWarehouse.FormattingEnabled = true;
            this.comboBoxWarehouse.IntegralHeight = false;
            this.comboBoxWarehouse.Location = new System.Drawing.Point(16, 291);
            this.comboBoxWarehouse.Name = "comboBoxWarehouse";
            this.comboBoxWarehouse.Size = new System.Drawing.Size(154, 21);
            this.comboBoxWarehouse.Sorted = true;
            this.comboBoxWarehouse.TabIndex = 111;
            this.comboBoxWarehouse.Visible = false;
            this.comboBoxWarehouse.SelectedIndexChanged += new System.EventHandler(this.comboBoxWarehouse_SelectedIndexChanged);
            // 
            // tbQtyLoading
            // 
            this.tbQtyLoading.Location = new System.Drawing.Point(146, 225);
            this.tbQtyLoading.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.tbQtyLoading.Name = "tbQtyLoading";
            this.tbQtyLoading.Size = new System.Drawing.Size(154, 20);
            this.tbQtyLoading.TabIndex = 110;
            this.tbQtyLoading.Click += new System.EventHandler(this.tbQtyLoading_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(14, 226);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(130, 13);
            this.label14.TabIndex = 109;
            this.label14.Text = "Preset Gross Volume (liter)";
            // 
            // txtSnFlowMeter
            // 
            this.txtSnFlowMeter.Enabled = false;
            this.txtSnFlowMeter.Location = new System.Drawing.Point(146, 126);
            this.txtSnFlowMeter.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.txtSnFlowMeter.Name = "txtSnFlowMeter";
            this.txtSnFlowMeter.Size = new System.Drawing.Size(154, 20);
            this.txtSnFlowMeter.TabIndex = 108;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(14, 128);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(77, 13);
            this.label10.TabIndex = 107;
            this.label10.Text = "SN Flow Meter";
            // 
            // txtSelesaiLoading
            // 
            this.txtSelesaiLoading.Enabled = false;
            this.txtSelesaiLoading.Location = new System.Drawing.Point(512, 117);
            this.txtSelesaiLoading.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.txtSelesaiLoading.Name = "txtSelesaiLoading";
            this.txtSelesaiLoading.Size = new System.Drawing.Size(154, 20);
            this.txtSelesaiLoading.TabIndex = 106;
            // 
            // txtMulaiLoading
            // 
            this.txtMulaiLoading.Enabled = false;
            this.txtMulaiLoading.Location = new System.Drawing.Point(512, 95);
            this.txtMulaiLoading.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.txtMulaiLoading.Name = "txtMulaiLoading";
            this.txtMulaiLoading.Size = new System.Drawing.Size(154, 20);
            this.txtMulaiLoading.TabIndex = 105;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(379, 120);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(82, 13);
            this.label9.TabIndex = 104;
            this.label9.Text = "Selesai Loading";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(379, 95);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 13);
            this.label4.TabIndex = 103;
            this.label4.Text = "Mulai Loading";
            // 
            // txtNomorSuratJalan
            // 
            this.txtNomorSuratJalan.Location = new System.Drawing.Point(146, 199);
            this.txtNomorSuratJalan.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.txtNomorSuratJalan.Name = "txtNomorSuratJalan";
            this.txtNomorSuratJalan.Size = new System.Drawing.Size(154, 20);
            this.txtNomorSuratJalan.TabIndex = 102;
            this.txtNomorSuratJalan.Click += new System.EventHandler(this.txtNomorSuratJalan_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(14, 201);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(94, 13);
            this.label8.TabIndex = 101;
            this.label8.Text = "Nomor Surat Jalan";
            // 
            // txtTemperatur
            // 
            this.txtTemperatur.Location = new System.Drawing.Point(146, 175);
            this.txtTemperatur.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.txtTemperatur.Name = "txtTemperatur";
            this.txtTemperatur.Size = new System.Drawing.Size(154, 20);
            this.txtTemperatur.TabIndex = 100;
            this.txtTemperatur.Click += new System.EventHandler(this.txtTemperatur_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(14, 176);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 13);
            this.label6.TabIndex = 99;
            this.label6.Text = "Temperatur";
            // 
            // txtDensity
            // 
            this.txtDensity.Location = new System.Drawing.Point(146, 150);
            this.txtDensity.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.txtDensity.Name = "txtDensity";
            this.txtDensity.Size = new System.Drawing.Size(154, 20);
            this.txtDensity.TabIndex = 98;
            this.txtDensity.Click += new System.EventHandler(this.txtDensity_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(14, 151);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 13);
            this.label5.TabIndex = 97;
            this.label5.Text = "Density";
            // 
            // txtReceiveAtCode
            // 
            this.txtReceiveAtCode.Enabled = false;
            this.txtReceiveAtCode.Location = new System.Drawing.Point(146, 99);
            this.txtReceiveAtCode.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.txtReceiveAtCode.Name = "txtReceiveAtCode";
            this.txtReceiveAtCode.Size = new System.Drawing.Size(154, 20);
            this.txtReceiveAtCode.TabIndex = 94;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 103);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 13);
            this.label3.TabIndex = 93;
            this.label3.Text = "Kode Warehouse";
            // 
            // chkConfirm
            // 
            this.chkConfirm.AutoSize = true;
            this.chkConfirm.Location = new System.Drawing.Point(391, 141);
            this.chkConfirm.Name = "chkConfirm";
            this.chkConfirm.Size = new System.Drawing.Size(290, 17);
            this.chkConfirm.TabIndex = 92;
            this.chkConfirm.Text = "Apakah pengiriman sudah selesai atau sudah lengkap ?";
            this.chkConfirm.UseVisualStyleBackColor = true;
            this.chkConfirm.Visible = false;
            // 
            // btnSearchPo
            // 
            this.btnSearchPo.Location = new System.Drawing.Point(304, 23);
            this.btnSearchPo.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.btnSearchPo.Name = "btnSearchPo";
            this.btnSearchPo.Size = new System.Drawing.Size(20, 17);
            this.btnSearchPo.TabIndex = 90;
            this.btnSearchPo.Text = "--";
            this.btnSearchPo.Click += new System.EventHandler(this.btnSearchPo_Click);
            // 
            // txtReceivedfmAkhir
            // 
            this.txtReceivedfmAkhir.Enabled = false;
            this.txtReceivedfmAkhir.Location = new System.Drawing.Point(512, 72);
            this.txtReceivedfmAkhir.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.txtReceivedfmAkhir.Name = "txtReceivedfmAkhir";
            this.txtReceivedfmAkhir.Size = new System.Drawing.Size(154, 20);
            this.txtReceivedfmAkhir.TabIndex = 27;
            this.txtReceivedfmAkhir.Text = "0";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(380, 73);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(86, 13);
            this.label12.TabIndex = 26;
            this.label12.Text = "Flow Meter Akhir";
            // 
            // txtReceivedfmAwal
            // 
            this.txtReceivedfmAwal.Enabled = false;
            this.txtReceivedfmAwal.Location = new System.Drawing.Point(512, 46);
            this.txtReceivedfmAwal.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.txtReceivedfmAwal.Name = "txtReceivedfmAwal";
            this.txtReceivedfmAwal.Size = new System.Drawing.Size(154, 20);
            this.txtReceivedfmAwal.TabIndex = 25;
            this.txtReceivedfmAwal.Text = "0";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(380, 48);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(85, 13);
            this.label13.TabIndex = 24;
            this.label13.Text = "Flow Meter Awal";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(14, 77);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(58, 13);
            this.label11.TabIndex = 20;
            this.label11.Text = "Diterima Di";
            // 
            // txtReceivedQty
            // 
            this.txtReceivedQty.Enabled = false;
            this.txtReceivedQty.Location = new System.Drawing.Point(512, 22);
            this.txtReceivedQty.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.txtReceivedQty.Name = "txtReceivedQty";
            this.txtReceivedQty.Size = new System.Drawing.Size(154, 20);
            this.txtReceivedQty.TabIndex = 17;
            this.txtReceivedQty.Text = "0";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(380, 24);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(23, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "Qty";
            // 
            // txtDistrik
            // 
            this.txtDistrik.Enabled = false;
            this.txtDistrik.Location = new System.Drawing.Point(146, 47);
            this.txtDistrik.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.txtDistrik.Name = "txtDistrik";
            this.txtDistrik.Size = new System.Drawing.Size(154, 20);
            this.txtDistrik.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 53);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Distrik";
            // 
            // txtPoNo
            // 
            this.txtPoNo.BackColor = System.Drawing.SystemColors.Info;
            this.txtPoNo.Location = new System.Drawing.Point(146, 23);
            this.txtPoNo.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.txtPoNo.Name = "txtPoNo";
            this.txtPoNo.ReadOnly = true;
            this.txtPoNo.Size = new System.Drawing.Size(154, 20);
            this.txtPoNo.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 30);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nomer PO";
            // 
            // txtWarehouse
            // 
            this.txtWarehouse.Enabled = false;
            this.txtWarehouse.Location = new System.Drawing.Point(146, 73);
            this.txtWarehouse.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.txtWarehouse.Name = "txtWarehouse";
            this.txtWarehouse.Size = new System.Drawing.Size(154, 20);
            this.txtWarehouse.TabIndex = 111;
            // 
            // FReceivedDirectOwnerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(812, 354);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FReceivedDirectOwnerForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form Receive Direct Owner";
            this.Load += new System.EventHandler(this.FReceivedDirectForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox txtReceivedfmAkhir;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtReceivedfmAwal;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtReceivedQty;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSearchPo;
        public System.Windows.Forms.TextBox txtPoNo;
        public System.Windows.Forms.TextBox txtDistrik;
        private System.Windows.Forms.CheckBox chkConfirm;
        public System.Windows.Forms.TextBox txtSelesaiLoading;
        public System.Windows.Forms.TextBox txtMulaiLoading;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.TextBox txtNomorSuratJalan;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.TextBox txtTemperatur;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.TextBox txtDensity;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.TextBox txtReceiveAtCode;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.TextBox txtSnFlowMeter;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.Button button1;
        public System.Windows.Forms.TextBox tbQtyLoading;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button btnSubmitReceive;
        private System.Windows.Forms.ComboBox comboBoxWarehouse;
        public System.Windows.Forms.TextBox txtWarehouse;
    }
}