﻿using MySql.Data.MySqlClient; using System.Data.SQLite; using System.IO; using System.Reflection;
using SDK;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LCR600SDK
{
    public partial class FTransProgress : Form
    {
        private string myConnectionString = string.Empty;
        private byte device = 0;
        PasswordEncrypDecrypt decryptor = new PasswordEncrypDecrypt();

        private void FTransProgress_Load(object sender, EventArgs e)
        {
            var _proper = Properties.Settings.Default;
            //myConnectionString = "Data Source=" + Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "SQLite", "db_iflash.db") + " ;New=False;";
            var passPhrase = "iFlashDesktop";
            var decryp = decryptor.DecryptPassword(_proper.db_pass, passPhrase);
            myConnectionString = "Data Source=" + Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "SQLite", "db_iflash.db") + " ;New=False;";
            initLoadForm();
        }

        public FTransProgress()
        {
            InitializeComponent();
        }

        void initLoadForm()
        {
            try
            { 
                doOpenLCR();
                //var _lcrData = GlobalModel.lcrData;
                //var _logsheet = GlobalModel.logsheet_detail;
                //var lcrData = new LcrData();

                //System.Threading.Thread.Sleep(1000);
                //lcrData.ValveStatus = startLCR();

                //GlobalModel.lcrData = lcrData;
                //lblValve.Text = $"Valve: {lcrData.ValveStatus}";

                //var _fmStart = getGrossTotalizer();
                //_logsheet.flow_meter_start = isNumeric(_fmStart) ? Convert.ToDouble(_fmStart) : 0;

                //GlobalModel.logsheet_detail = _logsheet;
                //getDataLcr();
                timer1.Enabled = true;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            try
            {
                var _lcrData = GlobalModel.lcrData;
                _lcrData.ValveStatus = printLCR();
                lblValve.Text = $"Valve: {_lcrData.ValveStatus}";
                GlobalModel.lcrData = _lcrData;

                System.Threading.Thread.Sleep(1000);

                saveData();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        void saveData()
        {
            var _item = GlobalModel.logsheet_detail;
            var _lcrData = GlobalModel.lcrData;

            _item.qty = isNumeric(_lcrData.GrossQTY) ? Convert.ToInt16(_lcrData.GrossQTY) : 0;
            _item.flow_meter_end = isNumeric(_lcrData.GrossTotalizer) ? Convert.ToInt16(_lcrData.GrossTotalizer) : 0;

            SQLiteConnection conn;

            conn = new SQLiteConnection(myConnectionString);
            if (conn.State != ConnectionState.Open) conn.Open();
            string query = string.Empty;
            int recs = 0;
            query = "INSERT INTO tbl_t_log_sheet_detail(job_row_id,dstrct_code,log_sheet_code,input_type,issued_date,whouse_id,unit_no,max_tank_capacity,hm_before,hm,flw_meter,meter_faktor,qty_loqsheet,qty,shift,fuel_oil_type,stat_type,nrp_operator,nama_operator,work_area,location,ref_condition,ref_hour_start,ref_hour_stop,note,timezone,flag_loading,loadingerror,loadingdatetime,mod_by,mod_date,is_load_to_fosto,foto,flow_meter_start,flow_meter_end,resp_code,resp_name,text_header,text_sub_header,text_body,syncs)"
                  + " VALUES(@job_row_id,@dstrct_code,@log_sheet_code,@input_type,@issued_date,@whouse_id,@unit_no,@max_tank_capacity,@hm_before,@hm,@flw_meter,@meter_faktor,@qty_loqsheet,@qty,@shift,@fuel_oil_type,@stat_type,@nrp_operator,@nama_operator,@work_area,@location,@ref_condition,@ref_hour_start,@ref_hour_stop,@note,@timezone,@flag_loading,@loadingerror,@loadingdatetime,@mod_by,@mod_date,@is_load_to_fosto,@foto,@flow_meter_start,@flow_meter_end,@resp_code,@resp_name,@text_header,@text_sub_header,@text_body,@syncs)";

            SQLiteCommand cmd = new SQLiteCommand(query, conn);

            cmd.Parameters.Add(new SQLiteParameter("@job_row_id", _item.job_row_id));
            cmd.Parameters.Add(new SQLiteParameter("@dstrct_code", _item.dstrct_code));
            cmd.Parameters.Add(new SQLiteParameter("@log_sheet_code", _item.log_sheet_code));
            cmd.Parameters.Add(new SQLiteParameter("@input_type", _item.input_type));
            cmd.Parameters.Add(new SQLiteParameter("@issued_date", _item.issued_date));
            cmd.Parameters.Add(new SQLiteParameter("@whouse_id", _item.whouse_id));
            cmd.Parameters.Add(new SQLiteParameter("@unit_no", _item.unit_no));
            cmd.Parameters.Add(new SQLiteParameter("@max_tank_capacity", _item.max_tank_capacity));
            cmd.Parameters.Add(new SQLiteParameter("@hm_before", _item.hm_before));
            cmd.Parameters.Add(new SQLiteParameter("@hm", _item.hm));
            cmd.Parameters.Add(new SQLiteParameter("@flw_meter", _item.flw_meter));
            cmd.Parameters.Add(new SQLiteParameter("@meter_faktor", _item.meter_faktor));
            cmd.Parameters.Add(new SQLiteParameter("@qty_loqsheet", _item.qty_loqsheet));
            cmd.Parameters.Add(new SQLiteParameter("@qty", _item.qty));
            cmd.Parameters.Add(new SQLiteParameter("@shift", _item.shift));
            cmd.Parameters.Add(new SQLiteParameter("@fuel_oil_type", _item.fuel_oil_type));
            cmd.Parameters.Add(new SQLiteParameter("@stat_type", _item.stat_type));
            cmd.Parameters.Add(new SQLiteParameter("@nrp_operator", _item.nrp_operator));
            cmd.Parameters.Add(new SQLiteParameter("@nama_operator", _item.nama_operator));
            cmd.Parameters.Add(new SQLiteParameter("@work_area", _item.work_area));
            cmd.Parameters.Add(new SQLiteParameter("@location", _item.location));
            cmd.Parameters.Add(new SQLiteParameter("@ref_condition", _item.ref_condition));
            cmd.Parameters.Add(new SQLiteParameter("@ref_hour_start", _item.ref_hour_start));
            cmd.Parameters.Add(new SQLiteParameter("@ref_hour_stop", _item.ref_hour_stop));
            cmd.Parameters.Add(new SQLiteParameter("@note", _item.note));
            cmd.Parameters.Add(new SQLiteParameter("@timezone", _item.timezone));
            cmd.Parameters.Add(new SQLiteParameter("@flag_loading", _item.flag_loading));
            cmd.Parameters.Add(new SQLiteParameter("@loadingerror", _item.loadingerror));
            cmd.Parameters.Add(new SQLiteParameter("@loadingdatetime", _item.loadingdatetime));
            cmd.Parameters.Add(new SQLiteParameter("@mod_by", _item.mod_by));
            cmd.Parameters.Add(new SQLiteParameter("@mod_date", _item.mod_date));
            cmd.Parameters.Add(new SQLiteParameter("@is_load_to_fosto", _item.is_load_to_fosto));
            cmd.Parameters.Add(new SQLiteParameter("@foto", _item.foto));
            cmd.Parameters.Add(new SQLiteParameter("@flow_meter_start", _item.flow_meter_start));
            cmd.Parameters.Add(new SQLiteParameter("@flow_meter_end", _item.flow_meter_end));
            cmd.Parameters.Add(new SQLiteParameter("@resp_code", _item.resp_code));
            cmd.Parameters.Add(new SQLiteParameter("@resp_name", _item.resp_name));
            cmd.Parameters.Add(new SQLiteParameter("@text_header", _item.text_header));
            cmd.Parameters.Add(new SQLiteParameter("@text_sub_header", _item.text_sub_header));
            cmd.Parameters.Add(new SQLiteParameter("@text_body", _item.text_body));
            cmd.Parameters.Add(new SQLiteParameter("@syncs", _item.syncs));

            recs = cmd.ExecuteNonQuery();

            if (recs > 0)
            {
                if (Application.OpenForms["FTransaction"] != null)
                {
                    (Application.OpenForms["FTransaction"] as FTransaction).clearData();
                }
                this.Close();
            }
            if (conn.State == ConnectionState.Open) conn.Close();
        }

        int execCmd(String sQuery, SQLiteConnection sConn)
        {
            int iReturn = 0;

            var cmd = new SQLiteCommand(sQuery, sConn);
            iReturn = cmd.ExecuteNonQuery();

            return iReturn;
        }

        #region LCR
        private void doOpenLCR()
        {
            var lcrData = new LcrData();

            device = connectLCR();
            if (device > 0)
            {
                lcrData.GrossQTY = "0";
                lcrData.GrossTotalizer = "0";
                lcrData.GrossPreset = "0";
                lcrData.Flowrate = "0";
                lcrData.ValveStatus = "#####";

                GlobalModel.lcrData = lcrData;
            }
        }

        private void doCloseLCR()
        {
            var lcrData = new LcrData();
            lcrData.GrossQTY = "-1";
            lcrData.GrossTotalizer = "-1";
            lcrData.GrossPreset = "-1";
            lcrData.Flowrate = "-1";
            lcrData.ValveStatus = "#####";

            GlobalModel.lcrData = lcrData;
        }

        private byte connectLCR()
        {
            byte device = 250;
            byte numDevices;
            byte[] deviceList = new byte[device];
            IntPtr deviceListPtr = Marshal.AllocHGlobal(250);
            byte rc = LCP02API.LCP02Open(device, device, out numDevices, deviceListPtr);
            if (rc != (byte)LCPAPI.LCPR.LCPR_OK && rc != (byte)LCP02API.LCR_RETURN_CODES.LCP02Ra_ALREADYOPENED)
            {
                device = 0;
            }
            return device;
        }

        private void startValve()
        {
            var _lcrData = GlobalModel.lcrData;
            _lcrData.ValveStatus = startLCR();
            GlobalModel.lcrData = _lcrData;
        }

        void getDataLcr()
        {
            var _lcrData = GlobalModel.lcrData;

            var _GrossQTY = getGrossQty();
            var _GrossTotalizer = getGrossTotalizer();
            var _GrossPreset = getGrossPreset();
            var _Flowrate = getFlowrate();

            //_lcrData.GrossQTY = isNumeric(_GrossQTY) ? _GrossQTY : _lcrData.GrossQTY;
            //_lcrData.GrossTotalizer = isNumeric(_GrossTotalizer) ? _GrossTotalizer : _lcrData.GrossTotalizer;
            //_lcrData.GrossPreset = isNumeric(_GrossPreset) ? _GrossPreset : _lcrData.GrossPreset;
            //_lcrData.Flowrate = isNumeric(_Flowrate) ? _Flowrate : _lcrData.Flowrate;

            lblLitr.Text = $"{_GrossQTY}";
            lblFLowrate.Text = $"{_Flowrate}";
            lblTotalizer.Text = $"{_GrossTotalizer}"; 

            GlobalModel.lcrData = _lcrData;

            string allText = _GrossQTY + _GrossTotalizer + _GrossPreset;

            if (allText.Contains("FAILED"))
            {
                doCloseLCR();
                doOpenLCR();
                doCloseLCR();
            }
        }

        bool isNumeric(String sValue)
        {
            long _long = 0;
            return long.TryParse(sValue, out _long);
        }

        private string getFlowrate()
        {
            byte rc;
            string flowRateStr = "FAILED";

            if (device == 0)
            {
                return flowRateStr;
            }

            byte fieldNum = (byte)LCP02API.LCR_FIELD_NUMBERS.LCRF_FlowRate_NE;
            int size = LCP02API.LCP02FieldSize(fieldNum);
            IntPtr flowRatePtr = Marshal.AllocHGlobal((int)size);

            byte devStatus;

            rc = LCP02API.LCP02GetField(device, fieldNum, flowRatePtr, out devStatus,
                                        (byte)LCP02API.LCR_RETURN_CODES.LCRM_NO_WAIT);

            if (rc != (byte)LCPAPI.LCPR.LCPR_OK)
            {
                return flowRateStr;
            }

            string strflowRate = Marshal.PtrToStringAnsi(flowRatePtr, (int)size);
            LCP02API.FloatByteArray fbaGross;
            byte[] strGrossArr = null;

            fbaGross = new LCP02API.FloatByteArray();
            strGrossArr = Encoding.GetEncoding(1252).GetBytes(strflowRate);
            fbaGross.Byte1 = strGrossArr[0];
            fbaGross.Byte2 = strGrossArr[1];
            fbaGross.Byte3 = strGrossArr[2];
            fbaGross.Byte4 = strGrossArr[3];

            double grossQty = 0;
            strflowRate = fbaGross.int32Value.ToString();
            if (strflowRate != "")
            {
                grossQty = Convert.ToDouble(strflowRate);
                grossQty = grossQty / 10;
            }

            flowRateStr = grossQty.ToString();

            return flowRateStr;
        }

        private string getGrossPreset()
        {
            byte rc;
            string grossPresetStr = "FAILED";

            if (device == 0)
            {
                return grossPresetStr;
            }

            byte fieldNum = (byte)LCP02API.LCR_FIELD_NUMBERS.LCRF_GrossPreset_PL;
            int size = LCP02API.LCP02FieldSize(fieldNum);
            IntPtr grossPresetPtr = Marshal.AllocHGlobal((int)size);

            byte devStatus;

            rc = LCP02API.LCP02GetField(device, fieldNum, grossPresetPtr, out devStatus,
                                        (byte)LCP02API.LCR_RETURN_CODES.LCRM_NO_WAIT);

            if (rc != (byte)LCPAPI.LCPR.LCPR_OK)
            {
                return grossPresetStr;
            }

            string strGrossPreset = Marshal.PtrToStringAnsi(grossPresetPtr, (int)size);
            LCP02API.FloatByteArray fbaGross;
            byte[] strGrossArr = null;

            fbaGross = new LCP02API.FloatByteArray();
            strGrossArr = Encoding.GetEncoding(1252).GetBytes(strGrossPreset);
            fbaGross.Byte1 = strGrossArr[0];
            fbaGross.Byte2 = strGrossArr[1];
            fbaGross.Byte3 = strGrossArr[2];
            fbaGross.Byte4 = strGrossArr[3];

            double grossQty = 0;
            strGrossPreset = fbaGross.int32Value.ToString();
            if (strGrossPreset != "")
            {
                grossQty = Convert.ToDouble(strGrossPreset);
                grossQty = grossQty / 10;
            }

            grossPresetStr = grossQty.ToString();

            return grossPresetStr;
        }

        private string getGrossTotalizer()
        {
            byte rc;
            string grossTotalizerStr = "FAILED";

            if (device == 0)
            {
                return grossTotalizerStr;
            }

            byte fieldNum = (byte)LCP02API.LCR_FIELD_NUMBERS.LCRF_GrossTotal_WM;
            int size = LCP02API.LCP02FieldSize(fieldNum);
            IntPtr grossTotalizerPtr = Marshal.AllocHGlobal((int)size);

            byte devStatus;

            rc = LCP02API.LCP02GetField(device, fieldNum, grossTotalizerPtr, out devStatus,
                                        (byte)LCP02API.LCR_RETURN_CODES.LCRM_NO_WAIT);

            if (rc != (byte)LCPAPI.LCPR.LCPR_OK)
            {
                return grossTotalizerStr;
            }

            string strGrossTotalizer = Marshal.PtrToStringAnsi(grossTotalizerPtr, (int)size);
            LCP02API.FloatByteArray fbaGross;
            byte[] strGrossArr = null;

            fbaGross = new LCP02API.FloatByteArray();
            strGrossArr = Encoding.GetEncoding(1252).GetBytes(strGrossTotalizer);
            fbaGross.Byte1 = strGrossArr[0];
            fbaGross.Byte2 = strGrossArr[1];
            fbaGross.Byte3 = strGrossArr[2];
            fbaGross.Byte4 = strGrossArr[3];

            double grossQty = 0;
            strGrossTotalizer = fbaGross.int32Value.ToString();
            if (strGrossTotalizer != "")
            {
                grossQty = Convert.ToDouble(strGrossTotalizer);
                grossQty = grossQty / 10;
            }

            grossTotalizerStr = grossQty.ToString();

            return grossTotalizerStr;
        }

        private string getGrossQty()
        {
            byte rc;
            string grossQtyStr = "FAILED";


            if (device == 0)
            {
                return grossQtyStr;
            }


            byte fieldNum = (byte)LCP02API.LCR_FIELD_NUMBERS.LCRF_GrossQty_NE;
            int size = LCP02API.LCP02FieldSize(fieldNum);
            IntPtr grossQtyPtr = Marshal.AllocHGlobal((int)size);

            byte devStatus;

            rc = LCP02API.LCP02GetField(device, fieldNum, grossQtyPtr, out devStatus,
                                        (byte)LCP02API.LCR_RETURN_CODES.LCRM_NO_WAIT);

            if (rc != (byte)LCPAPI.LCPR.LCPR_OK)
            {
                return grossQtyStr;
            }

            string strGrossQty = Marshal.PtrToStringAnsi(grossQtyPtr, (int)size);
            LCP02API.FloatByteArray fbaGross;
            byte[] strGrossArr = null;

            fbaGross = new LCP02API.FloatByteArray();
            strGrossArr = Encoding.GetEncoding(1252).GetBytes(strGrossQty);
            fbaGross.Byte1 = strGrossArr[0];
            fbaGross.Byte2 = strGrossArr[1];
            fbaGross.Byte3 = strGrossArr[2];
            fbaGross.Byte4 = strGrossArr[3];

            double grossQty = 0;
            strGrossQty = fbaGross.int32Value.ToString();
            if (strGrossQty != "")
            {
                grossQty = Convert.ToDouble(strGrossQty);
                grossQty = grossQty / 10;
            }

            grossQtyStr = grossQty.ToString();

            return grossQtyStr;
        }

        private string startLCR()
        {
            string strStatus = "FAILED";

            if (device == 0)
            {
                return strStatus;
            }


            byte rc = LCP02API.LCP02IssueCommand(device, (byte)LCP02API.LCR_COMMANDS.LCRC_RUN,
                                                         (byte)LCP02API.LCR_RETURN_CODES.LCRM_NO_WAIT);

            if (rc == (byte)LCPAPI.LCPR.LCPR_OK)
            {
                strStatus = "RUN";
            }

            return strStatus;
        }

        private string printLCR()
        {
            string strStatus = "FAILED";

            if (device == 0)
            {
                return strStatus;
            }

            byte rc = LCP02API.LCP02IssueCommand(device, (byte)LCP02API.LCR_COMMANDS.LCRC_PRINT,
                                                         (byte)LCP02API.LCR_RETURN_CODES.LCRM_NO_WAIT);

            if (rc == (byte)LCPAPI.LCPR.LCPR_OK)
            {
                strStatus = "PRINT";
            }

            return strStatus;
        }
        #endregion

        private void FTransProgress_FormClosing(object sender, FormClosingEventArgs e)
        {
            doCloseLCR();
        }

        private void timer1_Tick(object sender, EventArgs e)
        { 
            //var _logsheet = GlobalModel.logsheet_detail;
            //if (_logsheet.flow_meter_start == 0)
            //{
            //    var _fmStart = getGrossTotalizer();
            //    _logsheet.flow_meter_start = isNumeric(_fmStart) ? Convert.ToDouble(_fmStart) : 0;

            //    GlobalModel.logsheet_detail = _logsheet;
            //}  
            
            getDataLcr();
        }
    }
}
