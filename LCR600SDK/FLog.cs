﻿using MySql.Data.MySqlClient; using System.Data.SQLite; using System.IO; using System.Reflection;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LCR600SDK
{
    public partial class FLog : Form
    {
        private string myConnectionString = string.Empty;
        private string pUrl_service = string.Empty;
        private int idxCell = -1;
        PasswordEncrypDecrypt decryptor = new PasswordEncrypDecrypt();
        WriteLogText log = new WriteLogText();
        public FLog()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void FLog_Load(object sender, EventArgs e)
        {
            var _proper = Properties.Settings.Default;
            pUrl_service = _proper.url_service;
            var passPhrase = "iFlashDesktop";
            this.Text = "iFLash Log " + GlobalModel.app_version;
            var decryp = decryptor.DecryptPassword(_proper.db_pass, passPhrase);
            myConnectionString = "Data Source=" + Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "SQLite", "db_iflash.db") + " ;New=False;";
            loadGrid();
        }
        void dataGridView1_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            dataGridView1.BorderStyle = BorderStyle.None;
            dataGridView1.AlternatingRowsDefaultCellStyle.BackColor = Color.FromArgb(238, 239, 249);
            dataGridView1.CellBorderStyle = DataGridViewCellBorderStyle.SingleHorizontal;
            dataGridView1.DefaultCellStyle.SelectionBackColor = Color.SeaGreen;
            dataGridView1.DefaultCellStyle.SelectionForeColor = Color.WhiteSmoke;
            dataGridView1.BackgroundColor = Color.FromArgb(0, 101, 179);
            dataGridView1.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridView1.EnableHeadersVisualStyles = false;
            dataGridView1.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
            dataGridView1.ColumnHeadersDefaultCellStyle.Font = new Font("MS Reference Sans Serif", 20);
            dataGridView1.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(0, 101, 179);
            dataGridView1.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
            if (dataGridView1.Columns.Count > 0)
            {
                dataGridView1.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                dataGridView1.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                dataGridView1.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                dataGridView1.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;                
            }

            if (dataGridView1.Controls.OfType<VScrollBar>().First().Visible)
            {
                vScrollBar1.Visible = true;
                panelScrollbar.Visible = true;
            }
            else
            {
                vScrollBar1.Visible = false;
                panelScrollbar.Visible = false;
            }

        }
        public void loadGrid()
        {
            SQLiteConnection conn = new SQLiteConnection(myConnectionString);
            SQLiteCommand cmd = new SQLiteCommand(querySql(), conn);

            if (conn.State != ConnectionState.Open) conn.Open();

            DataTable dataTable = new DataTable();
            SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);

            da.Fill(dataTable);

            dataGridView1.DataSource = dataTable;
            dataGridView1.Columns["pid"].Visible = false;
            if (conn.State == ConnectionState.Open)
                if (conn.State == ConnectionState.Open) conn.Close();
        }
        string querySql()
        {
            var sql = " SELECT pid,mod_date,actions,message FROM tbl_log order by mod_date desc";
            return sql;
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex >= 0)
                {
                    idxCell = e.RowIndex;
                    string action = dataGridView1.Rows[idxCell].Cells["actions"].FormattedValue.ToString();
                    string message = dataGridView1.Rows[idxCell].Cells["message"].FormattedValue.ToString();
                    string mod_date = dataGridView1.Rows[idxCell].Cells["mod_date"].FormattedValue.ToString();                    
                    var body = string.Format("Action : {0} \n Mod Date : {1} \n Message : \n {2}", action, mod_date, message);
                    var f = new FPopUpNotification(body, "Detail Log");
                    f.ShowDialog();
                }
            }
            catch(Exception ex)
            {
                //MessageBox.Show(ex.Message);
                MessageBox.Show("Exception: Terjadi kesalahan pada saat cell click, mohon periksa log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(ex.Message, "dataGridView1_CellClick()");
            }
            
        }

        void save_error_log(string data, string actions)
        {
            try
            {
                DateTime localDate = DateTime.Now;
                var _item = new tbl_log();
                var initData = GlobalModel.GlobalVar;

                _item.pid = System.Guid.NewGuid().ToString();
                _item.actions = $"District {initData.DataEmp.DstrctCode}/FLog/{actions}";
                _item.message = $"{data}";
                _item.mod_date = localDate;
                _item.mod_by = initData.DataEmp.Nrp;

                SQLiteConnection conn;

                conn = new SQLiteConnection(myConnectionString);
                if (conn.State != ConnectionState.Open) conn.Open();
                string query = string.Empty;
                query = "INSERT INTO tbl_log(pid,actions,message,mod_date,mod_by)"
                      + " VALUES(@pid,@actions,@message,@mod_date,@mod_by)";

                SQLiteCommand cmd = new SQLiteCommand(query, conn);

                cmd.Parameters.Add(new SQLiteParameter("@pid", _item.pid));
                cmd.Parameters.Add(new SQLiteParameter("@actions", _item.actions));
                cmd.Parameters.Add(new SQLiteParameter("@message", _item.message));
                cmd.Parameters.Add(new SQLiteParameter("@mod_date", _item.mod_date));
                cmd.Parameters.Add(new SQLiteParameter("@mod_by", _item.mod_by));
                cmd.ExecuteNonQuery();
                if (conn.State == ConnectionState.Open) conn.Close();
            }
            catch (Exception)
            {
            }
        }

        private void dataGridView1_Scroll(object sender, ScrollEventArgs e)
        {
            if (e.ScrollOrientation == ScrollOrientation.VerticalScroll)
            {
                vScrollBar1.Value = e.NewValue;
            }
            
        }

        private void vScrollBar1_Scroll(object sender, ScrollEventArgs e)
        {
            dataGridView1.FirstDisplayedScrollingRowIndex = e.NewValue;

        }
    }

}
