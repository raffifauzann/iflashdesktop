﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using SDK;

namespace LCR600SDK
{
    class ClsLCR
    {
        private bool isconnected = false;
        private bool isrunning = false;
        private byte device = 250;
        private ClsResult res = new ClsResult();
        
        public ClsResult ConnectLCR()
        {

            byte numDevices;
            byte[] deviceList = new byte[device];
            IntPtr deviceListPtr = Marshal.AllocHGlobal(250);

            ClearResult();

            res.rc = LCP02API.LCP02Open(device, device, out numDevices, deviceListPtr);

            if (res.rc != (byte)LCPAPI.LCPR.LCPR_OK && res.rc != (byte)LCP02API.LCR_RETURN_CODES.LCP02Ra_ALREADYOPENED)
            {
                res.issuccess = false;                
                device = 0;
            }
            else
            {
                isconnected = true;
                res.issuccess = isconnected;
            }
            return res;
        }
        public ClsResult DisconnectLCR()
        {
            ClearResult();
            res.rc = LCP02API.LCP02Close();
            res.issuccess = (res.rc == (byte)LCPAPI.LCPR.LCPR_OK);
            if (res.issuccess)
            {
                isconnected = false;
            }
            return res;
        }
        public ClsResult OpenValve()
        {
            if (!isconnected)
            {
                return ReturnFailed();
            }

            ClearResult();

            res = CloseValve();

            if (res.issuccess)
            {
                ClearResult();
                res = RunCommand((byte)LCP02API.LCR_COMMANDS.LCRC_RUN,(byte)LCP02API.LCR_RETURN_CODES.LCRM_WAIT);
            }

            if (res.issuccess)
                isrunning = true;

            return res;
        }
        public ClsResult CloseValve()
        {
            if (!isconnected)
            {
                return ReturnFailed();
            }

            ClearResult();

            res = RunCommand((byte)LCP02API.LCR_COMMANDS.LCRC_PRINT, (byte)LCP02API.LCR_RETURN_CODES.LCRM_WAIT);
            if (res.issuccess)
            {
                isrunning = false;
            }
            return res;
        }

        public ClsResult Pause()
        {
            if (!isconnected)
            {
                return ReturnFailed();
            }

            ClearResult();

            if (!pauseDelivery())
            {
                res = ReturnFailed();
                return res;
            }
            res.issuccess = true;
            return res;
        }
        public ClsResult GetGrossQty()
        {
            string grossQtyStr = "-9999";
            byte devStatus;

            ClearResult();

            if (!isconnected)
            {
                res = ReturnFailed();
                res.value = grossQtyStr;
                return res;
            }

            byte fieldNum = (byte)LCP02API.LCR_FIELD_NUMBERS.LCRF_GrossQty_NE;
            int size = LCP02API.LCP02FieldSize(fieldNum);
            IntPtr grossQtyPtr = Marshal.AllocHGlobal((int)size);

            res.rc = LCP02API.LCP02GetField(device, fieldNum, grossQtyPtr, out devStatus,
                                        (byte)LCP02API.LCR_RETURN_CODES.LCRM_WAIT);

            if (res.rc != (byte)LCPAPI.LCPR.LCPR_OK)
            {
                res = ReturnFailed();
                res.value = grossQtyStr;
                return res;
            }

            string strGrossQty = Marshal.PtrToStringAnsi(grossQtyPtr, (int)size);
            LCP02API.FloatByteArray fbaGross;
            byte[] strGrossArr = null;

            fbaGross = new LCP02API.FloatByteArray();
            strGrossArr = Encoding.GetEncoding(1252).GetBytes(strGrossQty);
            fbaGross.Byte1 = strGrossArr[0];
            fbaGross.Byte2 = strGrossArr[1];
            fbaGross.Byte3 = strGrossArr[2];
            fbaGross.Byte4 = strGrossArr[3];

            double grossQty = 0;
            strGrossQty = fbaGross.int32Value.ToString();
            if (strGrossQty != "")
            {
                grossQty = Convert.ToDouble(strGrossQty);
                grossQty = grossQty / 10;
            }

            grossQtyStr = grossQty.ToString();

            res.issuccess = true;
            res.value = grossQtyStr;
            return res;
        }
        public ClsResult GetGrossTotalizer()
        {
            string grossTotalizerStr = "-9999";
            byte devStatus;

            ClearResult();

            if (!isconnected)
            {
                res = ReturnFailed();
                res.value = grossTotalizerStr;
                return res;
            }

            byte fieldNum = (byte)LCP02API.LCR_FIELD_NUMBERS.LCRF_GrossTotal_WM;
            int size = LCP02API.LCP02FieldSize(fieldNum);
            IntPtr grossTotalizerPtr = Marshal.AllocHGlobal((int)size);

            

            res.rc = LCP02API.LCP02GetField(device, fieldNum, grossTotalizerPtr, out devStatus,
                                        (byte)LCP02API.LCR_RETURN_CODES.LCRM_WAIT);
            //LCRM_NO_WAIT
            if (res.rc != (byte)LCPAPI.LCPR.LCPR_OK)
            {
                res = ReturnFailed();
                res.value = grossTotalizerStr;
                return res;
            }

            string strGrossTotalizer = Marshal.PtrToStringAnsi(grossTotalizerPtr, (int)size);
            LCP02API.FloatByteArray fbaGross;
            byte[] strGrossArr = null;

            fbaGross = new LCP02API.FloatByteArray();
            strGrossArr = Encoding.GetEncoding(1252).GetBytes(strGrossTotalizer);
            fbaGross.Byte1 = strGrossArr[0];
            fbaGross.Byte2 = strGrossArr[1];
            fbaGross.Byte3 = strGrossArr[2];
            fbaGross.Byte4 = strGrossArr[3];

            double grossQty = 0;
            strGrossTotalizer = fbaGross.int32Value.ToString();
            if (strGrossTotalizer != "")
            {
                grossQty = Convert.ToDouble(strGrossTotalizer);
                grossQty = grossQty / 10;
            }

            grossTotalizerStr = grossQty.ToString();

            res.issuccess = true;
            res.value = grossTotalizerStr;
            return res;
        }
        public ClsResult SetPresetGrossVolume(int value)
        {
            byte fieldToSet;
            byte devStatus;            

            ClearResult();

            if (!isconnected)
            {
                res = ReturnFailed();
                res.value = "";
                return res;
            }

            if (isrunning)
            {
                if (!pauseDelivery())
                {
                    res = ReturnFailed();
                    res.value = "Pause failed";
                    return res;
                }
            }

            fieldToSet = (byte)LCP02API.LCR_FIELD_NUMBERS.LCRF_GrossPreset_PL;
            IntPtr dataToSend = Marshal.AllocHGlobal(sizeof(int));
            Marshal.WriteInt32(dataToSend, value);
            res.rc = LCP02API.LCP02SetField(device, fieldToSet, dataToSend, out devStatus, (byte)LCP02API.LCR_RETURN_CODES.LCRM_WAIT);

            if (res.rc != (byte)LCPAPI.LCPR.LCPR_OK)
            {
                res = ReturnFailed();
                res.value = "(1) Set Preset Gross Volume Failed.";
                return res;
            }
            else
            {
                ClearResult();
                res = RunCommand((byte)LCP02API.LCR_COMMANDS.LCRC_RUN, (byte)LCP02API.LCR_RETURN_CODES.LCRM_WAIT);
                if (!res.issuccess)
                {
                    res = ReturnFailed();
                    res.value = "(2) Set Preset Gross Volume Failed.";
                    return res;
                }
            }

            res.issuccess = true;
            res.value = value.ToString();
            return res;
        }
        private bool pauseDelivery()
        {
            byte pauseDelivery = (byte)LCP02API.LCR_COMMANDS.LCRC_STOP;

            byte rc = LCP02API.LCP02IssueCommand(device, pauseDelivery, (byte)LCP02API.LCR_RETURN_CODES.LCRM_WAIT);

            return (rc == (byte)LCPAPI.LCPR.LCPR_OK);            
        }
        private void ClearResult()
        {
            res.issuccess = false;
            res.value = "";
            res.rc = (byte)0;
        }
        private ClsResult RunCommand(byte command, byte mode)
        {            
            res.rc = LCP02API.LCP02IssueCommand(device, command,mode);
            res.issuccess = (res.rc == (byte)LCPAPI.LCPR.LCPR_OK);
            return res;
        }
        private ClsResult ReturnFailed()
        {
            ClearResult();
            res.rc = (byte) 99;
            return res;
        }
    }
}
