﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LCR600SDK
{
    public partial class FAdminValidation : Form
    {
        PasswordEncrypDecrypt decryptor = new PasswordEncrypDecrypt();
        public FAdminValidation()
        {
            InitializeComponent();
        }
        private void FAdminValidation_Load(object sender, EventArgs e)
        {
            //ONE TIME ONLY
            //var _proper = Properties.Settings.Default;
            //var pass = "iFlashLCR600";
            //var encypt = EncryptPassword(pass, pass);
            //_proper.admin_pass = encypt;
            //_proper.Save();
            //_proper.Reload();

        }
        private void btnLogin_Click(object sender, EventArgs e)
        {
            var _proper = Properties.Settings.Default;
            lblNotif.Visible = false;
            if (string.IsNullOrEmpty(txtPassword.Text))
            {
                lblNotif.Visible = true;
                lblNotif.Text = "Password tidak boleh kosong!";
            }
            else
            {
                try
                {
                    
                    var password = txtPassword.Text;
                    var decryptPass = _proper.admin_pass;
                    var pass = decryptor.DecryptPassword(decryptPass, password);
                    if (pass == password)
                    {
                        this.DialogResult = DialogResult.OK;
                        this.Hide();
                    }
                    else
                    {
                        lblNotif.Visible = true;
                        lblNotif.Text = "Password salah! Mohon dicoba lagi";
                    }
                }
                catch (Exception ex)
                {
                    lblNotif.Visible = true;
                    lblNotif.Text = "Password salah! Mohon dicoba lagi";
                }
                
            }
          
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Hide();            
        }

        private void btnShowPass_Click(object sender, EventArgs e)
        {
            if (iconBtnHide.Visible)
            {
                txtPassword.UseSystemPasswordChar = false;
                iconBtnHide.Visible = false;
                iconBtnShow.Visible = true;
            }
            else
            {
                txtPassword.UseSystemPasswordChar = true;
                iconBtnHide.Visible = true;
                iconBtnShow.Visible = false;
            }
        }

        private void txtPassword_Click(object sender, EventArgs e)
        {
            try
            {
                ProcessStartInfo psStartInfo = new ProcessStartInfo();
                var dir = Path.Combine(Environment.CurrentDirectory, "VKeyboard", "VKeyboard.exe");
                psStartInfo.FileName = dir;
                //psStartInfo.FileName = @"C:\Program Files\Common Files\microsoft shared\ink\TabTip.exe";

                Process ps = Process.Start(psStartInfo);

            }
            catch (Exception ex)
            {
                var error = ex.Message;
            }
        }
    }
}
