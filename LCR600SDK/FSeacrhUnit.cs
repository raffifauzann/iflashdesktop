﻿using MySql.Data.MySqlClient; using System.Data.SQLite; using System.IO; using System.Reflection;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LCR600SDK
{
    public partial class FSeacrhUnit : Form
    {
        public string no_unit = "";

        private string myConnectionString = string.Empty;
        private int idxCell  = -1;
        PasswordEncrypDecrypt decryptor = new PasswordEncrypDecrypt();
        WriteLogText log = new WriteLogText();
        public FSeacrhUnit()
        {
            GlobalModel.AutoRefueling = new refueling();
            InitializeComponent();
        }

        private void FSeacrhUnit_Load(object sender, EventArgs e)
        {
            var _proper = Properties.Settings.Default;
            //myConnectionString = "Data Source=" + Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "SQLite", "db_iflash.db") + " ;New=False;";
            var passPhrase = "iFlashDesktop";
            var decryp = decryptor.DecryptPassword(_proper.db_pass, passPhrase);
            myConnectionString = "Data Source=" + Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "SQLite", "db_iflash.db") + " ;New=False;";
            //loadGrid(true,"");

            if (txtSearch.Text.Trim().Length >= 1)
                loadGrid(false, txtSearch.Text);
            else
                loadGrid(true, txtSearch.Text);
        }

        void loadGrid(bool isAll, String sKey)
        {
            try
            {
                //String iQuery = "SELECT * FROM unit order by unitNo";
                String iQuery = "SELECT UnitNo, District, MaxTankCapacity FROM unit order by unitNo";
                if (!isAll)
                    //iQuery = $"SELECT * FROM unit where unitNo LIKE '%{sKey}%'  order by unitNo";
                    iQuery = $"SELECT UnitNo, District, MaxTankCapacity FROM unit where unitNo LIKE '%{sKey}%'  order by unitNo";

                SQLiteConnection conn = new SQLiteConnection(myConnectionString);
                SQLiteCommand cmd = new SQLiteCommand(iQuery, conn);

                if (conn.State != ConnectionState.Open) conn.Open();

                DataTable dataTable = new DataTable();
                SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);

                da.Fill(dataTable);

                dataGridView1.DataSource = dataTable;

                if (conn.State == ConnectionState.Open)
                    if (conn.State == ConnectionState.Open) conn.Close();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
                MessageBox.Show("Exception: Terjadi kesalahan saat load data, mohon cek log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(ex.Message, "loadGrid()");
            }
            
        }

        void save_error_log(string data, string actions)
        {
            try
            {
                DateTime localDate = DateTime.Now;
                var _item = new tbl_log();
                var initData = GlobalModel.GlobalVar;

                _item.pid = System.Guid.NewGuid().ToString();
                _item.actions = $"District {initData.DataEmp.DstrctCode}/FSearchUnit/{actions}";
                _item.message = $"{data}";
                _item.mod_date = localDate;
                _item.mod_by = initData.DataEmp.Nrp;

                SQLiteConnection conn;

                conn = new SQLiteConnection(myConnectionString);
                if (conn.State != ConnectionState.Open) conn.Open();
                string query = string.Empty;
                query = "INSERT INTO tbl_log(pid,actions,message,mod_date,mod_by)"
                      + " VALUES(@pid,@actions,@message,@mod_date,@mod_by)";

                SQLiteCommand cmd = new SQLiteCommand(query, conn);

                cmd.Parameters.Add(new SQLiteParameter("@pid", _item.pid));
                cmd.Parameters.Add(new SQLiteParameter("@actions", _item.actions));
                cmd.Parameters.Add(new SQLiteParameter("@message", _item.message));
                cmd.Parameters.Add(new SQLiteParameter("@mod_date", _item.mod_date));
                cmd.Parameters.Add(new SQLiteParameter("@mod_by", _item.mod_by));
                cmd.ExecuteNonQuery();
                if (conn.State == ConnectionState.Open) conn.Close();
            }
            catch (Exception)
            {
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if ( e.RowIndex >= 0)
            {
                idxCell = e.RowIndex;
                Console.WriteLine("Button on row {0} clicked", e.RowIndex);
            }
        }

        private void btnPilih_Click(object sender, EventArgs e)
        {           
            try
            {
                if (idxCell < 0)
                {
                    //MessageBox.Show("Klik unit yang akan di pilih", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dataGridView1.Rows[0].Selected = true;
                    string value = dataGridView1.Rows[0].Cells[0].FormattedValue.ToString();
                    string maxTankCapacity = dataGridView1.Rows[0].Cells[2].FormattedValue.ToString();
                    no_unit = value;
                    GlobalModel.AutoRefueling.preset_gross = int.Parse(maxTankCapacity);                   
                }
                else
                {

                    if (idxCell > dataGridView1.Rows.Count)
                    {
                        idxCell = dataGridView1.Rows.Count - 1;
                    }
                    string value = dataGridView1.Rows[idxCell].Cells[0].FormattedValue.ToString();
                    string maxTankCapacity = dataGridView1.Rows[idxCell].Cells[2].FormattedValue.ToString();
                    no_unit = value;
                    GlobalModel.AutoRefueling.preset_gross = int.Parse(maxTankCapacity);               
                }

                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                MessageBox.Show("Exception: Terjadi kesalahan saat pilih data, mohon cek log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(ex.Message, "btnPilih_Click()");
            }
        }

        private void txtSearch_KeyUp(object sender, KeyEventArgs e)
        {
            if(txtSearch.Text.Trim().Length >= 1) 
                loadGrid(false, txtSearch.Text); 
            else 
                loadGrid(true, txtSearch.Text); 
        }

        private void panel5_Paint(object sender, PaintEventArgs e)
        {

        }

        private void FSeacrhUnit_Activated(object sender, EventArgs e)
        {
            this.WindowState = System.Windows.Forms.FormWindowState.Normal;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
        }

        private void btnDT_Click(object sender, EventArgs e)
        {
            txtSearch.Text = "DT";
        }

        private void btnEX_Click(object sender, EventArgs e)
        {
            txtSearch.Text = "EX";
        }

        private void btnSH_Click(object sender, EventArgs e)
        {
            txtSearch.Text = "SH";
        }

        private void btnDR_Click(object sender, EventArgs e)
        {
            txtSearch.Text = "DR";
        }

        private void btnGS_Click(object sender, EventArgs e)
        {
            txtSearch.Text = "GS";
        }

        private void btnWL_Click(object sender, EventArgs e)
        {
            txtSearch.Text = "WL";
        }

        private void btnDZ_Click(object sender, EventArgs e)
        {
            txtSearch.Text = "DZ";
        }

        private void btnTL_Click(object sender, EventArgs e)
        {
            txtSearch.Text = "TL";
        }

        private void btnWP_Click(object sender, EventArgs e)
        {
            txtSearch.Text = "WP";
        }

        private void btnGR_Click(object sender, EventArgs e)
        {
            txtSearch.Text = "GR";
        }

        private void btnCP_Click(object sender, EventArgs e)
        {
            txtSearch.Text = "CP";
        }

        private void btnDel_Click(object sender, EventArgs e)
        {
            txtSearch.Text = "";
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            txtSearch.Text = txtSearch.Text + "1";
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            txtSearch.Text = txtSearch.Text + "2";
        }

        private void btn3_Click(object sender, EventArgs e)
        {
            txtSearch.Text = txtSearch.Text + "3";
        }

        private void btn4_Click(object sender, EventArgs e)
        {
            txtSearch.Text = txtSearch.Text + "4";
        }

        private void btn5_Click(object sender, EventArgs e)
        {
            txtSearch.Text = txtSearch.Text + "5";        }

        private void btn6_Click(object sender, EventArgs e)
        {
            txtSearch.Text = txtSearch.Text + "6";
        }

        private void btn7_Click(object sender, EventArgs e)
        {
            txtSearch.Text = txtSearch.Text + "7";
        }

        private void btn8_Click(object sender, EventArgs e)
        {
            txtSearch.Text = txtSearch.Text + "8";
        }

        private void btn9_Click(object sender, EventArgs e)
        {
            txtSearch.Text = txtSearch.Text + "9";
        }

        private void btn0_Click(object sender, EventArgs e)
        {
            txtSearch.Text = txtSearch.Text + "0";
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            if (txtSearch.Text.Trim().Length >= 1)
                loadGrid(false, txtSearch.Text);
            else
                loadGrid(true, txtSearch.Text);
        }

        private void dataGridView1_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            dataGridView1.BorderStyle = BorderStyle.None;
            dataGridView1.AlternatingRowsDefaultCellStyle.BackColor = Color.FromArgb(238, 239, 249);
            dataGridView1.CellBorderStyle = DataGridViewCellBorderStyle.SingleHorizontal;
            dataGridView1.DefaultCellStyle.SelectionBackColor = Color.SeaGreen;
            dataGridView1.DefaultCellStyle.SelectionForeColor = Color.WhiteSmoke;
            dataGridView1.BackgroundColor = Color.FromArgb(0, 101, 179);
            dataGridView1.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridView1.EnableHeadersVisualStyles = false;
            dataGridView1.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
            dataGridView1.ColumnHeadersDefaultCellStyle.Font = new Font("MS Reference Sans Serif", 20);
            dataGridView1.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(0, 101, 179);
            dataGridView1.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
        }

        private void txtSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ProcessStartInfo psStartInfo = new ProcessStartInfo();
                //psStartInfo.FileName = @"C:\Program Files\Common Files\microsoft shared\ink\TabTip.exe";
                var dir = Path.Combine(Environment.CurrentDirectory, "VKeyboard", "VKeyboard.exe");
                psStartInfo.FileName = dir;

                //Process ps = Process.Start(psStartInfo);

            }
            catch (Exception ex)
            {
                var error = ex.Message;
            }
        }
    }
}
