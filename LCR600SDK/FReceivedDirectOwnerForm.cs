﻿using Flurl.Http;
using LCR600SDK.DataClass;
using MySql.Data.MySqlClient;
using System.Data.SQLite;
using System.IO;
using System.Reflection;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data;

namespace LCR600SDK
{
    public partial class FReceivedDirectOwnerForm : Form
    {
        public ListReceivedDirectClass pData;
        private string myConnectionString = string.Empty;
        public string whosueSelected = string.Empty;        
        public string URL_API_CALL = string.Empty;
        public string CALL_FROM = string.Empty;
        SQLiteConnection conn;
        private string pUrl_service = string.Empty;
        public string pTblRef = string.Empty;
        public string pPidPO = string.Empty;
        public int PoQty;
        public string pReceivedAt = string.Empty;
        public string pSendFromId = string.Empty;
        public string pSendFromName = string.Empty;
        public string meterFaktor = string.Empty;
        private string pShiftStart1 = string.Empty;
        private string pShiftStart2 = string.Empty;
        private bool statusOnline = false;
        public int nomor_urut;
        PasswordEncrypDecrypt decryptor = new PasswordEncrypDecrypt();
        WriteLogText log = new WriteLogText();
        public FReceivedDirectOwnerForm()
        {
            var _proper = Properties.Settings.Default;
            var passPhrase = "iFlashDesktop";
            var decryp = decryptor.DecryptPassword(_proper.db_pass, passPhrase);
            myConnectionString = "Data Source=" + Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "SQLite", "db_iflash.db") + " ;New=False;";
            //myConnectionString = "Data Source=" + Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "SQLite", "db_iflash.db") + " ;New=False;";
            this.Text = "Receive Direct Owner Form" + GlobalModel.app_version;
            GlobalModel.AutoRefueling = new refueling();
            conn = new SQLiteConnection(myConnectionString);
            InitializeComponent();
        }

        private void FReceivedDirectForm_Load(object sender, EventArgs e)
        {
            var _proper = Properties.Settings.Default;
            statusOnline = _proper.connection == "ONLINE" ? true : false;
            pUrl_service = _proper.url_service;
            pData = new ListReceivedDirectClass();            
            initData();
            getLastSettingData(conn);
            string urut = "";
            if(nomor_urut>0 && nomor_urut< 10)
            {
                urut = "00" + nomor_urut.ToString();
            }else if(nomor_urut >=10 && nomor_urut < 100)
            {
                urut = "0" + nomor_urut.ToString();
            }else if (nomor_urut >= 100 && nomor_urut < 1000)
            {
                urut = nomor_urut.ToString();
            }

            //txtNomorSuratJalan.Text = DateTime.Now.ToString("ddMMyyyy") + getShift() + urut;

            var _globalData = GlobalModel.GlobalVar;
            var dstrctCode = _globalData.DataEmp.DstrctCode;
            if (txtReceivedQty.Text == "0")
            {
                btnSubmitReceive.Enabled = false;
            }

            getListWarehouseName(dstrctCode, conn);

            if (conn.State == ConnectionState.Open) conn.Close();
        }

        void getListWarehouseName(string dstrctCode, SQLiteConnection conn)
        {
            conn = new SQLiteConnection(myConnectionString);
            if (conn.State != ConnectionState.Open) conn.Open();
            string query = string.Empty;
            query = "SELECT DISTINCT WhouseLocation FROM flowmeter where DstrctCode = '" + dstrctCode + "'";
            SQLiteCommand cmd = new SQLiteCommand(query, conn);
            SQLiteDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                comboBoxWarehouse.Items.Add(reader["WhouseLocation"]);                
            }
            reader.Dispose();
            if (conn.State == ConnectionState.Open) conn.Close();
        }

        void getLastSettingData(SQLiteConnection conn)
        {


            if (conn.State != ConnectionState.Open) conn.Open();
            string query = string.Empty;
            query = "SELECT * FROM tbl_setting where Whouseke = 2 ORDER BY Mod_date DESC LIMIT 1";
            SQLiteCommand cmd = new SQLiteCommand(query, conn);
            SQLiteDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {                
                txtSnFlowMeter.Text = reader["SnCode"].ToString();
                meterFaktor = reader["MeterFakor"].ToString();
                txtWarehouse.Text = reader["WhouseName"].ToString();
                txtReceiveAtCode.Text = reader["Whouseid"].ToString();
            }
            reader.Dispose();
            if (conn.State == ConnectionState.Open) conn.Close();
        }

        void initData()
        {
            txtPoNo.Text = "";
            txtDistrik.Text = "";
            txtDensity.Text = "";
            txtTemperatur.Text = "";
            txtReceiveAtCode.Text = "";
            txtNomorSuratJalan.Text = "";
            txtSnFlowMeter.Text = "";            
            txtReceivedQty.Text = "0";                        
            txtReceivedfmAwal.Text = "0";
            txtReceivedfmAkhir.Text = "0";
            txtMulaiLoading.Text = "";
            txtSelesaiLoading.Text = "";
            txtWarehouse.Text = "";
        }

        
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
           LoadAsync();
        }

        async Task LoadAsync()
        {
            setData();
            //btnSubmitReceive.Enabled = true;
            var f = new FmManual();
            f.preset = int.Parse(tbQtyLoading.Text) * 10;

            f.trans_type_select = "RECV_DIRECT_OWNER";
            GlobalModel.AutoRefueling.trans_type_select = f.trans_type_select;
            f.waitCounterMax = 35 * 10;
            f.ListReceivedDirectClass = pData;
            if (f.ShowDialog() == DialogResult.OK)
            {
                txtReceivedQty.Text = Convert.ToString(f.quantity);
                txtReceivedfmAwal.Text = Convert.ToString(f.totalizer_awal);
                txtReceivedfmAkhir.Text = Convert.ToString(f.totalizer_akhir);
                txtMulaiLoading.Text = pData.start_loading;
                txtSelesaiLoading.Text = pData.end_loading;
                btnSubmitReceive.Enabled = true;
            }
            else
            {
                txtReceivedQty.Text = Convert.ToString(f.quantity);
                txtReceivedfmAwal.Text = Convert.ToString(f.totalizer_awal);
                txtReceivedfmAkhir.Text = Convert.ToString(f.totalizer_akhir);
                txtMulaiLoading.Text = pData.start_loading;
                txtSelesaiLoading.Text = pData.end_loading;
            }
        }

        string getShift()
        {
            string startHour = "";
            string endHour = "";
            String _shift = String.Empty;

            SQLiteConnection conn;
            conn = new SQLiteConnection(myConnectionString);
            if (conn.State != ConnectionState.Open) conn.Open();
            string query = string.Empty;

            query = "SELECT * FROM tbl_setting";
            SQLiteCommand cmds = new SQLiteCommand(query, conn);
            SQLiteDataReader reader = cmds.ExecuteReader();

            while (reader.Read())
            {
                pShiftStart1 = reader["StartShift1"].ToString();
                pShiftStart2 = reader["StartShift2"].ToString();
            }

            startHour = pShiftStart1;
            endHour = pShiftStart2;
            try
            {
                startHour = startHour.Split(':')[0];
                endHour = endHour.Split(':')[0];
            }
            catch (Exception)
            {
            }

            reader.Dispose();
            if (conn.State == ConnectionState.Open) conn.Close();

            pShiftStart1 = startHour;
            pShiftStart2 = endHour;

            if (DateTime.Now.Hour >= int.Parse(pShiftStart1) && DateTime.Now.Hour < int.Parse(pShiftStart2))
                _shift = "1";
            else
                _shift = "2";

            return _shift;
                        
        }

        void setData()
        {
            string startHour = "";
            string endHour = "";
            String _shift = String.Empty;

            SQLiteConnection conn;
            conn = new SQLiteConnection(myConnectionString);
            if (conn.State != ConnectionState.Open) conn.Open();
            string query = string.Empty;

            query = "SELECT * FROM tbl_setting";
            SQLiteCommand cmds = new SQLiteCommand(query, conn);
            SQLiteDataReader reader = cmds.ExecuteReader();

            while (reader.Read())
            {
                pShiftStart1 = reader["StartShift1"].ToString();
                pShiftStart2 = reader["StartShift2"].ToString();
            }

            startHour = pShiftStart1;
            endHour = pShiftStart2;
            try
            {
                startHour = startHour.Split(':')[0];
                endHour = endHour.Split(':')[0];
            }
            catch (Exception)
            {
            }

            reader.Dispose();
            if (conn.State == ConnectionState.Open) conn.Close();

            pShiftStart1 = startHour;
            pShiftStart2 = endHour;

            if (DateTime.Now.Hour >= int.Parse(pShiftStart1) && DateTime.Now.Hour < int.Parse(pShiftStart2))
                _shift = "1";
            else
                _shift = "2";

            var _date = DateTime.Now;

            if (DateTime.Now.Hour <= int.Parse(pShiftStart1))
                _date = _date.AddDays(-1);                        

            var _globalData = GlobalModel.GlobalVar;            
            pData.PidReceive = System.Guid.NewGuid().ToString();           
            pData.RefId = txtPoNo.Text;
            pData.PoNo = txtPoNo.Text;
            pData.District = txtDistrik.Text;                        
            pData.ReceiveQty = (int)Math.Round(double.Parse(txtReceivedQty.Text.Trim(), new System.Globalization.CultureInfo("id-ID")));                                    
            pData.ReceiveAt = txtWarehouse.Text;
            pData.ReceiveAtCode = txtReceiveAtCode.Text;
            pData.LastModBy = _globalData.DataEmp.Nrp;            
            pData.RefId = txtPoNo.Text;
            pData.ReceiveDensity = txtDensity.Text.Replace(',', '.');
            var temperature = txtTemperatur.Text.Replace(',', '.');            
            pData.ReceiveTemperature = Math.Round(Convert.ToDouble(temperature), 0, MidpointRounding.AwayFromZero).ToString();                        
            pData.ReceiveSnFlowMeter1 = txtSnFlowMeter.Text;
            pData.ReceiveMeterFaktor = meterFaktor.Replace(',', '.');
            pData.TotaliserAwal = (int)Math.Round(double.Parse(txtReceivedfmAwal.Text.Trim(), new System.Globalization.CultureInfo("id-ID")));// int.Parse(txtSendfmAwal.Text);
            pData.TotaliserAkhir = (int)Math.Round(double.Parse(txtReceivedfmAkhir.Text.Trim(), new System.Globalization.CultureInfo("id-ID")));// int.Parse(txtSendfmAkhir.Text);            
            pData.SirNo = txtNomorSuratJalan.Text;
            pData.ReceiveBy = _globalData.Gl[0].Name;
            pData.ReceiveDate = _date.Date;
            pData.LastModBy = _globalData.Gl[0].Nrp;
            pData.Foto = string.Empty;

            ////test
            //txtReceivedQty.Text = "10";
            //txtReceivedfmAwal.Text = "0";
            //txtReceivedfmAkhir.Text = "10";
            //pData.ReceiveQty = 10;
            //pData.ReceiveQtyPama = pData.ReceiveQty * (int)Math.Round(double.Parse(pData.ReceiveMeterFaktor.Replace('.', ','), new System.Globalization.CultureInfo("id-ID")));
            //pData.TotaliserAwal = 0;
            //pData.TotaliserAkhir = 10;
            //pData.start_loading = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            //pData.end_loading = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"); ;
        }

        bool isValid()
        {
            return true;
        }
        int submitDataToLocalSuccess()
        {
            try
            {
                var _item = pData;
                SQLiteConnection conn;

                conn = new SQLiteConnection(myConnectionString);
                if (conn.State != ConnectionState.Open) conn.Open();
                string query = string.Empty;
                int recs = 0;
                query = "INSERT INTO tbl_t_receive_direct_owner(pid_receive,district,ref_id,po_no,receive_at,receive_at_code,receive_qty,receive_sn_flow_meter1,receive_meter_faktor,receive_density,receive_temperature,receive_by,receive_date,sir_no,receive_qty_pama,totaliser_awal,totaliser_akhir,start_loading,end_loading,syncs)"
                      + " VALUES(@pid_receive,@district,@ref_id,@po_no,@receive_at,@receive_at_code,@receive_qty,@receive_sn_flow_meter1,@receive_meter_faktor,@receive_density,@receive_temperature,@receive_by,@receive_date,@sir_no,@receive_qty_pama,@totaliser_awal,@totaliser_akhir,@start_loading,@end_loading,@syncs)";

                SQLiteCommand cmd = new SQLiteCommand(query, conn);

                cmd.Parameters.Add(new SQLiteParameter("@pid_receive", _item.PidReceive));
                cmd.Parameters.Add(new SQLiteParameter("@district", _item.District));
                cmd.Parameters.Add(new SQLiteParameter("@ref_id", _item.RefId));
                cmd.Parameters.Add(new SQLiteParameter("@po_no", _item.PoNo));
                cmd.Parameters.Add(new SQLiteParameter("@receive_at", _item.ReceiveAt));
                cmd.Parameters.Add(new SQLiteParameter("@receive_at_code", _item.ReceiveAtCode));
                cmd.Parameters.Add(new SQLiteParameter("@receive_qty", _item.ReceiveQty));
                cmd.Parameters.Add(new SQLiteParameter("@receive_sn_flow_meter1", _item.ReceiveSnFlowMeter1));
                cmd.Parameters.Add(new SQLiteParameter("@receive_meter_faktor", _item.ReceiveMeterFaktor));
                cmd.Parameters.Add(new SQLiteParameter("@receive_density", _item.ReceiveDensity));
                cmd.Parameters.Add(new SQLiteParameter("@receive_temperature", _item.ReceiveTemperature));
                cmd.Parameters.Add(new SQLiteParameter("@receive_by", _item.ReceiveBy));
                cmd.Parameters.Add(new SQLiteParameter("@receive_date", _item.ReceiveDate));
                cmd.Parameters.Add(new SQLiteParameter("@sir_no", _item.SirNo));
                cmd.Parameters.Add(new SQLiteParameter("@receive_qty_pama", _item.ReceiveQtyPama));
                cmd.Parameters.Add(new SQLiteParameter("@totaliser_awal", _item.TotaliserAwal));
                cmd.Parameters.Add(new SQLiteParameter("@totaliser_akhir", _item.TotaliserAkhir));
                cmd.Parameters.Add(new SQLiteParameter("@start_loading", _item.start_loading));
                cmd.Parameters.Add(new SQLiteParameter("@end_loading", _item.end_loading));
                cmd.Parameters.Add(new SQLiteParameter("@syncs", 7));

                recs = cmd.ExecuteNonQuery();


                if (conn.State == ConnectionState.Open) conn.Close();
                return recs;
            }
            catch (Exception ex)
            {
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(ex.ToString(), "submitDataToLocal()");
                FileToExcel("receive_direct");
                MessageBox.Show("Data Tidak Tersimpan, Silahkan tulis di logsheet manual", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return -1;
            }


        }
        int submitDataToLocal()
        {
            try
            {
                var _item = pData;                
                SQLiteConnection conn;

                conn = new SQLiteConnection(myConnectionString);
                if (conn.State != ConnectionState.Open) conn.Open();
                string query = string.Empty;
                int recs = 0;
                query = "INSERT INTO tbl_t_receive_direct_owner(pid_receive,district,ref_id,po_no,receive_at,receive_at_code,receive_qty,receive_sn_flow_meter1,receive_meter_faktor,receive_density,receive_temperature,receive_by,receive_date,sir_no,receive_qty_pama,totaliser_awal,totaliser_akhir,start_loading,end_loading,syncs)"
                      + " VALUES(@pid_receive,@district,@ref_id,@po_no,@receive_at,@receive_at_code,@receive_qty,@receive_sn_flow_meter1,@receive_meter_faktor,@receive_density,@receive_temperature,@receive_by,@receive_date,@sir_no,@receive_qty_pama,@totaliser_awal,@totaliser_akhir,@start_loading,@end_loading,@syncs)";

                SQLiteCommand cmd = new SQLiteCommand(query, conn);

                cmd.Parameters.Add(new SQLiteParameter("@pid_receive", _item.PidReceive));                               
                cmd.Parameters.Add(new SQLiteParameter("@district", _item.District));
                cmd.Parameters.Add(new SQLiteParameter("@ref_id", _item.RefId));
                cmd.Parameters.Add(new SQLiteParameter("@po_no", _item.PoNo));                
                cmd.Parameters.Add(new SQLiteParameter("@receive_at", _item.ReceiveAt));
                cmd.Parameters.Add(new SQLiteParameter("@receive_at_code", _item.ReceiveAtCode));
                cmd.Parameters.Add(new SQLiteParameter("@receive_qty", _item.ReceiveQty));
                cmd.Parameters.Add(new SQLiteParameter("@receive_sn_flow_meter1", _item.ReceiveSnFlowMeter1));
                cmd.Parameters.Add(new SQLiteParameter("@receive_meter_faktor", _item.ReceiveMeterFaktor));
                cmd.Parameters.Add(new SQLiteParameter("@receive_density", _item.ReceiveDensity));
                cmd.Parameters.Add(new SQLiteParameter("@receive_temperature", _item.ReceiveTemperature));
                cmd.Parameters.Add(new SQLiteParameter("@receive_by", _item.ReceiveBy));
                cmd.Parameters.Add(new SQLiteParameter("@receive_date", _item.ReceiveDate));                                                
                cmd.Parameters.Add(new SQLiteParameter("@sir_no", _item.SirNo));
                cmd.Parameters.Add(new SQLiteParameter("@receive_qty_pama", _item.ReceiveQtyPama));                
                cmd.Parameters.Add(new SQLiteParameter("@totaliser_awal", _item.TotaliserAwal));
                cmd.Parameters.Add(new SQLiteParameter("@totaliser_akhir", _item.TotaliserAkhir));
                cmd.Parameters.Add(new SQLiteParameter("@start_loading", _item.start_loading));
                cmd.Parameters.Add(new SQLiteParameter("@end_loading", _item.end_loading));
                cmd.Parameters.Add(new SQLiteParameter("@syncs", 2));

                recs = cmd.ExecuteNonQuery();


                if (conn.State == ConnectionState.Open) conn.Close();
                return recs;
            }
            catch (Exception ex)
            {
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(ex.ToString(), "submitDataToLocal()");
                FileToExcel("receive_direct");
                MessageBox.Show("Data Tidak Tersimpan, Silahkan tulis di logsheet manual", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return -1;
            }
            

        }
        public void FileToExcel(string name)
        {
            StringBuilder sb = new StringBuilder();
            var filename = "log_transaksi_" + name + DateTime.Now.ToString("yyyyMMdd") + ".csv";
            //cek direktori jika tidak ada maka dibuat direktori baru
            if (!Directory.Exists(Path.Combine(Environment.CurrentDirectory, "log_transaksi_eror_simpan_db")))
            {
                Directory.CreateDirectory(Path.Combine(Environment.CurrentDirectory, "log_transaksi_eror_simpan_db"));
            }
            var pathExcel = Path.Combine(Environment.CurrentDirectory, "log_transaksi_eror_simpan_db", filename);
            //cek file jika tidak ada maka buat header baru
            if (!File.Exists(pathExcel))
            {
                sb.Append("pid_receive;ref_tbl;ref_id;district;po_no;receive_from;receive_at;receive_at_code;receive_qty;receive_sn_flow_meter1;receive_meter_faktor;receive_density;receive_temperature;receive_by;receive_date;foto;is_valid;is_complete_po;last_mod_by;last_mod_date;oc_flag_loading_mse1r1;oc_loading_remarks_mse1r1;oc_loading_datetime_mse1r1;sir_no;receive_qty_pama;receive_date_exit;totaliser_awal;totaliser_akhir;start_loading;end_loading;\r\n");                
            }
            var item = pData;
            sb.Append(item.PidReceive + ";");
            sb.Append(item.RefTbl + ";");
            sb.Append(item.RefId + ";");
            sb.Append(item.District + ";");
            sb.Append(item.PoNo + ";");
            sb.Append(item.ReceiveForm + ";");
            sb.Append(item.ReceiveAt + ";");
            sb.Append(item.ReceiveAtCode + ";");
            sb.Append(item.ReceiveQty + ";");
            sb.Append(item.ReceiveSnFlowMeter1 + ";");
            sb.Append(item.ReceiveMeterFaktor + ";");
            sb.Append(item.ReceiveDensity + ";");
            sb.Append(item.ReceiveTemperature + ";");
            sb.Append(item.ReceiveBy + ";");
            sb.Append(item.ReceiveDate + ";");
            sb.Append(item.Foto + ";");
            sb.Append(item.IsValid + ";");
            sb.Append(item.IsCOmpletePo + ";");
            sb.Append(item.LastModBy + ";");
            sb.Append(item.LastModDate + ";");
            sb.Append(item.OcFlaghLoadingMse1r1 + ";");
            sb.Append(item.OcLoadingRemarksMse1r1 + ";");
            sb.Append(item.OcLoadingDateTimeMse1r1 + ";");
            sb.Append(item.SirNo + ";");
            sb.Append(item.ReceiveQtyPama + ";");
            sb.Append(item.ReceiveDateExit + ";");
            sb.Append(item.TotaliserAwal + ";");
            sb.Append(item.TotaliserAkhir + ";");
            sb.Append(item.start_loading + ";");
            sb.Append(item.end_loading + ";");
            sb.Append("\r\n");
           


            if (File.Exists(pathExcel))
            {
                File.AppendAllText(pathExcel, sb.ToString() + Environment.NewLine);
            }
            else
            {
                File.WriteAllText(pathExcel, sb.ToString());
            }

        }
        async void submitData()
        {
            var _auth = GlobalModel.loginModel;
            String _url = $"{pUrl_service}api/FuelService/submitReceiveDirectDesktop";

            try
            {                
                returnValue data = await _url.WithHeaders(new { Authorization = _auth.token })
                       .PostJsonAsync(pData).ReceiveJson<returnValue>();

                if (data.status)
                {                                        
                    MessageBox.Show("Data Submitted", MessageBoxButtons.OK.ToString());
                    submitDataToLocalSuccess();
                    _backToCallForm();
                    this.Close();
                }
                else
                {
                    MessageBox.Show(data.remarks, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    log.WriteToFile("logLCR600.txt", data.remarks);                                        
                    save_error_log(data.remarks, "submitData");
                    MessageBox.Show("Menyimpan data transaksi ke local", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    var res = submitDataToLocal();
                    if (res == 1)
                    {
                        var f = new FPopUpNotification("Berhasil menyimpan data pada lokal!", "Simpan Data Receive Direct");
                        f.ShowDialog();
                        this.Close();
                    }
                }
                    

            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception: Terjadi kesalahan saat submit data mohon dicek logLCR600.txt", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", ex.ToString());
                save_error_log(ex.ToString(), "submitData()");
                MessageBox.Show("Menyimpan data transaksi ke local", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                var res = submitDataToLocal();
                if (res == 1)
                {
                    var f = new FPopUpNotification("Berhasil menyimpan data pada lokal!", "Simpan Data Receive Direct");
                    f.ShowDialog();
                    this.Close();
                }
            }
        }
        void save_error_log(string data, string actions)
        {
            try
            {
                DateTime localDate = DateTime.Now;
                var _item = new tbl_log();
                var initData = GlobalModel.GlobalVar;

                _item.pid = System.Guid.NewGuid().ToString();
                _item.actions = $"District {initData.DataEmp.DstrctCode}/FReceivedDirectForm/{actions}";
                _item.message = $"{data}";
                _item.mod_date = localDate;
                _item.mod_by = initData.DataEmp.Nrp;

                SQLiteConnection conn;

                conn = new SQLiteConnection(myConnectionString);
                if (conn.State != ConnectionState.Open) conn.Open();
                string query = string.Empty;
                query = "INSERT INTO tbl_log(pid,actions,message,mod_date,mod_by)"
                      + " VALUES(@pid,@actions,@message,@mod_date,@mod_by)";

                SQLiteCommand cmd = new SQLiteCommand(query, conn);

                cmd.Parameters.Add(new SQLiteParameter("@pid", _item.pid));
                cmd.Parameters.Add(new SQLiteParameter("@actions", _item.actions));
                cmd.Parameters.Add(new SQLiteParameter("@message", _item.message));
                cmd.Parameters.Add(new SQLiteParameter("@mod_date", _item.mod_date));
                cmd.Parameters.Add(new SQLiteParameter("@mod_by", _item.mod_by));
                cmd.ExecuteNonQuery();
                if (conn.State == ConnectionState.Open) conn.Close();
            }
            catch (Exception)
            {
            }
        }

        public void _backToCallForm()
        {
            switch (CALL_FROM)
            {
                case "RECV_DARAT":
                    if (Application.OpenForms["FReceivedFTDaratSub"] != null)
                        (Application.OpenForms["FReceivedFTDaratSub"] as FReceivedFTDaratSub).loadGridAsync(URL_API_CALL);
                    break;
                case "RECV_PORT":
                    if (Application.OpenForms["FReceivedFTPort"] != null)
                        (Application.OpenForms["FReceivedFTPort"] as FReceivedFTPort).loadGridAsync(URL_API_CALL);
                    break;
                case "SEND_PORT":
                    if (Application.OpenForms["FSendFTPort"] != null)
                        (Application.OpenForms["FSendFTPort"] as FSendFTPort).loadGridAsync(URL_API_CALL);
                    break;
                case "RECV_DIRECT":
                    if (Application.OpenForms["FReceivedDirect"] != null)
                        (Application.OpenForms["FReceivedDirect"] as FReceivedDirect).loadGridAsync(URL_API_CALL);
                    break;
                case "RECV_DIRECT_OWNER":
                    if (Application.OpenForms["FReceivedDirectOwner"] != null)
                        (Application.OpenForms["FReceivedDirectOwner"] as FReceivedDirectOwner).loadGridAsync(URL_API_CALL);
                    break;
                    
            }

            this.Close();
        }

      

        private void btnSendFrom_Click(object sender, EventArgs e)
        {
            if (txtDistrik.Text.Trim().Length == 0)
            {
                MessageBox.Show("PO belum di pilih", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                FRefSendPortFrom f = new FRefSendPortFrom();
                f.pPO_NO = txtPoNo.Text;
                f.ShowDialog();
            }
        }

        private void btnSendTo_Click(object sender, EventArgs e)
        {
            if (txtDistrik.Text.Trim().Length == 0)
            {
                MessageBox.Show("PO belum di pilih", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                FRefSendPortTo f = new FRefSendPortTo();
                f.pDistrik = txtDistrik.Text;
                f.ShowDialog();
            }
        }

        private void btnSearchPo_Click(object sender, EventArgs e)
        {
            var f = new FRefPODirect();
            f.ShowDialog();
        }
        

        public void callKeyboardVirtual()
        {
            try
            {
                ProcessStartInfo psStartInfo = new ProcessStartInfo();
                //psStartInfo.FileName = @"C:\Program Files\Common Files\microsoft shared\ink\TabTip.exe";
                var dir = Path.Combine(Environment.CurrentDirectory, "VKeyboard", "VKeyboard.exe");
                psStartInfo.FileName = dir;

                Process ps = Process.Start(psStartInfo);

            }
            catch (Exception ex)
            {
                var error = ex.Message;
            }
        }
        private int timeoutCheckConnection = 15000;
        public async Task<string> checkConnectivity()
        {
            try
            {
                WebClient requestCek = new WebClient();
                var str = requestCek.DownloadString(pUrl_service);
                return str;
            }
            catch (WebException ex)
            {
                return "";
            }
        }

        private async void btnSubmitReceive_Click(object sender, EventArgs e)
        {
            try
            {                                               
                var checkconnect = await checkConnectivity();
                if (checkconnect.Contains("Supply Service"))
                {
                    submitData();                    
                }
                else
                {
                    var res = submitDataToLocal();
                    if (res == 1)
                    {
                        var f = new FPopUpNotification("Berhasil menyimpan data pada lokal!", "Simpan Data Receive Direct");
                        f.ShowDialog();
                        this.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
                MessageBox.Show("Exception: Terjadi kesalahan saat submit, mohon cek log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(ex.Message, "btnSubmitReceive_Click()");
            }

        }

        private void comboBoxWarehouse_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {                                
                SQLiteConnection conn;

                conn = new SQLiteConnection(myConnectionString);
                if (conn.State != ConnectionState.Open) conn.Open();
                string query = string.Empty;

                string selectedItem = this.comboBoxWarehouse.GetItemText(this.comboBoxWarehouse.SelectedItem);

                query = "SELECT DISTINCT Warehouse FROM flowmeter where WhouseLocation = '" + selectedItem + "'";
                SQLiteCommand cmds = new SQLiteCommand(query, conn);
                SQLiteDataReader readers = cmds.ExecuteReader();
                while (readers.Read())
                {
                    txtReceiveAtCode.Text = readers["Warehouse"].ToString();
                }
                
                if (conn.State == ConnectionState.Open) conn.Close();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
                MessageBox.Show("Exception: Terjadi kesalahan saat pilih data warehouse, mohon cek log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(ex.Message, "comboBoxWarehouse_SelectedIndexChanged()");
            }
        }

        private void txtDensity_Click(object sender, EventArgs e)
        {
            callKeyboardVirtual();
        }

        private void txtTemperatur_Click(object sender, EventArgs e)
        {
            callKeyboardVirtual();
        }

        private void txtNomorSuratJalan_Click(object sender, EventArgs e)
        {
            callKeyboardVirtual();
        }

        private void tbQtyLoading_Click(object sender, EventArgs e)
        {
            callKeyboardVirtual();
        }
    }
}
