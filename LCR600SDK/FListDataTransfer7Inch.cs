﻿using Flurl.Http;
using MySql.Data.MySqlClient; using System.Data.SQLite; using System.IO; using System.Reflection;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows.Forms;

namespace LCR600SDK
{
    public partial class FListDataTransfer7Inch : Form
    {
        private string myConnectionString = string.Empty;
        private string pUrl_service= string.Empty;
        private bool statusOnline = false;
        PasswordEncrypDecrypt decryptor = new PasswordEncrypDecrypt();
        WriteLogText log = new WriteLogText();
        public FListDataTransfer7Inch()
        {
            InitializeComponent();
        }

        private void FListData_Load(object sender, EventArgs e)
        {
            var _proper = Properties.Settings.Default;
            pUrl_service = _proper.url_service;
            statusOnline = _proper.connection == "ONLINE" ? true : false;
            //myConnectionString = "Data Source=" + Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "SQLite", "db_iflash.db") + " ;New=False;";
            var passPhrase = "iFlashDesktop";
            var decryp = decryptor.DecryptPassword(_proper.db_pass, passPhrase);
            myConnectionString = "Data Source=" + Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "SQLite", "db_iflash.db") + " ;New=False;";
            dateTimePicker2.Text = DateTime.Now.ToString("yyyy-MM-dd");
            dateTimePicker1.Text = DateTime.Now.ToString("yyyy-MM-dd");
            loadGridSearch(true, "");
            loadGrid();

            timerSyncTranfer.Stop();
        }
        
        void dataGridView1_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            dataGridView1.BorderStyle = BorderStyle.None;
            dataGridView1.AlternatingRowsDefaultCellStyle.BackColor = Color.FromArgb(238, 239, 249);
            dataGridView1.CellBorderStyle = DataGridViewCellBorderStyle.SingleHorizontal;
            dataGridView1.DefaultCellStyle.SelectionBackColor = Color.SeaGreen;
            dataGridView1.DefaultCellStyle.SelectionForeColor = Color.WhiteSmoke;
            dataGridView1.BackgroundColor = Color.FromArgb(0, 101, 179);
            dataGridView1.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridView1.EnableHeadersVisualStyles = false;
            dataGridView1.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
            dataGridView1.ColumnHeadersDefaultCellStyle.Font = new Font("MS Reference Sans Serif", 20);
            dataGridView1.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(0, 101, 179);
            dataGridView1.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;

            
            vScrollBar1.Visible = true;
            hScrollBar1.Visible = true;            

            int totalwidth = dataGridView1.RowHeadersWidth + 1;

            for (int i = 0; i < dataGridView1.Columns.Count; i++)
            {
                totalwidth += dataGridView1.Columns[i].Width;
            }

            hScrollBar1.Maximum = totalwidth;
            hScrollBar1.LargeChange = dataGridView1.Width;
            hScrollBar1.SmallChange = dataGridView1.Columns[0].Width;

            vScrollBar1.Maximum = dataGridView1.RowCount;
        }
        void loadGridSearch(bool isAll, String sKey)
        {
            try
            {
                String iQuery = querySql();

                SQLiteConnection conn = new SQLiteConnection(myConnectionString);
                SQLiteCommand cmd = new SQLiteCommand(iQuery, conn);

                if (conn.State != ConnectionState.Open) conn.Open();

                DataTable dataTable = new DataTable();
                SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);

                da.Fill(dataTable);

                dataGridView1.DataSource = dataTable;                
                if (conn.State == ConnectionState.Open)
                    if (conn.State == ConnectionState.Open) conn.Close();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
                MessageBox.Show("Exception: Terjadi kesalahan pada saat search data, mohon periksa log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(ex.Message, "loadGridSearch()");
            }
        }
        public void loadGrid()
        {
            SQLiteConnection conn = new SQLiteConnection(myConnectionString);            
            SQLiteCommand cmd = new SQLiteCommand(querySql(), conn);

            if (conn.State != ConnectionState.Open) conn.Open();

            DataTable dataTable = new DataTable();
            SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);

            da.Fill(dataTable);            
            dataGridView1.DataSource = dataTable; 
            

            if (conn.State == ConnectionState.Open)
                if (conn.State == ConnectionState.Open) conn.Close();
        }
        string querySql()
        {
            var sql = " SELECT sync_desc," +
               "(substring(xfer_date,1,4) || '-' || substring(xfer_date,5,2) || '-' || substring(xfer_date,7,2)) as transfer_date" +
               ",dstrct_code,issued_whouse,received_whouse,qty_xfer,shift,fmeter_begin,fmeter_end,xfer_code,employee_id ";
            sql += " FROM vw_whtranfer  ";
            sql += " WHERE xfer_date between  '" + dateTimePicker1.Value.ToString("yyyyMMdd") + "' and '" + dateTimePicker2.Value.ToString("yyyyMMdd") + "' and shift ='" + cbShift.Text + "' ";
            sql += " order BY case when syncs = 2 then 1 ";
            sql += " when syncs = 9 then 2 ";
            sql += " when syncs = 3 then 3  ";
            sql += " when syncs = 7 then 4 ";
            sql += " END asc ";
            sql += " , xfer_date asc ";
            return sql;
            return sql;
        }
        string querySqlSync()
        {
            var sql = " SELECT * ";
            sql += " FROM whtranfer  ";
            sql += " WHERE syncs = 3 and xfer_date between  '" + dateTimePicker1.Value.ToString("yyyyMMdd") + "' and '" + dateTimePicker2.Value.ToString("yyyyMMdd") + "' and shift ='" + cbShift.Text + "' ";
            sql += " order BY case when syncs = 2 then 1 ";
            sql += " when syncs = 9 then 2 ";
            sql += " when syncs = 3 then 3  ";
            sql += " when syncs = 7 then 4 ";
            sql += " END asc ";
            sql += " , xfer_date asc ";
            return sql;
        }
        void btnSyncWait()
        {                        
            btnSync.Enabled = false;
            btnSync.Enabled = false;
            btnSync.Visible = false;
            btnPleaseWait.Enabled = false;
            btnPleaseWait.Visible = true;
        }
        void btnSyncEnabled()
        {
            btnSync.Enabled = true;
            btnSync.Visible = true;
            btnPleaseWait.Visible = false;
            btnPleaseWait.Enabled = false;
        }
        private void timerSyncTranfer_Tick(object sender, EventArgs e)
        {
            btnSyncWait();
        }
        public async Task<string> checkConnectivity()
        {
            try
            {
                WebClient requestCek = new WebClient();
                var str = requestCek.DownloadString(pUrl_service);
                return str;
            }
            catch (WebException ex)
            {
                return "";
            }
        }
        private async void btnSync_Click(object sender, EventArgs e)
        {

            try
            {
                var checkconnect = await checkConnectivity();
                if (checkconnect.Contains("Supply Service"))
                //if (statusOnline)
                {
                    onSyncAsync();
                    
                    //loadGrid();
                }
                else
                {
                    var f = new FPopUpNotification("Status saat ini OFFLINE mohon untuk sambungkan jaringan dan ganti setting connection menjadi ONLINE \r\n Setting > User Setting > Connection > Save", "Failed To Connect");
                    f.ShowDialog();
                }

            }            
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message.ToString());
                MessageBox.Show("Exception: Terjadi kesalahan pada saat sync data, mohon periksa log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(ex.Message, "btnSync_Click()");
            }
        }
        async Task onSyncAsync()
        {
            try
            {
                var _proper = Properties.Settings.Default;
                var _auth = GlobalModel.loginModel;
                String _url = $"{pUrl_service}api/IssuingService/SyncTransfer";
                List<whtranfer> SendListTransfer = new List<whtranfer>();
                var getData = getListTransfer();
                //var total = getData.Count();
                //double howMany = double.Parse(total.ToString()) / 10.0;
                //var counter = howMany.ToString().Split('.');
                //if (counter.Count() == 1)
                //{
                //    counter = howMany.ToString().Split(',');
                //}

                var listFailedJobRow = new List<List<string>>();
                if (getData.Count == 0)
                {
                    timerSyncTranfer.Stop();
                    btnSyncEnabled();
                }
                else
                {
                    lblKirimData.Text = string.Format("{0} data dari {1} data", 0, getData.Count);
                    for (int i = 0; i < getData.Count; i++)
                    {

                        SendListTransfer.Add(getData[i]);
                        string jsonConvert = Newtonsoft.Json.JsonConvert.SerializeObject(SendListTransfer);
                        var json = new JavaScriptSerializer().Serialize(SendListTransfer);
                        //if (SendListTransfer.Count == 1 /*&& int.Parse(counter[0]) != 0*/)
                        if (SendListTransfer.Count == int.Parse(_proper.data_per_kirim) /*&& int.Parse(counter[0]) != 0*/)
                        {                               
                            ClsIssuingTransfer data = await _url.WithHeaders(new { Authorization = _auth.token }).PostJsonAsync(SendListTransfer).ReceiveJson<ClsIssuingTransfer>();
                            if (data.status)
                            {
                                btnPleaseWait.Text = string.Format("Please Wait. {0} data dari {1} data", (i + 1), getData.Count);
                                updateTableSync(SendListTransfer, data);
                                loadGrid();
                                SendListTransfer = new List<whtranfer>();
                            }
                            else
                            {
                                save_error_log(data.remarks, "onSyncAsync()");
                            }
                            //lblKirimData.Text = string.Format("{0} data dari {1} data", (i + 1), getData.Count);
                            //updateTableSync(SendListTransfer, data);
                            //loadGrid();
                            //SendListTransfer = new List<whtranfer>();
                        }
                        else
                        {
                            if (i == getData.Count - 1 /*&& SendListTransfer.Count == int.Parse(counter[1])*/)
                            {
                                ClsIssuingTransfer data = await _url.WithHeaders(new { Authorization = _auth.token }).PostJsonAsync(SendListTransfer).ReceiveJson<ClsIssuingTransfer>();
                                if (data.status)
                                {
                                    btnPleaseWait.Text = string.Format("Please Wait. {0} data dari {1} data", (i + 1), getData.Count);
                                    updateTableSync(SendListTransfer, data);
                                    loadGrid();
                                }
                                else
                                {
                                    save_error_log(data.remarks, "onSyncAsync()");
                                }
                                //lblKirimData.Text = string.Format("{0} data dari {1} data", (i + 1), getData.Count);                                
                                //updateTableSync(SendListTransfer, data);
                                //loadGrid();
                            }
                        }
                        if (i == getData.Count - 1)
                        {
                            timerSyncTranfer.Stop();
                            btnSyncEnabled();
                        }
                    }
                }
           
                
                //String _url = $"{pUrl_service}api/IssuingService/SyncTransfer";
                //ClsIssuingTransfer data = await _url.WithHeaders(new { Authorization = _auth.token }).PostJsonAsync(getListTransfer()).ReceiveJson<ClsIssuingTransfer>();
                //timerSyncTranfer.Stop();
                //btnSyncEnabled();
                //save_error_log(data.remarks, "onSyncAsync()");
                //updateTableSync(data);
                //loadGrid();
            }
            catch (FlurlHttpException ex)
            {
                var resp = ex.Message;
                if (ex.InnerException != null)
                {
                    resp = ex.InnerException.Message;
                }
                MessageBox.Show(resp);
                save_error_log(resp, "onSyncAsync()");
                timerSyncTranfer.Stop();
                btnSyncEnabled();
            }

        }
        void save_error_log(string data, string actions)
        {
            try
            {                
                DateTime localDate = DateTime.Now;
                var _item = new tbl_log();
                var initData = GlobalModel.GlobalVar;

                _item.pid = System.Guid.NewGuid().ToString();
                _item.actions = $"District {initData.DataEmp.DstrctCode}/FListDataTransfer7Inch/{actions}";
                _item.message = $"{data}";
                _item.mod_date = localDate;
                _item.mod_by = initData.DataEmp.Nrp;

                SQLiteConnection conn;

                conn = new SQLiteConnection(myConnectionString);
                if (conn.State != ConnectionState.Open) conn.Open();
                string query = string.Empty;
                query = "INSERT INTO tbl_log(pid,actions,message,mod_date,mod_by)"
                      + " VALUES(@pid,@actions,@message,@mod_date,@mod_by)";

                SQLiteCommand cmd = new SQLiteCommand(query, conn);

                cmd.Parameters.Add(new SQLiteParameter("@pid", _item.pid));
                cmd.Parameters.Add(new SQLiteParameter("@actions", _item.actions));
                cmd.Parameters.Add(new SQLiteParameter("@message", _item.message));
                cmd.Parameters.Add(new SQLiteParameter("@mod_date", _item.mod_date));
                cmd.Parameters.Add(new SQLiteParameter("@mod_by", _item.mod_by));
                cmd.ExecuteNonQuery();
                if (conn.State == ConnectionState.Open) conn.Close();
            }
            catch (Exception)
            {
            }
        }
        List<whtranfer> getListTransfer()
        {
            SQLiteConnection conn = new SQLiteConnection(myConnectionString);
            try
            {
                List<whtranfer> _list = new List<whtranfer>();
                
                if (conn.State != ConnectionState.Open) conn.Open();
                string query = string.Empty;

                try
                {
                    query = $"update whtranfer set syncs = 3 where syncs in(2,9)";
                    var recs = execCmd(query, conn);
                    loadGrid();
                    timerSyncTranfer.Start();
                }
                catch (SQLiteException ex)
                {
                    //MessageBox.Show(ex.ToString());
                    MessageBox.Show("Exception: Terjadi kesalahan pada saat get data transfer local, mohon periksa log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    log.WriteToFile("logLCR600.txt", ex.Message);
                    save_error_log(ex.Message, "getListTransfer()");
                }

                //query = "select * from whtranfer where syncs = 3 ORDER BY created_date asc";
                query = querySqlSync();

                SQLiteCommand cmd = new SQLiteCommand(query, conn);
                SQLiteDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var cls = new whtranfer();

                    cls.job_row_id = reader["job_row_id"].ToString();
                    cls.xfer_code = reader["xfer_code"].ToString();
                    cls.xfer_date = reader["xfer_date"].ToString();
                    cls.dstrct_code = reader["dstrct_code"].ToString();
                    cls.employee_id = reader["employee_id"].ToString();
                    cls.issued_whouse = reader["issued_whouse"].ToString();
                    cls.received_whouse = reader["received_whouse"].ToString();
                    cls.stock_code = reader["stock_code"].ToString();
                    cls.qty_xfer = float.Parse(reader["qty_xfer"].ToString());
                    cls.shift = reader["shift"].ToString();
                    cls.fmeter_serial_no = reader["fmeter_serial_no"].ToString();
                    cls.faktor_meter = float.Parse(reader["faktor_meter"].ToString());
                    cls.fmeter_begin = float.Parse(reader["fmeter_begin"].ToString());
                    cls.fmeter_end = float.Parse(reader["fmeter_end"].ToString());
                    cls.created_by = reader["created_by"].ToString();
                    cls.xfer_by = reader["xfer_by"].ToString();
                    cls.received_by = reader["received_by"].ToString();
                    cls.issued_whouse_type = reader["issued_whouse_type"].ToString();
                    cls.received_whouse_type = reader["received_whouse_type"].ToString();
                    cls.mod_date = Convert.ToDateTime(reader["mod_date"].ToString());
                    cls.created_date = Convert.ToDateTime(reader["created_date"].ToString());
                    cls.timezone = reader["timezone"].ToString();
                    cls.syncs = int.Parse(reader["syncs"].ToString());

                    _list.Add(cls);
                }
                reader.Dispose();
                if (conn.State == ConnectionState.Open) conn.Close();

                return _list;

            }
            catch(SQLiteException ex)
            {
                if (conn.State == ConnectionState.Open) conn.Close();
                //MessageBox.Show(ex.Message);
                MessageBox.Show("Exception: Terjadi kesalahan pada saat get data transfer local, mohon periksa log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(ex.Message, "getListTransfer()");
                return null;
            }
            
        }

        void updateTableSync(List<whtranfer> SendListTransfer, ClsIssuingTransfer sClsIssuing)
        {
            SQLiteConnection conn;
            int recs = 0;
            string query = string.Empty;
            conn = new SQLiteConnection(myConnectionString);
            if (conn.State != ConnectionState.Open) conn.Open();

            string send_pid = String.Join("','", SendListTransfer.Select(x => x.job_row_id));
            if (sClsIssuing.status)
            {
                query = $"update whtranfer set syncs = 7 where job_row_id in('{send_pid}') and syncs=3";
                recs = execCmd(query, conn);
            }         
            if (conn.State == ConnectionState.Open) conn.Close();

            if (Application.OpenForms["FMain"] != null)
            {
                (Application.OpenForms["FMain"] as FMain).toolStripStatusLabelStatus.Text = "Sync finish";
            }
        }
        int execCmd(String sQuery, SQLiteConnection sConn)
        {
            int iReturn = 0;

            var cmd = new SQLiteCommand(sQuery, sConn);
            iReturn = cmd.ExecuteNonQuery();

            return iReturn;
        }
        void changeFilter()
        {
            DateTime fromdate = Convert.ToDateTime(dateTimePicker1.Text);
            DateTime todate = Convert.ToDateTime(dateTimePicker2.Text);

            if (fromdate <= todate)
            {
                TimeSpan ts = todate.Subtract(fromdate);
                int days = Convert.ToInt16(ts.Days);

                try
                {
                    String iQuery = querySql();
                    SQLiteConnection conn = new SQLiteConnection(myConnectionString);
                    SQLiteCommand cmd = new SQLiteCommand(iQuery, conn);

                    if (conn.State != ConnectionState.Open) conn.Open();
                    DataTable dataTable = new DataTable();

                    dataTable.Load(cmd.ExecuteReader());
                    dataGridView1.DataSource = dataTable;
                    if (conn.State == ConnectionState.Open) conn.Close();
                }
                catch (Exception)
                {

                    throw;
                }
            }
        }
        private void dateTimePicker2_CloseUp(object sender, EventArgs e)
        {
            changeFilter();
        }

        private void dateTimePicker1_CloseUp(object sender, EventArgs e)
        {
            changeFilter();
        }

        private void cbShift_SelectedIndexChanged(object sender, EventArgs e)
        {
            changeFilter();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void toolStripStatusLabel1_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_Scroll(object sender, ScrollEventArgs e)
        {
            if (e.ScrollOrientation == ScrollOrientation.VerticalScroll)
            {
                vScrollBar1.Value = e.NewValue;
            }
            else if (e.ScrollOrientation == ScrollOrientation.HorizontalScroll)
            {
                hScrollBar1.Value = e.NewValue;
            }
        }

        private void vScrollBar1_Scroll(object sender, ScrollEventArgs e)
        {
            dataGridView1.FirstDisplayedScrollingRowIndex = e.NewValue;

        }
        
        private void hScrollBar1_Scroll(object sender, ScrollEventArgs e)
        {
            dataGridView1.HorizontalScrollingOffset = e.NewValue;
        }
    }
}
