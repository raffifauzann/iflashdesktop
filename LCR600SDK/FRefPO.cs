﻿using Flurl.Http;
using LCR600SDK.DataClass;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LCR600SDK
{
    public partial class FRefPO : Form
    {
        private List<PoRitasiMaintank> _dataResult;
        private string pUrl_service = string.Empty;
        public string activity = string.Empty;

        public FRefPO()
        {
            InitializeComponent();
        }

        private void FRefPO_Load(object sender, EventArgs e)
        {
            var _proper = Properties.Settings.Default;
            pUrl_service = _proper.url_service;
            loadGridAsync(); 
        }

        public async void loadGridAsync()
        {
            string URL_API = $"{pUrl_service}api/FuelService/getPoRitasiMaintank?activity={activity}"; 
            var _auth = GlobalModel.loginModel;
            try
            {
                var res = await URL_API.WithHeaders(new { Authorization = _auth.token }).GetStringAsync();
                var _data = PoRitasiMaintank.FromJson(res);

                GlobalModel.poRitasiMaintank = _data;
                _dataResult = _data;
                DataTable dt = GeneralFunc.ToDataTable(_data);
                dataGridView1.DataSource = dt; 
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            var _list = GlobalModel.poRitasiMaintank;
            _dataResult = _list.Where(d => d.PoNo.Contains(txtSearch.Text.Trim().ToUpper())).ToList();
            DataTable dt = GeneralFunc.ToDataTable(_dataResult);
            dataGridView1.DataSource = dt;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.Columns[e.ColumnIndex].Name == "Action")
            { 
                if (Application.OpenForms["FSendFTPortForm"] != null)
                { 
                    var _from = (Application.OpenForms["FSendFTPortForm"] as FSendFTPortForm);
                    _from.txtPoNo.Text = _dataResult[e.RowIndex].PoNo;
                    _from.txtDistrik.Text = _dataResult[e.RowIndex].District;
                    _from.pTblRef = _dataResult[e.RowIndex].RefTbl;
                    _from.pPidPO = _dataResult[e.RowIndex].PidPo;
                    _from.pReceivedAt = _dataResult[e.RowIndex].ReceiveAt;
                }
                this.Close();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
    }
}
