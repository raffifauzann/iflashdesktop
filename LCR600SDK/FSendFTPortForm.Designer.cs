﻿namespace LCR600SDK
{
    partial class FSendFTPortForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FSendFTPortForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnProcess = new System.Windows.Forms.Button();
            this.cbSnCode = new System.Windows.Forms.ComboBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtSnFlowMeter = new System.Windows.Forms.TextBox();
            this.comboBoxTransportir = new System.Windows.Forms.ComboBox();
            this.chkConfirm = new System.Windows.Forms.CheckBox();
            this.btnSendTo = new System.Windows.Forms.Button();
            this.btnSendFrom = new System.Windows.Forms.Button();
            this.btnSearchPo = new System.Windows.Forms.Button();
            this.txtSegel6 = new System.Windows.Forms.TextBox();
            this.txtSegel5 = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.txtSegel4 = new System.Windows.Forms.TextBox();
            this.txtSegel3 = new System.Windows.Forms.TextBox();
            this.txtSegel2 = new System.Windows.Forms.TextBox();
            this.txtSegel1 = new System.Windows.Forms.TextBox();
            this.txtReceiveAt = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.txtReceiveDippingHole2Mt = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.txtReceiveDippingHole1Mt = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.txtSendfmAkhir = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtSendfmAwal = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtSendFrom = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtSendQty = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtSendDate = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtReceiveDriverName = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtSendDriverName = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtFt = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtDo = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtTranportir = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtDistrik = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPoNo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSegel10 = new System.Windows.Forms.TextBox();
            this.txtSegel9 = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.txtSegel8 = new System.Windows.Forms.TextBox();
            this.txtSegel7 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(101)))), ((int)(((byte)(179)))));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.btnSubmit);
            this.panel1.Controls.Add(this.btnCancel);
            this.panel1.Controls.Add(this.btnProcess);
            this.panel1.Controls.Add(this.cbSnCode);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Location = new System.Drawing.Point(4, 6);
            this.panel1.Margin = new System.Windows.Forms.Padding(0, 1, 0, 1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(804, 476);
            this.panel1.TabIndex = 0;
            // 
            // btnSubmit
            // 
            this.btnSubmit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSubmit.BackColor = System.Drawing.Color.Green;
            this.btnSubmit.FlatAppearance.BorderSize = 0;
            this.btnSubmit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSubmit.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.btnSubmit.ForeColor = System.Drawing.Color.White;
            this.btnSubmit.Location = new System.Drawing.Point(660, 416);
            this.btnSubmit.Margin = new System.Windows.Forms.Padding(2);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(112, 45);
            this.btnSubmit.TabIndex = 6;
            this.btnSubmit.Text = "SUBMIT";
            this.btnSubmit.UseVisualStyleBackColor = false;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.BackColor = System.Drawing.Color.Red;
            this.btnCancel.FlatAppearance.BorderSize = 0;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.btnCancel.ForeColor = System.Drawing.Color.White;
            this.btnCancel.Location = new System.Drawing.Point(388, 417);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(1);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(124, 45);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "CANCEL";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnProcess
            // 
            this.btnProcess.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnProcess.BackColor = System.Drawing.Color.Orange;
            this.btnProcess.FlatAppearance.BorderSize = 0;
            this.btnProcess.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProcess.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.btnProcess.ForeColor = System.Drawing.Color.White;
            this.btnProcess.Location = new System.Drawing.Point(514, 417);
            this.btnProcess.Margin = new System.Windows.Forms.Padding(1);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Size = new System.Drawing.Size(143, 45);
            this.btnProcess.TabIndex = 4;
            this.btnProcess.Text = "PROCESS";
            this.btnProcess.UseVisualStyleBackColor = false;
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // cbSnCode
            // 
            this.cbSnCode.FormattingEnabled = true;
            this.cbSnCode.Location = new System.Drawing.Point(16, 417);
            this.cbSnCode.Name = "cbSnCode";
            this.cbSnCode.Size = new System.Drawing.Size(154, 21);
            this.cbSnCode.TabIndex = 91;
            this.cbSnCode.Visible = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(101)))), ((int)(((byte)(179)))));
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.txtSnFlowMeter);
            this.panel2.Controls.Add(this.comboBoxTransportir);
            this.panel2.Controls.Add(this.chkConfirm);
            this.panel2.Controls.Add(this.btnSendTo);
            this.panel2.Controls.Add(this.btnSendFrom);
            this.panel2.Controls.Add(this.btnSearchPo);
            this.panel2.Controls.Add(this.txtSegel6);
            this.panel2.Controls.Add(this.txtSegel5);
            this.panel2.Controls.Add(this.label30);
            this.panel2.Controls.Add(this.label31);
            this.panel2.Controls.Add(this.txtSegel4);
            this.panel2.Controls.Add(this.txtSegel3);
            this.panel2.Controls.Add(this.txtSegel2);
            this.panel2.Controls.Add(this.txtSegel1);
            this.panel2.Controls.Add(this.txtReceiveAt);
            this.panel2.Controls.Add(this.label19);
            this.panel2.Controls.Add(this.label20);
            this.panel2.Controls.Add(this.label21);
            this.panel2.Controls.Add(this.label22);
            this.panel2.Controls.Add(this.txtReceiveDippingHole2Mt);
            this.panel2.Controls.Add(this.label27);
            this.panel2.Controls.Add(this.txtReceiveDippingHole1Mt);
            this.panel2.Controls.Add(this.label28);
            this.panel2.Controls.Add(this.txtSendfmAkhir);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.txtSendfmAwal);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.txtSendFrom);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.txtSendQty);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.txtSendDate);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.txtReceiveDriverName);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.txtSendDriverName);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.txtFt);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.txtDo);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.txtTranportir);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.txtDistrik);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.txtPoNo);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Location = new System.Drawing.Point(16, 16);
            this.panel2.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(758, 397);
            this.panel2.TabIndex = 0;
            // 
            // txtSnFlowMeter
            // 
            this.txtSnFlowMeter.Enabled = false;
            this.txtSnFlowMeter.Location = new System.Drawing.Point(146, 282);
            this.txtSnFlowMeter.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.txtSnFlowMeter.Name = "txtSnFlowMeter";
            this.txtSnFlowMeter.Size = new System.Drawing.Size(154, 20);
            this.txtSnFlowMeter.TabIndex = 94;
            // 
            // comboBoxTransportir
            // 
            this.comboBoxTransportir.FormattingEnabled = true;
            this.comboBoxTransportir.Location = new System.Drawing.Point(146, 75);
            this.comboBoxTransportir.Margin = new System.Windows.Forms.Padding(2);
            this.comboBoxTransportir.Name = "comboBoxTransportir";
            this.comboBoxTransportir.Size = new System.Drawing.Size(154, 21);
            this.comboBoxTransportir.TabIndex = 93;
            // 
            // chkConfirm
            // 
            this.chkConfirm.AutoSize = true;
            this.chkConfirm.ForeColor = System.Drawing.Color.White;
            this.chkConfirm.Location = new System.Drawing.Point(400, 360);
            this.chkConfirm.Name = "chkConfirm";
            this.chkConfirm.Size = new System.Drawing.Size(290, 17);
            this.chkConfirm.TabIndex = 92;
            this.chkConfirm.Text = "Apakah pengiriman sudah selesai atau sudah lengkap ?";
            this.chkConfirm.UseVisualStyleBackColor = true;
            this.chkConfirm.Visible = false;
            // 
            // btnSendTo
            // 
            this.btnSendTo.Location = new System.Drawing.Point(304, 258);
            this.btnSendTo.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.btnSendTo.Name = "btnSendTo";
            this.btnSendTo.Size = new System.Drawing.Size(20, 17);
            this.btnSendTo.TabIndex = 88;
            this.btnSendTo.Text = "--";
            this.btnSendTo.UseVisualStyleBackColor = true;
            this.btnSendTo.Click += new System.EventHandler(this.btnSendTo_Click);
            // 
            // btnSendFrom
            // 
            this.btnSendFrom.Enabled = false;
            this.btnSendFrom.Location = new System.Drawing.Point(304, 231);
            this.btnSendFrom.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.btnSendFrom.Name = "btnSendFrom";
            this.btnSendFrom.Size = new System.Drawing.Size(20, 17);
            this.btnSendFrom.TabIndex = 89;
            this.btnSendFrom.Text = "--";
            this.btnSendFrom.Visible = false;
            this.btnSendFrom.Click += new System.EventHandler(this.btnSendFrom_Click);
            // 
            // btnSearchPo
            // 
            this.btnSearchPo.Location = new System.Drawing.Point(304, 23);
            this.btnSearchPo.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.btnSearchPo.Name = "btnSearchPo";
            this.btnSearchPo.Size = new System.Drawing.Size(20, 17);
            this.btnSearchPo.TabIndex = 90;
            this.btnSearchPo.Text = "--";
            this.btnSearchPo.Click += new System.EventHandler(this.btnSearchPo_Click);
            // 
            // txtSegel6
            // 
            this.txtSegel6.Location = new System.Drawing.Point(530, 150);
            this.txtSegel6.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.txtSegel6.Name = "txtSegel6";
            this.txtSegel6.Size = new System.Drawing.Size(154, 20);
            this.txtSegel6.TabIndex = 69;
            // 
            // txtSegel5
            // 
            this.txtSegel5.Location = new System.Drawing.Point(530, 125);
            this.txtSegel5.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.txtSegel5.Name = "txtSegel5";
            this.txtSegel5.Size = new System.Drawing.Size(154, 20);
            this.txtSegel5.TabIndex = 68;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.ForeColor = System.Drawing.Color.White;
            this.label30.Location = new System.Drawing.Point(398, 150);
            this.label30.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(43, 13);
            this.label30.TabIndex = 65;
            this.label30.Text = "Segel 6";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.ForeColor = System.Drawing.Color.White;
            this.label31.Location = new System.Drawing.Point(398, 125);
            this.label31.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(43, 13);
            this.label31.TabIndex = 64;
            this.label31.Text = "Segel 5";
            // 
            // txtSegel4
            // 
            this.txtSegel4.Location = new System.Drawing.Point(530, 99);
            this.txtSegel4.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.txtSegel4.Name = "txtSegel4";
            this.txtSegel4.Size = new System.Drawing.Size(154, 20);
            this.txtSegel4.TabIndex = 63;
            // 
            // txtSegel3
            // 
            this.txtSegel3.Location = new System.Drawing.Point(530, 75);
            this.txtSegel3.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.txtSegel3.Name = "txtSegel3";
            this.txtSegel3.Size = new System.Drawing.Size(154, 20);
            this.txtSegel3.TabIndex = 62;
            // 
            // txtSegel2
            // 
            this.txtSegel2.Location = new System.Drawing.Point(530, 48);
            this.txtSegel2.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.txtSegel2.Name = "txtSegel2";
            this.txtSegel2.Size = new System.Drawing.Size(154, 20);
            this.txtSegel2.TabIndex = 61;
            // 
            // txtSegel1
            // 
            this.txtSegel1.Location = new System.Drawing.Point(530, 23);
            this.txtSegel1.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.txtSegel1.Name = "txtSegel1";
            this.txtSegel1.Size = new System.Drawing.Size(154, 20);
            this.txtSegel1.TabIndex = 60;
            // 
            // txtReceiveAt
            // 
            this.txtReceiveAt.BackColor = System.Drawing.SystemColors.Info;
            this.txtReceiveAt.Enabled = false;
            this.txtReceiveAt.Location = new System.Drawing.Point(146, 258);
            this.txtReceiveAt.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.txtReceiveAt.Name = "txtReceiveAt";
            this.txtReceiveAt.Size = new System.Drawing.Size(154, 20);
            this.txtReceiveAt.TabIndex = 59;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.ForeColor = System.Drawing.Color.White;
            this.label19.Location = new System.Drawing.Point(398, 102);
            this.label19.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(43, 13);
            this.label19.TabIndex = 46;
            this.label19.Text = "Segel 4";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.ForeColor = System.Drawing.Color.White;
            this.label20.Location = new System.Drawing.Point(398, 77);
            this.label20.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(43, 13);
            this.label20.TabIndex = 44;
            this.label20.Text = "Segel 3";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.ForeColor = System.Drawing.Color.White;
            this.label21.Location = new System.Drawing.Point(398, 48);
            this.label21.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(43, 13);
            this.label21.TabIndex = 42;
            this.label21.Text = "Segel 2";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.ForeColor = System.Drawing.Color.White;
            this.label22.Location = new System.Drawing.Point(398, 23);
            this.label22.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(43, 13);
            this.label22.TabIndex = 40;
            this.label22.Text = "Segel 1";
            // 
            // txtReceiveDippingHole2Mt
            // 
            this.txtReceiveDippingHole2Mt.Location = new System.Drawing.Point(146, 331);
            this.txtReceiveDippingHole2Mt.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.txtReceiveDippingHole2Mt.Name = "txtReceiveDippingHole2Mt";
            this.txtReceiveDippingHole2Mt.Size = new System.Drawing.Size(154, 20);
            this.txtReceiveDippingHole2Mt.TabIndex = 31;
            this.txtReceiveDippingHole2Mt.Text = "0";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.ForeColor = System.Drawing.Color.White;
            this.label27.Location = new System.Drawing.Point(14, 337);
            this.label27.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(77, 13);
            this.label27.TabIndex = 30;
            this.label27.Text = "Dipping Hole 2";
            // 
            // txtReceiveDippingHole1Mt
            // 
            this.txtReceiveDippingHole1Mt.Location = new System.Drawing.Point(146, 307);
            this.txtReceiveDippingHole1Mt.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.txtReceiveDippingHole1Mt.Name = "txtReceiveDippingHole1Mt";
            this.txtReceiveDippingHole1Mt.Size = new System.Drawing.Size(154, 20);
            this.txtReceiveDippingHole1Mt.TabIndex = 29;
            this.txtReceiveDippingHole1Mt.Text = "0";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.ForeColor = System.Drawing.Color.White;
            this.label28.Location = new System.Drawing.Point(14, 314);
            this.label28.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(77, 13);
            this.label28.TabIndex = 28;
            this.label28.Text = "Dipping Hole 1";
            // 
            // txtSendfmAkhir
            // 
            this.txtSendfmAkhir.Enabled = false;
            this.txtSendfmAkhir.Location = new System.Drawing.Point(530, 333);
            this.txtSendfmAkhir.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.txtSendfmAkhir.Name = "txtSendfmAkhir";
            this.txtSendfmAkhir.Size = new System.Drawing.Size(154, 20);
            this.txtSendfmAkhir.TabIndex = 27;
            this.txtSendfmAkhir.Text = "0";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.ForeColor = System.Drawing.Color.White;
            this.label12.Location = new System.Drawing.Point(398, 334);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(86, 13);
            this.label12.TabIndex = 26;
            this.label12.Text = "Flow Meter Akhir";
            // 
            // txtSendfmAwal
            // 
            this.txtSendfmAwal.Enabled = false;
            this.txtSendfmAwal.Location = new System.Drawing.Point(530, 307);
            this.txtSendfmAwal.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.txtSendfmAwal.Name = "txtSendfmAwal";
            this.txtSendfmAwal.Size = new System.Drawing.Size(154, 20);
            this.txtSendfmAwal.TabIndex = 25;
            this.txtSendfmAwal.Text = "0";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.ForeColor = System.Drawing.Color.White;
            this.label13.Location = new System.Drawing.Point(398, 309);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(85, 13);
            this.label13.TabIndex = 24;
            this.label13.Text = "Flow Meter Awal";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.ForeColor = System.Drawing.Color.White;
            this.label14.Location = new System.Drawing.Point(14, 286);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(80, 13);
            this.label14.TabIndex = 22;
            this.label14.Text = "SN Flow Meter ";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(14, 258);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(58, 13);
            this.label11.TabIndex = 20;
            this.label11.Text = "Diterima Di";
            // 
            // txtSendFrom
            // 
            this.txtSendFrom.BackColor = System.Drawing.SystemColors.Info;
            this.txtSendFrom.Location = new System.Drawing.Point(146, 231);
            this.txtSendFrom.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.txtSendFrom.Name = "txtSendFrom";
            this.txtSendFrom.ReadOnly = true;
            this.txtSendFrom.Size = new System.Drawing.Size(154, 20);
            this.txtSendFrom.TabIndex = 19;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(14, 233);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(60, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "Dikirim Dari";
            // 
            // txtSendQty
            // 
            this.txtSendQty.Enabled = false;
            this.txtSendQty.Location = new System.Drawing.Point(530, 283);
            this.txtSendQty.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.txtSendQty.Name = "txtSendQty";
            this.txtSendQty.Size = new System.Drawing.Size(154, 20);
            this.txtSendQty.TabIndex = 17;
            this.txtSendQty.Text = "0";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(398, 285);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(23, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "Qty";
            // 
            // txtSendDate
            // 
            this.txtSendDate.Enabled = false;
            this.txtSendDate.Location = new System.Drawing.Point(146, 204);
            this.txtSendDate.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.txtSendDate.Name = "txtSendDate";
            this.txtSendDate.Size = new System.Drawing.Size(154, 20);
            this.txtSendDate.TabIndex = 15;
            this.txtSendDate.Visible = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(14, 205);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(84, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "Berangkat Pada";
            this.label8.Visible = false;
            // 
            // txtReceiveDriverName
            // 
            this.txtReceiveDriverName.Location = new System.Drawing.Point(146, 178);
            this.txtReceiveDriverName.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.txtReceiveDriverName.Name = "txtReceiveDriverName";
            this.txtReceiveDriverName.Size = new System.Drawing.Size(154, 20);
            this.txtReceiveDriverName.TabIndex = 13;
            this.txtReceiveDriverName.Visible = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(14, 180);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(125, 13);
            this.label9.TabIndex = 12;
            this.label9.Text = "Nama Driver Penerimaan";
            this.label9.Visible = false;
            // 
            // txtSendDriverName
            // 
            this.txtSendDriverName.Location = new System.Drawing.Point(146, 154);
            this.txtSendDriverName.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.txtSendDriverName.Name = "txtSendDriverName";
            this.txtSendDriverName.Size = new System.Drawing.Size(154, 20);
            this.txtSendDriverName.TabIndex = 11;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(14, 156);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(121, 13);
            this.label10.TabIndex = 10;
            this.label10.Text = "Nama Driver Pengiriman";
            // 
            // txtFt
            // 
            this.txtFt.Location = new System.Drawing.Point(146, 125);
            this.txtFt.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.txtFt.Name = "txtFt";
            this.txtFt.Size = new System.Drawing.Size(154, 20);
            this.txtFt.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(14, 132);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(92, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Nomer Fuel Truck";
            // 
            // txtDo
            // 
            this.txtDo.Location = new System.Drawing.Point(146, 100);
            this.txtDo.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.txtDo.Name = "txtDo";
            this.txtDo.Size = new System.Drawing.Size(154, 20);
            this.txtDo.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(14, 107);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(94, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Nomer Surat Jalan";
            // 
            // txtTranportir
            // 
            this.txtTranportir.Location = new System.Drawing.Point(146, 358);
            this.txtTranportir.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.txtTranportir.Name = "txtTranportir";
            this.txtTranportir.Size = new System.Drawing.Size(154, 20);
            this.txtTranportir.TabIndex = 5;
            this.txtTranportir.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(14, 79);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Transportir";
            // 
            // txtDistrik
            // 
            this.txtDistrik.Enabled = false;
            this.txtDistrik.Location = new System.Drawing.Point(146, 47);
            this.txtDistrik.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.txtDistrik.Name = "txtDistrik";
            this.txtDistrik.Size = new System.Drawing.Size(154, 20);
            this.txtDistrik.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(14, 53);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Distrik";
            // 
            // txtPoNo
            // 
            this.txtPoNo.BackColor = System.Drawing.SystemColors.Info;
            this.txtPoNo.Location = new System.Drawing.Point(146, 23);
            this.txtPoNo.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.txtPoNo.Name = "txtPoNo";
            this.txtPoNo.ReadOnly = true;
            this.txtPoNo.Size = new System.Drawing.Size(154, 20);
            this.txtPoNo.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(14, 30);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nomer PO";
            // 
            // txtSegel10
            // 
            this.txtSegel10.Location = new System.Drawing.Point(162, 557);
            this.txtSegel10.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.txtSegel10.Name = "txtSegel10";
            this.txtSegel10.Size = new System.Drawing.Size(154, 20);
            this.txtSegel10.TabIndex = 75;
            this.txtSegel10.Visible = false;
            // 
            // txtSegel9
            // 
            this.txtSegel9.Location = new System.Drawing.Point(162, 533);
            this.txtSegel9.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.txtSegel9.Name = "txtSegel9";
            this.txtSegel9.Size = new System.Drawing.Size(154, 20);
            this.txtSegel9.TabIndex = 74;
            this.txtSegel9.Visible = false;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(30, 560);
            this.label32.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(49, 13);
            this.label32.TabIndex = 73;
            this.label32.Text = "Segel 10";
            this.label32.Visible = false;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(30, 535);
            this.label33.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(43, 13);
            this.label33.TabIndex = 72;
            this.label33.Text = "Segel 9";
            this.label33.Visible = false;
            // 
            // txtSegel8
            // 
            this.txtSegel8.Location = new System.Drawing.Point(162, 505);
            this.txtSegel8.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.txtSegel8.Name = "txtSegel8";
            this.txtSegel8.Size = new System.Drawing.Size(154, 20);
            this.txtSegel8.TabIndex = 71;
            this.txtSegel8.Visible = false;
            // 
            // txtSegel7
            // 
            this.txtSegel7.Location = new System.Drawing.Point(162, 481);
            this.txtSegel7.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.txtSegel7.Name = "txtSegel7";
            this.txtSegel7.Size = new System.Drawing.Size(154, 20);
            this.txtSegel7.TabIndex = 70;
            this.txtSegel7.Visible = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(30, 508);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(43, 13);
            this.label15.TabIndex = 67;
            this.label15.Text = "Segel 8";
            this.label15.Visible = false;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(30, 483);
            this.label29.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(43, 13);
            this.label29.TabIndex = 66;
            this.label29.Text = "Segel 7";
            this.label29.Visible = false;
            // 
            // FSendFTPortForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(101)))), ((int)(((byte)(179)))));
            this.ClientSize = new System.Drawing.Size(812, 488);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.txtSegel7);
            this.Controls.Add(this.txtSegel8);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.txtSegel10);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.txtSegel9);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FSendFTPortForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form Send FT Port";
            this.Load += new System.EventHandler(this.FReceivedFTPortForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox txtSegel10;
        private System.Windows.Forms.TextBox txtSegel9;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox txtSegel8;
        private System.Windows.Forms.TextBox txtSegel7;
        private System.Windows.Forms.TextBox txtSegel6;
        private System.Windows.Forms.TextBox txtSegel5;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox txtSegel4;
        private System.Windows.Forms.TextBox txtSegel3;
        private System.Windows.Forms.TextBox txtSegel2;
        private System.Windows.Forms.TextBox txtSegel1;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtReceiveDippingHole2Mt;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtReceiveDippingHole1Mt;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox txtSendfmAkhir;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtSendfmAwal;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtSendQty;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtSendDate;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtReceiveDriverName;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtSendDriverName;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtFt;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtDo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtTranportir;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSearchPo;
        public System.Windows.Forms.TextBox txtPoNo;
        private System.Windows.Forms.Button btnSendFrom;
        private System.Windows.Forms.Button btnSendTo;
        public System.Windows.Forms.TextBox txtReceiveAt;
        public System.Windows.Forms.TextBox txtSendFrom;
        public System.Windows.Forms.TextBox txtDistrik;
        private System.Windows.Forms.ComboBox cbSnCode;
        private System.Windows.Forms.CheckBox chkConfirm;
        private System.Windows.Forms.ComboBox comboBoxTransportir;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnProcess;
        private System.Windows.Forms.TextBox txtSnFlowMeter;
    }
}