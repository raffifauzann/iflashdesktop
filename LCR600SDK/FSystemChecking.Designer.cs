﻿
namespace LCR600SDK
{
    partial class FSystemChecking
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FSystemChecking));
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblCheckWarehouse = new System.Windows.Forms.Label();
            this.lblCheckFuelman = new System.Windows.Forms.Label();
            this.lblCheckFlowmeter = new System.Windows.Forms.Label();
            this.lblCheckUnit = new System.Windows.Forms.Label();
            this.lblCheckTransportir = new System.Windows.Forms.Label();
            this.btnCheckDone = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(113, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(264, 37);
            this.label1.TabIndex = 0;
            this.label1.Text = "System Checking";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(508, 54);
            this.panel1.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(181, 37);
            this.label2.TabIndex = 1;
            this.label2.Text = "Warehouse";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(142, 37);
            this.label3.TabIndex = 2;
            this.label3.Text = "Fuelman";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 131);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(166, 37);
            this.label4.TabIndex = 3;
            this.label4.Text = "Flowmeter";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 168);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 37);
            this.label5.TabIndex = 4;
            this.label5.Text = "Unit";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(12, 205);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(174, 37);
            this.label6.TabIndex = 5;
            this.label6.Text = "Transportir";
            // 
            // lblCheckWarehouse
            // 
            this.lblCheckWarehouse.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCheckWarehouse.AutoSize = true;
            this.lblCheckWarehouse.BackColor = System.Drawing.Color.Yellow;
            this.lblCheckWarehouse.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCheckWarehouse.Location = new System.Drawing.Point(228, 57);
            this.lblCheckWarehouse.Name = "lblCheckWarehouse";
            this.lblCheckWarehouse.Size = new System.Drawing.Size(273, 37);
            this.lblCheckWarehouse.TabIndex = 6;
            this.lblCheckWarehouse.Text = "ERROR, KLIK INI!";
            this.lblCheckWarehouse.Click += new System.EventHandler(this.lblCheckWarehouse_Click);
            // 
            // lblCheckFuelman
            // 
            this.lblCheckFuelman.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCheckFuelman.AutoSize = true;
            this.lblCheckFuelman.BackColor = System.Drawing.Color.Yellow;
            this.lblCheckFuelman.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCheckFuelman.Location = new System.Drawing.Point(228, 94);
            this.lblCheckFuelman.Name = "lblCheckFuelman";
            this.lblCheckFuelman.Size = new System.Drawing.Size(150, 37);
            this.lblCheckFuelman.TabIndex = 7;
            this.lblCheckFuelman.Text = "Checking";
            // 
            // lblCheckFlowmeter
            // 
            this.lblCheckFlowmeter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCheckFlowmeter.AutoSize = true;
            this.lblCheckFlowmeter.BackColor = System.Drawing.Color.Yellow;
            this.lblCheckFlowmeter.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCheckFlowmeter.Location = new System.Drawing.Point(228, 131);
            this.lblCheckFlowmeter.Name = "lblCheckFlowmeter";
            this.lblCheckFlowmeter.Size = new System.Drawing.Size(150, 37);
            this.lblCheckFlowmeter.TabIndex = 8;
            this.lblCheckFlowmeter.Text = "Checking";
            // 
            // lblCheckUnit
            // 
            this.lblCheckUnit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCheckUnit.AutoSize = true;
            this.lblCheckUnit.BackColor = System.Drawing.Color.Yellow;
            this.lblCheckUnit.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCheckUnit.Location = new System.Drawing.Point(228, 168);
            this.lblCheckUnit.Name = "lblCheckUnit";
            this.lblCheckUnit.Size = new System.Drawing.Size(150, 37);
            this.lblCheckUnit.TabIndex = 9;
            this.lblCheckUnit.Text = "Checking";
            // 
            // lblCheckTransportir
            // 
            this.lblCheckTransportir.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCheckTransportir.AutoSize = true;
            this.lblCheckTransportir.BackColor = System.Drawing.Color.Yellow;
            this.lblCheckTransportir.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCheckTransportir.Location = new System.Drawing.Point(228, 205);
            this.lblCheckTransportir.Name = "lblCheckTransportir";
            this.lblCheckTransportir.Size = new System.Drawing.Size(150, 37);
            this.lblCheckTransportir.TabIndex = 10;
            this.lblCheckTransportir.Text = "Checking";
            // 
            // btnCheckDone
            // 
            this.btnCheckDone.BackColor = System.Drawing.Color.Green;
            this.btnCheckDone.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCheckDone.FlatAppearance.BorderSize = 0;
            this.btnCheckDone.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCheckDone.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCheckDone.ForeColor = System.Drawing.Color.White;
            this.btnCheckDone.Location = new System.Drawing.Point(0, 0);
            this.btnCheckDone.Name = "btnCheckDone";
            this.btnCheckDone.Size = new System.Drawing.Size(508, 77);
            this.btnCheckDone.TabIndex = 11;
            this.btnCheckDone.Text = "OK";
            this.btnCheckDone.UseVisualStyleBackColor = false;
            this.btnCheckDone.Click += new System.EventHandler(this.btnCheckDone_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnCheckDone);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 256);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(508, 77);
            this.panel2.TabIndex = 12;
            // 
            // FSystemChecking
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(508, 333);
            this.ControlBox = false;
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.lblCheckTransportir);
            this.Controls.Add(this.lblCheckUnit);
            this.Controls.Add(this.lblCheckFlowmeter);
            this.Controls.Add(this.lblCheckFuelman);
            this.Controls.Add(this.lblCheckWarehouse);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FSystemChecking";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FSystemChecking";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.FSystemChecking_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblCheckWarehouse;
        private System.Windows.Forms.Label lblCheckFuelman;
        private System.Windows.Forms.Label lblCheckFlowmeter;
        private System.Windows.Forms.Label lblCheckUnit;
        private System.Windows.Forms.Label lblCheckTransportir;
        private System.Windows.Forms.Button btnCheckDone;
        private System.Windows.Forms.Panel panel2;
    }
}