﻿
using System.Windows.Forms;

namespace LCR600SDK
{
    partial class FUserSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FUserSetting));
            this.label2 = new System.Windows.Forms.Label();
            this.btnConnection = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tpConnection = new System.Windows.Forms.TabPage();
            this.label19 = new System.Windows.Forms.Label();
            this.numCheckOnlineLogin = new System.Windows.Forms.NumericUpDown();
            this.label20 = new System.Windows.Forms.Label();
            this.btnSaveConnection = new System.Windows.Forms.Button();
            this.tpUserSetting = new System.Windows.Forms.TabPage();
            this.numIntervalAutoSync = new System.Windows.Forms.NumericUpDown();
            this.label17 = new System.Windows.Forms.Label();
            this.numLCRWholeTenth = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.numDataPerKirimSync = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.pMenu7Inch = new System.Windows.Forms.Panel();
            this.btn7InchMenu4 = new System.Windows.Forms.Button();
            this.btn7InchMenu9 = new System.Windows.Forms.Button();
            this.btn7InchMenu2 = new System.Windows.Forms.Button();
            this.btn7InchMenu8 = new System.Windows.Forms.Button();
            this.btn7InchMenu3 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cbDfStatusType = new System.Windows.Forms.ComboBox();
            this.btnAutoSyncOnOff = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.btnSaveUserSetting = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.clb_menu_access = new System.Windows.Forms.CheckedListBox();
            this.pMenu10Inch = new System.Windows.Forms.Panel();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btnMenu2 = new System.Windows.Forms.Button();
            this.btnMenu3 = new System.Windows.Forms.Button();
            this.btnMenu4 = new System.Windows.Forms.Button();
            this.btnMenu1 = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btnMenu16 = new System.Windows.Forms.Button();
            this.btnMenu15 = new System.Windows.Forms.Button();
            this.btnMenu7 = new System.Windows.Forms.Button();
            this.btnMenu6 = new System.Windows.Forms.Button();
            this.btnMenu5 = new System.Windows.Forms.Button();
            this.btnMenu10 = new System.Windows.Forms.Button();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.btnMenu18 = new System.Windows.Forms.Button();
            this.btnMenu17 = new System.Windows.Forms.Button();
            this.btnMenu14 = new System.Windows.Forms.Button();
            this.btnMenu13 = new System.Windows.Forms.Button();
            this.btnMenu12 = new System.Windows.Forms.Button();
            this.btnMenu11 = new System.Windows.Forms.Button();
            this.btnMenu9 = new System.Windows.Forms.Button();
            this.btnMenu8 = new System.Windows.Forms.Button();
            this.tpWarehouse = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnNewConfig = new System.Windows.Forms.Button();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.cbWarehouseName = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.dateTimeShift2 = new System.Windows.Forms.DateTimePicker();
            this.dateTimeShift1 = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.comboBoxSerialNumber = new System.Windows.Forms.ComboBox();
            this.comboBoxWarehouse = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.tpWarehouse2 = new System.Windows.Forms.TabPage();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnNewConfig2 = new System.Windows.Forms.Button();
            this.btnSubmit2 = new System.Windows.Forms.Button();
            this.cbWarehouseName2 = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.dateTimeShift22 = new System.Windows.Forms.DateTimePicker();
            this.dateTimeShift12 = new System.Windows.Forms.DateTimePicker();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.comboBoxSerialNumberwh2 = new System.Windows.Forms.ComboBox();
            this.comboBoxWarehouse2 = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tpConnection.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numCheckOnlineLogin)).BeginInit();
            this.tpUserSetting.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numIntervalAutoSync)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numLCRWholeTenth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDataPerKirimSync)).BeginInit();
            this.pMenu7Inch.SuspendLayout();
            this.pMenu10Inch.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tpWarehouse.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tpWarehouse2.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F);
            this.label2.Location = new System.Drawing.Point(8, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(208, 42);
            this.label2.TabIndex = 4;
            this.label2.Text = "Connection";
            // 
            // btnConnection
            // 
            this.btnConnection.BackColor = System.Drawing.Color.Green;
            this.btnConnection.FlatAppearance.BorderSize = 0;
            this.btnConnection.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConnection.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F);
            this.btnConnection.ForeColor = System.Drawing.Color.White;
            this.btnConnection.Location = new System.Drawing.Point(243, 18);
            this.btnConnection.Name = "btnConnection";
            this.btnConnection.Size = new System.Drawing.Size(340, 50);
            this.btnConnection.TabIndex = 5;
            this.btnConnection.Text = "ONLINE";
            this.btnConnection.UseVisualStyleBackColor = false;
            this.btnConnection.Click += new System.EventHandler(this.btnConnection_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tpConnection);
            this.tabControl1.Controls.Add(this.tpUserSetting);
            this.tabControl1.Controls.Add(this.tpWarehouse);
            this.tabControl1.Controls.Add(this.tpWarehouse2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 22F);
            this.tabControl1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(828, 716);
            this.tabControl1.TabIndex = 11;
            this.tabControl1.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.tabControl1_DrawItem);
            this.tabControl1.Selected += new System.Windows.Forms.TabControlEventHandler(this.tabControl1_Selected);
            // 
            // tpConnection
            // 
            this.tpConnection.BackColor = System.Drawing.SystemColors.Control;
            this.tpConnection.Controls.Add(this.label19);
            this.tpConnection.Controls.Add(this.numCheckOnlineLogin);
            this.tpConnection.Controls.Add(this.label20);
            this.tpConnection.Controls.Add(this.btnSaveConnection);
            this.tpConnection.Controls.Add(this.label2);
            this.tpConnection.Controls.Add(this.btnConnection);
            this.tpConnection.Location = new System.Drawing.Point(4, 42);
            this.tpConnection.Name = "tpConnection";
            this.tpConnection.Padding = new System.Windows.Forms.Padding(3);
            this.tpConnection.Size = new System.Drawing.Size(820, 670);
            this.tpConnection.TabIndex = 0;
            this.tpConnection.Text = "Connection";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Enabled = false;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F);
            this.label19.Location = new System.Drawing.Point(186, 116);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(97, 42);
            this.label19.TabIndex = 51;
            this.label19.Text = "detik";
            this.label19.Visible = false;
            // 
            // numCheckOnlineLogin
            // 
            this.numCheckOnlineLogin.Enabled = false;
            this.numCheckOnlineLogin.Location = new System.Drawing.Point(15, 116);
            this.numCheckOnlineLogin.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numCheckOnlineLogin.Name = "numCheckOnlineLogin";
            this.numCheckOnlineLogin.Size = new System.Drawing.Size(165, 41);
            this.numCheckOnlineLogin.TabIndex = 50;
            this.numCheckOnlineLogin.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.numCheckOnlineLogin.Visible = false;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Enabled = false;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F);
            this.label20.Location = new System.Drawing.Point(8, 71);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(544, 42);
            this.label20.TabIndex = 49;
            this.label20.Text = "Interval Check Online saat login";
            this.label20.Visible = false;
            // 
            // btnSaveConnection
            // 
            this.btnSaveConnection.BackColor = System.Drawing.Color.Green;
            this.btnSaveConnection.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnSaveConnection.FlatAppearance.BorderSize = 0;
            this.btnSaveConnection.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSaveConnection.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold);
            this.btnSaveConnection.ForeColor = System.Drawing.Color.White;
            this.btnSaveConnection.Location = new System.Drawing.Point(3, 579);
            this.btnSaveConnection.Name = "btnSaveConnection";
            this.btnSaveConnection.Size = new System.Drawing.Size(814, 88);
            this.btnSaveConnection.TabIndex = 17;
            this.btnSaveConnection.Text = "SAVE";
            this.btnSaveConnection.UseVisualStyleBackColor = false;
            this.btnSaveConnection.Click += new System.EventHandler(this.btnSaveConnection_Click);
            // 
            // tpUserSetting
            // 
            this.tpUserSetting.BackColor = System.Drawing.SystemColors.Control;
            this.tpUserSetting.Controls.Add(this.numIntervalAutoSync);
            this.tpUserSetting.Controls.Add(this.label17);
            this.tpUserSetting.Controls.Add(this.numLCRWholeTenth);
            this.tpUserSetting.Controls.Add(this.label11);
            this.tpUserSetting.Controls.Add(this.numDataPerKirimSync);
            this.tpUserSetting.Controls.Add(this.label10);
            this.tpUserSetting.Controls.Add(this.pMenu7Inch);
            this.tpUserSetting.Controls.Add(this.label1);
            this.tpUserSetting.Controls.Add(this.cbDfStatusType);
            this.tpUserSetting.Controls.Add(this.btnAutoSyncOnOff);
            this.tpUserSetting.Controls.Add(this.label3);
            this.tpUserSetting.Controls.Add(this.btnSaveUserSetting);
            this.tpUserSetting.Controls.Add(this.label4);
            this.tpUserSetting.Controls.Add(this.clb_menu_access);
            this.tpUserSetting.Controls.Add(this.pMenu10Inch);
            this.tpUserSetting.Location = new System.Drawing.Point(4, 42);
            this.tpUserSetting.Name = "tpUserSetting";
            this.tpUserSetting.Padding = new System.Windows.Forms.Padding(3);
            this.tpUserSetting.Size = new System.Drawing.Size(820, 670);
            this.tpUserSetting.TabIndex = 1;
            this.tpUserSetting.Text = "User Setting";
            // 
            // numIntervalAutoSync
            // 
            this.numIntervalAutoSync.Location = new System.Drawing.Point(693, 78);
            this.numIntervalAutoSync.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numIntervalAutoSync.Name = "numIntervalAutoSync";
            this.numIntervalAutoSync.Size = new System.Drawing.Size(94, 41);
            this.numIntervalAutoSync.TabIndex = 47;
            this.numIntervalAutoSync.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F);
            this.label17.Location = new System.Drawing.Point(350, 72);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(328, 42);
            this.label17.TabIndex = 46;
            this.label17.Text = "Interval Sync (mnt)";
            // 
            // numLCRWholeTenth
            // 
            this.numLCRWholeTenth.Location = new System.Drawing.Point(693, 123);
            this.numLCRWholeTenth.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numLCRWholeTenth.Name = "numLCRWholeTenth";
            this.numLCRWholeTenth.Size = new System.Drawing.Size(94, 41);
            this.numLCRWholeTenth.TabIndex = 45;
            this.numLCRWholeTenth.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F);
            this.label11.Location = new System.Drawing.Point(350, 122);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(335, 42);
            this.label11.TabIndex = 44;
            this.label11.Text = "LCR Whole / Tenth";
            // 
            // numDataPerKirimSync
            // 
            this.numDataPerKirimSync.Location = new System.Drawing.Point(249, 72);
            this.numDataPerKirimSync.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numDataPerKirimSync.Name = "numDataPerKirimSync";
            this.numDataPerKirimSync.Size = new System.Drawing.Size(95, 41);
            this.numDataPerKirimSync.TabIndex = 22;
            this.numDataPerKirimSync.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F);
            this.label10.Location = new System.Drawing.Point(1, 66);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(248, 42);
            this.label10.TabIndex = 21;
            this.label10.Text = "Data per kirim";
            // 
            // pMenu7Inch
            // 
            this.pMenu7Inch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pMenu7Inch.Controls.Add(this.btn7InchMenu4);
            this.pMenu7Inch.Controls.Add(this.btn7InchMenu9);
            this.pMenu7Inch.Controls.Add(this.btn7InchMenu2);
            this.pMenu7Inch.Controls.Add(this.btn7InchMenu8);
            this.pMenu7Inch.Controls.Add(this.btn7InchMenu3);
            this.pMenu7Inch.Location = new System.Drawing.Point(8, 218);
            this.pMenu7Inch.Name = "pMenu7Inch";
            this.pMenu7Inch.Size = new System.Drawing.Size(799, 151);
            this.pMenu7Inch.TabIndex = 43;
            // 
            // btn7InchMenu4
            // 
            this.btn7InchMenu4.BackColor = System.Drawing.Color.Green;
            this.btn7InchMenu4.FlatAppearance.BorderSize = 0;
            this.btn7InchMenu4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn7InchMenu4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.btn7InchMenu4.ForeColor = System.Drawing.Color.White;
            this.btn7InchMenu4.Location = new System.Drawing.Point(7, 3);
            this.btn7InchMenu4.Name = "btn7InchMenu4";
            this.btn7InchMenu4.Size = new System.Drawing.Size(176, 66);
            this.btn7InchMenu4.TabIndex = 42;
            this.btn7InchMenu4.Text = "Refueling Pitstop";
            this.btn7InchMenu4.UseVisualStyleBackColor = false;
            this.btn7InchMenu4.Click += new System.EventHandler(this.btnMenu1_Click);
            // 
            // btn7InchMenu9
            // 
            this.btn7InchMenu9.BackColor = System.Drawing.Color.Green;
            this.btn7InchMenu9.FlatAppearance.BorderSize = 0;
            this.btn7InchMenu9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn7InchMenu9.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.btn7InchMenu9.ForeColor = System.Drawing.Color.White;
            this.btn7InchMenu9.Location = new System.Drawing.Point(187, 76);
            this.btn7InchMenu9.Name = "btn7InchMenu9";
            this.btn7InchMenu9.Size = new System.Drawing.Size(176, 66);
            this.btn7InchMenu9.TabIndex = 41;
            this.btn7InchMenu9.Text = "List Data Transfer";
            this.btn7InchMenu9.UseVisualStyleBackColor = false;
            this.btn7InchMenu9.Click += new System.EventHandler(this.btnMenu9_Click);
            // 
            // btn7InchMenu2
            // 
            this.btn7InchMenu2.BackColor = System.Drawing.Color.Green;
            this.btn7InchMenu2.FlatAppearance.BorderSize = 0;
            this.btn7InchMenu2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn7InchMenu2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.btn7InchMenu2.ForeColor = System.Drawing.Color.White;
            this.btn7InchMenu2.Location = new System.Drawing.Point(186, 4);
            this.btn7InchMenu2.Name = "btn7InchMenu2";
            this.btn7InchMenu2.Size = new System.Drawing.Size(176, 66);
            this.btn7InchMenu2.TabIndex = 34;
            this.btn7InchMenu2.Text = "Refueling FT";
            this.btn7InchMenu2.UseVisualStyleBackColor = false;
            this.btn7InchMenu2.Click += new System.EventHandler(this.btnMenu2_Click);
            // 
            // btn7InchMenu8
            // 
            this.btn7InchMenu8.BackColor = System.Drawing.Color.Green;
            this.btn7InchMenu8.FlatAppearance.BorderSize = 0;
            this.btn7InchMenu8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn7InchMenu8.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.btn7InchMenu8.ForeColor = System.Drawing.Color.White;
            this.btn7InchMenu8.Location = new System.Drawing.Point(5, 76);
            this.btn7InchMenu8.Name = "btn7InchMenu8";
            this.btn7InchMenu8.Size = new System.Drawing.Size(176, 66);
            this.btn7InchMenu8.TabIndex = 40;
            this.btn7InchMenu8.Text = "List Data Issued";
            this.btn7InchMenu8.UseVisualStyleBackColor = false;
            this.btn7InchMenu8.Click += new System.EventHandler(this.btnMenu8_Click);
            // 
            // btn7InchMenu3
            // 
            this.btn7InchMenu3.BackColor = System.Drawing.Color.Green;
            this.btn7InchMenu3.FlatAppearance.BorderSize = 0;
            this.btn7InchMenu3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn7InchMenu3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.btn7InchMenu3.ForeColor = System.Drawing.Color.White;
            this.btn7InchMenu3.Location = new System.Drawing.Point(368, 4);
            this.btn7InchMenu3.Name = "btn7InchMenu3";
            this.btn7InchMenu3.Size = new System.Drawing.Size(176, 66);
            this.btn7InchMenu3.TabIndex = 35;
            this.btn7InchMenu3.Text = "Warehouse Transfer";
            this.btn7InchMenu3.UseVisualStyleBackColor = false;
            this.btn7InchMenu3.Click += new System.EventHandler(this.btnMenu3_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F);
            this.label1.Location = new System.Drawing.Point(6, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(208, 42);
            this.label1.TabIndex = 11;
            this.label1.Text = "Trans Type";
            // 
            // cbDfStatusType
            // 
            this.cbDfStatusType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDfStatusType.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F);
            this.cbDfStatusType.FormattingEnabled = true;
            this.cbDfStatusType.Items.AddRange(new object[] {
            "HR",
            "KM"});
            this.cbDfStatusType.Location = new System.Drawing.Point(249, 13);
            this.cbDfStatusType.Name = "cbDfStatusType";
            this.cbDfStatusType.Size = new System.Drawing.Size(95, 50);
            this.cbDfStatusType.TabIndex = 12;
            // 
            // btnAutoSyncOnOff
            // 
            this.btnAutoSyncOnOff.BackColor = System.Drawing.Color.Green;
            this.btnAutoSyncOnOff.FlatAppearance.BorderSize = 0;
            this.btnAutoSyncOnOff.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAutoSyncOnOff.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F);
            this.btnAutoSyncOnOff.ForeColor = System.Drawing.Color.White;
            this.btnAutoSyncOnOff.Location = new System.Drawing.Point(650, 16);
            this.btnAutoSyncOnOff.Name = "btnAutoSyncOnOff";
            this.btnAutoSyncOnOff.Size = new System.Drawing.Size(120, 50);
            this.btnAutoSyncOnOff.TabIndex = 15;
            this.btnAutoSyncOnOff.Text = "OFF";
            this.btnAutoSyncOnOff.UseVisualStyleBackColor = false;
            this.btnAutoSyncOnOff.Click += new System.EventHandler(this.btnAutoSyncOnOff_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F);
            this.label3.Location = new System.Drawing.Point(350, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(189, 42);
            this.label3.TabIndex = 14;
            this.label3.Text = "Auto Sync";
            // 
            // btnSaveUserSetting
            // 
            this.btnSaveUserSetting.BackColor = System.Drawing.Color.Green;
            this.btnSaveUserSetting.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnSaveUserSetting.FlatAppearance.BorderSize = 0;
            this.btnSaveUserSetting.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSaveUserSetting.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold);
            this.btnSaveUserSetting.ForeColor = System.Drawing.Color.White;
            this.btnSaveUserSetting.Location = new System.Drawing.Point(3, 579);
            this.btnSaveUserSetting.Name = "btnSaveUserSetting";
            this.btnSaveUserSetting.Size = new System.Drawing.Size(814, 88);
            this.btnSaveUserSetting.TabIndex = 13;
            this.btnSaveUserSetting.Text = "SAVE";
            this.btnSaveUserSetting.UseVisualStyleBackColor = false;
            this.btnSaveUserSetting.Click += new System.EventHandler(this.btnSaveUserSetting_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F);
            this.label4.Location = new System.Drawing.Point(14, 167);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(244, 42);
            this.label4.TabIndex = 17;
            this.label4.Text = "Menu Access";
            // 
            // clb_menu_access
            // 
            this.clb_menu_access.FormattingEnabled = true;
            this.clb_menu_access.Items.AddRange(new object[] {
            "Refueling Pitstop",
            "Refueling FT",
            "Warehouse Transfer",
            "Warehouse Transfer Pengirim",
            "Received FT Port",
            "Received FT Darat",
            "Pengiriman FT Port PAMA",
            "List Data Issued",
            "List Data Transfer",
            "Received Direct",
            "List Data Receive Direct",
            "List Data Pengiriman FT Port PAMA",
            "List Data Received FT Port",
            "List Data Received FT Darat",
            "Receive Direct Owner",
            "Pengiriman POT",
            "List Data Receive Direct Owner",
            "List Data Pengiriman POT"});
            this.clb_menu_access.Location = new System.Drawing.Point(42, 560);
            this.clb_menu_access.Name = "clb_menu_access";
            this.clb_menu_access.Size = new System.Drawing.Size(722, 76);
            this.clb_menu_access.TabIndex = 32;
            this.clb_menu_access.Visible = false;
            // 
            // pMenu10Inch
            // 
            this.pMenu10Inch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pMenu10Inch.Controls.Add(this.tabControl2);
            this.pMenu10Inch.Location = new System.Drawing.Point(13, 212);
            this.pMenu10Inch.Name = "pMenu10Inch";
            this.pMenu10Inch.Size = new System.Drawing.Size(803, 342);
            this.pMenu10Inch.TabIndex = 42;
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tabPage1);
            this.tabControl2.Controls.Add(this.tabPage2);
            this.tabControl2.Controls.Add(this.tabPage3);
            this.tabControl2.Location = new System.Drawing.Point(3, 4);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(746, 335);
            this.tabControl2.TabIndex = 49;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.btnMenu2);
            this.tabPage1.Controls.Add(this.btnMenu3);
            this.tabPage1.Controls.Add(this.btnMenu4);
            this.tabPage1.Controls.Add(this.btnMenu1);
            this.tabPage1.Location = new System.Drawing.Point(4, 42);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(738, 289);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Transaksi";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // btnMenu2
            // 
            this.btnMenu2.BackColor = System.Drawing.Color.Green;
            this.btnMenu2.FlatAppearance.BorderSize = 0;
            this.btnMenu2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMenu2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.btnMenu2.ForeColor = System.Drawing.Color.White;
            this.btnMenu2.Location = new System.Drawing.Point(188, 6);
            this.btnMenu2.Name = "btnMenu2";
            this.btnMenu2.Size = new System.Drawing.Size(176, 66);
            this.btnMenu2.TabIndex = 37;
            this.btnMenu2.Text = "Refueling FT";
            this.btnMenu2.UseVisualStyleBackColor = false;
            this.btnMenu2.Click += new System.EventHandler(this.btnMenu2_Click);
            // 
            // btnMenu3
            // 
            this.btnMenu3.BackColor = System.Drawing.Color.Green;
            this.btnMenu3.FlatAppearance.BorderSize = 0;
            this.btnMenu3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMenu3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.btnMenu3.ForeColor = System.Drawing.Color.White;
            this.btnMenu3.Location = new System.Drawing.Point(370, 6);
            this.btnMenu3.Name = "btnMenu3";
            this.btnMenu3.Size = new System.Drawing.Size(176, 66);
            this.btnMenu3.TabIndex = 38;
            this.btnMenu3.Text = "Warehouse Transfer";
            this.btnMenu3.UseVisualStyleBackColor = false;
            this.btnMenu3.Click += new System.EventHandler(this.btnMenu3_Click);
            // 
            // btnMenu4
            // 
            this.btnMenu4.BackColor = System.Drawing.Color.Green;
            this.btnMenu4.FlatAppearance.BorderSize = 0;
            this.btnMenu4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMenu4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.btnMenu4.ForeColor = System.Drawing.Color.White;
            this.btnMenu4.Location = new System.Drawing.Point(552, 6);
            this.btnMenu4.Name = "btnMenu4";
            this.btnMenu4.Size = new System.Drawing.Size(176, 66);
            this.btnMenu4.TabIndex = 39;
            this.btnMenu4.Text = "Warehouse Transfer Pengirim";
            this.btnMenu4.UseVisualStyleBackColor = false;
            this.btnMenu4.Click += new System.EventHandler(this.btnMenu4_Click);
            // 
            // btnMenu1
            // 
            this.btnMenu1.BackColor = System.Drawing.Color.Green;
            this.btnMenu1.FlatAppearance.BorderSize = 0;
            this.btnMenu1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMenu1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.btnMenu1.ForeColor = System.Drawing.Color.White;
            this.btnMenu1.Location = new System.Drawing.Point(6, 6);
            this.btnMenu1.Name = "btnMenu1";
            this.btnMenu1.Size = new System.Drawing.Size(176, 66);
            this.btnMenu1.TabIndex = 34;
            this.btnMenu1.Text = "Refueling Pitstop";
            this.btnMenu1.UseVisualStyleBackColor = false;
            this.btnMenu1.Click += new System.EventHandler(this.btnMenu1_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.btnMenu16);
            this.tabPage2.Controls.Add(this.btnMenu15);
            this.tabPage2.Controls.Add(this.btnMenu7);
            this.tabPage2.Controls.Add(this.btnMenu6);
            this.tabPage2.Controls.Add(this.btnMenu5);
            this.tabPage2.Controls.Add(this.btnMenu10);
            this.tabPage2.Location = new System.Drawing.Point(4, 42);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(738, 289);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Pengiriman & Receive";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // btnMenu16
            // 
            this.btnMenu16.BackColor = System.Drawing.Color.Red;
            this.btnMenu16.FlatAppearance.BorderSize = 0;
            this.btnMenu16.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMenu16.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.btnMenu16.ForeColor = System.Drawing.Color.White;
            this.btnMenu16.Location = new System.Drawing.Point(188, 78);
            this.btnMenu16.Name = "btnMenu16";
            this.btnMenu16.Size = new System.Drawing.Size(176, 66);
            this.btnMenu16.TabIndex = 50;
            this.btnMenu16.Text = "Pengiriman POT";
            this.btnMenu16.UseVisualStyleBackColor = false;
            this.btnMenu16.Click += new System.EventHandler(this.btnMenu16_Click);
            // 
            // btnMenu15
            // 
            this.btnMenu15.BackColor = System.Drawing.Color.Red;
            this.btnMenu15.FlatAppearance.BorderSize = 0;
            this.btnMenu15.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMenu15.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.btnMenu15.ForeColor = System.Drawing.Color.White;
            this.btnMenu15.Location = new System.Drawing.Point(6, 78);
            this.btnMenu15.Name = "btnMenu15";
            this.btnMenu15.Size = new System.Drawing.Size(176, 66);
            this.btnMenu15.TabIndex = 49;
            this.btnMenu15.Text = "Received Direct Owner";
            this.btnMenu15.UseVisualStyleBackColor = false;
            this.btnMenu15.Click += new System.EventHandler(this.btnMenu15_Click);
            // 
            // btnMenu7
            // 
            this.btnMenu7.BackColor = System.Drawing.Color.Red;
            this.btnMenu7.FlatAppearance.BorderSize = 0;
            this.btnMenu7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMenu7.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.btnMenu7.ForeColor = System.Drawing.Color.White;
            this.btnMenu7.Location = new System.Drawing.Point(370, 6);
            this.btnMenu7.Name = "btnMenu7";
            this.btnMenu7.Size = new System.Drawing.Size(176, 66);
            this.btnMenu7.TabIndex = 47;
            this.btnMenu7.Text = "Pengiriman FT Port PAMA";
            this.btnMenu7.UseVisualStyleBackColor = false;
            this.btnMenu7.Click += new System.EventHandler(this.btnMenu7_Click);
            // 
            // btnMenu6
            // 
            this.btnMenu6.BackColor = System.Drawing.Color.Red;
            this.btnMenu6.FlatAppearance.BorderSize = 0;
            this.btnMenu6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMenu6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.btnMenu6.ForeColor = System.Drawing.Color.White;
            this.btnMenu6.Location = new System.Drawing.Point(188, 6);
            this.btnMenu6.Name = "btnMenu6";
            this.btnMenu6.Size = new System.Drawing.Size(176, 66);
            this.btnMenu6.TabIndex = 46;
            this.btnMenu6.Text = "Received FT Darat";
            this.btnMenu6.UseVisualStyleBackColor = false;
            this.btnMenu6.Click += new System.EventHandler(this.btnMenu6_Click);
            // 
            // btnMenu5
            // 
            this.btnMenu5.BackColor = System.Drawing.Color.Red;
            this.btnMenu5.FlatAppearance.BorderSize = 0;
            this.btnMenu5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMenu5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.btnMenu5.ForeColor = System.Drawing.Color.White;
            this.btnMenu5.Location = new System.Drawing.Point(6, 6);
            this.btnMenu5.Name = "btnMenu5";
            this.btnMenu5.Size = new System.Drawing.Size(176, 66);
            this.btnMenu5.TabIndex = 45;
            this.btnMenu5.Text = "Received FT Port";
            this.btnMenu5.UseVisualStyleBackColor = false;
            this.btnMenu5.Click += new System.EventHandler(this.btnMenu5_Click);
            // 
            // btnMenu10
            // 
            this.btnMenu10.BackColor = System.Drawing.Color.Red;
            this.btnMenu10.FlatAppearance.BorderSize = 0;
            this.btnMenu10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMenu10.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.btnMenu10.ForeColor = System.Drawing.Color.White;
            this.btnMenu10.Location = new System.Drawing.Point(552, 6);
            this.btnMenu10.Name = "btnMenu10";
            this.btnMenu10.Size = new System.Drawing.Size(176, 66);
            this.btnMenu10.TabIndex = 48;
            this.btnMenu10.Text = "Received Direct";
            this.btnMenu10.UseVisualStyleBackColor = false;
            this.btnMenu10.Click += new System.EventHandler(this.btnMenu10_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.btnMenu18);
            this.tabPage3.Controls.Add(this.btnMenu17);
            this.tabPage3.Controls.Add(this.btnMenu14);
            this.tabPage3.Controls.Add(this.btnMenu13);
            this.tabPage3.Controls.Add(this.btnMenu12);
            this.tabPage3.Controls.Add(this.btnMenu11);
            this.tabPage3.Controls.Add(this.btnMenu9);
            this.tabPage3.Controls.Add(this.btnMenu8);
            this.tabPage3.Location = new System.Drawing.Point(4, 42);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(738, 289);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "List Data";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // btnMenu18
            // 
            this.btnMenu18.BackColor = System.Drawing.Color.Red;
            this.btnMenu18.FlatAppearance.BorderSize = 0;
            this.btnMenu18.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMenu18.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.btnMenu18.ForeColor = System.Drawing.Color.White;
            this.btnMenu18.Location = new System.Drawing.Point(553, 102);
            this.btnMenu18.Name = "btnMenu18";
            this.btnMenu18.Size = new System.Drawing.Size(176, 90);
            this.btnMenu18.TabIndex = 56;
            this.btnMenu18.Text = "List Data Pengiriman POT";
            this.btnMenu18.UseVisualStyleBackColor = false;
            this.btnMenu18.Click += new System.EventHandler(this.btnMenu18_Click);
            // 
            // btnMenu17
            // 
            this.btnMenu17.BackColor = System.Drawing.Color.Red;
            this.btnMenu17.FlatAppearance.BorderSize = 0;
            this.btnMenu17.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMenu17.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.btnMenu17.ForeColor = System.Drawing.Color.White;
            this.btnMenu17.Location = new System.Drawing.Point(371, 102);
            this.btnMenu17.Name = "btnMenu17";
            this.btnMenu17.Size = new System.Drawing.Size(176, 90);
            this.btnMenu17.TabIndex = 55;
            this.btnMenu17.Text = "List Data Receive Direct Owner";
            this.btnMenu17.UseVisualStyleBackColor = false;
            this.btnMenu17.Click += new System.EventHandler(this.btnMenu17_Click);
            // 
            // btnMenu14
            // 
            this.btnMenu14.BackColor = System.Drawing.Color.Red;
            this.btnMenu14.FlatAppearance.BorderSize = 0;
            this.btnMenu14.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMenu14.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.btnMenu14.ForeColor = System.Drawing.Color.White;
            this.btnMenu14.Location = new System.Drawing.Point(189, 102);
            this.btnMenu14.Name = "btnMenu14";
            this.btnMenu14.Size = new System.Drawing.Size(176, 90);
            this.btnMenu14.TabIndex = 54;
            this.btnMenu14.Text = "List Data Received FT Darat";
            this.btnMenu14.UseVisualStyleBackColor = false;
            this.btnMenu14.Click += new System.EventHandler(this.btnMenu14_Click);
            // 
            // btnMenu13
            // 
            this.btnMenu13.BackColor = System.Drawing.Color.Red;
            this.btnMenu13.FlatAppearance.BorderSize = 0;
            this.btnMenu13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMenu13.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.btnMenu13.ForeColor = System.Drawing.Color.White;
            this.btnMenu13.Location = new System.Drawing.Point(7, 102);
            this.btnMenu13.Name = "btnMenu13";
            this.btnMenu13.Size = new System.Drawing.Size(176, 90);
            this.btnMenu13.TabIndex = 53;
            this.btnMenu13.Text = "List Data Received FT Port";
            this.btnMenu13.UseVisualStyleBackColor = false;
            this.btnMenu13.Click += new System.EventHandler(this.btnMenu13_Click);
            // 
            // btnMenu12
            // 
            this.btnMenu12.BackColor = System.Drawing.Color.Red;
            this.btnMenu12.FlatAppearance.BorderSize = 0;
            this.btnMenu12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMenu12.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.btnMenu12.ForeColor = System.Drawing.Color.White;
            this.btnMenu12.Location = new System.Drawing.Point(553, 6);
            this.btnMenu12.Name = "btnMenu12";
            this.btnMenu12.Size = new System.Drawing.Size(176, 90);
            this.btnMenu12.TabIndex = 52;
            this.btnMenu12.Text = "List Data Pengiriman FT Port PAMA";
            this.btnMenu12.UseVisualStyleBackColor = false;
            this.btnMenu12.Click += new System.EventHandler(this.btnMenu12_Click);
            // 
            // btnMenu11
            // 
            this.btnMenu11.BackColor = System.Drawing.Color.Red;
            this.btnMenu11.FlatAppearance.BorderSize = 0;
            this.btnMenu11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMenu11.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.btnMenu11.ForeColor = System.Drawing.Color.White;
            this.btnMenu11.Location = new System.Drawing.Point(371, 6);
            this.btnMenu11.Name = "btnMenu11";
            this.btnMenu11.Size = new System.Drawing.Size(176, 90);
            this.btnMenu11.TabIndex = 51;
            this.btnMenu11.Text = "List Data Receive Direct";
            this.btnMenu11.UseVisualStyleBackColor = false;
            this.btnMenu11.Click += new System.EventHandler(this.btnMenu11_Click);
            // 
            // btnMenu9
            // 
            this.btnMenu9.BackColor = System.Drawing.Color.Green;
            this.btnMenu9.FlatAppearance.BorderSize = 0;
            this.btnMenu9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMenu9.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.btnMenu9.ForeColor = System.Drawing.Color.White;
            this.btnMenu9.Location = new System.Drawing.Point(189, 6);
            this.btnMenu9.Name = "btnMenu9";
            this.btnMenu9.Size = new System.Drawing.Size(176, 90);
            this.btnMenu9.TabIndex = 50;
            this.btnMenu9.Text = "List Data Transfer";
            this.btnMenu9.UseVisualStyleBackColor = false;
            this.btnMenu9.Click += new System.EventHandler(this.btnMenu9_Click);
            // 
            // btnMenu8
            // 
            this.btnMenu8.BackColor = System.Drawing.Color.Green;
            this.btnMenu8.FlatAppearance.BorderSize = 0;
            this.btnMenu8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMenu8.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.btnMenu8.ForeColor = System.Drawing.Color.White;
            this.btnMenu8.Location = new System.Drawing.Point(7, 6);
            this.btnMenu8.Name = "btnMenu8";
            this.btnMenu8.Size = new System.Drawing.Size(176, 90);
            this.btnMenu8.TabIndex = 49;
            this.btnMenu8.Text = "List Data Issued";
            this.btnMenu8.UseVisualStyleBackColor = false;
            this.btnMenu8.Click += new System.EventHandler(this.btnMenu8_Click);
            // 
            // tpWarehouse
            // 
            this.tpWarehouse.Controls.Add(this.panel1);
            this.tpWarehouse.Controls.Add(this.cbWarehouseName);
            this.tpWarehouse.Controls.Add(this.label5);
            this.tpWarehouse.Controls.Add(this.dateTimeShift2);
            this.tpWarehouse.Controls.Add(this.dateTimeShift1);
            this.tpWarehouse.Controls.Add(this.label6);
            this.tpWarehouse.Controls.Add(this.label7);
            this.tpWarehouse.Controls.Add(this.comboBoxSerialNumber);
            this.tpWarehouse.Controls.Add(this.comboBoxWarehouse);
            this.tpWarehouse.Controls.Add(this.label8);
            this.tpWarehouse.Controls.Add(this.label9);
            this.tpWarehouse.Location = new System.Drawing.Point(4, 42);
            this.tpWarehouse.Name = "tpWarehouse";
            this.tpWarehouse.Padding = new System.Windows.Forms.Padding(3);
            this.tpWarehouse.Size = new System.Drawing.Size(820, 670);
            this.tpWarehouse.TabIndex = 2;
            this.tpWarehouse.Text = "WHouse 1";
            this.tpWarehouse.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnNewConfig);
            this.panel1.Controls.Add(this.btnSubmit);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(3, 567);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(814, 100);
            this.panel1.TabIndex = 39;
            // 
            // btnNewConfig
            // 
            this.btnNewConfig.BackColor = System.Drawing.Color.Blue;
            this.btnNewConfig.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnNewConfig.FlatAppearance.BorderSize = 0;
            this.btnNewConfig.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNewConfig.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNewConfig.ForeColor = System.Drawing.Color.White;
            this.btnNewConfig.Location = new System.Drawing.Point(0, -76);
            this.btnNewConfig.Name = "btnNewConfig";
            this.btnNewConfig.Size = new System.Drawing.Size(814, 88);
            this.btnNewConfig.TabIndex = 38;
            this.btnNewConfig.Text = "NEW CONFIG";
            this.btnNewConfig.UseVisualStyleBackColor = false;
            this.btnNewConfig.Visible = false;
            this.btnNewConfig.Click += new System.EventHandler(this.btnNewConfig_Click);
            // 
            // btnSubmit
            // 
            this.btnSubmit.BackColor = System.Drawing.Color.Green;
            this.btnSubmit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnSubmit.FlatAppearance.BorderSize = 0;
            this.btnSubmit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSubmit.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSubmit.ForeColor = System.Drawing.Color.White;
            this.btnSubmit.Location = new System.Drawing.Point(0, 12);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(814, 88);
            this.btnSubmit.TabIndex = 37;
            this.btnSubmit.Text = "SAVE";
            this.btnSubmit.UseVisualStyleBackColor = false;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // cbWarehouseName
            // 
            this.cbWarehouseName.DropDownHeight = 150;
            this.cbWarehouseName.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbWarehouseName.FormattingEnabled = true;
            this.cbWarehouseName.IntegralHeight = false;
            this.cbWarehouseName.Location = new System.Drawing.Point(321, 66);
            this.cbWarehouseName.Name = "cbWarehouseName";
            this.cbWarehouseName.Size = new System.Drawing.Size(340, 50);
            this.cbWarehouseName.Sorted = true;
            this.cbWarehouseName.TabIndex = 36;
            this.cbWarehouseName.SelectedIndexChanged += new System.EventHandler(this.cbWarehouseName_SelectedIndexChanged);
            this.cbWarehouseName.Click += new System.EventHandler(this.cbWarehouseName_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(-4, 66);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(319, 42);
            this.label5.TabIndex = 35;
            this.label5.Text = "Warehouse Name";
            // 
            // dateTimeShift2
            // 
            this.dateTimeShift2.CustomFormat = "HH:mm";
            this.dateTimeShift2.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimeShift2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimeShift2.Location = new System.Drawing.Point(486, 193);
            this.dateTimeShift2.Name = "dateTimeShift2";
            this.dateTimeShift2.ShowUpDown = true;
            this.dateTimeShift2.Size = new System.Drawing.Size(175, 49);
            this.dateTimeShift2.TabIndex = 33;
            this.dateTimeShift2.Value = new System.DateTime(2022, 3, 17, 0, 0, 0, 0);
            // 
            // dateTimeShift1
            // 
            this.dateTimeShift1.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimeShift1.CustomFormat = "HH:mm";
            this.dateTimeShift1.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimeShift1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimeShift1.Location = new System.Drawing.Point(184, 193);
            this.dateTimeShift1.Name = "dateTimeShift1";
            this.dateTimeShift1.ShowUpDown = true;
            this.dateTimeShift1.Size = new System.Drawing.Size(167, 49);
            this.dateTimeShift1.TabIndex = 32;
            this.dateTimeShift1.Value = new System.DateTime(2022, 3, 17, 0, 0, 0, 0);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(357, 193);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(123, 42);
            this.label6.TabIndex = 31;
            this.label6.Text = "Shift 2";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(50, 193);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(123, 42);
            this.label7.TabIndex = 30;
            this.label7.Text = "Shift 1";
            // 
            // comboBoxSerialNumber
            // 
            this.comboBoxSerialNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxSerialNumber.FormattingEnabled = true;
            this.comboBoxSerialNumber.Location = new System.Drawing.Point(321, 125);
            this.comboBoxSerialNumber.Name = "comboBoxSerialNumber";
            this.comboBoxSerialNumber.Size = new System.Drawing.Size(340, 50);
            this.comboBoxSerialNumber.TabIndex = 29;
            this.comboBoxSerialNumber.SelectedIndexChanged += new System.EventHandler(this.comboBoxSerialNumber_SelectedIndexChanged);
            // 
            // comboBoxWarehouse
            // 
            this.comboBoxWarehouse.DropDownHeight = 150;
            this.comboBoxWarehouse.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxWarehouse.FormattingEnabled = true;
            this.comboBoxWarehouse.IntegralHeight = false;
            this.comboBoxWarehouse.Location = new System.Drawing.Point(321, 6);
            this.comboBoxWarehouse.Name = "comboBoxWarehouse";
            this.comboBoxWarehouse.Size = new System.Drawing.Size(340, 50);
            this.comboBoxWarehouse.Sorted = true;
            this.comboBoxWarehouse.TabIndex = 28;
            this.comboBoxWarehouse.SelectedIndexChanged += new System.EventHandler(this.comboBoxWarehouse_SelectedIndexChanged);
            this.comboBoxWarehouse.Click += new System.EventHandler(this.comboBoxWarehouse_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(3, 125);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(170, 42);
            this.label8.TabIndex = 27;
            this.label8.Text = "SN Code";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(-4, 6);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(256, 42);
            this.label9.TabIndex = 26;
            this.label9.Text = "Warehouse ID";
            // 
            // tpWarehouse2
            // 
            this.tpWarehouse2.Controls.Add(this.panel2);
            this.tpWarehouse2.Controls.Add(this.cbWarehouseName2);
            this.tpWarehouse2.Controls.Add(this.label12);
            this.tpWarehouse2.Controls.Add(this.dateTimeShift22);
            this.tpWarehouse2.Controls.Add(this.dateTimeShift12);
            this.tpWarehouse2.Controls.Add(this.label13);
            this.tpWarehouse2.Controls.Add(this.label14);
            this.tpWarehouse2.Controls.Add(this.comboBoxSerialNumberwh2);
            this.tpWarehouse2.Controls.Add(this.comboBoxWarehouse2);
            this.tpWarehouse2.Controls.Add(this.label15);
            this.tpWarehouse2.Controls.Add(this.label16);
            this.tpWarehouse2.Location = new System.Drawing.Point(4, 42);
            this.tpWarehouse2.Name = "tpWarehouse2";
            this.tpWarehouse2.Padding = new System.Windows.Forms.Padding(3);
            this.tpWarehouse2.Size = new System.Drawing.Size(820, 670);
            this.tpWarehouse2.TabIndex = 3;
            this.tpWarehouse2.Text = "WHouse 2";
            this.tpWarehouse2.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnNewConfig2);
            this.panel2.Controls.Add(this.btnSubmit2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(3, 567);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(814, 100);
            this.panel2.TabIndex = 49;
            // 
            // btnNewConfig2
            // 
            this.btnNewConfig2.BackColor = System.Drawing.Color.Blue;
            this.btnNewConfig2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnNewConfig2.FlatAppearance.BorderSize = 0;
            this.btnNewConfig2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNewConfig2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNewConfig2.ForeColor = System.Drawing.Color.White;
            this.btnNewConfig2.Location = new System.Drawing.Point(0, -76);
            this.btnNewConfig2.Name = "btnNewConfig2";
            this.btnNewConfig2.Size = new System.Drawing.Size(814, 88);
            this.btnNewConfig2.TabIndex = 38;
            this.btnNewConfig2.Text = "NEW CONFIG";
            this.btnNewConfig2.UseVisualStyleBackColor = false;
            this.btnNewConfig2.Visible = false;
            this.btnNewConfig2.Click += new System.EventHandler(this.btnNewConfig2_Click);
            // 
            // btnSubmit2
            // 
            this.btnSubmit2.BackColor = System.Drawing.Color.Green;
            this.btnSubmit2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnSubmit2.FlatAppearance.BorderSize = 0;
            this.btnSubmit2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSubmit2.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSubmit2.ForeColor = System.Drawing.Color.White;
            this.btnSubmit2.Location = new System.Drawing.Point(0, 12);
            this.btnSubmit2.Name = "btnSubmit2";
            this.btnSubmit2.Size = new System.Drawing.Size(814, 88);
            this.btnSubmit2.TabIndex = 37;
            this.btnSubmit2.Text = "SAVE";
            this.btnSubmit2.UseVisualStyleBackColor = false;
            this.btnSubmit2.Click += new System.EventHandler(this.btnSubmit2_Click_1);
            // 
            // cbWarehouseName2
            // 
            this.cbWarehouseName2.DropDownHeight = 150;
            this.cbWarehouseName2.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbWarehouseName2.FormattingEnabled = true;
            this.cbWarehouseName2.IntegralHeight = false;
            this.cbWarehouseName2.Location = new System.Drawing.Point(323, 67);
            this.cbWarehouseName2.Name = "cbWarehouseName2";
            this.cbWarehouseName2.Size = new System.Drawing.Size(340, 50);
            this.cbWarehouseName2.Sorted = true;
            this.cbWarehouseName2.TabIndex = 48;
            this.cbWarehouseName2.SelectedIndexChanged += new System.EventHandler(this.cbWarehouseName2_SelectedIndexChanged);
            this.cbWarehouseName2.Click += new System.EventHandler(this.cbWarehouseName2_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(-2, 67);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(319, 42);
            this.label12.TabIndex = 47;
            this.label12.Text = "Warehouse Name";
            // 
            // dateTimeShift22
            // 
            this.dateTimeShift22.CustomFormat = "HH:mm";
            this.dateTimeShift22.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimeShift22.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimeShift22.Location = new System.Drawing.Point(491, 193);
            this.dateTimeShift22.Name = "dateTimeShift22";
            this.dateTimeShift22.ShowUpDown = true;
            this.dateTimeShift22.Size = new System.Drawing.Size(175, 49);
            this.dateTimeShift22.TabIndex = 46;
            this.dateTimeShift22.Value = new System.DateTime(2022, 3, 17, 0, 0, 0, 0);
            // 
            // dateTimeShift12
            // 
            this.dateTimeShift12.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimeShift12.CustomFormat = "HH:mm";
            this.dateTimeShift12.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimeShift12.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimeShift12.Location = new System.Drawing.Point(165, 195);
            this.dateTimeShift12.Name = "dateTimeShift12";
            this.dateTimeShift12.ShowUpDown = true;
            this.dateTimeShift12.Size = new System.Drawing.Size(167, 49);
            this.dateTimeShift12.TabIndex = 45;
            this.dateTimeShift12.Value = new System.DateTime(2022, 3, 17, 0, 0, 0, 0);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(362, 200);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(123, 42);
            this.label13.TabIndex = 44;
            this.label13.Text = "Shift 2";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(35, 200);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(123, 42);
            this.label14.TabIndex = 43;
            this.label14.Text = "Shift 1";
            // 
            // comboBoxSerialNumberwh2
            // 
            this.comboBoxSerialNumberwh2.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxSerialNumberwh2.FormattingEnabled = true;
            this.comboBoxSerialNumberwh2.Location = new System.Drawing.Point(323, 128);
            this.comboBoxSerialNumberwh2.Name = "comboBoxSerialNumberwh2";
            this.comboBoxSerialNumberwh2.Size = new System.Drawing.Size(340, 50);
            this.comboBoxSerialNumberwh2.TabIndex = 42;
            this.comboBoxSerialNumberwh2.SelectedIndexChanged += new System.EventHandler(this.comboBoxSerialNumber2_SelectedIndexChanged);
            // 
            // comboBoxWarehouse2
            // 
            this.comboBoxWarehouse2.DropDownHeight = 150;
            this.comboBoxWarehouse2.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxWarehouse2.FormattingEnabled = true;
            this.comboBoxWarehouse2.IntegralHeight = false;
            this.comboBoxWarehouse2.Location = new System.Drawing.Point(323, 6);
            this.comboBoxWarehouse2.Name = "comboBoxWarehouse2";
            this.comboBoxWarehouse2.Size = new System.Drawing.Size(340, 50);
            this.comboBoxWarehouse2.Sorted = true;
            this.comboBoxWarehouse2.TabIndex = 41;
            this.comboBoxWarehouse2.SelectedIndexChanged += new System.EventHandler(this.comboBoxWarehouse2_SelectedIndexChanged);
            this.comboBoxWarehouse2.Click += new System.EventHandler(this.comboBoxWarehouse2_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(6, 128);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(170, 42);
            this.label15.TabIndex = 40;
            this.label15.Text = "SN Code";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(3, 6);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(256, 42);
            this.label16.TabIndex = 39;
            this.label16.Text = "Warehouse ID";
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.BackColor = System.Drawing.Color.Red;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold);
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(712, 2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(112, 35);
            this.button2.TabIndex = 18;
            this.button2.Text = "EXIT";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // FUserSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(828, 716);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FUserSetting";
            this.Text = "FUserSetting";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FUserSetting_Load);
            this.tabControl1.ResumeLayout(false);
            this.tpConnection.ResumeLayout(false);
            this.tpConnection.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numCheckOnlineLogin)).EndInit();
            this.tpUserSetting.ResumeLayout(false);
            this.tpUserSetting.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numIntervalAutoSync)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numLCRWholeTenth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDataPerKirimSync)).EndInit();
            this.pMenu7Inch.ResumeLayout(false);
            this.pMenu10Inch.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tpWarehouse.ResumeLayout(false);
            this.tpWarehouse.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.tpWarehouse2.ResumeLayout(false);
            this.tpWarehouse2.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnConnection;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tpConnection;
        private System.Windows.Forms.TabPage tpUserSetting;
        private TabPage tpWarehouse;
        private Button btnSubmit;
        private ComboBox cbWarehouseName;
        private Label label5;
        private DateTimePicker dateTimeShift2;
        private DateTimePicker dateTimeShift1;
        private Label label6;
        private Label label7;
        private ComboBox comboBoxSerialNumber;
        private ComboBox comboBoxWarehouse;
        private Label label8;
        private Label label9;
        private Button btnNewConfig;
        private Label label1;
        private ComboBox cbDfStatusType;
        private Button btnAutoSyncOnOff;
        private Label label3;
        private Button btnSaveUserSetting;
        private Label label4;
        private Button button2;
        private Button btnSaveConnection;
        private Panel panel1;
        private NumericUpDown numDataPerKirimSync;
        private Label label10;
        private CheckedListBox clb_menu_access;
        private Panel pMenu7Inch;
        private Button btn7InchMenu9;
        private Button btn7InchMenu2;
        private Button btn7InchMenu8;
        private Button btn7InchMenu3;
        private Panel pMenu10Inch;
        private NumericUpDown numLCRWholeTenth;
        private Label label11;
        private TabControl tabControl2;
        private TabPage tabPage1;
        private TabPage tabPage2;
        private TabPage tabPage3;
        private Button btnMenu1;
        private Button btnMenu2;
        private Button btnMenu3;
        private Button btnMenu4;
        private Button btnMenu7;
        private Button btnMenu6;
        private Button btnMenu5;
        private Button btnMenu10;
        private Button btnMenu14;
        private Button btnMenu13;
        private Button btnMenu12;
        private Button btnMenu11;
        private Button btnMenu9;
        private Button btnMenu8;
        private TabPage tpWarehouse2;
        private ComboBox cbWarehouseName2;
        private Label label12;
        private DateTimePicker dateTimeShift22;
        private DateTimePicker dateTimeShift12;
        private Label label13;
        private Label label14;
        private ComboBox comboBoxSerialNumberwh2;
        private ComboBox comboBoxWarehouse2;
        private Label label15;
        private Label label16;
        private Panel panel2;
        private Button btnNewConfig2;
        private Button btnSubmit2;
        private Button btnMenu16;
        private Button btnMenu15;
        private Button btnMenu18;
        private Button btnMenu17;
        private Button btn7InchMenu4;
        private NumericUpDown numIntervalAutoSync;
        private Label label17;
        private Label label19;
        private NumericUpDown numCheckOnlineLogin;
        private Label label20;
    }
}