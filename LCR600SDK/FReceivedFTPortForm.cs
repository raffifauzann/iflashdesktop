﻿using Flurl.Http;
using LCR600SDK.DataClass;
using MySql.Data.MySqlClient;
using System.Data.SQLite;
using System.IO;
using System.Reflection;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;

namespace LCR600SDK
{
    public partial class FReceivedFTPortForm : Form
    {
        public ListDatumRitasiMaintank pData;
        private string myConnectionString = string.Empty;
        public string whosueSelected = string.Empty;
        private string pShiftStart1 = string.Empty;
        private string pShiftStart2 = string.Empty;
        private string pWHouse = string.Empty;
        private string pWHouseCode = string.Empty;
        private string pSnCOde = string.Empty;
        private string pMeterFaktor = string.Empty;
        public string URL_API_CALL = string.Empty;
        public string CALL_FROM = string.Empty;
        SQLiteConnection conn;
        private string pUrl_service = string.Empty;
        PasswordEncrypDecrypt decryptor = new PasswordEncrypDecrypt();
        WriteLogText log = new WriteLogText();
        public FReceivedFTPortForm()
        {
            var _proper = Properties.Settings.Default;
            //myConnectionString = "Data Source=" + Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "SQLite", "db_iflash.db") + " ;New=False;";
            var passPhrase = "iFlashDesktop";
            var decryp = decryptor.DecryptPassword(_proper.db_pass, passPhrase);
            myConnectionString = "Data Source=" + Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "SQLite", "db_iflash.db") + " ;New=False;";
            conn = new SQLiteConnection(myConnectionString);
            GlobalModel.AutoRefueling = new refueling();
            InitializeComponent();
        }

        private void FReceivedFTPortForm_Load(object sender, EventArgs e)
        {
            panelHide.Hide();
            var _proper = Properties.Settings.Default;
            pUrl_service = _proper.url_service;
            btnSubmit.Enabled = false;
            initData();
            initDataMaster();
        }

        void initData()
        {
            txtPoNo.Text = pData.SendPoNo;
            txtDistrik.Text = pData.District;
            txtTranportir.Text = pData.TransportirName;
            txtDo.Text = pData.SendDoNo;
            txtFt.Text = pData.SendFtNo;
            txtSendDriverName.Text = pData.SendDriverName;
            txtReceiveDriverName.Text = pData.ReceiveDriverName;
            txtSendDate.Text = pData.SendDate.ToString();
            txtSendQty.Text = pData.SendQty.ToString();
            txtSendFrom.Text = pData.SendFrom;
            txtReceiveAt.Text = pData.ReceiveAt;
            txtSendfmAwal.Text = "0";
            txtSendfmAkhir.Text = "0";
            txtSegel1.Text = pData.SendNoSegel1;
            txtSegel2.Text = pData.SendNoSegel2;
            txtSegel3.Text = pData.SendNoSegel3;
            txtSegel4.Text = pData.SendNoSegel4;
            txtSegel5.Text = pData.SendNoSegel5;
            txtSegel6.Text = pData.SendNoSegel6;
            txtSegel7.Text = pData.SendNoSegel7;
            txtSegel8.Text = pData.SendNoSegel8;
            txtSegel9.Text = pData.SendNoSegel9;
            txtSegel10.Text = pData.SendNoSegel10;
        }

        void initDataMaster()
        {
            try
            {
                if (conn.State != ConnectionState.Open) conn.Open();
                string query = string.Empty;

                query = "SELECT * FROM tbl_setting";
                SQLiteCommand cmds = new SQLiteCommand(query, conn);
                SQLiteDataReader reader = cmds.ExecuteReader();

                while (reader.Read())
                {
                    pWHouse = reader["WhouseName"].ToString();
                    pWHouseCode = reader["Whouseid"].ToString();
                    pSnCOde = reader["SnCode"].ToString();
                    txtReceiveAt.Text = reader["WhouseName"].ToString();
                    txtSnFlowMeter.Text = reader["SnCode"].ToString();
                    pMeterFaktor = reader["MeterFakor"].ToString();
                }
                reader.Dispose();
                if (conn.State == ConnectionState.Open) conn.Close();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
                MessageBox.Show("Exception: Terjadi kesalahan saat inisialisasi, mohon cek log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(ex.Message, "initDataMaster()");
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnTerima_Click(object sender, EventArgs e)
        {

            if (txtReceiveDensity.Text == "" || txtReceiveDensity.Text == "0")
            {
                var f = new FPopUpNotification("Density tidak boleh kosong!", "Data Invalid");
                f.ShowDialog();
            }
            else if (txtReceiveTemperature.Text == "" || txtReceiveTemperature.Text == "0")
            {
                var f = new FPopUpNotification("Temperature tidak boleh kosong!", "Data Invalid");
                f.ShowDialog();
            }
            else if (txtReceiveQtyAdditive.Text == "" || txtReceiveQtyAdditive.Text == "0")
            {
                var f = new FPopUpNotification("Qty Additive tidak boleh kosong!", "Data Invalid");
                f.ShowDialog();
            }
            else
            {
                terima();
            }

        }

        void terima()
        {
            var _globalData = GlobalModel.GlobalVar;

            if (isValid())
            {
                pData.SendPoNo = txtPoNo.Text;
                pData.SendDate = DateTime.Parse(txtSendDate.Text);
                pData.ReceiveAt = txtReceiveAt.Text;
                pData.ReceiveAtCode = pWHouseCode;
                pData.ReceiveDriverName = txtReceiveDriverName.Text;
                pData.ReceiveDippingHole1Mt = txtReceiveDippingHole1Mt.Text;
                pData.ReceiveDippingHole2Mt = txtReceiveDippingHole2Mt.Text;
                pData.ReceiveMeterFaktor = double.Parse(pMeterFaktor);
                pData.ReceiveSnFlowMeter1 = txtSnFlowMeter.Text;
                pData.ReceiveFlowMeterAkhir1 = int.Parse(txtReceiveFlowMeterAkhir1.Text);
                pData.ReceiveFlowMeterAwal1 = int.Parse(txtReceiveFlowMeterAwal1.Text);
                pData.ReceiveMeterFaktor2 = double.Parse(pMeterFaktor);
                pData.ReceiveSnFlowMeter2 = txtSnFlowMeter.Text;
                pData.ReceiveFlowMeterAkhir2 = 0;
                pData.ReceiveFlowMeterAwal2 = 0;
                pData.ReceiveQty1 = int.Parse(txtReceiveQty1.Text);
                pData.ReceiveQty2 = 0;
                var density = txtReceiveDensity.Text.Replace(',', '.');
                pData.ReceiveDensity = txtReceiveDensity.Text.Replace(',', '.');
                var temperature = txtReceiveTemperature.Text.Replace(',', '.');
                var qtyAdditive = txtReceiveQtyAdditive.Text.Replace(',', '.');
                pData.ReceiveTemperature = Math.Round(Convert.ToDouble(temperature), 0, MidpointRounding.AwayFromZero).ToString();
                pData.ReceiveQtyAdditive = Math.Round(Convert.ToDouble(qtyAdditive), 0, MidpointRounding.AwayFromZero).ToString();

                pData.LastModBy = _globalData.DataEmp.Nrp;
                //pData.LastModDate = DateTime.Now;
                int receive_qty1_x_mf = int.Parse(txtReceiveQty1.Text, System.Globalization.CultureInfo.InvariantCulture);
                int receive_qty2_x_mf = 0;
                pData.receiveQty1xMf = receive_qty1_x_mf;
                pData.receiveQty2xMf = receive_qty2_x_mf;
                pData.receiveQtyTotal = receive_qty1_x_mf + receive_qty2_x_mf;
                pData.ReceiveNoSegel1 = ckSegel1.Checked ? txtSegel1.Text.Trim() : "";
                pData.ReceiveNoSegel2 = ckSegel2.Checked ? txtSegel2.Text.Trim() : "";
                pData.ReceiveNoSegel3 = ckSegel3.Checked ? txtSegel3.Text.Trim() : "";
                pData.ReceiveNoSegel4 = ckSegel4.Checked ? txtSegel4.Text.Trim() : "";
                pData.ReceiveNoSegel5 = ckSegel5.Checked ? txtSegel5.Text.Trim() : "";
                pData.ReceiveNoSegel6 = ckSegel6.Checked ? txtSegel6.Text.Trim() : "";


                if (chkConfirm.Checked && pData.RefTbl.ToLower().Contains("darat"))
                {
                    pData.DocStatus = "70";
                    pData.ReceiveIscomplete = "1";
                }

                if (chkConfirm.Checked && pData.RefTbl.ToLower().Contains("port"))
                {
                    pData.DocStatus = "80";
                    pData.ReceiveIscomplete = "1";
                }

                #region data testing
                //txtReceiveFlowMeterAwal1.Text = "0";
                //txtReceiveFlowMeterAkhir1.Text = "10";
                //txtReceiveQty1.Text = "10";
                //pData.ReceiveQty1 = 10;
                //pData.receiveQty1xMf = pData.ReceiveQty1 * (int)Math.Round(double.Parse(pData.ReceiveMeterFaktor.ToString().Replace('.', ','), new System.Globalization.CultureInfo("id-ID")));                
                //pData.ReceiveFlowMeterAwal1 = 0;
                //pData.ReceiveFlowMeterAkhir1 = 10;
                //pData.TimeStart = DateTime.Now;
                //pData.TimeEnd = DateTime.Now;
                #endregion


                GlobalModel.ListDatumRitasiMaintank = pData;
                //btnSubmit.Enabled = true;

                var f = new FmManual();
                f.preset = 0 * 10;
                f.trans_type_select = "PORT_RECEIVED";
                GlobalModel.AutoRefueling.trans_type_select = f.trans_type_select;
                f.waitCounterMax = 0 * 10;
                f.ListDatumRitasiMaintank = pData;
                if (f.ShowDialog() == DialogResult.OK)
                {
                    txtReceiveQty1.Text = Convert.ToString(f.quantity);
                    txtReceiveFlowMeterAwal1.Text = Convert.ToString(f.totalizer_awal);
                    txtReceiveFlowMeterAkhir1.Text = Convert.ToString(f.totalizer_akhir);
                    //_backToCallForm();
                    btnSubmit.Enabled = true;
                }
                else
                {
                    txtReceiveQty1.Text = Convert.ToString(f.quantity);
                    txtReceiveFlowMeterAwal1.Text = Convert.ToString(f.totalizer_awal);
                    txtReceiveFlowMeterAkhir1.Text = Convert.ToString(f.totalizer_akhir);
                }
            }
        }

        bool isValid()
        {
            return true;
        }
        void save_error_log(string data, string actions)
        {
            try
            {
                DateTime localDate = DateTime.Now;
                var _item = new tbl_log();
                var initData = GlobalModel.GlobalVar;

                _item.pid = System.Guid.NewGuid().ToString();
                _item.actions = $"District {initData.DataEmp.DstrctCode}/FRecievedFTPortForm/{actions}";
                _item.message = $"{data}";
                _item.mod_date = localDate;
                _item.mod_by = initData.DataEmp.Nrp;

                SQLiteConnection conn;

                conn = new SQLiteConnection(myConnectionString);
                if (conn.State != ConnectionState.Open) conn.Open();
                string query = string.Empty;
                query = "INSERT INTO tbl_log(pid,actions,message,mod_date,mod_by)"
                      + " VALUES(@pid,@actions,@message,@mod_date,@mod_by)";

                SQLiteCommand cmd = new SQLiteCommand(query, conn);

                cmd.Parameters.Add(new SQLiteParameter("@pid", _item.pid));
                cmd.Parameters.Add(new SQLiteParameter("@actions", _item.actions));
                cmd.Parameters.Add(new SQLiteParameter("@message", _item.message));
                cmd.Parameters.Add(new SQLiteParameter("@mod_date", _item.mod_date));
                cmd.Parameters.Add(new SQLiteParameter("@mod_by", _item.mod_by));
                cmd.ExecuteNonQuery();
                if (conn.State == ConnectionState.Open) conn.Close();
            }
            catch (Exception)
            {
            }
        }
        public async void submitData()
        {
            var _auth = GlobalModel.loginModel;
            pData = GlobalModel.ListDatumRitasiMaintank;
            String _url = $"{pUrl_service}api/FuelService/receiveRitasiToMaintank";

            try
            {

                //#region Decimal Digit LCR = WHOLE
                //var qty1 = int.Parse(txtReceiveQty1.Text) * 10;
                //var totalAwal1 = int.Parse(txtReceiveFlowMeterAwal1.Text) * 10;
                //var totalAkhir1 = int.Parse(txtReceiveFlowMeterAkhir1.Text) * 10;                
                //pData.ReceiveQty1 = qty1;                
                //int receive_qty1_x_mf = int.Parse(qty1.ToString(), System.Globalization.CultureInfo.InvariantCulture);
                //int receive_qty2_x_mf = 0;
                //pData.receiveQty1xMf = receive_qty1_x_mf;
                //pData.receiveQty2xMf = receive_qty2_x_mf;
                //pData.receiveQtyTotal = receive_qty1_x_mf + receive_qty2_x_mf;
                //#endregion
                returnValue data = await _url.WithHeaders(new { Authorization = _auth.token })
                       .PostJsonAsync(pData).ReceiveJson<returnValue>();
                save_error_log(data.remarks, "submitData()");
                if (data.status)
                {                    
                    MessageBox.Show("Data Submitted", MessageBoxButtons.OK.ToString());
                    submitDataToLocalSuccess();
                    _backToCallForm();                    
                    this.Close();
                }
                else
                {
                    MessageBox.Show(data.remarks, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    log.WriteToFile("logLCR600.txt", data.remarks);
                    save_error_log(data.remarks, "submitData");
                    MessageBox.Show("Menyimpan data transaksi ke local", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    var res = submitDataToLocal();
                    if (res == 1)
                    {
                        var f = new FPopUpNotification("Berhasil menyimpan data pada lokal!", "Simpan Data Receive FT");
                        f.ShowDialog();
                        this.Close();
                    }
                }
                    

            }
            catch (FlurlHttpException ex)
            {
                var resp = ex.Message;
                if (ex.InnerException != null)
                {
                    resp = ex.InnerException.Message;
                }
                //var resp = await ex.GetResponseStringAsync();
                MessageBox.Show("Exception: Terjadi kesalahan saat submit data, mohon cek log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", resp);
                save_error_log(resp, "submitData()");


            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                MessageBox.Show("Exception: Terjadi kesalahan saat submit data, mohon cek log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(ex.Message, "submitData()");
            }
        }

        void _backToCallForm()
        {
            switch (CALL_FROM)
            {
                case "RECV_DARAT":
                    if (Application.OpenForms["FReceivedFTDaratSub"] != null)
                        (Application.OpenForms["FReceivedFTDaratSub"] as FReceivedFTDaratSub).loadGridAsync(URL_API_CALL);
                    break;
                case "RECV_PORT":
                    if (Application.OpenForms["FReceivedFTPort"] != null)
                        (Application.OpenForms["FReceivedFTPort"] as FReceivedFTPort).loadGridAsync(URL_API_CALL);
                    break;
            }
            //this.Close();
        }
        private void btnRecordLcr_Click(object sender, EventArgs e)
        {

        }

        private async void btnSubmit_Click(object sender, EventArgs e)
        {
            var _confirm = MessageBox.Show("Anda yakin data sudah benar?", "Confirm submit!!", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (_confirm == DialogResult.Yes)
            {
                var checkconnect = await checkConnectivity();
                if (checkconnect.Contains("Supply Service"))
                {
                    submitData();                    
                }
                else
                {
                    var res = submitDataToLocal();
                    if (res == 1)
                    {
                        var f = new FPopUpNotification("Berhasil menyimpan data pada lokal!", "Simpan Data Pengiriman FT Port Pama");
                        f.ShowDialog();
                        this.Close();
                    }
                }
            }
        }

        int submitDataToLocalSuccess()
        {
            try
            {
                var _item = pData;
                SQLiteConnection conn;

                conn = new SQLiteConnection(myConnectionString);
                if (conn.State != ConnectionState.Open) conn.Open();
                string query = string.Empty;
                int recs = 0;
                query = "INSERT INTO tbl_t_receive_maintank_ritasi(" +
                    "pid_ritasi_maintank" +
                    ",district" +
                    ",send_date" +
                    ",send_po_no" +
                    ",receive_at" +
                    ",receive_at_code" +
                    ",receive_dipping_hole1_mt" +
                    ",receive_dipping_hole2_mt" +
                    ",receive_sn_flow_meter1" +
                    ",receive_meter_faktor" +
                    ",receive_flow_meter_awal1" +
                    ",receive_flow_meter_akhir1" +
                    ",receive_qty1" +
                    ",receive_qty1_x_mf" +
                    ",receive_sn_flow_meter2" +
                    ",receive_meter_faktor2" +
                    ",receive_flow_meter_awal2" +
                    ",receive_flow_meter_akhir2" +
                    ",receive_qty2" +
                    ",receive_qty2_x_mf" +
                    ",receive_qty_total" +
                    ",receive_density" +
                    ",receive_temperature" +
                    ",receive_qty_additive" +
                    ",receive_iscomplete" +
                    ",receive_driver_name" +
                    ",receive_no_segel_1" +
                    ",receive_no_segel_2" +
                    ",receive_no_segel_3" +
                    ",receive_no_segel_4" +
                    ",receive_no_segel_5" +
                    ",receive_no_segel_6" +
                    ",doc_status" +
                    ",last_mod_by" +
                    ",foto" +
                    ",time_start" +
                    ",time_end" +
                    ",syncs)"
                      + " VALUES(" +
                        "@pid_ritasi_maintank" +
                        ",@district" +
                        ",@send_date" +
                        ",@send_po_no" +
                        ",@receive_at" +
                        ",@receive_at_code" +
                        ",@receive_dipping_hole1_mt" +
                        ",@receive_dipping_hole2_mt" +
                        ",@receive_sn_flow_meter1" +
                        ",@receive_meter_faktor" +
                        ",@receive_flow_meter_awal1" +
                        ",@receive_flow_meter_akhir1" +
                        ",@receive_qty1" +
                        ",@receive_qty1_x_mf" +
                        ",@receive_sn_flow_meter2" +
                        ",@receive_meter_faktor2" +
                        ",@receive_flow_meter_awal2" +
                        ",@receive_flow_meter_akhir2" +
                        ",@receive_qty2" +
                        ",@receive_qty2_x_mf" +
                        ",@receive_qty_total" +
                        ",@receive_density" +
                        ",@receive_temperature" +
                        ",@receive_qty_additive" +
                        ",@receive_iscomplete" +
                        ",@receive_driver_name" +
                        ",@receive_no_segel_1" +
                        ",@receive_no_segel_2" +
                        ",@receive_no_segel_3" +
                        ",@receive_no_segel_4" +
                        ",@receive_no_segel_5" +
                        ",@receive_no_segel_6" +
                        ",@doc_status" +
                        ",@last_mod_by" +
                        ",@foto" +
                        ",@time_start" +
                        ",@time_end" +
                        ",@syncs)";

                SQLiteCommand cmd = new SQLiteCommand(query, conn);



                cmd.Parameters.Add(new SQLiteParameter("@pid_ritasi_maintank", _item.PidRitasiMaintank));
                cmd.Parameters.Add(new SQLiteParameter("@district", _item.District));
                cmd.Parameters.Add(new SQLiteParameter("@send_date", _item.SendDate));
                cmd.Parameters.Add(new SQLiteParameter("@send_po_no", _item.SendPoNo));
                cmd.Parameters.Add(new SQLiteParameter("@receive_at", _item.ReceiveAt));
                cmd.Parameters.Add(new SQLiteParameter("@receive_at_code", _item.ReceiveAtCode));
                cmd.Parameters.Add(new SQLiteParameter("@receive_dipping_hole1_mt", _item.ReceiveDippingHole1Mt));
                cmd.Parameters.Add(new SQLiteParameter("@receive_dipping_hole2_mt", _item.ReceiveDippingHole2Mt));
                cmd.Parameters.Add(new SQLiteParameter("@receive_sn_flow_meter1", _item.ReceiveSnFlowMeter1));
                cmd.Parameters.Add(new SQLiteParameter("@receive_meter_faktor", _item.ReceiveMeterFaktor));
                cmd.Parameters.Add(new SQLiteParameter("@receive_flow_meter_awal1", _item.ReceiveFlowMeterAwal1));
                cmd.Parameters.Add(new SQLiteParameter("@receive_flow_meter_akhir1", _item.ReceiveFlowMeterAkhir1));
                cmd.Parameters.Add(new SQLiteParameter("@receive_qty1", _item.ReceiveQty1));
                cmd.Parameters.Add(new SQLiteParameter("@receive_qty1_x_mf", _item.receiveQty1xMf));
                cmd.Parameters.Add(new SQLiteParameter("@receive_sn_flow_meter2", _item.ReceiveSnFlowMeter2));
                cmd.Parameters.Add(new SQLiteParameter("@receive_meter_faktor2", _item.ReceiveMeterFaktor2));
                cmd.Parameters.Add(new SQLiteParameter("@receive_flow_meter_awal2", _item.ReceiveFlowMeterAwal2));
                cmd.Parameters.Add(new SQLiteParameter("@receive_flow_meter_akhir2", _item.ReceiveFlowMeterAkhir2));
                cmd.Parameters.Add(new SQLiteParameter("@receive_qty2", _item.ReceiveQty2));
                cmd.Parameters.Add(new SQLiteParameter("@receive_qty2_x_mf", _item.receiveQty2xMf));
                cmd.Parameters.Add(new SQLiteParameter("@receive_qty_total", _item.receiveQtyTotal));
                cmd.Parameters.Add(new SQLiteParameter("@receive_density", _item.ReceiveDensity));
                cmd.Parameters.Add(new SQLiteParameter("@receive_temperature", _item.ReceiveTemperature));
                cmd.Parameters.Add(new SQLiteParameter("@receive_qty_additive", _item.ReceiveQtyAdditive));
                cmd.Parameters.Add(new SQLiteParameter("@receive_iscomplete", "1"));
                cmd.Parameters.Add(new SQLiteParameter("@receive_driver_name", _item.ReceiveDriverName));
                cmd.Parameters.Add(new SQLiteParameter("@receive_no_segel_1", _item.ReceiveNoSegel1));
                cmd.Parameters.Add(new SQLiteParameter("@receive_no_segel_2", _item.ReceiveNoSegel2));
                cmd.Parameters.Add(new SQLiteParameter("@receive_no_segel_3", _item.ReceiveNoSegel3));
                cmd.Parameters.Add(new SQLiteParameter("@receive_no_segel_4", _item.ReceiveNoSegel4));
                cmd.Parameters.Add(new SQLiteParameter("@receive_no_segel_5", _item.ReceiveNoSegel5));
                cmd.Parameters.Add(new SQLiteParameter("@receive_no_segel_6", _item.ReceiveNoSegel6));
                if(_item.RefTbl.Contains("Receive at Port"))
                {
                    cmd.Parameters.Add(new SQLiteParameter("@doc_status", "80"));
                }
                else
                {
                    cmd.Parameters.Add(new SQLiteParameter("@doc_status", "70"));
                }                
                cmd.Parameters.Add(new SQLiteParameter("@last_mod_by", _item.LastModBy));
                cmd.Parameters.Add(new SQLiteParameter("@foto", _item.Foto));
                cmd.Parameters.Add(new SQLiteParameter("@time_start", _item.TimeStart));
                cmd.Parameters.Add(new SQLiteParameter("@time_end", _item.TimeEnd));
                cmd.Parameters.Add(new SQLiteParameter("@syncs", 7));

                recs = cmd.ExecuteNonQuery();


                if (conn.State == ConnectionState.Open) conn.Close();
                return recs;
            }
            catch (Exception ex)
            {
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(ex.ToString(), "submitDataToLocalSuccess()");
                FileToExcel("receive_ft_port");
                MessageBox.Show("Data Tidak Tersimpan dilocal", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return -1;
            }


        }

        int submitDataToLocal()
        {
            try
            {
                var _item = pData;
                SQLiteConnection conn;

                conn = new SQLiteConnection(myConnectionString);
                if (conn.State != ConnectionState.Open) conn.Open();
                string query = string.Empty;
                int recs = 0;
                query = "INSERT INTO tbl_t_receive_maintank_ritasi(" +
                    "pid_ritasi_maintank" +
                    ",district" +
                    ",send_date" +
                    ",send_po_no" +
                    ",receive_at" +
                    ",receive_at_code" +
                    ",receive_dipping_hole1_mt" +
                    ",receive_dipping_hole2_mt" +
                    ",receive_sn_flow_meter1" +
                    ",receive_meter_faktor" +
                    ",receive_flow_meter_awal1" +
                    ",receive_flow_meter_akhir1" +
                    ",receive_qty1" +
                    ",receive_qty1_x_mf" +
                    ",receive_sn_flow_meter2" +
                    ",receive_meter_faktor2" +
                    ",receive_flow_meter_awal2" +
                    ",receive_flow_meter_akhir2" +
                    ",receive_qty2" +
                    ",receive_qty2_x_mf" +
                    ",receive_qty_total" +
                    ",receive_density" +
                    ",receive_temperature" +
                    ",receive_qty_additive" +
                    ",receive_iscomplete" +
                    ",receive_driver_name" +
                    ",receive_no_segel_1" +
                    ",receive_no_segel_2" +
                    ",receive_no_segel_3" +
                    ",receive_no_segel_4" +
                    ",receive_no_segel_5" +
                    ",receive_no_segel_6" +
                    ",doc_status" +
                    ",last_mod_by" +
                    ",foto" +
                    ",time_start" +
                    ",time_end" +
                    ",syncs)"
                      + " VALUES(" +
                        "@pid_ritasi_maintank" +
                        ",@district" +
                         ",@send_date" +
                        ",@send_po_no" +
                        ",@receive_at" +
                        ",@receive_at_code" +
                        ",@receive_dipping_hole1_mt" +
                        ",@receive_dipping_hole2_mt" +
                        ",@receive_sn_flow_meter1" +
                        ",@receive_meter_faktor" +
                        ",@receive_flow_meter_awal1" +
                        ",@receive_flow_meter_akhir1" +
                        ",@receive_qty1" +
                        ",@receive_qty1_x_mf" +
                        ",@receive_sn_flow_meter2" +
                        ",@receive_meter_faktor2" +
                        ",@receive_flow_meter_awal2" +
                        ",@receive_flow_meter_akhir2" +
                        ",@receive_qty2" +
                        ",@receive_qty2_x_mf" +
                        ",@receive_qty_total" +
                        ",@receive_density" +
                        ",@receive_temperature" +
                        ",@receive_qty_additive" +
                        ",@receive_iscomplete" +
                        ",@receive_driver_name" +
                        ",@receive_no_segel_1" +
                        ",@receive_no_segel_2" +
                        ",@receive_no_segel_3" +
                        ",@receive_no_segel_4" +
                        ",@receive_no_segel_5" +
                        ",@receive_no_segel_6" +
                        ",@doc_status" +
                        ",@last_mod_by" +
                        ",@foto" +
                        ",@time_start" +
                        ",@time_end" +
                        ",@syncs)";

                SQLiteCommand cmd = new SQLiteCommand(query, conn);



                cmd.Parameters.Add(new SQLiteParameter("@pid_ritasi_maintank", _item.PidRitasiMaintank));
                cmd.Parameters.Add(new SQLiteParameter("@district", _item.District));
                cmd.Parameters.Add(new SQLiteParameter("@send_date", _item.SendDate));
                cmd.Parameters.Add(new SQLiteParameter("@send_po_no", _item.SendPoNo));
                cmd.Parameters.Add(new SQLiteParameter("@receive_at", _item.ReceiveAt));
                cmd.Parameters.Add(new SQLiteParameter("@receive_at_code", _item.ReceiveAtCode));
                cmd.Parameters.Add(new SQLiteParameter("@receive_dipping_hole1_mt", _item.ReceiveDippingHole1Mt));
                cmd.Parameters.Add(new SQLiteParameter("@receive_dipping_hole2_mt", _item.ReceiveDippingHole2Mt));
                cmd.Parameters.Add(new SQLiteParameter("@receive_sn_flow_meter1", _item.ReceiveSnFlowMeter1));
                cmd.Parameters.Add(new SQLiteParameter("@receive_meter_faktor", _item.ReceiveMeterFaktor));
                cmd.Parameters.Add(new SQLiteParameter("@receive_flow_meter_awal1", _item.ReceiveFlowMeterAwal1));
                cmd.Parameters.Add(new SQLiteParameter("@receive_flow_meter_akhir1", _item.ReceiveFlowMeterAkhir1));
                cmd.Parameters.Add(new SQLiteParameter("@receive_qty1", _item.ReceiveQty1));
                cmd.Parameters.Add(new SQLiteParameter("@receive_qty1_x_mf", _item.receiveQty1xMf));
                cmd.Parameters.Add(new SQLiteParameter("@receive_sn_flow_meter2", _item.ReceiveSnFlowMeter2));
                cmd.Parameters.Add(new SQLiteParameter("@receive_meter_faktor2", _item.ReceiveMeterFaktor2));
                cmd.Parameters.Add(new SQLiteParameter("@receive_flow_meter_awal2", _item.ReceiveFlowMeterAwal2));
                cmd.Parameters.Add(new SQLiteParameter("@receive_flow_meter_akhir2", _item.ReceiveFlowMeterAkhir2));
                cmd.Parameters.Add(new SQLiteParameter("@receive_qty2", _item.ReceiveQty2));
                cmd.Parameters.Add(new SQLiteParameter("@receive_qty2_x_mf", _item.receiveQty2xMf));
                cmd.Parameters.Add(new SQLiteParameter("@receive_qty_total", _item.receiveQtyTotal));
                cmd.Parameters.Add(new SQLiteParameter("@receive_density", _item.ReceiveDensity));
                cmd.Parameters.Add(new SQLiteParameter("@receive_temperature", _item.ReceiveTemperature));
                cmd.Parameters.Add(new SQLiteParameter("@receive_qty_additive", _item.ReceiveQtyAdditive));
                cmd.Parameters.Add(new SQLiteParameter("@receive_iscomplete", _item.ReceiveIscomplete));
                cmd.Parameters.Add(new SQLiteParameter("@receive_driver_name", _item.ReceiveDriverName));
                cmd.Parameters.Add(new SQLiteParameter("@receive_no_segel_1", _item.ReceiveNoSegel1));
                cmd.Parameters.Add(new SQLiteParameter("@receive_no_segel_2", _item.ReceiveNoSegel2));
                cmd.Parameters.Add(new SQLiteParameter("@receive_no_segel_3", _item.ReceiveNoSegel3));
                cmd.Parameters.Add(new SQLiteParameter("@receive_no_segel_4", _item.ReceiveNoSegel4));
                cmd.Parameters.Add(new SQLiteParameter("@receive_no_segel_5", _item.ReceiveNoSegel5));
                cmd.Parameters.Add(new SQLiteParameter("@receive_no_segel_6", _item.ReceiveNoSegel6));
                cmd.Parameters.Add(new SQLiteParameter("@doc_status", _item.DocStatus));
                cmd.Parameters.Add(new SQLiteParameter("@last_mod_by", _item.LastModBy));
                cmd.Parameters.Add(new SQLiteParameter("@foto", _item.Foto));
                cmd.Parameters.Add(new SQLiteParameter("@time_start", _item.TimeStart));
                cmd.Parameters.Add(new SQLiteParameter("@time_end", _item.TimeEnd));
                cmd.Parameters.Add(new SQLiteParameter("@syncs", 2));

                recs = cmd.ExecuteNonQuery();


                if (conn.State == ConnectionState.Open) conn.Close();
                return recs;
            }
            catch (Exception ex)
            {
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(ex.ToString(), "submitDataToLocal()");
                FileToExcel("receive_ft_port");
                MessageBox.Show("Data Tidak Tersimpan, Silahkan tulis di logsheet manual", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return -1;
            }


        }

        public void FileToExcel(string name)
        {
            StringBuilder sb = new StringBuilder();
            var filename = "log_transaksi_" + name + DateTime.Now.ToString("yyyyMMdd") + ".csv";
            //cek direktori jika tidak ada maka dibuat direktori baru
            if (!Directory.Exists(Path.Combine(Environment.CurrentDirectory, "log_transaksi_eror_simpan_db")))
            {
                Directory.CreateDirectory(Path.Combine(Environment.CurrentDirectory, "log_transaksi_eror_simpan_db"));
            }
            var pathExcel = Path.Combine(Environment.CurrentDirectory, "log_transaksi_eror_simpan_db", filename);
            //cek file jika tidak ada maka buat header baru
            if (!File.Exists(pathExcel))
            {
                sb.Append("pid_ritasi_maintank;ref_tbl; ref_id;district; send_type; send_po_no;send_do_no; send_qty; transportir_code; transportir_name; send_ft_no; send_driver_name; send_date; send_bbn;send_from; send_from_code; send_to;send_to_code; send_dipping_hole1_port; send_dipping_hole2_port; send_iscomplete; send_sn_flow_meter; send_meter_faktor; send_flow_meter_awal; send_flow_meter_akhir; send_qty_x_mf; send_no_segel_1; send_no_segel_2; send_no_segel_3; send_no_segel_4; send_no_segel_5; send_no_segel_6; doc_status; last_mod_by; foto; send_time_start; send_time_end; \r\n");
            }
            var item = pData;
            sb.Append(item.PidRitasiMaintank + ";");
            sb.Append(item.RefTbl + ";");
            sb.Append(item.RefId + ";");
            sb.Append(item.District + ";");
            sb.Append(item.SendType + ";");
            sb.Append(item.SendPoNo + ";");
            sb.Append(item.SendDoNo + ";");
            sb.Append(item.SendQty + ";");
            sb.Append(item.TransportirCode + ";");
            sb.Append(item.TransportirName + ";");
            sb.Append(item.SendFtNo + ";");
            sb.Append(item.SendDriverName + ";");
            sb.Append(item.SendDate + ";");
            sb.Append(item.SendBbn + ";");
            sb.Append(item.SendFrom + ";");
            sb.Append(item.SendFromCode + ";");
            sb.Append(item.SendTo + ";");
            sb.Append(item.SendToCode + ";");
            sb.Append(item.SendDippingHole1Port + ";");
            sb.Append(item.SendDippingHole2Port + ";");
            sb.Append(item.SendIscomplete + ";");
            sb.Append(item.SendsnFlowMeter + ";");
            sb.Append(item.SendMeterFaktor + ";");
            sb.Append(item.SendFlowMeterAwal + ";");
            sb.Append(item.SendFlowMeterAkhir + ";");
            sb.Append(item.SendNoSegel1 + ";");
            sb.Append(item.SendNoSegel2 + ";");
            sb.Append(item.SendNoSegel3 + ";");
            sb.Append(item.SendNoSegel4 + ";");
            sb.Append(item.SendNoSegel5 + ";");
            sb.Append(item.SendNoSegel6 + ";");
            sb.Append(item.DocStatus + ";");
            sb.Append(item.LastModBy + ";");
            sb.Append(item.Foto + ";");
            sb.Append(item.TimeStart + ";");
            sb.Append(item.TimeEnd + ";");
            sb.Append("\r\n");



            if (File.Exists(pathExcel))
            {
                File.AppendAllText(pathExcel, sb.ToString() + Environment.NewLine);
            }
            else
            {
                File.WriteAllText(pathExcel, sb.ToString());
            }

        }


        public async Task<string> checkConnectivity()
        {
            try
            {
                WebClient requestCek = new WebClient();
                var str = requestCek.DownloadString(pUrl_service);
                return str;
            }
            catch (WebException ex)
            {
                return "";
            }
        }
    }
}
