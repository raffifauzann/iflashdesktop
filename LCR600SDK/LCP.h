/*==========================================================================*/
/* Source File: LCP.h                                                       */
/* Program:     Generic                                                     */
/* Functions:   N/A                                                         */
/*                                                                          */
/* Authors          Dates     Revision  Change Log                          */
/*  Bryan Haynes    02/02/01    1.00    Original                            */
/*  Bryan Haynes    02/19/01    1.00    Transmit enable support added.      */
/*  Scott Wang      02/23/01    1.00    Converted to Windows NT.            */
/*  Bryan Haynes    12/19/01    1.03    LCPGetRequest prototype changed.    */
/*  Bryan Haynes    12/10/02    1.04    Slave functions implemented.        */
/*  Bryan Haynes    06/25/03    1.04    Fixed external DLL import variable  */
/*                                      declarations for non-C++            */
/*                                      applications.                       */
/*  Bryan Haynes    07/07/03    1.04    Added the version number string.    */
/*  Bryan Haynes    07/18/03    1.05    LCPR_MissingMHz added.              */
/*  Bryan Haynes    11/04/05    1.10    LCPR_MessageSize added.             */
/*  Bryan Haynes    12/21/06    1.00    Ported to Visual Studio 2005.       */
/*  Bryan Haynes    02/28/08    1.08    AT command support added.           */
/*  Bryan Haynes    11/02/09    1.09    LIBSPEC changed to LCP_LIBSPEC.     */
/*  Shakila Xavier  02/12/10    1.13    Adding new APIs for IP and version. */
/*  Shakila Xavier  06/02/11    1.14    Added support for greater than 255  */
/*                                      nodes.                              */
/*  Shakila Xavier  03/21/12    1.15    Ported to 32-bit Windows.           */
/*  Mark Shtulberg  05/15/12    1.20    Added Set and Get Status APIs for   */
/*                                      multithread synchronization.        */
/*  Bryan Haynes    10/14/14    1.21    Ported to 64-bit Windows.           */
/*  Mark Shtulberg  02/02/15    1.26    Added BadMessageID error code.      */
/*  Bryan Haynes    05/16/17    1.27    Finally got around to adding the    */
/*                                      message ID for retrieving the       */
/*                                      product ID from an LCP device.      */
/*  Bryan Haynes    07/03/17    1.27    Ported to eCos.                     */
/*                                                                          */
/* Function:                                                                */
/*      This file contains the defines, global variables, structures, and   */
/*      function prototypes for the Liquid Controls Protocol library.       */
/*                                                                          */
/* Inputs:                                                                  */
/*      None                                                                */
/*                                                                          */
/* Outputs:                                                                 */
/*      None                                                                */
/*                                                                          */
/*--------------------------------------------------------------------------*/
/* Copyright (c) Liquid Controls, LLC 2001-2017                             */
/*               105 Albrecht Drive                                         */
/*               Lake Bluff, IL  60044-2242  USA                            */
/*==========================================================================*/

//=======================================
// Ensure the file is only included once.
//=======================================

#ifndef LCP_H                                                           // check for previous inclusion
#define LCP_H                                                           // ensure only one inclusion

//==========
// Includes.
//==========

#if defined(_MSC_VER) && (_MSC_VER > 800)                               // check for DOS
#include <tchar.h>                                                      // generic character mapping functions
#include <winsock2.h>                                                   // WinSock definitions
#include <ws2tcpip.h>                                                   // IPV6 address
#endif

//=====================
// Boolean definitions.
//=====================

#ifndef FALSE
#define FALSE   (0)                                                     // boolean FALSE
#endif

#ifndef TRUE
#define TRUE    (!FALSE)                                                // boolean TRUE
#endif

//=====================
// General definitions.
//=====================

#define LCP_MID_GetPID          (0x00)                                  // LCP message ID to get the product ID

#define LCP_PID_Host            (0U)                                    // LCP product ID for any host application

#define LCP_Baud115200          (0U)                                    // 115.2K baud
#define LCP_Baud57600           (1U)                                    //  57.6K baud
#define LCP_Baud19200           (2U)                                    //  19.2K baud
#define LCP_Baud9600            (3U)                                    //   9.6K baud
#define LCP_Baud4800            (4U)                                    //   4.8K baud
#define LCP_Baud2400            (5U)                                    //   2.4K baud
#define LCP_NumBauds            (6U)                                    // number of supported LCP bauds

#define LCP_TXEnableNone        (0U)                                    // transmit enable signal is not used
#define LCP_TXEnable_RTS        (1U)                                    // transmit enabled via the !RTS line
#define LCP_TXEnableRTS         (2U)                                    // transmit enabled via the RTS line
#define LCP_TXEnable_DTR        (3U)                                    // transmit enabled via the !DTR line
#define LCP_TXEnableDTR         (4U)                                    // transmit enabled via the DTR line
#define LCP_TXEnable_RTS_DTR    (5U)                                    // transmit enabled via the !RTS and !DTR line
#define LCP_TXEnableRTSDTR      (6U)                                    // transmit enabled via the RTS and DTR line
#define LCP_NumTXEnables        (7U)                                    // number of transmit enable signals supported

#define LCP_MsgOverhead         (8U)                                    // overhead of a LCP message
#define LCP_MsgOverheadLimit    (14U)                                   // maximum overhead of a LCP message
#define LCP_MsgSize(dataLen)    (LCP_MsgOverheadLimit + 2U*dataLen)
#define LCP_MsgSizeLimit        (523U)                                  // maximum size of a LCP message
#define LCP_DataSizeLimit       (255U)                                  // maximum size of the data portion of a LCP message

#define LCP_BroadcastNode       (0U)                                    // broadcast node address
#define LCP_NodeAddressLimit    (255U)                                  // maximum allowed LCP node address

#define LCPZ_NameAndRevision    (15U)                                   // maximum length of name and revision string

#if defined(_MSC_VER) && (_MSC_VER <= 800)                              // check for DOS
    #define LCP_PortCOM1        (0x03F8)                                // default base address of COM1
    #define LCP_PortCOM2        (0x02F8)                                // default base address of COM2
    #define LCP_PortCOM3        (0x03E8)                                // default base address of COM3
    #define LCP_PortCOM4        (0x02E8)                                // default base address of COM4

    #define LCP_MSToTimerTicks(ms)  ((unsigned long)(0.5F+(((double)ms)*1193.182F)))
    #define LCP_USToTimerTicks(us)  ((unsigned long)(0.5F+(((double)us)*1.193182F)))
#endif

#if defined(eCos)                                                       // check for eCos
    #define LCP_IndefiniteWait  (USHRT_MAX)                             // indefinite timeout value

    typedef void LCPReqThread(unsigned char rxFrom, unsigned char rxTo, unsigned char rxStatus, unsigned char rxMsgSize, unsigned char *rxMsg, unsigned char *txStatus, unsigned char *txMsgSize, unsigned char *txMsg);
#endif

//====================================
// LCP message status bit definitions.
//====================================

#define LCP_StatusMsgID         (0x01)                                  // mask of message ID in the LCP status byte
#define LCP_StatusSynchronize   (0x02)                                  // flag indicating message ID synchronization is needed
#define LCP_StatusCheckRequest  (0x04)                                  // check the active request
#define LCP_StatusBusy          (0x04)                                  // slave is busy but will respond as soon as possible
#define LCP_StatusAbortRequest  (0x08)                                  // abort the active request
#define LCP_StatusAborted       (0x08)                                  // the active request was aborted
#define LCP_StatusNoRequest     (0x10)                                  // no request is active
#define LCP_StatusOverrun       (0x20)                                  // message overrun bit in the LCP status byte
#define LCP_StatusLCPpp         (0x40)                                  // LCP++ in use bit
#define LCP_StatusUnsupported   (0x40)                                  // message ID unsupported bit in the LCP status byte
#define LCP_StatusReply         (0x80)                                  // reply bit in the LCP status byte

//==============
// Return codes.
//==============

#define LCPR_OK                 (  0U)                                  // operation was carried out successfully

#define LCPR_AlreadyInstalled   (200U)                                  // the LCP device drivers are already installed
#define LCPR_BadNameAndRevision (201U)                                  // invalid name and revision string
#define LCPR_BadNodeAddress     (202U)                                  // invalid node address of host application
#define LCPR_BadBaseAddress     (203U)                                  // invalid base communications address
#define LCPR_BadIRQ             (204U)                                  // invalid IRQ
#define LCPR_BadBaudRate        (205U)                                  // invalid baud rate
#define LCPR_BadTimeout         (206U)                                  // invalid timeout
#define LCPR_BadTXEnable        (207U)                                  // invalid transmit enable
#define LCPR_NULLPointer        (208U)                                  // a NULL pointer was passed as an address
#define LCPR_NoRequestActive    (209U)                                  // the slave device reported that no request was active
#define LCPR_NoResponse         (210U)                                  // the slave device did not respond to the request
#define LCPR_RequestAborted     (211U)                                  // the slave device aborted the last request
#define LCPR_UnsupportedCommand (212U)                                  // the slave device reports an unsupported command
#define LCPR_BufferOverflow     (213U)                                  // the slave device reports its receive buffer overflowed
#define LCPR_Busy               (214U)                                  // the slave device reports that it is busy but is working on the request
#define LCPR_NotInstalled       (215U)                                  // the LCP device drivers are not installed
#define LCPR_MemoryAllocation   (216U)                                  // there is not enough dynamic memory available for the request
#define LCPR_MissingMHz         (217U)                                  // the MHz environment variable is missing
#define LCPR_MessageSize        (218U)                                  // the response message contains the incorrect number of bytes
#define LCPR_WinSockErr         (219U)                                  // there was a problem initializing winsock
#define LCPR_SocketErr          (220U)                                  // there was a problem initializing socket
#define LCPR_BadIPAddress       (221U)                                  // invalid IP Addr 
#define LCPR_BadIPPort          (222U)                                  // invalid IP Port
#define LCPR_BadMessageID       (223U)                                  // invalid Message ID

//===============================
// Connection status definitions.
//===============================

#define LCP_ConnectionNew       (0x01)                                  // message just received by socket
#define LCP_ConnectionActive    (0x02)                                  // connection to the particular node is established
#define LCP_ConnectionPending   (0x04)                                  // this and only this socket is in master mode or own by other thread

//==================
// Enum definitions.
//==================

typedef enum {
    LCP_Default = 0,                                                    // no IP is used
    LCP_IPv4    = 4,                                                    // LCP IP address is IPv4
    LCP_IPv6    = 6                                                     // LCP IP address is IPv6
} LCP_IPVERSION;

//=======================
// Structure definitions.
//=======================

#if defined(_MSC_VER) && (_MSC_VER > 800)                               // check for DOS

#pragma pack(1)                                                         // pack structures on byte boundaries

typedef struct LCPip_addr {
   union {
      IN6_ADDR IP6Addr;
      IN_ADDR  IPAddr;
   };
   LCP_IPVERSION IPversion;                                             // IP version being used
} LCPIP_ADDR, *PLCPIP_ADDR, FAR *LPLCPIP_ADDR;

#pragma pack()                                                          // revert to default or /Zp packing

#else

#pragma pack(1)                                                         // pack structures on byte boundaries

//
// IPv4 Internet address
// This is an 'on-wire' format structure.
//
typedef struct in_addr {
    union {
        struct { unsigned char s_b1,s_b2,s_b3,s_b4; } S_un_b;
        struct { unsigned short s_w1,s_w2; } S_un_w;
        unsigned long S_addr;
    } S_un;
} IN_ADDR;

//
// IPv6 Internet address (RFC 2553)
// This is an 'on-wire' format structure.
//
typedef struct in6_addr {
    union {
        unsigned char  Byte[16];
        unsigned short Word[8];
    } u;
} IN6_ADDR;

typedef struct LCPip_addr {
   union {
      IN6_ADDR IP6Addr;
      IN_ADDR  IPAddr;
   };
   LCP_IPVERSION IPversion;                                             // IP version being used
} LCPIP_ADDR;

typedef LCPIP_ADDR *PLCPIP_ADDR;

#pragma pack()                                                          // revert to default or /Zp packing

#endif

//===============================
// External reference definition.
//===============================

#undef LCP_LIBSPEC
#undef SCOPE

#if defined(eCos)                                                       // check for eCos
    #define WINAPI
#elif defined(_MSC_VER) && (_MSC_VER <= 800)                            // check for DOS
    #define WINAPI
#endif

#if defined(eCos)                                                       // check for eCos
    #define LCP_LIBSPEC
//#elif (defined(_MSC_VER) && (_MSC_VER <= 800)) || defined(_COMPILING_LCLCP)
#elif defined(_MSC_VER) && (_MSC_VER <= 800)                            // check for DOS
    #define LCP_LIBSPEC
#elif defined(_COMPILING_LCLCP)
    #define LCP_LIBSPEC __declspec(dllexport)
#else
    #define LCP_LIBSPEC __declspec(dllimport)
#endif

#if defined(eCos)                                                       // check for eCos
    #define SCOPE extern
#elif defined(_MSC_VER) && (_MSC_VER <= 800)                            // check for DOS
    #define SCOPE extern
#elif defined(__cplusplus)
    #define SCOPE extern "C"
#else
    #define SCOPE extern
#endif

//====================
// External variables.
//====================

#ifndef LCLCP_EXTERNALS                                                 // check for previous declaration
    #define LCLCP_EXTERNALS                                             // ensure only one declaration

    #if defined(_MSC_VER) && (_MSC_VER <= 800)                          // check for DOS
        SCOPE LCP_LIBSPEC char LCPATResponse[128];
        SCOPE LCP_LIBSPEC char *verLCxCLCP;
        SCOPE LCP_LIBSPEC unsigned short LCPATTimer;
        SCOPE LCP_LIBSPEC char LCPATCommands;
    #else
        SCOPE LCP_LIBSPEC char *verLCLCP;
    #endif

    SCOPE LCP_LIBSPEC int LCPInstalled;
#endif

//==============================
// External function prototypes.
//==============================

#if defined(eCos)                                                       // check for eCos

    SCOPE LCP_LIBSPEC unsigned char WINAPI LCPAbortRequest(unsigned short hLCPix, unsigned char txTo);
    SCOPE LCP_LIBSPEC unsigned char WINAPI LCPCheckRequest(unsigned short hLCPix, unsigned char txTo, unsigned char *rxData, unsigned char *rxDataLen);
    SCOPE LCP_LIBSPEC unsigned char WINAPI LCPGetProductInformation(unsigned short hLCPix, unsigned char txTo, unsigned char *PID, char *name);
    SCOPE LCP_LIBSPEC unsigned char WINAPI LCPGetRequest(unsigned short hLCPix, unsigned short timeout, unsigned char *rxTo, unsigned char *rxFrom, unsigned char *rxDataLen, unsigned char *rxData, unsigned char *rxStatus);
    SCOPE LCP_LIBSPEC char * WINAPI LCPGetVersion(char *str, size_t strSize);
    SCOPE LCP_LIBSPEC unsigned char WINAPI LCPInstall(unsigned char PID, char *name, unsigned char nodeAddr, unsigned int portNumber, unsigned char baudRateIX, unsigned char txEnable, unsigned short timeout, unsigned char retries, LCPReqThread *ReqThread, unsigned short *hLCPix);
    SCOPE LCP_LIBSPEC void WINAPI LCPReverseBytes(void *fieldData, size_t fieldSize);
    SCOPE LCP_LIBSPEC unsigned char WINAPI LCPSendRequest(unsigned short hLCPix, unsigned char txTo, unsigned char *txData, unsigned char txDataLen, unsigned char *rxData, unsigned char *rxDataLen);
    SCOPE LCP_LIBSPEC unsigned char WINAPI LCPSendResponse(unsigned short hLCPix, unsigned char txTo,unsigned char *txData,unsigned char txDataLen,unsigned char txStatus);
    SCOPE LCP_LIBSPEC unsigned char WINAPI LCPUninstall(unsigned short hLCPix);
 
#else

    SCOPE LCP_LIBSPEC unsigned char WINAPI LCPAbortRequest(unsigned char);
    SCOPE LCP_LIBSPEC unsigned char WINAPI LCPCheckRequest(unsigned char,unsigned char *,unsigned char *);
    SCOPE LCP_LIBSPEC unsigned char WINAPI LCPGetProductInformation(unsigned char,unsigned char *,char *);
    SCOPE LCP_LIBSPEC unsigned char WINAPI LCPGetRequest(unsigned char *,unsigned char *,unsigned char *,unsigned char *);
    SCOPE LCP_LIBSPEC void WINAPI LCPGetVersion(char *);
    SCOPE LCP_LIBSPEC void WINAPI LCPReverseBytes(void *,size_t);
    SCOPE LCP_LIBSPEC unsigned char WINAPI LCPSendRequest(unsigned char,unsigned char *,unsigned char,unsigned char *,unsigned char *);
    SCOPE LCP_LIBSPEC unsigned char WINAPI LCPSendResponse(unsigned char,unsigned char *,unsigned char,unsigned char);
    SCOPE LCP_LIBSPEC unsigned char WINAPI LCPUninstall(void);
    SCOPE LCP_LIBSPEC void WINAPI LCPGetInstallStatus(int *);

#if defined(_MSC_VER) && (_MSC_VER <= 800)                              // check for DOS

    SCOPE LCP_LIBSPEC unsigned char WINAPI LCPGetSetupParameters(unsigned char *,char *,unsigned char *,unsigned short *,unsigned short *,unsigned char *,unsigned char *,unsigned short *,unsigned char *);
    SCOPE LCP_LIBSPEC unsigned char WINAPI LCPInstall(unsigned char,char *,unsigned char,unsigned short,unsigned short,unsigned char,unsigned char,unsigned short,unsigned char);
    SCOPE LCP_LIBSPEC int LCPSendATCommand(char);
    SCOPE LCP_LIBSPEC void LCPSetTimer(unsigned long);
    SCOPE LCP_LIBSPEC int LCPTimerHasExpired(void);
    SCOPE LCP_LIBSPEC void LCPWaitForTransmission(void);

#else

    SCOPE LCP_LIBSPEC unsigned char WINAPI LCPInstall(unsigned char,char *,unsigned char,WCHAR *,unsigned char,unsigned char,unsigned short,unsigned char);
    SCOPE LCP_LIBSPEC unsigned char WINAPI LCPIPInstall(unsigned char,char *,unsigned char,unsigned short,unsigned char,int);
    SCOPE LCP_LIBSPEC unsigned char WINAPI LCPIPUninstall(void);
    SCOPE LCP_LIBSPEC unsigned char WINAPI LCPMakeIPNodeKnown(unsigned char,PLCPIP_ADDR,int);
	
// APIs to support greater than 255 nodes.

	SCOPE LCP_LIBSPEC unsigned char WINAPI LCPIPAbortRequest(unsigned char,PLCPIP_ADDR);
	SCOPE LCP_LIBSPEC unsigned char WINAPI LCPIPCheckRequest(unsigned char,PLCPIP_ADDR,unsigned char *,unsigned char *);
	SCOPE LCP_LIBSPEC unsigned char WINAPI LCPIPGetProductInformation(unsigned char,PLCPIP_ADDR,unsigned char *,char *);
	SCOPE LCP_LIBSPEC unsigned char WINAPI LCPIPGetRequest(unsigned char *,PLCPIP_ADDR,unsigned char *,unsigned char *,unsigned char *);
	SCOPE LCP_LIBSPEC unsigned char WINAPI LCPIPSendRequest(unsigned char,PLCPIP_ADDR,unsigned char *,unsigned char,unsigned char *,unsigned char *);
	SCOPE LCP_LIBSPEC unsigned char WINAPI LCPIPSendResponse(unsigned char,PLCPIP_ADDR,unsigned char *,unsigned char,unsigned char);
	SCOPE LCP_LIBSPEC unsigned char WINAPI LCPIPUninstall(void);
	SCOPE LCP_LIBSPEC unsigned char WINAPI LCPIPGetConnectionStatus(unsigned char,PLCPIP_ADDR,unsigned char *);
	SCOPE LCP_LIBSPEC unsigned char WINAPI LCPIPSetConnectionStatus(unsigned char ,PLCPIP_ADDR,unsigned char);

#endif

#endif

#endif
