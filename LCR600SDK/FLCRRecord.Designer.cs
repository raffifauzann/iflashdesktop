﻿namespace LCR600SDK
{
    partial class FLCRRecord
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FLCRRecord));
            this.lblAlertClosed = new System.Windows.Forms.Label();
            this.txtTotalizerStart = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtTotalizerGross = new System.Windows.Forms.TextBox();
            this.txtRefStart = new System.Windows.Forms.TextBox();
            this.txtRefStop = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.timerOpen = new System.Windows.Forms.Timer(this.components);
            this.timerQty = new System.Windows.Forms.Timer(this.components);
            this.timerClose = new System.Windows.Forms.Timer(this.components);
            this.txtGrossQty = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lblAlertClosed
            // 
            this.lblAlertClosed.AutoSize = true;
            this.lblAlertClosed.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAlertClosed.ForeColor = System.Drawing.Color.Red;
            this.lblAlertClosed.Location = new System.Drawing.Point(152, 226);
            this.lblAlertClosed.Name = "lblAlertClosed";
            this.lblAlertClosed.Size = new System.Drawing.Size(781, 42);
            this.lblAlertClosed.TabIndex = 15;
            this.lblAlertClosed.Text = "TRANSACTION CLOSED.. PLEASE WAIT..";
            this.lblAlertClosed.Visible = false;
            // 
            // txtTotalizerStart
            // 
            this.txtTotalizerStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalizerStart.Location = new System.Drawing.Point(58, 165);
            this.txtTotalizerStart.Name = "txtTotalizerStart";
            this.txtTotalizerStart.Size = new System.Drawing.Size(187, 29);
            this.txtTotalizerStart.TabIndex = 24;
            this.txtTotalizerStart.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(53, 137);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 25);
            this.label4.TabIndex = 23;
            this.label4.Text = "label4";
            this.label4.Visible = false;
            // 
            // txtTotalizerGross
            // 
            this.txtTotalizerGross.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTotalizerGross.Enabled = false;
            this.txtTotalizerGross.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalizerGross.Location = new System.Drawing.Point(763, 130);
            this.txtTotalizerGross.Name = "txtTotalizerGross";
            this.txtTotalizerGross.Size = new System.Drawing.Size(187, 29);
            this.txtTotalizerGross.TabIndex = 22;
            // 
            // txtRefStart
            // 
            this.txtRefStart.Enabled = false;
            this.txtRefStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRefStart.Location = new System.Drawing.Point(58, 102);
            this.txtRefStart.Name = "txtRefStart";
            this.txtRefStart.Size = new System.Drawing.Size(187, 29);
            this.txtRefStart.TabIndex = 21;
            // 
            // txtRefStop
            // 
            this.txtRefStop.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtRefStop.Enabled = false;
            this.txtRefStop.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRefStop.Location = new System.Drawing.Point(763, 67);
            this.txtRefStop.Name = "txtRefStop";
            this.txtRefStop.Size = new System.Drawing.Size(187, 29);
            this.txtRefStop.TabIndex = 20;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(792, 102);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(158, 25);
            this.label3.TabIndex = 19;
            this.label3.Text = "Totalizer Gross";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(855, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 25);
            this.label2.TabIndex = 18;
            this.label2.Text = "Ref Stop";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(53, 74);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 25);
            this.label1.TabIndex = 17;
            this.label1.Text = "Ref Start";
            // 
            // timerOpen
            // 
            this.timerOpen.Interval = 1000;
            this.timerOpen.Tick += new System.EventHandler(this.timerOpen_Tick);
            // 
            // timerQty
            // 
            this.timerQty.Interval = 120000;
            this.timerQty.Tick += new System.EventHandler(this.timerQty_Tick);
            // 
            // timerClose
            // 
            this.timerClose.Interval = 5000;
            this.timerClose.Tick += new System.EventHandler(this.timerClose_Tick);
            // 
            // txtGrossQty
            // 
            this.txtGrossQty.Enabled = false;
            this.txtGrossQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 140.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGrossQty.Location = new System.Drawing.Point(319, 4);
            this.txtGrossQty.Name = "txtGrossQty";
            this.txtGrossQty.Size = new System.Drawing.Size(417, 219);
            this.txtGrossQty.TabIndex = 25;
            this.txtGrossQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // FLCRRecord
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1020, 450);
            this.Controls.Add(this.txtGrossQty);
            this.Controls.Add(this.txtTotalizerStart);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtTotalizerGross);
            this.Controls.Add(this.txtRefStart);
            this.Controls.Add(this.txtRefStop);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblAlertClosed);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FLCRRecord";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Fuel Record";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FLCRRecord_FormClosed);
            this.Load += new System.EventHandler(this.FLCRRecord_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblAlertClosed;
        private System.Windows.Forms.TextBox txtTotalizerStart;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtTotalizerGross;
        private System.Windows.Forms.TextBox txtRefStart;
        private System.Windows.Forms.TextBox txtRefStop;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Timer timerOpen;
        private System.Windows.Forms.Timer timerQty;
        private System.Windows.Forms.Timer timerClose;
        private System.Windows.Forms.TextBox txtGrossQty;
    }
}