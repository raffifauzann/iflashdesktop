﻿namespace LCR600SDK
{
    partial class FWhouseTransfer7Inch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FWhouseTransfer7Inch));
            this.btnNewTrans = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.listFuelman = new System.Windows.Forms.ListBox();
            this.listWHPenerima = new System.Windows.Forms.ListBox();
            this.txtBxWHPenerima = new System.Windows.Forms.TextBox();
            this.lblWHpenerima = new System.Windows.Forms.Label();
            this.txtQtyOrder = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbWhPenerima = new System.Windows.Forms.ComboBox();
            this.cmbFuelman = new System.Windows.Forms.ComboBox();
            this.lblQty = new System.Windows.Forms.Label();
            this.txtFuelQty = new System.Windows.Forms.TextBox();
            this.lblLiters = new System.Windows.Forms.Label();
            this.txtBxFuelman = new System.Windows.Forms.TextBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btn0 = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.btn9 = new System.Windows.Forms.Button();
            this.btn8 = new System.Windows.Forms.Button();
            this.btn7 = new System.Windows.Forms.Button();
            this.btn6 = new System.Windows.Forms.Button();
            this.btn5 = new System.Windows.Forms.Button();
            this.btn4 = new System.Windows.Forms.Button();
            this.btn3 = new System.Windows.Forms.Button();
            this.btn2 = new System.Windows.Forms.Button();
            this.btn1 = new System.Windows.Forms.Button();
            this.panel2.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnNewTrans
            // 
            this.btnNewTrans.BackColor = System.Drawing.Color.Blue;
            this.btnNewTrans.FlatAppearance.BorderSize = 0;
            this.btnNewTrans.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNewTrans.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNewTrans.ForeColor = System.Drawing.Color.White;
            this.btnNewTrans.Location = new System.Drawing.Point(582, 354);
            this.btnNewTrans.Name = "btnNewTrans";
            this.btnNewTrans.Size = new System.Drawing.Size(160, 90);
            this.btnNewTrans.TabIndex = 0;
            this.btnNewTrans.Text = "NEW";
            this.btnNewTrans.UseVisualStyleBackColor = false;
            this.btnNewTrans.Visible = false;
            this.btnNewTrans.Click += new System.EventHandler(this.btnNewTrans_Click);
            // 
            // btnStart
            // 
            this.btnStart.BackColor = System.Drawing.Color.Green;
            this.btnStart.FlatAppearance.BorderSize = 0;
            this.btnStart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStart.ForeColor = System.Drawing.Color.White;
            this.btnStart.Location = new System.Drawing.Point(581, 354);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(160, 90);
            this.btnStart.TabIndex = 0;
            this.btnStart.Text = "START";
            this.btnStart.UseVisualStyleBackColor = false;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.button1);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.panel5);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(784, 71);
            this.panel2.TabIndex = 3;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.BackColor = System.Drawing.Color.Red;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(663, 7);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(109, 44);
            this.button1.TabIndex = 81;
            this.button1.Text = "Close";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic) 
                | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Blue;
            this.label3.Location = new System.Drawing.Point(168, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(275, 39);
            this.label3.TabIndex = 1;
            this.label3.Text = "TRANSACTION";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.pictureBox1);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(99, 71);
            this.panel5.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(99, 71);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 40;
            this.pictureBox1.TabStop = false;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.listFuelman);
            this.panel3.Controls.Add(this.listWHPenerima);
            this.panel3.Controls.Add(this.txtBxWHPenerima);
            this.panel3.Controls.Add(this.lblWHpenerima);
            this.panel3.Controls.Add(this.txtQtyOrder);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.cmbWhPenerima);
            this.panel3.Controls.Add(this.cmbFuelman);
            this.panel3.Controls.Add(this.lblQty);
            this.panel3.Controls.Add(this.txtFuelQty);
            this.panel3.Controls.Add(this.lblLiters);
            this.panel3.Controls.Add(this.txtBxFuelman);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(0, 71);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(244, 490);
            this.panel3.TabIndex = 4;
            // 
            // listFuelman
            // 
            this.listFuelman.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.listFuelman.FormattingEnabled = true;
            this.listFuelman.ItemHeight = 29;
            this.listFuelman.Location = new System.Drawing.Point(7, 149);
            this.listFuelman.Name = "listFuelman";
            this.listFuelman.Size = new System.Drawing.Size(235, 33);
            this.listFuelman.TabIndex = 84;
            this.listFuelman.Visible = false;
            this.listFuelman.Click += new System.EventHandler(this.listFuelman_Click);
            // 
            // listWHPenerima
            // 
            this.listWHPenerima.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.listWHPenerima.FormattingEnabled = true;
            this.listWHPenerima.ItemHeight = 29;
            this.listWHPenerima.Location = new System.Drawing.Point(6, 67);
            this.listWHPenerima.Name = "listWHPenerima";
            this.listWHPenerima.Size = new System.Drawing.Size(235, 33);
            this.listWHPenerima.TabIndex = 82;
            this.listWHPenerima.Visible = false;
            this.listWHPenerima.Click += new System.EventHandler(this.listWHPenerima_Click);
            // 
            // txtBxWHPenerima
            // 
            this.txtBxWHPenerima.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBxWHPenerima.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.txtBxWHPenerima.Location = new System.Drawing.Point(6, 32);
            this.txtBxWHPenerima.Name = "txtBxWHPenerima";
            this.txtBxWHPenerima.Size = new System.Drawing.Size(235, 35);
            this.txtBxWHPenerima.TabIndex = 83;
            this.txtBxWHPenerima.Click += new System.EventHandler(this.txtBxWHPenerima_Click);
            this.txtBxWHPenerima.TextChanged += new System.EventHandler(this.txtBxWHPenerima_TextChanged);
            // 
            // lblWHpenerima
            // 
            this.lblWHpenerima.AutoSize = true;
            this.lblWHpenerima.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWHpenerima.Location = new System.Drawing.Point(49, 363);
            this.lblWHpenerima.Name = "lblWHpenerima";
            this.lblWHpenerima.Size = new System.Drawing.Size(0, 25);
            this.lblWHpenerima.TabIndex = 81;
            this.lblWHpenerima.Visible = false;
            // 
            // txtQtyOrder
            // 
            this.txtQtyOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.txtQtyOrder.Location = new System.Drawing.Point(6, 204);
            this.txtQtyOrder.Name = "txtQtyOrder";
            this.txtQtyOrder.Size = new System.Drawing.Size(235, 35);
            this.txtQtyOrder.TabIndex = 80;
            this.txtQtyOrder.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.label4.Location = new System.Drawing.Point(5, 171);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(238, 29);
            this.label4.TabIndex = 7;
            this.label4.Text = "Quantity Order (Liter)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(32, 86);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(192, 25);
            this.label2.TabIndex = 1;
            this.label2.Text = "Fuelman Penerima";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(39, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(144, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "WH Penerima";
            // 
            // cmbWhPenerima
            // 
            this.cmbWhPenerima.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbWhPenerima.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbWhPenerima.Cursor = System.Windows.Forms.Cursors.Default;
            this.cmbWhPenerima.DropDownHeight = 300;
            this.cmbWhPenerima.DropDownWidth = 300;
            this.cmbWhPenerima.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbWhPenerima.FormattingEnabled = true;
            this.cmbWhPenerima.IntegralHeight = false;
            this.cmbWhPenerima.Location = new System.Drawing.Point(6, 408);
            this.cmbWhPenerima.Name = "cmbWhPenerima";
            this.cmbWhPenerima.Size = new System.Drawing.Size(235, 33);
            this.cmbWhPenerima.Sorted = true;
            this.cmbWhPenerima.TabIndex = 0;
            this.cmbWhPenerima.Visible = false;
            // 
            // cmbFuelman
            // 
            this.cmbFuelman.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbFuelman.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbFuelman.DropDownHeight = 300;
            this.cmbFuelman.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbFuelman.FormattingEnabled = true;
            this.cmbFuelman.IntegralHeight = false;
            this.cmbFuelman.Location = new System.Drawing.Point(6, 369);
            this.cmbFuelman.Name = "cmbFuelman";
            this.cmbFuelman.Size = new System.Drawing.Size(235, 33);
            this.cmbFuelman.TabIndex = 1;
            this.cmbFuelman.Visible = false;
            this.cmbFuelman.Click += new System.EventHandler(this.cmbFuelman_Click);
            // 
            // lblQty
            // 
            this.lblQty.AutoSize = true;
            this.lblQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQty.Location = new System.Drawing.Point(15, 267);
            this.lblQty.Name = "lblQty";
            this.lblQty.Size = new System.Drawing.Size(115, 25);
            this.lblQty.TabIndex = 3;
            this.lblQty.Text = "Fuel Qty :";
            this.lblQty.Visible = false;
            // 
            // txtFuelQty
            // 
            this.txtFuelQty.Enabled = false;
            this.txtFuelQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFuelQty.Location = new System.Drawing.Point(6, 295);
            this.txtFuelQty.Name = "txtFuelQty";
            this.txtFuelQty.Size = new System.Drawing.Size(232, 49);
            this.txtFuelQty.TabIndex = 2;
            this.txtFuelQty.Visible = false;
            // 
            // lblLiters
            // 
            this.lblLiters.AutoSize = true;
            this.lblLiters.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLiters.Location = new System.Drawing.Point(136, 267);
            this.lblLiters.Name = "lblLiters";
            this.lblLiters.Size = new System.Drawing.Size(91, 25);
            this.lblLiters.TabIndex = 4;
            this.lblLiters.Text = "LITERS";
            this.lblLiters.Visible = false;
            // 
            // txtBxFuelman
            // 
            this.txtBxFuelman.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.txtBxFuelman.Location = new System.Drawing.Point(7, 114);
            this.txtBxFuelman.Name = "txtBxFuelman";
            this.txtBxFuelman.Size = new System.Drawing.Size(235, 35);
            this.txtBxFuelman.TabIndex = 85;
            this.txtBxFuelman.Click += new System.EventHandler(this.txtBxFuelman_Click);
            this.txtBxFuelman.TextChanged += new System.EventHandler(this.txtBxFuelman_TextChanged);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btnStart);
            this.panel4.Controls.Add(this.btnNewTrans);
            this.panel4.Controls.Add(this.btn0);
            this.panel4.Controls.Add(this.btnClear);
            this.panel4.Controls.Add(this.btn9);
            this.panel4.Controls.Add(this.btn8);
            this.panel4.Controls.Add(this.btn7);
            this.panel4.Controls.Add(this.btn6);
            this.panel4.Controls.Add(this.btn5);
            this.panel4.Controls.Add(this.btn4);
            this.panel4.Controls.Add(this.btn3);
            this.panel4.Controls.Add(this.btn2);
            this.panel4.Controls.Add(this.btn1);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(784, 561);
            this.panel4.TabIndex = 5;
            // 
            // btn0
            // 
            this.btn0.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn0.Location = new System.Drawing.Point(416, 355);
            this.btn0.Name = "btn0";
            this.btn0.Size = new System.Drawing.Size(160, 90);
            this.btn0.TabIndex = 79;
            this.btn0.Text = "0";
            this.btn0.UseVisualStyleBackColor = true;
            this.btn0.Click += new System.EventHandler(this.btn0_Click);
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.Color.Red;
            this.btnClear.FlatAppearance.BorderSize = 0;
            this.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.ForeColor = System.Drawing.Color.White;
            this.btnClear.Location = new System.Drawing.Point(248, 354);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(160, 90);
            this.btnClear.TabIndex = 78;
            this.btnClear.Text = "CLEAR";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btn9
            // 
            this.btn9.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn9.Location = new System.Drawing.Point(582, 261);
            this.btn9.Name = "btn9";
            this.btn9.Size = new System.Drawing.Size(160, 90);
            this.btn9.TabIndex = 77;
            this.btn9.Text = "9";
            this.btn9.UseVisualStyleBackColor = true;
            this.btn9.Click += new System.EventHandler(this.btn9_Click);
            // 
            // btn8
            // 
            this.btn8.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn8.Location = new System.Drawing.Point(416, 261);
            this.btn8.Name = "btn8";
            this.btn8.Size = new System.Drawing.Size(160, 90);
            this.btn8.TabIndex = 76;
            this.btn8.Text = "8";
            this.btn8.UseVisualStyleBackColor = true;
            this.btn8.Click += new System.EventHandler(this.btn8_Click);
            // 
            // btn7
            // 
            this.btn7.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn7.Location = new System.Drawing.Point(249, 261);
            this.btn7.Name = "btn7";
            this.btn7.Size = new System.Drawing.Size(160, 90);
            this.btn7.TabIndex = 75;
            this.btn7.Text = "7";
            this.btn7.UseVisualStyleBackColor = true;
            this.btn7.Click += new System.EventHandler(this.btn7_Click);
            // 
            // btn6
            // 
            this.btn6.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn6.Location = new System.Drawing.Point(582, 169);
            this.btn6.Name = "btn6";
            this.btn6.Size = new System.Drawing.Size(160, 90);
            this.btn6.TabIndex = 74;
            this.btn6.Text = "6";
            this.btn6.UseVisualStyleBackColor = true;
            this.btn6.Click += new System.EventHandler(this.btn6_Click);
            // 
            // btn5
            // 
            this.btn5.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn5.Location = new System.Drawing.Point(416, 168);
            this.btn5.Name = "btn5";
            this.btn5.Size = new System.Drawing.Size(160, 90);
            this.btn5.TabIndex = 73;
            this.btn5.Text = "5";
            this.btn5.UseVisualStyleBackColor = true;
            this.btn5.Click += new System.EventHandler(this.btn5_Click);
            // 
            // btn4
            // 
            this.btn4.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn4.Location = new System.Drawing.Point(250, 167);
            this.btn4.Name = "btn4";
            this.btn4.Size = new System.Drawing.Size(160, 90);
            this.btn4.TabIndex = 72;
            this.btn4.Text = "4";
            this.btn4.UseVisualStyleBackColor = true;
            this.btn4.Click += new System.EventHandler(this.btn4_Click);
            // 
            // btn3
            // 
            this.btn3.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn3.Location = new System.Drawing.Point(582, 76);
            this.btn3.Name = "btn3";
            this.btn3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btn3.Size = new System.Drawing.Size(160, 90);
            this.btn3.TabIndex = 71;
            this.btn3.Text = "3";
            this.btn3.UseVisualStyleBackColor = true;
            this.btn3.Click += new System.EventHandler(this.btn3_Click);
            // 
            // btn2
            // 
            this.btn2.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn2.Location = new System.Drawing.Point(416, 75);
            this.btn2.Name = "btn2";
            this.btn2.Size = new System.Drawing.Size(160, 90);
            this.btn2.TabIndex = 70;
            this.btn2.Text = "2";
            this.btn2.UseVisualStyleBackColor = true;
            this.btn2.Click += new System.EventHandler(this.btn2_Click);
            // 
            // btn1
            // 
            this.btn1.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn1.Location = new System.Drawing.Point(250, 75);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(160, 90);
            this.btn1.TabIndex = 69;
            this.btn1.Text = "1";
            this.btn1.UseVisualStyleBackColor = true;
            this.btn1.Click += new System.EventHandler(this.btn1_Click);
            // 
            // FWhouseTransfer7Inch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel4);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FWhouseTransfer7Inch";
            this.Text = "IFlash - Warehouse Transfer";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Activated += new System.EventHandler(this.FWhouseTransfer_Activated);
            this.Load += new System.EventHandler(this.FWhouseTransfer_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button btnNewTrans;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbFuelman;
        private System.Windows.Forms.ComboBox cmbWhPenerima;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblLiters;
        private System.Windows.Forms.Label lblQty;
        private System.Windows.Forms.TextBox txtFuelQty;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtQtyOrder;
        private System.Windows.Forms.Button btn0;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btn9;
        private System.Windows.Forms.Button btn8;
        private System.Windows.Forms.Button btn7;
        private System.Windows.Forms.Button btn6;
        private System.Windows.Forms.Button btn5;
        private System.Windows.Forms.Button btn4;
        private System.Windows.Forms.Button btn3;
        private System.Windows.Forms.Button btn2;
        private System.Windows.Forms.Button btn1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label lblWHpenerima;
        private System.Windows.Forms.ListBox listWHPenerima;
        private System.Windows.Forms.TextBox txtBxWHPenerima;
        private System.Windows.Forms.ListBox listFuelman;
        private System.Windows.Forms.TextBox txtBxFuelman;
    }
}