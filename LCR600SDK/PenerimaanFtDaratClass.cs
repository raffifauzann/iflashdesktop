﻿using System;
using System.Collections.Generic;

using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace LCR600SDK
{
    public partial class PenerimaanFtDaratClass
    {
        [JsonProperty("status", NullValueHandling = NullValueHandling.Ignore)]
        public bool? Status { get; set; }

        [JsonProperty("remarks", NullValueHandling = NullValueHandling.Ignore)]
        public string Remarks { get; set; }

        [JsonProperty("listData", NullValueHandling = NullValueHandling.Ignore)]
        public List<ListPenerimaanFtDaratClass> ListData { get; set; }

        [JsonProperty("totalPage", NullValueHandling = NullValueHandling.Ignore)]
        public long? TotalPage { get; set; }

        [JsonProperty("currentPage", NullValueHandling = NullValueHandling.Ignore)]
        public long? CurrentPage { get; set; }

        [JsonProperty("pageSize", NullValueHandling = NullValueHandling.Ignore)]
        public long? PageSize { get; set; }

        [JsonProperty("totalSize", NullValueHandling = NullValueHandling.Ignore)]
        public long? TotalSize { get; set; }
    }

    public partial class ListPenerimaanFtDaratClass
    {
        [JsonProperty("pid_delivery_darat", NullValueHandling = NullValueHandling.Ignore)]
        public string PidDeliveryDarat { get; set; }

        [JsonProperty("district", NullValueHandling = NullValueHandling.Ignore)]
        public string District { get; set; }

        [JsonProperty("po_no", NullValueHandling = NullValueHandling.Ignore)]
        public string PoNo { get; set; }

        [JsonProperty("po_qty", NullValueHandling = NullValueHandling.Ignore)]
        public long? PoQty { get; set; }

        [JsonProperty("transportir_code", NullValueHandling = NullValueHandling.Ignore)]
        public string TransportirCode { get; set; }

        [JsonProperty("transportir_name", NullValueHandling = NullValueHandling.Ignore)]
        public string TransportirName { get; set; }

        [JsonProperty("plan_start_period", NullValueHandling = NullValueHandling.Ignore)]
        public DateTime? PlanStartPeriod { get; set; }

        [JsonProperty("plan_finish_period", NullValueHandling = NullValueHandling.Ignore)]
        public DateTime? PlanFinishPeriod { get; set; }

        [JsonProperty("doc_status", NullValueHandling = NullValueHandling.Ignore)]
        public string DocStatus { get; set; }

        [JsonProperty("doc_description", NullValueHandling = NullValueHandling.Ignore)]
        public string DocDescription { get; set; }

        [JsonProperty("text_header", NullValueHandling = NullValueHandling.Ignore)]
        public string TextHeader { get; set; }

        [JsonProperty("text_sub_header", NullValueHandling = NullValueHandling.Ignore)]
        public string TextSubHeader { get; set; }

        [JsonProperty("text_body", NullValueHandling = NullValueHandling.Ignore)]
        public string TextBody { get; set; }

        [JsonProperty("is_editable", NullValueHandling = NullValueHandling.Ignore)]
        public long? IsEditable { get; set; }

        [JsonProperty("last_mod_by", NullValueHandling = NullValueHandling.Ignore)]
        public string LastModBy { get; set; }

        [JsonProperty("last_mod_date", NullValueHandling = NullValueHandling.Ignore)]
        public DateTime? LastModDate { get; set; }
    }
}

