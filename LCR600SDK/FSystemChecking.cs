﻿using Flurl.Http;
using MySql.Data.MySqlClient;
using System.Data.SQLite;
using System.IO;
using System.Reflection;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data;

namespace LCR600SDK
{
    public partial class FSystemChecking : Form
    {
        string query = string.Empty;
        int recs = 0;
        private string myConnectionString = string.Empty;
        string notifError;
        PasswordEncrypDecrypt decryptor = new PasswordEncrypDecrypt();
        WriteLogText log = new WriteLogText();
        public FSystemChecking()
        {
            InitializeComponent();
        }

        private async void FSystemChecking_Load(object sender, EventArgs e)
        {
            var _proper = Properties.Settings.Default;
            var passPhrase = "iFlashDesktop";
            var decryp = decryptor.DecryptPassword(_proper.db_pass, passPhrase);
            myConnectionString = "Data Source=" + Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "SQLite", "db_iflash.db") + " ;New=False;";
            this.Text = "System Checking " + GlobalModel.app_version;
            SQLiteConnection conn = new SQLiteConnection(myConnectionString);

            try
            {
               
                if (conn.State != ConnectionState.Open) conn.Open();


                var getFuelman = await CheckFuelman(conn);
                if (getFuelman > 0)
                {
                    lblCheckFuelman.Text = "OK";
                    log.WriteToFile("logLCR600.txt", "DB Local for Fuelman ...OK");
                    lblCheckFuelman.BackColor = Color.Green;
                    lblCheckFuelman.ForeColor = Color.White;
                }
                else
                {
                    lblCheckFuelman.Text = "ERROR";
                    log.WriteToFile("logLCR600.txt", "DB Local for Fuelman ...ERROR");
                    lblCheckFuelman.BackColor = Color.Red;
                    lblCheckFuelman.ForeColor = Color.White;
                }

                var getFlowmeter = await CheckFlowmeter(conn);
                if (getFlowmeter > 0)
                {
                    lblCheckFlowmeter.Text = "OK";
                    log.WriteToFile("logLCR600.txt", "DB Local for Flowmeter ...OK");
                    lblCheckFlowmeter.BackColor = Color.Green;
                    lblCheckFlowmeter.ForeColor = Color.White;
                }
                else
                {
                    lblCheckFlowmeter.Text = "ERROR";
                    log.WriteToFile("logLCR600.txt", "DB Local for Flowmeter ...ERROR");
                    lblCheckFlowmeter.BackColor = Color.Red;
                    lblCheckFlowmeter.ForeColor = Color.White;
                }


                var getUnit = await CheckUnit(conn);
                if (getUnit > 0)
                {
                    lblCheckUnit.Text = "OK";
                    log.WriteToFile("logLCR600.txt", "DB Local for Unit ...OK");
                    lblCheckUnit.BackColor = Color.Green;
                    lblCheckUnit.ForeColor = Color.White;
                }
                else
                {
                    lblCheckUnit.Text = "ERROR";
                    log.WriteToFile("logLCR600.txt", "DB Local for Unit ...ERROR");
                    lblCheckUnit.BackColor = Color.Red;
                    lblCheckUnit.ForeColor = Color.White;
                }

                var getTransportir = await CheckTransportir(conn);
                if (getTransportir > 0)
                {
                    lblCheckTransportir.Text = "OK";
                    log.WriteToFile("logLCR600.txt", "DB Local for Transportir ...OK");
                    lblCheckTransportir.BackColor = Color.Green;
                    lblCheckTransportir.ForeColor = Color.White;
                }
                else
                {
                    lblCheckTransportir.Text = "ERROR";
                    log.WriteToFile("logLCR600.txt", "DB Local for Transportir ...ERROR");
                    lblCheckTransportir.BackColor = Color.Red;
                    lblCheckTransportir.ForeColor = Color.White;
                }

                var cek = await checkSetingWarehouse();
                if (cek.Contains("Kosong") || cek.Contains("Belum") )
                {
                    lblCheckWarehouse.Text = "ERROR, Click!";
                    lblCheckWarehouse.BackColor = Color.Red;
                    lblCheckWarehouse.ForeColor = Color.White;
                    this.Show();
                    notifError = cek;
                    log.WriteToFile("logLCR600.txt", "Setting warehouse belum lengkap detail : " + cek);
                }
                else
                {
                    notifError = cek;                    
                    lblCheckWarehouse.Text = "OK";
                    log.WriteToFile("logLCR600.txt", "Setting warehouse ...OK");
                    lblCheckWarehouse.BackColor = Color.Green;
                    lblCheckWarehouse.ForeColor = Color.White;
                }

                if (conn.State == ConnectionState.Open) conn.Close();
            }
            catch (Exception ex)
            {
                //log.WriteToFile("logLCR600.txt", "FSystemChecking.cs Exception : "+ex);
                //MessageBox.Show(ex.Message);
                if (conn.State == ConnectionState.Open) conn.Close();
                MessageBox.Show("FSystemChecking.cs Exception: Terjadi kesalahan saat load system checking, mohon cek log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", "FSystemChecking.cs Exception : " + ex);
                save_error_log(ex.Message, "FSystemChecking_Load()");
            }
        }

        private void LblCheckWarehouse_Click(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private async Task<int> CheckFuelman(SQLiteConnection conn)
        {
            query = "Select Count(*) from fuelman";
            SQLiteCommand cmd = new SQLiteCommand(query, conn);
            var ret = Convert.ToInt32(cmd.ExecuteScalar());

            return ret;
        }

        private async Task<int> CheckFlowmeter(SQLiteConnection conn)
        {
            query = "Select Count(*) from flowmeter";
            SQLiteCommand cmd = new SQLiteCommand(query, conn);
            var ret = Convert.ToInt32(cmd.ExecuteScalar());

            return ret;
        }
        private async Task<int> CheckUnit(SQLiteConnection conn)
        {
            query = "Select Count(*) from unit";
            SQLiteCommand cmd = new SQLiteCommand(query, conn);
            var ret = Convert.ToInt32(cmd.ExecuteScalar());

            return ret;
        }
        private async Task<int> CheckTransportir(SQLiteConnection conn)
        {
            query = "Select Count(*) from transportir";
            SQLiteCommand cmd = new SQLiteCommand(query, conn);
            var ret = Convert.ToInt32(cmd.ExecuteScalar());

            return ret;
        }
        private async Task<int> CheckWarehouse(SQLiteConnection conn)
        {
            query = "Select Count(*) from tbl_setting";
            SQLiteCommand cmd = new SQLiteCommand(query, conn);
            var ret = Convert.ToInt32(cmd.ExecuteScalar());

            return ret;
        }
        private async Task<string> checkSetingWarehouse()
        {
            SQLiteConnection conn;

            conn = new SQLiteConnection(myConnectionString);
            if (conn.State != ConnectionState.Open) conn.Open();
            string query = string.Empty;

            query = "SELECT * FROM tbl_setting";
            SQLiteCommand cmds = new SQLiteCommand(query, conn);
            SQLiteDataReader reader = cmds.ExecuteReader();
            var pid = string.Empty;
            var whouseid = string.Empty;
            var whousename = string.Empty;
            var sncode = string.Empty;
            var meterfaktor = string.Empty;
            var shift1 = string.Empty;
            var shift2 = string.Empty;

            while (reader.Read())
            {
                pid = reader["Pid"].ToString();
                whouseid = reader["WhouseId"].ToString();
                whousename = reader["WhouseName"].ToString();
                sncode = reader["SnCode"].ToString();
                meterfaktor = reader["MeterFakor"].ToString();
                shift1 = reader["startShift1"].ToString();
                shift2 = reader["startShift2"].ToString();

            }
            var ret = "";
            bool valid = false;
            if (string.IsNullOrEmpty(pid))
            {
                ret = "Belum Setting Warehouse";                
            }
            else
            {
                ret = string.IsNullOrEmpty(whouseid) ? "Warehouse Kosong" : "Warehouse Ok";
                ret += string.IsNullOrEmpty(whousename) ? Environment.NewLine + "Warehouse Name Kosong" : Environment.NewLine + "Warehouse Name Ok";
                ret += string.IsNullOrEmpty(sncode) ? Environment.NewLine + "SN Code Kosong" : Environment.NewLine + "SN Code Ok";
                ret += string.IsNullOrEmpty(shift1) ? Environment.NewLine + "Shift 1 Belum Set jam" : Environment.NewLine + "Shift 1 Ok";
                ret += string.IsNullOrEmpty(shift2) ? Environment.NewLine + "Shift 2 Belum Set jam" : Environment.NewLine + "Shift 2 Ok";
            }

            reader.Dispose();
            if (conn.State == ConnectionState.Open) conn.Close();
            return ret;

        }

        private void btnCheckDone_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        void save_error_log(string data, string actions)
        {
            try
            {
                DateTime localDate = DateTime.Now;
                var _item = new tbl_log();
                var initData = GlobalModel.GlobalVar;

                _item.pid = System.Guid.NewGuid().ToString();
                _item.actions = $"District {initData.DataEmp.DstrctCode}/FMain/{actions}";
                _item.message = $"{data}";
                _item.mod_date = localDate;
                _item.mod_by = initData.DataEmp.Nrp;

                SQLiteConnection conn;

                conn = new SQLiteConnection(myConnectionString);
                if (conn.State != ConnectionState.Open) conn.Open();
                string query = string.Empty;
                query = "INSERT INTO tbl_log(pid,actions,message,mod_date,mod_by)"
                      + " VALUES(@pid,@actions,@message,@mod_date,@mod_by)";

                SQLiteCommand cmd = new SQLiteCommand(query, conn);

                cmd.Parameters.Add(new SQLiteParameter("@pid", _item.pid));
                cmd.Parameters.Add(new SQLiteParameter("@actions", _item.actions));
                cmd.Parameters.Add(new SQLiteParameter("@message", _item.message));
                cmd.Parameters.Add(new SQLiteParameter("@mod_date", _item.mod_date));
                cmd.Parameters.Add(new SQLiteParameter("@mod_by", _item.mod_by));
                cmd.ExecuteNonQuery();
                if (conn.State == ConnectionState.Open) conn.Close();
            }
            catch (Exception)
            {
            }
        }

        private void lblCheckWarehouse_Click(object sender, EventArgs e)
        {
            var f = new FPopUpNotification(notifError, "Warehouse setting error!");
            f.ShowDialog();
        }
    }
}
