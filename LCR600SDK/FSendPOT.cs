﻿using Flurl.Http;
using LCR600SDK.DataClass;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LCR600SDK
{
    public partial class FSendPOT : Form
    {
        private string URL_API = string.Empty;
        private string URL_API_CALL = string.Empty;
        public string pPage = "1";
        public string pPageSize = "50";
        public string pActivity = "darat";
        public string pStatus = "Plan Darat";
        public string pStart = "";
        public string pEnd = "";
        private string pUrl_service = string.Empty;

        public FSendPOT()
        {
            InitializeComponent();
        }

        private void FReceivedFTPort_Load(object sender, EventArgs e)
        {
            var _proper = Properties.Settings.Default;
            pUrl_service = _proper.url_service;
            URL_API = $"{pUrl_service}api/FuelService/getRitasiMaintankDesktop";
            String _url = $"{URL_API}?page={pPage}&pageSize={pPageSize}&activity={pActivity}&status={pStatus}&start={pStart}&end={pEnd}";
            this.Text = "Pengiriman POT " + GlobalModel.app_version;
            loadGridAsync(_url);
        }
        void dataGridView1_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            dataGridView1.BorderStyle = BorderStyle.None;
            dataGridView1.AlternatingRowsDefaultCellStyle.BackColor = Color.FromArgb(238, 239, 249);
            dataGridView1.CellBorderStyle = DataGridViewCellBorderStyle.SingleHorizontal;
            dataGridView1.DefaultCellStyle.SelectionBackColor = Color.SeaGreen;
            dataGridView1.DefaultCellStyle.SelectionForeColor = Color.WhiteSmoke;
            dataGridView1.BackgroundColor = Color.FromArgb(0, 101, 179);
            dataGridView1.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridView1.EnableHeadersVisualStyles = false;
            dataGridView1.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
            dataGridView1.ColumnHeadersDefaultCellStyle.Font = new Font("MS Reference Sans Serif", 20);
            dataGridView1.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(0, 101, 179);
            dataGridView1.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
        }
        public async void loadGridAsync(string _url)
        {
            URL_API_CALL = _url;
            var _auth = GlobalModel.loginModel;  
            try
            {
                var datas = await _url.WithHeaders(new { Authorization = _auth.token }).GetJsonAsync<RitasiMaintankReceiveClass>();
                GlobalModel.ritasiMaintankReceive = datas;
                setPaging();
                DataTable dt = GeneralFunc.ToDataTable(GlobalModel.ritasiMaintankReceive.ListData);
                dataGridView1.DataSource = dt;
                button2.Text = "Go To Page";
                button2.BackColor = Color.MidnightBlue;
                button2.ForeColor = Color.White;


                Console.WriteLine("Get token Success");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            } 
        } 

        void setPaging()
        {
            try
            { 
                var _data = GlobalModel.ritasiMaintankReceive;
                txtPage.Text = $"{_data.CurrentPage}";
                lblPageOf.Text = $"Page {_data.CurrentPage} of {_data.TotalPage}    - Total rows {_data.TotalSize}";
                btnNext.Enabled = (_data.TotalPage > _data.CurrentPage);
                btnPrev.Enabled = (_data.CurrentPage > 1);
            }
            catch(Exception)
            {
                MessageBox.Show("Tidak ada data untuk di tampilan", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.Columns[e.ColumnIndex].Name == "Action")
            {
                var f = new FReceivedFTPortForm();
                f.URL_API_CALL = URL_API_CALL;
                f.CALL_FROM = "RECV_DARAT_OWNER";
                f.pData = GlobalModel.ritasiMaintankReceive.ListData[e.RowIndex];
                f.ShowDialog(); 
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            String _url = $"{URL_API}?page={pPage}&pageSize={pPageSize}&activity={pActivity}&search={txtSearch.Text}&status={pStatus}&start={dateTimePicker1.Text}&end={dateTimePicker2.Text}";
            loadGridAsync(_url);
        }

        private void txtSearch_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            {
                String _url = $"{URL_API}?page={pPage}&pageSize={pPageSize}&activity={pActivity}&search={txtSearch.Text}&status={pStatus}&start={pStart}&end={pEnd}";
                loadGridAsync(_url);
            }
        }

        private void btnPrev_Click(object sender, EventArgs e)
        {
            String _url = $"{URL_API}?page={Convert.ToInt32(txtPage.Text) - 1}&pageSize={pPageSize}&activity={pActivity}&search={txtSearch.Text}&status={pStatus}&start={pStart}&end={pEnd}";
            loadGridAsync(_url);
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            String _url = $"{URL_API}?page={Convert.ToInt32(txtPage.Text) + 1}&pageSize={pPageSize }&activity={pActivity}&search={txtSearch.Text}&status={pStatus}&start={pStart}&end={pEnd}";
            loadGridAsync(_url);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        { 
            var f = new FSendPOTForm();
            f.URL_API_CALL = URL_API_CALL;
            f.CALL_FROM = "SEND_DARAT"; 
            f.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void dataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs args)
        {
            if (args.ColumnIndex == 4)
            {
                var SendDate = args.Value.ToString();
                args.Value = DateTime.Parse(SendDate).ToString("dd-MM-yyyy");
                args.FormattingApplied = true;
            }            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var _data = GlobalModel.ritasiMaintankReceive;
            button2.Text = "Waiting";
            button2.ForeColor = Color.Black;
            button2.BackColor = Color.Gold;
            if (numGoToPage.Value > _data.TotalPage)
            {
                numGoToPage.Value = decimal.Parse(_data.TotalPage.ToString());
            }
            else if (numGoToPage.Value <= 0)
            {
                numGoToPage.Value = 1;
            }

            String _url = $"{URL_API}?page={numGoToPage.Value}&pageSize={pPageSize}&activity={pActivity}&search={txtSearch.Text}&status={pStatus}&start={pStart}&end={pEnd}";
            loadGridAsync(_url);
        }
    }
}
