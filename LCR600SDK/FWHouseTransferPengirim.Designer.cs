﻿
namespace LCR600SDK
{
    partial class FWHouseTransferPengirim
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FWHouseTransferPengirim));
            this.lblLiters = new System.Windows.Forms.Label();
            this.lblQty = new System.Windows.Forms.Label();
            this.txtFuelQty = new System.Windows.Forms.TextBox();
            this.cmbWhPenerima = new System.Windows.Forms.ComboBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.listFuelman = new System.Windows.Forms.ListBox();
            this.listWHPenerima = new System.Windows.Forms.ListBox();
            this.txtBxWHPenerima = new System.Windows.Forms.TextBox();
            this.txtBxFuelman = new System.Windows.Forms.TextBox();
            this.btn0 = new System.Windows.Forms.Button();
            this.txtQtyOrder = new System.Windows.Forms.TextBox();
            this.btnClear = new System.Windows.Forms.Button();
            this.btn9 = new System.Windows.Forms.Button();
            this.btn8 = new System.Windows.Forms.Button();
            this.btn7 = new System.Windows.Forms.Button();
            this.btn6 = new System.Windows.Forms.Button();
            this.btn5 = new System.Windows.Forms.Button();
            this.btn4 = new System.Windows.Forms.Button();
            this.btn3 = new System.Windows.Forms.Button();
            this.btn2 = new System.Windows.Forms.Button();
            this.btn1 = new System.Windows.Forms.Button();
            this.cmbFuelman = new System.Windows.Forms.ComboBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtWHPenerima = new System.Windows.Forms.TextBox();
            this.txtFuelmanPenerima = new System.Windows.Forms.TextBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnNewTrans = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblLiters
            // 
            this.lblLiters.AutoSize = true;
            this.lblLiters.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLiters.Location = new System.Drawing.Point(360, 397);
            this.lblLiters.Name = "lblLiters";
            this.lblLiters.Size = new System.Drawing.Size(91, 25);
            this.lblLiters.TabIndex = 4;
            this.lblLiters.Text = "LITERS";
            this.lblLiters.Visible = false;
            // 
            // lblQty
            // 
            this.lblQty.AutoSize = true;
            this.lblQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQty.Location = new System.Drawing.Point(23, 278);
            this.lblQty.Name = "lblQty";
            this.lblQty.Size = new System.Drawing.Size(115, 25);
            this.lblQty.TabIndex = 3;
            this.lblQty.Text = "Fuel Qty :";
            this.lblQty.Visible = false;
            // 
            // txtFuelQty
            // 
            this.txtFuelQty.Enabled = false;
            this.txtFuelQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFuelQty.Location = new System.Drawing.Point(28, 306);
            this.txtFuelQty.Name = "txtFuelQty";
            this.txtFuelQty.Size = new System.Drawing.Size(326, 116);
            this.txtFuelQty.TabIndex = 2;
            this.txtFuelQty.Visible = false;
            // 
            // cmbWhPenerima
            // 
            this.cmbWhPenerima.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbWhPenerima.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbWhPenerima.DropDownHeight = 300;
            this.cmbWhPenerima.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.cmbWhPenerima.FormattingEnabled = true;
            this.cmbWhPenerima.IntegralHeight = false;
            this.cmbWhPenerima.Location = new System.Drawing.Point(6, 227);
            this.cmbWhPenerima.Name = "cmbWhPenerima";
            this.cmbWhPenerima.Size = new System.Drawing.Size(232, 37);
            this.cmbWhPenerima.TabIndex = 0;
            this.cmbWhPenerima.Visible = false;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.listFuelman);
            this.panel4.Controls.Add(this.listWHPenerima);
            this.panel4.Controls.Add(this.txtBxWHPenerima);
            this.panel4.Controls.Add(this.txtBxFuelman);
            this.panel4.Controls.Add(this.btn0);
            this.panel4.Controls.Add(this.txtQtyOrder);
            this.panel4.Controls.Add(this.btnClear);
            this.panel4.Controls.Add(this.btn9);
            this.panel4.Controls.Add(this.btn8);
            this.panel4.Controls.Add(this.btn7);
            this.panel4.Controls.Add(this.btn6);
            this.panel4.Controls.Add(this.btn5);
            this.panel4.Controls.Add(this.btn4);
            this.panel4.Controls.Add(this.btn3);
            this.panel4.Controls.Add(this.btn2);
            this.panel4.Controls.Add(this.btn1);
            this.panel4.Controls.Add(this.lblLiters);
            this.panel4.Controls.Add(this.lblQty);
            this.panel4.Controls.Add(this.txtFuelQty);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(244, 91);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(784, 493);
            this.panel4.TabIndex = 9;
            // 
            // listFuelman
            // 
            this.listFuelman.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.listFuelman.FormattingEnabled = true;
            this.listFuelman.ItemHeight = 29;
            this.listFuelman.Location = new System.Drawing.Point(28, 132);
            this.listFuelman.Name = "listFuelman";
            this.listFuelman.Size = new System.Drawing.Size(235, 33);
            this.listFuelman.TabIndex = 95;
            this.listFuelman.Visible = false;
            this.listFuelman.Click += new System.EventHandler(this.listFuelman_Click);
            // 
            // listWHPenerima
            // 
            this.listWHPenerima.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.listWHPenerima.FormattingEnabled = true;
            this.listWHPenerima.ItemHeight = 29;
            this.listWHPenerima.Location = new System.Drawing.Point(28, 69);
            this.listWHPenerima.Name = "listWHPenerima";
            this.listWHPenerima.Size = new System.Drawing.Size(235, 33);
            this.listWHPenerima.TabIndex = 93;
            this.listWHPenerima.Visible = false;
            this.listWHPenerima.Click += new System.EventHandler(this.listWHPenerima_Click);
            // 
            // txtBxWHPenerima
            // 
            this.txtBxWHPenerima.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBxWHPenerima.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.txtBxWHPenerima.Location = new System.Drawing.Point(28, 38);
            this.txtBxWHPenerima.Name = "txtBxWHPenerima";
            this.txtBxWHPenerima.Size = new System.Drawing.Size(235, 35);
            this.txtBxWHPenerima.TabIndex = 94;
            this.txtBxWHPenerima.Click += new System.EventHandler(this.txtBxWHPenerima_Click);
            this.txtBxWHPenerima.TextChanged += new System.EventHandler(this.txtBxWHPenerima_TextChanged);
            // 
            // txtBxFuelman
            // 
            this.txtBxFuelman.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.txtBxFuelman.Location = new System.Drawing.Point(28, 97);
            this.txtBxFuelman.Name = "txtBxFuelman";
            this.txtBxFuelman.Size = new System.Drawing.Size(235, 35);
            this.txtBxFuelman.TabIndex = 96;
            this.txtBxFuelman.Click += new System.EventHandler(this.txtBxFuelman_Click);
            this.txtBxFuelman.TextChanged += new System.EventHandler(this.txtBxFuelman_TextChanged);
            // 
            // btn0
            // 
            this.btn0.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn0.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn0.Location = new System.Drawing.Point(469, 323);
            this.btn0.Name = "btn0";
            this.btn0.Size = new System.Drawing.Size(150, 100);
            this.btn0.TabIndex = 90;
            this.btn0.Text = "0";
            this.btn0.UseVisualStyleBackColor = true;
            this.btn0.Click += new System.EventHandler(this.btn0_Click);
            // 
            // txtQtyOrder
            // 
            this.txtQtyOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.txtQtyOrder.Location = new System.Drawing.Point(28, 166);
            this.txtQtyOrder.Name = "txtQtyOrder";
            this.txtQtyOrder.Size = new System.Drawing.Size(235, 35);
            this.txtQtyOrder.TabIndex = 92;
            this.txtQtyOrder.Text = "0";
            // 
            // btnClear
            // 
            this.btnClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClear.BackColor = System.Drawing.Color.Red;
            this.btnClear.FlatAppearance.BorderSize = 0;
            this.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.ForeColor = System.Drawing.Color.White;
            this.btnClear.Location = new System.Drawing.Point(315, 323);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(150, 100);
            this.btnClear.TabIndex = 89;
            this.btnClear.Text = "CLEAR";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btn9
            // 
            this.btn9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn9.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn9.Location = new System.Drawing.Point(627, 217);
            this.btn9.Name = "btn9";
            this.btn9.Size = new System.Drawing.Size(150, 100);
            this.btn9.TabIndex = 88;
            this.btn9.Text = "9";
            this.btn9.UseVisualStyleBackColor = true;
            this.btn9.Click += new System.EventHandler(this.btn9_Click);
            // 
            // btn8
            // 
            this.btn8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn8.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn8.Location = new System.Drawing.Point(471, 217);
            this.btn8.Name = "btn8";
            this.btn8.Size = new System.Drawing.Size(150, 100);
            this.btn8.TabIndex = 87;
            this.btn8.Text = "8";
            this.btn8.UseVisualStyleBackColor = true;
            this.btn8.Click += new System.EventHandler(this.btn8_Click);
            // 
            // btn7
            // 
            this.btn7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn7.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn7.Location = new System.Drawing.Point(315, 217);
            this.btn7.Name = "btn7";
            this.btn7.Size = new System.Drawing.Size(150, 100);
            this.btn7.TabIndex = 86;
            this.btn7.Text = "7";
            this.btn7.UseVisualStyleBackColor = true;
            this.btn7.Click += new System.EventHandler(this.btn7_Click);
            // 
            // btn6
            // 
            this.btn6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn6.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn6.Location = new System.Drawing.Point(625, 111);
            this.btn6.Name = "btn6";
            this.btn6.Size = new System.Drawing.Size(150, 100);
            this.btn6.TabIndex = 85;
            this.btn6.Text = "6";
            this.btn6.UseVisualStyleBackColor = true;
            this.btn6.Click += new System.EventHandler(this.btn6_Click);
            // 
            // btn5
            // 
            this.btn5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn5.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn5.Location = new System.Drawing.Point(469, 111);
            this.btn5.Name = "btn5";
            this.btn5.Size = new System.Drawing.Size(150, 100);
            this.btn5.TabIndex = 84;
            this.btn5.Text = "5";
            this.btn5.UseVisualStyleBackColor = true;
            this.btn5.Click += new System.EventHandler(this.btn5_Click);
            // 
            // btn4
            // 
            this.btn4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn4.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn4.Location = new System.Drawing.Point(313, 111);
            this.btn4.Name = "btn4";
            this.btn4.Size = new System.Drawing.Size(150, 100);
            this.btn4.TabIndex = 83;
            this.btn4.Text = "4";
            this.btn4.UseVisualStyleBackColor = true;
            this.btn4.Click += new System.EventHandler(this.btn4_Click);
            // 
            // btn3
            // 
            this.btn3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn3.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn3.Location = new System.Drawing.Point(627, 6);
            this.btn3.Name = "btn3";
            this.btn3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btn3.Size = new System.Drawing.Size(150, 100);
            this.btn3.TabIndex = 82;
            this.btn3.Text = "3";
            this.btn3.UseVisualStyleBackColor = true;
            this.btn3.Click += new System.EventHandler(this.btn3_Click);
            // 
            // btn2
            // 
            this.btn2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn2.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn2.Location = new System.Drawing.Point(471, 6);
            this.btn2.Name = "btn2";
            this.btn2.Size = new System.Drawing.Size(150, 100);
            this.btn2.TabIndex = 81;
            this.btn2.Text = "2";
            this.btn2.UseVisualStyleBackColor = true;
            this.btn2.Click += new System.EventHandler(this.btn2_Click);
            // 
            // btn1
            // 
            this.btn1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn1.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn1.Location = new System.Drawing.Point(315, 6);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(150, 100);
            this.btn1.TabIndex = 80;
            this.btn1.Text = "1";
            this.btn1.UseVisualStyleBackColor = true;
            this.btn1.Click += new System.EventHandler(this.btn1_Click);
            // 
            // cmbFuelman
            // 
            this.cmbFuelman.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbFuelman.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbFuelman.DropDownHeight = 300;
            this.cmbFuelman.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.cmbFuelman.FormattingEnabled = true;
            this.cmbFuelman.IntegralHeight = false;
            this.cmbFuelman.Location = new System.Drawing.Point(6, 288);
            this.cmbFuelman.Name = "cmbFuelman";
            this.cmbFuelman.Size = new System.Drawing.Size(232, 37);
            this.cmbFuelman.TabIndex = 1;
            this.cmbFuelman.Visible = false;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.txtWHPenerima);
            this.panel3.Controls.Add(this.txtFuelmanPenerima);
            this.panel3.Controls.Add(this.cmbWhPenerima);
            this.panel3.Controls.Add(this.cmbFuelman);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(0, 91);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(244, 493);
            this.panel3.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(87, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(144, 25);
            this.label1.TabIndex = 93;
            this.label1.Text = "WH Penerima";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(39, 111);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(192, 25);
            this.label5.TabIndex = 92;
            this.label5.Text = "Fuelman Penerima";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.label4.Location = new System.Drawing.Point(5, 169);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(238, 29);
            this.label4.TabIndex = 91;
            this.label4.Text = "Quantity Order (Liter)";
            // 
            // txtWHPenerima
            // 
            this.txtWHPenerima.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtWHPenerima.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWHPenerima.Location = new System.Drawing.Point(3, 334);
            this.txtWHPenerima.Margin = new System.Windows.Forms.Padding(2);
            this.txtWHPenerima.Name = "txtWHPenerima";
            this.txtWHPenerima.Size = new System.Drawing.Size(228, 32);
            this.txtWHPenerima.TabIndex = 5;
            this.txtWHPenerima.Visible = false;
            // 
            // txtFuelmanPenerima
            // 
            this.txtFuelmanPenerima.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFuelmanPenerima.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFuelmanPenerima.Location = new System.Drawing.Point(1, 373);
            this.txtFuelmanPenerima.Margin = new System.Windows.Forms.Padding(2);
            this.txtFuelmanPenerima.Name = "txtFuelmanPenerima";
            this.txtFuelmanPenerima.Size = new System.Drawing.Size(229, 32);
            this.txtFuelmanPenerima.TabIndex = 6;
            this.txtFuelmanPenerima.Visible = false;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.pictureBox1);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(99, 91);
            this.panel5.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(99, 91);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 40;
            this.pictureBox1.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic) 
                | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Blue;
            this.label3.Location = new System.Drawing.Point(248, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(379, 55);
            this.label3.TabIndex = 1;
            this.label3.Text = "TRANSACTION";
            // 
            // btnNewTrans
            // 
            this.btnNewTrans.BackColor = System.Drawing.Color.Blue;
            this.btnNewTrans.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnNewTrans.FlatAppearance.BorderSize = 0;
            this.btnNewTrans.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNewTrans.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNewTrans.ForeColor = System.Drawing.Color.White;
            this.btnNewTrans.Location = new System.Drawing.Point(0, 0);
            this.btnNewTrans.Name = "btnNewTrans";
            this.btnNewTrans.Size = new System.Drawing.Size(1028, 138);
            this.btnNewTrans.TabIndex = 0;
            this.btnNewTrans.Text = "NEW TRANSACTION";
            this.btnNewTrans.UseVisualStyleBackColor = false;
            this.btnNewTrans.Visible = false;
            this.btnNewTrans.Click += new System.EventHandler(this.btnNewTrans_Click);
            // 
            // btnStart
            // 
            this.btnStart.BackColor = System.Drawing.Color.Green;
            this.btnStart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnStart.FlatAppearance.BorderSize = 0;
            this.btnStart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStart.ForeColor = System.Drawing.Color.White;
            this.btnStart.Location = new System.Drawing.Point(0, 0);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(1028, 138);
            this.btnStart.TabIndex = 0;
            this.btnStart.Text = "START";
            this.btnStart.UseVisualStyleBackColor = false;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnNewTrans);
            this.panel1.Controls.Add(this.btnStart);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 584);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1028, 138);
            this.panel1.TabIndex = 6;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.button1);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.panel5);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1028, 91);
            this.panel2.TabIndex = 7;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.BackColor = System.Drawing.Color.Red;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(871, 20);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(109, 44);
            this.button1.TabIndex = 82;
            this.button1.Text = "Close";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // FWHouseTransferPengirim
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1028, 722);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FWHouseTransferPengirim";
            this.Text = "IFlash - Warehouse Transfer";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FWHouseTransferPengirim_Load);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblLiters;
        private System.Windows.Forms.Label lblQty;
        private System.Windows.Forms.TextBox txtFuelQty;
        private System.Windows.Forms.ComboBox cmbWhPenerima;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.ComboBox cmbFuelman;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox txtWHPenerima;
        private System.Windows.Forms.TextBox txtFuelmanPenerima;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnNewTrans;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btn0;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btn9;
        private System.Windows.Forms.Button btn8;
        private System.Windows.Forms.Button btn7;
        private System.Windows.Forms.Button btn6;
        private System.Windows.Forms.Button btn5;
        private System.Windows.Forms.Button btn4;
        private System.Windows.Forms.Button btn3;
        private System.Windows.Forms.Button btn2;
        private System.Windows.Forms.Button btn1;
        private System.Windows.Forms.TextBox txtQtyOrder;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ListBox listFuelman;
        private System.Windows.Forms.ListBox listWHPenerima;
        private System.Windows.Forms.TextBox txtBxWHPenerima;
        private System.Windows.Forms.TextBox txtBxFuelman;
    }
}