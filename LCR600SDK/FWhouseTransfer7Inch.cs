﻿using MySql.Data.MySqlClient; using System.Data.SQLite; using System.IO; using System.Reflection;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LCR600SDK
{
    public partial class FWhouseTransfer7Inch : Form
    {
        private string myConnectionString = string.Empty;
        public string whosueSelected = string.Empty;
        private string pShiftStart1 = string.Empty;
        private string pShiftStart2 = string.Empty;
        private string pWHouse = string.Empty;
        private string pSnCOde = string.Empty;
        PasswordEncrypDecrypt decryptor = new PasswordEncrypDecrypt();
        WriteLogText log = new WriteLogText();
        public FWhouseTransfer7Inch()
        {
            var _proper = Properties.Settings.Default;
            _proper.maxTickSecond = 35;
            //myConnectionString = "Data Source=" + Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "SQLite", "db_iflash.db") + " ;New=False;";
            var passPhrase = "iFlashDesktop";
            var decryp = decryptor.DecryptPassword(_proper.db_pass, passPhrase);
            myConnectionString = "Data Source=" + Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "SQLite", "db_iflash.db") + " ;New=False;";
            GlobalModel.AutoRefueling = new refueling();
            InitializeComponent();

            var acsc = new AutoCompleteStringCollection();
            txtBxWHPenerima.AutoCompleteCustomSource = acsc;
            txtBxWHPenerima.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            txtBxWHPenerima.AutoCompleteSource = AutoCompleteSource.CustomSource;

        }

        private void FWhouseTransfer_Load(object sender, EventArgs e)
        {
            initData();
            loadFlowMeter();
            loadFuelman();
        }

        void loadFlowMeter()
        {
            try
            {
                var _globalData = GlobalModel.GlobalVar;

                SQLiteConnection conn;
                conn = new SQLiteConnection(myConnectionString);
                if (conn.State != ConnectionState.Open) conn.Open();
                string query = string.Empty;
                query = $"SELECT * FROM flowmeter where DstrctCode = '{ _globalData.DataEmp.DstrctCode}' order by WhouseLocation";
                SQLiteCommand cmd = new SQLiteCommand(query, conn);
                SQLiteDataReader reader = cmd.ExecuteReader();

                List<ListItem> items = new List<ListItem>();
                while (reader.Read())
                {
                    items.Add(new ListItem(reader["Warehouse"].ToString(), reader["WhouseLocation"].ToString()));
                }

                cmbWhPenerima.DataSource = items;
                cmbWhPenerima.ValueMember = "value";
                cmbWhPenerima.DisplayMember = "text";

                listWHPenerima.DataSource = items;
                listWHPenerima.ValueMember = "value";
                listWHPenerima.DisplayMember = "text";


                reader.Dispose();
                if (conn.State == ConnectionState.Open) conn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
        void loadFuelman()
        {
            try
            {
                var _globalData = GlobalModel.GlobalVar;

                SQLiteConnection conn;
                conn = new SQLiteConnection(myConnectionString);
                if (conn.State != ConnectionState.Open) conn.Open();
                string query = string.Empty;
                query = $"SELECT * FROM fuelman where District = '{ _globalData.DataEmp.DstrctCode}' order by FuelmanName";
                SQLiteCommand cmd = new SQLiteCommand(query, conn);
                SQLiteDataReader reader = cmd.ExecuteReader();

                List<ListItem> items = new List<ListItem>();
                while (reader.Read())
                {
                    items.Add(new ListItem(reader["Nrp"].ToString(), $"{reader["Nrp"].ToString()}-{reader["FuelmanName"].ToString()}"));
                }

                cmbFuelman.DataSource = items;
                cmbFuelman.ValueMember = "value";
                cmbFuelman.DisplayMember = "text";

                listFuelman.DataSource = items;
                listFuelman.ValueMember = "value";
                listFuelman.DisplayMember = "text";

                reader.Dispose();
                if (conn.State == ConnectionState.Open) conn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

        }

        void clearTextBox()
        {
            lblQty.Visible = false;
            lblLiters.Visible = false;
            txtFuelQty.Visible = false;
            txtQtyOrder.Text = "0";
            //txtWHPenerima.Text = "";
            //txtFuelmanPenerima.Text = "";
            GlobalModel.AutoRefueling = new refueling();
        }

        private void btnNewTrans_Click(object sender, EventArgs e)
        {
            clearTextBox();
            btnNewTrans.Visible = false;
            btnStart.Visible = true;
            btnStart.Enabled = true;
            //txtWHPenerima.Enabled = true;
            //txtFuelmanPenerima.Enabled = true;

        }

        public void idleSystem()
        {
            btnNewTrans.Enabled = true;
            btnNewTrans.Visible = true;
            btnStart.Enabled = !btnNewTrans.Enabled;
            btnStart.Visible = false;
            lblQty.Visible = true;
            lblLiters.Visible = true;
            txtFuelQty.Visible = true;
            //txtWHPenerima.Enabled = false;
            //txtFuelmanPenerima.Enabled = false;
            //cmbWhPenerima.Visible = false;
            //cmbFuelman.Visible = false;
        }


        bool isValid()
        {
            return true;
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            if (isValid())
            {
                fillData();
                var f = new FmManual();
                f.preset = int.Parse(txtQtyOrder.Text) * 10;
                f.waitCounterMax = 35 * 10;
                if (f.ShowDialog() == DialogResult.OK)
                {
                    //txtFuelQty.Text = GlobalModel.AutoRefueling.receive_qty1.ToString();
                    txtFuelQty.Text = Convert.ToString(f.quantity);
                }
                else
                {
                    txtFuelQty.Text = Convert.ToString(f.quantity);
                    //txtFuelQty.Text = GlobalModel.AutoRefueling.receive_qty1.ToString();
                }
                //var f = new FRunLcrTransfer();
                ////f.ShowDialog();
                //if (f.ShowDialog() == DialogResult.OK)
                //{
                //    txtFuelQty.Text = Convert.ToString(f.LitersQty);
                //}
                //else
                //{
                //    txtFuelQty.Text = Convert.ToString(f.LitersQty);
                //}
            }

            idleSystem();
        }

        void initData()
        {
            //var _globalData = GlobalModel.settingWhouse;
            //txtWh.Text = _globalData.whouse[0].ToString();
            try
            {
                SQLiteConnection conn;

                conn = new SQLiteConnection(myConnectionString);
                if (conn.State != ConnectionState.Open) conn.Open();
                string query = string.Empty;

                query = "SELECT * FROM tbl_setting";
                SQLiteCommand cmds = new SQLiteCommand(query, conn);
                SQLiteDataReader reader = cmds.ExecuteReader();

                while (reader.Read())
                {
                    pWHouse = reader["Whouseid"].ToString();
                    pSnCOde = reader["SnCode"].ToString();
                }
                reader.Dispose();
                if (conn.State == ConnectionState.Open) conn.Close();

                initWhSetting(conn);
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
                MessageBox.Show("FWarehouseTransfer7Inch.cs Exception: Terjadi kesalahan saat initialisasi data, mohon cek log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", "FWarehouseTransfer7Inch.cs Exception initData : " + ex.Message);
                save_error_log(ex.Message, "initData()");
            }
        }

        void initWhSetting(SQLiteConnection conn)
        {
            try
            {
                var _globalData = GlobalModel.GlobalVar;

                if (conn.State != ConnectionState.Open) conn.Open();
                string query = string.Empty;
                query = $"SELECT * FROM flowmeter where Warehouse='{pWHouse}' and  SnCode ='{pSnCOde}'";
                SQLiteCommand cmds = new SQLiteCommand(query, conn);
                SQLiteDataReader reader = cmds.ExecuteReader();

                while (reader.Read())
                {
                    _globalData.FlowMeter[0].FlowMeterNo = reader["FlowMeterNo"].ToString();
                    _globalData.FlowMeter[0].MeterFakor = int.Parse(reader["MeterFakor"].ToString());
                    _globalData.FlowMeter[0].SnCode = reader["SnCode"].ToString();
                    _globalData.FlowMeter[0].WhouseLocation = reader["WhouseLocation"].ToString();
                    _globalData.FlowMeter[0].Warehouse = reader["Warehouse"].ToString();
                }


                reader.Dispose();
                if (conn.State == ConnectionState.Open) conn.Close();
            }
            catch (Exception ex)
            {
                //save_error_log(ex.ToString(), "initWhSetting()");
                //MessageBox.Show(ex.ToString());
                MessageBox.Show("FWarehouseTransfer7Inch.cs Exception: Terjadi kesalahan saat initialisasi warehouse data, mohon cek log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", "FWarehouseTransfer7Inch.cs Exception initWhSetting : " + ex.Message);
                save_error_log(ex.Message, "initWhSetting()");
            }
        }

        void save_error_log(string data, string actions)
        {
            try
            {
                DateTime localDate = DateTime.Now;
                var _item = new tbl_log();
                var initData = GlobalModel.GlobalVar;

                _item.pid = System.Guid.NewGuid().ToString();
                _item.actions = $"District {initData.DataEmp.DstrctCode}/FWhouseTransfer/{actions}";
                _item.message = $"{data}";
                _item.mod_date = localDate;
                _item.mod_by = initData.DataEmp.Nrp;

                SQLiteConnection conn;

                conn = new SQLiteConnection(myConnectionString);
                if (conn.State != ConnectionState.Open) conn.Open();
                string query = string.Empty;
                query = "INSERT INTO tbl_log(pid,actions,message,mod_date,mod_by)"
                      + " VALUES(@pid,@actions,@message,@mod_date,@mod_by)";

                SQLiteCommand cmd = new SQLiteCommand(query, conn);

                cmd.Parameters.Add(new SQLiteParameter("@pid", _item.pid));
                cmd.Parameters.Add(new SQLiteParameter("@actions", _item.actions));
                cmd.Parameters.Add(new SQLiteParameter("@message", _item.message));
                cmd.Parameters.Add(new SQLiteParameter("@mod_date", _item.mod_date));
                cmd.Parameters.Add(new SQLiteParameter("@mod_by", _item.mod_by));
                cmd.ExecuteNonQuery();
                if (conn.State == ConnectionState.Open) conn.Close();
            }
            catch (Exception)
            {
            }
        }

        void fillData()
        {
            var _globalData = GlobalModel.GlobalVar;
            WhTranfer whTranfer = new WhTranfer();

            string startHour = "";
            string endHour = "";

            String _shift = String.Empty;
            //String _timezone = "WIB";
            String _timezone = "WITA";

            SQLiteConnection conn;

            conn = new SQLiteConnection(myConnectionString);
            if (conn.State != ConnectionState.Open) conn.Open();
            string query = string.Empty;

            query = "SELECT * FROM tbl_setting";
            SQLiteCommand cmds = new SQLiteCommand(query, conn);
            SQLiteDataReader reader = cmds.ExecuteReader();

            while (reader.Read())
            {
                pShiftStart1 = reader["StartShift1"].ToString();
                pShiftStart2 = reader["StartShift2"].ToString();
            }

            startHour = pShiftStart1;
            endHour = pShiftStart2;
            try
            {
                startHour = startHour.Split(':')[0];
                endHour = endHour.Split(':')[0];
            }
            catch (Exception)
            {
            }

            reader.Dispose();
            if (conn.State == ConnectionState.Open) conn.Close();

            pShiftStart1 = startHour;
            pShiftStart2 = endHour;

            if (DateTime.Now.Hour >= int.Parse(pShiftStart1) && DateTime.Now.Hour < int.Parse(pShiftStart2))
                _shift = "1";
            else
                _shift = "2";

            var _date = DateTime.Now;

            if (DateTime.Now.Hour <= int.Parse(pShiftStart1))
                _date = _date.AddDays(-1);

            whTranfer.job_row_id = System.Guid.NewGuid().ToString();
            whTranfer.xfer_code = $"X{_globalData.DataEmp.DstrctCode}-{DateTime.Now.ToString("yyyy-MMddHHmmss")}";
            whTranfer.xfer_date = _date.ToString("yyyyMMdd");
            whTranfer.dstrct_code = _globalData.DataEmp.DstrctCode;
            whTranfer.employee_id = _globalData.DataEmp.Nrp;
            whTranfer.issued_whouse = pWHouse;
            whTranfer.fmeter_serial_no = pSnCOde;
            whTranfer.issued_whouse_type = "Manual";
            whTranfer.received_whouse = listWHPenerima.SelectedValue.ToString();
            //whTranfer.received_whouse = txtBxWHPenerima.Text;
            whTranfer.received_whouse_type = "Manual";
            whTranfer.stock_code = "000005517";
            whTranfer.qty_xfer = 1000;
            whTranfer.shift = _shift;
            whTranfer.faktor_meter = 1;
            whTranfer.fmeter_begin = 0;
            whTranfer.fmeter_end = 0;
            whTranfer.xfer_by = $"{_globalData.DataEmp.Nrp}-{_globalData.DataEmp.Name}";
            //whTranfer.received_by = cmbFuelman.SelectedValue.ToString();
            whTranfer.received_by = listFuelman.SelectedValue.ToString();
            //whTranfer.received_by = txtBxFuelman.Text;
            whTranfer.mod_date = DateTime.Now;
            whTranfer.created_date = DateTime.Now;
            whTranfer.created_by = _globalData.DataEmp.Nrp + "_LCR";
            whTranfer.timezone = _timezone;
            whTranfer.is_submit = 2;
            whTranfer.syncs = 2;
            GlobalModel.whTransfer = whTranfer;

            //saveData(whTranfer);
        }

        private void FWhouseTransfer_Activated(object sender, EventArgs e)
        {
            this.WindowState = System.Windows.Forms.FormWindowState.Normal;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            txtQtyOrder.Text = txtQtyOrder.Text + "1";
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            txtQtyOrder.Text = txtQtyOrder.Text + "2";
        }

        private void btn3_Click(object sender, EventArgs e)
        {
            txtQtyOrder.Text = txtQtyOrder.Text + "3";
        }

        private void btn4_Click(object sender, EventArgs e)
        {
            txtQtyOrder.Text = txtQtyOrder.Text + "4";
        }

        private void btn5_Click(object sender, EventArgs e)
        {
            txtQtyOrder.Text = txtQtyOrder.Text + "5";
        }

        private void btn6_Click(object sender, EventArgs e)
        {
            txtQtyOrder.Text = txtQtyOrder.Text + "6";
        }

        private void btn7_Click(object sender, EventArgs e)
        {
            txtQtyOrder.Text = txtQtyOrder.Text + "7";
        }

        private void btn8_Click(object sender, EventArgs e)
        {
            txtQtyOrder.Text = txtQtyOrder.Text + "8";
        }

        private void btn9_Click(object sender, EventArgs e)
        {
            txtQtyOrder.Text = txtQtyOrder.Text + "9";
        }

        private void btn0_Click(object sender, EventArgs e)
        {
            txtQtyOrder.Text = txtQtyOrder.Text + "0";
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtQtyOrder.Text = "";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void cmbWhPenerima_Click(object sender, EventArgs e)
        {
            try
            {
                ProcessStartInfo psStartInfo = new ProcessStartInfo();
                //psStartInfo.FileName = @"C:\Program Files\Common Files\microsoft shared\ink\TabTip.exe";
                var dir = Path.Combine(Environment.CurrentDirectory, "VKeyboard", "VKeyboard.exe");
                psStartInfo.FileName = dir;

                Process ps = Process.Start(psStartInfo);

            }
            catch (Exception ex)
            {
                var error = ex.Message;
            }
        }

        private void cmbFuelman_Click(object sender, EventArgs e)
        {
            try
            {
                ProcessStartInfo psStartInfo = new ProcessStartInfo();
                //psStartInfo.FileName = @"C:\Program Files\Common Files\microsoft shared\ink\TabTip.exe";
                var dir = Path.Combine(Environment.CurrentDirectory, "VKeyboard", "VKeyboard.exe");
                psStartInfo.FileName = dir;

                Process ps = Process.Start(psStartInfo);

            }
            catch (Exception ex)
            {
                var error = ex.Message;
            }
        }

        void hideResults()
        {
            listWHPenerima.Visible = false;
        }
        void showResults()
        {
            listWHPenerima.Visible = true;
            int heigth = listWHPenerima.Items.Count * 10;
            if (heigth < 50)
            {
                heigth = 100;
            }


            listWHPenerima.Size = new Size(235, heigth);

        }
        void hideResultsFuelMan()
        {
            listFuelman.Visible = false;
        }
        void showResultFuelman()
        {
            listFuelman.Visible = true;
            int heigth2 = listFuelman.Items.Count * 10;
            if (heigth2 < 50)
            {
                heigth2 = 100;
            }

            listFuelman.Size = new Size(235, heigth2);
        }
        private void txtBxWHPenerima_TextChanged(object sender, EventArgs e)
        {

            if (txtBxWHPenerima.Text.Length == 0)
            {
                loadFlowMeter();
                hideResults();
            }
            else
            {
                loadFlowMeter();
                var itemsFound = listWHPenerima.Items.Cast<ListItem>().Where(f => f.text.Contains(txtBxWHPenerima.Text)).ToList();
                listWHPenerima.DataSource = itemsFound;
                listWHPenerima.ValueMember = "value";
                listWHPenerima.DisplayMember = "text";
                showResults();
            }
        }

        private void txtBxWHPenerima_Click(object sender, EventArgs e)
        {
            try
            {
                ProcessStartInfo psStartInfo = new ProcessStartInfo();
                //psStartInfo.FileName = @"C:\Program Files\Common Files\microsoft shared\ink\TabTip.exe";
                var dir = Path.Combine(Environment.CurrentDirectory, "VKeyboard", "VKeyboard.exe");
                psStartInfo.FileName = dir;

                Process ps = Process.Start(psStartInfo);

            }
            catch (Exception ex)
            {
                var error = ex.Message;
            }
            showResults();
            hideResultsFuelMan();
        }
        private void txtBxWHPenerima_Leave(object sender, EventArgs e)
        {
            hideResults();
        }

        public void showKeyboard()
        {
            try
            {
                ProcessStartInfo psStartInfo = new ProcessStartInfo();
                //psStartInfo.FileName = @"C:\Program Files\Common Files\microsoft shared\ink\TabTip.exe";
                var dir = Path.Combine(Environment.CurrentDirectory, "VKeyboard", "VKeyboard.exe");
                psStartInfo.FileName = dir;

                Process ps = Process.Start(psStartInfo);

            }
            catch (Exception ex)
            {
                var error = ex.Message;
            }
        }

        private void listWHPenerima_Click(object sender, EventArgs e)
        {
            txtBxWHPenerima.Text = ((ListItem)listWHPenerima.SelectedItem).text;
            var cek = listWHPenerima.SelectedValue.ToString();
            hideResults();
        }

        private void listFuelman_Click(object sender, EventArgs e)
        {
            txtBxFuelman.Text = ((ListItem)listFuelman.SelectedItem).text;
            var cek = listFuelman.SelectedValue.ToString();
            hideResultsFuelMan();
        }

        private void txtBxFuelman_TextChanged(object sender, EventArgs e)
        {
            if (txtBxFuelman.Text.Length == 0)
            {
                loadFuelman();
                hideResultsFuelMan();
            }
            else
            {
                loadFuelman();
                var itemsFound = listFuelman.Items.Cast<ListItem>().Where(f => f.text.Contains(txtBxFuelman.Text)).ToList();
                listFuelman.DataSource = itemsFound;
                listFuelman.ValueMember = "value";
                listFuelman.DisplayMember = "text";
                showResultFuelman();
            }
        }

        private void txtBxFuelman_Click(object sender, EventArgs e)
        {
            try
            {
                ProcessStartInfo psStartInfo = new ProcessStartInfo();
                //psStartInfo.FileName = @"C:\Program Files\Common Files\microsoft shared\ink\TabTip.exe";
                var dir = Path.Combine(Environment.CurrentDirectory, "VKeyboard", "VKeyboard.exe");
                psStartInfo.FileName = dir;

                Process ps = Process.Start(psStartInfo);

            }
            catch (Exception ex)
            {
                var error = ex.Message;
            }
            showResultFuelman();
            hideResults();
        }

        private void txtBxFuelman_Leave(object sender, EventArgs e)
        {
            hideResultsFuelMan();
        }
    }
}
