﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LCR600SDK
{
    public class ListItem
    {
        public ListItem(String sValue, String sText)
        {
            this.value = sValue;
            this.text = sText;
        }

        public String value { get; set; }
        public String text { get; set; }
    }
}
