﻿using Flurl.Http;
using MySql.Data.MySqlClient; using System.Data.SQLite; using System.IO; using System.Reflection;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LCR600SDK
{
    public partial class FListDataReceiveDirectOwner : Form
    {
        private string myConnectionString = string.Empty;
        private string pUrl_service = string.Empty;
        private int idxCell = -1;
        private string pid_receive = "";
        private string valueSirNo = "";
        private string valueSirNoEdited = "";
        private string valueSirNoBefore = "";
        private bool statusOnline = false;
        PasswordEncrypDecrypt decryptor = new PasswordEncrypDecrypt();
        WriteLogText log = new WriteLogText();
        public FListDataReceiveDirectOwner()
        {
            InitializeComponent();
        }

        private void FListDataReceiveDirect_Load(object sender, EventArgs e)
        {
            dateTimePicker1.Value = DateTime.Now;
            dateTimePicker2.Value = DateTime.Now;
            var _proper = Properties.Settings.Default;
            pUrl_service = _proper.url_service;
            statusOnline = _proper.connection == "ONLINE" ? true : false;
            dateTimePicker1.Text = DateTime.Now.ToString("yyyy-MM-dd");
            dateTimePicker2.Text = DateTime.Now.ToString("yyyy-MM-dd");
            //myConnectionString = "Data Source=" + Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "SQLite", "db_iflash.db") + " ;New=False;";
            var passPhrase = "iFlashDesktop";
            var decryp = decryptor.DecryptPassword(_proper.db_pass, passPhrase);
            myConnectionString = "Data Source=" + Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "SQLite", "db_iflash.db") + " ;New=False;";
            loadGridSearch(true, "");
            loadGrid();
            
        }
        void dataGridView1_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            dataGridView1.BorderStyle = BorderStyle.None;
            dataGridView1.AlternatingRowsDefaultCellStyle.BackColor = Color.FromArgb(238, 239, 249);
            dataGridView1.CellBorderStyle = DataGridViewCellBorderStyle.SingleHorizontal;
            dataGridView1.DefaultCellStyle.SelectionBackColor = Color.SeaGreen;
            dataGridView1.DefaultCellStyle.SelectionForeColor = Color.WhiteSmoke;
            dataGridView1.BackgroundColor = Color.FromArgb(0, 101, 179);
            dataGridView1.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridView1.EnableHeadersVisualStyles = false;
            dataGridView1.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
            dataGridView1.ColumnHeadersDefaultCellStyle.Font = new Font("MS Reference Sans Serif", 20);
            dataGridView1.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(0, 101, 179);
            dataGridView1.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
            if (dataGridView1.Columns.Count > 0)
            {
                dataGridView1.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                dataGridView1.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                dataGridView1.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                dataGridView1.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                dataGridView1.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                dataGridView1.Columns[5].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                dataGridView1.Columns[6].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            }
            
        }
        void loadGridSearch(bool isAll, String sKey)
        {
            try
            {
                String iQuery = querySql();

                SQLiteConnection conn = new SQLiteConnection(myConnectionString);
                SQLiteCommand cmd = new SQLiteCommand(iQuery, conn);

                if (conn.State != ConnectionState.Open) conn.Open();

                DataTable dataTable = new DataTable();
                SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);

                da.Fill(dataTable);

                dataGridView1.DataSource = dataTable;
                dataGridView1.Columns["pid_receive"].Visible = false;
                if (conn.State == ConnectionState.Open)
                    if (conn.State == ConnectionState.Open) conn.Close();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
                MessageBox.Show("SQLiteException: Terjadi kesalahan pada saat search data, mohon periksa log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(ex.Message, "loadGridSearch()");
            }
        }

        public void loadGrid()
        {
            SQLiteConnection conn = new SQLiteConnection(myConnectionString);
            SQLiteCommand cmd = new SQLiteCommand(querySql(), conn);

            if (conn.State != ConnectionState.Open) conn.Open();

            DataTable dataTable = new DataTable();
            SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);

            da.Fill(dataTable);

            dataGridView1.DataSource = dataTable;
            dataGridView1.Columns["pid_receive"].Visible = false;
            if (conn.State == ConnectionState.Open)
                if (conn.State == ConnectionState.Open) conn.Close();
        }

        void btnSyncWait()
        {
            btnSync.Enabled = false;
            btnSync.Visible = false;
            btnPleaseWait.Enabled = false;
            btnPleaseWait.Visible = true;
        }
        void btnSyncEnabled()
        {
            btnSync.Enabled = true;
            btnSync.Visible = true;
            btnPleaseWait.Visible = false;
            btnPleaseWait.Enabled = false;
        }

        private void timerSyncIssuing_Tick_1(object sender, EventArgs e)
        {
            btnSyncWait();
        }
        public async Task<string> checkConnectivity()
        {
            try
            {
                WebClient requestCek = new WebClient();
                var str = requestCek.DownloadString(pUrl_service);
                return str;
            }
            catch (WebException ex)
            {
                return "";
            }
        }
        private async void btnSync_Click(object sender, EventArgs e)
        {
            try
            {
                var checkconnect = await checkConnectivity();
                if (checkconnect.Contains("Supply Service"))
                //if (statusOnline)
                {
                    onSyncAsync();                    
                }
                else
                {
                    var f = new FPopUpNotification("Status saat ini OFFLINE mohon untuk sambungkan jaringan dan ganti setting connection menjadi ONLINE \r\n Setting > User Setting > Connection > Save", "Failed To Connect");
                    f.ShowDialog();
                }

            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message.ToString());
                MessageBox.Show("Exception: Terjadi kesalahan pada saat sync, mohon periksa log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", ex.Message);                
                save_error_log(ex.Message, "btnSync_Click()");
                timerSyncIssuing.Stop();
                btnSyncEnabled();
            }
        }

        async Task onSyncAsync()
        {
            var _proper = Properties.Settings.Default;
            var _auth = GlobalModel.loginModel;

            String _url = $"{pUrl_service}api/FuelService/SyncReceiveDirectDesktop";            
            try
            {
                List<tbl_t_receive_direct> SendListIssuing = new List<tbl_t_receive_direct>();
                var getData = getListReceiveDirectOwner();
              

                var listFailedPidReceive = new List<List<string>>();
                if (getData.Count() == 0)
                {
                    timerSyncIssuing.Stop();
                    btnSyncEnabled();
                }
                else
                {                    
                    btnPleaseWait.Text = string.Format("Please Wait. {0} data dari {1} data", 0, getData.Count);
                    for (int i = 0; i < getData.Count; i++)
                    {

                        SendListIssuing.Add(getData[i]);
                        
                        //if (SendListIssuing.Count == 1 /*&& int.Parse(counter[0]) != 0*/)
                        if (SendListIssuing.Count == int.Parse(_proper.data_per_kirim) /*&& int.Parse(counter[0]) != 0*/)
                        {
                            var json = Newtonsoft.Json.JsonConvert.SerializeObject(SendListIssuing);
                            ClsReceiveDirect data = await _url.WithHeaders(new { Authorization = _auth.token }).PostJsonAsync(SendListIssuing).ReceiveJson<ClsReceiveDirect>();
                            if (data.status)
                            {
                                btnPleaseWait.Text = string.Format("Please Wait. {0} data dari {1} data", (i + 1), getData.Count);
                            }
                            else
                            {
                                if(data.failed_pid != null && data.failed_pid.Count > 0)
                                {
                                    listFailedPidReceive.Add(data.failed_pid);
                                    SendListIssuing = new List<tbl_t_receive_direct>();
                                }
                            }
                            //if (data.failed_pid.Count > 0)
                            //{
                                
                            //}
                            //else
                            //{
                            //    btnPleaseWait.Text = string.Format("Please Wait. {0} data dari {1} data", (i+1), getData.Count);
                            //}
                            save_error_log(data.remarks, "onSyncAsync()");
                            updateTableSync(SendListIssuing, data);
                            loadGrid();
                            SendListIssuing = new List<tbl_t_receive_direct>();
                        }
                        else 
                        {
                            if (i == getData.Count - 1 /*&& SendListIssuing.Count == int.Parse(counter[1])*/)
                            {
                                ClsReceiveDirect data = await _url.WithHeaders(new { Authorization = _auth.token }).PostJsonAsync(SendListIssuing).ReceiveJson<ClsReceiveDirect>();
                                if (data.status)
                                {
                                    btnPleaseWait.Text = string.Format("Please Wait. {0} data dari {1} data", (i + 1), getData.Count);
                                }
                                else
                                {
                                    if (data.failed_pid!=null && data.failed_pid.Count > 0)
                                    {
                                        listFailedPidReceive.Add(data.failed_pid);
                                        String failed_pids = String.Join(" \r\n- ", listFailedPidReceive);
                                        btnPleaseWait.Text = string.Format("Please Wait. {0} data dari {1} data", (i + 1), getData.Count);
                                    }
                                }
                                //else
                                //{
                                //    btnPleaseWait.Text = string.Format("Please Wait. {0} data dari {1} data", (i + 1), getData.Count);
                                //}
                                save_error_log(data.remarks, "onSyncAsync()");
                                updateTableSync(SendListIssuing, data);
                                loadGrid();
                            }                              
                        }
                        if (i == getData.Count - 1)
                        {
                            timerSyncIssuing.Stop();
                            btnSyncEnabled();
                        }
                    }
                }
            }
            catch (FlurlHttpException ex)
            {
                var resp = ex.Message;
                if (ex.InnerException != null)
                {
                    resp = ex.InnerException.Message;
                }
                //var resp = await ex.GetResponseStringAsync();
                MessageBox.Show("Terjadi kesalahan saat sync, periksa log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(resp, "onSyncAsync()");
                timerSyncIssuing.Stop();
                btnSyncEnabled();
            }
            catch (Exception ex)
            {                
                //MessageBox.Show(ex.Message);
                MessageBox.Show("Exception: Terjadi kesalahan pada saat sync, mohon periksa log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", ex.Message);                
                save_error_log(ex.Message, "onSyncAsync()");
                timerSyncIssuing.Stop();
                btnSyncEnabled();
            }



        }
        void save_error_log(string data, string actions)
        {
            try
            {
                DateTime localDate = DateTime.Now;
                var _item = new tbl_log();
                var initData = GlobalModel.GlobalVar;

                _item.pid = System.Guid.NewGuid().ToString();
                _item.actions = $"District {initData.DataEmp.DstrctCode}/FListDataReceiveDirect/{actions}";
                _item.message = $"{data}";
                _item.mod_date = localDate;
                _item.mod_by = initData.DataEmp.Nrp;

                SQLiteConnection conn;

                conn = new SQLiteConnection(myConnectionString);
                if (conn.State != ConnectionState.Open) conn.Open();
                string query = string.Empty;
                query = "INSERT INTO tbl_log(pid,actions,message,mod_date,mod_by)"
                      + " VALUES(@pid,@actions,@message,@mod_date,@mod_by)";

                SQLiteCommand cmd = new SQLiteCommand(query, conn);

                cmd.Parameters.Add(new SQLiteParameter("@pid", _item.pid));
                cmd.Parameters.Add(new SQLiteParameter("@actions", _item.actions));
                cmd.Parameters.Add(new SQLiteParameter("@message", _item.message));
                cmd.Parameters.Add(new SQLiteParameter("@mod_date", _item.mod_date));
                cmd.Parameters.Add(new SQLiteParameter("@mod_by", _item.mod_by));
                cmd.ExecuteNonQuery();
                if (conn.State == ConnectionState.Open) conn.Close();
            }
            catch (Exception)
            {
            }
        }

        void updateTableSync(List<tbl_t_receive_direct> SendListIssuing, ClsReceiveDirect sClsIssuing)
        {
            SQLiteConnection conn;
            int recs = 0;
            string query = string.Empty;
            conn = new SQLiteConnection(myConnectionString);
            if (conn.State != ConnectionState.Open) conn.Open();

            String failed_pids = String.Join("','", sClsIssuing.failed_pid);
            string send_pid = String.Join("','", SendListIssuing.Select(x => x.pid_receive));

            if (sClsIssuing.failed_pid.Count() > 0)
            {
                var success_pid = send_pid.Replace(failed_pids, "");
                query = $"update tbl_t_receive_direct_owner set syncs = 9 where pid_receive in('{failed_pids}') and syncs=3 AND receive_date between '" + dateTimePicker1.Value.ToString("yyyy-MM-dd 00:00:00") + "' and '" + dateTimePicker2.Value.ToString("yyyy-MM-dd 23:59:59") + "'";
                recs = execCmd(query, conn);                                                                                                                                                                       
                                                                                                                                                                                                                   
                query = $"update tbl_t_receive_direct_owner set syncs = 7 where pid_receive in('{success_pid}') and syncs=3 AND receive_date between '" + dateTimePicker1.Value.ToString("yyyy-MM-dd 00:00:00") + "' and '" + dateTimePicker2.Value.ToString("yyyy-MM-dd 23:59:59") + "''";
                recs = execCmd(query, conn);
            }
            else
            {
                query = $"update tbl_t_receive_direct set syncs = 7 where pid_receive in('{send_pid}') and syncs=3 AND receive_date between '" + dateTimePicker1.Value.ToString("yyyy-MM-dd 00:00:00") + "' and '" + dateTimePicker2.Value.ToString("yyyy-MM-dd 23:59:59") + "'";
                recs = execCmd(query, conn);
            }
            if (conn.State == ConnectionState.Open) conn.Close();

            if (Application.OpenForms["FMain"] != null)
            {
                (Application.OpenForms["FMain"] as FMain).toolStripStatusLabelStatus.Text = "Sync finish";
            }
        }

        int execCmd(String sQuery, SQLiteConnection sConn)
        {
            try
            {
                int iReturn = 0;

                var cmd = new SQLiteCommand(sQuery, sConn);
                iReturn = cmd.ExecuteNonQuery();

                return iReturn;
            }
            catch (Exception e)
            {
                return 0;
            }

        }

        List<tbl_t_receive_direct> getListReceiveDirectOwner()
        {
            List<tbl_t_receive_direct> _list = new List<tbl_t_receive_direct>();
            SQLiteConnection conn;

            conn = new SQLiteConnection(myConnectionString);
            if (conn.State != ConnectionState.Open) conn.Open();
            string query = string.Empty;

            try
            {

                query = $"update tbl_t_receive_direct_owner set syncs = 3 where syncs in(2,9) AND receive_date between '" + dateTimePicker1.Value.ToString("yyyy-MM-dd 00:00:00") + "' and '" + dateTimePicker2.Value.ToString("yyyy-MM-dd 23:59:59") + "'";
                var recs = execCmd(query, conn);
                loadGrid();
                timerSyncIssuing.Start();
            }
            catch (SQLiteException ex)
            {
                //MessageBox.Show(ex.ToString());
                MessageBox.Show("Exception: Terjadi kesalahan pada saat get data receive direct owner local, mohon periksa log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(ex.Message, "getListReceiveDirect()");
            }

            //query = "select * from tbl_t_log_sheet_detail where syncs=3 AND issued_date between '" + dateTimePicker1.Value.ToString("yyyy-MM-dd 00:00:00") + "' and '" + dateTimePicker2.Value.ToString("yyyy-MM-dd 23:59:59") + "' and shift ='" + cbShift.Text + "'  ORDER BY ref_hour_start";
            query = querySqlSync();
            try
            {

                SQLiteCommand cmd = new SQLiteCommand(query, conn);
                SQLiteDataReader reader = cmd.ExecuteReader();



                while (reader.Read())
                {
                    var cls = new tbl_t_receive_direct();

                    cls.pid_receive = reader["pid_receive"].ToString();
                    cls.ref_id = reader["ref_id"].ToString();
                    cls.district = reader["district"].ToString();
                    cls.po_no = reader["po_no"].ToString();                    
                    cls.receive_at = reader["receive_at"].ToString();
                    cls.receive_at_code = reader["receive_at_code"].ToString();
                    cls.receive_qty = int.Parse(reader["receive_qty"].ToString());
                    cls.receive_sn_flow_meter1 = reader["receive_sn_flow_meter1"].ToString();
                    cls.receive_meter_faktor = reader["receive_meter_faktor"].ToString();
                    cls.receive_density = reader["receive_density"].ToString();
                    cls.receive_temperature = reader["receive_temperature"].ToString();
                    cls.receive_by = reader["receive_by"].ToString();
                    cls.receive_date = Convert.ToDateTime(reader["receive_date"].ToString());                    
                    cls.sir_no = reader["sir_no"].ToString();
                    cls.receive_qty_pama = int.Parse(reader["receive_qty_pama"].ToString());                    
                    cls.totaliser_awal = int.Parse(reader["totaliser_awal"].ToString());
                    cls.totaliser_akhir = int.Parse(reader["totaliser_akhir"].ToString());
                    cls.start_loading = reader["start_loading"].ToString();
                    cls.end_loading = reader["end_loading"].ToString();



                    _list.Add(cls);
                }
                reader.Dispose();
                conn.Close();
            }
            catch (SQLiteException ex)
            {
                conn.Close();
                //MessageBox.Show(ex.ToString());
                MessageBox.Show("Exception: Terjadi kesalahan pada saat get data receive direct local, mohon periksa log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(ex.Message, "getListReceiveDirect()");
            }
            if (conn.State == ConnectionState.Open) conn.Close();
            return _list;
        }
      
     
        
       


        void changeFilter()
        {
            DateTime fromdate = Convert.ToDateTime(dateTimePicker1.Text);
            DateTime todate = Convert.ToDateTime(dateTimePicker2.Text);

            if (fromdate <= todate)
            {
                TimeSpan ts = todate.Subtract(fromdate);
                int days = Convert.ToInt16(ts.Days);

                try
                {
                    String iQuery = querySql();// "SELECT issued_date,ref_hour_start, unit_no, hm, whouse_id, qty, shift , sync_desc, job_row_id FROM vw_logsheet where issued_date between '" + dateTimePicker1.Value.ToString("yyyy-MM-dd 00:00:00") + "' and '" + dateTimePicker2.Value.ToString("yyyy-MM-dd 23:59:59") + "' order by issued_date,ref_hour_start";
                    SQLiteConnection conn = new SQLiteConnection(myConnectionString);
                    SQLiteCommand cmd = new SQLiteCommand(iQuery, conn);

                    if (conn.State != ConnectionState.Open) conn.Open();
                    DataTable dataTable = new DataTable();

                    dataTable.Load(cmd.ExecuteReader());
                    dataGridView1.DataSource = dataTable;                    
                    if (conn.State == ConnectionState.Open) conn.Close();
                }
                catch (Exception)
                {

                    throw;
                }
            }
        }

        string querySql()
        {            
            var sql = " SELECT sync_desc,pid_receive,receive_date,po_no, receive_at, sir_no,start_loading, receive_qty,totaliser_awal,totaliser_akhir ";
            sql += " FROM vw_receive_direct_owner ";
            sql += " WHERE receive_date between  '" + dateTimePicker1.Value.ToString("yyyy-MM-dd  00:00:00") + "' and '" + dateTimePicker2.Value.ToString("yyyy-MM-dd 23:59:59") +"'";
            if (!string.IsNullOrEmpty(txtSearch.Text)) 
            {
                sql += " and po_no like '%" + txtSearch.Text + "%' ";
            }
            sql += " ORDER BY case when syncs = 2 then 1 ";
            sql += " when syncs = 9 then 2 ";
            sql += " when syncs = 3 then 3 ";
            sql += " when syncs = 7 then 4 ";
            sql += " END ASC ";
            sql += ", receive_date ASC ,start_loading ASC ";
            return sql;
        }
        string querySqlSync()
        {            
            var sql = " SELECT * ";
            sql += " FROM tbl_t_receive_direct_owner ";
            sql += " WHERE syncs = 3 and receive_date between  '" + dateTimePicker1.Value.ToString("yyyy-MM-dd  00:00:00") + "' and '" + dateTimePicker2.Value.ToString("yyyy-MM-dd 23:59:59") +  "' ";
            if (!string.IsNullOrEmpty(txtSearch.Text))
            {
                sql += " and po_no like '%" + txtSearch.Text + "%' ";
            }
            sql += " ORDER BY case when syncs = 2 then 1 ";
            sql += " when syncs = 9 then 2 ";
            sql += " when syncs = 3 then 3 ";
            sql += " when syncs = 7 then 4 ";
            sql += " END ASC ";
            sql += ", receive_date ASC ,start_loading ASC ";
            return sql;
        }

        private void dateTimePicker2_CloseUp(object sender, EventArgs e)
        {
            changeFilter();
        }

        private void dateTimePicker1_CloseUp(object sender, EventArgs e)
        {
            changeFilter();
        }

        private void cbShift_SelectedIndexChanged(object sender, EventArgs e)
        {
            changeFilter();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void cbStatusIssuing_SelectedIndexChanged(object sender, EventArgs e)
        {
            changeFilter();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            changeFilter();
        }
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (e.ColumnIndex == 5)
                {
                    idxCell = e.RowIndex;
                    var idxCOllumn = e.ColumnIndex;
                    var columnidx = dataGridView1.Rows[idxCell].Cells["sir_no"].ColumnIndex;

                    string value = dataGridView1.Rows[idxCell].Cells[columnidx].FormattedValue.ToString();
                    string pidreceive = dataGridView1.Rows[idxCell].Cells["pid_receive"].FormattedValue.ToString();

                    if (valueSirNoEdited != valueSirNoBefore && pid_receive != pidreceive)
                    {
                        updateSirNo();
                        loadGridSearch(false, dateTimePicker2.Text);
                    }
                    valueSirNoBefore = value;
                    pid_receive = pidreceive;
                    if (idxCOllumn == columnidx)
                    {
                        DataGridViewCell cell = dataGridView1.Rows[idxCell].Cells[columnidx];
                        dataGridView1.CurrentCell = cell;
                        dataGridView1.BeginEdit(true);
                    }
                }
                else
                {
                    valueSirNoEdited = dataGridView1.Rows[idxCell].Cells["sir_no"].FormattedValue.ToString();
                    if (valueSirNoEdited != valueSirNoBefore)
                    {
                        updateSirNo();
                        valueSirNoBefore = valueSirNoEdited;
                        loadGridSearch(false, dateTimePicker2.Text);

                    }

                }
            }
        }

        private void dataGridView1_KeyUp(object sender, KeyEventArgs eKey)
        {
            if (eKey.KeyCode == Keys.Enter)
            {
                if (dataGridView1.CurrentRow.Index >= 0)
                {
                    idxCell = dataGridView1.CurrentRow.Index;
                    var idxCOllumn = dataGridView1.CurrentCell.ColumnIndex;
                    var columnidx = dataGridView1.Rows[idxCell].Cells["sir_no"].ColumnIndex;
                    var cek = dataGridView1;
                    if (idxCell > 0)
                    {
                        valueSirNoEdited = dataGridView1.Rows[idxCell - 1].Cells["sir_no"].FormattedValue.ToString();
                        pid_receive = dataGridView1.Rows[idxCell - 1].Cells["pid_receive"].FormattedValue.ToString();

                    }
                    if (idxCOllumn == columnidx)
                    {
                        if (valueSirNoBefore != valueSirNoEdited)
                        {
                            updateSirNo();
                        }

                        loadGridSearch(false, dateTimePicker2.Text);
                        DataGridViewCell cell = dataGridView1.Rows[idxCell].Cells["sir_no"];
                        dataGridView1.CurrentCell = cell;
                        dataGridView1.BeginEdit(true);
                    }

                }
            }
        }

        private void dataGridView1_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (e.RowIndex >= 0)
            {

                idxCell = dataGridView1.CurrentRow.Index;
                var idxCOllumn = dataGridView1.CurrentCell.ColumnIndex;
                var columnidx = dataGridView1.Rows[idxCell].Cells["sir_no"].ColumnIndex;
                DataGridViewCell cell = dataGridView1.Rows[idxCell].Cells["sir_no"];
                dataGridView1.CurrentCell = cell;
                valueSirNoBefore = dataGridView1.Rows[idxCell].Cells["sir_no"].FormattedValue.ToString();
                pid_receive = dataGridView1.Rows[idxCell].Cells["pid_receive"].FormattedValue.ToString();
            }
        }

        private void dataGridView1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {

                idxCell = dataGridView1.CurrentRow.Index;
                var idxCOllumn = dataGridView1.CurrentCell.ColumnIndex;
                var columnidx = dataGridView1.Rows[idxCell].Cells["sir_no"].ColumnIndex;
                DataGridViewCell cell = dataGridView1.Rows[idxCell].Cells["sir_no"];
                dataGridView1.CurrentCell = cell;
                valueSirNoEdited = dataGridView1.Rows[idxCell].Cells["sir_no"].FormattedValue.ToString();
                pid_receive = dataGridView1.Rows[idxCell].Cells["pid_receive"].FormattedValue.ToString();
            }
        }

        void updateSirNo()
        {
            try
            {
                btnSyncWait();
                btnPleaseWait.Text = string.Format("Please Wait. Updating Do No");
                SQLiteConnection conn = new SQLiteConnection(myConnectionString);
                var querySql = "UPDATE tbl_t_receive_direct SET syncs = 2, sir_no = '" + valueSirNoEdited + "' WHERE pid_receive = '" + pid_receive.ToString() + "'";
                SQLiteCommand cmd = new SQLiteCommand(querySql, conn);
                //update tbl_t_log_sheet_detail set syncs = 3 where syncs
                if (conn.State != ConnectionState.Open) conn.Open();

                DataTable dataTable = new DataTable();
                SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);

                da.Fill(dataTable);

                dataGridView1.DataSource = dataTable;

                if (conn.State == ConnectionState.Open)
                    if (conn.State == ConnectionState.Open) conn.Close();
                btnSyncEnabled();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
                MessageBox.Show("Exception: Terjadi kesalahan pada saat update Sir No, mohon periksa log LCR600", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteToFile("logLCR600.txt", ex.Message);
                save_error_log(ex.Message, "updateDoNo()");

            }

        }
    }
}
