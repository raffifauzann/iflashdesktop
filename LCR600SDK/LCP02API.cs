﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace SDK
{
    class LCP02API
    {
        #region Constants

        // Miscellaneous LCR definitions.

        public const Int32 LCP02_PID_LCR = 2;                // LCP product ID for LCR

        public const int LCR_SERIAL_PORT = -1;               // IP port number if serial communications is being used

        public const Int32 LCR_MIN_SLAVE_ADDR = 1;           // minimum LCR node address
        public const Int32 LCR_MAX_SLAVE_ADDR = 250;         // maximum LCR node address

        public const Int32 LCR_NEW_DEVICE_ADDR = 250;        // new LCR device address

        public const uint LCRFeaturePOS      = (0x00000001); // the POS feature has been purchased
        public const uint LCRFeatureAviation = (0x00000002); // the Aviation feature has been purchased

        public const Int32 LCR_GET_TYPE         = 0;         // get field type parameter subfunction code
        public const Int32 LCR_GET_WIDTH        = 1;         // get field width parameter subfunction code
        public const Int32 LCR_GET_SECURITY     = 2;         // get field security byte parameter subfunction code
        public const Int32 LCR_NUM_PARAM_CODES  = 3;         // number of parameter subfunction codes

        public const Int32 POSNumProducts         = 100;     // number of POS products
        public const Int32 POSNumTaxes            = 16;      // number of taxes
        public const Int32 POSNumCashDiscounts    = 16;      // number of cash discounts
        public const Int32 POSNumVolumeDiscounts  = 16;      // number of volume discounts
        public const Int32 POSNumMiscCharges      = 16;      // number of miscellaneous charges

        public const Int32 POSNumTaxCategories    = 10;      // number of tax categories
        public const Int32 POSNumCashIntervals    = 3;       // number of cash intervals
        public const Int32 POSNumVolumeIntervals  = 3;       // number of volume intervals

        #endregion Constants

        #region Enumeration

        #region LCR Field Types

        public enum LCR_FIELD_TYPES
        {
            LCRT_TEXT = 0,                  // ASCIIZ string
            LCRT_INTEGER = 1,               // 2, byte signed integer
            LCRT_DATE = 2,                  // 9, byte ASCIIZ string
            LCRT_TIME = 3,                  // 9, byte ASCIIZ string
            LCRT_LONG = 4,                  // 4, byte signed long integer
            LCRT_VOLUME = 5,                // 4, byte signed long integer with implied decimal point
            LCRT_FFLOAT = 6,                // free format 4, byte floating point
            LCRT_LIST600 = 10,              // 1, byte unsigned char
            LCRT_LIST = 50,                 // 1, byte unsigned char
            LCRT_UFLOAT = 80,               // 4, byte unsigned floating point
            LCRT_SFLOAT = 90,               // 4, byte signed floating point
        }

        #endregion LCR Field Tyes

        #region LCR Text Field Lengths

        public enum LCR_FIELD_LENGTHS
        {
            LCRTl_DeliveryFinish        = 17,    // delivery finish date and time
            LCRTl_DeliveryStart         = 17,    // delivery start date and time
            LCRTl_FactoryKey            = 15,    // factory key
            LCRTl_Language              = 10,    // language revision
            LCRTl_LastCalibrated        = 17,    // date and time of last calibration
            LCRTl_MeterID               = 10,    // meter identifier
            LCRTl_ProductCode           = 5,     // product code
            LCRTl_ProductDescriptor     = 18,    // product descriptor
            LCRTl_SerialID              = 10,    // serial identifier
            LCRTl_ShiftStart            = 17,    // date and time of beginning of shift
            LCRTl_Software              = 10,    // software revision
            LCRTl_Ticket                = 10,    // ticket revision
            LCRTl_TicketHeaderLine      = 35,    // ticket header line
            LCRTl_UnitID                = 10,    // unit identifier
            LCRTl_UserKey               = 10,    // user key
            LCRTl_UserSetKey            = 10,    // key to set user key
            LCRTl_POSActivationKey      = 16,    // length of the POS activation key string
            LCRTl_POSCashDiscountName   = 20,    // length of the name of cash discounts
            LCRTl_POSHeader             = 13,    // length of header text for ticket printing
            LCRTl_POSMiscChargeName     = 8,     // length of the name of miscellaneous charges
            LCRTl_POSProductCode        = 5,     // length of the POS product code
            LCRTl_POSProductName        = 18,    // length of the POS product name
            LCRTl_POSTaxName            = 20,    // length of the name of taxes
            LCRTl_POSVolumeDiscountName = 20     // length of the name of volume discounts
         };

        #endregion LCR Text Field Lengths

        #region LCR List Type Groups

        public enum LCR_LIST_TYPE_GROUPS
        {
            LCRLg_PRODUCT_NUMBER        = 0,     // product number list
            LCRLg_PRESETS_ALLOWED       = 1,     // presets types allowed list
            LCRLg_PRODUCT_TYPE          = 2,     // product type list
            LCRLg_DATE_FORMAT           = 3,     // date format list
            LCRLg_UNIT_OF_MEASURE       = 4,     // unit of measure list
            LCRLg_YES_NO                = 5,     // yes/no list
            LCRLg_AUX_UNIT_OF_MEASURE   = 6,     // auxiliary unit of measure list
            LCRLg_PRESET_TYPE           = 7,     // preset type list
            LCRLg_HEADER_LINE_INDEX     = 8,     // header line index list
            LCRLg_PULSE_OUTPUT          = 9,     // pulse output list
            LCRLg_TEMPERATURE_UNIT      = 10,    // temperature unit list
            LCRLg_RATE_BASE             = 11,    // rate base list
            LCRLg_LINEARIZATION_POINT   = 12,    // linearization point list
            LCRLg_SECURITY              = 13,    // security locked/unlocked list
            LCRLg_DECIMALS              = 14,    // number of decimals list
            LCRLg_COMP_TYPE             = 15,    // compensation type list
            LCRLg_DIAGNOSTIC_MESSAGE    = 16,    // diagnostic messages list
            LCRLg_COMP_PARAMETER        = 17,    // compensation parameter list
            LCRLg_COMP_TEMPERATURE      = 18,    // compensation temperature list
            LCRLg_PRINTER               = 19,    // printer type list
            LCRLg_RESIDUAL              = 20,    // residual processing list
            LCRLg_FLOW_DIRECTION        = 21,    // flow direction list
            LCRLg_AUXILIARY_PORT_1      = 22,    // auxiliary port 1 option list
            LCRLg_AUXILIARY_PORT_2      = 23,    // auxiliary port 2 option list
            LCRLg_LCR_RESET             = 24,    // LCR reset option list
            LCRLg_LINEARIZATION_STATE   = 25,    // linearization state list
            LCRLg_PASSWORD_USAGE        = 26,    // password usage list
            LCRLg_CUSTOMER_USAGE        = 27,    // customer number usage list

            LCRLg_FS_PORT               = 50,    // File Server port selection list
            LCRLg_FS_BAUD               = 51,    // File Server baud rate selection list
            LCRLg_FS_TXENABLE           = 52,    // File Server tx enable bit selection list

            LCRLg_DELIVERY_SCREEN       = 53,    // delivery screen list

            LCRLg_LOCK_DISCOUNTS        = 60,    // lock discounts list
            LCRLg_LOCK_PRICE            = 61,    // lock price list
            LCRLg_SAVE_NEW_PRICE        = 62,    // save new price list
            LCRLg_ALLOW_NEW_DLVY_PRICE  = 63,    // allow new delivery prices list
   
            LCRLg_PRODUCT_CALIBRATION   = 64,    // product calibration list
            LCRLg_PRODUCT_TAX_NUM       = 65,    // product tax number list
            LCRLg_PRODUCT_CASH_DISC     = 66,    // product cash discount list
            LCRLg_PRODUCT_VOLUME_DISC   = 67,    // product volume discount list

            LCRLg_POS_TAX_NUM           = 68,    // POS tax number list
            LCRLg_POS_TAX_CATEGORY      = 69,    // POS tax category list
            LCRLg_POS_TAX_TYPE          = 70,    // POS tax type list

            LCRLg_POS_CASH_DISC_NUM     = 71,    // POS cash discount number list
            LCRLg_POS_CASH_DISC_INT     = 72,    // POS cash discount interval list
            LCRLg_APPLY_CASH_DISC       = 73,    // apply cash discount list

            LCRLg_POS_VOLUME_DISC_NUM   = 74,    // POS volume discount number list
            LCRLg_POS_VOLUME_DISC_INT   = 75,    // POS volume discount interval list
            LCRLg_APPLY_VOLUME_DISC     = 76,    // apply volume discount list

            LCRLg_POS_MISC_CHARGE_NUM   = 77,    // POS miscellaneous charge number list
            LCRLg_POS_MISC_CH_TAX_NUM   = 78     // POS miscellaneous charge tax number list
        };

        #endregion LCR List Type Groups

        #region LCR POS Tax Types

        public enum LCR_POS_TAX_TYPES : ulong 
        {
            LCRL_POSTaxTypeUnused    = 0x00000000UL,
            LCRL_POSTaxTypePercent   = 0x00000001UL,
            LCRL_POSTaxTypePerUnit   = 0x00000002UL,
            LCRL_POSTaxTypeTaxOnTax  = 0xFFFFFFFCUL
        };

        #endregion LCR POS Tax Types

        #region Delivery Screens

        public enum LCR_DELIVERY_SCREEN
        {
            LCRL_ScreenUser                 = 0,
            LCRL_ScreenPumpAndPrint         = 1,
            LCRL_ScreenPreset               = 2,
            LCRL_ScreenAviation             = 3,
            LCRL_ScreenPOS                  = 4,

            LCR_NUM_DeliveryScreens         = 5
        };

        #endregion Delivery Screens

        #region Miscellaneous Charge Number

        public enum LCR_MISC_CHARGE_NUMBER
        {
            LCRL_POSMiscCharge1    = 0,
            LCRL_POSMiscCharge2    = 1,
            LCRL_POSMiscCharge3    = 2,
            LCRL_POSMiscCharge4    = 3,
            LCRL_POSMiscCharge5    = 4,
            LCRL_POSMiscCharge6    = 5,
            LCRL_POSMiscCharge7    = 6,
            LCRL_POSMiscCharge8    = 7,
            LCRL_POSMiscCharge9    = 8,
            LCRL_POSMiscCharge10   = 9,
            LCRL_POSMiscCharge11   = 10,
            LCRL_POSMiscCharge12   = 11,
            LCRL_POSMiscCharge13   = 12,
            LCRL_POSMiscCharge14   = 13,
            LCRL_POSMiscCharge15   = 14,
            LCRL_POSMiscCharge16   = 15,

            LCR_NUM_POSMiscCharges = 16
        };

        #endregion Miscellaneous Charge Number

        #region POS Tax Types

        public enum POS_TAX_TYPES
        {
            LCRL_POSTaxType1              = 0,
            LCRL_POSTaxType2              = 1,
            LCRL_POSTaxType3              = 2,
            LCRL_POSTaxType4              = 3,
            LCRL_POSTaxType5              = 4,
            LCRL_POSTaxType6              = 5,
            LCRL_POSTaxType7              = 6,
            LCRL_POSTaxType8              = 7,
            LCRL_POSTaxType9              = 8,
            LCRL_POSTaxType10             = 9,
            LCRL_POSTaxType11             = 10,
            LCRL_POSTaxType12             = 11,
            LCRL_POSTaxType13             = 12,
            LCRL_POSTaxType14             = 13,
            LCRL_POSTaxType15             = 14,
            LCRL_POSTaxType16             = 15,

            LCR_NUM_POSTaxTypes           = 16,
            LCRL_POSTaxNone               = 16,
            LCRL_POSTaxSameAsProduct      = 17,

            LCR_NUM_POSMiscChargeTaxTypes = 18
        };

        #endregion POS Tax Types

        #region POS Tax Categories

        public enum POS_TAX_CATEGORIES
        {
            LCRL_POSTaxCategoryA          = 0,
            LCRL_POSTaxCategoryB          = 1,
            LCRL_POSTaxCategoryC          = 2,
            LCRL_POSTaxCategoryD          = 3,
            LCRL_POSTaxCategoryE          = 4,
            LCRL_POSTaxCategoryF          = 5,
            LCRL_POSTaxCategoryG          = 6,
            LCRL_POSTaxCategoryH          = 7,
            LCRL_POSTaxCategoryI          = 8,
            LCRL_POSTaxCategoryJ          = 9,

            LCR_NUM_POSTaxCategories      = 10
        };

        #endregion POS Tax Categories

        #region Cash Discount Numbers

        public enum CASH_DISCOUNT_NUMBERS
        {
            LCRL_POSCashDiscount1           = 0,
            LCRL_POSCashDiscount2           = 1,
            LCRL_POSCashDiscount3           = 2,
            LCRL_POSCashDiscount4           = 3,
            LCRL_POSCashDiscount5           = 4,
            LCRL_POSCashDiscount6           = 5,
            LCRL_POSCashDiscount7           = 6,
            LCRL_POSCashDiscount8           = 7,
            LCRL_POSCashDiscount9           = 8,
            LCRL_POSCashDiscount10          = 9,
            LCRL_POSCashDiscount11          = 10,
            LCRL_POSCashDiscount12          = 11,
            LCRL_POSCashDiscount13          = 12,
            LCRL_POSCashDiscount14          = 13,
            LCRL_POSCashDiscount15          = 14,
            LCRL_POSCashDiscount16          = 15,
            LCR_NUM_POSCashDiscounts        = 16
        };

        #endregion Cash Discount Numbers

        #region Cash Discount Intervals

        public enum CASH_DISCOUNT_INTERVALS
        {
            LCRL_POSCashInterval1           = 0,
            LCRL_POSCashInterval2           = 1,
            LCRL_POSCashInterval3           = 2,

            LCR_NUM_POSCashIntervals        = 3
        };

        #endregion Cash Discount Intervals

        #region Apply Cash Discounts

        public enum APPLY_CASH_DISCOUNTS
        {
            LCRL_POSApplyBeforeTax          = 0,
            LCRL_POSApplyAfterTax           = 1,
            LCR_NUM_POSApplyDiscounts       = 2
        };

        #endregion Apply Cash Discounts

        #region Volume Discount Number

        public enum VOLUME_DISCOUNT_NUMBER
        {
             LCRL_POSVolumeDiscount1         = 0,
             LCRL_POSVolumeDiscount2         = 1,
             LCRL_POSVolumeDiscount3         = 2,
             LCRL_POSVolumeDiscount4         = 3,
             LCRL_POSVolumeDiscount5         = 4,
             LCRL_POSVolumeDiscount6         = 5,
             LCRL_POSVolumeDiscount7         = 6,
             LCRL_POSVolumeDiscount8         = 7,
             LCRL_POSVolumeDiscount9         = 8,
             LCRL_POSVolumeDiscount10        = 9,
             LCRL_POSVolumeDiscount11        = 10,
             LCRL_POSVolumeDiscount12        = 11,
             LCRL_POSVolumeDiscount13        = 12,
             LCRL_POSVolumeDiscount14        = 13,
             LCRL_POSVolumeDiscount15        = 14,
             LCRL_POSVolumeDiscount16        = 15,
             LCR_NUM_POSVolumeDiscounts      = 16
        };

        #endregion Volume Discount Number

        #region Volume Discount Intervals

        public enum VOLUME_DISCOUNT_INTERVALS
        {
             LCRL_POSVolumeInterval1         = 0,
             LCRL_POSVolumeInterval2         = 1,
             LCRL_POSVolumeInterval3         = 2,

             LCR_NUM_POSVolumeIntervals      = 3
        };

        #endregion Volume Discount Intervals

        #region Product Numbers

        public enum PRODUCT_NUMBERS
        {
             LCRL_PRODUCT1                   = 0,
             LCRL_PRODUCT2                   = 1,
             LCRL_PRODUCT3                   = 2,
             LCRL_PRODUCT4                   = 3,

             LCR_NUM_PRODUCTS                = 4,
             LCR_NUM_BASE_PRODUCTS           = 4,

             LCRL_PRODUCT5                   = 4,
             LCRL_PRODUCT6                   = 5,
             LCRL_PRODUCT7                   = 6,
             LCRL_PRODUCT8                   = 7,
             LCRL_PRODUCT9                   = 8,
             LCRL_PRODUCT10                  = 9,
             LCRL_PRODUCT11                  = 10,
             LCRL_PRODUCT12                  = 11,
             LCRL_PRODUCT13                  = 12,
             LCRL_PRODUCT14                  = 13,
             LCRL_PRODUCT15                  = 14,
             LCRL_PRODUCT16                  = 15,

             LCR_NUM_POS_PRODUCTS            = 16
        };

        #endregion Product Numbers

        #region Preset Types Allowed

        public enum PRESET_TYPES_ALLOWED
        {
             LCRL_PRESETS_NONE               = 0,
             LCRL_PRESETS_GROSS              = 1,
             LCRL_PRESETS_NET                = 2,
             LCRL_PRESETS_BOTH               = 3,

             LCR_NUM_PRESETS_ALLOWED         = 4
        }

        #endregion Preset Types Allowed

        #region Product Types

        public enum PRODUCT_TYPES
        {
             LCRL_AMMONIA                    = 0,
             LCRL_AVIATION                   = 1,
             LCRL_DISTILLATE                 = 2,
             LCRL_GASOLINE                   = 3,
             LCRL_METHANOL                   = 4,
             LCRL_LPG                        = 5,
             LCRL_LUBE_OIL                   = 6,
             LCRL_NO_PRODUCT_TYPE            = 7,

             LCR_NUM_PRODUCT_TYPES           = 8
        }

        #endregion Product Types

        #region Date Formats

        public enum DATE_FORMATS
        {
             LCRL_MMDDYY                     = 0,
             LCRL_DDMMYY                     = 1,

             LCR_NUM_DATE_FORMATS            = 2
        }

        #endregion Date Formats

        #region Units Of Measure

        public enum UNITS_OF_MEASURE
        {
             LCRL_GALLONS                    = 0,
             LCRL_LITRES                     = 1,
             LCRL_CUBIC_M                    = 2,
             LCRL_LBS                        = 3,
             LCRL_KGS                        = 4,
             LCRL_BARRELS                    = 5,
             LCRL_OTHER                      = 6,

             LCR_NUM_UNITS_OF_MEASURE        = 7
        }

        #endregion Units Of Measure

        #region Yes No Fields

        public enum YES_NO_FIELDS
        {
             LCRL_YES                        = 0,
             LCRL_NO                         = 1,

             LCR_NUM_YES_NO                  = 2
        }

        #endregion Yes No Fields

        #region Preset Types

        public enum PRESET_TYPES
        {
             LCRL_CLEAR                      = 0,
             LCRL_MULTIPLE                   = 1,
             LCRL_RETAIN                     = 2,
             LCRL_INVENTORY                  = 3,

             LCR_NUM_PRESET_TYPES            = 4
        }

        #endregion Preset Types

        #region Header Line Indexes

        public enum HEADER_LINE_INDEXES
        {
             LCRL_HEADER1                    = 0,
             LCRL_HEADER2                    = 1,
             LCRL_HEADER3                    = 2,
             LCRL_HEADER4                    = 3,
             LCRL_HEADER5                    = 4,
             LCRL_HEADER6                    = 5,
             LCRL_HEADER7                    = 6,
             LCRL_HEADER8                    = 7,
             LCRL_HEADER9                    = 8,
             LCRL_HEADER10                   = 9,
             LCRL_HEADER11                   = 10,
             LCRL_HEADER12                   = 11,

             LCR_NUM_HEADER_LINES            = 12
        }

        #endregion Header Line Indexes

        #region Pulse Outputs

        public enum PULSE_OUTPUTS
        {
            LCRL_RISING         = 0,
            LCRL_FALLING        = 1,
            LCR_NUM_PULSE_EDGES = 2
        }

        #endregion Pulse Outputs

        #region Temp Units

        public enum TEMP_UNITS
        {
            LCRL_DEG_C                 = 0,
            LCRL_DEG_F                 = 1,
            LCR_NUM_TEMPERATURE_SCALES = 2
        }

        #endregion Temp Units

        #region Rate Base

        public enum RATE_BASE
        {
            LCRL_SECONDS       = 0,
            LCRL_MINUTES       = 1,
            LCRL_HOURS         = 2,
            LCR_NUM_RATE_BASES = 3
        }

        #endregion Rate Base

        #region Linearization Points

        public enum LINEARIZATION_POINTS
        {
             LCRL_LINEAR_POINT1              = 0,
             LCRL_LINEAR_POINT2              = 1,
             LCRL_LINEAR_POINT3              = 2,
             LCRL_LINEAR_POINT4              = 3,
             LCRL_LINEAR_POINT5              = 4,
             LCRL_LINEAR_POINT6              = 5,
             LCRL_LINEAR_POINT7              = 6,
             LCRL_LINEAR_POINT8              = 7,
             LCRL_LINEAR_POINT9              = 8,
             LCRL_LINEAR_POINT10             = 9,

             LCR_NUM_LINEAR_POINTS           = 10
        }

        #endregion Linearization Points

        #region Security

        public enum SECURITY
        {
            LCRL_LOCKED             = 0,
            LCRL_UNLOCKED           = 1,
            LCR_NUM_SECURITY_STATES = 2
        }

        #endregion Security

        #region Decimal Places

        public enum DECIMAL_PLACES
        {
            LCRL_HUNDREDTHS  = 0,
            LCRL_TENTHS      = 1,
            LCRL_WHOLE       = 2,
            LCR_NUM_DECIMALS = 3
        }

        #endregion Decimal Places

        #region Compensation Types

        public enum COMPENSATION_TYPES
        {
             LCRL_COMP_TYPE_NONE             = 0,
             LCRL_LINEAR_F                   = 1,
             LCRL_LINEAR_C                   = 2,
             LCRL_TABLE_24                   = 3,
             LCRL_TABLE_54                   = 4,
             LCRL_TABLE_6B                   = 5,
             LCRL_TABLE_54B                  = 6,
             LCRL_TABLE_54C                  = 7,
             LCRL_TABLE_54D                  = 8,
             LCRL_NH3                        = 9,

             LCR_NUM_COMP_TYPES              = 10,
        }

        #endregion Compensation Types

        #region Diagnostic Messages

        public enum DIAGNOSTIC_MESSAGES
        {
             LCRL_ROM_CHECKSUM               = 0,
             LCRL_TEMPERATURE                = 1,
             LCRL_VCF_TYPE                   = 2,
             LCRL_VCF_PARAMETER              = 3,
             LCRL_VCF_DOMAIN                 = 4,
             LCRL_METER_CALIBRATION          = 5,
             LCRL_PULSER                     = 6,
             LCRL_PRESET_STOP                = 7,
             LCRL_NO_FLOW_STOP               = 8,
             LCRL_STOP_REQUESTED             = 9,
             LCRL_DELIVERY_END_REQUEST       = 10,
             LCRL_POWER_FAIL                 = 11,
             LCRL_PRESET                     = 12,
             LCRL_NO_TERMINAL                = 13,
             LCRL_PRINTER_NOT_READY          = 14,
             LCRL_DATA_ACCESS_ERROR          = 15,
             LCRL_DELIVERY_TICKET_PENDING    = 16,
             LCRL_SHIFT_TICKET_PENDING       = 17,
             LCRL_FLOW_ACTIVE                = 18,
             LCRL_DELIVERY_ACTIVE            = 19,
             LCRL_GROSS_PRESET_ACTIVE        = 20,
             LCRL_NET_PRESET_ACTIVE          = 21,
             LCRL_GROSS_PRESET_STOP          = 22,
             LCRL_NET_PRESET_STOP            = 23,
             LCRL_TVC_ACTIVE                 = 24,
             LCRL_S1_CLOSE                   = 25,
             LCRL_INIT_WARNING               = 28,
             LCRL_END_OF_LIST                = 29
        }

        #endregion Diagnostic Messages

        #region Compensation Parameters

        public enum COMPENSATION_PARAMS
        {
             LCRL_COMP_PARAM_NONE            = 0,
             LCRL_COEFFICIENT1               = 1,
             LCRL_COEFFICIENT2               = 2,
             LCRL_SG_60                      = 3,
             LCRL_DENSITY_KGPL               = 4,
             LCRL_API_GRAVITY                = 5,
             LCRL_DENSITY_KGPCUM1            = 6,
             LCRL_COEFFICIENT3               = 7,
             LCRL_DENSITY_KGPCUM2            = 8,
             LCRL_NO_COMP_PARAM              = 9
        }

        #endregion Compensation Parameters

        #region Compensation Temperatures

        public enum COMPENSATION_TEMPS
        {
             LCRL_NO_COMP_TEMP               = 0,
             LCRL_DEG_F1                     = 1,
             LCRL_DEG_C1                     = 2,
             LCRL_DEG_F2                     = 3,
             LCRL_DEG_C2                     = 4,
             LCRL_DEG_F3                     = 5,
             LCRL_DEG_C3                     = 6,
             LCRL_DEG_C4                     = 7,
             LCRL_DEG_C5                     = 8,
             LCRL_DEG_C6                     = 9
        }

        #endregion Compensation Temperatures

        #region Printer Types

        public enum PRINTER_TYPES
        {
             LCRL_EPSON200ROLL               = 0,
             LCRL_EPSONNEWFONTB              = 0,

             LCRL_EPSON290SLIP               = 1,
             LCRL_EPSONNEWFONTA              = 1,

             LCRL_EPSON295SLIP               = 2,
             LCRL_EPSONOLDFONTA              = 2,

             LCRL_EPSON300ROLL               = 3,
             LCRL_EPSONOLDFONTB              = 3,

             LCRL_OKIDATAML184T              = 4,

             LCRL_AXIOHMBLASTER              = 5,
             LCRL_BLASTER                    = 5,

             LCR_NUM_PRINTER_TYPES           = 6
        }

        #endregion Printer Types

        #region Residuals

        public enum RESIDUALS
        {
             LCRL_ROUND                      = 0,
             LCRL_DELETE                   = 1,

             LCR_NUM_RESIDUALS               = 2
        }

        #endregion Residuals

        #region Flow Directions

        public enum FLOW_DIRECTIONS
        {
             LCRL_RIGHT                      = 0,
             LCRL_LEFT                       = 1,

             LCR_NUM_FLOW_DIRECTIONS         = 2
        }

        #endregion Flow Directions

        #region Auxiliary 1 Options

        public enum AUX1_OPTIONS
        {
             LCRL_AUX1_ON_DURING_DELIVERY    = 0,
             LCRL_AUX1_OFF                   = 1,
             LCRL_AUX1_ON                    = 2,
             LCRL_AUX1_MONITOR_FLOW_RATE     = 3,

             LCR_NUM_AUX1_COMMANDS           = 4,

             LCRL_AUX1_TOGGLE_FLOW_RATE      = 4,
             LCR600_NUM_AUX1_COMMANDS        = 5
        }

        #endregion Auxiliary 1 Options

        #region Auxiliary 2 Options

        public enum AUX2_OPTIONS
        {
             LCRL_AUX2_FLOW_DIRECTION        = 0,
             LCRL_AUX2_ON_DURING_DELIVERY    = 1,
             LCRL_AUX2_OFF                   = 2,
             LCRL_AUX2_ON                    = 3,

             LCR_NUM_AUX2_COMMANDS           = 4,

             LCRL_AUX2_TOGGLE_FLOW_RATE      = 4,
             LCR600_NUM_AUX2_COMMANDS        = 5
        }

        #endregion Auxiliary 2 Options

        #region Reset Commands

        public enum RESET_COMMANDS
        {
             LCRL_CLEAR_ALL                  = 0,
             LCRL_REBUILD                    = 2,

             LCR_NUM_RESET_COMMANDS          = 3
        }

        #endregion Reset Commands

        #region Linearization State

        public enum LINEARIZATION_STATES
        {
             LCRL_APPLIED                    = 0,
             LCRL_SETUP                      = 1,

             LCR_NUM_LINEARIZE_OPTIONS       = 2
        }

        #endregion Linearization State

        #region Password Usage

        public enum PASSWORD_USAGE
        {
             LCRL_PASSWORD_NONE              = 0,
             LCRL_PASSWORD_DELIVERY          = 1,
             LCRL_PASSWORD_SHIFT             = 2,

             LCR_NUM_PASSWORD_OPTIONS        = 3
        }

        #endregion Password Usage

        #region Customer Number Usage

        public enum CUSTOMER_OPTIONS
        {
             LCRL_CUSTOMER_NONE              = 0,
             LCRL_CUSTOMER_DELIVERY          = 1,

             LCR_NUM_CUSTOMER_OPTIONS        = 2
        }

        #endregion Customer Number Usage

        #region LCR A to D Code Words

        public enum LCR_CODE_WORDS
        {
            LCRA_R100_0         = 0x01,
            LCRA_R128_6         = 0x02,
            LCRA_RTD_SLOPE      = 0x04,
            LCRA_RTD_OFFSET     = 0x08,
            LCRA_VOLT_12        = 0x10,
            LCRA_VOLT_16        = 0x20,
            LCRA_VOLT_SLOPE     = 0x40,
            LCRA_VOLT_OFFSET    = 0x80
        }

        #endregion LCR A to D Code Words

        #region LCR Field Numbers

        public enum LCR_FIELD_NUMBERS
        {
            LCRF_ProductNumber_DL           = 0,
            LCRF_ProductCode_DL             = 1,
            LCRF_GrossQty_NE                = 2,
            LCRF_NetQty_NE                  = 3,
            LCRF_FlowRate_NE                = 4,
            LCRF_GrossPreset_PL             = 5,
            LCRF_NetPreset_PL               = 6,
            LCRF_Temp_NE                    = 7,
            LCRF_Residual_WM                = 8,
            LCRF_PulsesPerDistance_UL       = 9,
            LCRF_CalibDistance_UL           = 10,
            LCRF_ProductDescriptor_DL       = 11,
            LCRF_Odometer_UL                = 12,
            LCRF_ShiftGross_NE              = 13,
            LCRF_ShiftNet_NE                = 14,
            LCRF_ShiftDeliveries_NE         = 15,
            LCRF_ClearShift_DL              = 16,
            LCRF_GrossTotal_WM              = 17,
            LCRF_NetTotal_WM                = 18,
            LCRF_DateFormat_UL              = 19,
            LCRF_Date_UL                    = 20,
            LCRF_Time_UL                    = 21,
            LCRF_SaleNumber_WM              = 22,
            LCRF_TicketNumber_WM            = 23,
            LCRF_UnitID_UL                  = 24,
            LCRF_NoFlowTimer_DL             = 25,
            LCRF_S1Close_WM                 = 26,
            LCRF_PresetType_DL              = 27,
            LCRF_PulseOutputEdge_UL         = 28,
            LCRF_Header_AE                  = 29,
            LCRF_TicketHeaderLine_UL        = 30,
            LCRF_PrintGrossAndParam_WM      = 31,
            LCRF_VolCorrectedMsg_WM         = 32,
            LCRF_Temp_WM                    = 33,
            LCRF_TempOffset_WM              = 34,
            LCRF_TempScale_WM               = 35,
            LCRF_MeterID_WM                 = 36,
            LCRF_TicketRequired_WM          = 37,
            LCRF_QtyUnits_WM                = 38,
            LCRF_Decimals_WM                = 39,
            LCRF_FlowDirection_WM           = 40,
            LCRF_TimeUnit_WM                = 41,
            LCRF_CalibrationEvent_NE        = 42,
            LCRF_ConfigurationEvent_NE      = 43,
            LCRF_GrossCount_NE              = 44,
            LCRF_NetCount_NE                = 45,
            LCRF_ProverQty_WM               = 46,
            LCRF_PulsesPerUnit_WM           = 47,
            LCRF_AuxMult_WM                 = 48,
            LCRF_AuxUnit_WM                 = 49,
            LCRF_CalibrationNumber_NE       = 50,
            LCRF_LinearPoint_AE             = 51,
            LCRF_LinearFlowRate_WM          = 52,
            LCRF_LinearError_WM             = 53,
            LCRF_LinearProverQty_WM         = 54,
            LCRF_Linearize_WM               = 55,
            LCRF_Printer_WM                 = 56,
            LCRF_CompensationType_WM        = 57,
            LCRF_CompensationParam_WM       = 58,
            LCRF_BaseTemp_WM                = 59,
            LCRF_Software_NE                = 60,
            LCRF_PricePerUnit_DL            = 61,
            LCRF_TaxPerUnit_DL              = 62,
            LCRF_PercentTax_DL              = 63,
            LCRF_DiagnosticMessages_AE      = 64,
            LCRF_TotalTaxPerUnit_NE         = 65,
            LCRF_TotalPercentTax_NE         = 66,
            LCRF_ADCCode_NE                 = 67,
            LCRF_SupplyVoltage_NE           = 68,
            LCRF_PulserReversals_NE         = 69,
            LCRF_ShiftStart_NE              = 70,
            LCRF_AuxQty_NE                  = 71,
            LCRF_UserKey_DL                 = 72,
            LCRF_Security_UL                = 73,
            LCRF_FactoryKey_AE              = 74,
            LCRF_R100_0_FL                  = 75,
            LCRF_R128_6_FL                  = 76,
            LCRF_RawADC_NE                  = 77,
            LCRF_RTDSlope_FL                = 78,
            LCRF_RTDOffset_FL               = 79,
            LCRF_SerialID_FL                = 80,
            LCRF_UserSetKey_FL              = 81,
            LCRF_LCRReset_FL                = 82,
            LCRF_CompParamType_NE           = 83,
            LCRF_CompTempType_NE            = 84,
            LCRF_PresetsAllowed_DL          = 85,
            LCRF_Aux1_DL                    = 86,
            LCRF_Aux2_DL                    = 87,
            LCRF_DefaultProduct_NE          = 88,
            LCRF_DeliveryStart_NE           = 89,
            LCRF_DeliveryFinish_NE          = 90,
            LCRF_LastCalibrated_NE          = 91,
            LCRF_GrossRemaining_NE          = 92,
            LCRF_NetRemaining_NE            = 93,
            LCRF_ProductType_WM             = 94,
            LCRF_SubTotalCost_NE            = 95,
            LCRF_TotalTax_NE                = 96,
            LCRF_TotalCost_NE               = 97,
            LCRF_Ticket_NE                  = 98,
            LCRF_Language_NE                = 99,

            LCR_NUM_FIELDS                  = 100,

            LCRF_PreviousGross              = 100,
            LCRF_PreviousNet                = 101,
            LCRF_ShiftPassword              = 105,
            LCRF_CustomerID                 = 106,
            LCRF_UsePassword                = 107,
            LCRF_UseCustomer                = 108,
            LCRF_MaxPressure                = 109,
            LCRF_MaxPressureFlowRate        = 110,
            LCRF_ShutdownPressure           = 111,

            LCRF_DBMNode                    = 112,
            LCRF_FSNode                     = 113,
            LCRF_FSPort                     = 114,
            LCRF_FSBaud                     = 115,
            LCRF_FSRetries                  = 116,
            LCRF_FSTxEnable                 = 117,
            LCRF_FSTimeout                  = 118,

            LCRF_PulseOutputMultiplier      = 119,
            LCRF_Aux1FlowRateToggle         = 120,
            LCRF_Aux2FlowRateToggle         = 121,
            LCRF_DeliveryScreen             = 122,

            LCRF_BootloaderRev              = 123,

            LCRF_ActivateFeatureKey         = 124,
            LCRF_FeaturesActivated          = 125,

            LCRF_BASE_POS_FIELD             = 192,

            LCRF_PricePreset                = LCRF_BASE_POS_FIELD +  0,
            LCRF_POSLockDiscounts           = LCRF_BASE_POS_FIELD +  1,
            LCRF_POSLockPrice               = LCRF_BASE_POS_FIELD +  2,
            LCRF_POSSaveNewPrice            = LCRF_BASE_POS_FIELD +  3,
            LCRF_POSAllowNewDeliveryPrice   = LCRF_BASE_POS_FIELD +  4,
            LCRF_POSProductNumber           = LCRF_BASE_POS_FIELD +  5,
            LCRF_POSProductName             = LCRF_BASE_POS_FIELD +  6,
            LCRF_POSProductCode             = LCRF_BASE_POS_FIELD +  7,
            LCRF_POSProductType             = LCRF_BASE_POS_FIELD +  8,
            LCRF_POSProductCalibration      = LCRF_BASE_POS_FIELD +  9,
            LCRF_POSProductPrice            = LCRF_BASE_POS_FIELD + 10,
            LCRF_POSProductTax              = LCRF_BASE_POS_FIELD + 11,
            LCRF_POSProductCashDiscount     = LCRF_BASE_POS_FIELD + 12,
            LCRF_POSProductVolumeDiscount   = LCRF_BASE_POS_FIELD + 13,
            LCRF_POSTaxNumber               = LCRF_BASE_POS_FIELD + 14,
            LCRF_POSTaxName                 = LCRF_BASE_POS_FIELD + 15,
            LCRF_POSTaxCategory             = LCRF_BASE_POS_FIELD + 16,
            LCRF_POSTaxValue                = LCRF_BASE_POS_FIELD + 17,
            LCRF_POSTaxType                 = LCRF_BASE_POS_FIELD + 18,
            LCRF_POSTaxHeader               = LCRF_BASE_POS_FIELD + 19,
            LCRF_POSCashDiscountNumber      = LCRF_BASE_POS_FIELD + 20,
            LCRF_POSCashDiscountName        = LCRF_BASE_POS_FIELD + 21,
            LCRF_POSCashDiscountInterval    = LCRF_BASE_POS_FIELD + 22,
            LCRF_POSCashDiscountPercent     = LCRF_BASE_POS_FIELD + 23,
            LCRF_POSCashDiscountPerUnit     = LCRF_BASE_POS_FIELD + 24,
            LCRF_POSCashDiscountDays        = LCRF_BASE_POS_FIELD + 25,
            LCRF_POSCashDiscountApply       = LCRF_BASE_POS_FIELD + 26,
            LCRF_POSVolumeDiscountNumber    = LCRF_BASE_POS_FIELD + 27,
            LCRF_POSVolumeDiscountName      = LCRF_BASE_POS_FIELD + 28,
            LCRF_POSVolumeDiscountInterval  = LCRF_BASE_POS_FIELD + 29,
            LCRF_POSVolumeDiscountPercent   = LCRF_BASE_POS_FIELD + 30,
            LCRF_POSVolumeDiscountPerUnit   = LCRF_BASE_POS_FIELD + 31,
            LCRF_POSVolumeDiscountAmount    = LCRF_BASE_POS_FIELD + 32,
            LCRF_POSVolumeDiscountApply     = LCRF_BASE_POS_FIELD + 33,
            LCRF_POSSubTotalDue             = LCRF_BASE_POS_FIELD + 34,
            LCRF_POSPreVolumeDiscount       = LCRF_BASE_POS_FIELD + 35,
            LCRF_POSTaxDue                  = LCRF_BASE_POS_FIELD + 36,
            LCRF_POSPostVolumeDiscount      = LCRF_BASE_POS_FIELD + 37,
            LCRF_POSTotalDue                = LCRF_BASE_POS_FIELD + 38,
            LCRF_POSCashDiscount            = LCRF_BASE_POS_FIELD + 39,
            LCRF_POSDiscountDate            = LCRF_BASE_POS_FIELD + 40,
            LCRF_POSMiscChargeNumber        = LCRF_BASE_POS_FIELD + 41,
            LCRF_POSMiscChargeName          = LCRF_BASE_POS_FIELD + 42,
            LCRF_POSMiscChargeTax           = LCRF_BASE_POS_FIELD + 43,
            LCRF_POSMiscChargePrice         = LCRF_BASE_POS_FIELD + 44,
            LCRF_POSMiscChargeQty           = LCRF_BASE_POS_FIELD + 45,
            LCRF_POSMiscChargeSubTotal      = LCRF_BASE_POS_FIELD + 46,
            LCRF_POSMiscChargeTaxDue        = LCRF_BASE_POS_FIELD + 47,
            LCRF_POSMiscChargeTotal         = LCRF_BASE_POS_FIELD + 48,

            LCR_NUM_POS_FIELDS              = 49,

            LCR600_NUM_FIELDS               = LCRF_BASE_POS_FIELD + LCR_NUM_POS_FIELDS,

            LCR_MAX_FIELDS                  = 256
        }

        #endregion LCR Field Numbers

        #region LCR Command Code Definitions

        public enum LCR_COMMANDS
        {
             LCRC_RUN            = 0,         // run command
             LCRC_STOP           = 1,         // stop command
             LCRC_END_DELIVERY   = 2,         // end delivery command
             LCRC_AUX            = 3,         // auxiliary command
             LCRC_SHIFT          = 4,         // shift command
             LCRC_CALIBRATE      = 5,         // calibrate command
             LCRC_PRINT          = 6,         // print command

             LCR_NUM_COMMANDS    = 7          // number of LCR commands
        }

        #endregion LCR Command Code Definitions

        #region LCR Status Masks

        // LCR status masks.
        //      LCRSc_* = Delivery Code
        //      LCRSd_* = Delivery Status
        //      LCRSm_* = Machine Status
        //      LCRSp_* = Printer Status

        public enum LCR_STATUS_MASKS
        {
             LCRSc_DEL_TICKET_PENDING    = 0x0001,
             LCRSc_SHIFT_TICKET_PENDING  = 0x0002,
             LCRSc_FLOW_ACTIVE           = 0x0004,
             LCRSc_DELIVERY_ACTIVE       = 0x0008,
             LCRSc_GROSS_PRESET_ACTIVE   = 0x0010,
             LCRSc_NET_PRESET_ACTIVE     = 0x0020,
             LCRSc_GROSS_PRESET_STOP     = 0x0040,
             LCRSc_NET_PRESET_STOP       = 0x0080,
             LCRSc_TVC_ACTIVE            = 0x0100,
             LCRSc_S1_CLOSE_REACHED      = 0x0200,
             LCRSc_BEGIN_DELIVERY        = 0x0400,
             LCRSc_NEW_DELIVERY_QUEUED   = 0x0800,
             LCRSc_INIT_WARNING          = 0x1000,
             LCRSc_CONFIG_EVENT          = 0x2000,
             LCRSc_CALIB_EVENT           = 0x4000,

             LCRSd_ROM_CHECKSUM          = 0x0001,
             LCRSd_TEMPERATURE           = 0x0002,
             LCRSd_VCF_TYPE              = 0x0004,
             LCRSd_WATCHDOG              = 0x0004,
             LCRSd_VCF_PARAMETER         = 0x0008,
             LCRSd_VCF_SETUP             = 0x0008,
             LCRSd_VCF_DOMAIN            = 0x0010,
             LCRSd_METER_CALIBRATION     = 0x0020,
             LCRSd_PULSER_FAILURE        = 0x0040,
             LCRSd_PRESET_STOP           = 0x0080,
             LCRSd_NO_FLOW_STOP          = 0x0100,
             LCRSd_STOP_REQUEST          = 0x0200,
             LCRSd_DELIVERY_END_REQUEST  = 0x0400,
             LCRSd_POWER_FAIL            = 0x0800,
             LCRSd_PRESET_ERROR          = 0x1000,
             LCRSd_LAPPAD_ERROR          = 0x2000,
             LCRSd_PRINTER_ERROR         = 0x4000,
             LCRSd_DATA_ACCESS_ERROR     = 0x8000,

             LCRSm_UNKNOWN               = 0x07,

             LCRSm_SWITCH_BETWEEN        = 0x00,
             LCRSm_SWITCH_RUN            = 0x01,
             LCRSm_SWITCH_STOP           = 0x02,
             LCRSm_SWITCH_PRINT          = 0x03,
             LCRSm_SWITCH_SHIFT_PRINT    = 0x04,
             LCRSm_SWITCH_CALIBRATE      = 0x05,
             LCRSm_SWITCH                = 0x07,
             LCRSm_PRINTING              = 0x08,
             LCRSm_STATE_RUN             = 0x00,
             LCRSm_STATE_STOP            = 0x10,
             LCRSm_STATE_END_DELIVERY    = 0x20,
             LCRSm_STATE_AUX             = 0x30,
             LCRSm_STATE_SHIFT           = 0x40,
             LCRSm_STATE_CALIBRATE       = 0x50,
             LCRSm_STATE_WAIT_NOFLOW     = 0x60,
             LCRSm_STATE                 = 0x70,
             LCRSm_ERROR                 = 0x80,

             LCRSp_DELIVERY_TICKET       = 0x01,
             LCRSp_SHIFT_TICKET          = 0x02,
             LCRSp_DIAGNOSTIC_TICKET     = 0x04,
             LCRSp_USER_PRINT            = 0x08,
             LCRSp_OUT_OF_PAPER          = 0x10,  // reserved
             LCRSp_OFFLINE               = 0x20,  // reserved
             LCRSp_ERROR_WARNING         = 0x40,
             LCRSp_BUSY                  = 0x80,
        }

        #endregion LCR Status Masks

        #region LCR Return Codes

        public enum LCR_RETURN_CODES
        {
             LCP02Rd_PARAMID         = 0x20,  // bad parameter identifier return code
             LCP02Rd_FIELDNUM        = 0x21,  // bad field number return code
             LCP02Rd_FIELDDATA       = 0x22,  // bad field data
             LCP02Rd_NOTSET          = 0x23,  // set not allowed due to mode vs status
             LCP02Rd_COMMANDNUM      = 0x24,  // bad command number return code
             LCP02Rd_DEVICEADDR      = 0x25,  // bad device address
             LCP02Rd_REQUESTQUEUED   = 0x26,  // request is queued
             LCP02Rd_NOREQUESTQUEUED = 0x27,  // no request is queued
             LCP02Rd_REQUESTABORTED  = 0x28,  // request has been aborted
             LCP02Rd_PREVIOUSREQUEST = 0x29,  // previous request being processed
             LCP02Rd_ABORTNOTALLOWED = 0x2A,  // abort is not allowed
             LCP02Rd_PARAMBLOCK      = 0x2B,  // bad parameter block number return code
             LCP02Rd_BAUDRATE        = 0x2C,  // bad baud rate

             LCP02Rf_START           = 0x30,  // start operation failed
             LCP02Rf_NOACK           = 0x31,  // a shift out operation was not acknowledged
             LCP02Rf_CRC             = 0x32,  // CRC error on read data
             LCP02Rf_PARAMETERS      = 0x33,  // invalid input parameters
             LCP02Rf_PAGEBOUNDARY    = 0x34,  // page boundary error
             LCP02Rf_WRITEFAILED     = 0x35,  // write operation failed

             LCP02Rt_OVERRANGE       = 0x40,  // temperature over range
             LCP02Rt_UNDERRANGE      = 0x41,  // temperature under range
             LCP02Rt_VCFTYPE         = 0x43,  // VCF type error
             LCP02Rt_PARAMLIMIT      = 0x44,  // table parameter out of limits
             LCP02Rt_DOMAIN          = 0x45,  // temperature domain error

             LCP02Ra_OPEN            = 0x50,  // NBS card initialization error
             LCP02Ra_CLOSE           = 0x51,  // NBS card termination error
             LCP02Ra_CONNECT         = 0x52,  // connection failed error
             LCP02Ra_MSGSIZE         = 0x53,  // message size error
             LCP02Ra_DEVICE          = 0x54,  // invalid device number
             LCP02Ra_FIELDNUM        = 0x55,  // invalid field number
             LCP02Ra_FIELDDATA       = 0x56,  // bad field data
             LCP02Ra_COMMANDNUM      = 0x57,  // invalid command number
             LCP02Ra_NODEVICES       = 0x58,  // no devices present in network
             LCP02Ra_VERSION         = 0x59,  // version mismatch
             LCP02Ra_PARAMSTRING     = 0x5A,  // bad environment parameter string
             LCP02Ra_NOREQUESTQUEUED = 0x5B,  // no request has been queued
             LCP02Ra_QUEUEDREQUEST   = 0x5C,  // a request is already queued
             LCP02Ra_REQUESTCODE     = 0x5D,  // invalid request code queued
             LCP02Ra_NOTOPENED       = 0x5E,  // network communications is not opened
             LCP02Ra_ALREADYOPENED   = 0x5F,  // network communications already opened

             LCP02Rp_PRINTERBUSY     = 0x60,  // the printer is busy
             LCP02Rp_OVERFLOW        = 0x61,  // printer buffer overflow
             LCP02Rp_TIMEOUT         = 0x62,  // printer timeout

             LCP02Rg_FIELDINIT       = 0x70,  // flash variable has not been initialized
             LCP02Rg_FIELDRANGE      = 0x71,  // new field table data out of range
             LCP02Rg_NOTREAD         = 0x72,  // flash structure not yet read
             LCP02Rg_GROSSNOTACTIVE  = 0x73,  // gross preset is not active
             LCP02Rg_NETNOTACTIVE    = 0x74,  // net preset is not active
             LCP02Rg_NEVERSETTABLE   = 0x75,  // field is never settable
             LCP02Rg_FIELDNOTUSED    = 0x76,  // field is not used
             LCP02Rg_BADSWITCH       = 0x77,  // bad switch position for requested command
             LCP02Rg_BADSTATE        = 0x78,  // bad machine state for requested command
             LCP02Rg_PENDINGTICKET   = 0x79,  // command ignored due to pending ticket
             LCP02Rg_FLASHPROTECTED  = 0x7A,  // flash is protected via the switch
             LCP02Rg_DUPLINEARPOINT  = 0x7B,  // duplicate linearization points
             LCP02Rg_LINEARRANGE     = 0x7C,  // adjacent linear points range error
             LCP02Rg_ENDOFLIST       = 0x7D,  // end of transactions list
             LCP02Rg_LOSTRANSACTIONS = 0x7E,  // transactions were lost in the LCR
             LCP02Rg_CUSTOMERNOTSET  = 0x7F,  // customer number has not been set

            // LCR communications mode bytes.

             LCRM_NO_WAIT            = 0x00,  // API will not wait for response
             LCRM_WAIT               = 0x01,  // API will wait for response
             LCRM_ABORT              = 0x02,  // API will abort request if no response

            // LCR security levels.

             LCRS_PAUSE_LEVEL        = 0x00,  // delivery has been paused
             LCRS_DELIVERY_LEVEL     = 0x01,  // switch out of calibration, not unlocked, no delivery active
             LCRS_UNLOCKED_LEVEL     = 0x02,  // switch out of calibration, unlocked, no delivery active
             LCRS_WM_LEVEL           = 0x03,  // switch in calibration, no delivery active
             LCRS_FACTORY_LEVEL      = 0x04,  // user can set factory parameters
             LCRS_ALWAYS_EDITABLE    = 0x08,  // field is always editable
             LCRS_NEVER_EDITABLE     = 0x09,  // field is never editable

             LCRS_DELIVERY_ACTIVE    = 0x80   // delivery active bit
        }

        #endregion LCR Return Codes

        #endregion Enumeration

        // Structure definitions.

        #region Structure Definitions

        #pragma warning disable 0169, 0414          // Disable 'not used' warning

        [StructLayout(LayoutKind.Explicit)]
        public struct FloatByteArray
        {
            [FieldOffset(0)]
            public byte Byte1;
            [FieldOffset(1)]
            public byte Byte2;
            [FieldOffset(2)]
            public byte Byte3;
            [FieldOffset(3)]
            public byte Byte4;
            [FieldOffset(0)]
            public float fltValue;
            [FieldOffset(0)]
            public Int32 int32Value;
            [FieldOffset(0)]
            public Int16 int16Value;
            [FieldOffset(0)]
            public UInt32 intU32Value;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct LCRStatus
        {
            public UInt16 intDeliveryCode;
            public UInt16 intDeliveryStatus;
            public byte bytPrinterStatus;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct LCRTransaction
        {
            public Single sngTemperature;
            public UInt32 intCustomer;
            public Int32 intSaleNumber;
            public Int32 intGross;
            public Int32 intNet;
            public Int16 intStatus;
            public byte bytCompensationType;
            public byte bytDateFormat;
            public byte bytDecimals;
            public byte bytProduct;
            public byte bytQuantityUnits;
            public byte bytTemperatureScale;
            public byte[] bytDateTime;
        }

        #pragma warning restore 0169, 0414

        #endregion Structure Definitions

        #region Delegates

        public delegate void DelayFunction();

        #endregion Delegates

        #region Functions

        /// <summary>
        /// LCP02 function defintions
        /// </summary>

        [DllImport("LCLP0232.dll")]
        public static extern byte LCP02ActivatePumpAndPrint(byte device, byte mode);
        [DllImport("LCLP0232.dll")]
        public static extern byte LCP02AreYouThere(byte device);
        [DllImport("LCLP0232.dll")]
        public static extern byte LCP02Close();
        [DllImport("LCLP0232.dll")]
        public static extern byte LCP02DeleteTransaction(byte device, byte mode);
        [DllImport("LCLP0232.dll")]
        public static extern byte LCP02EStop(byte device, byte mode);
        [DllImport("LCLP0232.dll")]
        public static extern Int32 LCP02FieldSize(byte fieldNum);
        [DllImport("LCLP0232.dll")]
        public static extern byte LCP02GetDeliveryStatus(byte device, IntPtr status, out byte devStatus, byte mode);
        [DllImport("LCLP0232.dll")]
        public static extern byte LCP02GetField(byte device, byte fieldNum, IntPtr fieldData, out byte devStatus, byte mode);
        [DllImport("LCLP0232.dll")]
        public static extern byte LCP02GetFieldSecurity(IntPtr fieldSecurity);
        [DllImport("LCLP0232.dll")]
        public static extern byte LCP02GetFieldType(IntPtr fieldTypes);
        [DllImport("LCLP0232.dll")]
        public static extern byte LCP02GetFirstTicketLine(byte device, IntPtr ticketLine, byte mode);
        [DllImport("LCLP0232.dll")]
        public static extern byte LCP02GetLibraryRevision(IntPtr revision);
        [DllImport("LCLP0232.dll")]
        public static extern byte LCP02GetNextTicketLine(byte device, IntPtr ticketLine, byte mode);
        [DllImport("LCLP0232.dll")]
        public static extern byte LCP02GetNumFields(out byte numFields);
        [DllImport("LCLP0232.dll")]
        public static extern byte LCP02GetSecurityLevel(byte device, out byte security, byte mode);
        [DllImport("LCLP0232.dll")]
        public static extern byte LCP02GetStatus(byte device, IntPtr status, out byte devStatus, byte mode);
        [DllImport("LCLP0232.dll")]
        public static extern byte LCP02GetTransaction(byte device, IntPtr trans, byte mode);
        [DllImport("LCLP0232.dll")]
        public static extern byte LCP02GetVersion(byte device, out UInt16 version, byte mode);
        [DllImport("LCLP0232.dll")]
        public static extern byte LCP02IssueCommand(byte device, byte command, byte mode);
        [DllImport("LCLP0232.dll")]
        public static extern byte LCP02MustGetDeliveryStatus(byte device, IntPtr status, out byte devStatus, DelayFunction EntryDelay, DelayFunction ExitDelay);
        [DllImport("LCLP0232.dll")]
        public static extern byte LCP02MustGetField(byte device, byte fieldNum, IntPtr fieldData, out byte devStatus, DelayFunction EntryDelay, DelayFunction ExitDelay);
        [DllImport("LCLP0232.dll")]
        public static extern byte LCP02MustGetSecurityLevel(byte device, out byte security, DelayFunction EntryDelay, DelayFunction ExitDelay);
        [DllImport("LCLP0232.dll")]
        public static extern byte LCP02MustGetStatus(byte device, IntPtr status, out byte devStatus, DelayFunction EntryDelay, DelayFunction ExitDelay);
        [DllImport("LCLP0232.dll")]
        public static extern byte LCP02MustPrint(byte device, IntPtr text, Int32 textLen, DelayFunction EntryDelay, DelayFunction ExitDelay);
        [DllImport("LCLP0232.dll")]
        public static extern byte LCP02MustSetField(byte device, byte fieldNum, IntPtr fieldData, out byte devStatus, DelayFunction EntryDelay, DelayFunction ExitDelay);
        [DllImport("LCLP0232.dll")]
        public static extern byte LCP02Open(byte minDevice, byte maxDevice, out byte numDevices, IntPtr devices);
        [DllImport("LCLP0232.dll")]
        public static extern byte LCP02Print(byte device, IntPtr text, Int32 textLen, byte mode);
        [DllImport("LCLP0232.dll")]
        public static extern byte LCP02SetDeviceAddr(byte device, byte newDevice, byte mode);
        [DllImport("LCLP0232.dll")]
        public static extern byte LCP02SetField(byte device, byte fieldNum, IntPtr fieldData, out byte devStatus, byte mode);
        [DllImport("LCLP0232.dll")]
        public static extern byte LCP02AbortRequest(byte device);
        [DllImport("LCLP0232.dll")]
        public static extern byte LCP02CheckRequest(byte device);
        [DllImport("LCLP0232.dll")] 
        public static extern byte LCP02GetPortSettings(IntPtr portName, out byte baudRateIndex, out byte txEnable, out byte maxRetries, out int timeout, out int baudSearch);

        [DllImport("LCLP0232.dll")]
        public static extern byte LCP02IPActivatePumpAndPrint(byte device, IntPtr ipAddr, byte mode);
        [DllImport("LCLP0232.dll")]
        public static extern byte LCP02IPAreYouThere(byte device, IntPtr ipAddr);
        [DllImport("LCLP0232.dll")]
        public static extern byte LCP02IPDeleteTransaction(byte device, IntPtr ipAddr, byte mode);
        [DllImport("LCLP0232.dll")]
        public static extern byte LCP02IPEStop(byte device, IntPtr ipAddr, byte mode);
        [DllImport("LCLP0232.dll")]
        public static extern Int32 LCP02IPFieldSize(byte fieldNum, IntPtr ipAddr);
        [DllImport("LCLP0232.dll")]
        public static extern byte LCP02IPGetDeliveryStatus(byte device, IntPtr ipAddr, IntPtr status, out byte devStatus, byte mode);
        [DllImport("LCLP0232.dll")]
        public static extern byte LCP02IPGetField(byte device, IntPtr ipAddr, byte fieldNum, IntPtr fieldData, out byte devStatus, byte mode);
        [DllImport("LCLP0232.dll")]
        public static extern byte LCP02IPGetFirstTicketLine(byte device, IntPtr ipAddr, IntPtr ticketLine, byte mode);
        [DllImport("LCLP0232.dll")]
        public static extern byte LCP02IPGetNextTicketLine(byte device, IntPtr ipAddr, IntPtr ticketLine, byte mode);
        [DllImport("LCLP0232.dll")]
        public static extern byte LCP02IPGetSecurityLevel(byte device, IntPtr ipAddr, out byte security, byte mode);
        [DllImport("LCLP0232.dll")]
        public static extern byte LCP02IPGetStatus(byte device, IntPtr ipAddr, IntPtr status, out byte devStatus, byte mode);
        [DllImport("LCLP0232.dll")]
        public static extern byte LCP02IPGetTransaction(byte device, IntPtr ipAddr, IntPtr trans, byte mode);
        [DllImport("LCLP0232.dll")]
        public static extern byte LCP02IPGetVersion(byte device, IntPtr ipAddr, out Int16 version, byte mode);
        [DllImport("LCLP0232.dll")]
        public static extern byte LCP02IPIssueCommand(byte device, IntPtr ipAddr, byte command, byte mode);
        [DllImport("LCLP0232.dll")]
        public static extern byte LCP02IPMustGetDeliveryStatus(byte device, IntPtr ipAddr, IntPtr status, out byte devStatus, DelayFunction EntryDelay, DelayFunction ExitDelay);
        [DllImport("LCLP0232.dll")]
        public static extern byte LCP02IPMustGetField(byte device, IntPtr ipAddr, byte fieldNum, IntPtr fieldData, out byte devStatus, DelayFunction EntryDelay, DelayFunction ExitDelay);
        [DllImport("LCLP0232.dll")]
        public static extern byte LCP02IPMustGetSecurityLevel(byte device, IntPtr ipAddr, out byte security, DelayFunction EntryDelay, DelayFunction ExitDelay);
        [DllImport("LCLP0232.dll")]
        public static extern byte LCP02IPMustGetStatus(byte device, IntPtr ipAddr, IntPtr status, out byte devStatus, DelayFunction EntryDelay, DelayFunction ExitDelay);
        [DllImport("LCLP0232.dll")]
        public static extern byte LCP02IPMustPrint(byte device, IntPtr ipAddr, IntPtr text, Int32 textLen, DelayFunction EntryDelay, DelayFunction ExitDelay);
        [DllImport("LCLP0232.dll")]
        public static extern byte LCP02IPMustSetField(byte device, IntPtr ipAddr, byte fieldNum, IntPtr fieldData, out byte devStatus, DelayFunction EntryDelay, DelayFunction ExitDelay);
        [DllImport("LCLP0232.dll")]
        public static extern byte LCP02IPOpen(int ipPort);
        [DllImport("LCLP0232.dll")]
        public static extern byte LCP02IPPrint(byte device, IntPtr ipAddr, IntPtr text, Int32 textLen, byte mode);
        [DllImport("LCLP0232.dll")]
        public static extern byte LCP02IPSetDeviceAddr(byte device, IntPtr ipAddr, byte newDevice, byte mode);
        [DllImport("LCLP0232.dll")]
        public static extern byte LCP02IPSetField(byte device, IntPtr ipAddr, byte fieldNum, IntPtr fieldData, out byte devStatus, byte mode);
        [DllImport("LCLP0232.dll")]
        public static extern byte LCP02IPAbortRequest(byte device, IntPtr ipAddr);
        [DllImport("LCLP0232.dll")]
        public static extern byte LCP02IPCheckRequest(byte device, IntPtr ipAddr);
        [DllImport("LCLP0232.dll")]
        public static extern byte LCP02RegisterIPNodes(int port, IntPtr ipAddr, byte minDevice, byte maxDevice);
        [DllImport("LCLP0232.dll")]
        public static extern byte LCP02IPGetFieldParameter(byte device, IntPtr ipAddr, byte fieldParameter);
        
        #endregion Functions

    }
}