﻿

using MySql.Data.MySqlClient;
using System.Data.SQLite;
using System.IO;
using System.Reflection;
using SDK;
using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Data;

namespace LCR600SDK
{
    public partial class FRunLcrTransfer : Form
    {
        public double LitersQty = 0.0;
        public float qty = -1;
        private string myConnectionString = string.Empty;
        private byte device = 0;
        private int stepTimerOpen = 0;
        private int stepTimerClose = 0; 
        public int intCurrentTick = 0;
        public int intMaxTick = 0;
        private bool isProccessGet = false;
        PasswordEncrypDecrypt decryptor = new PasswordEncrypDecrypt();
        public FRunLcrTransfer()
        {
            InitializeComponent();
        }

        private const int CP_DISABLE_CLOSE_BUTTON = 0x200;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ClassStyle = cp.ClassStyle | CP_DISABLE_CLOSE_BUTTON;
                return cp;
            }
        }

        private bool open()
        {
            return true;
        }

        private bool start()
        {
            return true;
        }

        private bool print()
        {
            return true;
        }

        private bool closed()
        {
            return true;
        }

        private byte doConnectLCR()
        {
            byte device = 250;
            byte numDevices;
            byte[] deviceList = new byte[device];
            IntPtr deviceListPtr = Marshal.AllocHGlobal(250);
            byte rc = LCP02API.LCP02Open(device, device, out numDevices, deviceListPtr);
            if (rc != (byte)LCPAPI.LCPR.LCPR_OK && rc != (byte)LCP02API.LCR_RETURN_CODES.LCP02Ra_ALREADYOPENED)
            {
                device = 0;
            }
            return device;
        }

        private void doOpenLCR()
        {
            try
            {
                device = doConnectLCR();
                if (device > 0)
                {
                    txtGrossQty.Text = getGrossQty(qty).Qty.ToString();
                    txtTotalizerGross.Text = getGrossTotalizer(qty).Qty.ToString();
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        private string doStartLCR()
        {
            string strStatus = "PLEASE WAIT..";

            if (device == 0)
                return strStatus;

            byte rc = LCP02API.LCP02IssueCommand(device, (byte)LCP02API.LCR_COMMANDS.LCRC_RUN,
                                                         (byte)LCP02API.LCR_RETURN_CODES.LCRM_NO_WAIT);

            if (rc == (byte)LCPAPI.LCPR.LCPR_OK)
                strStatus = "RUN";

            return strStatus;
        }

        private string doPrintLCR()
        {
            string strStatus = "PLEASE WAIT..";

            if (device == 0)
                return strStatus;


            byte rc = LCP02API.LCP02IssueCommand(device, (byte)LCP02API.LCR_COMMANDS.LCRC_PRINT,
                                                         (byte)LCP02API.LCR_RETURN_CODES.LCRM_NO_WAIT);

            if (rc == (byte)LCPAPI.LCPR.LCPR_OK)
                strStatus = "STOP";

            return strStatus;
        }

        private bool doCloseLCR()
        {
            byte rc = LCP02API.LCP02Close();
            if (rc != (byte)LCPAPI.LCPR.LCPR_OK)
            {
                //MessageBox.Show("LCP02Close error, rc =" + rc)
                Console.WriteLine("LCP02Close error, rc =" + rc);
                return false;
            }
            return true;
        }

        private ClsLcrRequest getGrossQty(float lastQty)
        {
            string grossQtyStr = lastQty.ToString();
            double grossQty = 0;
            try
            {

                byte rc;

                if (device == 0)
                {
                    return new ClsLcrRequest(lastQty, false);
                }

                byte fieldNum = (byte)LCP02API.LCR_FIELD_NUMBERS.LCRF_GrossQty_NE;
                int size = LCP02API.LCP02FieldSize(fieldNum);
                IntPtr grossQtyPtr = Marshal.AllocHGlobal((int)size);

                byte devStatus;

                rc = LCP02API.LCP02GetField(device, fieldNum, grossQtyPtr, out devStatus,
                                            (byte)LCP02API.LCR_RETURN_CODES.LCRM_NO_WAIT);

                if (rc != (byte)LCPAPI.LCPR.LCPR_OK)
                {
                    return new ClsLcrRequest(lastQty, false);
                }

                string strGrossQty = Marshal.PtrToStringAnsi(grossQtyPtr, (int)size);
                LCP02API.FloatByteArray fbaGross;
                byte[] strGrossArr = null;

                fbaGross = new LCP02API.FloatByteArray();
                strGrossArr = Encoding.GetEncoding(1252).GetBytes(strGrossQty);
                fbaGross.Byte1 = strGrossArr[0];
                fbaGross.Byte2 = strGrossArr[1];
                fbaGross.Byte3 = strGrossArr[2];
                fbaGross.Byte4 = strGrossArr[3];
                
                if (lastQty > 0)
                {
                    if (fbaGross.int32Value <= 0)
                    {
                        grossQty = Convert.ToDouble(lastQty.ToString());
                    }
                    else
                    {
                        strGrossQty = fbaGross.int32Value.ToString();
                        if (strGrossQty != "")
                        {
                            grossQty = Convert.ToDouble(strGrossQty);
                            grossQty = grossQty / 10;
                        }
                        else
                        {
                            grossQty = Convert.ToDouble(strGrossQty);
                        }
                    }
                }
                else
                {
                    strGrossQty = fbaGross.int32Value.ToString();
                    if (strGrossQty != "")
                    {
                        grossQty = Convert.ToDouble(strGrossQty);
                        grossQty = grossQty / 10;
                    }
                    else
                    {
                        grossQty = 0;
                    }
                }

                grossQtyStr = grossQty.ToString();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return new ClsLcrRequest(grossQty, true);
        }

        private ClsLcrRequest getGrossTotalizer(float lastQty)
        {
            byte rc;
            string grossTotalizerStr = lastQty.ToString();
            double grossQty = 0;

            if (device == 0)
            {
                return new ClsLcrRequest(lastQty, false);
            }

            byte fieldNum = (byte)LCP02API.LCR_FIELD_NUMBERS.LCRF_GrossTotal_WM;
            int size = LCP02API.LCP02FieldSize(fieldNum);
            IntPtr grossTotalizerPtr = Marshal.AllocHGlobal((int)size);

            byte devStatus;

            rc = LCP02API.LCP02GetField(device, fieldNum, grossTotalizerPtr, out devStatus,
                                        (byte)LCP02API.LCR_RETURN_CODES.LCRM_NO_WAIT);

            if (rc != (byte)LCPAPI.LCPR.LCPR_OK)
            {
                return new ClsLcrRequest(lastQty, false);
            }

            string strGrossTotalizer = Marshal.PtrToStringAnsi(grossTotalizerPtr, (int)size);
            LCP02API.FloatByteArray fbaGross;
            byte[] strGrossArr = null;

            fbaGross = new LCP02API.FloatByteArray();
            strGrossArr = Encoding.GetEncoding(1252).GetBytes(strGrossTotalizer);
            fbaGross.Byte1 = strGrossArr[0];
            fbaGross.Byte2 = strGrossArr[1];
            fbaGross.Byte3 = strGrossArr[2];
            fbaGross.Byte4 = strGrossArr[3];

            
            strGrossTotalizer = fbaGross.int32Value.ToString();
            if (strGrossTotalizer != "")
            {
                grossQty = Convert.ToDouble(strGrossTotalizer);
                grossQty = grossQty / 10;
            }

            grossTotalizerStr = grossQty.ToString();

            return new ClsLcrRequest(grossQty, true);
        }
         

        private bool getData()
        {
            ClsLcrRequest clsLcrRequest = new ClsLcrRequest(0, false);
            try
            {
                isProccessGet = true;
                clsLcrRequest = getGrossQty(float.Parse(txtGrossQty.Text));
                txtGrossQty.Text = clsLcrRequest.Qty.ToString();

                clsLcrRequest = getGrossTotalizer(float.Parse(txtTotalizerGross.Text));
                txtTotalizerGross.Text = clsLcrRequest.Qty.ToString();

                if (txtTotalizerGross.Text.ToUpper() != "0" && txtTotalizerStart.Text.Trim() == "")
                {
                    txtTotalizerStart.Text = txtTotalizerGross.Text;
                }

                if (clsLcrRequest.Status == false)
                {
                    doCloseLCR();
                    Thread.Sleep(500);
                    doOpenLCR();
                    return false;
                } 

                saveLog();

                return true;
            }
            catch (Exception)
            {
                return false;
            } 
            finally
            {
                isProccessGet = false;
            }
        }

        private void saveLog()
        {
            string datetimeNow = DateTime.Now.ToString("yyyyMMdd_HHmmss");
            string fileName = @"C:\Lab\log_Qty_WhouseTransfer_" + datetimeNow + ".txt";

            try
            {
                var _item = GlobalModel.whTransfer;
                //var _item = GlobalModel.logsheet_detail;
                var initData = GlobalModel.GlobalVar;
                DateTime dt = DateTime.Parse(DateTime.Now.ToString());

                // Check if file already exists. If yes, delete it.     
                if (File.Exists(fileName))
                {
                    File.Delete(fileName);
                }

                // Create a new file     
                using (StreamWriter sw = File.CreateText(fileName))
                {
                    //sw.WriteLine("HM " + _item.hm.ToString());
                    //sw.WriteLine("UnitNo " + _item.unit_no.ToString());
                    //sw.WriteLine("NamaOpr " + _item.nama_operator.ToString());
                    sw.WriteLine("grossQty " + txtGrossQty.Text.ToString());
                    sw.WriteLine("grossTotalizerStart " + txtTotalizerStart.Text.ToString());
                    sw.WriteLine("grossTotalizerEnd " + txtTotalizerGross.Text.ToString());
                    sw.WriteLine("RefStart " + txtRefStart.Text.ToString());
                    sw.WriteLine("RefStop " + dt.ToString("HH:mm:ss"));
                    sw.WriteLine("ModDate " + DateTime.Now);
                    sw.WriteLine("ModBy " + initData.DataEmp.Nrp);
                }

                // Write file contents on console.     
                using (StreamReader sr = File.OpenText(fileName))
                {
                    string s = "";
                    while ((s = sr.ReadLine()) != null)
                    {
                        Console.WriteLine(s);
                    }
                }
            }
            catch (Exception Ex)
            {
                Console.WriteLine(Ex.ToString());
            }
        }

        private void timerOpen_Tick(object sender, EventArgs e)
        {
            if (stepTimerOpen == 1)
            {
                if (start())
                {
                    doStartLCR();
                    timerOpen.Enabled = false;
                    timerQty.Enabled = true;
                }
            }

            if (stepTimerOpen == 0)
            {
                if (open())
                {
                    doOpenLCR();
                    stepTimerOpen = 1;
                }
            }
        }

        private void timerQty_Tick(object sender, EventArgs e)
        {
            getData();
        }

        private int intWait = 35;
        private void timerClose_Tick(object sender, EventArgs e)
        {
            intWait -= 1;
            lblAlertClosed.Text = $"Wait {intWait} second";  

            if (stepTimerClose == 0 && intWait <= 0)
            { 
                intCurrentTick += 1;
                if (intCurrentTick <= intMaxTick)
                {
                    if (getData())
                    {
                        txtGrossQty.Visible = true;
                        this.LitersQty = Convert.ToDouble(txtGrossQty.Text.ToString());
                        tryDataOk();
                        this.DialogResult = DialogResult.OK;
                    }
                }
                else
                {
                    timerClose.Enabled = false;
                    showDialogConfirm();
                } 
            }
        }

        void setMaxIterate()
        {
            try
            {
                var _proper = Properties.Settings.Default;
                intMaxTick = _proper.maxTickSecond;
                intWait = 35;
            }
            catch (Exception)
            {
                intMaxTick = 5;
            }
        }

        void showDialogConfirm()
        {
            DialogResult dialogResult = MessageBox.Show("Sistem gagal membaca data Qty terakhir", "Ulangi ?", MessageBoxButtons.RetryCancel, MessageBoxIcon.Question);

            if (dialogResult == DialogResult.Retry)
            {
                timerClose.Enabled = true;
                intCurrentTick = 0;
                intWait = 5;
            }
            else
            {
                this.DialogResult = DialogResult.Abort;
            }
        }

        void tryDataOk()
        {
            try
            {

                bool isFailed = true;
                while (isFailed)
                {
                    if (txtGrossQty.Text.Trim().ToUpper() == "WAIT..")
                    {
                        getData();
                        System.Threading.Thread.Sleep(100);
                    }
                    else
                        isFailed = false;
                }
                saveData();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

        }

        void saveData()
        {
            try
            {
                var _item = GlobalModel.whTransfer;
                var _lcrData = GlobalModel.lcrData;

                _item.qty_xfer = Convert.ToDouble(txtGrossQty.Text.Trim());// isNumeric(txtGrossQTY.Text.Replace(".",",")) ? Convert.ToDouble(txtGrossQTY.Text) : 0;
                _item.fmeter_end = Convert.ToDouble(txtTotalizerGross.Text.Trim()); //isNumeric(txtGrossTotalizer.Text.Replace(".", ",")) ? Convert.ToDouble(txtGrossTotalizer.Text) : 0;
                _item.fmeter_begin = Convert.ToDouble(txtTotalizerStart.Text.Trim()); // isNumeric(txtTotalizerStart.Text.Replace(".", ",")) ? Convert.ToDouble(txtTotalizerStart.Text) : 0;
                _item.qty_xfer = Convert.ToDouble(txtGrossQty.Text.Trim());  
                var conn = new SQLiteConnection(myConnectionString);
                if (conn.State != ConnectionState.Open) conn.Open();
                string query = string.Empty;
                int recs = 0;
                query = " INSERT INTO whtranfer(job_row_id, xfer_code, xfer_date, dstrct_code, employee_id, issued_whouse, received_whouse, stock_code, qty_xfer, shift, fmeter_serial_no, faktor_meter, fmeter_begin, fmeter_end, created_by, xfer_by, received_by, is_submit, issued_whouse_type, received_whouse_type, mod_date, created_date, timezone, syncs)"
                        + "VALUES(@job_row_id, @xfer_code, @xfer_date, @dstrct_code, @employee_id, @issued_whouse, @received_whouse, @stock_code, @qty_xfer, @shift, @fmeter_serial_no, @faktor_meter, @fmeter_begin, @fmeter_end, @created_by, @xfer_by, @received_by, @is_submit, @issued_whouse_type, @received_whouse_type, @mod_date, @created_date, @timezone, @syncs)";

                SQLiteCommand cmd = new SQLiteCommand(query, conn);

                cmd.Parameters.Add(new SQLiteParameter("@job_row_id", _item.job_row_id));
                cmd.Parameters.Add(new SQLiteParameter("@xfer_code", _item.xfer_code));
                cmd.Parameters.Add(new SQLiteParameter("@xfer_date", _item.xfer_date));
                cmd.Parameters.Add(new SQLiteParameter("@dstrct_code", _item.dstrct_code));
                cmd.Parameters.Add(new SQLiteParameter("@employee_id", _item.employee_id));
                cmd.Parameters.Add(new SQLiteParameter("@issued_whouse", _item.issued_whouse));
                cmd.Parameters.Add(new SQLiteParameter("@received_whouse", _item.received_whouse));
                cmd.Parameters.Add(new SQLiteParameter("@stock_code", _item.stock_code));
                cmd.Parameters.Add(new SQLiteParameter("@qty_xfer", _item.qty_xfer));
                cmd.Parameters.Add(new SQLiteParameter("@shift", _item.shift));
                cmd.Parameters.Add(new SQLiteParameter("@fmeter_serial_no", _item.fmeter_serial_no));
                cmd.Parameters.Add(new SQLiteParameter("@faktor_meter", _item.faktor_meter));
                cmd.Parameters.Add(new SQLiteParameter("@fmeter_begin", _item.fmeter_begin));
                cmd.Parameters.Add(new SQLiteParameter("@fmeter_end", _item.fmeter_end));
                cmd.Parameters.Add(new SQLiteParameter("@created_by", _item.created_by));
                cmd.Parameters.Add(new SQLiteParameter("@xfer_by", _item.xfer_by));
                cmd.Parameters.Add(new SQLiteParameter("@received_by", _item.received_by));
                cmd.Parameters.Add(new SQLiteParameter("@is_submit", _item.is_submit));
                cmd.Parameters.Add(new SQLiteParameter("@issued_whouse_type", _item.issued_whouse_type));
                cmd.Parameters.Add(new SQLiteParameter("@received_whouse_type", _item.received_whouse_type));
                cmd.Parameters.Add(new SQLiteParameter("@mod_date", _item.mod_date));
                cmd.Parameters.Add(new SQLiteParameter("@created_date", _item.created_date));
                cmd.Parameters.Add(new SQLiteParameter("@timezone", _item.timezone));
                cmd.Parameters.Add(new SQLiteParameter("@syncs", _item.syncs));

                recs = cmd.ExecuteNonQuery();

                if (recs > 0)
                {

                }
                if (conn.State == ConnectionState.Open) conn.Close();
            }
            catch (Exception ex)
            {
                save_error_log(ex.ToString(), "saveData()");
                MessageBox.Show(ex.ToString());
            }
        }

        void save_error_log(string data, string actions)
        {
            try
            {
                DateTime localDate = DateTime.Now;
                var _item = new tbl_log();
                var initData = GlobalModel.GlobalVar;

                _item.pid = System.Guid.NewGuid().ToString();
                _item.actions = $"District {initData.DataEmp.DstrctCode}/FRunLcrTransfer/{actions}";
                _item.message = $"{data}";
                _item.mod_date = localDate;
                _item.mod_by = initData.DataEmp.Nrp;

                SQLiteConnection conn;

                conn = new SQLiteConnection(myConnectionString);
                if (conn.State != ConnectionState.Open) conn.Open();
                string query = string.Empty;
                query = "INSERT INTO tbl_log(pid,actions,message,mod_date,mod_by)"
                      + " VALUES(@pid,@actions,@message,@mod_date,@mod_by)";

                SQLiteCommand cmd = new SQLiteCommand(query, conn);

                cmd.Parameters.Add(new SQLiteParameter("@pid", _item.pid));
                cmd.Parameters.Add(new SQLiteParameter("@actions", _item.actions));
                cmd.Parameters.Add(new SQLiteParameter("@message", _item.message));
                cmd.Parameters.Add(new SQLiteParameter("@mod_date", _item.mod_date));
                cmd.Parameters.Add(new SQLiteParameter("@mod_by", _item.mod_by));
                cmd.ExecuteNonQuery();
                if (conn.State == ConnectionState.Open) conn.Close();
            }
            catch (Exception)
            {
            }
        }

        private void FRunLcr_Load(object sender, EventArgs e)
        {
            var _proper = Properties.Settings.Default; 
            setMaxIterate();
            //myConnectionString = "Data Source=" + Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "SQLite", "db_iflash.db") + " ;New=False;";
            var passPhrase = "iFlashDesktop";
            var decryp = decryptor.DecryptPassword(_proper.db_pass, passPhrase);
            myConnectionString = "Data Source=" + Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "SQLite", "db_iflash.db") + " ;New=False;";
            txtLoad();
            txtRefStart.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            stepTimerOpen = 0;

            doPrintLCR();
            doCloseLCR();

            timerQty.Enabled = false;
            timerClose.Enabled = false;
            timerOpen.Enabled = true;

            lblAlertClosed.Visible = false;
        }

        void txtLoad()
        {
            txtGrossQty.Text = "";
            txtTotalizerGross.Text = "";
            txtTotalizerStart.Text = "";
            txtRefStart.Text = "";
            txtRefStop.Text = "";
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            intWait = 30;

            try
            {
                btnStop.Enabled = false;
                lblAlertClosed.Visible = true;
                doPrintLCR();
                doCloseLCR();
                stepTimerClose = 0;
                txtRefStop.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                timerClose.Enabled = true;
                timerOpen.Enabled = false;
                timerQty.Enabled = false;
            }
            catch (Exception)
            {

            }
        }

        private void FRunLcr_FormClosing(object sender, FormClosingEventArgs e)
        {
            doPrintLCR();
            doCloseLCR();

            timerOpen.Enabled = false;
            timerQty.Enabled = false;
            timerClose.Enabled = false;
        }

        private void FRunLcr_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;

            doPrintLCR();
            doCloseLCR();

            timerOpen.Enabled = false;
            timerQty.Enabled = false;
            timerClose.Enabled = false;
        }
         
    }
}
