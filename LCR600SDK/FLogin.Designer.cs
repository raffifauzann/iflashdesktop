﻿namespace LCR600SDK
{
    partial class FLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FLogin));
            this.txtUSername = new System.Windows.Forms.TextBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblMesage = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblUrlConnection = new System.Windows.Forms.Label();
            this.btnConnection = new System.Windows.Forms.Button();
            this.pnlLoginFuelman = new System.Windows.Forms.Panel();
            this.pnlLoginDriver = new System.Windows.Forms.Panel();
            this.listHistoryLoginDriver = new System.Windows.Forms.ListBox();
            this.txtPasswordDriver = new System.Windows.Forms.TextBox();
            this.txtUsernameDriver = new System.Windows.Forms.TextBox();
            this.lblMesageDriver = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.iconButton1 = new FontAwesome.Sharp.IconButton();
            this.iconButton2 = new FontAwesome.Sharp.IconButton();
            this.listHistoryLogin = new System.Windows.Forms.ListBox();
            this.iconBtnHide = new FontAwesome.Sharp.IconButton();
            this.iconBtnShow = new FontAwesome.Sharp.IconButton();
            this.lblAppVersion = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblCheckConnection = new System.Windows.Forms.Label();
            this.tmr_counting = new System.Windows.Forms.Timer(this.components);
            this.btnLoginDriver = new LCR600SDK.CustomButton();
            this.customButton2 = new LCR600SDK.CustomButton();
            this.btnLogin = new LCR600SDK.CustomButton();
            this.btnCancel = new LCR600SDK.CustomButton();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.pnlLoginFuelman.SuspendLayout();
            this.pnlLoginDriver.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtUSername
            // 
            this.txtUSername.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.txtUSername.Location = new System.Drawing.Point(50, 144);
            this.txtUSername.Margin = new System.Windows.Forms.Padding(2);
            this.txtUSername.Name = "txtUSername";
            this.txtUSername.Size = new System.Drawing.Size(328, 35);
            this.txtUSername.TabIndex = 20;
            this.txtUSername.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtUSername.Click += new System.EventHandler(this.txtUSername_Click);
            this.txtUSername.TextChanged += new System.EventHandler(this.txtUSername_TextChanged);
            // 
            // txtPassword
            // 
            this.txtPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.txtPassword.Location = new System.Drawing.Point(50, 222);
            this.txtPassword.Margin = new System.Windows.Forms.Padding(2);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(328, 35);
            this.txtPassword.TabIndex = 21;
            this.txtPassword.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtPassword.UseSystemPasswordChar = true;
            this.txtPassword.Click += new System.EventHandler(this.txtPassword_Click);
            this.txtPassword.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPassword_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(124, 113);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(192, 29);
            this.label1.TabIndex = 22;
            this.label1.Text = "PNRP Fuelman";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(142, 191);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(128, 29);
            this.label2.TabIndex = 23;
            this.label2.Text = "Password";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic) 
                | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(60, 48);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(317, 39);
            this.label3.TabIndex = 24;
            this.label3.Text = "SILAHKAN LOGIN";
            // 
            // lblMesage
            // 
            this.lblMesage.AutoSize = true;
            this.lblMesage.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.lblMesage.ForeColor = System.Drawing.Color.OrangeRed;
            this.lblMesage.Location = new System.Drawing.Point(45, 259);
            this.lblMesage.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblMesage.Name = "lblMesage";
            this.lblMesage.Size = new System.Drawing.Size(135, 29);
            this.lblMesage.TabIndex = 25;
            this.lblMesage.Text = "lblMesage";
            this.lblMesage.Visible = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(453, -2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(271, 271);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 26;
            this.pictureBox1.TabStop = false;
            // 
            // lblUrlConnection
            // 
            this.lblUrlConnection.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblUrlConnection.AutoSize = true;
            this.lblUrlConnection.BackColor = System.Drawing.Color.Transparent;
            this.lblUrlConnection.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUrlConnection.ForeColor = System.Drawing.Color.Black;
            this.lblUrlConnection.Location = new System.Drawing.Point(528, 334);
            this.lblUrlConnection.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblUrlConnection.Name = "lblUrlConnection";
            this.lblUrlConnection.Size = new System.Drawing.Size(95, 20);
            this.lblUrlConnection.TabIndex = 29;
            this.lblUrlConnection.Text = "Production";
            this.lblUrlConnection.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnConnection
            // 
            this.btnConnection.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnConnection.BackColor = System.Drawing.Color.Green;
            this.btnConnection.FlatAppearance.BorderSize = 0;
            this.btnConnection.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConnection.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConnection.ForeColor = System.Drawing.Color.White;
            this.btnConnection.Location = new System.Drawing.Point(495, 385);
            this.btnConnection.Margin = new System.Windows.Forms.Padding(2);
            this.btnConnection.Name = "btnConnection";
            this.btnConnection.Size = new System.Drawing.Size(191, 43);
            this.btnConnection.TabIndex = 30;
            this.btnConnection.Text = "ONLINE";
            this.btnConnection.UseVisualStyleBackColor = false;
            this.btnConnection.Click += new System.EventHandler(this.btnOnlineOffline_ClickAsync);
            // 
            // pnlLoginFuelman
            // 
            this.pnlLoginFuelman.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(101)))), ((int)(((byte)(179)))));
            this.pnlLoginFuelman.Controls.Add(this.listHistoryLogin);
            this.pnlLoginFuelman.Controls.Add(this.btnLogin);
            this.pnlLoginFuelman.Controls.Add(this.txtPassword);
            this.pnlLoginFuelman.Controls.Add(this.txtUSername);
            this.pnlLoginFuelman.Controls.Add(this.lblMesage);
            this.pnlLoginFuelman.Controls.Add(this.label1);
            this.pnlLoginFuelman.Controls.Add(this.label2);
            this.pnlLoginFuelman.Controls.Add(this.btnCancel);
            this.pnlLoginFuelman.Controls.Add(this.label8);
            this.pnlLoginFuelman.Controls.Add(this.iconBtnHide);
            this.pnlLoginFuelman.Controls.Add(this.iconBtnShow);
            this.pnlLoginFuelman.Location = new System.Drawing.Point(2, 2);
            this.pnlLoginFuelman.Name = "pnlLoginFuelman";
            this.pnlLoginFuelman.Size = new System.Drawing.Size(470, 428);
            this.pnlLoginFuelman.TabIndex = 31;
            // 
            // pnlLoginDriver
            // 
            this.pnlLoginDriver.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(101)))), ((int)(((byte)(179)))));
            this.pnlLoginDriver.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlLoginDriver.Controls.Add(this.listHistoryLoginDriver);
            this.pnlLoginDriver.Controls.Add(this.btnLoginDriver);
            this.pnlLoginDriver.Controls.Add(this.txtPasswordDriver);
            this.pnlLoginDriver.Controls.Add(this.txtUsernameDriver);
            this.pnlLoginDriver.Controls.Add(this.lblMesageDriver);
            this.pnlLoginDriver.Controls.Add(this.label6);
            this.pnlLoginDriver.Controls.Add(this.label7);
            this.pnlLoginDriver.Controls.Add(this.label3);
            this.pnlLoginDriver.Controls.Add(this.customButton2);
            this.pnlLoginDriver.Controls.Add(this.iconButton1);
            this.pnlLoginDriver.Controls.Add(this.iconButton2);
            this.pnlLoginDriver.Location = new System.Drawing.Point(2, 2);
            this.pnlLoginDriver.Name = "pnlLoginDriver";
            this.pnlLoginDriver.Size = new System.Drawing.Size(470, 428);
            this.pnlLoginDriver.TabIndex = 88;
            // 
            // listHistoryLoginDriver
            // 
            this.listHistoryLoginDriver.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listHistoryLoginDriver.FormattingEnabled = true;
            this.listHistoryLoginDriver.ItemHeight = 42;
            this.listHistoryLoginDriver.Location = new System.Drawing.Point(50, 177);
            this.listHistoryLoginDriver.Name = "listHistoryLoginDriver";
            this.listHistoryLoginDriver.Size = new System.Drawing.Size(328, 214);
            this.listHistoryLoginDriver.TabIndex = 87;
            this.listHistoryLoginDriver.Visible = false;
            this.listHistoryLoginDriver.Click += new System.EventHandler(this.listHistoryLoginDriver_Click);
            // 
            // txtPasswordDriver
            // 
            this.txtPasswordDriver.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.txtPasswordDriver.Location = new System.Drawing.Point(50, 222);
            this.txtPasswordDriver.Margin = new System.Windows.Forms.Padding(2);
            this.txtPasswordDriver.Name = "txtPasswordDriver";
            this.txtPasswordDriver.Size = new System.Drawing.Size(328, 35);
            this.txtPasswordDriver.TabIndex = 21;
            this.txtPasswordDriver.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtPasswordDriver.UseSystemPasswordChar = true;
            this.txtPasswordDriver.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPasswordDriver_KeyDown);
            // 
            // txtUsernameDriver
            // 
            this.txtUsernameDriver.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.txtUsernameDriver.Location = new System.Drawing.Point(50, 144);
            this.txtUsernameDriver.Margin = new System.Windows.Forms.Padding(2);
            this.txtUsernameDriver.Name = "txtUsernameDriver";
            this.txtUsernameDriver.Size = new System.Drawing.Size(328, 35);
            this.txtUsernameDriver.TabIndex = 20;
            this.txtUsernameDriver.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtUsernameDriver.Click += new System.EventHandler(this.txtUsernameDriver_Click);
            this.txtUsernameDriver.TextChanged += new System.EventHandler(this.txtUsernameDriver_TextChanged);
            // 
            // lblMesageDriver
            // 
            this.lblMesageDriver.AutoSize = true;
            this.lblMesageDriver.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.lblMesageDriver.ForeColor = System.Drawing.Color.OrangeRed;
            this.lblMesageDriver.Location = new System.Drawing.Point(45, 259);
            this.lblMesageDriver.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblMesageDriver.Name = "lblMesageDriver";
            this.lblMesageDriver.Size = new System.Drawing.Size(85, 29);
            this.lblMesageDriver.TabIndex = 25;
            this.lblMesageDriver.Text = "label5";
            this.lblMesageDriver.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold);
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(124, 113);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(161, 29);
            this.label6.TabIndex = 22;
            this.label6.Text = "PNRP Driver";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold);
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(142, 191);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(128, 29);
            this.label7.TabIndex = 23;
            this.label7.Text = "Password";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic) 
                | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(71, 49);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(317, 39);
            this.label8.TabIndex = 24;
            this.label8.Text = "SILAHKAN LOGIN";
            // 
            // iconButton1
            // 
            this.iconButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.iconButton1.BackColor = System.Drawing.Color.Transparent;
            this.iconButton1.FlatAppearance.BorderSize = 0;
            this.iconButton1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.iconButton1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.iconButton1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconButton1.ForeColor = System.Drawing.Color.White;
            this.iconButton1.IconChar = FontAwesome.Sharp.IconChar.Lock;
            this.iconButton1.IconColor = System.Drawing.Color.White;
            this.iconButton1.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.iconButton1.IconSize = 25;
            this.iconButton1.Location = new System.Drawing.Point(383, 221);
            this.iconButton1.Name = "iconButton1";
            this.iconButton1.Size = new System.Drawing.Size(51, 44);
            this.iconButton1.TabIndex = 41;
            this.iconButton1.UseVisualStyleBackColor = false;
            this.iconButton1.Click += new System.EventHandler(this.btnShowPass_Click);
            // 
            // iconButton2
            // 
            this.iconButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.iconButton2.BackColor = System.Drawing.Color.Transparent;
            this.iconButton2.FlatAppearance.BorderSize = 0;
            this.iconButton2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.iconButton2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.iconButton2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconButton2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.iconButton2.IconChar = FontAwesome.Sharp.IconChar.LockOpen;
            this.iconButton2.IconColor = System.Drawing.Color.White;
            this.iconButton2.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.iconButton2.IconSize = 25;
            this.iconButton2.Location = new System.Drawing.Point(383, 221);
            this.iconButton2.Name = "iconButton2";
            this.iconButton2.Size = new System.Drawing.Size(51, 44);
            this.iconButton2.TabIndex = 42;
            this.iconButton2.UseVisualStyleBackColor = false;
            this.iconButton2.Click += new System.EventHandler(this.btnShowPass_Click);
            // 
            // listHistoryLogin
            // 
            this.listHistoryLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listHistoryLogin.FormattingEnabled = true;
            this.listHistoryLogin.ItemHeight = 42;
            this.listHistoryLogin.Location = new System.Drawing.Point(50, 178);
            this.listHistoryLogin.Name = "listHistoryLogin";
            this.listHistoryLogin.Size = new System.Drawing.Size(328, 214);
            this.listHistoryLogin.TabIndex = 87;
            this.listHistoryLogin.Click += new System.EventHandler(this.listHistoryLogin_Click);
            // 
            // iconBtnHide
            // 
            this.iconBtnHide.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.iconBtnHide.BackColor = System.Drawing.Color.Transparent;
            this.iconBtnHide.FlatAppearance.BorderSize = 0;
            this.iconBtnHide.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.iconBtnHide.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.iconBtnHide.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconBtnHide.ForeColor = System.Drawing.Color.White;
            this.iconBtnHide.IconChar = FontAwesome.Sharp.IconChar.Lock;
            this.iconBtnHide.IconColor = System.Drawing.Color.White;
            this.iconBtnHide.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.iconBtnHide.IconSize = 25;
            this.iconBtnHide.Location = new System.Drawing.Point(385, 221);
            this.iconBtnHide.Name = "iconBtnHide";
            this.iconBtnHide.Size = new System.Drawing.Size(51, 44);
            this.iconBtnHide.TabIndex = 41;
            this.iconBtnHide.UseVisualStyleBackColor = false;
            this.iconBtnHide.Click += new System.EventHandler(this.btnShowPass_Click);
            // 
            // iconBtnShow
            // 
            this.iconBtnShow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.iconBtnShow.BackColor = System.Drawing.Color.Transparent;
            this.iconBtnShow.FlatAppearance.BorderSize = 0;
            this.iconBtnShow.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.iconBtnShow.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.iconBtnShow.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconBtnShow.ForeColor = System.Drawing.SystemColors.ControlText;
            this.iconBtnShow.IconChar = FontAwesome.Sharp.IconChar.LockOpen;
            this.iconBtnShow.IconColor = System.Drawing.Color.White;
            this.iconBtnShow.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.iconBtnShow.IconSize = 25;
            this.iconBtnShow.Location = new System.Drawing.Point(385, 221);
            this.iconBtnShow.Name = "iconBtnShow";
            this.iconBtnShow.Size = new System.Drawing.Size(51, 44);
            this.iconBtnShow.TabIndex = 42;
            this.iconBtnShow.UseVisualStyleBackColor = false;
            this.iconBtnShow.Click += new System.EventHandler(this.btnShowPass_Click);
            // 
            // lblAppVersion
            // 
            this.lblAppVersion.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblAppVersion.AutoSize = true;
            this.lblAppVersion.BackColor = System.Drawing.Color.Transparent;
            this.lblAppVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAppVersion.ForeColor = System.Drawing.Color.Black;
            this.lblAppVersion.Location = new System.Drawing.Point(503, 303);
            this.lblAppVersion.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblAppVersion.MaximumSize = new System.Drawing.Size(500, 0);
            this.lblAppVersion.Name = "lblAppVersion";
            this.lblAppVersion.Size = new System.Drawing.Size(164, 31);
            this.lblAppVersion.TabIndex = 32;
            this.lblAppVersion.Text = "V 2.11.56.0";
            this.lblAppVersion.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(477, 272);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(209, 31);
            this.label4.TabIndex = 33;
            this.label4.Text = "iFlash Desktop";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblCheckConnection
            // 
            this.lblCheckConnection.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCheckConnection.AutoSize = true;
            this.lblCheckConnection.BackColor = System.Drawing.Color.Transparent;
            this.lblCheckConnection.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCheckConnection.ForeColor = System.Drawing.Color.Black;
            this.lblCheckConnection.Location = new System.Drawing.Point(491, 363);
            this.lblCheckConnection.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCheckConnection.Name = "lblCheckConnection";
            this.lblCheckConnection.Size = new System.Drawing.Size(117, 20);
            this.lblCheckConnection.TabIndex = 34;
            this.lblCheckConnection.Text = "Checking in : ";
            this.lblCheckConnection.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblCheckConnection.Visible = false;
            // 
            // tmr_counting
            // 
            this.tmr_counting.Interval = 1000;
            this.tmr_counting.Tick += new System.EventHandler(this.tmr_counting_Tick);
            // 
            // btnLoginDriver
            // 
            this.btnLoginDriver.BackColor = System.Drawing.Color.Green;
            this.btnLoginDriver.BackgroundColor = System.Drawing.Color.Green;
            this.btnLoginDriver.BorderColor = System.Drawing.Color.Empty;
            this.btnLoginDriver.BorderRadius = 40;
            this.btnLoginDriver.BorderSize = 0;
            this.btnLoginDriver.FlatAppearance.BorderSize = 0;
            this.btnLoginDriver.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green;
            this.btnLoginDriver.FlatAppearance.MouseOverBackColor = System.Drawing.Color.SeaGreen;
            this.btnLoginDriver.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLoginDriver.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F);
            this.btnLoginDriver.ForeColor = System.Drawing.Color.White;
            this.btnLoginDriver.Location = new System.Drawing.Point(50, 317);
            this.btnLoginDriver.Name = "btnLoginDriver";
            this.btnLoginDriver.Size = new System.Drawing.Size(338, 51);
            this.btnLoginDriver.TabIndex = 33;
            this.btnLoginDriver.Text = "LOGIN";
            this.btnLoginDriver.TextColor = System.Drawing.Color.White;
            this.btnLoginDriver.UseVisualStyleBackColor = false;
            this.btnLoginDriver.Click += new System.EventHandler(this.btnLoginDriver_Click);
            // 
            // customButton2
            // 
            this.customButton2.BackColor = System.Drawing.Color.Red;
            this.customButton2.BackgroundColor = System.Drawing.Color.Red;
            this.customButton2.BorderColor = System.Drawing.Color.Empty;
            this.customButton2.BorderRadius = 40;
            this.customButton2.BorderSize = 0;
            this.customButton2.FlatAppearance.BorderSize = 0;
            this.customButton2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.customButton2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Brown;
            this.customButton2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.customButton2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F);
            this.customButton2.ForeColor = System.Drawing.Color.White;
            this.customButton2.Location = new System.Drawing.Point(147, 317);
            this.customButton2.Name = "customButton2";
            this.customButton2.Size = new System.Drawing.Size(150, 51);
            this.customButton2.TabIndex = 35;
            this.customButton2.Text = "EXIT";
            this.customButton2.TextColor = System.Drawing.Color.White;
            this.customButton2.UseVisualStyleBackColor = false;
            this.customButton2.Visible = false;
            // 
            // btnLogin
            // 
            this.btnLogin.BackColor = System.Drawing.Color.Green;
            this.btnLogin.BackgroundColor = System.Drawing.Color.Green;
            this.btnLogin.BorderColor = System.Drawing.Color.Empty;
            this.btnLogin.BorderRadius = 40;
            this.btnLogin.BorderSize = 0;
            this.btnLogin.FlatAppearance.BorderSize = 0;
            this.btnLogin.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green;
            this.btnLogin.FlatAppearance.MouseOverBackColor = System.Drawing.Color.SeaGreen;
            this.btnLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F);
            this.btnLogin.ForeColor = System.Drawing.Color.White;
            this.btnLogin.Location = new System.Drawing.Point(50, 317);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(338, 51);
            this.btnLogin.TabIndex = 33;
            this.btnLogin.Text = "LOGIN";
            this.btnLogin.TextColor = System.Drawing.Color.White;
            this.btnLogin.UseVisualStyleBackColor = false;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.Red;
            this.btnCancel.BackgroundColor = System.Drawing.Color.Red;
            this.btnCancel.BorderColor = System.Drawing.Color.Empty;
            this.btnCancel.BorderRadius = 40;
            this.btnCancel.BorderSize = 0;
            this.btnCancel.FlatAppearance.BorderSize = 0;
            this.btnCancel.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnCancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Brown;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F);
            this.btnCancel.ForeColor = System.Drawing.Color.White;
            this.btnCancel.Location = new System.Drawing.Point(147, 317);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(150, 51);
            this.btnCancel.TabIndex = 35;
            this.btnCancel.Text = "EXIT";
            this.btnCancel.TextColor = System.Drawing.Color.White;
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Visible = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // FLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightGray;
            this.ClientSize = new System.Drawing.Size(709, 432);
            this.ControlBox = false;
            this.Controls.Add(this.lblCheckConnection);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lblAppVersion);
            this.Controls.Add(this.btnConnection);
            this.Controls.Add(this.lblUrlConnection);
            this.Controls.Add(this.pnlLoginDriver);
            this.Controls.Add(this.pnlLoginFuelman);
            this.Controls.Add(this.pictureBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FLogin";
            this.Padding = new System.Windows.Forms.Padding(2);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "IFlash Dekstop";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FLogin_FormClosing);
            this.Load += new System.EventHandler(this.FLogin_Load);
            this.Shown += new System.EventHandler(this.FLogin_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.pnlLoginFuelman.ResumeLayout(false);
            this.pnlLoginFuelman.PerformLayout();
            this.pnlLoginDriver.ResumeLayout(false);
            this.pnlLoginDriver.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox txtUSername;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblMesage;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblUrlConnection;
        private System.Windows.Forms.Button btnConnection;
        private System.Windows.Forms.Panel pnlLoginFuelman;
        private System.Windows.Forms.Label lblAppVersion;
        //private FontAwesome.Sharp.IconButton iconBtnHide;
        //private FontAwesome.Sharp.IconButton iconBtnShow;
        private System.Windows.Forms.Label label4;
        private CustomButton btnLogin;
        private CustomButton btnCancel;
        private System.Windows.Forms.Label lblCheckConnection;
        private System.Windows.Forms.Timer tmr_counting;
        private FontAwesome.Sharp.IconButton iconBtnHide;
        private FontAwesome.Sharp.IconButton iconBtnShow;
        private System.Windows.Forms.ListBox listHistoryLogin;
        private System.Windows.Forms.Panel pnlLoginDriver;
        private System.Windows.Forms.ListBox listHistoryLoginDriver;
        private CustomButton btnLoginDriver;
        private System.Windows.Forms.TextBox txtPasswordDriver;
        private System.Windows.Forms.TextBox txtUsernameDriver;
        private System.Windows.Forms.Label lblMesageDriver;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private CustomButton customButton2;
        private FontAwesome.Sharp.IconButton iconButton1;
        private FontAwesome.Sharp.IconButton iconButton2;
        //private beautifyForm.CustomButton btnLogin;
        //private beautifyForm.CustomButton customButton2;
    }
}