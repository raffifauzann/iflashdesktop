﻿namespace LCR600SDK
{
    partial class FSendPOT
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FSendPOT));
            this.panel1 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.dateTimePicker2 = new LCR600SDK.MyDateTimePicker();
            this.dateTimePicker1 = new LCR600SDK.MyDateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.numGoToPage = new System.Windows.Forms.NumericUpDown();
            this.button2 = new System.Windows.Forms.Button();
            this.lblPageOf = new System.Windows.Forms.Label();
            this.btnNext = new System.Windows.Forms.Button();
            this.txtPage = new System.Windows.Forms.TextBox();
            this.btnPrev = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.sendPoNoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pidRitasiMaintankDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.transportirNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sendQtyDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sendDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sendFromDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sendToDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sendDoNoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sendFtNoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sendDriverNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.districtDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DocDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.listDatumRitasiMaintankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.db_trainingDataSet = new LCR600SDK.db_trainingDataSet();
            this.listDatumRitasiMaintankTableAdapter = new LCR600SDK.db_trainingDataSetTableAdapters.ListDatumRitasiMaintankTableAdapter();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numGoToPage)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.listDatumRitasiMaintankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.db_trainingDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(101)))), ((int)(((byte)(179)))));
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.dateTimePicker2);
            this.panel1.Controls.Add(this.dateTimePicker1);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.btnSearch);
            this.panel1.Controls.Add(this.btnAdd);
            this.panel1.Controls.Add(this.txtSearch);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(974, 87);
            this.panel1.TabIndex = 24;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(511, 3);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(141, 29);
            this.label3.TabIndex = 95;
            this.label3.Text = "PO Number";
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 36F);
            this.dateTimePicker2.CustomFormat = "yyyy-MM-dd";
            this.dateTimePicker2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.dateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker2.Location = new System.Drawing.Point(312, 35);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(169, 35);
            this.dateTimePicker2.TabIndex = 94;
            this.dateTimePicker2.Value = new System.DateTime(2022, 2, 23, 10, 34, 50, 0);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker1.CustomFormat = "yyyy-MM-dd";
            this.dateTimePicker1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker1.Location = new System.Drawing.Point(128, 37);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(169, 35);
            this.dateTimePicker1.TabIndex = 93;
            this.dateTimePicker1.Value = new System.DateTime(2022, 2, 23, 10, 34, 50, 0);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(303, 1);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(190, 29);
            this.label2.TabIndex = 92;
            this.label2.Text = "Sampai Tanggal";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(136, 1);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(152, 29);
            this.label1.TabIndex = 91;
            this.label1.Text = "Dari Tanggal";
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.BackColor = System.Drawing.Color.Red;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(874, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(97, 36);
            this.button1.TabIndex = 27;
            this.button1.Text = "Close";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.BackColor = System.Drawing.Color.Orange;
            this.btnSearch.FlatAppearance.BorderSize = 0;
            this.btnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.btnSearch.Location = new System.Drawing.Point(700, 17);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(97, 39);
            this.btnSearch.TabIndex = 20;
            this.btnSearch.Text = "Cari";
            this.btnSearch.UseVisualStyleBackColor = false;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.Chartreuse;
            this.btnAdd.FlatAppearance.BorderSize = 0;
            this.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.btnAdd.ForeColor = System.Drawing.Color.Black;
            this.btnAdd.Location = new System.Drawing.Point(9, 25);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(110, 36);
            this.btnAdd.TabIndex = 3;
            this.btnAdd.Text = "Tambah";
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // txtSearch
            // 
            this.txtSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.txtSearch.Location = new System.Drawing.Point(499, 35);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(176, 35);
            this.txtSearch.TabIndex = 1;
            this.txtSearch.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSearch_KeyPress);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(101)))), ((int)(((byte)(179)))));
            this.panel3.Controls.Add(this.numGoToPage);
            this.panel3.Controls.Add(this.button2);
            this.panel3.Controls.Add(this.lblPageOf);
            this.panel3.Controls.Add(this.btnNext);
            this.panel3.Controls.Add(this.txtPage);
            this.panel3.Controls.Add(this.btnPrev);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 403);
            this.panel3.Margin = new System.Windows.Forms.Padding(1);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(974, 42);
            this.panel3.TabIndex = 25;
            // 
            // numGoToPage
            // 
            this.numGoToPage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.numGoToPage.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.numGoToPage.Location = new System.Drawing.Point(749, 5);
            this.numGoToPage.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numGoToPage.Name = "numGoToPage";
            this.numGoToPage.Size = new System.Drawing.Size(71, 35);
            this.numGoToPage.TabIndex = 32;
            this.numGoToPage.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.BackColor = System.Drawing.Color.MidnightBlue;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(826, 5);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(136, 35);
            this.button2.TabIndex = 31;
            this.button2.Text = "Go To Page";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // lblPageOf
            // 
            this.lblPageOf.AutoSize = true;
            this.lblPageOf.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.lblPageOf.ForeColor = System.Drawing.Color.White;
            this.lblPageOf.Location = new System.Drawing.Point(167, 3);
            this.lblPageOf.Name = "lblPageOf";
            this.lblPageOf.Size = new System.Drawing.Size(70, 29);
            this.lblPageOf.TabIndex = 28;
            this.lblPageOf.Text = "Page";
            // 
            // btnNext
            // 
            this.btnNext.Enabled = false;
            this.btnNext.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.btnNext.Location = new System.Drawing.Point(119, 5);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(42, 35);
            this.btnNext.TabIndex = 27;
            this.btnNext.Text = ">";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // txtPage
            // 
            this.txtPage.Enabled = false;
            this.txtPage.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.txtPage.Location = new System.Drawing.Point(67, 5);
            this.txtPage.Name = "txtPage";
            this.txtPage.Size = new System.Drawing.Size(46, 35);
            this.txtPage.TabIndex = 26;
            this.txtPage.Text = "0";
            this.txtPage.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnPrev
            // 
            this.btnPrev.Enabled = false;
            this.btnPrev.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.btnPrev.Location = new System.Drawing.Point(19, 5);
            this.btnPrev.Name = "btnPrev";
            this.btnPrev.Size = new System.Drawing.Size(42, 35);
            this.btnPrev.TabIndex = 25;
            this.btnPrev.Text = "<";
            this.btnPrev.UseVisualStyleBackColor = true;
            this.btnPrev.Click += new System.EventHandler(this.btnPrev_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dataGridView1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 87);
            this.panel2.Margin = new System.Windows.Forms.Padding(1);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(974, 316);
            this.panel2.TabIndex = 26;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridView1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(101)))), ((int)(((byte)(179)))));
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.sendPoNoDataGridViewTextBoxColumn,
            this.pidRitasiMaintankDataGridViewTextBoxColumn,
            this.transportirNameDataGridViewTextBoxColumn,
            this.sendQtyDataGridViewTextBoxColumn,
            this.sendDateDataGridViewTextBoxColumn,
            this.sendFromDataGridViewTextBoxColumn,
            this.sendToDataGridViewTextBoxColumn,
            this.sendDoNoDataGridViewTextBoxColumn,
            this.sendFtNoDataGridViewTextBoxColumn,
            this.sendDriverNameDataGridViewTextBoxColumn,
            this.districtDataGridViewTextBoxColumn,
            this.DocDescription});
            this.dataGridView1.DataSource = this.listDatumRitasiMaintankBindingSource;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.Size = new System.Drawing.Size(974, 316);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            this.dataGridView1.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridView1_CellFormatting);
            this.dataGridView1.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dataGridView1_DataBindingComplete);
            // 
            // sendPoNoDataGridViewTextBoxColumn
            // 
            this.sendPoNoDataGridViewTextBoxColumn.DataPropertyName = "SendPoNo";
            this.sendPoNoDataGridViewTextBoxColumn.HeaderText = "SendPoNo";
            this.sendPoNoDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.sendPoNoDataGridViewTextBoxColumn.Name = "sendPoNoDataGridViewTextBoxColumn";
            this.sendPoNoDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.sendPoNoDataGridViewTextBoxColumn.Width = 138;
            // 
            // pidRitasiMaintankDataGridViewTextBoxColumn
            // 
            this.pidRitasiMaintankDataGridViewTextBoxColumn.DataPropertyName = "PidRitasiMaintank";
            this.pidRitasiMaintankDataGridViewTextBoxColumn.HeaderText = "PidRitasiMaintank";
            this.pidRitasiMaintankDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.pidRitasiMaintankDataGridViewTextBoxColumn.Name = "pidRitasiMaintankDataGridViewTextBoxColumn";
            this.pidRitasiMaintankDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.pidRitasiMaintankDataGridViewTextBoxColumn.Visible = false;
            this.pidRitasiMaintankDataGridViewTextBoxColumn.Width = 211;
            // 
            // transportirNameDataGridViewTextBoxColumn
            // 
            this.transportirNameDataGridViewTextBoxColumn.DataPropertyName = "TransportirName";
            this.transportirNameDataGridViewTextBoxColumn.HeaderText = "TransportirName";
            this.transportirNameDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.transportirNameDataGridViewTextBoxColumn.Name = "transportirNameDataGridViewTextBoxColumn";
            this.transportirNameDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.transportirNameDataGridViewTextBoxColumn.Width = 202;
            // 
            // sendQtyDataGridViewTextBoxColumn
            // 
            this.sendQtyDataGridViewTextBoxColumn.DataPropertyName = "SendQty";
            this.sendQtyDataGridViewTextBoxColumn.HeaderText = "SendQty";
            this.sendQtyDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.sendQtyDataGridViewTextBoxColumn.Name = "sendQtyDataGridViewTextBoxColumn";
            this.sendQtyDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.sendQtyDataGridViewTextBoxColumn.Width = 112;
            // 
            // sendDateDataGridViewTextBoxColumn
            // 
            this.sendDateDataGridViewTextBoxColumn.DataPropertyName = "SendDate";
            this.sendDateDataGridViewTextBoxColumn.HeaderText = "SendDate";
            this.sendDateDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.sendDateDataGridViewTextBoxColumn.Name = "sendDateDataGridViewTextBoxColumn";
            this.sendDateDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.sendDateDataGridViewTextBoxColumn.Width = 126;
            // 
            // sendFromDataGridViewTextBoxColumn
            // 
            this.sendFromDataGridViewTextBoxColumn.DataPropertyName = "SendFrom";
            this.sendFromDataGridViewTextBoxColumn.HeaderText = "SendFrom";
            this.sendFromDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.sendFromDataGridViewTextBoxColumn.Name = "sendFromDataGridViewTextBoxColumn";
            this.sendFromDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.sendFromDataGridViewTextBoxColumn.Width = 133;
            // 
            // sendToDataGridViewTextBoxColumn
            // 
            this.sendToDataGridViewTextBoxColumn.DataPropertyName = "SendTo";
            this.sendToDataGridViewTextBoxColumn.HeaderText = "SendTo";
            this.sendToDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.sendToDataGridViewTextBoxColumn.Name = "sendToDataGridViewTextBoxColumn";
            this.sendToDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.sendToDataGridViewTextBoxColumn.Width = 106;
            // 
            // sendDoNoDataGridViewTextBoxColumn
            // 
            this.sendDoNoDataGridViewTextBoxColumn.DataPropertyName = "SendDoNo";
            this.sendDoNoDataGridViewTextBoxColumn.HeaderText = "SendDoNo";
            this.sendDoNoDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.sendDoNoDataGridViewTextBoxColumn.Name = "sendDoNoDataGridViewTextBoxColumn";
            this.sendDoNoDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.sendDoNoDataGridViewTextBoxColumn.Width = 139;
            // 
            // sendFtNoDataGridViewTextBoxColumn
            // 
            this.sendFtNoDataGridViewTextBoxColumn.DataPropertyName = "SendFtNo";
            this.sendFtNoDataGridViewTextBoxColumn.HeaderText = "SendFtNo";
            this.sendFtNoDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.sendFtNoDataGridViewTextBoxColumn.Name = "sendFtNoDataGridViewTextBoxColumn";
            this.sendFtNoDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.sendFtNoDataGridViewTextBoxColumn.Width = 129;
            // 
            // sendDriverNameDataGridViewTextBoxColumn
            // 
            this.sendDriverNameDataGridViewTextBoxColumn.DataPropertyName = "SendDriverName";
            this.sendDriverNameDataGridViewTextBoxColumn.HeaderText = "SendDriverName";
            this.sendDriverNameDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.sendDriverNameDataGridViewTextBoxColumn.Name = "sendDriverNameDataGridViewTextBoxColumn";
            this.sendDriverNameDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.sendDriverNameDataGridViewTextBoxColumn.Width = 205;
            // 
            // districtDataGridViewTextBoxColumn
            // 
            this.districtDataGridViewTextBoxColumn.DataPropertyName = "District";
            this.districtDataGridViewTextBoxColumn.HeaderText = "District";
            this.districtDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.districtDataGridViewTextBoxColumn.Name = "districtDataGridViewTextBoxColumn";
            this.districtDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.districtDataGridViewTextBoxColumn.Width = 92;
            // 
            // DocDescription
            // 
            this.DocDescription.DataPropertyName = "DocDescription";
            this.DocDescription.HeaderText = "Doc Status";
            this.DocDescription.MinimumWidth = 6;
            this.DocDescription.Name = "DocDescription";
            this.DocDescription.Width = 153;
            // 
            // listDatumRitasiMaintankBindingSource
            // 
            this.listDatumRitasiMaintankBindingSource.DataMember = "ListDatumRitasiMaintank";
            this.listDatumRitasiMaintankBindingSource.DataSource = this.db_trainingDataSet;
            // 
            // db_trainingDataSet
            // 
            this.db_trainingDataSet.DataSetName = "db_trainingDataSet";
            this.db_trainingDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // listDatumRitasiMaintankTableAdapter
            // 
            this.listDatumRitasiMaintankTableAdapter.ClearBeforeFill = true;
            // 
            // FSendPOT
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(974, 445);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel3);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(1);
            this.Name = "FSendPOT";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Pengiriman POT";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FReceivedFTPort_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numGoToPage)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.listDatumRitasiMaintankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.db_trainingDataSet)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private db_trainingDataSet db_trainingDataSet;
        private System.Windows.Forms.BindingSource listDatumRitasiMaintankBindingSource;
        private db_trainingDataSetTableAdapters.ListDatumRitasiMaintankTableAdapter listDatumRitasiMaintankTableAdapter;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.Label lblPageOf;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.TextBox txtPage;
        private System.Windows.Forms.Button btnPrev;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.DataGridViewTextBoxColumn sendPoNoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn pidRitasiMaintankDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn transportirNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sendQtyDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sendDateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sendFromDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sendToDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sendDoNoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sendFtNoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sendDriverNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn districtDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn DocDescription;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.NumericUpDown numGoToPage;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label3;
        private MyDateTimePicker dateTimePicker2;
        private MyDateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}