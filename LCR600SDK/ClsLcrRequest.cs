﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LCR600SDK
{
    class ClsLcrRequest
    {
        public double Qty;
        public bool Status;

        public ClsLcrRequest(double qty, bool status)
        {
            Qty = qty;
            Status = status;
        }
    }
}
