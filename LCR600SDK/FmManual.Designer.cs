﻿
namespace LCR600SDK
{
    partial class FmManual
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FmManual));
            this.tmrLCR = new System.Windows.Forms.Timer(this.components);
            this.grpManual = new System.Windows.Forms.GroupBox();
            this.txtPreset = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.lblManualValue = new System.Windows.Forms.Label();
            this.lblManualStatus = new System.Windows.Forms.Label();
            this.btnManualTotalizer = new System.Windows.Forms.Button();
            this.btnManualGrossQty = new System.Windows.Forms.Button();
            this.btnManualCloseValve = new System.Windows.Forms.Button();
            this.btnManualOpenValve = new System.Windows.Forms.Button();
            this.btnManualDisconnect = new System.Windows.Forms.Button();
            this.btnManualConnect = new System.Windows.Forms.Button();
            this.grpAuto = new System.Windows.Forms.GroupBox();
            this.lblCountDown = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtRefStop = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtRefStart = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtAutoPreset = new System.Windows.Forms.TextBox();
            this.lblAutoFlowAkhir1 = new System.Windows.Forms.Label();
            this.lblAutoFLowAwal1 = new System.Windows.Forms.Label();
            this.lblAutoValueQty = new System.Windows.Forms.Label();
            this.lblAutoStatus = new System.Windows.Forms.Label();
            this.btnAutoStop = new System.Windows.Forms.Button();
            this.tmrCountDown = new System.Windows.Forms.Timer(this.components);
            this.grpManual.SuspendLayout();
            this.grpAuto.SuspendLayout();
            this.SuspendLayout();
            // 
            // tmrLCR
            // 
            this.tmrLCR.Tick += new System.EventHandler(this.tmrLCR_Tick);
            // 
            // grpManual
            // 
            this.grpManual.Controls.Add(this.txtPreset);
            this.grpManual.Controls.Add(this.button1);
            this.grpManual.Controls.Add(this.lblManualValue);
            this.grpManual.Controls.Add(this.lblManualStatus);
            this.grpManual.Controls.Add(this.btnManualTotalizer);
            this.grpManual.Controls.Add(this.btnManualGrossQty);
            this.grpManual.Controls.Add(this.btnManualCloseValve);
            this.grpManual.Controls.Add(this.btnManualOpenValve);
            this.grpManual.Controls.Add(this.btnManualDisconnect);
            this.grpManual.Controls.Add(this.btnManualConnect);
            this.grpManual.Location = new System.Drawing.Point(12, 49);
            this.grpManual.Name = "grpManual";
            this.grpManual.Size = new System.Drawing.Size(257, 228);
            this.grpManual.TabIndex = 9;
            this.grpManual.TabStop = false;
            this.grpManual.Text = "Manual";
            // 
            // txtPreset
            // 
            this.txtPreset.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPreset.Location = new System.Drawing.Point(132, 141);
            this.txtPreset.Name = "txtPreset";
            this.txtPreset.Size = new System.Drawing.Size(111, 23);
            this.txtPreset.TabIndex = 9;
            this.txtPreset.Text = "0";
            this.txtPreset.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(15, 136);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(111, 33);
            this.button1.TabIndex = 8;
            this.button1.Text = "Set Preset Gross";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // lblManualValue
            // 
            this.lblManualValue.AutoSize = true;
            this.lblManualValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblManualValue.Location = new System.Drawing.Point(12, 235);
            this.lblManualValue.Name = "lblManualValue";
            this.lblManualValue.Size = new System.Drawing.Size(44, 17);
            this.lblManualValue.TabIndex = 7;
            this.lblManualValue.Text = "Value";
            // 
            // lblManualStatus
            // 
            this.lblManualStatus.AutoSize = true;
            this.lblManualStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblManualStatus.Location = new System.Drawing.Point(12, 211);
            this.lblManualStatus.Name = "lblManualStatus";
            this.lblManualStatus.Size = new System.Drawing.Size(48, 17);
            this.lblManualStatus.TabIndex = 6;
            this.lblManualStatus.Text = "Status";
            // 
            // btnManualTotalizer
            // 
            this.btnManualTotalizer.Location = new System.Drawing.Point(132, 97);
            this.btnManualTotalizer.Name = "btnManualTotalizer";
            this.btnManualTotalizer.Size = new System.Drawing.Size(111, 33);
            this.btnManualTotalizer.TabIndex = 5;
            this.btnManualTotalizer.Text = "Gross Totalizer";
            this.btnManualTotalizer.UseVisualStyleBackColor = true;
            this.btnManualTotalizer.Click += new System.EventHandler(this.btnManualTotalizer_Click);
            // 
            // btnManualGrossQty
            // 
            this.btnManualGrossQty.Location = new System.Drawing.Point(15, 97);
            this.btnManualGrossQty.Name = "btnManualGrossQty";
            this.btnManualGrossQty.Size = new System.Drawing.Size(111, 33);
            this.btnManualGrossQty.TabIndex = 4;
            this.btnManualGrossQty.Text = "Gross QTY";
            this.btnManualGrossQty.UseVisualStyleBackColor = true;
            this.btnManualGrossQty.Click += new System.EventHandler(this.btnManualGrossQty_Click);
            // 
            // btnManualCloseValve
            // 
            this.btnManualCloseValve.Location = new System.Drawing.Point(132, 58);
            this.btnManualCloseValve.Name = "btnManualCloseValve";
            this.btnManualCloseValve.Size = new System.Drawing.Size(111, 33);
            this.btnManualCloseValve.TabIndex = 3;
            this.btnManualCloseValve.Text = "Close Valve";
            this.btnManualCloseValve.UseVisualStyleBackColor = true;
            this.btnManualCloseValve.Click += new System.EventHandler(this.btnManualCloseValve_Click);
            // 
            // btnManualOpenValve
            // 
            this.btnManualOpenValve.Location = new System.Drawing.Point(15, 58);
            this.btnManualOpenValve.Name = "btnManualOpenValve";
            this.btnManualOpenValve.Size = new System.Drawing.Size(111, 33);
            this.btnManualOpenValve.TabIndex = 2;
            this.btnManualOpenValve.Text = "Open Valve";
            this.btnManualOpenValve.UseVisualStyleBackColor = true;
            this.btnManualOpenValve.Click += new System.EventHandler(this.btnManualOpenValve_Click);
            // 
            // btnManualDisconnect
            // 
            this.btnManualDisconnect.Location = new System.Drawing.Point(74, 174);
            this.btnManualDisconnect.Name = "btnManualDisconnect";
            this.btnManualDisconnect.Size = new System.Drawing.Size(111, 33);
            this.btnManualDisconnect.TabIndex = 1;
            this.btnManualDisconnect.Text = "Disconnect LCR";
            this.btnManualDisconnect.UseVisualStyleBackColor = true;
            this.btnManualDisconnect.Click += new System.EventHandler(this.btnManualDisconnect_Click);
            // 
            // btnManualConnect
            // 
            this.btnManualConnect.Location = new System.Drawing.Point(74, 19);
            this.btnManualConnect.Name = "btnManualConnect";
            this.btnManualConnect.Size = new System.Drawing.Size(111, 33);
            this.btnManualConnect.TabIndex = 0;
            this.btnManualConnect.Text = "Connect LCR";
            this.btnManualConnect.UseVisualStyleBackColor = true;
            this.btnManualConnect.Click += new System.EventHandler(this.btnManualConnect_Click);
            // 
            // grpAuto
            // 
            this.grpAuto.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(101)))), ((int)(((byte)(179)))));
            this.grpAuto.Controls.Add(this.lblCountDown);
            this.grpAuto.Controls.Add(this.label6);
            this.grpAuto.Controls.Add(this.label5);
            this.grpAuto.Controls.Add(this.label4);
            this.grpAuto.Controls.Add(this.label3);
            this.grpAuto.Controls.Add(this.txtRefStop);
            this.grpAuto.Controls.Add(this.label2);
            this.grpAuto.Controls.Add(this.txtRefStart);
            this.grpAuto.Controls.Add(this.label1);
            this.grpAuto.Controls.Add(this.txtAutoPreset);
            this.grpAuto.Controls.Add(this.lblAutoFlowAkhir1);
            this.grpAuto.Controls.Add(this.lblAutoFLowAwal1);
            this.grpAuto.Controls.Add(this.lblAutoValueQty);
            this.grpAuto.Controls.Add(this.lblAutoStatus);
            this.grpAuto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpAuto.Location = new System.Drawing.Point(0, 0);
            this.grpAuto.Name = "grpAuto";
            this.grpAuto.Size = new System.Drawing.Size(1028, 460);
            this.grpAuto.TabIndex = 10;
            this.grpAuto.TabStop = false;
            // 
            // lblCountDown
            // 
            this.lblCountDown.AutoSize = true;
            this.lblCountDown.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCountDown.ForeColor = System.Drawing.Color.White;
            this.lblCountDown.Location = new System.Drawing.Point(14, 301);
            this.lblCountDown.Name = "lblCountDown";
            this.lblCountDown.Size = new System.Drawing.Size(99, 37);
            this.lblCountDown.TabIndex = 24;
            this.lblCountDown.Text = "Value";
            this.lblCountDown.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblCountDown.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(390, 101);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(237, 29);
            this.label6.TabIndex = 23;
            this.label6.Text = "Gross Totalizer Akhir";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(390, 29);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(235, 29);
            this.label5.TabIndex = 22;
            this.label5.Text = "Gross Totalizer Awal";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(7, 192);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(169, 29);
            this.label4.TabIndex = 21;
            this.label4.Text = "Quantity (Liter)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(7, 111);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 29);
            this.label3.TabIndex = 20;
            this.label3.Text = "Ref Stop";
            // 
            // txtRefStop
            // 
            this.txtRefStop.Enabled = false;
            this.txtRefStop.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.txtRefStop.Location = new System.Drawing.Point(122, 109);
            this.txtRefStop.Name = "txtRefStop";
            this.txtRefStop.Size = new System.Drawing.Size(248, 35);
            this.txtRefStop.TabIndex = 19;
            this.txtRefStop.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(6, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 29);
            this.label2.TabIndex = 18;
            this.label2.Text = "Ref Start";
            // 
            // txtRefStart
            // 
            this.txtRefStart.Enabled = false;
            this.txtRefStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.txtRefStart.Location = new System.Drawing.Point(122, 68);
            this.txtRefStart.Name = "txtRefStart";
            this.txtRefStart.Size = new System.Drawing.Size(248, 35);
            this.txtRefStart.TabIndex = 17;
            this.txtRefStart.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(6, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(241, 29);
            this.label1.TabIndex = 16;
            this.label1.Text = "Preset Gross Volume";
            // 
            // txtAutoPreset
            // 
            this.txtAutoPreset.Enabled = false;
            this.txtAutoPreset.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.txtAutoPreset.Location = new System.Drawing.Point(259, 23);
            this.txtAutoPreset.Name = "txtAutoPreset";
            this.txtAutoPreset.Size = new System.Drawing.Size(111, 35);
            this.txtAutoPreset.TabIndex = 15;
            this.txtAutoPreset.Text = "0";
            this.txtAutoPreset.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblAutoFlowAkhir1
            // 
            this.lblAutoFlowAkhir1.AutoSize = true;
            this.lblAutoFlowAkhir1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAutoFlowAkhir1.ForeColor = System.Drawing.Color.White;
            this.lblAutoFlowAkhir1.Location = new System.Drawing.Point(398, 130);
            this.lblAutoFlowAkhir1.Name = "lblAutoFlowAkhir1";
            this.lblAutoFlowAkhir1.Size = new System.Drawing.Size(83, 31);
            this.lblAutoFlowAkhir1.TabIndex = 14;
            this.lblAutoFlowAkhir1.Text = "Value";
            this.lblAutoFlowAkhir1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblAutoFLowAwal1
            // 
            this.lblAutoFLowAwal1.AutoSize = true;
            this.lblAutoFLowAwal1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAutoFLowAwal1.ForeColor = System.Drawing.Color.White;
            this.lblAutoFLowAwal1.Location = new System.Drawing.Point(398, 58);
            this.lblAutoFLowAwal1.Name = "lblAutoFLowAwal1";
            this.lblAutoFLowAwal1.Size = new System.Drawing.Size(83, 31);
            this.lblAutoFLowAwal1.TabIndex = 13;
            this.lblAutoFLowAwal1.Text = "Value";
            this.lblAutoFLowAwal1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblAutoValueQty
            // 
            this.lblAutoValueQty.AutoSize = true;
            this.lblAutoValueQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 38.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAutoValueQty.ForeColor = System.Drawing.Color.White;
            this.lblAutoValueQty.Location = new System.Drawing.Point(12, 221);
            this.lblAutoValueQty.Name = "lblAutoValueQty";
            this.lblAutoValueQty.Size = new System.Drawing.Size(154, 59);
            this.lblAutoValueQty.TabIndex = 12;
            this.lblAutoValueQty.Text = "Value";
            this.lblAutoValueQty.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblAutoStatus
            // 
            this.lblAutoStatus.AutoSize = true;
            this.lblAutoStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAutoStatus.ForeColor = System.Drawing.Color.White;
            this.lblAutoStatus.Location = new System.Drawing.Point(12, 158);
            this.lblAutoStatus.Name = "lblAutoStatus";
            this.lblAutoStatus.Size = new System.Drawing.Size(48, 17);
            this.lblAutoStatus.TabIndex = 11;
            this.lblAutoStatus.Text = "Status";
            // 
            // btnAutoStop
            // 
            this.btnAutoStop.BackColor = System.Drawing.Color.Red;
            this.btnAutoStop.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnAutoStop.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.btnAutoStop.ForeColor = System.Drawing.Color.White;
            this.btnAutoStop.Location = new System.Drawing.Point(0, 460);
            this.btnAutoStop.Name = "btnAutoStop";
            this.btnAutoStop.Size = new System.Drawing.Size(1028, 112);
            this.btnAutoStop.TabIndex = 10;
            this.btnAutoStop.Text = "Stop Transaction";
            this.btnAutoStop.UseVisualStyleBackColor = false;
            this.btnAutoStop.Click += new System.EventHandler(this.btnAutoStop_Click);
            // 
            // tmrCountDown
            // 
            this.tmrCountDown.Interval = 1000;
            this.tmrCountDown.Tick += new System.EventHandler(this.tmrCountDown_Tick);
            // 
            // FmManual
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1028, 572);
            this.ControlBox = false;
            this.Controls.Add(this.grpAuto);
            this.Controls.Add(this.grpManual);
            this.Controls.Add(this.btnAutoStop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FmManual";
            this.Text = "FmManual";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FmManual_Load);
            this.grpManual.ResumeLayout(false);
            this.grpManual.PerformLayout();
            this.grpAuto.ResumeLayout(false);
            this.grpAuto.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Timer tmrLCR;
        private System.Windows.Forms.GroupBox grpManual;
        private System.Windows.Forms.Button btnManualTotalizer;
        private System.Windows.Forms.Button btnManualGrossQty;
        private System.Windows.Forms.Button btnManualCloseValve;
        private System.Windows.Forms.Button btnManualOpenValve;
        private System.Windows.Forms.Button btnManualDisconnect;
        private System.Windows.Forms.Button btnManualConnect;
        private System.Windows.Forms.Label lblManualValue;
        private System.Windows.Forms.Label lblManualStatus;
        private System.Windows.Forms.GroupBox grpAuto;
        private System.Windows.Forms.Label lblAutoStatus;
        private System.Windows.Forms.Button btnAutoStop;
        private System.Windows.Forms.Label lblAutoValueQty;
        private System.Windows.Forms.Label lblAutoFLowAwal1;
        private System.Windows.Forms.Label lblAutoFlowAkhir1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtPreset;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtAutoPreset;
        private System.Windows.Forms.TextBox txtRefStart;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtRefStop;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblCountDown;
        private System.Windows.Forms.Timer tmrCountDown;
    }
}